<?php include'header.php'; echo "Hello ! ".$username = $_SESSION['username']; ?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Add Wheat Vehicles</li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row">
        <div class="col-lg-2"><a href="<?php echo base_url(); ?>index.php/floorc/floorRpt">Reports</a></div>
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-12">
        	<h2>Vehicle Arrival Form</h2>
        </div>
    </div><br /><br />
    
    <form action="<?php echo base_url(); ?>index.php/floorc/VhclEntry" method="post">
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Registration Number</b></div>
        <div class="col-lg-3"><input type="text" id="vhcl_reg_no" name="vhcl_reg_no" value="" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Type</b></div>
        <div class="col-lg-3">
        <select id="vhcl_type" name="vhcl_type" class="form-control" required>
        	<option value="">--Select--</option>
            <option value="Truck">Truck</option>
            <option value="Pickup">Pickup</option>
            <option value="Tempo">Tempo</option>
            <option value="Other">Other</option>
        </select>
        </div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Arrival Time</b></div>
        <div class="col-lg-3"><input type="text" id="vehicle_arr_tm" name="vehicle_arr_tm" value="" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Driver's Name</b></div>
        <div class="col-lg-3"><input type="text" id="driver_nm" name="driver_nm" value="" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Driver's Mobile Number</b></div>
        <div class="col-lg-3"><input type="text" id="driver_mob_no" name="driver_mob_no" value="" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Documents Given By Driver</b></div>
        <div class="col-lg-3"><input type="text" id="doc_by_driver" name="doc_by_driver" value="" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Arrived From Place</b></div>
        <div class="col-lg-3"><input type="text" id="vehicle_arr_plc" name="vehicle_arr_plc" value="" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-5"></div>
        <div class="col-lg-2"><input type="submit" id="submit_btn" name="submit_btn" value="Submit" class="form-control" /></div>
        <div class="col-lg-5"></div>
    </div><br />
    </form>
  </section>
</section>
<?php include('footer.php'); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
$('#vehicle_arr_tm').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '24',
    maxTime: '11:55pm',
    defaultTime: '11',
    startTime: '24:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

$('#vehicle_dep_tm').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '24',
    maxTime: '11:55pm',
    defaultTime: '11',
    startTime: '24:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});
</script>