<?php include'header.php'; echo "Hello ! ".$username = $_SESSION['username']; ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i> Logistics Management</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Logistics Management</li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/floorc/floorRpt" target="_blank">
                <img src="<?php echo base_url(); ?>assets/admin/db/logisitics/material_vehicle.png" width="50%"/><br>
                Material Vehicles
            </a>
        </div>
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/logisitics/fg_dispatch.png" width="50%"/><br>
            FG Dispatch
        </div>
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/logisitics/ref_dispatch.png" width="50%"/><br>
            Refraction Dispatch
        </div>
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/logisitics/other_dispatch.png" width="50%"/><br>
            Other Dispatch
        </div>
    </div><br /><br />
  </section>
</section>
<?php include('footer.php'); ?>