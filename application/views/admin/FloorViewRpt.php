<?php include'header.php'; echo "Hello ! ".$username = $_SESSION['username']; ?>
<style>
	button.accordion {
		background-color:#ddd;
		color: #444;
		cursor: pointer;
		padding: 2px;
		width: 100%;
		border: none;
		text-align: center;
		font-weight:bold;
		outline: none;
		font-size: 12px;
		transition: 0.4s;
		border-radius:8px;
	}
	
	button.accordion.active, button.accordion:hover {
		background-color: #999999;
	}
	
	button.accordion:after {
		content: '\02795';
		font-size: 13px;
		color: #777;
		float: right;
		margin-left: 5px;
	}
	
	button.accordion.active:after {
		content: "\2796";
	}
	
	div.panel {
		padding: 0 5px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: 0.6s ease-in-out;
		opacity: 0;
		margin-bottom:4px;
	}
	
	div.panel.show {
		opacity: 1;
		max-height: 300px;
	}
	
	table thead tr{
		display:block;
	}
	
	table th,table td{
		width:300px;
	}
	
	table  tbody{		
		display:block;
		height:200px;
		overflow:auto;
	}
</style>


<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Floor Summary Report</li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row">
        <div class="col-lg-2"><a href="<?php echo base_url(); ?>index.php/floorc/floorRpt">Reports</a></div>
    </div>
    
    <div class="row">
    	<div class="col-lg-2"></div>
    	<div class="col-lg-8">
            <button class="accordion" style="color:black; font-weight:bold;">
            	1. Pending For Vehicle Entry
            </button>
            <div class="panel">
                <div id="mrp">
                    <h5 style="text-align:center"><a href="<?php echo base_url(); ?>index.php/floorc/VhclEntryView" target="_blank">Create New Entry</a></h5>
                </div>
            </div>
            
            <button class="accordion" style="color:black; font-weight:bold;">
            	2. Pending For Weighning
            </button>
            <div class="panel">
                <div id="mrp">
                    <table class="table table-bordered">
                        <tr style="background-color:#CCC">
                            <td>SNO</td>
                            <td>vhcl_reg_no</td>
                            <td>vhcl_type</td>
                            <td>vehicle_arr_tm</td>
                            <td>driver_nm</td>
                            <td>doc_by_driver</td>
                            <td>vehicle_arr_plc</td>
                            <td>created_by</td>
                            <td>created_date</td>
                        </tr>
                      <?php
                      $sql = "select * from vehicle_entry where status = 'Pending For Weighting Entry'";
                      $qry = $this->db->query($sql);
                      
                      $cnt=0;
                      foreach($qry->result() as $row){
                          $cnt++;
                      ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><a href="<?php echo base_url(); ?>index.php/floorc/weighing?vhcl_reg_no=<?php echo $row->vhcl_reg_no; ?>" target="_blank">
                            <?php echo $row->vhcl_reg_no; ?></a>
                            </td>
                            <td><?php echo $row->vhcl_type; ?></td>
                            <td><?php echo $row->vehicle_arr_tm; ?></td>
                            <td><?php echo $row->driver_nm; ?></td>
                            <td><?php echo $row->doc_by_driver; ?></td>
                            <td><?php echo $row->vehicle_arr_plc; ?></td>
                            <td><?php echo $row->created_by; ?></td>
                            <td><?php echo $row->created_date; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            
            <button class="accordion" style="color:black; font-weight:bold;">
            	3. Pending For Sample Test
            </button>
            <div class="panel">
                <div id="mrp">
                    <table class="table table-bordered">
                        <tr style="background-color:#CCC">
                            <td>SNO</td>
                            <td>vhcl_reg_no</td>
                            <td>vhcl_type</td>
                            <td>vehicle_arr_tm</td>
                            <td>driver_nm</td>
                            <td>doc_by_driver</td>
                            <td>vehicle_arr_plc</td>
                            <td>created_by</td>
                            <td>created_date</td>
                        </tr>
                      <?php
                      $sql = "select * from vehicle_entry where status = 'Pending For Sample Test'";
                      $qry = $this->db->query($sql);
                      
                      $cnt=0;
                      foreach($qry->result() as $row){
                          $cnt++;
                      ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><a href="<?php echo base_url(); ?>index.php/floorc/SampleTest?vhcl_reg_no=<?php echo $row->vhcl_reg_no; ?>" target="_blank">
                            <?php echo $row->vhcl_reg_no; ?></a></td>
                            <td><?php echo $row->vhcl_type; ?></td>
                            <td><?php echo $row->vehicle_arr_tm; ?></td>
                            <td><?php echo $row->driver_nm; ?></td>
                            <td><?php echo $row->doc_by_driver; ?></td>
                            <td><?php echo $row->vehicle_arr_plc; ?></td>
                            <td><?php echo $row->created_by; ?></td>
                            <td><?php echo $row->created_date; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            
            <button class="accordion" style="color:black; font-weight:bold;">
            	4. Pending For Sample Test Approval
            </button>
            <div class="panel">
                <div id="mrp">
                    <table class="table table-bordered">
                        <tr style="background-color:#CCC">
                            <td>SNO</td>
                            <td>vhcl_reg_no</td>
                            <td>vhcl_type</td>
                            <td>vehicle_arr_tm</td>
                            <td>driver_nm</td>
                            <td>doc_by_driver</td>
                            <td>vehicle_arr_plc</td>
                            <td>created_by</td>
                            <td>created_date</td>
                        </tr>
                      <?php
                      $sql = "select * from vehicle_entry where status = 'Pending For Sample Test Approval'";
                      $qry = $this->db->query($sql);
                      
                      $cnt=0;
                      foreach($qry->result() as $row){
                          $cnt++;
                      ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><a href="<?php echo base_url(); ?>index.php/floorc/SampleTestApp?vhcl_reg_no=<?php echo $row->vhcl_reg_no; ?>" target="_blank">
                            <?php echo $row->vhcl_reg_no; ?></a></td>
                            <td><?php echo $row->vhcl_type; ?></td>
                            <td><?php echo $row->vehicle_arr_tm; ?></td>
                            <td><?php echo $row->driver_nm; ?></td>
                            <td><?php echo $row->doc_by_driver; ?></td>
                            <td><?php echo $row->vehicle_arr_plc; ?></td>
                            <td><?php echo $row->created_by; ?></td>
                            <td><?php echo $row->created_date; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            
            <button class="accordion" style="color:black; font-weight:bold;">
            	5. Pending For Unloading
            </button>
            <div class="panel">
                <div id="mrp">
                    <table class="table table-bordered">
                        <tr style="background-color:#CCC">
                            <td>SNO</td>
                            <td>vhcl_reg_no</td>
                            <td>vhcl_type</td>
                            <td>vehicle_arr_tm</td>
                            <td>driver_nm</td>
                            <td>doc_by_driver</td>
                            <td>vehicle_arr_plc</td>
                            <td>created_by</td>
                            <td>created_date</td>
                        </tr>
                      <?php
                      $sql = "select * from vehicle_entry where status = 'Pending For Unloading'";
                      $qry = $this->db->query($sql);
                      
                      $cnt=0;
                      foreach($qry->result() as $row){
                          $cnt++;
                      ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><a href="<?php echo base_url(); ?>index.php/floorc/Unloading?vhcl_reg_no=<?php echo $row->vhcl_reg_no; ?>" target="_blank">
                            <?php echo $row->vhcl_reg_no; ?></a></td>
                            <td><?php echo $row->vhcl_type; ?></td>
                            <td><?php echo $row->vehicle_arr_tm; ?></td>
                            <td><?php echo $row->driver_nm; ?></td>
                            <td><?php echo $row->doc_by_driver; ?></td>
                            <td><?php echo $row->vehicle_arr_plc; ?></td>
                            <td><?php echo $row->created_by; ?></td>
                            <td><?php echo $row->created_date; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
            
            <button class="accordion" style="color:black; font-weight:bold;">
            	6. Pending For Gate Exit
            </button>
            <div class="panel">
                <div id="mrp">
                    <table class="table table-bordered">
                        <tr style="background-color:#CCC">
                            <td>SNO</td>
                            <td>vhcl_reg_no</td>
                            <td>vhcl_type</td>
                            <td>vehicle_arr_tm</td>
                            <td>driver_nm</td>
                            <td>doc_by_driver</td>
                            <td>vehicle_arr_plc</td>
                            <td>created_by</td>
                            <td>created_date</td>
                        </tr>
                      <?php
                      $sql = "select * from vehicle_entry where status = 'Pending For Gate Exit'";
                      $qry = $this->db->query($sql);
                      
                      $cnt=0;
                      foreach($qry->result() as $row){
                          $cnt++;
                      ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><a href="<?php echo base_url(); ?>index.php/floorc/GateExit?vhcl_reg_no=<?php echo $row->vhcl_reg_no; ?>" target="_blank">
                            <?php echo $row->vhcl_reg_no; ?></a></td>
                            <td><?php echo $row->vhcl_type; ?></td>
                            <td><?php echo $row->vehicle_arr_tm; ?></td>
                            <td><?php echo $row->driver_nm; ?></td>
                            <td><?php echo $row->doc_by_driver; ?></td>
                            <td><?php echo $row->vehicle_arr_plc; ?></td>
                            <td><?php echo $row->created_by; ?></td>
                            <td><?php echo $row->created_date; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>   
             </div>
             <button class="accordion" style="color:black; font-weight:bold;">
            	7. Pending For Gate Exit Completed
            </button>
            <div class="panel">
                <div id="mrp">
                    <table class="table table-bordered">
                        <tr style="background-color:#CCC">
                            <td>SNO</td>
                            <td>vhcl_reg_no</td>
                            <td>vhcl_type</td>
                            <td>vehicle_arr_tm</td>
                            <td>driver_nm</td>
                            <td>doc_by_driver</td>
                            <td>vehicle_arr_plc</td>
                            <td>created_by</td>
                            <td>created_date</td>
                        </tr>
                      <?php
                      $sql = "select * from vehicle_entry where status = 'Vehicle Entry Cycle Complete'";
                      $qry = $this->db->query($sql);
                      
                      $cnt=0;
                      foreach($qry->result() as $row){
                          $cnt++;
                      ?>
                        <tr>
                            <td><?php echo $cnt; ?></td>
                            <td><a href="<?php echo base_url(); ?>index.php/floorc/CompletedGateExit?vhcl_reg_no=<?php echo $row->vhcl_reg_no; ?>" target="_blank">
                            <?php echo $row->vhcl_reg_no; ?></a></td>
                            <td><?php echo $row->vhcl_type; ?></td>
                            <td><?php echo $row->vehicle_arr_tm; ?></td>
                            <td><?php echo $row->driver_nm; ?></td>
                            <td><?php echo $row->doc_by_driver; ?></td>
                            <td><?php echo $row->vehicle_arr_plc; ?></td>
                            <td><?php echo $row->created_by; ?></td>
                            <td><?php echo $row->created_date; ?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>   
             </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
  </section>
</section>
<?php include('footer.php'); ?>
<script type="text/javascript">
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
	  }
	}
</script>