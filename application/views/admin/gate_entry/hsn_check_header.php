<?php 
$po_num = $this->uri->segment(3);
$entry_no = $this->uri->segment(4); 
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Pending For HSN / Quantity Check Before GRCPT</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-4">
			<h4><u>PO NUMBER - <?= $po_num; ?></u></h4>
        </div>
        
        <div class="col-lg-6">
        </div>
        
        <div class="col-lg-2">
			<?php if($po_first_three == 'FPO'){ ?>
            
                   <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst_foreign/<?php echo $po_num; ?>" 
                   target="_blank" style="font-size:16px; font-weight:bold;">
                   <button type="button" class="form-control">PRINT PO</button></a>
            <?php } else { ?>
                    <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_num; ?>" 
                   target="_blank" style="font-size:16px; font-weight:bold;">
                   <button type="button" class="form-control">PRINT PO</button></a>
            <?php } ?>
         </div>
    </div><br /> 
    
    <?php
	
		$sql_po_stat="select status, pomas_podocstatus from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.po_num = b.pomas_pono and 
		a.po_num = '$po_num' and
		b.pomas_poamendmentno = (
			select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num
		)";
		
		$query_po_stat = $this->db->query($sql_po_stat);

		foreach ($query_po_stat->result() as $row){
			$po_live_staus = $row->status;
			$po_erp_staus = $row->pomas_podocstatus;
		}
			
		if($po_erp_staus == 'MD'){
			$erp_stat_desc = "MADE IN DRAFT";
		} else if($po_erp_staus == 'SC'){
			$erp_stat_desc = "SHORT CLOSED";
		} else if($po_erp_staus == 'FR'){
			$erp_stat_desc = "FRESH";
		} else if($po_erp_staus == 'DF'){
			$erp_stat_desc = "DRAFT";
		} else if($po_erp_staus == 'DE'){
			$erp_stat_desc = "DELETED";
		} else if($po_erp_staus == 'AM'){
			$erp_stat_desc = "UNDER AMENDMENT";
		} else if($po_erp_staus == 'CL'){
			$erp_stat_desc = "CLOSED";
		} else if($po_erp_staus == 'OP'){
			$erp_stat_desc = "OPEN";
		} else if($po_erp_staus == 'NT'){
			$erp_stat_desc = "NOT IN TOLERANCE CLOSED";
		} else if($po_erp_staus == 'RT'){
			$erp_stat_desc = "IN TOLERANCE CLOSED";
		}
	
	?>
	
	<div class="row">
		<div class="col-lg-6" style="text-transform:uppercase">
			   <h4>LIVE STATUS - <?php echo $po_live_staus; ?></h4>
		 </div>
         
         <div class="col-lg-3"></div>
         
         <div class="col-lg-3" style="text-align:right">
			   <h4>ERP STATUS - <?php echo $erp_stat_desc; ?></h4>
		 </div>
	</div><br />
    <!--Form Starts --->
	<form action="<?php echo base_url(); ?>index.php/hsn_checkc/grcpt_det_update" method="post" enctype="multipart/form-data">