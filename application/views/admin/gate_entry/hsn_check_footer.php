	<!--Footer Section ---->
    
    <?php 
		$po_num = $this->uri->segment(3); 
		$entry_no = $this->uri->segment(4); 
		//die;
	?>
    
    <?php
	$sql_gate_entry = "select distinct entry_no  from tipldb..gateentry_item_rec_table 
	where po_num = '$po_num' and entry_no = '$entry_no'";
	
	$qry_gate_entry = $this->db->query($sql_gate_entry);
	
	if($qry_gate_entry->num_rows() > 0){
		foreach($qry_gate_entry->result() as $row){
			$entry_no = $row->entry_no;
		}
	} else {
		$entry_no = "";
	}
	?>
    	
    <div class="row">
    	<div class="col-lg-2">
        </div>
        <div class="col-lg-2" style="font-size:17px; font-weight:bold; text-align:right">
        	<b>Remarks:</b><b style="color:#F00">*</b>
        </div> 
        <div class="col-lg-2">
        	<input type="text" name="remarks" value="" class="form-control" />
        </div>
        <?php
			$sql_po_stat="select pomas_podocstatus from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
			where a.po_num = b.pomas_pono and 
			a.po_num = '$po_num' and
			b.pomas_poamendmentno = (
				select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num
			)";
			
			$query_po_stat = $this->db->query($sql_po_stat);
			
			foreach ($query_po_stat->result() as $row){
				$po_erp_staus = $row->pomas_podocstatus;
			}
			
			if($po_erp_staus != 'OP'){
				echo "<b style='font-size:16px; color:red;'>Please Bring PO in Open State Before GRCPT.</b>";
			} else {
		?>
        <?php
			$sql_gr_chk = "select count(*) as count_gr from tipldb..gateentry_item_rec_master where entry_no = '$entry_no' and status in('GRCPT_Created')";
			$qry_gr_chk = $this->db->query($sql_gr_chk);
			foreach($qry_gr_chk->result() as $row){
				$count_gr = $row->count_gr;
			}
			
			if($count_gr <=0){
			
		?>
        <div class="col-lg-2">
        	<input type="submit" name="proceed_to_grcpt" value="Revert To Store" class="form-control" />
        </div>
		<?php } ?>
        <div class="col-lg-2">	
        	<input type="submit" name="proceed_to_grcpt" value="Proceed To GRCPT" class="form-control" />
        </div> 
        <?php } ?>
        <div class="col-lg-2">
        </div>   
    </div>
    </form>
    <!--Form Ends -->
    <br /><br />
    <div class="row">
    	<div class="col-lg-2">
        	<h4>Chat History</h4>
        </div>
    </div>
    <div class="row">
    	<div class="col-lg-6">
        	<table class="table table-bordered">
            	<tr style="background-color:#CCC">
                	<td><b>SNO</b></td>
                    <td><b>NAME</b></td>
                    <td><b>LEVEL</b></td>
                    <td><b>STATUS</b></td>
                    <td><b>INSTRUCTION</b></td>
                    <td><b>DATE TIME</b></td>
                </tr>
                <?php
					$sql_chat = "select * from tipldb..gate_entry_comment where entry_no = '$entry_no' order by comment_date";
					$qry_chat = $this->db->query($sql_chat);
					
					$sno = 0;
					foreach($qry_chat->result() as $row){
						$sno++;
						$comment_name = $row->comment_name;
						$level = $row->level;
						$status = $row->status;
						$instruction = $row->instruction;
						$comment_date = $row->comment_date;
				?>
                <tr>
                	<td><?php echo $sno; ?></td>
                    <td><?php echo $comment_name; ?></td>
                    <td><?php echo $level; ?></td>
                    <td><?php echo $status; ?></td>
                    <td><?php echo $instruction; ?></td>
                    <td><?php echo $comment_date; ?></td>
                </tr>
                <?php
					}
				?>
            </table>
        </div>
    	<div class="col-lg-6">
        </div>
    </div>
    
  </section>
</section>