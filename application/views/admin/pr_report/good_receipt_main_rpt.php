<?php $this->load->helper('pr_cycle_report'); ?>
<!--- PO Types ------>
<?php 
$category = $_REQUEST['category']; 
$po_type = $_REQUEST['po_type'];
$base_month = $_REQUEST['base_month'];
?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_month'), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1,'$base_month'), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_month')-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, '$base_month')-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_month')-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, '$base_month')-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_month')-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, '$base_month')-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>
<!--- PR Creation To PR Authorize Cycle Time ------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <table class="table table-bordered" style="text-align:center; border:solid 1px #000000;">
            <tr style="background-color:#CCC">
                <td colspan="3"><b>Last 3 Months Average<br>(In Hours)</b></td>
                <td rowspan="2"><b>Base Month Average<br>(In Hours)</b></td>
                <td rowspan="2"><b>Category</b></td>
                <td rowspan="2"><b>Stages</b></td>
                <td rowspan="2"><b>Department</b></td>
                <td colspan="4"><b>Open Cases Average<br>(In Hours)</b></td>
            </tr>
            <tr style="background-color:#CCC">
                <td><b><?php month_year($ltmsd); ?></b></td>
                <td><b><?php month_year($lsmsd); ?></b></td>
                <td><b><?php month_year($lfmsd); ?></b></td>
                <td><b>Age <= 4 <br>(Counts)</b></td>
                <td><b>Age > 4 <br>(Counts)</b></td>
                <td><b>Total Cases <br>(Counts)</b></td>
                <td><b>Open Cases Average</td>
           </tr>
           <?php
		   		$sql_stages = "select live_status,stages_cnt,first_col,second_col,pending_on_dept 
				from tipldb..gateentry_status_master order by priority";
				$qry_stages = $this->db->query($sql_stages);
				
				$count=0;
				$ltmam_tot = 0;
				$lsmam_tot = 0;
				$lfmam_tot = 0;
				$cmam_tot = 0;
				$open_less_tot = 0;
				$open_greater_tot = 0;
				$open_tot = 0;
				$open_avg_tot = 0;
				$greater = "greater";
				$less = "less";
				$all = "all";
				foreach($qry_stages->result() as $row){
					$count++;
					$stages = $row->live_status; 
					$stages_cnt = $row->stages_cnt;
					$first_col = $row->first_col;
					$second_col = $row->second_col;
					$pending_on_dept = $row->pending_on_dept;
					
					$base_comp_url = base_url()."index.php/pr_cycletimec/good_receipt_main_rpt_view";
		   ?>
           <tr>
           		<!--- Last Three Months --->	
           		<td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $ltmsd; ?>&fed=<?= $ltmed; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>">
					<?php echo $ltmam = gr_age_monthwise($category,$po_type,$ltmsd,$ltmed,$first_col,$second_col); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $lsmsd; ?>&fed=<?= $lsmed; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>">
					<?php echo $lsmam = gr_age_monthwise($category,$po_type,$lsmsd,$lsmed,$first_col,$second_col); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $lfmsd; ?>&fed=<?= $lfmed; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>">
					<?php echo $lfmam = gr_age_monthwise($category,$po_type,$lfmsd,$lfmed,$first_col,$second_col); ?>
                </a>
                </td>
                <!--- Base Month --->
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $cmsd; ?>&fed=<?= $cmed; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>">
					<?php echo $cmam = gr_age_monthwise($category,$po_type,$cmsd,$cmed,$first_col,$second_col); ?>
                </a>
                </td>
                <?php if($count == 1){ ?>
                	<td rowspan="<?php echo $stages_cnt; ?>" style="vertical-align:middle;"><b><?php echo $category; ?></b></td>
                <?php } ?>
                <td><b><?php echo $stages; ?></b></td>
                <td><b><?php echo $pending_on_dept; ?></b></td>
                <!--- Current Month Open Cases <= 1 --->
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>&type=<?= $less; ?>">
					<?php echo $open_less = gr_open_age_less($category,$po_type,$first_col,$second_col) ?>
                </a>
                </td>
                <!--- Current Month Open Cases > 1 --->
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>&type=<?= $greater; ?>">
					<?php echo $open_greater = gr_open_age_greater($category,$po_type,$first_col,$second_col) ?>
                </a>
                </td>
                <!--- Current Month Open Cases All --->
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>&type=<?= $all; ?>">
					<?php echo $open = gr_open_age_total($category,$po_type,$first_col,$second_col) ?>
                </a>
                </td>
                <!--- Current Month Open Cases Average --->
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&first_col=<?= $first_col; ?>&second_col=<?= $second_col; ?>&type=<?= $all; ?>">
					<?php echo $open_avg = gr_open_age_total_avg($category,$po_type,$first_col,$second_col) ?>
                </a>
                </td>
           </tr>
           <?php 
					$ltmam_tot = $ltmam_tot+$ltmam;
					$lsmam_tot = $lsmam_tot+$lsmam;
					$lfmam_tot = $lfmam_tot+$lfmam;
					$cmam_tot = $cmam_tot+$cmam;
					$open_less_tot = $open_less_tot+$open_less;
					$open_greater_tot = $open_greater_tot+$open_greater;
					$open_tot = $open_tot+$open;
					$open_avg_tot = $open_avg_tot+$open_avg;

		   		} 
		   ?>
           <tr>
           		<td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $ltmsd; ?>&fed=<?= $ltmed; ?>&fun=<?= "gr_age_monthwise_tot_det"; ?>">
					<?php echo $ltmam_tot; ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $lsmsd; ?>&fed=<?= $lsmed; ?>&fun=<?= "gr_age_monthwise_tot_det"; ?>">
					<?php echo $lsmam_tot; ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $lfmsd; ?>&fed=<?= $lfmed; ?>&fun=<?= "gr_age_monthwise_tot_det"; ?>">
					<?php echo $lfmam_tot; ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fsd=<?= $cmsd; ?>&fed=<?= $cmed; ?>&fun=<?= "gr_age_monthwise_tot_det"; ?>">
					<?php echo $cmam_tot; ?>
                </a>
                </td>
                <td colspan="3" style="background-color:#CCC"><b>TOTAL</b></td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fun=<?= "gr_open_cases_less_det"; ?>">
					<?php echo $open_less_tot; ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fun=<?= "gr_open_cases_greater_det"; ?>">
					<?php echo $open_greater_tot; ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fun=<?= "gr_open_cases_all_det"; ?>">
					<?php echo $open_tot; ?>
                </a>
                </td>
                <td>
                <a href="<?php echo $base_comp_url; ?>?category=<?= $category; ?>&po_type=<?= $po_type; ?>&fun=<?= "gr_open_cases_all_det"; ?>">
					<?php echo $open_avg_tot; ?>
                </a>
                </td>
           </tr>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>