<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

	$sql="select 
	DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-4, 0) as lfomsd, DATEADD(MONTH, DATEDIFF(MONTH, -4, GETDATE())-4, -1) as lfomed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	$lfomsd = substr($row->lfomsd,0,11);
	$lfomed = substr($row->lfomed,0,11);	
}

?>

<style>
.tab_heading{
	text-align:center;
}
</style>
<br />
<div class="row">
	<div class="col-lg-4"></div>
    <div class="col-lg-1"><b style="font-size:14px;">PR TYPE:</b></div>
	<div class="col-lg-3">
    	<select name="pr_type" id="pr_type" class="form-control" onchange="filter_pr_type(this.value)">
        	<option value="" disabled selected>--Select--</option>
            <option value="ALL">ALL</option>
            <option value="CGP">CGPR</option>
            <option value="FPR">FPR</option>
            <option value="IPR">IPR</option>
        </select>
    </div>
    <div class="col-lg-4"></div>
</div>
<br />
<div id="month_select"></div>
