<?php $this->load->helper('pr_cycle_report'); ?>

<?php
	$pr_type = $_REQUEST['pr_type'];
	$base_date = $_REQUEST['base_date'];
?>

<?php 
//Dates 

	$sql="select 
	DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date'), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, '$base_date'), -1) as cmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, '$base_date')-1, -1) as lfmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, '$base_date')-2, -1) as lsmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, '$base_date')-3, -1) as ltmed,
	DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-4, 0) as lfomsd, DATEADD(MONTH, DATEDIFF(MONTH, -4, '$base_date')-4, -1) as lfomed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	$lfomsd = substr($row->lfomsd,0,11);
	$lfomed = substr($row->lfomed,0,11);	
}

?>

<style>
.tab_heading{
	text-align:center;
}
</style>
<BR />
<H3 style="text-align:center">PR NEED DATE VS ACTUAL FR DATE REPORT</H3>
<table class="table table-bordered">
    <tr style="background-color:#CCC">
        <td rowspan="2"><b>TYPE</b></td>
        <td colspan="4" class="tab_heading"><b><?php month_year($lfomsd); ?></b></td>
        <td colspan="4" class="tab_heading"><b><?php month_year($ltmsd); ?></b></td>
        <td colspan="4" class="tab_heading"><b><?php month_year($lsmsd); ?></b></td>
        <td colspan="4" class="tab_heading"><b><?php month_year($lfmsd); ?></b></td>
        <td colspan="4" class="tab_heading"><b>Base Month(<?php month_year($cmsd); ?>)</b></td>
        <td rowspan="2"><b>CATEGORY</b></td>
        <td colspan="3" class="tab_heading"><b>Open Cases</b></td>
    </tr>
    <tr style="background-color:#CCC">
        <td><b>BT %</b></td>
        <td><b>D %</b></td>
        <td><b>OT %</b></td>
        <td><b>T</b></td>
        
        <td><b>BT %</b></td>
        <td><b>D %</b></td>
        <td><b>OT %</b></td>
        <td><b>T</b></td>
        
        <td><b>BT %</b></td>
        <td><b>D %</b></td>
        <td><b>OT %</b></td>
        <td><b>T</b></td>
        
        <td><b>BT %</b></td>
        <td><b>D %</b></td>
        <td><b>OT %</b></td>
        <td><b>T</b></td>
        
        <td><b>BT %</b></td>
        <td><b>D %</b></td>
        <td><b>OT %</b></td>
        <td><b>T</b></td>
        
        <td><b>D</b></td>
        <td><b>OT</b></td>
        <td><b>T</b></td>
    </tr>
    
    <?php if($pr_type == 'ALL'){ ?>
    
		<?php
            $sql_cat = "select distinct category from tipldb..pr_submit_table 
            where category not in('OTHERS','RAW MATERIALS') and substring(pr_num,1,3) not in('LPR') and category != ''  
            order by category asc";
            
            $qry_cat = $this->db->query($sql_cat);
            
            foreach($qry_cat->result() as $row){
                $category = $row->category;
        ?>
        <?php if($category == 'CAPITAL GOODS'){ ?>
        <tr>
            <td><b>CGPR</b></td>
            <!-----Last Fouth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php 
				$per = ( count_cases('BT', 'CGP', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','CGP',$category,$lfomsd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('D', 'CGP', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','CGP',$category,$lfomsd,$lfomed);
				echo number_format($per,2,".","");
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP',$category,$lfomsd,$lfomed); ?>
            <?php 
			$per = ( count_cases('OT', 'CGP', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','CGP',$category,$lfomsd,$lfomed);
			echo number_format($per,2,".","");
			?>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP',$category,$lfomsd,$lfomed); ?>
            </td>
            <!-----Last Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('BT', 'CGP', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','CGP',$category,$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('D', 'CGP', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','CGP',$category,$ltmsd,$ltmed);
				echo number_format($per,2,".","");
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP',$category,$ltmsd,$ltmed); ?>
            <?php 
			$per = ( count_cases('OT', 'CGP', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','CGP',$category,$ltmsd,$ltmed);
			echo number_format($per,2,".","");
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP',$category,$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Last second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('BT', 'CGP', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','CGP',$category,$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('D', 'CGP', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','CGP',$category,$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('OT', 'CGP', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','CGP',$category,$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP',$category,$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----Last First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('BT', 'CGP', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','CGP',$category,$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('D', 'CGP', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','CGP',$category,$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('OT', 'CGP', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','CGP',$category,$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP',$category,$lfmsd,$lfmed); ?>
            </a>
            </td>
            <!-----Base Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('BT', 'CGP', $category, $cmsd, $cmed) * 100)/count_cases('TOT','CGP',$category,$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('D', 'CGP', $category, $cmsd, $cmed) * 100)/count_cases('TOT','CGP',$category,$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('OT', 'CGP', $category, $cmsd, $cmed) * 100)/count_cases('TOT','CGP',$category,$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP',$category,$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td><b><?php echo $category; ?></b></td>
            
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=D&&type=CGP&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('D','CGP',$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=OT&&type=CGP&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('OT','CGP',$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=TOT&&type=CGP&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('TOT','CGP',$category); ?>
            </a>
            </td>
        </tr>
        <?php } else { ?>
        <tr>
            <td><b>FPR</b></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('BT','FPR',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('BT', 'FPR', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','FPR', $category, $lfomsd, $lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('D','FPR',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('D', 'FPR', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','FPR', $category, $lfomsd, $lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('OT','FPR',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('OT', 'FPR', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','FPR', $category, $lfomsd, $lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('TOT','FPR',$category,$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('BT','FPR',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('BT', 'FPR', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','FPR', $category, $ltmsd, $ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('D','FPR',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('D', 'FPR', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','FPR', $category, $ltmsd, $ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('OT','FPR',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('OT', 'FPR', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','FPR', $category, $ltmsd, $ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('TOT','FPR',$category,$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('BT','FPR',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('BT', 'FPR', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','FPR', $category, $lsmsd, $lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('D','FPR',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('D', 'FPR', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','FPR', $category, $lsmsd, $lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('OT','FPR',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('OT', 'FPR', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','FPR', $category, $lsmsd, $lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('TOT','FPR',$category,$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('BT','FPR',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('BT', 'FPR', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','FPR', $category, $lfmsd, $lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('D','FPR',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('D', 'FPR', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','FPR', $category, $lfmsd, $lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('OT','FPR',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('OT', 'FPR', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','FPR', $category, $lfmsd, $lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('TOT','FPR',$category,$lfmsd,$lfmed); ?>
            </a>
            </td>
            
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('BT','FPR',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('BT', 'FPR', $category, $cmsd, $cmed) * 100)/count_cases('TOT','FPR', $category, $cmsd, $cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('D','FPR',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('D', 'FPR', $category, $cmsd, $cmed) * 100)/count_cases('TOT','FPR', $category, $cmsd, $cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('OT','FPR',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('OT', 'FPR', $category, $cmsd, $cmed) * 100)/count_cases('TOT','FPR', $category, $cmsd, $cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=FPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('TOT','FPR',$category,$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td rowspan="2"><b><?php echo $category; ?></b></td>
            
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=D&&type=FPR&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('D','FPR',$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=OT&&type=FPR&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('OT','FPR',$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=TOT&&type=FPR&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('TOT','FPR',$category); ?>
            </a>
            </td>
        </tr>
        <tr>
            <td><b>IPR</b></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('BT','IPR',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('BT', 'IPR', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','IPR', $category, $lfomsd, $lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('D','IPR',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('D', 'IPR', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','IPR', $category, $lfomsd, $lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('OT','IPR',$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('OT', 'IPR', $category, $lfomsd, $lfomed) * 100)/count_cases('TOT','IPR', $category, $lfomsd, $lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('TOT','IPR',$category,$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('BT','IPR',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('BT', 'IPR', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','IPR', $category, $ltmsd, $ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('D','IPR',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('D', 'IPR', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','IPR', $category, $ltmsd, $ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('OT','IPR',$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('OT', 'IPR', $category, $ltmsd, $ltmed) * 100)/count_cases('TOT','IPR', $category, $ltmsd, $ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('TOT','IPR',$category,$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('BT','IPR',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('BT', 'IPR', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','IPR', $category, $lsmsd, $lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('D','IPR',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('D', 'IPR', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','IPR', $category, $lsmsd, $lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('OT','IPR',$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('OT', 'IPR', $category, $lsmsd, $lsmed) * 100)/count_cases('TOT','IPR', $category, $lsmsd, $lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('TOT','IPR',$category,$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('BT','IPR',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('BT', 'IPR', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','IPR', $category, $lfmsd, $lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('D','IPR',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('D', 'IPR', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','IPR', $category, $lfmsd, $lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('OT','IPR',$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('OT', 'IPR', $category, $lfmsd, $lfmed) * 100)/count_cases('TOT','IPR', $category, $lfmsd, $lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('TOT','IPR',$category,$lfmsd,$lfmed); ?>
            </a>
            </td>
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('BT','IPR',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('BT', 'IPR', $category, $cmsd, $cmed) * 100)/count_cases('TOT','IPR', $category, $cmsd, $cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('D','IPR',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('D', 'IPR', $category, $cmsd, $cmed) * 100)/count_cases('TOT','IPR', $category, $cmsd, $cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('OT','IPR',$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('OT', 'IPR', $category, $cmsd, $cmed) * 100)/count_cases('TOT','IPR', $category, $cmsd, $cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=IPR&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('TOT','IPR',$category,$cmsd,$cmed); ?>
            </a>
            </td>
            
            <!--OPEN CASES --->
           	<td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=D&&type=IPR&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('D','IPR',$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=OT&&type=IPR&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('OT','IPR',$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=TOT&&type=IPR&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('TOT','IPR',$category); ?>
            </a>
            </td>
        </tr>
        <?php }} ?>
        <!--- Total ----->
        <tr style="background-color:#CCC">
            <td><b>ALL</b></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=ALL&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('BT','ALL','GT',$lfomsd,$lfomed); ?>
            <?php 
				/*$per = ( count_cases('BT', 'ALL', 'GT', $lfomsd, $lfomed) * 100)/count_cases('TOT','ALL', 'GT', $lfomsd, $lfomed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=ALL&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('D','ALL','GT',$lfomsd,$lfomed); ?>
            <?php 
				/*$per = ( count_cases('D', 'ALL', 'GT', $lfomsd, $lfomed) * 100)/count_cases('TOT','ALL', 'GT', $lfomsd, $lfomed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=ALL&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('OT','ALL','GT',$lfomsd,$lfomed); ?>
            <?php 
				/*$per = ( count_cases('OT', 'ALL', 'GT', $lfomsd, $lfomed) * 100)/count_cases('TOT','ALL', 'GT', $lfomsd, $lfomed);
				echo number_format($per,2,".","");*/ 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=ALL&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT','ALL','GT',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=ALL&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT','ALL','GT',$ltmsd,$ltmed); ?>
            <?php 
				/*$per = ( count_cases('BT', 'ALL', 'GT', $ltmsd, $ltmed) * 100)/count_cases('TOT','ALL', 'GT', $ltmsd, $ltmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=ALL&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('D','ALL','GT',$ltmsd,$ltmed); ?>
            <?php 
				/*$per = ( count_cases('D', 'ALL', 'GT', $ltmsd, $ltmed) * 100)/count_cases('TOT','ALL', 'GT', $ltmsd, $ltmed);
				echo number_format($per,2,".","");*/ 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=ALL&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT','ALL','GT',$ltmsd,$ltmed); ?>
            <?php 
				/*$per = ( count_cases('OT', 'ALL', 'GT', $ltmsd, $ltmed) * 100)/count_cases('TOT','ALL', 'GT', $ltmsd, $ltmed);
				echo number_format($per,2,".","");*/ 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=ALL&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT','ALL','GT',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=ALL&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT','ALL','GT',$lsmsd,$lsmed); ?>
            <?php 
				/*$per = ( count_cases('BT', 'ALL', 'GT', $lsmsd, $lsmed) * 100)/count_cases('TOT','ALL', 'GT', $lsmsd, $lsmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=ALL&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('D','ALL','GT',$lsmsd,$lsmed); ?>
            <?php 
				/*$per = ( count_cases('D', 'ALL', 'GT', $lsmsd, $lsmed) * 100)/count_cases('TOT','ALL', 'GT', $lsmsd, $lsmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=ALL&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT','ALL','GT',$lsmsd,$lsmed); ?>
            <?php 
				/*$per = ( count_cases('OT', 'ALL', 'GT', $lsmsd, $lsmed) * 100)/count_cases('TOT','ALL', 'GT', $lsmsd, $lsmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=ALL&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT','ALL','GT',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=ALL&&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT','ALL','GT',$lfmsd,$lfmed); ?>
            <?php 
				/*$per = ( count_cases('BT', 'ALL', 'GT', $lfmsd, $lfmed) * 100)/count_cases('TOT','ALL', 'GT', $lfmsd, $lfmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=ALL&&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('D','ALL','GT',$lfmsd,$lfmed); ?>
            <?php 
				/*$per = ( count_cases('D', 'ALL', 'GT', $lfmsd, $lfmed) * 100)/count_cases('TOT','ALL', 'GT', $lfmsd, $lfmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=ALL&&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT','ALL','GT',$lfmsd,$lfmed); ?>
            <?php 
				/*$per = ( count_cases('OT', 'ALL', 'GT', $lfmsd, $lfmed) * 100)/count_cases('TOT','ALL', 'GT', $lfmsd, $lfmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=ALL&&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT','ALL','GT',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=ALL&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT','ALL','GT',$cmsd,$cmed); ?>
            <?php 
				/*$per = ( count_cases('BT', 'ALL', 'GT', $cmsd, $cmed) * 100)/count_cases('TOT','ALL', 'GT', $cmsd, $cmed);
				echo number_format($per,2,".",""); */
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=ALL&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('D','ALL','GT',$cmsd,$cmed); ?>
            <?php 
				/*$per = ( count_cases('D', 'ALL', 'GT', $cmsd, $cmed) * 100)/count_cases('TOT','ALL', 'GT', $cmsd, $cmed);
				echo number_format($per,2,".","");*/ 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=ALL&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT','ALL','GT',$cmsd,$cmed); ?>
            <?php 
				/*$per = ( count_cases('OT', 'ALL', 'GT', $cmsd, $cmed) * 100)/count_cases('TOT','ALL', 'GT', $cmsd, $cmed);
				echo number_format($per,2,".","");*/ 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=ALL&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT','ALL','GT',$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td><b>GRAND TOTAL</b></td>
            
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det_open&&case_type=D&type=ALL&&category=GT" target="_blank">
			<?php echo count_cases_tot_open('D','ALL','GT'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det_open&&case_type=OT&&type=ALL&&category=GT" target="_blank">
			<?php echo count_cases_tot_open('OT','ALL','GT'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det_open&&case_type=TOT&&type=ALL&&category=GT" target="_blank">
			<?php echo count_cases_tot_open('TOT','ALL','GT'); ?>
            </a>
            </td>
        </tr>
    
    <?php } else { ?>
    
    	<?php if($pr_type == 'CGP'){ ?>
    
        <tr>
            <td><b><?php echo $pr_type; ?></b></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('BT','CGP','CAPITAL GOODS',$lfomsd,$lfomed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lfomsd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('D','CGP','CAPITAL GOODS',$lfomsd,$lfomed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lfomsd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('OT','CGP','CAPITAL GOODS',$lfomsd, $lfomed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lfomSd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('BT','CGP','CAPITAL GOODS',$ltmsd, $ltmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('D','CGP','CAPITAL GOODS',$ltmsd, $ltmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('OT','CGP','CAPITAL GOODS',$ltmsd, $ltmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('BT','CGP','CAPITAL GOODS',$lsmsd, $lsmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('D','CGP','CAPITAL GOODS',$lsmsd, $lsmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('OT','CGP','CAPITAL GOODS',$lsmsd, $lsmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('BT','CGP','CAPITAL GOODS',$lfmsd, $lfmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('D','CGP','CAPITAL GOODS',$lfmsd, $lfmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('OT','CGP','CAPITAL GOODS',$lfmsd, $lfmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            </a>
            </td>
            
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('BT','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('BT','CGP','CAPITAL GOODS',$cmsd,$cmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('D','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('D','CGP','CAPITAL GOODS',$cmsd,$cmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('OT','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('OT','CGP','CAPITAL GOODS',$cmsd,$cmed) * 100)/count_cases('TOT','CGP','CAPITAL GOODS',$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td><b><?php echo "CAPITAL GOODS"; ?></b></td>
            
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=D&&type=CGP&&category=CAPITAL GOODS" target="_blank">
			<?php echo count_cases_open('D','CGP','CAPITAL GOODS'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=OT&&type=CGP&&category=CAPITAL GOODS" target="_blank">
			<?php echo count_cases_open('OT','CGP','CAPITAL GOODS'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS" target="_blank">
			<?php echo count_cases_open('TOT','CGP','CAPITAL GOODS'); ?>
            </a>
            </td>
        </tr>
        
        <!--- Total ----->
         <tr style="background-color:#CCC">
            <td><b><?php echo $pr_type; ?></b></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('BT','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('D','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('OT','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('BT','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('D','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('OT','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('BT','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('D','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('OT','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('BT','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('D','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('OT','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$lfmsd,$lfmed); ?>
            </a>
            </td>
            
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('BT','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('D','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('OT','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('TOT','CGP','CAPITAL GOODS',$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td><b>GRAND TOTAL</b></td>
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=D&&type=CGP&&category=CAPITAL GOODS" target="_blank">
			<?php echo count_cases_open('D','CGP','CAPITAL GOODS'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=OT&&type=CGP&&category=CAPITAL GOODS" target="_blank">
			<?php echo count_cases_open('OT','CGP','CAPITAL GOODS'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=TOT&&type=CGP&&category=CAPITAL GOODS" target="_blank">
			<?php echo count_cases_open('TOT','CGP','CAPITAL GOODS'); ?>
            </a>
            </td>
        </tr>
        
        <?php } else { ?>
		
		<?php
            $sql_cat = "select distinct category from tipldb..pr_submit_table 
            where category not in('OTHERS','RAW MATERIALS','CAPITAL GOODS') and substring(pr_num,1,3) not in('LPR') and category != ''  
            order by category asc";
            
            $qry_cat = $this->db->query($sql_cat);
            
            foreach($qry_cat->result() as $row){
                $category = $row->category;
        ?>
			
        <tr>
            <td><b><?php echo $pr_type; ?></b></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('BT',$pr_type,$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('BT',$pr_type,$category,$lfomsd,$lfomed) * 100)/count_cases('TOT',$pr_type,$category,$lfomsd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('D',$pr_type,$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('D',$pr_type,$category,$lfomsd,$lfomed) * 100)/count_cases('TOT',$pr_type,$category,$lfomsd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php //echo count_cases('OT',$pr_type,$category,$lfomsd,$lfomed); ?>
            <?php 
				$per = ( count_cases('OT',$pr_type,$category,$lfomsd,$lfomed) * 100)/count_cases('TOT',$pr_type,$category,$lfomsd,$lfomed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases('TOT',$pr_type,$category,$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('BT',$pr_type,$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('BT',$pr_type,$category,$ltmsd,$ltmed) * 100)/count_cases('TOT',$pr_type,$category,$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('D',$pr_type,$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('D',$pr_type,$category,$ltmsd,$ltmed) * 100)/count_cases('TOT',$pr_type,$category,$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php //echo count_cases('OT',$pr_type,$category,$ltmsd,$ltmed); ?>
            <?php 
				$per = ( count_cases('OT',$pr_type,$category,$ltmsd,$ltmed) * 100)/count_cases('TOT',$pr_type,$category,$ltmsd,$ltmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases('TOT',$pr_type,$category,$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('BT',$pr_type,$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('BT',$pr_type,$category,$lsmsd,$lsmed) * 100)/count_cases('TOT',$pr_type,$category,$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('D',$pr_type,$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('D',$pr_type,$category,$lsmsd,$lsmed) * 100)/count_cases('TOT',$pr_type,$category,$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php //echo count_cases('OT',$pr_type,$category,$lsmsd,$lsmed); ?>
            <?php 
				$per = ( count_cases('OT',$pr_type,$category,$lsmsd,$lsmed) * 100)/count_cases('TOT',$pr_type,$category,$lsmsd,$lsmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases('TOT',$pr_type,$category,$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('BT',$pr_type,$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('BT',$pr_type,$category,$lfmsd,$lfmed) * 100)/count_cases('TOT',$pr_type,$category,$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('D',$pr_type,$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('D',$pr_type,$category,$lfmsd,$lfmed) * 100)/count_cases('TOT',$pr_type,$category,$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php //echo count_cases('OT',$pr_type,$category,$lfmsd,$lfmed); ?>
            <?php 
				$per = ( count_cases('OT',$pr_type,$category,$lfmsd,$lfmed) * 100)/count_cases('TOT',$pr_type,$category,$lfmsd,$lfmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases('TOT',$pr_type,$category,$lfmsd,$lfmed); ?>
            </a>
            </td>
            
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('BT',$pr_type,$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('BT',$pr_type,$category,$cmsd,$cmed) * 100)/count_cases('TOT',$pr_type,$category,$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('D',$pr_type,$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('D',$pr_type,$category,$cmsd,$cmed) * 100)/count_cases('TOT',$pr_type,$category,$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php //echo count_cases('OT',$pr_type,$category,$cmsd,$cmed); ?>
            <?php 
				$per = ( count_cases('OT',$pr_type,$category,$cmsd,$cmed) * 100)/count_cases('TOT',$pr_type,$category,$cmsd,$cmed);
				echo number_format($per,2,".",""); 
			?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases('TOT',$pr_type,$category,$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td><b><?php echo $category; ?></b></td>
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=D&&type=<?php echo $pr_type; ?>&&category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('D',$pr_type,$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('OT',$pr_type,$category); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_det_open&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=category=<?php echo $category; ?>" target="_blank">
			<?php echo count_cases_open('TOT',$pr_type,$category); ?>
            </a>
            </td>
        </tr>
        <?php } ?>
        <!--- Total ----->
        <tr style="background-color:#CCC">
            <td><?php $pr_type; ?></td>
            <!-----Fourth Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('BT',$pr_type,'GT',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('D',$pr_type,'GT',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('OT',$pr_type,'GT',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfomsd; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT',$pr_type,'GT',$lfomsd,$lfomed); ?>
            </a>
            </td>
            <!-----Third Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT',$pr_type,'GT',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('D',$pr_type,'GT',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT',$pr_type,'GT',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $ltmsd; ?>&&to_date=<?php echo $ltmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT',$pr_type,'GT',$ltmsd,$ltmed); ?>
            </a>
            </td>
            <!-----Second Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT',$pr_type,'GT',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('D',$pr_type,'GT',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT',$pr_type,'GT',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lsmsd; ?>&&to_date=<?php echo $lsmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT',$pr_type,'GT',$lsmsd,$lsmed); ?>
            </a>
            </td>
            <!-----First Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT',$pr_type,'GT',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('D',$pr_type,'GT',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $lfmed; ?>&&to_date=<?php echo $lfomed; ?>" target="_blank">
			<?php echo count_cases_tot('OT',$pr_type,'GT',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=<?php echo $pr_type; ?>
            &&category=GT&&from_date=<?php echo $lfmsd; ?>&&to_date=<?php echo $lfmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT',$pr_type,'GT',$lfmsd,$lfmed); ?>
            </a>
            </td>
            <!-----Current Month -->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=BT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('BT',$pr_type,'GT',$cmsd,$cmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=D&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('D',$pr_type,'GT',$cmsd,$cmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('OT',$pr_type,'GT',$cmsd,$cmed); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=GT&&from_date=<?php echo $cmsd; ?>&&to_date=<?php echo $cmed; ?>" target="_blank">
			<?php echo count_cases_tot('TOT',$pr_type,'GT',$cmsd,$cmed); ?>
            </a>
            </td>
            
            <td><b>GRAND TOTAL</b></td>
            
            <!--OPEN CASES --->
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det_open&&case_type=D&&type=<?php echo $pr_type; ?>&&category=GT" target="_blank">
			<?php echo count_cases_tot_open('D',$pr_type,'GT'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det_open&&case_type=OT&&type=<?php echo $pr_type; ?>&&category=category=GT" target="_blank">
			<?php echo count_cases_tot_open('OT',$pr_type,'GT'); ?>
            </a>
            </td>
            <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det?fun=count_cases_tot_det_open&&case_type=TOT&&type=<?php echo $pr_type; ?>&&category=category=GT" target="_blank">
			<?php echo count_cases_tot_open('TOT',$pr_type,'GT'); ?>
            </a>
            </td>
        </tr>
        
        <?php } ?>
    
    <?php } ?>
    
</table>