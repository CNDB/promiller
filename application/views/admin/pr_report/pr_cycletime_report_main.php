<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PR-PO-GRCPT CYCLE TIME SUMMARY</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-3"></div>
    	<div class="col-lg-6">
        	<form method="post" onSubmit="">
                <table cellspacing="8px" cellpadding="4" cellspacing="4" align="center" border="0" width="100%">
                    <tr>
                        <td><b style="font-size:14px; font-weight:bolder">SELECT REPORT TYPE :</b></td>
                        <td> 
                            <select name="report_type" id="report_type" class="form-control">		
                                <option value="" disabled selected>--Select--</option>
                                <option value="PR">PR Creation To PR Authorize</option>
                                <option value="PO">PR Authorize To PO Release</option>
                                <option value="GRCPT">PR Release To GRCPT</option>
                                <option value="COST_SAVING">No With Cost Saving</option>
                                <option value="APPROVAL_CYCLETIME">Approval Cycletime</option>
                                <option value="PR_NEED_DATE_VS_ACTUAL">PR Need Date V/S actual</option>
                                <option value="GOOD_RECEIPT_CYCLETIME">Goods Receipt Cycletime Report</option>
                            </select>
                         </td>
                         <td>
                            <input type="button" value="Submit" onClick="filter()" class="form-control" style="margin-left:10px">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-lg-3"></div>
    </div><br>
    
    <div id="ajax_div"></div>
    
  </section>
</section>

<script>

function filter(){
	var report_type = document.getElementById("report_type").value;
	
	if(report_type == ''){
		alert("Please Select Report type");
		document.getElementById("report_type").focus;
		return false;
	}
	
	$("#ajax_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ajax_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	//var queryString="?report_type="+report_type;
	
	if(report_type == 'PR'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report", true);
	} else if(report_type == 'PO'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/po_report", true);
	} else if(report_type == 'GRCPT'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/grcpt_report", true);
	} else if(report_type == 'NO_OF_PO'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/no_of_po", true);
	} else if(report_type == 'COST_SAVING'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/cost_saving_report", true);
	} else if(report_type == 'NO_PO_COST_SAVING_MERGE'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/no_po_cost_saving_merge", true);
	} else if(report_type == 'APPROVAL_CYCLETIME'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/approval_cycletime", true);
	} else if(report_type == 'PR_NEED_DATE_VS_ACTUAL'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/need_data_vs_actual", true);
	} else if(report_type == 'GOOD_RECEIPT_CYCLETIME'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/goods_receipt_cycletime_main", true);
	}
	
	xmlhttp.send();
	
}


//Costing Report Filter
function costing_filter(a){	
	
	var values = $('#filter').val();
	
	//alert(values);
	
	$("#filter_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('filter_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?po_type="+values;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/cost_saving_det" + queryString, true); 
	
	xmlhttp.send();	
}

function filter_month(c,d){
	//alert(c);
	//alert(d);
	
	if(d == ''){
		alert("Please Select Base Month...");
		return false;
	}
	
	$("#filter_div2").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('filter_div2').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?po_type="+c+"&&base_date="+d;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/cost_saving_report_monthwise" + queryString, true); 
	
	xmlhttp.send();
}


function filter_pr_type(a){
	//alert(a);
	//alert(d);
	
	if(a == ''){
		alert("Select PR Type...");
		return false;
	}
	
	$("#month_select").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('month_select').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?pr_type="+a;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/fetch_months_ndva" + queryString, true); 
	
	xmlhttp.send();
}

function filter_ndva(a,b){
	//alert(a);
	//alert(b);
	
	$("#filter_div_ndva").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('filter_div_ndva').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?pr_type="+a+"&&base_date="+b;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/fetch_months_ndva_report_main" + queryString, true); 
	
	xmlhttp.send();
}


function get_potype(a){
	//alert(a);
	//alert(b);
	
	$("#po_type").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('po_type').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?category="+a;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/po_type" + queryString, true); 
	
	xmlhttp.send();
}

function get_base_month(a,b){
	//alert(a);
	//alert(b);
	
	$("#base_month_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('base_month_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?category="+a+"&po_type="+b;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/base_month" + queryString, true); 
	
	xmlhttp.send();
}

function get_good_receipt_main_rpt(a,b,c){
	
	$("#good_receipt_main_rpt").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('good_receipt_main_rpt').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?category="+a+"&po_type="+b+"&base_month="+c;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_cycletimec/good_receipt_main_rpt" + queryString, true); 
	
	xmlhttp.send();
}

</script>
