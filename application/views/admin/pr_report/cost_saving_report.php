<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>
<!--- No of Purchase Orders L1------>

<?php
	$sql_proc = "exec tipldb..no_with_costsaving_report '$lsmsd', '$cmed'";
	
	$qry_proc = $this->db->query($sql_proc);
	
	//echo $sql_proc; die;
?>

<form action="#" method="post">

<div class="row">
	<div class="col-lg-4"></div>
	<div class="col-lg-1"><b style="font-size:14px;">PO TYPE :</b></div>
    <div class="col-lg-3">
    	<select id="filter" name="filter" class="form-control" onchange="costing_filter(this.value)" multiple>
        	<option value="" disabled selected>--SELECT---</option>
            <option value="ALL">ALL</option>
            <option value="FPO">FPO</option>
            <option value="IPO">IPO</option>
            <option value="LPO">LPO</option>
            <option value="CGP">CAPITAL GOODS</option>
            <option value="SRV">SERVICE PO</option>
        </select>
    </div>
    <div class="col-lg-4"></div>
</div><br />
	
</form>

<div>
    <div class="row">
        <div class="col-lg-12" id="filter_div">
         <!----- AJAX RESPONSE DIV ------>  
        </div>
    </div><br />
</div>