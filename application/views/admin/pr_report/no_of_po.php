<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>
<!--- No of Purchase Orders L1------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <h4 style="text-align:center">No of Purchase Orders L1</h4>
        <table class="table table-bordered">
            <tr style="background-color:#CCC">
                <td colspan="2"><b><?php month_year($ltmsd); ?></b></td>
                <td colspan="2"><b><?php month_year($lsmsd); ?></b></td>
                <td colspan="2"><b><?php month_year($lfmsd); ?></b></td>
                <td rowspan="2"><b>L1 Approval</b></td>
                <td rowspan="2"><b>Category</b></td>
                <td rowspan="2"><b>Main Category</b></td>
                <td colspan="2"><b>Current Month</b></td>
            </tr>
            <tr style="background-color:#CCC">
            	<td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
            </tr>
            <?php
				$lvl = "LVL1";
				
				$sql_lvl1_app = "select distinct level1_approval_by from tipldb..po_master_table 
				where lvl1_app_req = 'Yes' and lvl2_app_req = 'No' and lvl3_app_req = 'No'"; 
				
				$qry_lvl1_app = $this->db->query($sql_lvl1_app);
				
				foreach($qry_lvl1_app->result() as $row){
					$level1_approval_by = $row->level1_approval_by;
					
					$sql_cat_l1 = "select distinct po_category from tipldb..po_master_table 
					where lvl1_app_req = 'Yes' and lvl2_app_req = 'No' and lvl3_app_req = 'No' and po_category != ''
					and level1_approval_by = '$level1_approval_by'";
					
					$qry_cat_l1 = $this->db->query($sql_cat_l1);
					
					$category = '';
					$i=0;
					foreach($qry_cat_l1->result() as $row){
						$i++;
						$category1 = $row->po_category;
						if($i==1){
							$category = $category.$category1;
						} else {
							$category = $category.", ".$category1;
						}
					}
			?>
            <tr>
            	<td><?php echo count_no_po($lvl,$level1_approval_by,$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level1_approval_by,$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po($lvl,$level1_approval_by,$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level1_approval_by,$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po($lvl,$level1_approval_by,$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level1_approval_by,$lfmsd,$lfmed); ?></td>
                <td><b><?php echo $level1_approval_by; ?></b></td>
                <td><b><?php echo $category; ?></b></td>
                <td></td>
                <td><?php echo count_no_po($lvl,$level1_approval_by,$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level1_approval_by,$cmsd,$cmed); ?></td>
            </tr>
            <?php } ?>
            <tr>
            	<td><?php echo count_no_po($lvl,'total',$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po($lvl,'total',$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po($lvl,'total',$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$lfmsd,$lfmed); ?></td>
                <td colspan="3" style="text-align:center; background-color:#999"><b>TOTAL</b></td>
                <td><?php echo count_no_po($lvl,'total',$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$cmsd,$cmed); ?></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>

<!--- No of Purchase Orders L2------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <h4 style="text-align:center">No of Purchase Orders L2</h4>
        <table class="table table-bordered">
            <tr style="background-color:#CCC">
                <td colspan="2"><b><?php month_year($ltmsd); ?></b></td>
                <td colspan="2"><b><?php month_year($lsmsd); ?></b></td>
                <td colspan="2"><b><?php month_year($lfmsd); ?></b></td>
                <td rowspan="2"><b>L2 Approval</b></td>
                <td rowspan="2"><b>Category</b></td>
                <td rowspan="2"><b>Main Category</b></td>
                <td colspan="2"><b>Current Month</b></td>
            </tr>
            <tr style="background-color:#CCC">
            	<td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
            </tr>
            <?php
				$lvl = "LVL2";
				
				$sql_lvl2_app = "select distinct level2_approval_by from tipldb..po_master_table 
				where lvl2_app_req = 'Yes' and lvl3_app_req = 'No' and level2_approval_by is not null and level2_approval_by != 'raviraj.mehra'"; 
				
				$qry_lvl2_app = $this->db->query($sql_lvl2_app);
				
				foreach($qry_lvl2_app->result() as $row){
					$level2_approval_by = $row->level2_approval_by;
					
					$sql_cat_l2 = "select distinct po_category from tipldb..po_master_table 
					where lvl2_app_req = 'Yes' and lvl3_app_req = 'No' and po_category != ''
					and level2_approval_by = '$level2_approval_by'";
					
					$qry_cat_l2 = $this->db->query($sql_cat_l2);
					
					$category = '';
					$i=0;
					foreach($qry_cat_l2->result() as $row){
						$i++;
						$category2 = $row->po_category;
						if($i==1){
							$category = $category.$category2;
						} else {
							$category = $category.", ".$category2;
						}
					}
			?>
            <tr>
            	<td><?php echo count_no_po($lvl,$level2_approval_by,$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level2_approval_by,$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po($lvl,$level2_approval_by,$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level2_approval_by,$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po($lvl,$level2_approval_by,$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level2_approval_by,$lfmsd,$lfmed); ?></td>
                <td><b><?php echo $level2_approval_by; ?></b></td>
                <td><b><?php echo $category; ?></b></td>
                <td></td>
                <td><?php echo count_no_po($lvl,$level2_approval_by,$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level2_approval_by,$cmsd,$cmed); ?></td>
            </tr>
            <?php } ?>
            <tr>
            	<td><?php echo count_no_po($lvl,'total',$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po($lvl,'total',$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po($lvl,'total',$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$lfmsd,$lfmed); ?></td>
                <td colspan="3" style="text-align:center; background-color:#999"><b>TOTAL</b></td>
                <td><?php echo count_no_po($lvl,'total',$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$cmsd,$cmed); ?></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>

<!--- No of Purchase Orders L3------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <h4 style="text-align:center">No of Purchase Orders L3</h4>
        <table class="table table-bordered">
            <tr style="background-color:#CCC">
                <td colspan="2"><b><?php month_year($ltmsd); ?></b></td>
                <td colspan="2"><b><?php month_year($lsmsd); ?></b></td>
                <td colspan="2"><b><?php month_year($lfmsd); ?></b></td>
                <td rowspan="2"><b>L3 Approval</b></td>
                <td rowspan="2"><b>Category</b></td>
                <td rowspan="2"><b>Main Category</b></td>
                <td colspan="2"><b>Current Month</b></td>
            </tr>
            <tr style="background-color:#CCC">
            	<td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
            </tr>
            <?php
				$lvl = "LVL3";
				
				$sql_lvl3_app = "select distinct level3_approval_by from tipldb..po_master_table 
				where lvl3_app_req = 'Yes'"; 
				
				$qry_lvl3_app = $this->db->query($sql_lvl3_app);
				
				foreach($qry_lvl3_app->result() as $row){
					$level3_approval_by = $row->level3_approval_by;
					
					$sql_cat_l3 = "select distinct po_category from tipldb..po_master_table 
					where lvl3_app_req = 'Yes' and po_category != ''
					and level3_approval_by = '$level3_approval_by'";
					
					$qry_cat_l3 = $this->db->query($sql_cat_l3);
					
					$category = '';
					$i=0;
					foreach($qry_cat_l3->result() as $row){
						$i++;
						$category3 = $row->po_category;
						if($i==1){
							$category = $category.$category3;
						} else {
							$category = $category.", ".$category3;
						}
					}
			?>
            <tr>
            	<td><?php echo count_no_po($lvl,$level3_approval_by,$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level3_approval_by,$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po($lvl,$level3_approval_by,$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level3_approval_by,$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po($lvl,$level3_approval_by,$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level3_approval_by,$lfmsd,$lfmed); ?></td>
                <td><b><?php echo $level3_approval_by; ?></b></td>
                <td><b><?php echo $category; ?></b></td>
                <td></td>
                <td><?php echo count_no_po($lvl,$level3_approval_by,$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po($lvl,$level3_approval_by,$cmsd,$cmed); ?></td>
            </tr>
            <?php } ?>
            <tr>
            	<td><?php echo count_no_po($lvl,'total',$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po($lvl,'total',$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po($lvl,'total',$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$lfmsd,$lfmed); ?></td>
                <td colspan="3" style="text-align:center; background-color:#999"><b>TOTAL</b></td>
                <td><?php echo count_no_po($lvl,'total',$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po($lvl,'total',$cmsd,$cmed); ?></td>
            </tr>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>