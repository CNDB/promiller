<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
$po_type = $_REQUEST['po_type'];
$base_date = $_REQUEST['base_date']; 
//die; 
?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date'), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, '$base_date'), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, '$base_date')-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, '$base_date')-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, '$base_date')-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, '$base_date')-3, -1) as ltmed";

//echo $sql;
//die;

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11); //Base Month
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);//First Last Month
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);//Second Last Month
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11); //Third Last Month
}

?>
<!--- No of Purchase Orders L1------>

<?php
	$sql_proc = "exec tipldb..no_with_costsaving_report '$lsmsd', '$cmed'";
	
	$qry_proc = $this->db->query($sql_proc);
	
	//echo $sql_proc; die;
	
	//echo "chandra narayan sharma";
	//die;
?>

<!----Response Div Starts ---->

<!--<table width="450px" border="1" cellpadding="5px" cellspacing="5px" style="font-weight:bold">
    <tr style="background-color:#0CF">
        <td colspan="6">Abbreviations</td>
    </tr>
    <tr>
        <td>LP</td>
        <td>Saving = Last Price - Current Price</td>
        <td>Loss  = Current Price - Last Price</td>
        <td>CP</td>
        <td>Saving = Costing - Current Price</td>
        <td>Loss = Current Price -Costing</td>
    </tr>  
</table><br>-->
<br />
<table class="table table-bordered" style="font-size:10px">
    <tr style="background-color:#CCC">
        <td colspan="25" style="text-align:center; font-size:14px"><b>NO WITH COST SAVING REPORT (IN LAKHS)</b></td>
    </tr>
    <tr style="background-color:#CCC">
        <td colspan="8" style="text-align:center"><b><?php month_year($lsmsd); ?></b></td>
        <td colspan="8" style="text-align:center"><b><?php month_year($lfmsd); ?></b></td>
        <td rowspan="3"><b>Category</b></td>
        <td colspan="8" style="text-align:center"><b>Base Month <?php month_year($cmsd); ?></b></td>
    </tr>
    <tr style="background-color:#CCC">
        <td rowspan="2" style="text-align:center"><b>No</b></td>
        <td rowspan="2" style="text-align:center"><b>Value (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>W/O Basis (No)</b></td>
        <td colspan="2" style="text-align:center"><b>Saving (Lakh)</b></td>
        <td colspan="2" style="text-align:center"><b>Loss (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>Net Saving/Loss (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>No</b></td>
        <td rowspan="2" style="text-align:center"><b>Value (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>W/O Basis (No)</b></td>
        <td colspan="2" style="text-align:center"><b>Saving (Lakh)</b></td>
        <td colspan="2" style="text-align:center"><b>Loss (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>Net Saving/Loss (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>No</b></td>
        <td rowspan="2" style="text-align:center"><b>Value (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>W/O Basis (No)</b></td>
        <td colspan="2" style="text-align:center"><b>Saving (Lakh)</b></td>
        <td colspan="2" style="text-align:center"><b>Loss (Lakh)</b></td>
        <td rowspan="2" style="text-align:center"><b>Net Saving/Loss (Lakh)</b></td>
    </tr>
    <tr style="background-color:#CCC">
        <td><b>LP</b></td>
        <td><b>CP</b></td>
        <td><b>LP</b></td>
        <td><b>CP</b></td>
        <td><b>LP</b></td>
        <td><b>CP</b></td>
        <td><b>LP</b></td>
        <td><b>CP</b></td>
        <td><b>LP</b></td>
        <td><b>CP</b></td>
        <td><b>LP</b></td>
        <td><b>CP</b></td>
    </tr>
    <?php
        $sql="select distinct po_category from tipldb..po_master_table where 
        po_category is not null and po_category !='' 
        and po_category not in('OTHERS','RAW MATERIALS')
        --and po_category = 'SECURITY' 
        ORDER BY po_category";
        
        $qry=$this->db->query($sql);
        
        foreach($qry->result() as $row){
            $category = $row->po_category;
            
            $link1 = "&&po_type=".$po_type."&&category=".$category."&&from_date=".$lsmsd."&&to_date=".$lsmed; //Second Last Month
            $link2 = "&&po_type=".$po_type."&&category=".$category."&&from_date=".$lfmsd."&&to_date=".$lfmed; //Last Month
            $link3 = "&&po_type=".$po_type."&&category=".$category."&&from_date=".$cmsd."&&to_date=".$cmed;   //Current Month
            
            $link4 = "&&po_type=".$po_type."&&category=total&&from_date=".$lsmsd."&&to_date=".$lsmed; //Second Last Month
            $link5 = "&&po_type=".$po_type."&&category=total&&from_date=".$lfmsd."&&to_date=".$lfmed; //Last Month
            $link6 = "&&po_type=".$po_type."&&category=total&&from_date=".$cmsd."&&to_date=".$cmed;   //Current Month
    ?>
    <tr>
        <!--- second last month ----->
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link1; ?>" target="_blank">
                <?php echo count_no_po_cat($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link1; ?>" target="_blank">
                <?php echo sum_no_po_cat($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link1; ?>" target="_blank">
                <?php echo without_basis_count($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link1; ?>" target="_blank">
                <?php echo saving_lp_cp($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link1; ?>" target="_blank">
                <?php echo saving_costing_cp($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link1; ?>" target="_blank">
                <?php echo loss_lp_cp($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link1; ?>" target="_blank">
                <?php echo loss_costing_cp($po_type,$category,$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <?php
                $net_saving = saving_lp_cp($po_type,$category,$lsmsd,$lsmed)+saving_costing_cp($po_type,$category,$lsmsd,$lsmed);
                $net_loss   = loss_lp_cp($po_type,$category,$lsmsd,$lsmed)+loss_costing_cp($po_type,$category,$lsmsd,$lsmed);
                
                echo $tot_saving_loss = $net_saving-$net_loss;
            ?>

        </td>
        <!--- last month ----->
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link2; ?>" target="_blank">
                <?php echo count_no_po_cat($po_type,$category,$lfmsd,$lfmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link2; ?>" target="_blank">
                <?php echo sum_no_po_cat($po_type,$category,$lfmsd,$lfmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link2; ?>" target="_blank">
                <?php echo without_basis_count($po_type,$category,$lfmsd,$lfmed); ?>
            </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link2; ?>" target="_blank">
        <?php echo saving_lp_cp($po_type,$category,$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link2; ?>" target="_blank">
        <?php echo saving_costing_cp($po_type,$category,$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link2; ?>" target="_blank">
        <?php echo loss_lp_cp($po_type,$category,$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link2; ?>" target="_blank">
        <?php echo loss_costing_cp($po_type,$category,$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
            <?php
                $net_saving = saving_lp_cp($po_type,$category,$lfmsd,$lfmed)+saving_costing_cp($po_type,$category,$lfmsd,$lfmed);
                $net_loss   = loss_lp_cp($po_type,$category,$lfmsd,$lfmed)+loss_costing_cp($po_type,$category,$lfmsd,$lfmed);
                
                echo $tot_saving_loss = $net_saving-$net_loss;
            ?>
        </td>
        <td><b><?php echo $category; ?></b></td>
        <!--- Current Month ----->
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link3; ?>" target="_blank">
                <?php echo count_no_po_cat($po_type,$category,$cmsd,$cmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link3; ?>" target="_blank">
                <?php echo sum_no_po_cat($po_type,$category,$cmsd,$cmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link3; ?>" target="_blank">
                <?php echo without_basis_count($po_type,$category,$cmsd,$cmed); ?>
            </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link3; ?>" target="_blank">
        <?php echo saving_lp_cp($po_type,$category,$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link3; ?>" target="_blank">
        <?php echo saving_costing_cp($po_type,$category,$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link3; ?>" target="_blank">
        <?php echo loss_lp_cp($po_type,$category,$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link3; ?>" target="_blank">
        <?php echo loss_costing_cp($po_type,$category,$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
            <?php
                $net_saving = saving_lp_cp($po_type,$category,$cmsd,$cmed)+saving_costing_cp($po_type,$category,$cmsd,$cmed);
                $net_loss   = loss_lp_cp($po_type,$category,$cmsd,$cmed)+loss_costing_cp($po_type,$category,$cmsd,$cmed);
                
                echo $tot_saving_loss = $net_saving-$net_loss;
            ?>
        </td>
    </tr>
    <?php } ?>
    <!------ Totals -------------->
    <tr>
        <!--- second last month ----->
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link4; ?>" target="_blank">
                <?php echo count_no_po_cat($po_type,'total',$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link4; ?>" target="_blank">
                <?php echo sum_no_po_cat($po_type,'total',$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link4; ?>" target="_blank">
                <?php echo without_basis_count($po_type,'total',$lsmsd,$lsmed); ?>
            </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link4; ?>" target="_blank">
        <?php echo saving_lp_cp($po_type,'total',$lsmsd,$lsmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link4; ?>" target="_blank">
        <?php echo saving_costing_cp('total',$lsmsd,$lsmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link4; ?>" target="_blank">
        <?php echo loss_lp_cp($po_type,'total',$lsmsd,$lsmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link4; ?>" target="_blank">
        <?php echo loss_costing_cp($po_type,'total',$lsmsd,$lsmed); ?>
        </a>
        </td>
        <td>
            <?php
                $net_saving = saving_lp_cp($po_type,'total',$lsmsd,$lsmed)+saving_costing_cp($po_type,'total',$lsmsd,$lsmed);
                $net_loss   = loss_lp_cp($po_type,'total',$lsmsd,$lsmed)+loss_costing_cp($po_type,'total',$lsmsd,$lsmed);
                
                echo $tot_saving_loss = $net_saving-$net_loss;
            ?>
        </td>
        <!--- last month ----->
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link5; ?>" target="_blank">
                <?php echo count_no_po_cat($po_type,'total',$lfmsd,$lfmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link5; ?>" target="_blank">
                <?php echo sum_no_po_cat($po_type,'total',$lfmsd,$lfmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link5; ?>" target="_blank">
                <?php echo without_basis_count($po_type,'total',$lfmsd,$lfmed); ?>
            </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link5; ?>" target="_blank">
        <?php echo saving_lp_cp($po_type,'total',$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link5; ?>" target="_blank">
        <?php echo saving_costing_cp($po_type,'total',$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link5; ?>" target="_blank">
        <?php echo loss_lp_cp($po_type,'total',$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link5; ?>" target="_blank">
        <?php echo loss_costing_cp($po_type,'total',$lfmsd,$lfmed); ?>
        </a>
        </td>
        <td>
            <?php
                $net_saving = saving_lp_cp('total',$lfmsd,$lfmed)+saving_costing_cp('total',$lfmsd,$lfmed);
                $net_loss   = loss_lp_cp($po_type,'total',$lfmsd,$lfmed)+loss_costing_cp('total',$lfmsd,$lfmed);
                
                echo $tot_saving_loss = $net_saving-$net_loss;
            ?>
        </td>
        <td style="text-align:center; background-color:#999"><b>TOTAL</b></td>
        <!--- current month ----->
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link6; ?>" target="_blank">
                <?php echo count_no_po_cat($po_type,'total',$cmsd,$cmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link6; ?>" target="_blank">
                <?php echo sum_no_po_cat($po_type,'total',$cmsd,$cmed); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link6; ?>" target="_blank">
                <?php echo without_basis_count($po_type,'total',$cmsd,$cmed); ?>
            </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link6; ?>" target="_blank">
        <?php echo saving_lp_cp($po_type,'total',$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link6; ?>" target="_blank">
        <?php echo saving_costing_cp($po_type,'total',$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link6; ?>" target="_blank">
        <?php echo loss_lp_cp($po_type,'total',$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
        <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link6; ?>" target="_blank">
        <?php echo loss_costing_cp($po_type,'total',$cmsd,$cmed); ?>
        </a>
        </td>
        <td>
            <?php
                $net_saving = saving_lp_cp($po_type,'total',$cmsd,$cmed)+saving_costing_cp($po_type,'total',$cmsd,$cmed);
                $net_loss   = loss_lp_cp($po_type,'total',$cmsd,$cmed)+loss_costing_cp($po_type,'total',$cmsd,$cmed);
                
                echo $tot_saving_loss = $net_saving-$net_loss;
            ?>
        </td>
    </tr>
</table>
<!----Response Div Ends ---->