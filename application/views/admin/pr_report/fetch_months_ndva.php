<?php $this->load->helper('pr_cycle_report'); ?>

<?php $pr_type = $_REQUEST['pr_type']; ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed,

DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-4, 0) as lfomsd, DATEADD(MONTH, DATEDIFF(MONTH, -4, GETDATE())-4, -1) as lfomed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-5, 0) as lfimsd, DATEADD(MONTH, DATEDIFF(MONTH, -5, GETDATE())-5, -1) as lfimed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-6, 0) as lsimsd, DATEADD(MONTH, DATEDIFF(MONTH, -6, GETDATE())-6, -1) as lsimed,

DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-7, 0) as lsemsd, DATEADD(MONTH, DATEDIFF(MONTH, -7, GETDATE())-7, -1) as lsemed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-8, 0) as legmsd, DATEADD(MONTH, DATEDIFF(MONTH, -8, GETDATE())-8, -1) as legmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-9, 0) as lnimsd, DATEADD(MONTH, DATEDIFF(MONTH, -9, GETDATE())-9, -1) as lnimed,

DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-10, 0) as ltemsd, DATEADD(MONTH, DATEDIFF(MONTH, -10, GETDATE())-10, -1) as ltemed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-11, 0) as lelmsd, DATEADD(MONTH, DATEDIFF(MONTH, -11, GETDATE())-11, -1) as lelmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0) as ltwmsd, DATEADD(MONTH, DATEDIFF(MONTH, -12, GETDATE())-12, -1) as ltwmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11); //base month
	
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);//Last month
	
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);// second last month
	
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11); //Third last
	
	$lfomsd = substr($row->lfomsd,0,11);
	$lfomed = substr($row->lfomed,0,11);//fourth last
	
	$lfimsd = substr($row->lfimsd,0,11);
	$lfimed = substr($row->lfimed,0,11);// fifth last
	
	$lsimsd = substr($row->lsimsd,0,11);
	$lsimed = substr($row->lsimed,0,11);// Sixth last
	
	$lsemsd = substr($row->lsemsd,0,11);
	$lsemed = substr($row->lsemed,0,11);// Seventh Last
	
	$legmsd = substr($row->legmsd,0,11);
	$legmed = substr($row->legmed,0,11);// Eight Last
	
	$lnimsd = substr($row->lnimsd,0,11);
	$lnimed = substr($row->lnimed,0,11);// Ninth last
	
	$ltemsd = substr($row->ltemsd,0,11);
	$ltemed = substr($row->ltemed,0,11);// Tenth Last
	
	$lelmsd = substr($row->lelmsd,0,11);
	$lelmed = substr($row->lelmed,0,11);// Eleventh Last
	
	$ltwmsd = substr($row->ltwmsd,0,11);
	$ltwmed = substr($row->ltwmed,0,11);// Twelth Last
	
}

?>

<div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-1"><b style="font-size:14px;">BASE MONTH:</b></div>
    <div class="col-lg-3">
        <select id="base_month" name="base_month" class="form-control" onChange="filter_ndva('<?php echo $pr_type; ?>',this.value);">
            <option value="" disabled selected>--Select--</option>
            <option value="<?php echo $cmsd; ?>"><?php month_year($cmsd); ?></option>
            <option value="<?php echo $lfmsd; ?>"><?php month_year($lfmsd); ?></option>
            <option value="<?php echo $lsmsd; ?>"><?php month_year($lsmsd); ?></option>
            <option value="<?php echo $ltmsd; ?>"><?php month_year($ltmsd); ?></option>
            <option value="<?php echo $lfomsd; ?>"><?php month_year($lfomsd); ?></option>
            <option value="<?php echo $lfimsd; ?>"><?php month_year($lfimsd); ?></option>
            <option value="<?php echo $lsimsd; ?>"><?php month_year($lsimsd); ?></option>
            <option value="<?php echo $lsemsd; ?>"><?php month_year($lsemsd); ?></option>
            <option value="<?php echo $legmsd; ?>"><?php month_year($legmsd); ?></option>
            <option value="<?php echo $lnimsd; ?>"><?php month_year($lnimsd); ?></option>
            <option value="<?php echo $ltemsd; ?>"><?php month_year($ltemsd); ?></option>
            <option value="<?php echo $lelmsd; ?>"><?php month_year($lelmsd); ?></option>
            <option value="<?php echo $ltwmsd; ?>"><?php month_year($ltwmsd); ?></option>
        </select>
    </div>
    <div class="col-lg-4"></div>
</div>


<div>
    <div class="row">
        <div class="col-lg-12" id="filter_div_ndva">
         <!----- AJAX RESPONSE DIV ------>  
        </div>
    </div><br>
</div><br>