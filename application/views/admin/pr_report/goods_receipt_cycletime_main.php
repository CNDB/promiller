<?php $this->load->helper('pr_cycle_report'); ?>

<?php
//Procedure Run
$sql_proc = "exec tipldb..gate_entry_cycletime_rpt";
$qry_proc = $this->db->query($sql_proc);

$this->db->close();
$this->db->initialize();
?>

<!--- Good Receipt Cycletime Report ------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10"><h4 style="text-align:center"><u>Goods Receipt Cycletime Report</u></h4></div>
    <div class="col-lg-1"></div>
</div><br>

<div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-2"><h4 style="text-align:center">Select Category</h4></div>
    <div class="col-lg-2">
        <select id="category" name="category" class="form-control" onchange="get_potype(this.value);">
        	<option value="">--Select--</option>
            <option value="ALL_CATEGORY">ALL CATEGORY</option>
            <?php
				$sql_cat = "select distinct po_category from tipldb..po_master_table where po_category is not null 
				and po_category != '' and po_category NOT IN('OTHERS','RAW MATERIALS') order by po_category";
				
				$qry_cat = $this->db->query($sql_cat);
				
				foreach($qry_cat->result() as $row){
					$category = $row->po_category;
			?>
            	<option value="<?php echo $category; ?>"><?php echo $category; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-4"></div>
</div><br>

<!-- Ajax Div -->
<div class="row" id="po_type"></div>