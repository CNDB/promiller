<?php $this->load->helper('pr_cycle_report'); ?>
<!--- PO Types ------>
<?php $category = $_REQUEST['category']; ?>

<div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-2"><h4 style="text-align:center">PO Type</h4></div>
    <div class="col-lg-2">
        <select id="pur_order_type" name="pur_order_type" class="form-control" onChange="get_base_month('<?php echo $category; ?>',this.value);">
        	<option value="">--Select--</option>
            <option value="ALL_PO">ALL PO</option>
            <option value="CGP">CGP</option>
            <option value="FPO">FPO</option>
            <option value="IPO">IPO</option>
            <option value="LPO">LPO</option>
        </select>
    </div>
    <div class="col-lg-4"></div>
</div><br>

<!-- Ajax Div -->
<div class="row" id="base_month_div"></div>