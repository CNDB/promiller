<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>
<!--- No of Purchase Orders L1------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <h4 style="text-align:center">No. With Cost Saving<br>(IN LAKHS)</h4>
        <table class="table table-bordered" style="font-size:10px">
            <tr style="background-color:#CCC">
                <td colspan="4" style="text-align:center"><b><?php month_year($ltmsd); ?></b></td>
                <td colspan="4" style="text-align:center"><b><?php month_year($lsmsd); ?></b></td>
                <td colspan="4" style="text-align:center"><b><?php month_year($lfmsd); ?></b></td>
                <td rowspan="2" style="text-align:center; vertical-align:middle"><b>CATEGORY</b></td>
                <td colspan="4" style="text-align:center"><b>CURRENT MONTH</b></td>
            </tr>
            <tr style="background-color:#CCC">
            	<td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>SAVING</b></td>
                <td><b>LOSS</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>SAVING</b></td>
                <td><b>LOSS</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>SAVING</b></td>
                <td><b>LOSS</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>VALUE</b></td>
                <td><b>SAVING</b></td>
                <td><b>LOSS</b></td>
            </tr>
            
            <?php
			$sql="select distinct po_category from tipldb..po_master_table where 
			po_category is not null and po_category !='' 
			and po_category not in('OTHERS','RAW MATERIALS') ORDER BY po_category";
			
			$qry=$this->db->query($sql);
			
			foreach($qry->result() as $row){
				$category = $row->po_category;
			?>
            <tr>
            	<td><?php echo count_no_po_cat($category,$ltmsd,$ltmed); ?></td>
                <td><?php echo sum_no_po_cat($category,$ltmsd,$ltmed); ?></td>
                <td><?php echo saving_no_po_cat($category,$ltmsd,$ltmed); ?></td>
                <td><?php echo loss_no_po_cat($category,$ltmsd,$ltmed); ?></td>
                <td><?php echo count_no_po_cat($category,$lsmsd,$lsmed); ?></td>
                <td><?php echo sum_no_po_cat($category,$lsmsd,$lsmed); ?></td>
                <td><?php echo saving_no_po_cat($category,$lsmsd,$lsmed); ?></td>
                <td><?php echo loss_no_po_cat($category,$lsmsd,$lsmed); ?></td>
                <td><?php echo count_no_po_cat($category,$lfmsd,$lfmed); ?></td>
                <td><?php echo sum_no_po_cat($category,$lfmsd,$lfmed); ?></td>
                <td><?php echo saving_no_po_cat($category,$lfmsd,$lfmed); ?></td>
                <td><?php echo loss_no_po_cat($category,$lfmsd,$lfmed); ?></td>
                <td><b><?php echo $category; ?></b></td>
                <td><?php echo count_no_po_cat($category,$cmsd,$cmed); ?></td>
                <td><?php echo sum_no_po_cat($category,$cmsd,$cmed); ?></td>
                <td><?php echo saving_no_po_cat($category,$cmsd,$cmed); ?></td>
                <td><?php echo loss_no_po_cat($category,$cmsd,$cmed); ?></td>
            </tr>
            <?php } ?> 
            <tr style="background-color:#CCC">
            	<td style="font-weight:bold;"><?php echo count_no_po_cat('total',$ltmsd,$ltmed); ?></td>
                <td style="font-weight:bold;"><?php echo sum_no_po_cat('total',$ltmsd,$ltmed); ?></td>
                <td style="font-weight:bold;"><?php echo saving_no_po_cat('total',$ltmsd,$ltmed); ?></td>
                <td style="font-weight:bold;"><?php echo loss_no_po_cat('total',$ltmsd,$ltmed); ?></td>
                <td style="font-weight:bold;"><?php echo count_no_po_cat('total',$lsmsd,$lsmed); ?></td>
                <td style="font-weight:bold;"><?php echo sum_no_po_cat('total',$lsmsd,$lsmed); ?></td>
                <td style="font-weight:bold;"><?php echo saving_no_po_cat('total',$lsmsd,$lsmed); ?></td>
                <td style="font-weight:bold;"><?php echo loss_no_po_cat('total',$lsmsd,$lsmed); ?></td>
                <td style="font-weight:bold;"><?php echo count_no_po_cat('total',$lfmsd,$lfmed); ?></td>
                <td style="font-weight:bold;"><?php echo sum_no_po_cat('total',$lfmsd,$lfmed); ?></td>
                <td style="font-weight:bold;"><?php echo saving_no_po_cat('total',$lfmsd,$lfmed); ?></td>
                <td style="font-weight:bold;"><?php echo loss_no_po_cat('total',$lfmsd,$lfmed); ?></td>
                <td style="font-weight:bold;"><b>TOTAL</b></td>
                <td style="font-weight:bold;"><?php echo count_no_po_cat('total',$cmsd,$cmed); ?></td>
                <td style="font-weight:bold;"><?php echo sum_no_po_cat('total',$cmsd,$cmed); ?></td>
                <td style="font-weight:bold;"><?php echo saving_no_po_cat('total',$cmsd,$cmed); ?></td>
                <td style="font-weight:bold;"><?php echo loss_no_po_cat('total',$cmsd,$cmed); ?></td>
            </tr> 
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>