<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>

<STYLE>

.tab_cls{
	font-size:11px;	
}

.td_cls{
	font-size:11px;
	text-align:center;
}

</STYLE>
<!--- No of Purchase Orders L1------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <h4 style="text-align:center">Approval Cycletime Report</h4>
        <table class="table table-bordered tab_cls">
            <tr style="background-color:#CCC">
                <td colspan="2" class="td_cls"><b><?php month_year($ltmsd); ?></b></td>
                <td colspan="2" class="td_cls"><b><?php month_year($lsmsd); ?></b></td>
                <td colspan="2" class="td_cls"><b><?php month_year($lfmsd); ?></b></td>
                <td rowspan="2" class="td_cls"><b>Approving Authority</b></td>
                <td rowspan="2" class="td_cls"><b>Category</b></td>
                <td colspan="2" class="td_cls"><b>Current Month</b></td>
            </tr>
            <tr style="background-color:#CCC">
            	<td><b>NO OF PO'S</b></td>
                <td><b>AVG APP TIME<br />(HOURS)</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>AVG APP TIME<br />(HOURS)</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>AVG APP TIME<br />(HOURS)</b></td>
                <td><b>NO OF PO'S</b></td>
                <td><b>AVG APP TIME<br />(HOURS)</b></td>
                
            </tr>
            <?php
				
				$sql_app = "select distinct level1_approval_by as owner_name from tipldb..po_master_table 
				where level1_approval_by is not null and level1_approval_by != ''
				and level1_approval_by not in('mmrathore','saket.ranjan','bharat.sharma','ashwani.giri')
				UNION
				select distinct level2_approval_by as owner_name from tipldb..po_master_table 
				where level2_approval_by is not null and level2_approval_by != ''
				and level2_approval_by not in('raviraj.mehra','sunil.tandon')
				UNION
				select distinct level3_approval_by as owner_name from tipldb..po_master_table 
				where level3_approval_by is not null and level3_approval_by != ''
				order by owner_name"; 
				
				$qry_app = $this->db->query($sql_app);
				
				foreach($qry_app->result() as $row){
					$owner_name = $row->owner_name;
					
					$sql_cat = "select distinct po_category from tipldb..po_master_table 
					where level1_approval_by is not null and level1_approval_by != ''
					and level1_approval_by not in('mmrathore','saket.ranjan','bharat.sharma','ashwani.giri')
					and level1_approval_by = '$owner_name'
					and po_category != ''
					UNION
					select distinct po_category from tipldb..po_master_table 
					where level2_approval_by is not null and level2_approval_by != ''
					and level2_approval_by not in('raviraj.mehra','sunil.tandon')
					and level2_approval_by = '$owner_name'
					and po_category != ''
					UNION
					select distinct po_category from tipldb..po_master_table 
					where level3_approval_by is not null and level3_approval_by != ''
					and level3_approval_by = '$owner_name'
					and po_category != ''
					order by po_category";
					
					$qry_cat = $this->db->query($sql_cat);
					
					$category = '';
					$i=0;
					foreach($qry_cat->result() as $row){
						$i++;
						$category1 = $row->po_category;
						if($i==1){
							$category = $category.$category1;
						} else {
							$category = $category.", ".$category1;
						}
					}
					
					$link1 = "&&owner_name=".$owner_name."&&from_date=".$ltmsd."&&to_date=".$ltmed;
					$link2 = "&&owner_name=".$owner_name."&&from_date=".$lsmsd."&&to_date=".$lsmed;
					$link3 = "&&owner_name=".$owner_name."&&from_date=".$lfmsd."&&to_date=".$lfmed;
					$link4 = "&&owner_name=".$owner_name."&&from_date=".$cmsd."&&to_date=".$cmed;
					
					//TOTAL CALCULATION
					$link5 = "&&owner_name=total&&from_date=".$ltmsd."&&to_date=".$ltmed;
					$link6 = "&&owner_name=total&&from_date=".$lsmsd."&&to_date=".$lsmed;
					$link7 = "&&owner_name=total&&from_date=".$lfmsd."&&to_date=".$lfmed;
					$link8 = "&&owner_name=total&&from_date=".$cmsd."&&to_date=".$cmed;
			?>
            <tr>
            	<td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link1; ?>" target="_blank">
                    	<?php echo count_no_po($owner_name,$ltmsd,$ltmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link1; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po($owner_name,$ltmsd,$ltmed)/count_no_po($owner_name,$ltmsd,$ltmed),2); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link2; ?>" target="_blank">
                    	<?php echo count_no_po($owner_name,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link2; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po($owner_name,$lsmsd,$lsmed)/count_no_po($owner_name,$lsmsd,$lsmed),2); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link3; ?>" target="_blank">
                    	<?php echo count_no_po($owner_name,$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link3; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po($owner_name,$lfmsd,$lfmed)/count_no_po($owner_name,$lfmsd,$lfmed),2); ?>
                    </a>
                </td>
                <td><b><?php echo $owner_name; ?></b></td>
                <td><b><?php echo $category; ?></b></td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link4; ?>" target="_blank">
                    	<?php echo count_no_po($owner_name,$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link4; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po($owner_name,$cmsd,$cmed)/count_no_po($owner_name,$cmsd,$cmed),2); ?>
                    </a>
                </td>
            </tr>
            <?php } ?>
            <tr>
            	<td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link5; ?>" target="_blank">
                    	<?php echo count_no_po('total',$ltmsd,$ltmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link5; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po('total',$ltmsd,$ltmed)/count_no_po('total',$ltmsd,$ltmed),2); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link6; ?>" target="_blank">
                    	<?php echo count_no_po('total',$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link6; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po('total',$lsmsd,$lsmed)/count_no_po('total',$lsmsd,$lsmed),2); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link7; ?>" target="_blank">
                    	<?php echo count_no_po('total',$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link7; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po('total',$lfmsd,$lfmed)/count_no_po('total',$lfmsd,$lfmed),2); ?>
                    </a>
                </td>
                <td colspan="2" style="text-align:center; background-color:#999"><b>TOTAL</b></td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link8; ?>" target="_blank">
                    	<?php echo count_no_po('total',$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=avg_app_time_no_po_det<?php echo $link8; ?>" target="_blank">
                    	<?php echo number_format(avg_app_time_no_po('total',$cmsd,$cmed)/count_no_po('total',$cmsd,$cmed),2); ?>
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>