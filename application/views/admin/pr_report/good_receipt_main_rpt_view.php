<?php $this->load->helper('pr_cycle_report'); ?>

<?php
	$category = $_REQUEST['category'];
	$po_type = $_REQUEST['po_type'];
	$fsd = $_REQUEST['fsd'];
	$fed = $_REQUEST['fed'];
	$first_col = $_REQUEST['first_col'];
	$second_col = $_REQUEST['second_col'];
	$type = $_REQUEST['type'];
	$fun = $_REQUEST['fun'];
	
	if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
			
	}
		
	if($po_type == 'ALL_PO'){
		$po_type = "FPO','IPO','LPO','CGP";
		
	}
	
	if($second_col != 'ge_rec_date' && $second_col != 'ge_send_to_pur_date'){
		$where_str = "where category in('".$category."') and substring(order_no,1,3) in('".$po_type."')";
	} else {
		$where_str = "where ge_no != ''";
	}
	
	if($fsd != "" && $fed != ""){
		$where_str .= " and ".$first_col." is not null and ".$second_col." is not null and convert(date,ge_create_date) between '".$fsd."' and '".$fed."'";	
	} else {
		if($second_col == 'ge_rec_date'){
			$where_str .= " and ".$first_col." is not null and ge_status = 'Fresh'";	
		} else if($second_col == 'ge_send_to_pur_date'){	
			$where_str .= " and ".$first_col." is not null  
			and grcpt_no is NULL and ge_status in('Received')";	
		} else if($second_col == 'ge_send_to_acc_date'){	
			$where_str .= " and ".$first_col." is not null 
			and grcpt_no is NULL 
			and ge_status in('Pending For HSN Check By Purchaser')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";	
		} else if($second_col == 'grcpt_create_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null and grcpt_no is NULL 
			and ge_status in('Pending For GRCPT')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";	
		} else if($second_col == 'grcpt_fr_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null and grcpt_no is not NULL and grcpt_status = 'FR'";	
		} else if($second_col == 'grcpt_live_insp_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null and grcpt_no is not NULL and grcpt_status = 'FZ'
			and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')";	
		} else if($second_col == 'grcpt_erp_mov_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null and grcpt_no is not NULL and grcpt_status = 'FA'";	
		} else if($second_col == 'live_rej_check_pur_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null and grcpt_no is not NULL and grcpt_status = 'FM' 
			and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
			and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
			and ge_status in('Inspection Done')";
				
		} else if($second_col == 'erp_gretn_create_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)";	
			
		} else if($second_col == 'live_gretn_create_date'){	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
			and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))";
				
		} else {	
			$where_str .= " and ".$first_col." is not null and ".$second_col." is null ";
		}
		
	}
	
	if($type == "greater"){
		$where_str .= " and datediff(hour,".$first_col.",getdate()) > 4";
	} else if($type == "less"){
		$where_str .= " and datediff(hour,".$first_col.",getdate()) <= 4";
	} else if($type == "all"){
		$where_str .= "";
	}
	
	$where_str .= " order by ge_create_date desc";
?>



<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">DETAILS</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-12">
        	<table class="table table-bordered" style="text-align:center; border:solid 1px #000000;">
            	<?php 
				if($fun != ""){
					
					if($fun == 'gr_age_monthwise_tot_det'){
						echo gr_age_monthwise_tot_det($category,$po_type,$fsd,$fed);
					} else if($fun == 'gr_open_cases_less_det') {
						echo gr_open_cases_less_det($category,$po_type);
					} else if($fun == 'gr_open_cases_greater_det') {
						echo gr_open_cases_greater_det($category,$po_type);
					} else if($fun == 'gr_open_cases_all_det') {
						echo gr_open_cases_all_det($category,$po_type);
					}
					
				} else { 
				?>
                <tr style="background-color:#CCC">
                    <td><b>SNO</b></td>
                    <td><b>GATE ENTRY NO</b></td>
                    <td><b>GATE ENTRY CREATE DATE</b></td>
                    <td><b>PO NUMBER</b></td>
                    <td><b>PO STATUS</b></td>
                    <td><b>SUPPLIER NAME</b></td>
                    <td><b>CATEGORY</b></td>
                    <td><b>GRCPT NO</b></td>
                    <td><b>GRCPT STATUS</b></td>
                    <td><b>GE STATUS</b></td>
                    <td><b>GRETN NO</b></td>
                    <td><b>START DATE(A)</b></td>
                    <td><b>END DATE(B)</b></td>
                    <td><b>ACTUAL AGE<br />(A - B)<br />(HOURS)</b></td>
                    <td><b>AGE FROM GATE ENTRY<br />(HOURS)</b></td>
                    <?php if($type != ''){ ?>
                    <td><b>PENDING ON DEPT</b></td>
                    <td style="text-transform:uppercase"><b>PENDING ON</b></td>
                    <?php } ?>
                </tr>
                <?php
					if($type != ""){
						$second_col1 = date("Y-m-d H:i:s");
						$second_col = "'".date("Y-m-d H:i:s")."'";
					}
					
					$sql = "select DATEDIFF(HOUR,$first_col,$second_col) as diff1, DATEDIFF(HOUR,ge_create_date,getdate()) as diff2,* 
					from tipldb..ge_to_grcptfm_cycletime ".$where_str; 
					
					//echo $sql; die;
					
					$qry = $this->db->query($sql);
					
					$sno =0;
					$diff_tot = 0;
					foreach($qry->result() as $row){
						$sno++;
						$ge_no = $row->ge_no;
						$ge_create_date = $row->ge_create_date;
						$order_no = $row->order_no;
						$order_curr_stat = $row->order_curr_stat;
						$supplier_name = $row->supplier_name;
						$category = $row->category;
						$grcpt_no = $row->grcpt_no;
						$grcpt_status = $row->grcpt_status;
						$ge_status = $row->for_status;
						$gretn_no = $row->gretn_no;
						$pend_dept = $row->pend_dept;
						$pend_on = $row->pend_on;
						$first_column = $row->$first_col;
						$second_column = $row->$second_col;
						$diff1 = $row->diff1;
						$diff_tot = $diff_tot + $diff1;	
						$diff2 = $row->diff2;	
				?>
                <tr>
                	<td><?php echo $sno; ?></td>
                    <td><?php echo $ge_no; ?></td>
                    <td><?php echo $ge_create_date; ?></td>
                    <td><?php echo $order_no; ?></td>
                    <td><?php echo $order_curr_stat; ?></td>
                    <td><?php echo wordwrap($supplier_name,25,"<br>\n"); ?></td>
                    <td><?php echo $category; ?></td>
                    <td><?php echo $grcpt_no; ?></td>
                    <td><?php echo $grcpt_status; ?></td>
                    <td><?php echo $ge_status; ?></td>
                    <td><?php echo $gretn_no; ?></td>
                    <td><?php echo $first_column; ?></td>
                    <td>
					<?php 
						if($type != ''){
							echo $second_col1; 
						} else {
							echo $second_column; 
						}
					?>
                    </td>
                    <td><?php echo number_format($diff1,2,".",""); ?></td>
                    <td><?php echo number_format($diff2,2,".",""); ?></td>
                    
                    <?php if($type != ''){?>
                    <td><?php echo $pend_dept; ?></td>
                    <td><?php echo $pend_on; ?></td>
                    <?php } ?>
                </tr>
                <?php } ?>
                <!-- DIFF AVERAGE -->
                <?php
				$diff_avg = $diff_tot/$sno;
				?>
                
                 <tr>
                	<td colspan="13"><b>AVERAGE (HOURS)</b></td>
                    <td><?php echo number_format($diff_avg,2,".",""); ?></td>
                    <?php if($type != ''){?>
                    <td colspan="3"></td>
                    <?php } ?>
                </tr>
                
                <?php } ?>
            </table>
        </div>
    </div><br>
    
  </section>
</section>


