<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

?>
<!--- PR Creation To PR Authorize Cycle Time ------>
<div class="row">
    <div class="col-lg-1"></div>
    <div class="col-lg-10">
        <h4 style="text-align:center">PR Creation To PR Authorize Cycle Time</h4>
        <table class="table table-bordered">
            <tr style="background-color:#CCC">
                <td colspan="3"><b>Last 3 Months Average<br>(In Days)</b></td>
                <td rowspan="2"><b>Current Month Average<br>(In Days)</b></td>
                <td rowspan="2"><b>Category Owner</b></td>
                <td rowspan="2"><b>Category</b></td>
                <td rowspan="2"><b>Master Category</b></td>
                <td rowspan="2"><b>Age <= 1 Day</b></td>
                <td rowspan="2"><b>Age > 1 Day</b></td>
                <td rowspan="2"><b>Total Items</b></td>
                <td rowspan="2"><b>Open Items Average<br>(In Days)</b></td>
            </tr>
            <tr style="background-color:#CCC">
                <td><b><?php month_year($ltmsd); ?></b></td>
                <td><b><?php month_year($lsmsd); ?></b></td>
                <td><b><?php month_year($lfmsd); ?></b></td>
           </tr>
           <?php
            $sql1 = "select * from tipldb..pr_po_report_master where deleted = 0 and type = 'PR'";
            $qry1 = $this->db->query($sql1);
            
            $count=0;
            foreach($qry1->result() as $row){
                $count++;
                $type          = $row->type;
                $owner_name    = $row->owner_name;
                $category      = $row->category;
                $category_qry  = $row->category_qry;
                $master_category = $row->master_category;
           ?>
           <tr>
                <td><?php echo avg_monthwise($type,$owner_name,$category_qry,$ltmsd,$ltmed); ?></td>
                <td><?php echo avg_monthwise($type,$owner_name,$category_qry,$lsmsd,$lsmed); ?></td>
                <td><?php echo avg_monthwise($type,$owner_name,$category_qry,$lfmsd,$lfmed); ?></td>
                <td><?php echo avg_monthwise($type,$owner_name,$category_qry,$cmsd,$cmed); ?></td>
                <td><b><?php echo $owner_name; ?></b></td>
                <td><b><?php echo $category; ?></b></td>
                <td><b><?php echo $master_category; ?></b></td>
                <td><?php echo pr_age_less($type,$owner_name,$category_qry); ?></td>
                <td><?php echo pr_age_greater($type,$owner_name,$category_qry); ?></td>
                <td><?php echo pr_count($type,$owner_name,$category_qry); ?></td>
                <td><?php echo avg_open_items($type,$owner_name,$category_qry); ?></td>
           </tr>
           <?php } ?>
           <?php
           $sql2 = "select * from tipldb..pr_po_report_master where deleted = 1 and type = 'PR' and owner_name = 'ALL'";
           $qry2 = $this->db->query($sql2);
           foreach($qry2->result() as $row){
                $type           = $row->type;
                $owner_qry = $row->owner_qry;
                $category_qry   = $row->category_qry;
           ?>
           <tr>
                <td style="background-color:#0CF"><?php echo avg_monthwise($type,$owner_qry,$category_qry,$ltmsd,$ltmed); ?></td>
                <td style="background-color:#0CF"><?php echo avg_monthwise($type,$owner_qry,$category_qry,$lsmsd,$lsmed); ?></td>
                <td style="background-color:#0CF"><?php echo avg_monthwise($type,$owner_qry,$category_qry,$lfmsd,$lfmed); ?></td>
                <td style="background-color:#0CF"><?php echo avg_monthwise($type,$owner_qry,$category_qry,$cmsd,$cmed); ?></td>
                <td colspan="3" style="text-align:center; background-color:#0CF"><b>AVERAGE</b></td>
                <td><?php echo pr_age_less($type,$owner_qry,$category_qry); ?></td>
                <td><?php echo pr_age_greater($type,$owner_qry,$category_qry); ?></td>
                <td><?php echo pr_count($type,$owner_qry,$category_qry); ?></td>
                <td style="background-color:#0CF"><?php echo avg_open_items($type,$owner_qry,$category_qry); ?></td>
           </tr>
           <?php } ?>
        </table>
    </div>
    <div class="col-lg-1"></div>
</div><br>