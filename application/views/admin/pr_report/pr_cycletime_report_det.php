<?php $this->load->helper('pr_cycle_report'); ?>

<?php
	$fun = $_REQUEST['fun'];
	$type = $_REQUEST['type'];
	$owner_name = $_REQUEST['owner_name'];
	$category = $_REQUEST['category'];
	$from_date = $_REQUEST['from_date'];
	$to_date = $_REQUEST['to_date'];
	$lvl = $_REQUEST['lvl'];
	$po_type = $_REQUEST['po_type'];
	$case_type = $_REQUEST['case_type'];
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PR-PO-GRCPT REPORT DETAILS</h4>
        </div>
    </div><br />
    
    <?php
	
	/*echo "FUN=".$fun = $_REQUEST['fun'];
	echo "<br>";
	echo "type=".$type = $_REQUEST['type'];
	echo "<br>";
	$owner_name = $_REQUEST['owner_name'];
	echo "category=".$category = $_REQUEST['category'];
	echo "<br>";
	echo "from date=".$from_date = $_REQUEST['from_date'];
	echo "<br>";
	echo "to date=".$to_date = $_REQUEST['to_date'];
	echo "<br>";
	$lvl = $_REQUEST['lvl'];
	$po_type = $_REQUEST['po_type'];
	echo "case type=".$case_type = $_REQUEST['case_type'];
	echo "<br>";
	die;*/
	
	?>
    
    <?php //echo $category; ?>
    
    <div class="row">
    	<div class="col-lg-12">
        	<table class="table table-bordered" style="font-size:10px;">
            	<?php
					if($fun == 'pr_count_det'){
						pr_count_det($type, $owner_name, $category);
					} else if($fun == 'pr_age_greater_det'){
						pr_age_greater_det($type, $owner_name, $category);
					} else if($fun == 'pr_age_less_det'){
						pr_age_less_det($type, $owner_name, $category);
					} else if($fun == 'avg_open_items_det'){
						avg_open_items_det($type, $owner_name, $category);
					} else if($fun == 'avg_monthwise_det'){
						avg_monthwise_det($type, $owner_name, $category, $from_date, $to_date);
					} else if($fun == 'det_count_no_po'){
						det_count_no_po($owner_name, $from_date, $to_date);
					} else if($fun == 'det_sum_no_po'){
						det_sum_no_po($lvl, $owner_name, $from_date, $to_date);
					} else if($fun == 'saving_lp_cp_det'){
						saving_lp_cp_det($po_type, $category, $from_date, $to_date);
					} else if($fun == 'saving_costing_cp_det'){
						saving_costing_cp_det($po_type, $category, $from_date, $to_date);
					} else if($fun == 'loss_lp_cp_det'){
						loss_lp_cp_det($po_type, $category, $from_date, $to_date);
					} else if($fun == 'loss_costing_cp_det'){
						loss_costing_cp_det($po_type,$category, $from_date, $to_date);
					} else if($fun == 'count_no_po_cat_det'){
						count_no_po_cat_det($po_type, $category, $from_date, $to_date);
					} else if($fun == 'without_basis_count_det'){
						without_basis_count_det($po_type, $category, $from_date, $to_date);
					} else if($fun == 'sum_no_po_cat_det'){
						sum_no_po_cat_det($po_type, $category, $from_date, $to_date);
					} else if($fun == 'saving_no_po_cat_det'){
						saving_no_po_cat_det($category, $from_date, $to_date);
					} else if($fun == 'loss_no_po_cat_det'){
						loss_no_po_cat_det($category, $from_date, $to_date);
					} else if($fun == 'avg_app_time_no_po_det'){
						avg_app_time_no_po_det($owner_name, $from_date, $to_date);
					} else if($fun == 'count_cases_det'){                                            //PR NEED DATE VS ACTUAL REPORT
						count_cases_det($case_type, $type, $category, $from_date, $to_date);
					} else if($fun == 'count_cases_tot_det'){
						count_cases_tot_det($case_type, $type, $category, $from_date, $to_date);
					} else if($fun == 'count_cases_det_open'){                                            //PR NEED DATE VS ACTUAL REPORT
						count_cases_det_open($case_type, $type, $category);
					} else if($fun == 'count_cases_tot_det_open'){
						count_cases_tot_det_open($case_type, $type, $category);
					}
				?>
            </table>
        </div>
    </div>
    
  </section>
</section>
