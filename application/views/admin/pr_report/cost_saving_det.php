<?php $this->load->helper('pr_cycle_report'); ?>

<?php 
//Dates 

$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed,

DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-4, 0) as lfomsd, DATEADD(MONTH, DATEDIFF(MONTH, -4, GETDATE())-4, -1) as lfomed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-5, 0) as lfimsd, DATEADD(MONTH, DATEDIFF(MONTH, -5, GETDATE())-5, -1) as lfimed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-6, 0) as lsimsd, DATEADD(MONTH, DATEDIFF(MONTH, -6, GETDATE())-6, -1) as lsimed,

DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-7, 0) as lsemsd, DATEADD(MONTH, DATEDIFF(MONTH, -7, GETDATE())-7, -1) as lsemed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-8, 0) as legmsd, DATEADD(MONTH, DATEDIFF(MONTH, -8, GETDATE())-8, -1) as legmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-9, 0) as lnimsd, DATEADD(MONTH, DATEDIFF(MONTH, -9, GETDATE())-9, -1) as lnimed,

DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-10, 0) as ltemsd, DATEADD(MONTH, DATEDIFF(MONTH, -10, GETDATE())-10, -1) as ltemed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-11, 0) as lelmsd, DATEADD(MONTH, DATEDIFF(MONTH, -11, GETDATE())-11, -1) as lelmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-12, 0) as ltwmsd, DATEADD(MONTH, DATEDIFF(MONTH, -12, GETDATE())-12, -1) as ltwmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11); //base month
	
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);//Last month
	
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);// second last month
	
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11); //Third last
	
	$lfomsd = substr($row->lfomsd,0,11);
	$lfomed = substr($row->lfomed,0,11);//fourth last
	
	$lfimsd = substr($row->lfomed,0,11);
	$lfimed = substr($row->lfimed,0,11);// fifth last
	
	$lsimsd = substr($row->lsimsd,0,11);
	$lsimed = substr($row->lsimed,0,11);// Sixth last
	
	$lsemsd = substr($row->lsemsd,0,11);
	$lsemed = substr($row->lsemed,0,11);// Seventh Last
	
	$legmsd = substr($row->legmsd,0,11);
	$legmed = substr($row->legmed,0,11);// Eight Last
	
	$lnimsd = substr($row->lnimsd,0,11);
	$lnimed = substr($row->lnimed,0,11);// Ninth last
	
	$ltemsd = substr($row->ltemsd,0,11);
	$ltemed = substr($row->ltemed,0,11);// Tenth Last
	
	$lelmsd = substr($row->lelmsd,0,11);
	$lelmed = substr($row->lelmed,0,11);// Eleventh Last
	
	$ltwmsd = substr($row->ltwmsd,0,11);
	$ltwmed = substr($row->ltwmed,0,11);// Twelth Last
	
}

?>
<!--- No of Purchase Orders L1------>

<?php
	//$sql_proc = "exec tipldb..no_with_costsaving_report '$lsmsd', '$cmed'";
	
	//$qry_proc = $this->db->query($sql_proc);
	
	//echo $sql_proc; die;
?>

<?php $po_type = $_REQUEST['po_type']; ?>

<!----Response Div Starts ---->
<div class="row">
	<div class="col-lg-4"></div>
    <div class="col-lg-1"><b style="font-size:14px;">Base Month :</b></div>
    <div class="col-lg-3">
    	<select id="base_month" name="base_month" class="form-control" onChange="filter_month('<?php echo $po_type; ?>',this.value)">
        	<option value="" disabled selected>--Select--</option>
            <option value="<?php echo $cmsd; ?>"><?php month_year($cmsd); ?></option>
            <option value="<?php echo $lfmsd; ?>"><?php month_year($lfmsd); ?></option>
            <option value="<?php echo $lsmsd; ?>"><?php month_year($lsmsd); ?></option>
            <option value="<?php echo $ltmsd; ?>"><?php month_year($ltmsd); ?></option>
            <option value="<?php echo $lfomsd; ?>"><?php month_year($lfomsd); ?></option>
            <option value="<?php echo $lfimsd; ?>"><?php month_year($lfimsd); ?></option>
            <option value="<?php echo $lsimsd; ?>"><?php month_year($lsimsd); ?></option>
            <option value="<?php echo $lsemsd; ?>"><?php month_year($lsemsd); ?></option>
            <option value="<?php echo $legmsd; ?>"><?php month_year($legmsd); ?></option>
            <option value="<?php echo $lnimsd; ?>"><?php month_year($lnimsd); ?></option>
            <option value="<?php echo $ltemsd; ?>"><?php month_year($ltemsd); ?></option>
            <option value="<?php echo $lelmsd; ?>"><?php month_year($lelmsd); ?></option>
            <option value="<?php echo $ltwmsd; ?>"><?php month_year($ltwmsd); ?></option>
        </select>
    </div>
    <div class="col-lg-4"></div>
</div>

<div>
    <div class="row">
        <div class="col-lg-12" id="filter_div2">
         <!----- AJAX RESPONSE DIV ------>  
        </div>
    </div><br>
</div>

<?php /*?><div class="row">
    <div class="col-lg-12">
    	<table width="250px" border="1" cellpadding="5px" cellspacing="5px" style="font-weight:bold">
        	<tr style="background-color:#0CF">
            	<td colspan="4">Abbreviations</td>
            </tr>
        	<tr>
            	<td>LP</td>
                <td>Last Price</td>
                <td>CP</td>
                <td>Current Price</td>
            </tr>  
        </table><br>
        
        <table class="table table-bordered" style="font-size:10px">
            <tr style="background-color:#CCC">
                <td colspan="25" style="text-align:center; font-size:14px"><b>NO WITH COST SAVING REPORT (IN LAKHS)</b></td>
            </tr>
            <tr style="background-color:#CCC">
                <td colspan="8" style="text-align:center"><b><?php month_year($lsmsd); ?></b></td>
                <td colspan="8" style="text-align:center"><b><?php month_year($lfmsd); ?></b></td>
                <td rowspan="3"><b>Category</b></td>
                <td colspan="8" style="text-align:center"><b>Current Month <?php month_year($cmsd); ?></b></td>
            </tr>
            <tr style="background-color:#CCC">
                <td rowspan="2" style="text-align:center"><b>No</b></td>
                <td rowspan="2" style="text-align:center"><b>Value (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>W/O Basis (No)</b></td>
                <td colspan="2" style="text-align:center"><b>Saving (Lakh)</b></td>
                <td colspan="2" style="text-align:center"><b>Loss (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>Net Saving/Loss (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>No</b></td>
                <td rowspan="2" style="text-align:center"><b>Value (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>W/O Basis (No)</b></td>
                <td colspan="2" style="text-align:center"><b>Saving (Lakh)</b></td>
                <td colspan="2" style="text-align:center"><b>Loss (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>Net Saving/Loss (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>No</b></td>
                <td rowspan="2" style="text-align:center"><b>Value (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>W/O Basis (No)</b></td>
                <td colspan="2" style="text-align:center"><b>Saving (Lakh)</b></td>
                <td colspan="2" style="text-align:center"><b>Loss (Lakh)</b></td>
                <td rowspan="2" style="text-align:center"><b>Net Saving/Loss (Lakh)</b></td>
            </tr>
            <tr style="background-color:#CCC">
                <td><b>LP-CP</b></td>
                <td><b>COSTING-CP</b></td>
                <td><b>CP-LP</b></td>
                <td><b>CP-COSTING</b></td>
                <td><b>LP-CP</b></td>
                <td><b>COSTING-CP</b></td>
                <td><b>CP-LP</b></td>
                <td><b>CP-COSTING</b></td>
                <td><b>LP-CP</b></td>
                <td><b>COSTING-CP</b></td>
                <td><b>CP-LP</b></td>
                <td><b>CP-COSTING</b></td>
            </tr>
            <?php
                $sql="select distinct po_category from tipldb..po_master_table where 
                po_category is not null and po_category !='' 
                and po_category not in('OTHERS','RAW MATERIALS')
                --and po_category = 'SECURITY' 
                ORDER BY po_category";
                
                $qry=$this->db->query($sql);
                
                foreach($qry->result() as $row){
                    $category = $row->po_category;
                    
                    $link1 = "&&po_type=".$po_type."&&category=".$category."&&from_date=".$lsmsd."&&to_date=".$lsmed; //Second Last Month
                    $link2 = "&&po_type=".$po_type."&&category=".$category."&&from_date=".$lfmsd."&&to_date=".$lfmed; //Last Month
                    $link3 = "&&po_type=".$po_type."&&category=".$category."&&from_date=".$cmsd."&&to_date=".$cmed;   //Current Month
                    
                    $link4 = "&&po_type=".$po_type."&&category=total&&from_date=".$lsmsd."&&to_date=".$lsmed; //Second Last Month
                    $link5 = "&&po_type=".$po_type."&&category=total&&from_date=".$lfmsd."&&to_date=".$lfmed; //Last Month
                    $link6 = "&&po_type=".$po_type."&&category=total&&from_date=".$cmsd."&&to_date=".$cmed;   //Current Month
            ?>
            <tr>
                <!--- second last month ----->
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link1; ?>" target="_blank">
                        <?php echo count_no_po_cat($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link1; ?>" target="_blank">
                        <?php echo sum_no_po_cat($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link1; ?>" target="_blank">
                        <?php echo without_basis_count($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link1; ?>" target="_blank">
                        <?php echo saving_lp_cp($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link1; ?>" target="_blank">
                        <?php echo saving_costing_cp($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link1; ?>" target="_blank">
                        <?php echo loss_lp_cp($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link1; ?>" target="_blank">
                        <?php echo loss_costing_cp($po_type,$category,$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <?php
                        $net_saving = saving_lp_cp($po_type,$category,$lsmsd,$lsmed)+saving_costing_cp($po_type,$category,$lsmsd,$lsmed);
                        $net_loss   = loss_lp_cp($po_type,$category,$lsmsd,$lsmed)+loss_costing_cp($po_type,$category,$lsmsd,$lsmed);
                        
                        echo $tot_saving_loss = $net_saving-$net_loss;
                    ?>
                </td>
                <!--- last month ----->
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link2; ?>" target="_blank">
                        <?php echo count_no_po_cat($po_type,$category,$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link2; ?>" target="_blank">
                        <?php echo sum_no_po_cat($po_type,$category,$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link2; ?>" target="_blank">
                        <?php echo without_basis_count($po_type,$category,$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link2; ?>" target="_blank">
                <?php echo saving_lp_cp($po_type,$category,$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link2; ?>" target="_blank">
                <?php echo saving_costing_cp($po_type,$category,$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link2; ?>" target="_blank">
                <?php echo loss_lp_cp($po_type,$category,$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link2; ?>" target="_blank">
                <?php echo loss_costing_cp($po_type,$category,$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                    <?php
                        $net_saving = saving_lp_cp($po_type,$category,$lfmsd,$lfmed)+saving_costing_cp($po_type,$category,$lfmsd,$lfmed);
                        $net_loss   = loss_lp_cp($po_type,$category,$lfmsd,$lfmed)+loss_costing_cp($po_type,$category,$lfmsd,$lfmed);
                        
                        echo $tot_saving_loss = $net_saving-$net_loss;
                    ?>
                </td>
                <td><b><?php echo $category; ?></b></td>
                <!--- Current Month ----->
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link3; ?>" target="_blank">
                        <?php echo count_no_po_cat($po_type,$category,$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link3; ?>" target="_blank">
                        <?php echo sum_no_po_cat($po_type,$category,$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link3; ?>" target="_blank">
                        <?php echo without_basis_count($po_type,$category,$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link3; ?>" target="_blank">
                <?php echo saving_lp_cp($po_type,$category,$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link3; ?>" target="_blank">
                <?php echo saving_costing_cp($po_type,$category,$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link3; ?>" target="_blank">
                <?php echo loss_lp_cp($po_type,$category,$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link3; ?>" target="_blank">
                <?php echo loss_costing_cp($po_type,$category,$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                    <?php
                        $net_saving = saving_lp_cp($po_type,$category,$cmsd,$cmed)+saving_costing_cp($po_type,$category,$cmsd,$cmed);
                        $net_loss   = loss_lp_cp($po_type,$category,$cmsd,$cmed)+loss_costing_cp($po_type,$category,$cmsd,$cmed);
                        
                        echo $tot_saving_loss = $net_saving-$net_loss;
                    ?>
                </td>
            </tr>
            <?php } ?>
            <!------ Totals -------------->
            <tr>
                <!--- second last month ----->
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link4; ?>" target="_blank">
                        <?php echo count_no_po_cat($po_type,'total',$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link4; ?>" target="_blank">
                        <?php echo sum_no_po_cat($po_type,'total',$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link4; ?>" target="_blank">
                        <?php echo without_basis_count($po_type,'total',$lsmsd,$lsmed); ?>
                    </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link4; ?>" target="_blank">
                <?php echo saving_lp_cp($po_type,'total',$lsmsd,$lsmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link4; ?>" target="_blank">
                <?php echo saving_costing_cp('total',$lsmsd,$lsmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link4; ?>" target="_blank">
                <?php echo loss_lp_cp($po_type,'total',$lsmsd,$lsmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link4; ?>" target="_blank">
                <?php echo loss_costing_cp($po_type,'total',$lsmsd,$lsmed); ?>
                </a>
                </td>
                <td>
                    <?php
                        $net_saving = saving_lp_cp($po_type,'total',$lsmsd,$lsmed)+saving_costing_cp($po_type,'total',$lsmsd,$lsmed);
                        $net_loss   = loss_lp_cp($po_type,'total',$lsmsd,$lsmed)+loss_costing_cp($po_type,'total',$lsmsd,$lsmed);
                        
                        echo $tot_saving_loss = $net_saving-$net_loss;
                    ?>
                </td>
                <!--- last month ----->
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link5; ?>" target="_blank">
                        <?php echo count_no_po_cat($po_type,'total',$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link5; ?>" target="_blank">
                        <?php echo sum_no_po_cat($po_type,'total',$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link5; ?>" target="_blank">
                        <?php echo without_basis_count($po_type,'total',$lfmsd,$lfmed); ?>
                    </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link5; ?>" target="_blank">
                <?php echo saving_lp_cp($po_type,'total',$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link5; ?>" target="_blank">
                <?php echo saving_costing_cp($po_type,'total',$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link5; ?>" target="_blank">
                <?php echo loss_lp_cp($po_type,'total',$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link5; ?>" target="_blank">
                <?php echo loss_costing_cp($po_type,'total',$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                    <?php
                        $net_saving = saving_lp_cp('total',$lfmsd,$lfmed)+saving_costing_cp('total',$lfmsd,$lfmed);
                        $net_loss   = loss_lp_cp($po_type,'total',$lfmsd,$lfmed)+loss_costing_cp('total',$lfmsd,$lfmed);
                        
                        echo $tot_saving_loss = $net_saving-$net_loss;
                    ?>
                </td>
                <td style="text-align:center; background-color:#999"><b>TOTAL</b></td>
                <!--- current month ----->
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=count_no_po_cat_det<?php echo $link6; ?>" target="_blank">
                        <?php echo count_no_po_cat($po_type,'total',$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=sum_no_po_cat_det<?php echo $link6; ?>" target="_blank">
                        <?php echo sum_no_po_cat($po_type,'total',$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=without_basis_count_det<?php echo $link6; ?>" target="_blank">
                        <?php echo without_basis_count($po_type,'total',$cmsd,$cmed); ?>
                    </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_lp_cp_det<?php echo $link6; ?>" target="_blank">
                <?php echo saving_lp_cp($po_type,'total',$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=saving_costing_cp_det<?php echo $link6; ?>" target="_blank">
                <?php echo saving_costing_cp($po_type,'total',$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_lp_cp_det<?php echo $link6; ?>" target="_blank">
                <?php echo loss_lp_cp($po_type,'total',$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/pr_cycletimec/pr_report_det/?fun=loss_costing_cp_det<?php echo $link6; ?>" target="_blank">
                <?php echo loss_costing_cp($po_type,'total',$cmsd,$cmed); ?>
                </a>
                </td>
                <td>
                    <?php
                        $net_saving = saving_lp_cp($po_type,'total',$cmsd,$cmed)+saving_costing_cp($po_type,'total',$cmsd,$cmed);
                        $net_loss   = loss_lp_cp($po_type,'total',$cmsd,$cmed)+loss_costing_cp($po_type,'total',$cmsd,$cmed);
                        
                        echo $tot_saving_loss = $net_saving-$net_loss;
                    ?>
                </td>
            </tr>
        </table>
    </div>
</div><br><?php */?>
<!----Response Div Ends ---->