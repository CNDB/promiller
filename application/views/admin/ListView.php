<?php include'header.php'; ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>User List</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>User List</li>						  	
            </ol>
        </div> 
    </div>

    <div class="row">
        <div class="col-lg-10"></div>
        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/dbuserc/index?id=">
                <input type="button" class="form-control" id="add_user" name="add_user" value="Add New User">
            </a>
        </div>
    </div><br>

    <div class="row">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>SNO.</th>
                    <?php 
                        $column_nm_arr = array();
                        foreach($ViewHead->result() as $row){
                            $column_name = $row->column_name;
                            array_push($column_nm_arr,$column_name)
                    ?>
                    <th><?php echo $column_name; ?></th>
                    <?php } ?>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    $count_arr = count($column_nm_arr);
                    $sno=0;
                    foreach($ViewList->result() as $row){
                        $sno++;
                        $id = $row->id;
                        $email = $row->email;
                        $password = $row->password;
                        $admin_pass = $row->admin_pass;
                        $username = $row->username;
                        $name = $row->name;
                        $dob = $row->dob;
                        $mob_no = $row->mob_no;
                        $role = $row->role;
                        $emp_active = $row->emp_active;
                        $date_time = $row->date_time;
                ?>
                <tr>
                    <?php
                        //for($i = 0; $i<$count_arr; $i++ ){
                            //echo $column_nm_arr[$i];
                    ?>
                    <td><?php echo $sno; ?></td>
                    <td><?php echo $id; ?></td>
                    <td><?php echo $email; ?></td>
                    <td><?php echo $password; ?></td>
                    <td><?php echo $admin_pass; ?></td>
                    <td><?php echo $username; ?></td>
                    <td><?php echo $name; ?></td>
                    <td><?php echo $dob; ?></td>
                    <td><?php echo $mob_no; ?></td>
                    <td><?php echo $role; ?></td>
                    <td><?php echo $emp_active; ?></td>
                    <td><?php echo $date_time; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/dbuserc/index?id=<?php echo $id; ?>">
                            <i class="fa fa-pencil"></i>
                            Edit
                        </a>
                    </td>
                    <?php
                        //}
                    ?>
                </tr>
                <?php } ?>
            </tbody>
        </table>  
    </div><br /><br />
        
  </section>
</section>
<?php include('footer.php'); ?>