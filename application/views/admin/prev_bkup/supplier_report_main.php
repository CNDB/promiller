<?php include'header.php'; ?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Supplier Analysis Report</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-3"></div>
    	<div class="col-lg-2"><b>Select Category : </b></div>
        <div class="col-lg-2">
        	<select id="category" name="category" class="form-control">
            	<option value="">--Select--</option>
                <option value="All">All</option>
                <?php
					foreach($category->result() as $row){
						$live_category = $row->live_category; 
				?>
                <option value="<?php echo $live_category; ?>"><?php echo $live_category; ?></option>
                <?php
					}
				?>
            </select>
        </div>
        <div class="col-lg-2"><input type="button" name="sort" id="sort" value="Sort" class="form-control" onClick="filter()"></div>
        <div class="col-lg-3"></div>
    </div><br>
        
    <div class="row">
    	<div class="col-lg-12" id="ajax_div">
        	<table class="table table-bordered">
            	<thead>
                	<tr>
                    	<th><b>SNO</b></th>
                        <th><b>SUPPLIER CODE</b></th>
                        <th><b>SUPPLIER NAME</b></th>
                        <th><b>STATE</b></th>
                        <th><b>NO. OF OPEN PO</b></th>
                        <th><b>VALUE OF OPEN PO</b></th>
                        <th><b>NO. OF PO RELEASED IN LAST 6 MONTHS</b></th>
                        <th><b>VALUE OF PO PURCHASED IN LAST 6 MONTHS</b></th>
                        <th><b>LAST PAYMENT TERM</b></th>
                        <th><b>CURRENT PAYMENT TERM</b></th>
                        <th><b>LAST FREIGHT TERM</b></th>
                        <th><b>CURRENT FREIGHT TERM</b></th>
                        <th><b>CATEGORIES</b></th>
                        <th><b>SUPPLIER EMAIL</b></th>
                        <th><b>SUPPLIER PHONE</b></th>
                        <th><b>CONTACT PERSON</b></th>
                    </tr>
                </thead>
                <tbody>
                <?php
					$sno = 0;
					foreach($all_supplier_data->result() as $row){
						$sno++;
						$supp_code = $row->supp_code;
						$supp_name = $row->supp_name;
						$supp_state = $row->supp_state;
						$po_open = $row->po_open;
						$po_open_value = $row->po_open_value;
						$released_po_last6_month = $row->released_po_last6_month;
						$purchase_po_last6_month_val = $row->purchase_po_last6_month_val;
						$last_payment_term = $row->last_payment_term;
						$current_payment_term = $row->current_payment_term;
						$last_frt_term = $row->last_frt_term;
						$current_frt_term = $row->current_frt_term;
						$category = $row->category;
						$supplier_email = $row->supp_email;
						$supplier_phone = $row->supp_phone;
						$contact_person = $row->contact_person;
				?>
                	<tr>
                    	<td><?php echo $sno; ?></td>
                        <td><?php echo $supp_code; ?></td>
                        <td><?php echo $supp_name; ?></td>
                        <td><?php echo $supp_state; ?></td>
                        <td><?php echo $po_open; ?></td>
                        <td><?php echo number_format($po_open_value,2); ?></td>
                        <td><?php echo $released_po_last6_month; ?></td>
                        <td><?php echo number_format($purchase_po_last6_month_val,2); ?></td>
                        <td><?php echo $last_payment_term; ?></td>
                        <td><?php echo $current_payment_term; ?></td>
                        <td><?php echo $last_frt_term; ?></td>
                        <td><?php echo $current_frt_term; ?></td>
                        <td><?php echo $category; ?></td>
                        <td><?php echo $supplier_email; ?></td>
                        <td><?php echo $supplier_phone; ?></td>
                        <td><?php echo $contact_person; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
  </section>
</section>

<?php include('footer.php'); ?>

<script>

function filter(){
	var category = document.getElementById("category").value;
	
	if(category == ''){
		alert("Please Select Category");
		document.getElementById("category").focus;
		return false;
	}
	
	$("#ajax_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ajax_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?category="+category;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/supplier_reportc/supp_ajax"+ queryString, true);
	xmlhttp.send();
	
}

</script>
