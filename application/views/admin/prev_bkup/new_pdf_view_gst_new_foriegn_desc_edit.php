<?php
ob_start();

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//Set Fonts
$pdf->SetFont('dejavusans', '', 10);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// Add a page
$pdf->AddPage();

$html = '<div>
		   <h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px"><u>PURCHASE ORDER</u></h4>
	     </div>'; 

$html .='<table width="100%" cellpadding="4px" cellspacing="0" style="font-size:6px;">
			<tr style="background-color:#CCC;">
				<td width="50%" style="border:solid 1px black; text-align:center;"><b>SUPPLIER DETAILS</b></td>
				<td width="50%" style="border:solid 1px black; text-align:center;"><b>BUYER DETAILS</b></td>
			</tr>
            <tr>
                <td width="50%" style="border:solid 1px black; padding: 2px;">';

                foreach ($view_po_pdf->result() as $row){
							$po_no = $row->pomas_pono;
							$supp_name = $row->supp_spmn_supname;
							$supp_add1 = $row->supp_addr_address1;
							$supp_add2 = $row->supp_addr_address2;
							$supp_add3 = $row->supp_addr_address3;
							$comp_add1 = $supp_add1."&nbsp;".$supp_add2."&nbsp;".$supp_add3;
							$comp_add = mb_convert_encoding($comp_add1, "ISO-8859-1", "UTF-8");
							$supp_addr_city = $row->supp_addr_city;
							$supp_addr_state = $row->supp_addr_statedesc;
							$supp_addr_zip = $row->supp_addr_zip;
							$supp_spmn_supcode = $row->supp_spmn_supcode;
							$po_supp_email = $row->supp_addr_email;
							$supp_addr_contperson = $row->supp_addr_contperson;
							$supplier_phone = $row->supp_addr_phone;
							
							//mail id is not avalable in erp
							if($po_supp_email == '' || is_null($po_supp_email) == TRUE ){
								$sql_supp_live_det = "select top 1 * from TIPLDB..insert_po where po_num = '$po_no'";
								$query_supp_live_det = $this->db->query($sql_supp_live_det);
								
								foreach ($query_supp_live_det->result() as $row) {
								  $po_supp_email = $row->po_supp_email;
								}
							}
							
							if($supp_addr_contperson == '' || is_null($supp_addr_contperson) == TRUE ){
								$sql_supp_live_det = "select top 1 * from TIPLDB..insert_po where po_num = '$po_no'";
								$query_supp_live_det = $this->db->query($sql_supp_live_det);
								
								foreach ($query_supp_live_det->result() as $row) {
								  $supp_addr_contperson = $row->contact_person;
								}
							}
							
 $html .= '          <table widht="100%" height="150px">
                        <tr height="10px">
                            <td width="30%"><b>VENDOR NAME:</b></td>
                            <td width="70%">'.mb_convert_encoding($supp_name, "ISO-8859-1", "UTF-8").'</td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>ADDRESS:</b></td>
                            <td width="70%">'.$comp_add.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
							<td width="30%"><b>CITY:</b></td>
                            <td width="70%">'.$supp_addr_city.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
						<tr height="10px">
                            <td width="30%"><b>CONTACT PERSON:</b></td>
                            <td width="70%">'.$supp_addr_contperson.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
						<tr height="10px">
                            <td width="30%"><b>PHONE:</b></td>
                            <td width="70%">'.$supplier_phone.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
						<tr height="10px">
                            <td width="30%"><b>EMAIL:</b></td>
                            <td width="70%">'.$po_supp_email.'</td>
                        </tr >
                        <tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>
                <td width="50%" style="border:solid 1px black; padding: 2px;">';
                
					foreach ($view_po_pdf->result() as $row){ 
						    $po_num = $row->pomas_pono;
							$po_type = $row->pomas_potype;
							$po_amend_no = $row->pomas_poamendmentno;
							$po_date = $row->pomas_podate;
							$po_freight = $row->paytm_incoterm;
							$po_freight_place = $row->paytm_incoplace;
							
							if($po_freight == 'FORD'){
								$po_freight_new = $po_freight." ,TIPL Ajmer";
							} else if($po_freight == 'FOR'){
								$po_freight_new = $po_freight." ,Ajmer";
							} else if($po_freight == 'EXW'){
								$po_freight_new = $po_freight." (EX-WORKS)";
							} else {
								$po_freight_new = $po_freight;
							}
							
							$po_payterm = $row->paytm_payterm;
							$trans_mode = $row->paytm_transmode;
				
$html .= '      	<table width="100%" height="150px" >
                        <tr height="10px">
                            <td width="30%"><b>PO NO:</b></td>
                            <td width="70%">'.$po_num.'</td>
                        </tr>
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>PO REV/AMEND:</b></td>
                            <td width="70%">'.$po_amend_no.'</td>
                        </tr>
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>PO DATE:</b></td>
                            <td width="70%">'.substr($po_date,0,11).'</td>
                        </tr>
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>IEC CODE:</b></td>
                            <td width="70%">1388001870</td>
                        </tr>
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>GST NO:</b></td>
                            <td width="70%">08AAACT1582B1ZO</td>
                        </tr>
						
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>BUYER NAME:</b></td>
                            <td width="70%">Sandeep Tak</td>
                        </tr>
						
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>PHONE:</b></td>
                            <td width="70%">+91 9352559002</td>
                        </tr>
						
						<tr height="10px">
                            <td width="30%"></td>
                            <td width="70%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="30%"><b>EMAIL:</b></td>
                            <td width="70%">sandeep.tak@tipl.com</td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>
            </tr>
			</table>';
			
			//Currency Selection Display
			$sql_currency ="select * from tipldb..po_master_table where po_num = '$po_num'";
			$query_currency = $this->db->query($sql_currency);
			
			foreach ($query_currency->result() as $row){
				$currency = $row->currency;
				$payment_fpo = $row->payment_fpo;
				$delivery_fpo = $row->delivery_fpo;
			}
			
			if($currency == 'USD'){
				$currency_symbol = "&#36;";
			} else if($currency == 'EURO'){
				$currency_symbol = "&euro;";
			} else if($currency == 'GBP'){
				$currency_symbol = "&#163;";
			} else if($currency == 'YEN'){
				$currency_symbol = "&#165;";
			} else {
				$currency_symbol = "&#x20B9;";
			}
			//Currency Selection Display
			
 $html .= '  <div>
		   		<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">ITEM DETAILS</h4>
	     	</div>
            
			<table width="100%" height="auto" cellpadding="1px" cellspacing="0" style="font-size:6px;">         
                        <tr style="background-color:#CCC">
                            <td style="border:solid 1px black; padding: 2px;"><b>SN</b></td>
                            <td style="border:solid 1px black; padding: 2px;" colspan="4"><b>ITEM CODE & DESCRIPTION</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>SUPPLIER ITEM CODE</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>MATERIAL NEED DATE<br>(YYYY-MM-DD)</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>QTY</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>RATE('.$currency_symbol.')</b></td>
                            <td style="border:solid 1px black; padding: 2px;"><b>UOM</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>VALUE BASIC('.$currency_symbol.')</b></td>
                        </tr>';
						
                        	$po_s_no = 1;
							$total_value1 = 0;
							foreach ($view_po_pdf_item->result() as $row){
								$po_num = $row->pomas_pono;
								$po_line_no = $row->poitm_polineno;
								$item_code =  $row->poitm_itemcode;
								$need_date1 = substr($row->poitm_needdate,0,11);
								$need_date = date("d-m-Y", strtotime($need_date1));
								$qty = $row->poitm_order_quantity;
								$uom = $row->poitm_puom;
								$unit_rate1 = $row->poitm_po_cost;
								$need_date = $row->poitm_needdate;
								//$item_value = $row->poitm_itemvalue;
								
								$sql_live_rate = "select current_price,supp_item_code from tipldb..po_master_table a, tipldb..insert_po b where a.po_num = b.po_num and a.po_num = '$po_num' and b.po_item_code = '$item_code'";
								
								$qry_live_rate = $this->db->query($sql_live_rate);
								
								if ($qry_live_rate->num_rows() > 0) {
									foreach($qry_live_rate->result() as $row){
										$unit_rate = $row->current_price;
										$supp_item_code = $row->supp_item_code;
									}
								} else {
									$unit_rate = $unit_rate1;
									$supp_item_code = "";
								}
								
								$item_value = $qty*$unit_rate;
								
								$sql_item_desc ="select * from tipldb..po_item_desc_edit_table 
								where item_code = '$item_code' and po_num = '$po_num' 
								and sno = (select max(sno) from tipldb..po_item_desc_edit_table 
								where item_code = '$item_code' and po_num = '$po_num')";
								
								$query_item_desc = $this->db->query($sql_item_desc);
								
								foreach ($query_item_desc->result() as $row) {
								  	$item_desc1 = $row->item_description;
									$item_desc1 = str_replace("<","Less Than",$item_desc1);
									$item_desc1 = str_replace(">","Greater Than",$item_desc1);
									$item_desc1 = str_replace("'","",$item_desc1);
									$item_desc = mb_convert_encoding($item_desc1, "ISO-8859-1", "UTF-8");
								}
								
								$total_basic_value = $total_basic_value+$item_value;
						
$html .= '              <tr>
                            <td style="border:solid 1px black; padding: 2px;">'.$po_s_no.'</td>
                            <td style="border:solid 1px black; padding: 2px; font-size:6px;" colspan="4"><b>'.$item_code.'</b><br><br>'.$item_desc.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.$supp_item_code.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.substr($need_date,0,11).'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($qty,2,'.','').'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($unit_rate,2,'.','').'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.$uom.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($item_value,2,'.','').'</td>
                        </tr>';
							$po_s_no++;}
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding:2px;"><b>TOTAL('.$currency_symbol.')</b></td>
							<td style="border:solid 1px black; padding:2px;"><b>'.number_format($total_basic_value,2,'.','').'</b></td>	
                       </tr>';
					   //Charges Document Level
						$tcd_amount_ch_tot = 0;
						foreach ($doc_lvl_charges->result() as $row){
							$tcd_amount_ch = $row->tcdamount;
							$tcd_desc_ch   = $row->tcdcodedesc;
							
							$tcd_amount_ch_tot = $tcd_amount_ch_tot+$tcd_amount_ch;
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>'.$tcd_desc_ch.'('.$currency_symbol.')</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;"><b>'.number_format($tcd_amount_ch,2,'.','').'</b></td>
                        </tr>';
						}
//Calculation Of GRAND TOTAL
						$grand_total = ($total_basic_value+$tcd_amount_ch_tot);
						//$grand_total1 = round($grand_total);
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>GRAND TOTAL('.$currency_symbol.')</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;"><b>'.number_format($grand_total,2,'.','').'</b></td>
                        </tr>';
						
						//Coverting Grand Total Into Words
						   $number = $grand_total;
						   $no = round($number,2);
						   $point = round($number - $no) * 100;
						   $hundred = null;
						   $digits_1 = strlen($no);
						   $i = 0;
						   $str = array();
						   $words = array(
							'0'  => '', 
							'1'  => 'One', 
							'2'  => 'Two',
							'3'  => 'Three', 
							'4'  => 'Four', 
							'5'  => 'Five', 
							'6'  => 'Six',
							'7'  => 'Seven', 
							'8'  => 'Eight', 
							'9'  => 'Nine',
							'10' => 'Ten', 
							'11' => 'Eleven', 
							'12' => 'Twelve',
							'13' => 'Thirteen', 
							'14' => 'Fourteen',
							'15' => 'Fifteen', 
							'16' => 'Sixteen', 
							'17' => 'Seventeen',
							'18' => 'Eighteen', 
							'19' => 'Nineteen', 
							'20' => 'Twenty',
							'30' => 'Thirty', 
							'40' => 'Forty', 
							'50' => 'Fifty',
							'60' => 'Sixty', 
							'70' => 'Seventy',
							'80' => 'Eighty', 
							'90' => 'Ninety');
						   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
						   while ($i < $digits_1) {
							 $divider = ($i == 2) ? 10 : 100;
							 $number = floor($no % $divider);
							 $no = floor($no / $divider);
							 $i += ($divider == 10) ? 1 : 2;
							 if ($number) {
								$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
								$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
								$str [] = ($number < 21) ? $words[$number] .
									" " . $digits[$counter] . $plural . " " . $hundred
									:
									$words[floor($number / 10) * 10]
									. " " . $words[$number % 10] . " "
									. $digits[$counter] . $plural . " " . $hundred;
							 } else $str[] = null;
						  }
						  $str = array_reverse($str);
						  $result = implode('', $str);
						  $points = ($point) ?
							"." . $words[$point / 10] . " " . 
								  $words[$point = $point % 10] : '';
						  $result . "Rupees  " . $points . " Paise";
						  
						//Coverting Grand Total Into Words
						
$html .= '              <tr>
                            <td colspan="11" style="border:solid 1px black; padding: 2px;">
								<b>AMOUNT IN WORDS('.$currency_symbol.') : '.$result ." ".$points." Only".'</b>
							</td>
                        </tr>';						
												
$html .= '			</table><br>';
					
$html .= '		
				<div><h3>Terms & Conditions</h3></div>  
				<table width="100%" height="auto" cellpadding="3px" cellspacing="0" style="font-size:6px; line-height:8px" border="1px">
					<tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;"><b>1. Incoterms :</b></td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">'.$po_freight.','.$po_freight_place.'</td>
					</tr>
					<tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;"><b>2. Shipment Through :</b></td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">'.$trans_mode.'</td>
					</tr>
					<tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;"><b>3. Payment Terms :</b></td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">'.$payment_fpo.'</td>
					</tr>
					<tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;"><b>4. Delivery Date :</b></td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">'.$delivery_fpo.'</td>
					</tr>
					<tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;"><b>5. Packing List :</b></td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">
						<p>Draft/estimated packing list after placing the purchase order is mandatorily require for arranging the Freight
							forwarder / courier company pick up. </p>
						</td>
					</tr>
					<tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;"><b>6. Documentation:</b></td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">
							<p style="text-align:justify">
							i)Commercial Invoice, Packing List & Test certificates/Certificate of confirmation (COC) are mandatorily 
							require along with all the shipments.Please send the commercial invoice, air waybill & packing list for approval to us 
							before dispatch of the material. All documents must be in English language.<br><br>  
							ii)The shipper name,shipper address, buyer name, buyer address, value,quantity, etc. mentioned on your Commercial invoice & 
							the Airway bill must totally match with the Proforma invoice sent to us for payment/ or as per our Purchase order.<br><br> 
							iii)Our approved commercial invoice, air waybill only need to be handover to our Freight forwarder / Courier company. 
							Once we approve the documents, do not change the detail on your commercial invoice / Air Waybill while handing over the 
							material to our freight forwarder / courier company.<br><br>  
							iv)If INCOTERM / Freight term is Ex Works/FOB/FCA, do not handover the material to any freight forwarder/courier 
							company unless we confirm you the name of the same.<br><br>  
							v) In case if any of the above mentioned terms & conditions 
							are not followed by you, all the losses to us if happen due to this will be recovered from your payment or will be adjusted 
							from your due / subsequent orders payment.
							</p>
						</td>
					</tr>';
					
					$sql_add_terms = "select * from tipldb..fpo_add_term where po_num = '$po_num'";
					$qry_add_terms = $this->db->query($sql_add_terms);
					
					if($qry_add_terms->num_rows() > 0){
					
					
$html .='		   <tr>
						<td width="20%" style="border:solid 1px black; padding: 2px;">7. Other Terms &amp; Conditions</td>
						<td width="80%" style="border:solid 1px black; padding: 2px;">';
					
							$sql_add_terms = "select * from tipldb..fpo_add_term where po_num = '$po_num'";
							$qry_add_terms = $this->db->query($sql_add_terms);
							
							$i=0;
							foreach($qry_add_terms->result() as $row){
								$i++;
								$fpo_add_term = $row->fpo_add_term;
								
$html .='		    <b>12.'.$i.').</b> '.$fpo_add_term.'<br>';
							}					
$html .='				</td>
				   </tr>';
				   
					}
				
$html .='	  </table><br><br>';					
					
					

$html .='			<div style="font-size:8px; width:100%; margin-left:10px">
						<b>For Toshniwal Industries Pvt. Ltd.</b><br><br>
						<b>Sandeep Tak</b><br>
					</div>';
					

//echo $html; 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('uploads/pdf/'.$po_no.'.pdf', 'FI');
?>