<!---- PO Header --->

<div class="row">
<div class="col-lg-4">
<h4>PO No.  <?php echo $nik; ?></h4>
<?php $po_first_three = substr($nik,0,3); ?>
</div>
<div class="col-lg-2">
    <form action="" method="post" role="form">
        <select name="selectpr" id="selectpr" class="form-control" onChange="check(this.value,'from_pr');" style="display:none;">
            <?php if ($nik != '') { ?>
            <option selected="selected" value="<?php echo $nik; ?>" disabled="disabled"><?php echo $nik; ?></option> 
            <?	} else {  ?>
            <option value="">--Select--</option>
            <?php foreach ($h->result() as $row){ $pono = $row->pomas_pono; ?> 
            <option  value="<?php echo $pono; ?>"><?php echo $pono; ?></option>  
            <?php }} ?>
        </select>
    </form>
</div>
<div class="col-lg-6">
<table border="1" align="center" class="table table-bordered" style="font-size:9px">
    <tr>
        <td><b>CONDITION</b></td>
        <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
        <td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
        <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
        <td>FIRST TIME PURCHASE</td>
    </tr>
    <tr>
        <td><b>COLOR</b></td>
        <td style="background-color:red;"></td>
        <td style="background-color:green;"></td>
        <td style="background-color:blue;"></td>
        <td style="background-color:yellow;"></td>
    </tr>
</table>
</div>
</div>
</div><br />
<div class="row">
<div class="col-lg-2">
<?php if($po_first_three == 'FPO'){ ?>

       <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst_foreign/<?php echo $nik; ?>" 
       target="_blank" style="font-size:16px; font-weight:bold;">
       <button type="button" class="form-control">PRINT PO</button></a>
<?php } else { ?>
		<a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $nik; ?>" 
       target="_blank" style="font-size:16px; font-weight:bold;">
       <button type="button" class="form-control">PRINT PO</button></a>
<?php } ?>
 </div>
</div><br />            
<div id="detail"></div>

<!-- PO Header Ends ---->