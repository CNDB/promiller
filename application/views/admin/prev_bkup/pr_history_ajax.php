<?php

$from_date = $_GET['from_date'];
$to_date = $_GET['to_date'];
$usage_type = $_GET['usage_type'];


//Queries 

if( $from_date != '' && $to_date != '' && $usage_type ==''){
	
	$sql_main = "select *,datediff(DAY, pr_date,getdate()) as diff from TIPLDB..pr_submit_table 
where convert(date,pr_date) BETWEEN '$from_date' and '$to_date' order by pr_date desc";
	
	$query_main = $this->db->query($sql_main);
	
} else if( $from_date != '' && $to_date != '' && $usage_type !=''){
	
	$sql_main = "select *,datediff(DAY, pr_date,getdate()) as diff from TIPLDB..pr_submit_table 
where convert(date,pr_date) BETWEEN '$from_date' and '$to_date' and usage = '$usage_type' order by pr_date desc";
	
	$query_main = $this->db->query($sql_main);
	
} else if( $from_date == '' && $to_date == '' && $usage_type != ''){
	
	$sql_main = "select *,datediff(DAY, pr_date,getdate()) as diff from TIPLDB..pr_submit_table 
where usage = '$usage_type' order by pr_date desc";
	
	$query_main = $this->db->query($sql_main);
	
} else {
	
	$sql_main = "select top 100 *,datediff(DAY, pr_date,getdate()) as diff from TIPLDB..pr_submit_table order by pr_date desc";
	
	$query_main = $this->db->query($sql_main);
	
}

?>

<table cellpadding="0" cellspacing="0" align="center" class="table table-borderd" width="600px" id="myTable" style="display:block; font-size:11px">
    <tr style="background-color:#0CF; font-weight:bold;">
        <td>S.NO.</td>
        <td>PR NUMBER</td>
        <td>ITEM CODE</td>
        <td>ITEM DESCRIPTION</td>
        <td>USAGE TYPE</td>
        <td>WHY SPECIAL RMKS</td>
        <td>CATEGORY</td>
        <td>PROJECT NAME</td>
        <td>PR QUANTITY</td>
        <td>UOM</td>
        <td>COSTING</td>
        <td>ATAC NO</td>
        <td>PR DATE</td>
        <td>NEED DATE</td>
        <td>PR AGE</td>
        <td>LIVE CREATED BY</td>
        <td>LVL1 APPROVAL BY</td>
        <td>LVL1 APPROVAL DATE</td>
        <td>LVL2 APPROVAL BY</td>
        <td>LVL2 APPROVAL DATE</td>
        <td>LIVE STATUS</td>
        <td>ERP STATUS</td>
    </tr>
    <?php
        $sno = 0;
        foreach ($query_main->result() as $row){
            $sno++;
            $pr_num = $row->pr_num;
            $item_code = $row->item_code;
            
            $item_code1 = urlencode($item_code);
    
            if(strpos($item_code1, '%2F') !== false)
            {
                $item_code2 = str_replace("%2F","chandra",$item_code1);
            }
            else 
            {
                $item_code2 = $item_code1;
            }
            
            $item_desc = $row->itm_desc;
            $item_desc_new = mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");
            $usage_type = $row->usage;
            $category = $row->category;
            $project_name = $row->project_name;
            $pr_qty = number_format($row->required_qty,2);
            $uom = $row->trans_uom;
            $costing = $row->costing;
            $pr_date = $row->pr_date;
            $need_date = $row->need_date;
            $pr_age = $row->diff;
            $live_created_by = $row->created_by;
            $pr_status = $row->pr_status;
            $why_spcl_rmks = $row->why_spcl_rmks;
            $atac_no = $row->atac_no;
            
            $lvl1_approval_by = $row->pr_approval_by;
            $pr_approval_inst = $row->pr_approval_inst;
            $lvl1_approval_date = $row->pr_approval_date;
            
            if($pr_approval_inst == 'DISAPPROVE'){
                $lvl1_approval_by = "";
                $lvl1_approval_date = "";
            }
            
            $pr_approval_by_lvl2 = $row->pr_approval_by_lvl2;
            $pr_approval_inst_lvl2 = $row->pr_approval_inst_lvl2;
            $pr_approval_date_lvl2 = $row->pr_approval_date_lvl2;
            
            if($pr_approval_inst_lvl2 == 'DISAPPROVE'){
                $pr_approval_by_lvl2 = "";
                $pr_approval_date_lvl2 = "";
            }
            
            $sql_erp = "select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'";
            $query_erp = $this->db->query($sql_erp);
            
            foreach ($query_erp->result() as $row){
                $erp_status = $row->preqm_status;
            }
            
            if($erp_status == 'DR'){
                $erp_status1 = 'DRAFT';
            } else if($erp_status == 'HD'){
                $erp_status1 = 'HOLD';
            } else if($erp_status == 'FR'){
                $erp_status1 = 'FRESH';
            } else if($erp_status == 'CA'){
                $erp_status1 = 'CANCELLED';
            } else if($erp_status == 'DE'){
                $erp_status1 = 'DELETED';
            } else if($erp_status == 'AU'){
                $erp_status1 = 'AUTHORIZED';
            } else {
                $erp_status1 = $erp_status;
            }
    ?>
    <tr>
        <td><?php echo $sno; ?></td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo $pr_num; ?>"><?php echo $pr_num; ?></a>
            <input type="hidden" name="pr_num" id="pr_num" value="<?php echo $pr_num; ?>">
        </td>
        <td><a href="<?php echo base_url(); ?>index.php//createpoc/pendal_view/<?php echo $item_code2; ?>"><?php echo $item_code; ?></a></td>
        <td><?php echo $item_desc_new; ?></td>
        <td><?php echo $usage_type; ?></td>
        <td><?php echo $why_spcl_rmks; ?></td>
        <td><?php echo $category; ?></td>
        <td><?php echo $project_name; ?></td>
        <td><?php echo $pr_qty; ?></td>
        <td><?php echo $uom; ?></td>
        <td><?php echo $costing; ?></td>
        <td>
            <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                <?php echo $atac_no; ?>
            </a>
        </td>
        <td><?php echo substr($pr_date,0,11); ?></td>
        <td><?php echo substr($need_date,0,11); ?></td>
        <td><?php echo $pr_age; ?></td>
        <td><?php echo $live_created_by; ?></td>
        
        <td><?php echo $lvl1_approval_by; ?></td>
        <td><?php echo substr($lvl1_approval_date,0,11); ?></td>
        <td><?php echo $pr_approval_by_lvl2; ?></td>
        <td><?php echo substr($pr_approval_date_lvl2,0,11); ?></td>
        
        <td><?php echo $pr_status; ?></td>
        <td><?php echo $erp_status1; ?></td>
    </tr>
    <?php
        }
    ?>
</table>