<?php 
	include'header.php'; 
	
	$po_num = $this->uri->segment(3);
	$nik = $this->uri->segment(3);
?>
<!--main content start-->
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Lead Time Update</h4>
        </div>
    </div><br />
    
    <?php include('po_header.php'); ?>
    
    <form action="<?php echo base_url(); ?>index.php/lead_time_updatec/insert_lead_time_det" method="post">
    
    <?php include('po_details_div.php'); ?>
    
    <?php include('po_supplier_quotes.php'); ?>
    
    <!---- Lead Time Columns ----->
    <input type="hidden" id="po_num" name="po_num" value="<?php echo $po_num; ?>">
    <br>
    <div class="row">
    	<div class="col-lg-3"><b>Enter Expected Lead Time(Supplier):</b><b style="color:red; font-size:18px">*</b></div>
        <div class="col-lg-3"><input type="text" name="supp_lead_time" id="supp_lead_time" value="" class="form-control" required></div>
        <div class="col-lg-2"><b>Enter Remarks:</b></div>
        <div class="col-lg-4">
        	<textarea id="remarks" name="remarks" class="form-control"></textarea>
        </div>
    </div>
    
    <br>
    <div class="row">
    	<div class="col-lg-5"></div>
        <div class="col-lg-2"><input type="submit" name="submit" id="submit" value="Submit" class="form-control"></div>
        <div class="col-lg-5"></div>
    </div>
    
    <!---- Lead Time Columns End --->
    
    </form>
    
    <?php include('po_chat_history.php'); ?>
    
    <?php include('po_action_timing.php'); ?>
          
  </section>
</section>
</body>
<!-- -main content end -->
<?php include('footer.php'); ?>