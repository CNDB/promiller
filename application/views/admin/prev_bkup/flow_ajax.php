<!-- Ajax Javascripts  -->

<?php // Fresh Purchase Requests ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getfreshpr(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('fresh_pr').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('fresh_pr').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/fresh_pr", true);
	xmlhttp.send();
	
	}

</script>

<?php // Attach Drawing ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getattach_drawing(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('attach_drawing').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('attach_drawing').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/attach_drawing", true);
	xmlhttp.send();
	
	}

</script>

<?php // Authorize Purchase Request planning level1?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_pr(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_pr').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_pr').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_pr", true);
	xmlhttp.send();
	
	}

</script>

<?php // Disapproved Purchase Request planning level1?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pr_plan(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_pr_plan').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_pr_plan').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pr_plan", true);
	xmlhttp.send();
	
	}

</script>

<?php // Authorize Purchase Request planning level2?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_pr_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_pr_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_pr_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_pr_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php // Disapproved Purchase Request planning level2?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pr_plan_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_pr_plan_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_pr_plan_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pr_plan_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php // Live authorized pr pending for erp authorisation ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function geterp_not_auth_pr(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('erp_not_auth_pr').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('erp_not_auth_pr').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/erp_not_auth_pr", true);
	xmlhttp.send();
	
	}

</script>

<?php //Authorized Purchase Requests whose po is not created in erp ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getpr_po_report(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('pr_po_report').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('pr_po_report').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/pr_po_report", true);
	xmlhttp.send();
	
	}

</script>


<?php // Authorize Purchase Request Purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_pr_pur(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_pr_pur').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_pr_pur').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_pr_pur", true);
	xmlhttp.send();
	
	}

</script>

<?php //Disapproved Purchase Request Purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pr_pur(){
		
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		  
		xmlhttp.onreadystatechange=function(){
		
			if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('dis_pr_pur').innerHTML=xmlhttp.responseText;
			} else {
				document.getElementById('dis_pr_pur').innerHTML="<strong>Waiting For Server...</strong>";
			}
		}
		
		
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pr_pur", true);
		xmlhttp.send();
	
	}

</script>

<?php //Fresh ERP Purchase Orders pending for live po creation ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getfresh_po(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('fresh_po').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('fresh_po').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/fresh_po", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO authorization level 1 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_po_lvl1(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_po_lvl1').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_po_lvl1').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_po_lvl1", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO disapproval level1	 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_po_lvl1(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_po_lvl1').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_po_lvl1').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_po_lvl1", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO authorization level 2 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_po_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_po_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_po_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_po_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO disapproval level2 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_po_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_po_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_po_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_po_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO authorization level 3 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_po_lvl3(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_po_lvl3').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_po_lvl3').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_po_lvl3", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO disapproval level3 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_po_lvl3(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_po_lvl3').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_po_lvl3').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_po_lvl3", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO Pending for authorization in ERP ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getnot_auth_po(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('not_auth_po').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('not_auth_po').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/not_auth_po", true);
	xmlhttp.send();
	
	}

</script>

<?php //ERP Amended PO'S ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getamend_po(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('amend_po').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('amend_po').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/amend_po", true);
	xmlhttp.send();
	
	}

</script>

<?php //Purchase order send to supplier for purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getsupp_for_pur(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('supp_for_pur').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('supp_for_pur').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/supp_for_pur", true);
	xmlhttp.send();
	
	}

</script>

<?php //Lead Time Update By Purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getlead_time_update(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('lead_time_update').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('lead_time_update').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/lead_time_update", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for manufacturing clearance planning ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmc_planning(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mc_planning').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mc_planning').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mc_planning", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for manufacturing clearance purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmc_purchase(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mc_purchase').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mc_purchase').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mc_purchase", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for acknowledgement from supplier ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getack_frm_supp(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ack_frm_supp').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('ack_frm_supp').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/ack_frm_supp", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for test certificate uploadion ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function gettc_upload(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('tc_upload').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('tc_upload').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/tc_upload", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending For DI Planning ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdi_planning(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('di_planning').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('di_planning').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/di_planning", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for DI Purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdi_purchase(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('di_purchase').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('di_purchase').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/di_purchase", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for dispatch instruction ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdispatch_inst(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dispatch_inst').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dispatch_inst').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dispatch_inst", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO Pending for delivery details ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdeli_details(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('deli_details').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('deli_details').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/deli_details", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO Pending for gate entry ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getgate_entry(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('gate_entry').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('gate_entry').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/gate_entry", true);
	xmlhttp.send();
	
	}

</script>

<?php //HSN & Quantity Check By Purchaser ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function gethsn_check(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('hsn_check').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('hsn_check').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/hsn_check", true);
	xmlhttp.send();
	
	}

</script>

<?php // WITHOUT MC AND DI PENDING FOR APPROVAL FROM REPORTING AUTHORITY ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmcdi_ra(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mcdi_ra').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mcdi_ra').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mcdi_ra", true);
	xmlhttp.send();
	
	}

</script>

<?php // DISAPPROVED FROM REPORTING AUTHORITY ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_ra(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_ra').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_ra').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_ra", true);
	xmlhttp.send();
	
	}

</script>

<?php // WITHOUT MC AND DI PENDING FOR APPROVAL FROM PROJECT INCHARGE ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmcdi_pi(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mcdi_pi').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mcdi_pi').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mcdi_pi", true);
	xmlhttp.send();
	
	}

</script>

<?php // DISAPPROVED FROM PROJECT INCHARGE ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pi(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_pi').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_pi').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pi", true);
	xmlhttp.send();
	
	}

</script>

<?php //  WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L1 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmcdi_cf1(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mcdi_cf1').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mcdi_cf1').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mcdi_cf1", true);
	xmlhttp.send();
	
	}

</script>

<?php //DISAPPROVED FROM CASHFLOW INCHARGE L1 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_cf1(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_cf1').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_cf1').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_cf1", true);
	xmlhttp.send();
	
	}

</script>

<?php //  WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L2 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmcdi_cf2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mcdi_cf2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mcdi_cf2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mcdi_cf2", true);
	xmlhttp.send();
	
	}

</script>

<?php //DISAPPROVED FROM CASHFLOW INCHARGE L2 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_cf2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_cf2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_cf2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_cf2", true);
	xmlhttp.send();
	
	}

</script>