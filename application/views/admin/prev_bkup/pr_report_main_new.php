<?php include'header.php'; ?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PR/PO Age Report </h4>
        </div>
    </div><br />
        
    <!-----Filters------>
    
    <form action="<?php echo base_url(); ?>index.php/pr_reportc/" method="post" onSubmit="return validate()">
    
    <div class="row">
    	<div class="col-lg-1"><b>From Date</b></div>
        <div class="col-lg-2">
        	<input type="text" name="from_date" id="from_date" value="" class="form-control" value="<?php echo $from_date; ?>" autocomplete="off">
        </div>
        <div class="col-lg-1"><b>To Date</b></div>
        <div class="col-lg-2">
        	<input type="text" name="to_date" id="to_date" value="" class="form-control" value="<?php echo $to_date; ?>" autocomplete="off">
        </div>
        <div class="col-lg-1"><b>Usage Type</b></div>
        <div class="col-lg-2">
        	<select id="usage_type" name="usage_type" class="form-control">
            	<option value="">--Select--</option>
                <option value="Purchase Request">Purchase Request</option>
                <option value="Purchase Order">Purchase Order</option>
            </select>
        </div>
        <div class="col-lg-2"><input type="button" name="sort" id="sort" value="Sort" class="form-control" onClick="filter()"></div>
        <div class="col-lg-1"></div>
    </div>
    
    <!----- PO Details -------->
    <div class="row">
    	<div class="col-lg-12">
            <div id="ajax_div"></div>
        </div>
    </div>
    
    </form>
    
  </section>
</section>

<?php include('footer.php'); ?>

<script>
$( "#to_date" ).datepicker();
$( "#from_date" ).datepicker();

function validate(){
	var from_date = document.getElementById("from_date").value;
	var to_date = document.getElementById("to_date").value;
	var usage_type = document.getElementById("usage_type").value;
	
	if(from_date != '' && to_date == ''){
		alert("Please Enter Both The Dates..");
		document.getElementById("to_date").focus;
		return false;
	}
	
	if(from_date == '' && to_date != ''){
		alert("Please Enter Both The Dates..");
		document.getElementById("to_date").focus;
		return false;
	}
	
	if(usage_type == ''){
		alert("Please Select Request Type");
		document.getElementById("usage_type").focus;
		return false;
	}
	
	if(from_date > to_date){
		alert("To Date Can't be less than From Date. Please Select Another Date Range");
		return false;
	}
}

function filter(){
	var from_date = document.getElementById("from_date").value;
	var to_date = document.getElementById("to_date").value;
	var usage_type = document.getElementById("usage_type").value;
	
	$("#ajax_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ajax_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?from_date="+from_date+"&&to_date="+to_date+"&&usage_type="+usage_type;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pr_reportc/pr_ajax_new"+ queryString, true);
	xmlhttp.send();
	
}

</script>
