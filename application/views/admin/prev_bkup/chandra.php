<!DOCTYPE html>
<html lang="en">
<head>

  <title>bootstrap-select</title>
  
  <link href="<?php echo base_url(); ?>assets/admin/classes/bootstrap.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/admin/classes/bootstrap-select.css" rel="stylesheet">
  
  <script src="<?php echo base_url(); ?>assets/admin/classes/jquery.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/classes/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>assets/admin/classes/bootstrap-select.js"></script>
    
</head>

<body>

    <select class="selectpicker" data-live-search="true">
        <option>--Select--</option>
        <option>Hot Dog, Fries and a Soda</option>
        <option>Burger, Shake and a Smile</option>
        <option>Sugar, Spice and all things nice</option>
    </select><br><br><br>
    
    <select class="selectpicker" data-live-search="true">
        <option value="select">--select--</option>
        <?php
            $sql9 ="select * from scmdb..so_order_hdr where sohdr_order_status = 'AU'";
                    
            $query9 = $this->db->query($sql9);
            
            $counter = 0;
            if ($query9->num_rows() > 0) {
                $counter++;
              foreach ($query9->result() as $row) {
                  $so_no1 = $row->sohdr_order_no;
        ?> 
            <option value="<?php echo $so_no1; ?>"><?php echo $so_no1; ?></option>
        <?php 
              } 
            } 
        ?>
    </select>

</body>
</html>