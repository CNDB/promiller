<?php
	include'header.php';
	
    $pr_num = $this->uri->segment(3);
	$username_pr = strtolower($_SESSION['username']);
?>
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">APPROVE PURCHASE REQUEST</h4>
        </div>
    </div><br />

<form action="<?php echo base_url(); ?>index.php/createpoc/insert_pr_sub_auth" method="post" enctype="multipart/form-data" onSubmit="return reqd()">

<?php include('pr_item_details.php'); ?>

<?php include('pr_live_filled_details.php'); ?>
            
<?php  /* Submit Form */  ?>

<?php
	if($count > 1){	
		echo "<br /><h3 style='color:red; text-align:center'>One Purchase Request Contains Only One Item.</h3><br />";
	} else {
?>


<?php

$username = $_SESSION['username'];
$sql_po_approval = "select * from tipldb..pr_submit_table where pr_num = '$pr_num'";

$query_approval_sql = $this->db->query($sql_po_approval);

foreach ($query_approval_sql->result() as $row) {
  $approval_auth_name = $row->level1_approval_mailto;
  $level2_approval_req = $row->level2_approval_req;
  $level2_approval_mailto = $row->level2_approval_mailto;
  $color = $row->color;
   
  echo "<input type='hidden' name='level2_approval_req' value='$level2_approval_req' />";
  echo "<input type='hidden' name='level2_approval_mailto' value='$level2_approval_mailto' />";
  echo "<input type='hidden' name='color' value='$color' />";
} 



// if($username_pr == 'chandra.sharma' || $username_pr == 'abhinav.toshniwal' || $username_pr ==  $approval_auth_name || $username_pr == 'raviraj.mehra'){ 

if(($username_pr == 'admin' || $username_pr == 'abhinav.toshniwal' || $username_pr == 'priyanka.vijay' || $username_pr == 'manisha.agarwal' || $username_pr ==  $approval_auth_name)||($category == 'SECURITY' && $username_pr == 'shubham.soni' )){
?>
            
<div class="row">
    <div class="col-lg-3">
    	<b>Approval / Disapproval Remarks</b>
    </div>
    
    <div class="col-lg-3">
    	<input type="text" name="remarks_auth" id="remarks_auth" value=""  class="form-control"/>
    </div>
    
    <div class="col-lg-3"> 
    <input type="submit" name="APPROVE" value="APPROVE" class="form-control" style="font-weight:bold; background:#000000; color:#FFFFFF;" />     
    </div>
    
    <div class="col-lg-3"> 
    <input type="submit" name="APPROVE" value="DISAPPROVE" class="form-control" style="font-weight:bold; background:#000000; color:#FFFFFF;" />     
    </div>
    
</div><br/><br/>
<?php } else {
	
	echo "<br><h3 style='color:red; text-align:center'>You are not authorized to approve/disapprove this PR</h3><br>"; 

}
?><?php } ?>

</form>

<?php include('pr_footer.php'); ?>