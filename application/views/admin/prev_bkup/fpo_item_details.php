<?php 
$sql_item_det_fet = "SELECT supp_item_code, current_price, total_item_value,* FROM tipldb..insert_po 
where po_num = '$po_num' and po_ipr_no = '$po_ipr_no'";

$qry_item_det_fet = $this->db->query($sql_item_det_fet);

if($qry_item_det_fet->num_rows() > 0){
foreach($qry_item_det_fet->result() as $row){
	$supp_item_code = $row->supp_item_code;
	$need_date_rmks = $row->need_date_rmks;

?>

<tr>
    <input type="hidden" name="po_num" value="<?php echo $po_num; ?>">
    <input type="hidden" name="po_date" value="<?php echo $po_date; ?>">
    <input type="hidden" name="po_line_no[]" value="<?php echo $po_line_no; ?>">
    <input type="hidden" name="po_supp_code" value="<?php echo $supp_code; ?>">
    <input type="hidden" name="po_qty" value="<?php echo $po_quantity; ?>">
    <input type="hidden" name="po_amend_no" value="<?php echo $po_amend_no; ?>"> 
    <input type="hidden" name="po_type" value="<?php echo $po_type; ?>">
    <input type="hidden" name="supp_phone" value="<?php echo $supp_phone; ?>">
    <input type="hidden" name="contact_person" value="<?php echo $contact_person; ?>"> 
    <input type="hidden" name="pay_mode" value="<?php echo $pay_mode; ?>">
    <input type="hidden" name="trans_mode" value="<?php echo $trans_mode; ?>">
    <input type="hidden" name="partial_ship" value="<?php echo $partial_ship; ?>">
    <input type="hidden" name="po_date" value="<?php echo $po_date; ?>">
    <input type="hidden" name="po_cost_pr_unt" value="<?php echo $cost_pr_unt; ?>">
    <input type="hidden" name="po_need_date" value="<?php echo $need_date; ?>">
    <input type="hidden" name="po_wh_code" value="<?php echo $wh_code; ?>">
    <input type="hidden" name="po_basic_val" value="<?php echo $totalcost; ?>">
    <input type="hidden" name="po_tot_val" value="<?php echo $totalcost; ?>">
    <input type="hidden" name="po_supp_email" value="<?php echo $supp_email; ?>">
    <input type="hidden" name="supp_state" value="<?php echo $supp_state; ?>">
    <input type="hidden" name="road_permit_req" value="<?php echo $road_permit_req; ?>">
    <input type="hidden" name="po_from_screen" value="<?php echo $po_from_screen; ?>">
    <input type="hidden" name="tc_req[]" value="<?php echo $tc_req; ?>">  
                
    <td>
        <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
        <?php echo $po_ipr_no; ?></a>
        <input type="hidden" name="po_ipr_no[]" value="<?php echo $po_ipr_no; ?>">
    </td>
    
    <td>
    	<!--- ITEM CODE --->
        <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
			<?php  echo "<b>ITEM CODE - </b>".$item_code; ?>
        </a>
       <input type="hidden" name="po_item_code[]" value="<?php echo $item_code; ?>"><br /><br />
        
        <!--- ITEM DESCRIPTION ---->
		<?php if($item_desc != ''){ echo "<b>ITEM DESC - </b>".mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");  }?>
        <input type="hidden" name="item_desc[]" value="<?php echo $item_desc_new; ?>"><br /><br />
         
        <!--- UOM ---->
        <?php if($uom != ''){ echo "<b>UOM - </b>".$uom; } ?>
        <input type="hidden" name="po_uom[]" value="<?php echo $uom; ?>"><br /><br />
        
        <!--- HSN CODE ---->
        <?php if($hsncode != ''){ echo "<b>HSN - </b>".$hsncode; } ?>
        <input type="hidden" name="hsncode[]" value="<?php echo $hsncode; ?>">
    </td>
    
    <td>
        <input type="text" name="supp_item_code[]" id="supp_item_code" value="<?php echo $supp_item_code; ?>">
    </td>
    
    <td>
		<?php echo $pr_qty; ?>
        <input type="hidden" name="pr_qty[]" id="pr_qty<?php echo $i; ?>" value="<?php echo $pr_qty; ?>">
    </td>
    
    <td>
		<?php echo number_format($for_stk_qty,2); ?>
        <input type="hidden" name="for_stk_quantity[]" value="<?php echo $for_stk_qty; ?>">
    </td>
    
    <td>
    	<!--- COSTING --->
		<?php /*?><a href="<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>" target="_blank"><?php */?>
			<?php if($costing != ''){ echo $costing."  ".$costing_currency; } ?>
        <?php /*?></a><?php */?>
        
        <input type="hidden" name="costing[]" value="<?php echo $costing; ?>">
        <input type="hidden" name="attached_cost_sheet[]" value="<?php echo $attached_cost_sheet; ?>"><BR>
        
        <!--- COSTING CURRENCY --->
        <?php echo $costing_currency; ?>
        <input type="hidden" name="costing_currency[]" value="<?php echo $costing_currency; ?>">
    </td>
    
    <td>
		<?php echo $last_price1; ?>
        <input type="hidden" name="last_price[]" value="<?php echo $last_price; ?>">
        <br><br>
        <?php if(!empty($last_price1)){ ?>
        <b>Last 3 GRCPT</b>
        <table border="1">
            <tr style="font-weight:bold; background:#0CF">
                <td>GRCPT NO</td>
                <td>GRCPT STATUS</td>
                <td>GRCPT DATE</td>
                <td>LAST PRICE</td>
            </tr>
            <?php
                //Last 3 GRCPT'S
                $sql_last3_gr = "select top 3 gr_hdr_grno,gr_lin_linestatus,gr_hdr_grdate,poitm_po_cost 
        from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..gr_hdr_grmain c, scmdb..gr_lin_details d 
        where b.poitm_itemcode = '".$item_code."' and c.gr_hdr_grno = d.gr_lin_grno and b.poitm_itemcode = d.gr_lin_itemcode 
        and a.pomas_pono = c.gr_hdr_orderno and c.gr_hdr_orderdoc = 'PO' and c.gr_hdr_grstatus not in('DL') and a.pomas_pono = b.poitm_pono 
        and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE') and a.pomas_poamendmentno = b.poitm_poamendmentno 
        and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr 
                     WHERE pomas_pono = a.pomas_pono AND pomas_poou = a.pomas_poou) 
        and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO') 
        order by pomas_poauthdate desc";
        
                $query_last3_gr = $this->db->query($sql_last3_gr);
                if ($query_last3_gr->num_rows() > 0) {
                  foreach ($query_last3_gr->result() as $row) {
                      $last3_grno = $row->gr_hdr_grno;
                      $last3_grstat = $row->gr_lin_linestatus;
                      $last3_grdate = $row->gr_hdr_grdate;
                      $last3_price = $row->poitm_po_cost;
            ?>
            <tr>
                <td><?=$last3_grno; ?></td>
                <td><?=$last3_grstat; ?></td>
                <td><?=substr($last3_grdate,0,11); ?></td>
                <td><?=number_format($last3_price,2,".",""); ?></td>
            </tr>
            <?php
                }
                } else {
                      $last3_grno = "";
                      $last3_grstat = "";
                      $last3_grdate = "";
                      $last3_price = ""; 
                }
            ?>
        </table>
        <?php } ?>
    </td>
    
    <td>
    <input type="text" name="current_price[]" id="current_price<?php echo $i; ?>" value="<?php echo $current_price; ?>" 
    onkeyup="calculate_item_val('<?php echo $i; ?>');" autocomplete="off" readonly>
    </td>
    
    <td>
		<?php
            if($costing != '' && $current_price > $costing && ($category != 'CAPITAL GOODS' || $category != 'IT' || $category != 'OTHERS')){
            ?>
                <input type="text" name="price_mismatch_reason[]" id="price_mismatch_reason<?php echo $i; ?>" value="">
            <?	
            } else if($costing == '' && $last_price != '' && $last_price < $current_price 
            && ($category != 'CAPITAL GOODS' || $category != 'IT' || $category != 'OTHERS')){
            ?>
                <input type="text" name="price_mismatch_reason[]" id="price_mismatch_reason<?php echo $i; ?>" value="">
            <?
            } else {
            ?>
                <input type="hidden" name="price_mismatch_reason[]" id="price_mismatch_reason<?php echo $i; ?>" value="Correct">
        <? } ?>
    </td>
    
    <td><?php echo number_format($line_level_charges,2); ?></td>
    <td><?php echo number_format($line_level_discount,2); ?></td>
    
    <td>
    <?php $item_value_new = ($item_value+$line_level_charges)-$line_level_discount; ?>
    <input type="text" name="item_value[]" id="item_value<?php echo $i; ?>" value="<?php echo round($item_value_new,2); ?>" readonly="readonly">
    </td>
    
    <td style="background-color:<?php echo $color; ?>">
		<?php echo $ipr_need_date; ?> 
        <input type="hidden" name="ipr_need_date[]" value="<?php echo $ipr_need_date; ?>"> 
        <input type="hidden" name="color[]" value="<?php echo $color; ?>">
    </td>
    
    <td><?php echo substr($po_need_date,0,11); ?></td>
        
	<?php if(strtotime($ipr_need_date) != strtotime($po_need_date)){ ?>
    <td><input type="text" name="need_date_rmks[]" id="need_date_rmks<?php echo $i; ?>" value="<?php echo $need_date_rmks; ?>"></td>
    <?php } else { ?>
    <td><input type="hidden" name="need_date_rmks[]" id="need_date_rmks<?php echo $i; ?>" value="PR & PO Need Date Are Equal"></td>
    <?php } ?>
    
    <td>
        <!-- PR TYPE -->
        <?php if($ipr_type != ''){ echo "<b>PR TYPE - </b>".$ipr_type; } ?>
        <input type="hidden" name="ipr_type[]" value="<?php echo $ipr_type; ?>"><br><br> 
        
        <!-- WHY SPECIAL REMARKS -->
        <?php if($why_spcl_rmks != ''){ echo "<b>SPCL RMKS - </b>".$why_spcl_rmks; } ?> 
        <input type="hidden" name="why_spcl_rmks[]" value="<?php echo $why_spcl_rmks; ?>"><br><br>
        
        <!-- CATEGORY -->
        <?php
            $po_first_three = substr($po_num,0,3);
            if($po_first_three == 'CGP'){
                $category = 'CAPITAL GOODS';
                $category1 = 'CAPITAL GOODS';
            }
            
            if($category != ''){ echo "<b>CATEGORY - </b>".$category; }
        ?> 
        <input type="hidden" name="category[]" value="<?php echo $category; ?>"> 
        <input type="hidden" name="category1" value="<?php echo $category; ?>">   
    </td>
    
    <td>
        <!--- SO NUMBER --->
        <?php if($sono != ''){ echo "<b>SO NO - </b>".$sono; } ?>
        <input type="hidden" name="sono[]" value="<?php echo $sono; ?>"> <br><br>
        
        <!--- ATAC NUMBER --->
        <input type="hidden" name="atac_no[]" value="<?php echo $atac_no; ?>">
        <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
            <?php if($atac_no != ''){  echo "<b>ATAC NO - </b>".$atac_no; } ?>
        </a> <br /><br />
        
        <!--- ATAC LD DATE --->
        <?php if($atac_ld_date != ''){  echo "<b>ATAC LD DATE - </b>".$atac_ld_date; } ?>
        <input type="hidden" name="atac_ld_date[]" value="<?php echo $atac_ld_date; ?>"> <br><br>
        
        <!--- ATAC NEED DATE --->
        <?php if($atac_need_date != ''){  echo "<b>ATAC NEED DATE - </b>".$atac_need_date; } ?> 
        <input type="hidden" name="atac_need_date[]" value="<?php echo $atac_need_date; ?>"><br><br>
        
        <!--- ATAC PAYMENT TERMS --->
        <?php if($atac_payment_terms != ''){  echo "<b>ATAC PAYTERM - </b>".$atac_payment_terms; } ?> 
        <input type="hidden" name="atac_payment_terms[]" value="<?php echo $atac_payment_terms; ?>"><br><br>
        
        <!--- PM GROUP --->
        <?php if($pm_group != ''){  echo "<b>PM GROUP - </b>".$pm_group; } ?> 
        <input type="hidden" name="pm_group[]" value="<?php echo $pm_group; ?>">
        <input type="hidden" name="pm_group1" value="<?php echo $pm_group; ?>"><br><br>
        
        <!--- PROJECT NAME --->
        <?php if($project_name != ''){  echo "<b>PROJECT NAME - </b>".$project_name; } ?> 
        <input type="hidden" name="project_name[]" value="<?php echo $project_name; ?>"><br><br> 
        
    </td>
    
    <td>
        <!-- PR REMARKS PLANNING -->
        <?php if($ipr_remarks != ''){  echo "<b>PR REMARKS PLANNING - </b>".$ipr_remarks; } ?>
        <input type="hidden" name="ipr_remarks[]" value="<?php echo $ipr_remarks; ?>"><br><br>
        
        <!-- PR REMARKS PURCHASE -->
        <?php if($ipr_remarks != ''){  echo "<b>PR REMARKS PURCHASE - </b>"; } ?>
        <input type="text" name="item_remarks[]" id="item_remarks" value="">
    </td>
    
    <td>
		<?php echo $supp_item_remarks; ?>
        <input type="hidden" name="supp_item_remarks[]" id="supp_item_remarks" value="<?php echo $supp_item_remarks; ?>">
    </td>
    
    <td>
        <!-- MANUFACTURING CLEARANCE -->
        <?php if($manufact_clrnce != ''){  echo "<b>MF - </b>".$manufact_clrnce; } ?>
        <input type="hidden" name="manufact_clrnce[]" id="manufact_clrnce" value="<?php echo $manufact_clrnce; ?>">
        
        <!-- DISPATCH INSTRUCTION -->
        <?php if($dispatch_inst != ''){  echo "<b>DI - </b>".$dispatch_inst; } ?>
        <input type="hidden" name="dispatch_inst[]" id="dispatch_inst" value="<?php echo $dispatch_inst; ?>">
    </td>
    
    <td>
        <?php if($wh_code != ''){  echo $wh_code; } ?>
        <input type="hidden" name="wh_code" value="<?php echo $wh_code; ?>">
    </td>
    
    <td>
        <!-- LAST SUPPLIER -->
        <?php if($SuppName != ''){  echo "<b>SUPP NAME - </b>".$SuppName; } ?>
        <input type="hidden" name="SuppName[]" value="<?php echo $SuppName; ?>"><br><br>
        
        <!-- LAST SUPPLIER RATE -->
        <?php if($SuppRate != ''){  echo "<b>SUPP RATE - </b>".number_format($SuppRate,2); } ?>
        <input type="hidden" name="SuppRate[]" value="<?php echo $SuppRate; ?>"><br><br>
        
        <!-- LAST SUPPLIER LEAD TIME -->
        <?php if($SuppLeadTime != ''){  echo "<b>SUPP LEAD TIME - </b>".number_format($SuppLeadTime,2); } ?>
        <input type="hidden" name="SuppLeadTime[]" value="<?php echo $SuppLeadTime; ?>"><br><br>
        
        <!-- LAST SUPPLIER RATE -->
        <?php if($last_grcpt_date != ''){  echo "<b>LAST GRCPT FM DATE - </b>".substr($last_grcpt_date,0,11); } ?>
        <input type="hidden" name="last_grcpt_date[]" value="<?php echo $last_grcpt_date; ?>"><br><br>
    </td>
    
    <td><input type="file" name="attach_cost_sheet[]" id="attach_cost_sheet" value="" style="display:none"></td>
</tr>

<?php }} else { ?>

<tr>
    <input type="hidden" name="po_num" value="<?php echo $po_num; ?>">
    <input type="hidden" name="po_date" value="<?php echo $po_date; ?>">
    <input type="hidden" name="po_line_no[]" value="<?php echo $po_line_no; ?>">
    <input type="hidden" name="po_supp_code" value="<?php echo $supp_code; ?>">
    <input type="hidden" name="po_qty" value="<?php echo $po_quantity; ?>">
    <input type="hidden" name="po_amend_no" value="<?php echo $po_amend_no; ?>"> 
    <input type="hidden" name="po_type" value="<?php echo $po_type; ?>">
    <input type="hidden" name="supp_phone" value="<?php echo $supp_phone; ?>">
    <input type="hidden" name="contact_person" value="<?php echo $contact_person; ?>"> 
    <input type="hidden" name="pay_mode" value="<?php echo $pay_mode; ?>">
    <input type="hidden" name="trans_mode" value="<?php echo $trans_mode; ?>">
    <input type="hidden" name="partial_ship" value="<?php echo $partial_ship; ?>">
    <input type="hidden" name="po_date" value="<?php echo $po_date; ?>">
    <input type="hidden" name="po_cost_pr_unt" value="<?php echo $cost_pr_unt; ?>">
    <input type="hidden" name="po_need_date" value="<?php echo $need_date; ?>">
    <input type="hidden" name="po_wh_code" value="<?php echo $wh_code; ?>">
    <input type="hidden" name="po_basic_val" value="<?php echo $totalcost; ?>">
    <input type="hidden" name="po_tot_val" value="<?php echo $totalcost; ?>">
    <input type="hidden" name="po_supp_email" value="<?php echo $supp_email; ?>">
    <input type="hidden" name="supp_state" value="<?php echo $supp_state; ?>">
    <input type="hidden" name="road_permit_req" value="<?php echo $road_permit_req; ?>">
    <input type="hidden" name="po_from_screen" value="<?php echo $po_from_screen; ?>">
    <input type="hidden" name="tc_req[]" value="<?php echo $tc_req; ?>">             
    <td>
        <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
        <?php echo $po_ipr_no; ?></a>
        <input type="hidden" name="po_ipr_no[]" value="<?php echo $po_ipr_no; ?>">
    </td>
    <td>
    	<!--- ITEM CODE --->
        <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
			<?php  echo "<b>ITEM CODE - </b>".$item_code; ?>
        </a>
       <input type="hidden" name="po_item_code[]" value="<?php echo $item_code; ?>"><br><br>
        
        <!--- ITEM DESCRIPTION ---->
		<?php if($item_desc != ''){ echo "<b>ITEM DESC - </b>".mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");  }?>
        <input type="hidden" name="item_desc[]" value="<?php echo $item_desc_new; ?>"><br><br>
         
        <!--- UOM ---->
        <?php if($uom != ''){ echo "<b>UOM - </b>".$uom; } ?>
        <input type="hidden" name="po_uom[]" value="<?php echo $uom; ?>"><br><br>
        
        <!--- HSN CODE ---->
        <?php if($hsncode != ''){ echo "<b>HSN - </b>".$hsncode; } ?>
        <input type="hidden" name="hsncode[]" value="<?php echo $hsncode; ?>">
    </td>
    
    <td><input type="text" name="supp_item_code[]" id="supp_item_code" value="<?php echo $supp_item_code; ?>"></td>
    
    <td>
		<?php echo number_format($pr_qty,2); ?>
        <input type="hidden" name="pr_qty[]" id="pr_qty<?php echo $i; ?>" value="<?php echo $pr_qty; ?>">
    </td>
    
    <td>
		<?php echo number_format($for_stk_qty,2); ?>
        <input type="hidden" name="for_stk_quantity[]" value="<?php echo $for_stk_qty; ?>">
    </td>
    
    <td>
    	<!--- COSTING --->
		<?php if($attached_cost_sheet != '' || $attached_cost_sheet != NULL){ ?>
        <a href="<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>" target="_blank">
            <?php if($costing != ''){ echo $costing."  ".$costing_currency; } ?>
        </a>
        <?php } else { ?>
            <?php if($costing != ''){ echo $costing."  ".$costing_currency; } ?>
        <?php } ?>
        
        <input type="hidden" name="costing[]" value="<?php echo $costing; ?>">
        <input type="hidden" name="attached_cost_sheet[]" value="<?php echo $attached_cost_sheet; ?>"><br>
        
        <!--- COSTING CURRENCY --->
        <?php echo $costing_currency; ?>
        <input type="hidden" name="costing_currency[]" value="<?php echo $costing_currency; ?>">
    </td>
    
    <td>
		<?php echo $last_price1; ?>
        <input type="hidden" name="last_price[]" value="<?php echo $last_price; ?>">
    </td>
    
    <td>
    <input type="text" name="current_price[]" id="current_price<?php echo $i; ?>" value="<?php echo $current_price; ?>" 
    onkeyup="calculate_item_val('<?php echo $i; ?>');" autocomplete="off" readonly="readonly">
    </td>
    
    <td>
		<?php
            if($costing != '' && $current_price > $costing && ($category != 'CAPITAL GOODS' || $category != 'IT' || $category != 'OTHERS')){
            ?>
                <input type="text" name="price_mismatch_reason[]" id="price_mismatch_reason<?php echo $i; ?>" value="">
            <?	
            } else if($costing == '' && $last_price != '' && $last_price < $current_price 
            && ($category != 'CAPITAL GOODS' || $category != 'IT' || $category != 'OTHERS')){
            ?>
                <input type="text" name="price_mismatch_reason[]" id="price_mismatch_reason<?php echo $i; ?>" value="" />
            <?
            } else {
            ?>
                <input type="hidden" name="price_mismatch_reason[]" id="price_mismatch_reason<?php echo $i; ?>" value="Correct" />
        <? } ?>
    </td>
    
    <td><?php echo number_format($line_level_charges,2); ?></td>
    
    <td><?php echo number_format($line_level_discount,2); ?></td>
    
    <td>
    <?php $item_value_new = ($item_value+$line_level_charges)-$line_level_discount; ?>
    <input type="text" name="item_value[]" id="item_value<?php echo $i; ?>" value="<?php echo $item_value_new; ?>" readonly="readonly" />
    </td>
    
    <td style="background-color:<?php echo $color; ?>">
		<?php echo $ipr_need_date; ?> 
        <input type="hidden" name="ipr_need_date[]" value="<?php echo $ipr_need_date; ?>" /> 
        <input type="hidden" name="color[]" value="<?php echo $color; ?>" />
    </td>
    
    <td><?php echo substr($po_need_date,0,11); ?></td>
        
	<?php if(strtotime($ipr_need_date) != strtotime($po_need_date)){ ?>
    <td><input type="text" name="need_date_rmks[]" id="need_date_rmks<?php echo $i; ?>" value="" /></td>
    <?php } else { ?>
    <td><input type="hidden" name="need_date_rmks[]" id="need_date_rmks<?php echo $i; ?>" value="PR & PO Need Date Are Equal" /></td>
    <?php } ?>
    
    <td>
        <!-- PR TYPE -->
        <?php if($ipr_type != ''){ echo "<b>PR TYPE - </b>".$ipr_type; } ?>
        <input type="hidden" name="ipr_type[]" value="<?php echo $ipr_type; ?>" /><br /><br /> 
        
        <!-- WHY SPECIAL REMARKS -->
        <?php if($why_spcl_rmks != ''){ echo "<b>SPCL RMKS - </b>".$why_spcl_rmks; } ?> 
        <input type="hidden" name="why_spcl_rmks[]" value="<?php echo $why_spcl_rmks; ?>" /><br /><br />
        
        <!-- CATEGORY -->
        <?php
            $po_first_three = substr($po_num,0,3);
            if($po_first_three == 'CGP'){
                $category = 'CAPITAL GOODS';
                $category1 = 'CAPITAL GOODS';
            }
            
            if($category != ''){ echo "<b>CATEGORY - </b>".$category; }
        ?> 
        <input type="hidden" name="category[]" value="<?php echo $category; ?>" /> 
        <input type="hidden" name="category1" value="<?php echo $category; ?>" />   
    </td>
    
    <td>
        <!--- SO NUMBER --->
        <?php if($sono != ''){ echo "<b>SO NO - </b>".$sono; } ?>
        <input type="hidden" name="sono[]" value="<?php echo $sono; ?>" /> <br /><br />
        
        <!--- ATAC NUMBER --->
        <input type="hidden" name="atac_no[]" value="<?php echo $atac_no; ?>" />
        <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
            <?php if($atac_no != ''){  echo "<b>ATAC NO - </b>".$atac_no; } ?>
        </a> <br /><br />
        
        <!--- ATAC LD DATE --->
        <?php if($atac_ld_date != ''){  echo "<b>ATAC LD DATE - </b>".$atac_ld_date; } ?>
        <input type="hidden" name="atac_ld_date[]" value="<?php echo $atac_ld_date; ?>" /> <br /><br />
        
        <!--- ATAC NEED DATE --->
        <?php if($atac_need_date != ''){  echo "<b>ATAC NEED DATE - </b>".$atac_need_date; } ?> 
        <input type="hidden" name="atac_need_date[]" value="<?php echo $atac_need_date; ?>" /><br /><br />
        
        <!--- ATAC PAYMENT TERMS --->
        <?php if($atac_payment_terms != ''){  echo "<b>ATAC PAYTERM - </b>".$atac_payment_terms; } ?> 
        <input type="hidden" name="atac_payment_terms[]" value="<?php echo $atac_payment_terms; ?>" /><br /><br />
        
        <!--- PM GROUP --->
        <?php if($pm_group != ''){  echo "<b>PM GROUP - </b>".$pm_group; } ?> 
        <input type="hidden" name="pm_group[]" value="<?php echo $pm_group; ?>" />
        <input type="hidden" name="pm_group1" value="<?php echo $pm_group; ?>" /><br /><br />
        
        <!--- PROJECT NAME --->
        <?php if($project_name != ''){  echo "<b>PROJECT NAME - </b>".$project_name; } ?> 
        <input type="hidden" name="project_name[]" value="<?php echo $project_name; ?>" /><br /><br /> 
        
    </td>
    
    <td>
        <!-- PR REMARKS PLANNING -->
        <?php if($ipr_remarks != ''){  echo "<b>PR REMARKS PLANNING - </b>".$ipr_remarks; } ?>
        <input type="hidden" name="ipr_remarks[]" value="<?php echo $ipr_remarks; ?>" /><br /><br />
        
        <!-- PR REMARKS PURCHASE -->
        <?php if($ipr_remarks != ''){  echo "<b>PR REMARKS PURCHASE - </b>"; } ?>
        <input type="text" name="item_remarks[]" id="item_remarks" value=""  />
    </td>
    
    <td>
		<?php echo $supp_item_remarks; ?>
        <input type="hidden" name="supp_item_remarks[]" id="supp_item_remarks" value="<?php echo $supp_item_remarks; ?>" />
    </td>
    
    <td>
        <!-- MANUFACTURING CLEARANCE -->
        <?php if($manufact_clrnce != ''){  echo "<b>MF - </b>".$manufact_clrnce; } ?>
        <input type="hidden" name="manufact_clrnce[]" id="manufact_clrnce" value="<?php echo $manufact_clrnce; ?>"  />
        
        <!-- DISPATCH INSTRUCTION -->
        <?php if($dispatch_inst != ''){  echo "<b>DI - </b>".$dispatch_inst; } ?>
        <input type="hidden" name="dispatch_inst[]" id="dispatch_inst" value="<?php echo $dispatch_inst; ?>"  />
    </td>
    
    <td>
        <?php if($wh_code != ''){  echo $wh_code; } ?>
        <input type="hidden" name="wh_code" value="<?php echo $wh_code; ?>" />
    </td>
    
    <td>
        <!-- LAST SUPPLIER -->
        <?php if($SuppName != ''){  echo "<b>SUPP NAME - </b>".$SuppName; } ?>
        <input type="hidden" name="SuppName[]" value="<?php echo $SuppName; ?>" /><br /><br />
        
        <!-- LAST SUPPLIER RATE -->
        <?php if($SuppRate != ''){  echo "<b>SUPP RATE - </b>".number_format($SuppRate,2); } ?>
        <input type="hidden" name="SuppRate[]" value="<?php echo $SuppRate; ?>" /><br /><br />
        
        <!-- LAST SUPPLIER LEAD TIME -->
        <?php if($SuppLeadTime != ''){  echo "<b>SUPP LEAD TIME - </b>".number_format($SuppLeadTime,2); } ?>
        <input type="hidden" name="SuppLeadTime[]" value="<?php echo $SuppLeadTime; ?>" /><br /><br />
        
        <!-- LAST SUPPLIER RATE -->
        <?php if($last_grcpt_date != ''){  echo "<b>LAST GRCPT FM DATE - </b>".substr($last_grcpt_date,0,11); } ?>
        <input type="hidden" name="last_grcpt_date[]" value="<?php echo $last_grcpt_date; ?>" /><br /><br />
    </td>
    
    <td><input type="file" name="attach_cost_sheet[]" id="attach_cost_sheet" value="" style="display:none" /></td>
</tr>

<?php } ?>