<?php include'header.php'; ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Supplier Card</h4>
        </div>
    </div><br>
    <form>
    <div class="row">
    	<div class="col-lg-3"></div>
        <div class="col-lg-2"><h4>Enter Supplier Code.</h4></div>
        <div class="col-lg-2"><input type="text" name="supp_code" id="supp_code" value="" class="form-control" required></div>
        <div class="col-lg-1"><input type="button" id="submit" name="submit" value="Submit" class="form-control" onClick="supp_det();"></div>
        <div class="col-lg-3"></div>
    </div>
    </form>
    <!--- Ajax Div -->
    <div id="detail"></div>
  </section>
</section>

<?php include('footer.php'); ?>
<script type="text/javascript">
function supp_det(){
	var supp_code = document.getElementById("supp_code").value;
	
	if(supp_code != ''){
	
	$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	} else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('detail').innerHTML=xmlhttp.responseText;
		} 
	}
	
	var queryString="?supp_code="+supp_code;
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/suppliercardc/suppliercard_det"+ queryString, true);
	xmlhttp.send();
	
	} else {
		alert("Please enter supplier code");
	}
}
</script>
