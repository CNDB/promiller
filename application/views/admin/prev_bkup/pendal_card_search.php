<?php
	include'header.php';                    
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDAL CARD</h4>
        </div>
    </div><br />
    
<?php echo $valueToSearch = $this->input->post('valueToSearch'); ?>
    
    <!-----Filters------>
    
    <form action="#" method="post" onSubmit="return validate()">
    
    <div class="row">
    	<div class="col-lg-2">
        	<b>Enter Item Code.</b>
        </div>
    	<div class="col-lg-4">
        	<input type="text" name="valueToSearch" id="valueToSearch" placeholder="Enter Item Code" class="form-control" autocomplete="off">
        </div>
        <div class="col-lg-2">
        	<input type="button" name="search" value="Search" class="form-control" onClick="return check_pr()">
        </div>    
    </div>
    
    </form>
    
    <div class="row">
    	<div class="col-lg-12" id="pendal_detail"></div>
    </div>
    
  </section>
</section>

<?php include('footer.php'); ?>

<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function check_pr(){
	
	  var searchbox = document.getElementById("valueToSearch").value;
	  
	  searchbox1 = encodeURIComponent(searchbox);
	  
	   if(searchbox == ''){
			alert("Please Enter Item Code...");
			document.getElementById("valueToSearch").focus;
			return false;
	   }
	   
	  $("#pendal_detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	   
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('pendal_detail').innerHTML=xmlhttp.responseText;
		}/* else {
			document.getElementById('pendal_detail').innerHTML="<strong>Waiting For Server...</strong>";
		}*/
	 }
	
	var queryString="?item_code="+searchbox1;
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pendalc/pendal_details" + queryString,true);
	xmlhttp.send();
	
	}

</script>

<!--<script>
//Disable Enter Key
$(document).keypress(
    function(event){
     if (event.which == '13') {
        event.preventDefault();
      }
});
</script>-->
