<!--main content start-->
<!--- PO Type ----->
<?php

	foreach ($view_po->result() as $row){
		$po_from_screen = $row->po_from_screen;
	}
		
	if($po_from_screen == 'fresh') {	
		$po_stat = "Fresh PO";	
	} else if($po_from_screen == 'disapprove_lvl1'){
		$po_stat = "Disapprove PO LVL1";
	} else if($po_from_screen == 'disapprove_lvl2'){
		$po_stat = "Disapprove PO LVL2";
	} else if($po_from_screen == 'amend_po'){
		$po_stat = "Amended PO";
	}
	
?>

<div class="row">
    <div class="col-lg-12">
           <h3><?php echo $po_stat; ?></h3>
     </div>
</div><br />

<form action="<?php echo base_url(); ?>index.php/createpodc/insert_po_sub_lvl1" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?>

<!--****** SUPPLIER QUOTES AND REMARKS *******-->  
<?php include("po_supplier_quotes.php"); ?> 

<?php //lock for po approval by abhinav sir ?>
<?php 
	$username = strtolower($_SESSION['username']);
	
	$sql_po_approval = "select * from tipldb..po_master_table where po_num = '$po_num'";

	$query_approval_sql = $this->db->query($sql_po_approval);
	
	foreach ($query_approval_sql->result() as $row) {
	  $level1_mail_to = $row->level1_mail_to;
	}
	
	if($level1_mail_to == 'sunil.tandon'){
		$level1_mail_to = 'sandeep.tak';
	}
		
		
	if((in_array($username, array('admin','priti.toshniwal','abhinav.toshniwal','rajeev.toshniwal','priyanka.vijay','manisha.agarwal')) ||
	$username == $level1_mail_to) || (in_array($category, array('CAPITAL GOODS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','OTHERS','PRODUCTION CONSUMABLES','TOOLS')) && $username == 'sandeep.tak') ){
?>
            
<div class="row">
    <div class="col-lg-2">
    <b>Approve Disapprove Comments:</b>  
    </div>
    <div class="col-lg-4"> 
    <input type="text" name="app_disapp_comnts_lvl1" id="app_disapp_comnts_lvl1" class="form-control"  value="" />  
    </div>
    <div class="col-lg-3"> 
    <input type="submit" name="po_approval_lvl1" value="Approve" class="form-control" id="po_approval_lvl1"
    style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px; display:block;" onclick="hide('po_approval_lvl1')" />     
    </div>
    <div class="col-lg-3"> 
    <input type="submit" name="po_approval_lvl1" value="Disapporve" class="form-control" id="po_approval_lvl1"
    style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px; display:block;" onclick="hide('po_approval_lvl1')"/>    
    </div>
</div><br /><br />

<?php
	} else {
		echo "<h3 style='color:red; text-align:center'>You are not authorized to approve/disapprove this PO</h3>";
	}
?>
<?php //lock for po approval by abhinav sir ?>
          
</form>
<?php //chat history ?>
<?php include('po_chat_history.php'); ?>
<?php //Action Timing Report ?>
<?php include('po_action_timing.php'); ?>     
<?php include('footer.php'); ?>