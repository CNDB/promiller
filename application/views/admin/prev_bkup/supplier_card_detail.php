<?php
/*$supp_code = $_REQUEST['supp_code'];
$supp_code = trim($supp_code);

$sql_supp_proc = "exec tipldb..supp_card_proc '$supp_code'";
$qry_supp_proc = $this->db->query($sql_supp_proc);
$this->db->close();
$this->db->initialize();*/
?>
<div class="row">
	<div class="col-lg-12">
    	<h3 align="center" style="text-transform:uppercase"><u>Vendor Card</u></h3>
    </div>
</div><br /><br />

<?php
	$sql_hdr = "select * from tipldb..supp_card_tbl where supp_rectype = 'Hdr'";
	$qry_hdr = $this->db->query($sql_hdr);
	foreach($qry_hdr->result() as $row){
		$supp_code = $row->supp_suppno;
		$supp_name = $row->supp_suppname;
		$supp_contactpersons = $row->supp_contactpersons;
		$supp_add1 = $row->supp_add1;
		$supp_add2 = $row->supp_add2;
		$supp_add3 = $row->supp_add3;
		$supp_city = $row->supp_city;
		$supp_state = $row->supp_state;
		$supp_phone = $row->supp_phone;
		$supp_amount = $row->supp_amount; //current year business
		$supp_amountbal = $row->supp_amountbal; //last year business
		$supp_grvalue = $row->supp_grvalue; //Pending order value
		$supp_termcondition = $row->supp_termcondition;
		$supp_incoterm = $row->supp_incoterm;
		$supp_payterm = $row->supp_payterm;
		$supp_advancepbl = $row->supp_advancepbl;
		$supp_incoplace = $row->supp_incoplace;
		$supp_paymode = $row->supp_paymode;
		$supp_insurancelib = $row->supp_insurancelib;
		$supp_insuranceterm = $row->supp_insuranceterm;
		$supp_taxdetail = $row->supp_taxdetail;
		$supp_attachnote = $row->supp_attachnote;
		$supp_itemmaped = $row->supp_itemmaped;
		$supp_itemdesc = $row->supp_itemdesc;
		$supp_itemdesc = str_replace("'","",$supp_itemdesc);
		$supp_itemdesc  = mb_convert_encoding($supp_itemdesc, "ISO-8859-1", "UTF-8");
		//full address
		$supp_addr = $supp_contactpersons."<br>".$supp_add1."&nbsp;".$supp_add2."&nbsp;".$supp_add3."&nbsp;".$supp_city."&nbsp;".$supp_state."<br>".$supp_phone;
?>

<div class="row" style="text-transform:uppercase">
	<div class="col-lg-2"><b><u>Vendor Code & Name</u><b><br /><?=$supp_code;?><br /><?=$supp_name;?></div>
    <div class="col-lg-2"><b><u>Contact Details</u><b><br /><?=$supp_addr; ?></div>
    <div class="col-lg-2"><b><u>Last Six Months Rating</u><b><br /></div>
    <div class="col-lg-2"><b><u>Current Year Business</u><b><br /><?=number_format($supp_amount,2,".","");?></div>
    <div class="col-lg-2"><b><u>Last Year Business</u><b><br /><?=number_format($supp_amountbal,2,".","");?></div>
    <div class="col-lg-2"><b><u>Pending Order Value</u><b><br /><?=number_format($supp_grvalue,2,".","");?></div>
</div><br /><br />

<div class="row" style="text-transform:uppercase">
	<div class="col-lg-12">
    	<h4 align="center"><u>Terms & Conditions</u></h4>
    </div>
</div><br /><br />

<div class="row" style="text-transform:uppercase">
	<div class="col-lg-2"><b><u>Terms & Condition</u><b><br /><?=$supp_termcondition;?></div>
	<div class="col-lg-2"><b><u>Inco Term</u><b><br /><?=$supp_incoterm;?></div>
    <div class="col-lg-2"><b><u>Payterm</u><b><br /><?=$supp_payterm;?></div>
    <div class="col-lg-2"><b><u>Insurance Term</u><b><br /><?=$supp_insuranceterm;?></div>
    <div class="col-lg-2"><b><u>Tax Detail</u><b><br /><?=$supp_taxdetail;?></div>
    <div class="col-lg-2"><b><u>Inco Place</u><b><br /><?=$supp_incoplace;?></div>
</div><br /><br />

<div class="row" style="text-transform:uppercase">
	<div class="col-lg-2"><b><u>Paymode</u><b><br /><?=$supp_paymode;?></div>
	<div class="col-lg-2"><b><u>Insurance Lib.</u><b><br /><?=$supp_insurancelib;?></div>
    <div class="col-lg-2"><b><u>Attached Note</u><b><br /><?=$supp_attachnote;?></div>
    <div class="col-lg-6"></div>
</div><br /><br />

<?php } ?>

<div class="row" style="text-transform:uppercase">
    <div class="col-lg-12"><b><u>Item Maped</u><b><br /><?=$supp_itemmaped;?></div>
</div><br /><br />

<div class="row" style="text-transform:uppercase">
    <div class="col-lg-12"><b><u>Item Description</u><b><br /><?=$supp_itemdesc;?></div>
</div><br /><br />

<!----- Pending Order ----->
<h4>Pending Order</h4>
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered">
        	<tr style="background-color:#0CF">
            	<td><b>Order No</b></td>
                <td><b>Line No‎</b></td>
                <td><b>Item Code‎</b></td>
                <td><b>Item Description‎</b></td>
                <td><b>Unit‎</b></td>
                <td><b>Order Qty‎</b></td>
                <td><b>Bal‎. Qty‎.‎</b></td>
                <td><b>Need ‎Date‎</b></td>
                <td><b>Delay‎</b></td>
                <td><b>WIP‎</b></td>
                <td><b>First Issue ‎Date</b></td>
                <td><b>Last Issue ‎Date‎</b></td>
                <td><b>Expected ‎Date‎</b></td>
                <td><b>Remark‎</b></td>
            </tr>
            <?php
				$sql_pend_odr = "select supp_rectype,* from tipldb..supp_card_tbl where supp_rectype = 'PendOrd'";
				$qry_pend_odr = $this->db->query($sql_pend_odr);
				foreach($qry_pend_odr->result() as $row){
					$supp_orderno = $row->supp_orderno;
					$supp_lineno = $row->supp_lineno;
					$supp_itemcode = $row->supp_itemcode;
					$supp_itemdesc = $row->supp_itemdesc;
					$supp_itemdesc = str_replace("'","",$supp_itemdesc);
					$supp_itemdesc  = mb_convert_encoding($supp_itemdesc, "ISO-8859-1", "UTF-8");
					$supp_itemuom = $row->supp_itemuom;
					$supp_orderqty = $row->supp_orderqty;
					$supp_balqty = $row->supp_balqty;
					$supp_needdt = $row->supp_needdt;
					$supp_delaydays = $row->supp_delaydays;
					$supp_wipdays = $row->supp_wipdays;
					$supp_firstissuedt = $row->supp_firstissuedt;
					$supp_lastissuedt = $row->supp_lastissuedt;
					$supp_excepteddt = $row->supp_excepteddt;
					$supp_remark = $row->supp_remark;
			?>
            <tr>
            	<td><?=$supp_orderno; ?></td>
                <td><?=$supp_lineno; ?></td>
                <td><?=$supp_itemcode; ?></td>
                <td><?=$supp_itemdesc; ?></td>
                <td><?=$supp_itemuom; ?></td>
                <td><?=$supp_orderqty; ?></td>
                <td><?=$supp_balqty; ?></td>
                <td><?=substr($supp_needdt,0,11); ?></td>
                <td><?=$supp_delaydays; ?></td>
                <td><?=$supp_wipdays; ?></td>
                <td><?=$supp_firstissuedt; ?></td>
                <td><?=$supp_lastissuedt; ?></td>
                <td><?=$supp_excepteddt; ?></td>
                <td><?=$supp_remark; ?></td>                
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

<!----- Executed Order ----->
<h4>Executed Order ‎:‎</h4>
<?php
$sql_exec_hdr = "select * from tipldb..supp_card_tbl where supp_rectype = 'ExeHD'";
$qry_exec_hdr = $this->db->query($sql_exec_hdr);
foreach($qry_exec_hdr->result() as $row){
	$supp_leadtime = $row->supp_leadtime;
	$supp_delaydays = $row->supp_delaydays;
	$supp_wipdays = $row->supp_wipdays;
	
	if($supp_leadtime != ''){
		$sup_leadtime = $supp_leadtime." days";
	}
	
	if($supp_delaydays != ''){
		$supp_delaydays = $supp_delaydays." days";
	}
	
	if($supp_wipdays != ''){
		$supp_wipdays = $supp_wipdays." days";
	}
?>
<div class="row" style="background-color:#0CF;">
    <div class="col-lg-3"><b>(Last 3 Months)</b></div>
    <div class="col-lg-3"><b>Average Lead Time:</b>&nbsp;<?=$sup_leadtime;?></div>
    <div class="col-lg-3"><b>Average Delay:‎</b>&nbsp;<?=$supp_delaydays;?></div>
    <div class="col-lg-3"><b>Average WIP‎:</b>&nbsp;<?=$supp_wipdays;?></div>
</div>
<?php } ?>

<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered">
        	<tr style="background-color:#0CF">
            	<td><b>Order No</b></td>
                <td><b>Line No‎</b></td>
                <td><b>Item Code‎</b></td>
                <td><b>Item Description‎</b></td>
                <td><b>Unit‎</b></td>
                <td><b>GRCPT No.</b></td>
                <td><b>GRCPT Status‎</b></td>
                <td><b>Order Qty</b></td>
                <td><b>Received Qty</b></td>
                <td><b>Accepted Qty</b></td>
                <td><b>Rejected Qty</b></td>
                <td><b>Returned Qty‎</b></td>
                <td><b>Balance Qty</b></td>
                <td><b>First Issue Date</b></td>
                <td><b>Last Issue Date</b></td>
                <td><b>Need Date</b></td>
                <td><b>Freeze Receipt Date</b></td>
                <td><b>Freeze Acceptance Date</b></td>
                <td><b>WIP Days</b></td>
                <td><b>Delay Days</b></td>
                <td><b>Lead Time</b></td>
            </tr>
            <?php
				$sql_exec_odr = "select * from tipldb..supp_card_tbl where supp_rectype = 'ExecOrd'";
				$qry_exec_odr = $this->db->query($sql_exec_odr);
				foreach($qry_exec_odr->result() as $row){
					$supp_orderno = $row->supp_orderno;
					$supp_lineno = $row->supp_lineno;
					$supp_itemcode = $row->supp_itemcode;
					$supp_itemdesc = $row->supp_itemdesc;
					$supp_itemdesc = str_replace("'","",$supp_itemdesc);
					$supp_itemdesc  = mb_convert_encoding($supp_itemdesc, "ISO-8859-1", "UTF-8");
					$supp_itemuom = $row->supp_itemuom;
					$supp_grcptno = $row->supp_grcptno;
					$supp_grcptstatus = $row->supp_grcptstatus;
					$supp_orderqty = $row->supp_orderqty;
					$supp_receiptqty = $row->supp_receiptqty;
					$supp_acceptedqty = $row->supp_acceptedqty;
					$supp_rejectedqty = $row->supp_rejectedqty;
					$supp_returnedqty = $row->supp_returnedqty;
					$supp_balqty = $row->supp_balqty;
					$supp_firstissuedt = $row->supp_firstissuedt;
					$supp_lastissuedt = $row->supp_lastissuedt;
					$supp_needdt = $row->supp_needdt;
					$supp_freezreceiptdt = $row->supp_freezreceiptdt;
					$supp_freezeacceptdt = $row->supp_freezeacceptdt;
					$supp_wipdays = $row->supp_wipdays;
					$supp_delaydays = $row->supp_delaydays;
					$supp_leadtime = $row->supp_leadtime;
			?>
            <tr>
            	<td><?=$supp_orderno; ?></td>
                <td><?=$supp_lineno; ?></td>
                <td><?=$supp_itemcode; ?></td>
                <td><?=$supp_itemdesc; ?></td>
                <td><?=$supp_itemuom; ?></td>
                <td><?=$supp_grcptno; ?></td>
                <td><?=$supp_grcptstatus; ?></td>
                <td><?=number_format($supp_orderqty,2,".",""); ?></td>
                <td><?=number_format($supp_receiptqty,2,".",""); ?></td>
                <td><?=number_format($supp_acceptedqty,2,".",""); ?></td>
                <td><?=number_format($supp_rejectedqty,2,".",""); ?></td>
                <td><?=number_format($supp_returnedqty,2,".",""); ?></td>
                <td><?=number_format($supp_balqty,2,".",""); ?></td>
                <td><?=substr($supp_firstissuedt,0,11); ?></td>
                <td><?=substr($supp_lastissuedt,0,11); ?></td>
                <td><?=substr($supp_needdt,0,11); ?></td>
                <td><?=substr($supp_freezreceiptdt,0,11); ?></td>
                <td><?=substr($supp_freezeacceptdt,0,11); ?></td>
                <td><?=number_format($supp_wipdays,2,".",""); ?></td>
                <td><?=number_format($supp_delaydays,2,".",""); ?></td>
                <td><?=number_format($supp_leadtime,2,".",""); ?></td>                
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

<!----- Pending Sobi ----->
<h4>Pending SOBI</h4>
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered">
        	<tr style="background-color:#0CF">
            	<td><b>Order No</b></td>
                <td><b>GRCPT No.</b></td>
                <td><b>GRCPT Date</b></td>
                <td><b>GRCPT Status</b></td>
                <td><b>Supplier Invoice No.</b></td>
                <td><b>GRCPT Value</b></td>
                <td><b>Freeze Move Date‎</b></td>
                <td><b>Need ‎Date‎</b></td>
                <td><b>Delay‎</b></td>
                <td><b>Payment Due Date‎</b></td>
            </tr>
            <?php
				$sql_pend_sobi = "select * from tipldb..supp_card_tbl where supp_rectype = 'Pendsobi'";
				$qry_pend_sobi = $this->db->query($sql_pend_sobi);
				foreach($qry_pend_sobi->result() as $row){
					$supp_orderno = $row->supp_orderno;
					$supp_grcptno = $row->supp_grcptno;
					$supp_grcptdt = $row->supp_grcptdt;
					$supp_grcptstatus = $row->supp_grcptstatus;
					$supp_invoiceno = $row->supp_invoiceno;
					$supp_grvalue = $row->supp_grvalue;
					$supp_freezeacceptdt = $row->supp_freezeacceptdt;
					$supp_needdt = $row->supp_needdt;
					$supp_delaydays = $row->supp_delaydays;
					$supp_voudt = $row->supp_voudt;
			?>
            <tr>
            	<td><?=$supp_orderno;?></td>
                <td><?=$supp_grcptno;?></td>
                <td><?=substr($supp_grcptdt,0,11);?></td>
                <td><?=$supp_grcptstatus;?></td>
                <td><?=$supp_invoiceno;?></td>
                <td><?=number_format($supp_grvalue,2,".","");?></td>
                <td><?=substr($supp_freezeacceptdt,0,11);?></td>
                <td><?=substr($supp_needdt,0,11);?></td>
                <td><?=number_format($supp_delaydays,2,".","");?></td>
                <td><?=substr($supp_voudt,0,11);?></td>                
            </tr>
            <?php } ?>
            <?php
				$sql_pend_sobi_tot = "select * from tipldb..supp_card_tbl where supp_rectype = 'GRtot'";
				$qry_pend_sobi_tot = $this->db->query($sql_pend_sobi_tot);
				foreach($qry_pend_sobi_tot->result() as $row){
					$supp_grvalue = $row->supp_grvalue;
			?>
            <tr>
            	<td colspan="5"><b>TOTAL</b></td>
                <td><?=number_format($supp_grvalue,2,".","");?></td>
                <td colspan="4"></td>                
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

<!----- Returned Notes ----->
<h4>Returned Notes</h4>
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered">
        	<tr style="background-color:#0CF">
            	<td><b>Return Note No.</b></td>
                <td><b>Order No.</b></td>
                <td><b>GRCPT No.</b></td>
                <td><b>GRCPT Status</b></td>
                <td><b>Item Code</b></td>
                <td><b>Item Description</b></td>
                <td><b>Order Qty</b></td>
                <td><b>Received Qty‎</b></td>
                <td><b>Accepted Qty</b></td>
                <td><b>Rejected Qty</b></td>
                <td><b>Returned Qty‎</b></td>
                <td><b>Sobi No.</b></td>
                <td><b>Sobi Amount‎</b></td>
                <td><b>Debit Note No.</b></td>
                <td><b>Debit Note Amount‎</b></td>
                <td><b>Net Paid</b></td>
            </tr>
            <?php
				$sql_ret_nt = "select * from tipldb..supp_card_tbl where supp_rectype = 'RtnNote'";
				$qry_ret_nt = $this->db->query($sql_ret_nt);
				foreach($qry_ret_nt->result() as $row){
					$supp_returnnotno = $row->supp_returnnotno;
					$supp_orderno = $row->supp_orderno;
					$supp_grcptno = $row->supp_grcptno;
					$supp_grcptstatus = $row->supp_grcptstatus;
					$supp_itemcode = $row->supp_itemcode;
					$supp_itemdesc = $row->supp_itemdesc;
					$supp_orderqty = $row->supp_orderqty;
					$supp_receiptqty = $row->supp_receiptqty;
					$supp_acceptedqty = $row->supp_acceptedqty;
					$supp_rejectedqty = $row->supp_rejectedqty;
					$supp_returnedqty = $row->supp_returnedqty;
					$supp_sobino = $row->supp_sobino;
					$supp_amountbal = $row->supp_amountbal;
					$supp_vouno = $row->supp_vouno;
					$supp_amount = $row->supp_amount;
					$supp_unadjamt = $row->supp_unadjamt;
			?>
            <tr>
            	<td><?=$supp_returnnotno;?></td>
                <td><?=$supp_orderno;?></td>
                <td><?=$supp_grcptno;?></td>
                <td><?=$supp_grcptstatus;?></td>
                <td><?=$supp_itemcode;?></td>
                <td><?=$supp_itemdesc;?></td>
                <td><?=number_format($supp_orderqty,2,".","");?></td>
                <td><?=number_format($supp_receiptqty,2,".","");?></td>
                <td><?=number_format($supp_acceptedqty,2,".","");?></td>
                <td><?=number_format($supp_rejectedqty,2,".","");?></td>
                <td><?=number_format($supp_returnedqty,2,".","");?></td>
                <td><?=$supp_sobino;?></td> 
                <td><?=number_format($supp_amountbal,2,".","");?></td>
                <td><?=$supp_vouno;?></td>
                <td><?=number_format($supp_amount,2,".","");?></td>
                <td><?=number_format($supp_unadjamt,2,".","");?></td>               
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

<!----- Unadjusted Sobi/Ven And Credit Notes ----->
<h4>Unadjusted Sobi/Ven And Credit Notes</h4>
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered">
        	<tr style="background-color:#0CF">
            	<td><b>Document No.</b></td>
                <td><b>Status.</b></td>
                <td><b>Date.</b></td>
                <td><b>Supplier Invoice No.</b></td>
                <td><b>GRCPT No.</b></td>
                <td><b>Order No.</b></td>
                <td><b>Payterm</b></td>
                <td><b>Amount</b></td>
                <td><b>Balance Amount</b></td>
                <td><b>Delay</b></td>
                <td><b>Freeze Movement Date</b></td>
                <td><b>Supplier Invoice Date.</b></td>
                <td><b>Anchor Date‎</b></td>
                <td><b>Due Date</b></td>
                <td><b>Tran Type‎</b></td>
            </tr>
            <?php
				$sql_unadjt_sobi = "select * from tipldb..supp_card_tbl where supp_rectype = 'PenPym'";
				$qry_unadjt_sobi = $this->db->query($sql_unadjt_sobi);
				foreach($qry_unadjt_sobi->result() as $row){
					$supp_vouno = $row->supp_vouno;
					$supp_grcptstatus = $row->supp_grcptstatus;
					$supp_voudt = $row->supp_voudt;
					$supp_invoiceno = $row->supp_invoiceno;
					$supp_grcptno = $row->supp_grcptno;
					$supp_orderno = $row->supp_orderno;
					$supp_payterm = $row->supp_payterm;
					$supp_amount = $row->supp_amount;
					$supp_amountbal = $row->supp_amountbal;
					$supp_delaydays = $row->supp_delaydays;
					$supp_freezeacceptdt = $row->supp_freezeacceptdt;
					$supp_grcptdt = $row->supp_grcptdt;
					$supp_lastissuedt = $row->supp_lastissuedt;
					$supp_sobino = $row->supp_sobino;
			?>
            <tr>
            	<td><?=$supp_vouno;?></td>
                <td><?=$supp_grcptstatus;?></td>
                <td><?=substr($supp_voudt,0,11);?></td>
                <td><?=$supp_invoiceno;?></td>
                <td><?=$supp_grcptno;?></td>
                <td><?=$supp_orderno;?></td>
                <td><?=$supp_payterm;?></td>
                <td><?=number_format($supp_amount,2,".","");?></td>
                <td><?=number_format($supp_amountbal,2,".","");?></td>
                <td><?=number_format($supp_delaydays,2,".","");?></td>
                <td><?=substr($supp_freezeacceptdt,0,11);?></td>
                <td><?=substr($supp_freezeacceptdt,0,11);?></td>
                <td><?=substr($supp_grcptdt,0,11);?></td>
                <td><?=substr($supp_lastissuedt,0,11);?></td>
                <td><?=$supp_sobino;?></td>             
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

<!----- Unadjusted Debit Documents ----->
<h4>Unadjusted Debit Documents</h4>
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered">
        	<tr style="background-color:#0CF">
            	<td><b>Document No‎.</b></td>
                <td><b>Status</b></td>
                <td><b>Date‎</b></td>
                <td><b>Remarks‎</b></td>
                <td><b>Paid On‎</b></td>
                <td><b>Amount‎</b></td>
                <td><b>UnAdjusted‎ Amt‎.‎</b></td>
                <td><b>Tran Type</b></td>
            </tr>
            <?php
				$sql_debt_doc = "select * from tipldb..supp_card_tbl where supp_rectype = 'UAdjDn'";
				$qry_debt_doc = $this->db->query($sql_debt_doc);
				foreach($qry_debt_doc->result() as $row){
					$supp_vouno = $row->supp_vouno;
					$supp_grcptstatus = $row->supp_grcptstatus;
					$supp_voudt = $row->supp_voudt;
					$supp_remark = $row->supp_remark;
					$supp_paidon = $row->supp_paidon;
					$supp_amount = $row->supp_amount;
					$supp_unadjamt = $row->supp_unadjamt;
					$supp_sobino = $row->supp_sobino;
			?>
            <tr>
            	<td><?=$supp_vouno;?></td>
                <td><?=$supp_grcptstatus;?></td>
                <td><?=$supp_voudt;?></td>
                <td><?=$supp_remark;?></td>
                <td><?=$supp_paidon;?></td>
                <td><?=$supp_amount;?></td>
                <td><?=$supp_unadjamt;?></td>
                <td><?=$supp_sobino;?></td>            
            </tr>
            <?php } ?>
            <?php
				$sql_debt_doc_tot = "select * from tipldb..supp_card_tbl where supp_rectype = 'UNtot'";
				$qry_debt_doc_tot = $this->db->query($sql_debt_doc_tot);
				foreach($qry_debt_doc_tot->result() as $row){
					$supp_amountbal = $row->supp_amountbal;
					$supp_vouno = $row->supp_vouno;
					$supp_unadjamt = $row->supp_unadjamt;
			?>
            <tr>
            	<td colspan="6"><b>Total Unadj‎. ‎Amt‎.  ‎DR‎.</b></td>
                <td><?=$supp_unadjamt;?></td>
                <td></td>            
            </tr>
            <tr>
            	<td colspan="6"><b>Net Balance <?=$supp_vouno;?></b></td>
                <td><?=$supp_amountbal;?></td>
                <td></td>            
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

