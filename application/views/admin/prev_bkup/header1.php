<?php $this->load->helper('status'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Bootstrap Admin Templete">
        <meta name="author" content="Chandra Narayan">
        <meta name="keyword" content="Administration Panel">
        <title>TIPL Admin Panel</title>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/admin/img/favicon.png">
        <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap-theme.css" rel="stylesheet">
        <!-- ICONS CLASS -->
        <link href="<?php echo base_url(); ?>assets/admin/css/elegant-icons-style.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/admin/css/font-awesome.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/admin/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>assets/admin/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/owl.carousel.css" type="text/css">
        <link href="<?php echo base_url(); ?>assets/admin/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/fullcalendar.css">
        <link href="<?php echo base_url(); ?>assets/admin/css/widgets.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/admin/css/style-responsive.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/admin/css/xcharts.min.css" rel=" stylesheet">	
        <link href="<?php echo base_url(); ?>assets/admin/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/admin/css/jquery-ui.css" rel="stylesheet"/>
        <?php //select 2 box ?>
        <link href="<?php echo base_url(); ?>assets/admin/classes/bootstrap.css" rel="stylesheet"/>
        <link href="<?php echo base_url(); ?>assets/admin/css/select2.min.css" rel="stylesheet"/>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/select2.min.js"></script>
        <!--- Data Table Scripts --->
        <script src="<?php echo base_url(); ?>assets/admin/js/dataTables.bootstrap.min.css"></script>
        <script src="<?php echo base_url(); ?>assets/admin/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/admin/js/dataTables.bootstrap.min.js"></script>
		<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/ColReorderWithResize.js"></script>
        <?php $live_url="http://live.tipl.com/tipl_project1/"; ?>
        
        <style>
        /* table border */
        .table th{
			border: #000 solid 1px !important;
			color:#000;
			font-weight:bold;
        }
        
        .table td{
        	border: #000 solid 1px !important;
        }
        
        #myBtn {
			display: none;
			position: fixed;
			bottom: 20px;
			right: 30px;
			z-index: 99;
			font-size: 18px;
			border: none;
			outline: none;
			background-color: red;
			color: white;
			cursor: pointer;
			padding: 10px;
			border-radius: 4px;
        }
        
        #myBtn:hover {
        	background-color: #555;
        }
        
        </style>
    
    </head>

    <body>
    <button onclick="topFunction()" id="myBtn" title="Go to top">Top</button>
    <?php 
    $username = $_SESSION['username'];
    if($username == ''){
        header('Location: http://live.tipl.com/pr/index.php/login');
    }
    
    //Fetching User Other Details
    $user_email = $username."@tipl.com";
    
    $sql_user_det = "select id,user_type,name,* from tipldb..login where email like '$user_email%' and  emp_active = 'Yes'";
    $qry_user_det = $this->db->query($sql_user_det);
    
    foreach($qry_user_det->result() as $row){
        $user_id =  $row->id;
        $user_type = $row->user_type;
        $name = $row->name;
    } 
    ?>
        <section id="container" class="sidebar-closed">
            <header class="header dark-bg">
                <div class="toggle-nav">
                    <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i class="icon_menu"></i></div>
                </div>
                
                <a href="<?php echo base_url(); ?>index.php/welcome/dashboard" class="logo">TIPL <span class="lite">PURCHASE</span></a>
                
                <div class="top-nav notification-row">
                    <ul class="nav pull-right top-menu">
                      <li><a href="#"><?php echo "Hello ! ".$username; ?></a></li>
                      <li><a href="<?php echo base_url(); ?>index.php/logout"><i class="icon_key_alt"></i> Log Out</a></li>
                    </ul>
                </div>
            </header>
            <aside>
              <div id="sidebar"  class="nav-collapse">
                  <ul class="sidebar-menu">                
                      <li class="active">
                          <a class="" href="<?php echo base_url(); ?>index.php/welcome/dashboard">
                              <span style="font-size:14px">Dashboard</span>
                          </a>
                      </li>
                      <!--- PPC New --->
                      <li class="active">
                          <a class="" href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw">
                              <span style="font-size:14px">PPC</span>
                          </a>
                      </li>
                      <!-- PPC New --->
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/flowc">
                              <span style="font-size:14px">Pending<br>Summary</span>
                          </a>
                      </li>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/pr_cycletimec/">
                              <span style="font-size:14px">PR-PO-GRCPT CT</span>
                          </a>
                      </li>
                      
                      <?php if($username == 'admin' || $username == 'abhinav.toshniwal'){ ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/createpoc_lvl1/cost_sheet">
                              <span style="font-size:14px">Cost Sheet</span>
                          </a>
                      </li>
                      <?php } ?>
                      
                      <?php if($username == 'admin' || $username == 'admin'){ ?>
                      <?php /*?><li>
                          <a class="" href="<?php echo base_url(); ?>index.php/currencyc">
                              <span style="font-size:14px">Currency</span>
                          </a>
                      </li><?php */?>
                      <?php } ?>
                      
                      <?php if($username == 'admin' || $username == 'abhinav.toshniwal'){ ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/itemc/index">
                              <span style="font-size:14px">Item Report</span>
                          </a>
                      </li>
                      <?php } ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/mrp_reportc">
                              <span style="font-size:14px">MRP Report</span>
                          </a>
                      </li>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/mrp_reportc/mrp_item_report">
                              <span style="font-size:14px">Item Request Report</span>
                          </a>
                      </li>
                      
                      <?php /*?><li>
                          <a class="" href="<?php echo base_url(); ?>index.php/po_fy_reportc/pr_po_pending_report">
                              <span style="font-size:14px">PR PO Report</span>
                          </a>
                      </li><?php */?>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/pending_poc/">
                              <span style="font-size:14px">Pending PR/PO Report</span>
                          </a>
                      </li>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/po_history_itemwisec/index">
                              <span style="font-size:14px">PO History Itemwise</span>
                          </a>
                      </li>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/po_reportc/index">
                              <span style="font-size:14px">PO History</span>
                          </a>
                      </li>
                      
                      <?php if($username == 'admin' || $username == 'abhinav.toshniwal'){ ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/pr_reportc/index">
                              <span style="font-size:14px">PR History</span>
                          </a>
                      </li>
                      <?php } ?>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/pr_reportc/pr_report_main_new">
                              <span style="font-size:14px">PR/PO Age Report</span>
                          </a>
                      </li>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/view_prc/">
                              <span style="font-size:14px">View PR</span>
                          </a>
                      </li>
                      
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/view_poc/">
                              <span style="font-size:14px">View PO</span>
                          </a>
                      </li>
                      <?php /*?><li>
                          <a class="" href="<?php echo base_url(); ?>index.php/pendalc/">
                              <span style="font-size:14px">Pendal Card</span>
                          </a>
                      </li><?php */?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/supplier_reportc/">
                              <span style="font-size:14px">Supplier Analysis Report</span>
                          </a>
                      </li>
                      
                      <?php if($username == 'admin'){ ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/rightc/">
                              <span style="font-size:14px">Manage Rights</span>
                          </a>
                      </li>
                      <?php } ?>
                      
                      <?php if($username == 'admin' || $username == 'sandeep.tak'){ ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/createpodc/po_desc_edit_main/">
                              <span style="font-size:14px">Edit Desc FPO</span>
                          </a>
                      </li>
                      <?php } ?>
                      
                      <?php if($username == 'admin' || $username == 'abhinav.toshniwal'){ ?>
                      <li>
                          <a class="" href="<?php echo base_url(); ?>index.php/po_master_updatec/">
                              <span style="font-size:14px">PO Master Update</span>
                          </a>
                      </li>
                      <?php } ?>
                  </ul>
              </div>
            </aside>