<!--main content start-->
<!--******* ITEM INFORMATION *******-->
<form action="<?php echo base_url(); ?>index.php/drawingc/insert_pr_sub" method="post" enctype="multipart/form-data" onsubmit="return reqd()">
 <div class="row">
    <div class="col-lg-12" style=" overflow-x:auto;">
     <table class="table table-bordered" id="dataTable" border="1">
        <thead>
          <tr>
            <th>Item Code</th>
            <th>Item Description</th>
            <th>Last Yr Cons.</th>
            <th>Current Stock</th>
            <th>Total Incoming Stock</th>
            <th>Reservation QTY</th>
            <th>Authorized QTY</th>
            <th>Last Price</th>
            <th>Transanction UOM</th>
            <th>Need Date</th>
            <th>Warehouse Code</th>
            <th>Cost</th>
            <th>Remarks (On Calculating Cost)</th>
            <th>Costing Not Available Remarks</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($v->result() as $row)
            { 
                $pr_date1 = substr($row->preqm_prdate,0, 11);
                $pr_date = date("d-m-Y", strtotime($pr_date1));
                $pr_num = $row->prqit_prno;
                $item_code = $row->prqit_itemcode;
                $item_code1 = urlencode($item_code);
                
                if(strpos($item_code1, '%2F') !== false)
                {
                    $item_code2 = str_replace("%2F","chandra",$item_code1);
                }
                else 
                {
                    $item_code2 = $item_code1;
                }
                
                $req_qty = $row->prqit_reqdqty;
                $req_qty1= number_format($req_qty,2);
                $auth_qty = $row->prqit_authqty;
                $auth_qty1 = number_format($auth_qty,2);
                $uom = $row->prqit_puom;
                $need_date1 = substr($row->prqit_needdate,0,11);
                $need_date = date("d-m-Y", strtotime($need_date1));
                $wh_code = $row->prqit_warehousecode;
                
                $sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
                $sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
                $sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
                $sql5 ="select MAX(LastRate) as LastRate from tipldb..pendalcard_rkg where flag='ItemLastFiveTrans' and itemcode='$item_code'";
                $sql6 ="select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
                $sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
				$sql8 ="select * from TIPLDB..pr_submit_table where pr_num = '$pr_num'";
                
                $query2 = $this->db->query($sql2);
                $query3 = $this->db->query($sql3);
                $query4 = $this->db->query($sql4);
                $query5 = $this->db->query($sql5);
                $query6 = $this->db->query($sql6);
                $query7 = $this->db->query($sql7);
				$query8 = $this->db->query($sql8);
                    
                if ($query2->num_rows() > 0) {
                  foreach ($query2->result() as $row) {
                      $lastyr_cons = $row->ConsTotalQty;
                      $lastyr_cons1 = number_format($lastyr_cons,2);
                    }
                } else {
                      $lastyr_cons = 0;
                      $lastyr_cons1 = 0.00;
                }
                
                if ($query3->num_rows() > 0) {
                      $current_stk = 0;
                  foreach ($query3->result() as $row) {
                      $current_stock = $row->ItemStkAccepted;
                      $current_stk = $current_stk + $current_stock;
                      $current_stk1 = number_format($current_stk,2);
                    }
                } else {
                      $current_stk = 0;
                      $current_stk1 = 0.00;
                }
                
                if ($query4->num_rows() > 0) {
                  foreach ($query4->result() as $row) {
                      $reservation_qty = $row->AllocPendingTot;
                      $reservation_qty1 = number_format($reservation_qty,2);
                    }
                } else {
                      $reservation_qty = 0;
                      $reservation_qty1 = 0.00;
                }
                
                if ($query5->num_rows() > 0) {
                  foreach ($query5->result() as $row) {
                      $last_price = $row->LastRate;
                      $last_price1 = number_format($last_price,2);
                    }
                } else {
                      $last_price = 0;
                      $last_price1 = 0.00;
                }
                
                if ($query6->num_rows() > 0) {
                  foreach ($query6->result() as $row) {
                      $item_desc = $row->ItemDescription;
                    }
                } else {
                      $item_desc = "";
                }
                
                if ($query7->num_rows() > 0) {
                    $incoming_qty_tot1 = 0;
                  foreach ($query7->result() as $row) {
                      $incoming_qty = $row->PendPoSchQty;
                      $incoming_qty_tot1 = $incoming_qty_tot + $incoming_qty;
                      $incoming_qty_tot  = number_format($incoming_qty_tot1,2);
                    }
                } else {
                      $incoming_qty_tot = "";
                }
				
                foreach ($query8->result() as $row) {
					$costing = $row->costing;
					$cost_calculation_remarks = $row->cost_calculation_remarks;
					$cost_not_available_remarks = $row->cost_not_available_remarks;
                }       
          ?>
          <tr>
                <?php
				   echo "<input type='hidden' name='pr_num' value='$pr_num' />";
                   echo "<input type='hidden' name='pr_date' value='$pr_date' />";
                ?>
            <td>
                <?php echo $item_code; echo "<input type='hidden' name='item_code' value='$item_code' />";?><br />
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" 
                target="_blank">PendalCard</a>
            </td>
            <td>
                <?php 
                    echo $item_desc; 
                    echo "<input type='hidden' name='itm_desc' value='$item_desc' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo $lastyr_cons1; 
                    echo "<input type='hidden' name='lastyr_cons' value='$lastyr_cons1' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo $current_stk1; 
                    echo "<input type='hidden' name='current_stk' value='$current_stk1' />";		  
                ?>
            </td>
            <td>
                <?php
                   echo $incoming_qty_tot;
                   echo "<input type='hidden' name='pr_date' value='$incoming_qty_tot1' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $reservation_qty1; 
                    echo "<input type='hidden' name='reservation_qty' value='$reservation_qty1' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo $auth_qty1; 
                    echo "<input type='hidden' name='authorise_qty' value='$auth_qty1' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $last_price1; 
                    echo "<input type='hidden' name='last_price' value='$last_price1' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $uom; 
                    echo "<input type='hidden' name='trans_uom' value='$uom' />";
                ?>
            </td> 
            <td>
                <?php
                    echo $need_date;
                    echo "<input type='hidden' name='need_date' value='$need_date' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $wh_code; 
                    echo "<input type='hidden' name='warehse_code' value='$wh_code' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $costing; 
                    echo "<input type='hidden' name='costing' value='$costing' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $cost_calculation_remarks; 
                    echo "<input type='hidden' name='cost_calculation_remarks' value='$cost_calculation_remarks' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $cost_not_available_remarks; 
                    echo "<input type='hidden' name='cost_not_available_remarks' value='$cost_not_available_remarks' />";
                ?>
            </td>                          
          </tr>
          <?php }  ?> 
        </tbody>
      </table>
  </div>
</div><br /><br />
<?php
    $sql8 ="select * from TIPLDB..pr_submit_table where pr_num = '$pr_num'";
    
    $query8 = $this->db->query($sql8);
    
    if ($query8->num_rows() > 0) {
      foreach ($query8->result() as $row) {
          $usage              = $row->usage;
          $category           = $row->category;
          $wo_no              = $row->work_odr_no;
          $sono               = $row->sono;
          $customer_name      = $row->customer_name;
          $drawing_no         = $row->drawing_no;
          $pm_group           = $row->pm_group;
          $project_name       = $row->project_name;
          $atac_no            = $row->atac_no;
          $atac_ld_date       = $row->atac_ld_date;
          $atac_need_date     = $row->atac_need_date;
          $atac_payment_terms = $row->atac_payment_terms;
          $remarks            = $row->remarks;
          $project_target_date = $row->project_target_date;
          $test_cert_req      = $row->test_cert;
		  $manufact_clearnace = $row->manufact_clearance;
		  $dispatch_inst      = $row->dispatch_inst;
		  $pr_supp_remarks    = $row->pr_supp_remarks;
?>
<div class="row">
    <div class="col-lg-2">
        <b>Usage :- </b><?php echo $usage; ?>
    </div>
    <div class="col-lg-2">
        <b>category :- </b><?php echo $category; ?>
    </div>
    <div class="col-lg-2">
        <b>Work Order No :- </b><?php echo $wo_no; ?>
    </div>
    <div class="col-lg-2">
        <b>SO No :- </b><?php echo $sono; ?>
    </div>
    <div class="col-lg-2">
        <b>Customer Name :- </b><?php echo $customer_name; ?>
    </div>
    <div class="col-lg-2">
        <b>Drawing No :- </b><?php echo $drawing_no; ?> 
    </div>
</div><br />
<div class="row">
    <div class="col-lg-2">
        <b>PM Group :- </b><?php echo $pm_group; ?>
    </div>
    <div class="col-lg-2">
        <b>Project Name :- </b><?php echo $project_name; ?>
    </div>
    <div class="col-lg-2">
        <b>ATAC No. :- </b><?php echo $atac_no; ?>
    </div>
    <div class="col-lg-2">
        <b>ATAC LD Date :- </b><?php echo $atac_ld_date; ?>
    </div>
    <div class="col-lg-2">
        <b>ATAC Need Date :- </b><?php echo $atac_need_date; ?>
    </div>
    <div class="col-lg-2">
        <b>ATAC Payment Terms :- </b><?php echo $atac_payment_terms; ?>
    </div>
</div><br />
<div class="row">
    <div class="col-lg-2">
        <b>PR Remarks :- </b><?php echo $remarks; ?>
    </div>
    <div class="col-lg-2">
        <b>Special Remarks For Supplier :- </b><?php echo $pr_supp_remarks; ?>
    </div>
    <div class="col-lg-2">
       <b>Project Target Date :- </b><?php echo $project_target_date; ?> 
    </div>
    <div class="col-lg-2">
        <b>Test Cert. Req. :- </b><?php echo $test_cert_req; ?>
    </div>
    <div class="col-lg-2">
        <b>Manufacturing Clearnace :- </b><?php echo $manufact_clearnace; ?>
    </div>
    <div class="col-lg-2">
    	<b>Dispatch Instruction :- </b><?php echo $dispatch_inst; ?>
    </div> 
</div><br />
<div class="row">
	<div class="col-lg-2">
        <b>Attach Drawing</b>
    </div>
    <div class="col-lg-4">
        <input type='file' name='attach_drawing'  id='attach_drawing' class='form-control'/> 
    </div>
    <div class="col-lg-6">
    </div>
</div><br />
<?php
    }
}
?>

 <div class="row">
    <div class="col-lg-5">    
    </div>
    <div class="col-lg-2"> 
    <input type="submit" name="submt" value="SUBMIT" class="form-control" style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px" />     
    </div>
    <div class="col-lg-5">    
    </div>
</div>

</form><br /><br />
<!--*********** Current Year Consumption & Receipt **********-->
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
        Month Wise Current Year Consumption &amp; Receipt
    </div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>TYPE‎</th>
                <th>APR‎</th>
                <th>MAY‎‎</th>
                <th>JUN</th>
                <th>JUL</th>
                <th>AUG‎</th>
                <th>SEP‎</th>
                <th>OCT‎</th>
                <th>NOV‎</th>
                <th>DEC</th>
                <th>JAN‎</th>
                <th>FEB</th>
                <th>MAR</th>
                <th>TOTAL‎</th>
                <th>AVERAGE</th>
              </tr>
            </thead>
            <tbody>
			  <?php 
              foreach ($mnthwsecurryrcons1->result() as $row){
              $fin_yr  = $row->SuppName; 
              $apr 	 = $row->CurrYrConsRecptApril;
              $apr_new = number_format($row->CurrYrConsRecptApril,2);
              $may     = $row->CurrYrConsRecptMay;
              $may_new = number_format($row->CurrYrConsRecptMay,2);
              $jun     = $row->CurrYrConsRecptJune;
              $jun_new = number_format($row->CurrYrConsRecptJune,2);
              $jul     = $row->CurrYrConsRecptJuly;
              $jul_new = number_format($row->CurrYrConsRecptJuly,2);
              $aug     = $row->CurrYrConsRecptAugust;
              $aug_new = number_format($row->CurrYrConsRecptAugust,2);
              $sep     = $row->CurrYrConsRecptSeptember;
              $sep_new = number_format($row->CurrYrConsRecptSeptember,2);
              $oct     = $row->CurrYrConsRecptOctober;
              $oct_new = number_format($row->CurrYrConsRecptOctober,2);
              $nov     = $row->CurrYrConsRecptNovember;
              $nov_new = number_format($row->CurrYrConsRecptNovember,2);
              $dec     = $row->CurrYrConsRecptDecember;
              $dec_new = number_format($row->CurrYrConsRecptDecember,2);
              $jan     = $row->CurrYrConsRecptJan;
              $jan_new = number_format($row->CurrYrConsRecptJan,2);
              $feb     = $row->CurrYrConsRecptFeb;
              $feb_new = number_format($row->CurrYrConsRecptFeb,2);
              $mar     = $row->CurrYrConsRecptMarch;
              $mar_new = number_format($row->CurrYrConsRecptMarch,2);
			  
			  if($apr == '' || $apr == NULL || $apr == 0){
				  $div_apr = 0;
			  } else {
				  $div_apr = 1;
			  }
			  
			  if($may == '' || $may == NULL || $may == 0){
				  $div_may = 0;
			  } else {
				  $div_may = 1;
			  }
			  
			  if($jun == '' || $jun == NULL || $jun == 0){
				  $div_jun = 0;
			  } else {
				  $div_jun = 1;
			  }
			  
			  if($jul == '' || $jul == NULL || $jul == 0){
				  $div_jul = 0;
			  } else {
				  $div_jul = 1;
			  }
			  
			  if($aug == '' || $aug == NULL || $aug == 0){
				  $div_aug = 0;
			  } else {
				  $div_aug = 1;
			  }
			  
			  if($sep == '' || $sep == NULL || $sep == 0){
				  $div_sep = 0;
			  } else {
				  $div_sep = 1;
			  }
			  
			  if($oct == '' || $oct == NULL || $oct == 0){
				  $div_oct = 0;
			  } else {
				  $div_oct = 1;
			  }
			  
			  if($nov == '' || $nov == NULL || $nov == 0){
				  $div_nov = 0;
			  } else {
				  $div_nov = 1;
			  }
			  
			  if($dec == '' || $dec == NULL || $dec == 0){
				  $div_dec = 0;
			  } else {
				  $div_dec = 1;
			  }
			  
			  if($jan == '' || $jan == NULL || $jan == 0){
				  $div_jan = 0;
			  } else {
				  $div_jan = 1;
			  }
			  
			  if($feb == '' || $feb == NULL || $feb == 0){
				  $div_feb = 0;
			  } else {
				  $div_feb = 1;
			  }
			  
			  if($mar == '' || $mar == NULL || $mar == 0){
				  $div_mar = 0;
			  } else {
				  $div_mar = 1;
			  }
			  
              $tot     = $apr+$may+$jun+$jul+$aug+$sep+$oct+$nov+$dec+$jan+$feb+$mar;
			  $div_tot = $div_apr+$div_may+$div_jun+$div_jul+$div_aug+$div_sep+$div_oct+$div_nov+$div_dec+$div_jan+$div_feb+$div_mar;
              $tot_new = number_format($tot,2);
              $avg     = $tot/$div_tot;
              $avg_new = number_format($avg,2);
              ?>
              <tr>
                <td>Consumption ( <?php echo $fin_yr; ?> )</td>
                <td><?php echo $apr_new; ?></td>
                <td><?php echo $may_new; ?></td>
                <td><?php echo $jun_new; ?></td>
                <td><?php echo $jul_new; ?></td>
                <td><?php echo $aug_new; ?></td>
                <td><?php echo $sep_new; ?></td>
                <td><?php echo $oct_new; ?></td>
                <td><?php echo $nov_new; ?></td>
                <td><?php echo $dec_new; ?></td>
                <td><?php echo $jan_new; ?></td>
                <td><?php echo $feb_new; ?></td>
                <td><?php echo $mar_new; ?></td>
                <td><?php echo $tot_new; ?></td>
                <td><?php echo $avg_new; ?></td>                          
              </tr>
              <?php } ?>
              <?php 
			  foreach ($mnthwsecurryrrcpt1->result() as $row){
			  $fin_yr1  = $row->SuppName; 
              $apr1     = $row->CurrYrConsRecptApril;
              $apr1_new = number_format($row->CurrYrConsRecptApril,2);
              $may1     = $row->CurrYrConsRecptMay;
              $may1_new = number_format($row->CurrYrConsRecptMay,2);
              $jun1     = $row->CurrYrConsRecptJune;
              $jun1_new = number_format($row->CurrYrConsRecptJune,2);
              $jul1     = $row->CurrYrConsRecptJuly;
              $jul1_new = number_format($row->CurrYrConsRecptJuly,2);
              $aug1     = $row->CurrYrConsRecptAugust;
              $aug1_new = number_format($row->CurrYrConsRecptAugust,2);
              $sep1     = $row->CurrYrConsRecptSeptember;
              $sep1_new = number_format($row->CurrYrConsRecptSeptember,2);
              $oct1     = $row->CurrYrConsRecptOctober;
              $oct1_new = number_format($row->CurrYrConsRecptOctober,2);
              $nov1     = $row->CurrYrConsRecptNovember;
              $nov1_new = number_format($row->CurrYrConsRecptNovember,2);
              $dec1     = $row->CurrYrConsRecptDecember;
              $dec1_new = number_format($row->CurrYrConsRecptDecember,2);
              $jan1     = $row->CurrYrConsRecptJan;
              $jan1_new = number_format($row->CurrYrConsRecptJan,2);
              $feb1     = $row->CurrYrConsRecptFeb;
              $feb1_new = number_format($row->CurrYrConsRecptFeb,2);
              $mar1     = $row->CurrYrConsRecptMarch;
              $mar1_new = number_format($row->CurrYrConsRecptMarch,2);
			  
			  if($apr1 == '' || $apr1 == NULL || $apr1 == 0){
				  $div_apr1 = 0;
			  } else {
				  $div_apr1 = 1;
			  }
			  
			  if($may1 == '' || $may1 == NULL || $may1 == 0){
				  $div_may1 = 0;
			  } else {
				  $div_may1 = 1;
			  }
			  
			  if($jun1 == '' || $jun1 == NULL || $jun1 == 0){
				  $div_jun1 = 0;
			  } else {
				  $div_jun1 = 1;
			  }
			  
			  if($jul1 == '' || $jul1 == NULL || $jul1 == 0){
				  $div_jul1 = 0;
			  } else {
				  $div_jul1 = 1;
			  }
			  
			  if($aug1 == '' || $aug1 == NULL || $aug1 == 0){
				  $div_aug1 = 0;
			  } else {
				  $div_aug1 = 1;
			  }
			  
			  if($sep1 == '' || $sep1 == NULL || $sep1 == 0){
				  $div_sep1 = 0;
			  } else {
				  $div_sep1 = 1;
			  }
			  
			  if($oct1 == '' || $oct1 == NULL || $oct1 == 0){
				  $div_oct1 = 0;
			  } else {
				  $div_oct1 = 1;
			  }
			  
			  if($nov1 == '' || $nov1 == NULL || $nov1 == 0){
				  $div_nov1 = 0;
			  } else {
				  $div_nov1 = 1;
			  }
			  
			  if($dec1 == '' || $dec1 == NULL || $dec1 == 0){
				  $div_dec1 = 0;
			  } else {
				  $div_dec1 = 1;
			  }
			  
			  if($jan1 == '' || $jan1 == NULL || $jan1 == 0){
				  $div_jan1 = 0;
			  } else {
				  $div_jan1 = 1;
			  }
			  
			  if($feb1 == '' || $feb1 == NULL || $feb1 == 0){
				  $div_feb1 = 0;
			  } else {
				  $div_feb1 = 1;
			  }
			  
			  if($mar1 == '' || $mar1 == NULL || $mar1 == 0){
				  $div_mar1 = 0;
			  } else {
				  $div_mar1 = 1;
			  }
			  
              $tot1     = $apr1+$may1+$jun1+$jul1+$aug1+$sep1+$oct1+$nov1+$dec1+$jan1+$feb1+$mar1;
			  $div_tot1 = $div_apr1+$div_may1+$div_jun1+$div_jul1+$div_aug1+$div_sep1+$div_oct1+$div_nov1+$div_dec1+$div_jan1+$div_feb1+$div_mar1;
              $tot1_new = number_format($tot1,2);
              $avg1     = $tot1/$div_tot1;
              $avg1_new = number_format($avg1,2);
              ?>
              <tr>
                <td>Receipt ( <?php echo $fin_yr1; ?> ) </td>
                <td><?php echo $apr1_new; ?></td>
                <td><?php echo $may1_new; ?></td>
                <td><?php echo $jun1_new; ?></td>
                <td><?php echo $jul1_new; ?></td>
                <td><?php echo $aug1_new; ?></td>
                <td><?php echo $sep1_new; ?></td>
                <td><?php echo $oct1_new; ?></td>
                <td><?php echo $nov1_new; ?></td>
                <td><?php echo $dec1_new; ?></td>
                <td><?php echo $jan1_new; ?></td>
                <td><?php echo $feb1_new; ?></td>
                <td><?php echo $mar1_new; ?></td>
                <td><?php echo $tot1_new; ?></td>
                <td><?php echo $avg1_new; ?></td>                          
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />

<?php 
$fin_yr;
$curr_yr = substr($fin_yr,3,4);
$last_yr1 = $curr_yr-1;
$last_yr2 = substr($curr_yr,2,2);
$last_fin_yr = "( FY-".$last_yr1."-".$last_yr2." )"; 
?>
            
<!--********* Yearly Consumption Of Past 1 Years *******-->
          
<!--***** RECORDER LEVEL RECORDER QUANTITY ******-->
          
<div class="row">
    <div class="col-lg-6" style="font-weight:bold; font-size:15px;">
          Last year Consumption‎ &amp; Receipt
    </div>
    <div class="col-lg-6" style="font-weight:bold; font-size:15px;">
          Reorder Level Reorder Quantity
    </div>
</div>
          		
 <div class="row">
    <div class="col-lg-6">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Ty‎pe‎</th>
                <th>Qty‎.‎</th>
                <th>Value‎</th>
              </tr>
            </thead>
            <tbody>
              <?php 
			  foreach ($lstyrcumnrec1->result() as $row) {
				  $cons_tot_qty = $row->ConsTotalQty;
				  $cons_tot_val = $row->ConsTotalVal;
				  $recpt_qty    = $row->RecptTotalQty;
				  $recpt_val    = $row->RecptTotalVal;
			  ?>
              <tr>
                <td>Consumption <?php echo $last_fin_yr; ?></td>
                <td><?php echo number_format($cons_tot_qty,2); ?></td>
                <td><?php echo number_format($cons_tot_val,2); ?></td>                          
              </tr>
              <tr>
                <td>Receipt <?php echo $last_fin_yr; ?></td>
                <td><?php echo number_format($recpt_qty,2); ?></td>
                <td><?php echo number_format($recpt_val,2); ?></td>                          
              </tr>
              <?php 
			  } 
			  ?>
            </tbody>
       </table>
    </div>
    
    <div class="col-lg-6">
      <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>REORDER LEVEL</th>
                <th>REORDER QUANTITY</th>
              </tr>
            </thead>
            <?php   
			foreach ($reorder->result() as $row) { 
				$reorder_level =  $row->iou_reorderlevel;
				$reorder_qty =  $row->iou_reorderqty;
			?>
            <tbody>
              <tr>
                <td><?php echo number_format($reorder_level, 2); ?></td><!--RECORDER LEVEL-->
                <td><?php echo number_format($reorder_qty, 2); ?></td><!--RECORDER QUANTITY-->                         
              </tr>
            </tbody>
            <?php 
			}  
			?>
      </table>
    </div>
 </div>
            
<!--******* Last 5 Purchases With Rate, Supplier, City, Lead, Time(days) *********-->  
          
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
          Last Five Purchases
    </div>      
</div> 		
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1" style="font-size:10px">
            <thead>
              <tr>
                <th>PO‎/‎SCO‎/WO No‎</th>
                <th>GRCPT‎/‎Productio‎n Receipt</th>
                <th>Status</th>
                <th>Rate</th>
                <th>Supplier Name‎</th>
                <th>PR‎/‎SCR No‎</th>
                <th>Creation of PR‎/‎SCR Date</th>
                <th>Need ‎Date‎</th>
                <th>GRCPT ‎(‎Freeze & ‎Move Date‎)</th>
                <th>Lead ‎Time‎</th>
                <th>Receipt Qty‎.</th>
                <th>Accept‎ed Qty‎.</th>
                <th>Rejected Qty‎.</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach ($lastfivetrans1->result() as $row) 
                { 
                    $po_sco_wo_no = $row->LastPOGRTran;
                    $grcpt_prod_recipt = $row->LastReceipt;
                    $status = $row->AllocTranStatus;
                    $rate = number_format($row->LastRate,2);
                    $supp_name = $row->LastSupplier;
                    $pr_scr_no = $row->LastPRSCRTran;
                    $pr_scr_createdate1 = substr($row->LastPRSCRDate,0,10);
                    $pr_scr_createdate = date("d-m-Y", strtotime($pr_scr_createdate1));
                    $pr_need_date1 = substr($row->LastPRSCRNeedDt,0,10);
                    $pr_need_date = date("d-m-Y", strtotime($pr_need_date1));
                    $grcpt_freeze_move_dt1 = substr($row->LastMoveDt,0,10);
                    $grcpt_freeze_move_dt = date("d-m-Y", strtotime($grcpt_freeze_move_dt1));
                    $lead_time = $row->LastLeadTime;
                    $receipt_qty = round($row->LastTranQty,2);
                    $accepted_qty = round($row->CurrYrConsRecptJan,2);
                    $rejected_qty = round($row->CurrYrConsRecptFeb,2);
              ?>
              <tr>
                <td><?php echo $po_sco_wo_no; ?></td>
                <td><?php echo $grcpt_prod_recipt; ?></td>
                <td><?php echo $status; ?></td>
                <td><?php echo $rate; ?></td>
                <td><?php echo $supp_name; ?></td>
                <td><?php echo $pr_scr_no; ?></td>
                <td><?php echo $pr_scr_createdate; ?></td>
                <td><?php echo $pr_need_date; ?></td> 
                <td><?php echo $grcpt_freeze_move_dt; ?></td>
                <td><?php echo $lead_time; ?></td>
                <td><?php echo number_format($receipt_qty,2); ?></td>
                <td><?php echo number_format($accepted_qty,2); ?></td>
                <td><?php echo number_format($rejected_qty,2); ?></td>                            
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />

<!--******* SUPPLIER *********-->

<div class="row">
	<div class="col-lg-6" style="font-weight:bold; font-size:15px;">
    	Supplier
    </div>
    <div class="col-lg-6">

    </div>
</div>
<div class="row">
	<div class="col-lg-6">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Suppliers‎‎‎</th>
                <th>Purchase Rate‎‎</th>
                <th>Lead Time‎‎‎</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($supplier1->result() as $row) { ?>
              <tr>
                <td><?php echo $row->SuppName; ?></td>
                <td><?php echo number_format($row->SuppRate,2); ?></td>
                <td><?php echo $row->SuppLeadTime; ?></td>                       
              </tr>
              <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-lg-6">
    </div>
</div><br />

<!--******* USED IN BOM DETAIL *********-->

<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
       Used In BOM Detail‎‎‎‎‎‎‎
    </div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Product ‎Structure No</th>
                <th>Output Item‎‎</th>
                <th>Item‎ desc.‎‎</th>
                <th>Required Qty‎</th>
                <th>Warehouse‎</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($bomdetail1->result() as $row) { ?>
              <tr>
                <td><?php echo $row->PSNo; ?></td>
                <td><?php echo $row->FgCode; ?></td>
                <td><?php 
                
                $FgCodeDesc = $row->FgCodeDesc;
                $fgdec = explode(':',$FgCodeDesc);
                if ($fgdec[0] == 'LOC'){
                $dec = $fgdec[1]; }
                else {
                $dec = $fgdec[0]; }
                
                
                echo $dec; ?></td>
                <td><?php echo number_format($row->BomItemQty,2); ?></td>
                <td><?php echo $row->BomWhCode; ?></td>                         
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />
     		
<!--main content end-->  
<!-- container section end -->
      
<?php include('footer.php'); ?>