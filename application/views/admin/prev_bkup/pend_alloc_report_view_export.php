<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=export.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Excel</title>
</head>

<body>
<table border="1" style="font-size:10px">
    <tr style="background-color:#0CF; font-weight:bold">
        <td>SNO</td>
        <td>PR NUMBER</td>
        <td>ITEM CODE</td>
        <td>ITEM DESC</td>
        <td>REQ. QTY</td>
        <td>CATEGORY</td>
        <td>PEND. ALLOC.</td>
        <td>USAGE</td>
        <td>PR REMARKS</td>
        <td>ATAC NO</td>
        <td>SO NO.</td>
        <td>CUSTOMER NAME</td>
        <td>PR STATUS</td> 
    </tr>
    <?php
    $sno = 0;
    foreach($pend_alloc->result() as $row){ 
    $sno++;
    $pr_num = $row->pr_num;
    //ITEM CODE ENCODING
    $item_code = $row->item_code;
    $item_code1 = urlencode($item_code);
    if(strpos($item_code1, '%2F') !== false)
    {
        $item_code2 = str_replace("%2F","chandra",$item_code1);
    }
    else 
    {
        $item_code2 = $item_code1;
    }
    //ITEM CODE ENCODING
    //ITEM DESCRIPTION
    $item_desc1 = $row->itm_desc;
    $item_desc2 = str_replace("'","",$item_desc1);
    $item_desc = mb_convert_encoding($item_desc2, "ISO-8859-1", "UTF-8");
    //ITEM DESCRIPTION
    
    $required_qty = $row->required_qty;
    $category = $row->category;
    $pend_alloc = $row->pend_alloc;
    $usage = $row->usage;
    $remarks = $row->remarks;
    $sono = $row->sono;
    $pr_status = $row->pr_status;
    
    //CUSTOMER NAME
    $atac_no = $row->atac_no;
    
    $sql="select acc_name from tipldb..pr_submit_table a, tipldb..master_atac b where a.atac_no = b.atac_no and a.atac_no = '$atac_no'";
    $qry = $this->db->query($sql);
    if($qry->num_rows() > 0){
        foreach($qry->result() as $row){
            $customer_name = $row->acc_name;
        }
    } else {
        $customer_name = "";
    }
    ?>
    <tr>
        <td><?php echo $sno; ?></td>
        <td><a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo $pr_num; ?>"><?php echo $pr_num; ?></a></td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>">
                <?php echo $item_code; ?>
            </a>
        </td>
        <td><?php echo $item_desc; ?></td>
        <td><?php echo number_format($required_qty,2); ?></td>
        <td><?php echo $category; ?></td>
        <td><?php echo $pend_alloc; ?></td>
        <td><?php echo $usage; ?></td>
        <td><?php echo $remarks; ?></td>
        <td>
            <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                <?php echo $atac_no; ?>
            </a>
        </td>
        <td>
            <a href="http://live.tipl.com/tipl_project1/so_wo_linkage/so_tree.php?so_no=<?php echo $row->sono; ?>">
                <?php echo $sono; ?>
            </a>
        </td>
        <td><?php echo $customer_name; ?></td>
        <td><?php echo $pr_status; ?></td> 
    </tr>
    <? } ?>
</table>
</body>
</html>