<?php 
include_once('header.php');  
$this->load->helper('mrp_helper'); 

$item_code = $_REQUEST['item_code'];
$cp_status = $_REQUEST['cp_status'];
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
            	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEM DETAILS</h4>
            </div>
        </div><br />
        
        <div class="row">
        	<div class="col-lg-12">
            	<table class="table table-bordered">
                	<tr style="background-color:#CCC; font-weight:bold">
                    	<td>ItemCode</td>
                        <td>TranNo</td>
                        <td>TranQty</td>
                        <td>WO Status</td>
                    </tr>
                    <?php
						$sql="select * from tipldb..pendingissuetbl where itemcode = '$item_code' and cp_status = '$cp_status'";
						$qry=$this->db->query($sql);
						
						foreach($qry->result() as $row){
							$ItemCode = $row->ItemCode;
							$item_code1 = urlencode($item_code); 
							if(strpos($item_code1, '%2F') !== false){ 
							  $item_code2 = str_replace("%2F","chandra",$item_code1);
							} else {
							  $item_code2 = $item_code1;
							} 
							$TranNo = $row->TranNo;
							$TranQty = $row->TranQty;
							$TranStatus = $row->TranStatus;
							
							if($TranStatus == 'D'){
								$TranStatusDesc = 'Requested';
							} else if($TranStatus == 'E'){
								$TranStatusDesc = 'Accepted';
							} else if($TranStatus == 'F'){
								$TranStatusDesc = 'Firmed';
							} else if($TranStatus == 'G'){
								$TranStatusDesc = 'Released';
							} else if($TranStatus == 'S'){
								$TranStatusDesc = 'Order/acty started';
							} else if($TranStatus == 'U'){
								$TranStatusDesc = 'Order/acty finished';
							} else if($TranStatus == 'V'){
								$TranStatusDesc = 'Order closed';
							} else if($TranStatus == 'W'){
								$TranStatusDesc = 'Acty short closed';
							}
					?>
                    <tr>
                    	<td>
                        	<a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
								<?php echo $item_code;?>
                            </a>
                        </td>
                        <td><?=$TranNo;?></td>
                        <td><?=$TranQty;?></td>
                        <td><?=$TranStatusDesc;?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div><br />
    </section>
</section>     		
<!--main content end-->
<?php include_once('footer.php'); ?>