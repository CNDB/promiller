 <?php include'header.php'; ?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Live Created ERP Not Authorized PR</h4>
        </div>
    </div><br /> 
       
    <div class="row">
      <div class="col-lg-3">
      </div>
      
      <div class="col-lg-6">
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>PR Number</th>
                <th>ERP Status</th>
                <th>Pending On</th>
              </tr>
            </thead>
            <tbody>
             <?php
              foreach ($r->result() as $row){
              	$pr_num = $row->preqm_prno;
				$pr_erp_status = $row->preqm_status;
				$created_by = $row->preqm_createdby;
             ?>          
              <tr>
            	<td>
                	<?php echo $pr_num; ?><br />
                </td>
            	<td>
                	<?php echo $pr_erp_status;	?>
                </td>
                <td>
                	<?php echo $created_by;	?>
                </td>
            	                           
              </tr>
             <?php }  ?>  
         </tbody>
        </table>
      </div>
      
      <div class="col-lg-3">
      </div>
      
      <br />
    </div><br><br>

  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>