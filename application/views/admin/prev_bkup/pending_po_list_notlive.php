 <?php include'header.php'; ?>
 
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDING PO LIST</h4>
        </div>
    </div><br><br>
    <div class="row">
    	<div class="col-lg-12">
        	<table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
            	<tr style="background-color:#0CF">
                	<td><b>S.NO.</b></td>
                	<td><b>PO NUM.</b></td>
                    <td><b>SUPP NAME</b></td>
                    <td><b>CATEGORY</b></td>
                    <td><b>PO CREATE DATE</b></td>
                    <td><b>NEED DATE</b></td>
                    <td><b>PO VALUE</b></td>
                    <td><b>PO LIVE STATUS</b></td>
                    <td><b>AGE</b></td>
                    <td><b>REMARKS</b></td>
                </tr>
                <?php
					$sno = 0; 
					foreach ($get_data->result() as $row){
						$sno++; 
						$po_num = $row->pomas_pono;
						$po_supplier_code = $row->pomas_suppliercode;
						$po_create_date = $row->pomas_podate;
						$po_value = $row->pomas_pobasicvalue;
						//Need Date
						
						$sql1 ="select * from scmdb..po_poitm_item_detail where poitm_pono = '$po_num' 
and poitm_poamendmentno = (select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono = '$po_num')";
						$query1 = $this->db->query($sql1);
						
						foreach ($query1->result() as $row) {
							$po_need_date = $row->poitm_needdate;
						}
						
						//Supplier Name
						
						$sql2 ="select * from scmdb..supp_spmn_suplmain where supp_spmn_supcode = '$po_supplier_code'";
						$query2 = $this->db->query($sql2);
						
						foreach ($query2->result() as $row) {
							$supplier_name = $row->supp_spmn_supname;
						}
						
						//PO Age
						
						$sql3 ="select *, datediff(DAY, pomas_podate, getdate()) as diff from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num'";
						$query3 = $this->db->query($sql3);
						
						foreach ($query3->result() as $row) {
							$po_age = $row->diff;
						}
						
						//PO Status Live
						
						$sql4 = "select * from tipldb..po_master_table where po_num = '$po_num'";
						$query4 = $this->db->query($sql4);
						
						foreach ($query4->result() as $row) {
							$status = $row->status;
						}
						
						//PO CATEGORY
						
						/* $sql5 = "SELECT * FROM TIPLDB..insert_po a, TIPLDB..pr_submit_table b WHERE a.po_ipr_no = b.pr_num and a.po_num = '$po_num'";
						$query5 = $this->db->query($sql5);
						
						foreach ($query5->result() as $row) {
							$category = $row->category;
						} */
						
						$category='';
						$category1='';
						
						$sql5 = "SELECT * FROM TIPLDB..insert_po a, TIPLDB..pr_submit_table b WHERE a.po_ipr_no = b.pr_num and a.po_num = '$po_num'";
						$query5 = $this->db->query($sql5);
						
						foreach ($query5->result() as $row) {
							$category1 = $row->category;
						}
						
						if($category1 != ''){
							$category = $category1;
						}
						
						if($category1 == ''){
						
							$sql5 = "select * from TIPLDB..no_category_po b where b.po_num = '$po_num' ";
							$query5 = $this->db->query($sql5);
							
							foreach ($query5->result() as $row) {
								$category = $row->category;
							}
						
						}
						
						if($category1 == ''){
						
							$sql51 = "select distinct category
							from SCMDB..po_poprq_poprcovg_detail a, TIPLDB..pr_submit_table b
							where b.pr_num = a.poprq_prno
							and poprq_pono='$po_num'";
							$query51 = $this->db->query($sql51);
							
							foreach ($query51->result() as $row) {
								$category = $row->category;
							}
						
						}
						
						
				?>
                <tr>
                	<td><?php echo $sno; ?></td>
                    <td><?php echo $po_num; ?></td>
                    <td><?php echo $supplier_name; ?></td>
                    <td><?php echo $category; ?></td>
                    <td><?php echo substr($po_create_date,0,11); ?></td>
                    <td><?php echo substr($po_need_date,0,11); ?></td>
                    <td><?php echo number_format($po_value,2); ?></td>
                    <td><?php echo $status; ?></td>
                    <td><?php echo $po_age; ?></td>
                    <td></td>
                </tr>
                <?php 
					}
				?>
            </table>
        </div>
    </div> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>