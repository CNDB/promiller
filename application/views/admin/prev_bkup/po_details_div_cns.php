<!--********* PO INFORMATION *********-->
<?php
	$po_num = $this->uri->segment(3);
	$entry_no = $this->uri->segment(4);
	
	//Executing Gate Entry Procedure
	$sql_proc = "exec tipldb..gateentry_master_proc";
	$qry_proc = $this->db->query($sql_proc);
?>


<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>PO DETAILS</h3>
    </div>
</div>

<?php $total_order_value_with_taxes = 0;
 foreach ($view_po->result() as $row){ 
   $po_num                       = $row->po_num;
   $pr_num                       = $row->po_ipr_no; 
   $supp_name                    = $row->po_supp_name;
   $supp_addr                    = $row->po_supp_add;
   $supp_email                   = $row->po_supp_email;
   $supp_phone                   = $row->supp_phone;
   $contact_person               = $row->contact_person;
   $supp_addr                    = $row->po_supp_add;
   $order_value                  = $row->pomas_pobasicvalue;
   $cst_tax                      = $row->pomas_tcdtotalrate;
   $total_tax                    = $row->pomas_tcal_total_amount;
   $total_order_value_with_taxes = ($order_value + $cst_tax + $total_tax);
   $po_age                       = $row->diff;
   $payment_term                 = $row->payterm;
   $freight_place                = $row->freight_place;
   $carrier_name                 = $row->carrier_name;
   $deli_type                    = $row->po_deli_type;
   $lead_time                    = $row->po_lead_time;
   $manufact_clearnace           = $row->po_manfact_clernce;
   $dispatch_instruction         = $row->po_dispatch_inst;
   $freight                      = $row->freight;
   $insurance_liablity           = $row->insurance_liablity;
   $transport_mode               = $row->trans_mode;
   $freight_type                 = $row->freight_type;
   $approx_freight               = $row->approx_freight;
   $po_spcl_inst_frm_supp        = $row->po_spcl_inst_frm_supp;
   $ld_applicable                = $row->ld_applicable;
   $currency                     = $row->currency;
   $po_created_in_live           = $row->po_approvedby_lvl0;
   $po_created_in_live_date      = $row->po_approveddate_lvl0;
   $po_created_in_erp            = $row->pomas_createdby;
   $po_created_in_erp_date       = $row->pomas_createddate;
   $lvl1_app_req                 = $row->lvl1_app_req;
   $lvl2_app_req                 = $row->lvl2_app_req;
   $lvl3_app_req                 = $row->lvl3_app_req;
   $pomas_poamendmentno          = $row->pomas_poamendmentno;
   //Service PO Feilds Starts
   $spo_type = $row->spo_type;
   $spo_category = $row->po_category;
   $spo_usage_type = $row->spo_usage_type;
   $spo_atac_no = $row->spo_atac_no;
   $spo_atac_ld_date = $row->spo_atac_ld_date;
   $spo_atac_need_date = $row->spo_atac_need_date;
   $spo_atac_payment_terms = $row->spo_atac_payment_terms;
   $spo_atac_customer_payment_terms = $row->spo_atac_customer_payment_terms;
   $spo_project_name = $row->spo_project_name;
   $spo_customer_name = $row->spo_customer_name;
   $spo_pm_group = $row->spo_pm_group;
   $spo_so_no = $row->spo_so_no;
   $spo_why_spcl_rmks = $row->spo_why_spcl_rmks;
   //Advance Feilds
   $advance_amount = $row->advance_amount;
   $advance_remarks = $row->advance_remarks;
   $advance_entered_by = $row->advance_entered_by;
   $advance_entry_date = $row->advance_entry_date;
   
   echo "<input type='hidden' id='lvl1_app_req' name='lvl1_app_req' value='$lvl1_app_req'>";
   echo "<input type='hidden' id='lvl2_app_req' name='lvl2_app_req' value='$lvl2_app_req'>";
   echo "<input type='hidden' id='lvl3_app_req' name='lvl3_app_req' value='$lvl3_app_req'>";
   
   //Service PO Feilds Ends
    if($freight == "FOR"){
		$freight1 = $freight.",&nbsp;Ajmer";
	} else if($freight == "FORD"){
		$freight1 = $freight.",&nbsp;TIPL Ajmer";
	} else {
		$freight1 = $freight."&nbsp;";
	}
	
	$sql1 ="select *, datediff(DAY, preqm_prdate,getdate()) as diff1 from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'";
	$query1 = $this->db->query($sql1);
	
    foreach ($query1->result() as $row) {
	  $pr_age = $row->diff1;
    }					
?>
<div class="row">
    <div class="col-lg-2">
        <b>Supplier Name:</b><br />
        <?php echo $supp_name; ?>   	
    </div>
    <div class="col-lg-2">
        <b>Supplier Address:</b><br />
        <?php echo $supp_addr; ?>
    </div>
    <div class="col-lg-2">
        <b>Supplier Email:</b><br />
        <?php echo $supp_email; ?>
    </div>
    <div class="col-lg-2">
        <b>Supplier Phone:</b><br />
        <?php echo $supp_phone; ?>
    </div>
    <div class="col-lg-2">
        <b>Contact Person:</b><br />
        <?php echo $contact_person;?>
    </div>
	<div class="col-lg-2" style="background:#FF0;">
        <b>Order Value:</b><br />
		<?php echo number_format($total_order_value_with_taxes,2); ?>
    </div>
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
        <b>PO Age:</b><br />
        <?php echo $po_age."&nbsp; Days"; ?>
    </div>
	<div class="col-lg-2">
        <b>PR Age:</b><br />
        <?php echo $pr_age."&nbsp; Days"; ?>
    </div>
    <div class="col-lg-2" style="background:#FF0;">
        <b>Payment Terms:</b><br />
        <?php echo $payment_term; ?>   	
    </div>
	<div class="col-lg-2" style="background:#FF0;">
        <b>Freight Terms:</b><br />
        <?php echo $freight1; ?>
    </div>
    <div class="col-lg-2" style="background:#FF0;">
        <b>Freight Place:</b><br />
        <?php echo $freight_place; ?>
    </div>
	<div class="col-lg-2">
        <b>Carrier Name:</b><br />
        <?php echo $carrier_name; ?>
    </div> 
</div><br /><br />

<div class="row">
	<div class="col-lg-2" style="background:#FF0;">
        <b>Insurance Term:</b><br />
        <?php echo $insurance_liablity; ?>
    </div>
	<div class="col-lg-2" style="background:#FF0;">
        <b>Delivery Type:</b><br />
        <?php echo $deli_type; ?>
    </div>
    <div class="col-lg-2">
        <b>Expected Material Recipt Date:</b><br />
        <?php echo $lead_time; ?>
    </div>
	<div class="col-lg-2">
        <b>Mode Of Transport:</b><br />
        <?php echo $transport_mode; ?>
    </div> 
    <div class="col-lg-2"  style="background:#FF0;">
        <b>Freight Type:</b><br />
        <?php echo $freight_type; ?>
    </div>
	<div class="col-lg-2">
    	<b>Approximate Freight:</b><br />
        <?php echo $approx_freight; ?>
    </div>   
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
        <b>LD Applicable:</b><br />
        <?php echo $ld_applicable; ?>
    </div>
    <div class="col-lg-2">
        <b>PO Special Instructions For Supplier:</b><br />
        <?php echo $po_spcl_inst_frm_supp; ?>
    </div>
    <div class="col-lg-2">
    	<b>Currency :</b><br />
		<?php echo $currency; ?>
    </div>
    <div class="col-lg-2">
        <b>PO Created ERP By: </b><br />
        <?php echo $po_created_in_erp; ?>
    </div>
	<div class="col-lg-2">
    	<b>PO Created ERP Date:</b><br />
        <?php echo date("d-m-Y", strtotime($po_created_in_erp_date)); ?>
    </div>
    <div class="col-lg-2">
    	<b>PO Created Live By:</b><br />
        <?php echo strtoupper($po_created_in_live); ?>
        <input type="hidden" name="live_created_by" id="live_created_by" value="<?php echo $po_created_in_live; ?>" />
    </div>
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
    	<b>PO Created Live Date:</b><br />
        <?php echo date("d-m-Y", strtotime($po_created_in_live_date)); ?>
    </div>
    
    <?php
	
	$sql_gate_entry = "select distinct entry_no  from tipldb..gateentry_item_rec_table where po_num = '$po_num' and entry_no = '$entry_no'";
	$qry_gate_entry = $this->db->query($sql_gate_entry);
	
	if($qry_gate_entry->num_rows() > 0){
	} else {
		$entry_no = "";
	}
	
	?>
    
    <div class="col-lg-2" style="background-color:#FF0">
    	<b>Gate Entry No:</b><br /><?= $entry_no; ?> <input type="hidden" name="entry_no" id="entry_no" value="<?=$entry_no; ?>" />"; ?>
    </div>
    
    <div class="col-lg-2" style="background-color:#FF0">
    	<b>PO Revision No :</b><br /><?= $pomas_poamendmentno; ?>
    </div>
    
</div><br /><br />
<!-- Service PO Details Starts --->
<?php 
	$po_first_three = substr($po_num,0,3); 
	if($po_first_three == 'SRV'){ 
?>
<div class="row">
    <div class="col-lg-12">
    	<h4>Service PO Live Filled Details</h4>
    </div>
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
    	<b>Service PO Type</b><br />
        <?php echo $spo_type; ?>
    </div>
    <div class="col-lg-2">
    	<b>Service PO Category</b><br />
        <?php echo $spo_category; ?>
    </div>
    <div class="col-lg-2">
    	<b>Service PO Usage Type</b><br />
        <?php echo $spo_usage_type; ?>
    </div>
    <div class="col-lg-6"></div>	
</div><br /><br />

<?php if($spo_usage_type == 'Special'){ ?>

<div class="row">
    <div class="col-lg-2">
    	<b>Special Remarks</b><br />
        <?php echo $spo_why_spcl_rmks; ?>
    </div>
    <div class="col-lg-10"></div>
</div><br /><br />

<?php } else { ?>

<!-- ATAC Details Starts -->
<div class="row">
	<div class="col-lg-2">
    	<b>ATAC No.</b><br />
        <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $spo_atac_no; ?>">
			<?php echo $spo_atac_no; ?>
        </a>
    </div>
    <div class="col-lg-2">
    	<b>ATAC LD Date</b><br />
        <?php echo $spo_atac_ld_date; ?>
    </div>
    <div class="col-lg-2">
    	<b>ATAC Need Date</b><br />
        <?php echo $spo_atac_need_date; ?>
    </div>
    <div class="col-lg-2">
    	<b>ATAC Payment Terms</b><br />
        <?php echo $spo_atac_payment_terms; ?>
    </div>
    <div class="col-lg-2">
    	<b>Customer Payment Terms</b><br />
        <?php echo $spo_atac_customer_payment_terms; ?>
    </div>
    <div class="col-lg-2">
    	<b>Project Name</b><br />
        <?php echo $spo_project_name; ?>
    </div>
</div><br /><br />

<div class="row">
	<div class="col-lg-2">
    	<b>Customer Name</b><br />
        <?php echo $spo_customer_name; ?>
    </div>
    <div class="col-lg-2">
    	<b>PM Group</b><br />
        <?php echo $spo_pm_group; ?>
    </div>
    <div class="col-lg-2">
    	<b>SO Number</b><br />
        <?php echo $spo_so_no; ?>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>
</div><br /><br />

<?php } ?>

<!--- ATAC Details Ends --->

<!-- Payment Milestones Starts  --->
<div class="row">
	<div class="col-lg-12">
    	<h4>Payment Milestones</h4>
    </div>
</div><br />
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered" align="center" cellpadding="0" cellspacing="0" border="1">
        	<tr style="background-color:#CCC">
            	<td><b>SNO</b></td>
                <td><b>Description</b></td>
                <td><b>Target Date</b></td>
                <td><b>Percentage Of PO Value</b></td>
                <td><b>Amount</b></td>
                <td><b>Payment Term</b></td>
                <td><b>Payment Method</b></td>
                <td><b>PDC Days</b></td>
            </tr>
            <?php
				$sql_paymt_milestone = "select * from TIPLDB..spo_payment_milestones where po_num = '$po_num'  order by s_no asc";
				$qry_paymt_milestone = $this->db->query($sql_paymt_milestone);
				
				$sno = 0;
				foreach($qry_paymt_milestone->result() as $row){
					$sno++;
			?>
            <tr>
            	<td><?php echo $sno; ?></td>
            	<td><?php echo $desc = $row->description; ?></td>
                <td><?php echo $targt_date = $row->target_date; ?></td>
                <td><?php echo $per_po_val = $row->percentage_po_value; ?></td>
                <td><?php echo $amt = $row->amount; ?></td>
                <td><?php echo $paytrm = $row->payterm; ?></td>
                <td><?php echo $paymthd = $row->payment_method; ?></td>
                <td><?php echo $pdc_day = $row->pdc_days; ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />

<?php } ?>
<!-- Payment Milestones Ends --->

<!--- Service PO Details Ends ---->
<?php break; } ?>

<!--********* ITEM INFORMATION *********-->
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>ITEM DETAILS</h3>
    </div>
</div>
<div class="row">
  <div class="col-lg-12" style=" overflow-x:auto;">
     <table class="table table-bordered" id="dataTable" border="1">
        <thead>
          <tr>
            <th>IPR No.</th> 
            <th>Item Code, Description &amp; UOM</th>
            <th>PR Qty</th>
            <th>PO Qty</th>
            <?php
				$sql_rec_qty = "select count(*) as cnt1 from tipldb..gateentry_item_rec_table where po_num = '$po_num' and entry_no = '$entry_no'";
				
				$qry_rec_qty = $this->db->query($sql_rec_qty);
				foreach($qry_rec_qty->result() as $row){
					$count_rec_qty = $row->cnt1;
				}
				
				if($count_rec_qty > 0){
			?>
            <th>Recieved Qty.</th>
            <th>Previous Rec. Qty</th>
            <th>Excess Recieved Rmks</th>
            <th>Gate Entry No.</th>
            <th>Store Remarks</th>
            <th>Test Certificate</th>
            <?php } ?>
            <th>Costing</th>
            <th>Last Price</th> 
            <th>Current Price</th>
            <th>Charges</th>
            <th>Discount</th> 
            <th>Total Item Value</th>
            <th>PR Need Date</th>
            <th>PO Need Date</th>  
            <th>PR Type &amp Special Remarks, Category</th>
            <th>SO &amp; ATAC Details</th>
            <th>PR Remarks (Planning &amp Purchase)</th>  
            <th>Supplier Remarks (Planning &amp Purchase)</th>
            <th>MC & DI</th>                
            <th>Warehouse Code</th>
            <th>Last Supplier Details</th> 
            <?php if($po_first_three == 'SRV'){ ?>
            <th>SPO ITEM COST SHEET</th> 
            <?php } ?>
          </tr>
        </thead>
        <tbody>
		 <?php
           foreach ($view_po->result() as $row){ 
            $po_line_no  = $row->po_line_no;
            $po_s_no  = $row->po_s_no;
            $item_code = $row->po_item_code;
            $item_code1 = urlencode($item_code);
            
            if(strpos($item_code1, '%2F') !== false){
                $item_code2 = str_replace("%2F","chandra",$item_code1);
            } else {
                $item_code2 = $item_code1;
            }
    
            $po_ipr_no = $row->po_ipr_no;
            $po_num = $row->po_num;
            $item_desc = $row->po_itm_desc;
            $wh_code = $row->po_wh_code;
            $uom  = $row->po_uom;
            $current_price = $row->current_price;
            $item_value = $row->total_item_value;
            $item_remarks = $row->item_remarks;
            $for_stk_qty = $row->for_stk_quantity;
            $po_drwg_refno = $row->po_drwg_refno;
            $totalcost = round($row->po_qty, 2) * round($row->po_cost_pr_unt, 2);
			$spo_cost_sheet = $row->attach_cost_sheet;
            
            $sql1 ="exec tipldb..pendalcard '$item_code'";
			$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
			$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
			$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
			$sql5 ="select top 1 poitm_po_cost, gr_lin_fadate, gr_lin_fmdate, gr_lin_frdate, gr_hdr_grdate, gr_lin_linestatus,
* from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, 
scmdb..gr_hdr_grmain c, scmdb..gr_lin_details d where 
b.poitm_itemcode = '$item_code'
and c.gr_hdr_grno = d.gr_lin_grno
and b.poitm_itemcode = d.gr_lin_itemcode
and a.pomas_pono = c.gr_hdr_orderno
and c.gr_hdr_orderdoc = 'PO'
and c.gr_hdr_grstatus not in('DL')
and a.pomas_pono = b.poitm_pono and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE')
and a.pomas_poamendmentno = b.poitm_poamendmentno 
and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr WHERE pomas_pono = a.pomas_pono AND pomas_poou = a.pomas_poou)
and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO')
order by pomas_poauthdate desc";
			
			$sql6 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no' and item_code = '$item_code'";
			$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
			$sql8 ="select * from TIPLDB..road_permit_state a, scmdb..supp_addr_address b where a.state_code = b.supp_addr_state 
			and supp_addr_supcode = '$supp_code'";
			$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
			$sql_current_yr_con = "select RecptTotalQty from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$item_code'";
			$sql_last_supplier_details = "select SuppName, SuppRate, SuppLeadTime from tipldb..pendalcard_rkg where Flag='SuppDetail' 
			and ItemCode='$item_code'";
			$sql_reorder_details = "select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
			
			//Charges & Discount
			$sql_linelvl_charges = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
			where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
			and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode != 'PDISCOUNTS'";
									
			$sql_linelvl_discount = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
			where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
			and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode = 'PDISCOUNTS'";
			
			//PO Need Date
			$sql_po_nd = "select * from scmdb..po_poitm_item_detail where poitm_pono = '$po_num' and poitm_polineno = '$po_line_no'";
			
			//HSN CODE ITEM
			$sql_hsnsac = "select commoditycode from scmdb..trd_tax_group_dtl 
			where item_code='$item_code' and tax_group_code like'%GST%'";
			
			//Gate Entry Details
			$sql_gate_entry_det = "select recieved_quantity,entry_no,remarks from tipldb..gateentry_item_rec_table 
			where po_num = '$po_num' and item_code = '$item_code' and po_line_no = '$po_line_no'
			and entry_no = '$entry_no'";
			
			//Fetching Item Description New Code Starts
			$sql_item_desc = "select * from scmdb..itm_ml_multilanguage a, scmdb..itm_lov_varianthdr b 
			where a.ml_itemcode = b.lov_itemcode and b.lov_itemcode = '".$item_code."'";
			
			$sql_prev_rec_qty = "select sum(gr_lin_receivedqty) as gr_lin_receivedqty 
			from scmdb..gr_hdr_grmain a, scmdb..gr_lin_details b 
			where a.gr_hdr_grno = b.gr_lin_grno
			and a.gr_hdr_orderno = '".$po_num."'
			and b.gr_lin_itemcode = '".$item_code."'
			and a.gr_hdr_grstatus in('FM')";
			
			//Gate Entry Details
			$query1 = $this->db->query($sql1);
			$this->db->close();
			$this->db->initialize();
			$query2 = $this->db->query($sql2);
			$query3 = $this->db->query($sql3);
			$query4 = $this->db->query($sql4);
			$query5 = $this->db->query($sql5);
			$query6 = $this->db->query($sql6);
			$query7 = $this->db->query($sql7);
			$query8 = $this->db->query($sql8);
			$query_draw = $this->db->query($sql_draw);
			$query_current_yr_con = $this->db->query($sql_current_yr_con);
			$query_last_supplier_details = $this->db->query($sql_last_supplier_details);
			$query_reorder_details = $this->db->query($sql_reorder_details);
			$query_po_nd = $this->db->query($sql_po_nd);
			
			//Fetching Item Description New Code
			$qry_item_desc = $this->db->query($sql_item_desc);
			
			//charges and discount starts
			$query_linelvl_charges = $this->db->query($sql_linelvl_charges);
			$query_linelvl_discount = $this->db->query($sql_linelvl_discount);
			$query_hsnsac = $this->db->query($sql_hsnsac);
			$qry_gate_entry_det = $this->db->query($sql_gate_entry_det);
			
			//Previous received qty
			$qry_prev_rec_qty = $this->db->query($sql_prev_rec_qty);
			
			if($qry_prev_rec_qty->num_rows() > 0){
				foreach($qry_prev_rec_qty->result() as $row){
					$prev_rec_qty = $row->gr_lin_receivedqty;
				}
			} else {
				$prev_rec_qty = 0;
			}
			
			if($qry_gate_entry_det->num_rows() > 0){
				foreach($qry_gate_entry_det->result() as $row){
					$recieved_quantity = $row->recieved_quantity;
					$entry_no = $row->entry_no;
					$remarks = $row->remarks;
				}
			} else {
				$recieved_quantity = 0;
				$entry_no = "";
				$remarks = "";
			}
		
			if($query_hsnsac->num_rows() > 0){
				foreach($query_hsnsac->result() as $row){
					$hsncode = $row->commoditycode;
				}
			} else {
				$hsncode = "";
			}
			
			if ($query_linelvl_charges->num_rows() > 0) {
			  foreach ($query_linelvl_charges->result() as $row) {
				  $line_level_charges = $row->potcd_tcdamount;
				}
			} else {
				  $line_level_charges = 0;
			}
			
			if ($query_linelvl_discount->num_rows() > 0) {
			  foreach ($query_linelvl_discount->result() as $row) {
				  $line_level_discount = $row->potcd_tcdamount;
				}
			} else {
				  $line_level_discount = 0;
			}
			
			//Fetching Item Description New Code Starts
			if ($qry_item_desc->num_rows() > 0) {
				foreach($qry_item_desc->result() as $row){
					$ml_itemvardesc = $row->ml_itemvardesc;
					$lov_matlspecification = $row->lov_matlspecification;
					$item_desc = $ml_itemvardesc." ".$lov_matlspecification;
				}
			} else {
				$item_desc = "";
			}
			
			if ($query2->num_rows() > 0) {
				foreach ($query2->result() as $row) {
				  $lastyr_cons = $row->ConsTotalQty;
				}
			} else {
				  $lastyr_cons = 0;
			}
			
			if ($query3->num_rows() > 0) {
				$current_stk = 0;
				foreach ($query3->result() as $row) {
				  $current_stock = $row->ItemStkAccepted;
				  $current_stk = $current_stk + $current_stock;
				}
			} else {
				$current_stk = 0;
			}
			
			if ($query4->num_rows() > 0) {
				foreach ($query4->result() as $row) {
				  $reservation_qty = $row->AllocPendingTot;
				}
			} else {
				$reservation_qty = 0;
			}
			
			if ($query5->num_rows() > 0) {
				foreach ($query5->result() as $row) {
					$last_price = $row->poitm_po_cost;
					$last_price1 = number_format($last_price,2);
					$gr_lin_linestatus = $row->gr_lin_linestatus;
					
					if($gr_lin_linestatus == 'FA'){
					  $last_grcpt_date = $row->gr_lin_fadate;
					} else if($gr_lin_linestatus == 'FM'){
					  $last_grcpt_date = $row->gr_lin_fmdate;
					} else if($gr_lin_linestatus == 'FZ'){
					  $last_grcpt_date = $row->gr_lin_frdate;
					} else if($gr_lin_linestatus == 'DR' || $gr_lin_linestatus == 'FR'){
					  $last_grcpt_date = $row->gr_hdr_grdate;
					} 
				}
			} else {
				$last_price = "";
				$last_price1 = "";
				$last_grcpt_date = "";
			}
			
			if ($query6->num_rows() > 0) {
				foreach ($query6->result() as $row) {
					$ipr_type            = $row->usage;
					$sono                = $row->sono;
					$atac_no             = $row->atac_no;
					$atac_ld_date        = $row->atac_ld_date;
					$atac_need_date      = $row->atac_need_date;
					$atac_payment_terms  = $row->atac_payment_terms;
					$pm_group            = $row->pm_group;
					$category            = $row->category;
					$project_name        = $row->project_name;
					$costing             = $row->costing;
					$ipr_remarks         = $row->remarks;
					$attached_cost_sheet = $row->attch_cost_sheet;
					$ipr_need_date       = $row->need_date;
					$manufact_clrnce	 = $row->manufact_clearance;
					$dispatch_inst       = $row->dispatch_inst;
					$supp_item_remarks   = $row->pr_supp_remarks;
					$costing_currency    = $row->costing_currency;
					$why_spcl_rmks       = $row->why_spcl_rmks;
					$pr_qty              = number_format($row->required_qty,2);
				}
			} else {
				$ipr_type            = "";
				$sono                = "";
				$atac_no             = "";
				$pm_group            = "";
				$category            = "";
				$project_name        = "";
				$costing             = "";
				$ipr_remarks         = "";
				$ipr_need_date       = "";
				$atac_ld_date        = "";
				$atac_need_date      = "";
				$atac_payment_terms  = "";
				$attached_cost_sheet = "";
				$manufact_clrnce	 = "";
				$dispatch_inst       = "";
				$supp_item_remarks   = "";
				$costing_currency    = "";
				$why_spcl_rmks       = "";
				$pr_qty              = ""; 	  
			}
			
			if ($query7->num_rows() > 0) {
				$incoming_qty_tot1 = 0;
				foreach ($query7->result() as $row) {
				  $incoming_qty = $row->PendPoSchQty;
				  $incoming_qty_tot = $incoming_qty_tot1 + $incoming_qty;
				}
			} else {
				$incoming_qty_tot = "";
			}
			
			if ($query8->num_rows() > 0) {
				foreach ($query8->result() as $row) {
				  $supp_state = $row->state_code;
				  $road_permit_req = $row->road_permit;
				}
			} else {
				$supp_state = "";
				$road_permit_req = "";
			}
			
			if ($query_draw->num_rows() > 0) {
				foreach ($query_draw->result() as $row) {
				  $drawing_no = $row->DrawingNo;
				  $purchase_uom  = $row->ItemPurcaseUom;
				  $manufact_uom = $row->ItemMnfgUom;
				}
			} else {
				$drawing_no = "";
				$purchase_uom  = "";
				$manufact_uom = "";
			}
			
			if ($query_current_yr_con->num_rows() > 0) {
				foreach ($query_current_yr_con->result() as $row) {
				  $current_yr_con = $row->RecptTotalQty;
				}
			} else {
				$current_yr_con = "";
			}
			
			if ($query_last_supplier_details->num_rows() > 0) {
				foreach ($query_last_supplier_details->result() as $row) {
				  $SuppName = $row->SuppName; 
				  $SuppRate = $row->SuppRate; 
				  $SuppLeadTime = $row->SuppLeadTime;
				}
			} else {
				$SuppName = ""; 
				$SuppRate = ""; 
				$SuppLeadTime = "";
			}
			
			if ($query_reorder_details->num_rows() > 0) {
				foreach ($query_reorder_details->result() as $row) {
				  $iou_reorderlevel = $row->iou_reorderlevel; 
				  $iou_reorderqty = $row->iou_reorderqty;
				}
			} else {
				$iou_reorderlevel = ""; 
				$iou_reorderqty = "";
			}
			
			$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";
	
			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {
			  foreach ($query->result() as $row) {
				  $ucon_confact_ntr = $row->ucon_confact_ntr;
				  $ucon_confact_dtr = $row->ucon_confact_dtr;
				  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
			  }
			} else {
				   $conversion_factor = "";  
			}
			//Supplier Average Lead Time
						
			$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
			$query_supplier = $this->db->query($sql_supplier);
			
			if ($query_supplier->num_rows() > 0) {
				$supplier_lead_time_tot = 0;
				$counter = 0;
				foreach ($query_supplier->result() as $row) {
				  $counter++;
				  $supplier_lead_time = $row->SuppLeadTime;
				  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
				}
				$supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
			} else {
			   $counter = 0;
			   $supplier_lead_time = "";
			   $supplier_lead_time_tot = ""; 
			   $supplier_avg_lead_time = 0;  
			}
			
			foreach($query_po_nd->result() as $row){
				$po_need_date = $row->poitm_needdate;
			}
			
			$sql_supplier = "exec TIPLDB..supplier_details  '1' , '$item_code'  , '$po_ipr_no'";
			$query_supplier = $this->db->query($sql_supplier);
			
			$this->db->close();
			$this->db->initialize();
			
			$sql_supplier_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
			$query_supplier_dtl = $this->db->query($sql_supplier_dtl);
			
			 foreach ($query_supplier_dtl->result() as $row) {
				$diffdays    = $row->diffDays;
			 }
			 
			//echo "Cha==".$diffdays;
			
			if($diffdays != NULL){
				if($diffdays > 0){
					$color = "red";
				} else if($diffdays < 0){
					$color = "green";
				} else if($diffdays = 0){
					$color = "blue";
				}
			} else {
				$color = "yellow";
			}
         ?>
            <tr>
                <?php 
                    echo "<input type='hidden' name='po_s_no' value='$po_s_no' />"; 
                    echo "<input type='hidden' name='po_num' value='$po_num' />";	
                ?>
            <td>
                <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
                	<?php echo $po_ipr_no; ?>
                </a>
                <input type="hidden" id="po_ipr_no" name="po_ipr_no[]" value="<?=$po_ipr_no;?>" />
            </td>
            <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" 
                target="_blank"><?php echo $item_code; ?></a> <br /><br />
                <?php echo mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8"); ?><br /><br />
                <?php echo "<b>UOM - </b>".$uom; ?><br /><br />
                <?php if($hsncode != ''){ echo "<b>HSN - </b>".$hsncode; } ?>
            </td>
            <td><?php echo $pr_qty; ?></td>
            <td><?php echo number_format($for_stk_qty,2); ?></td>
            
            <?php if($count_rec_qty > 0){ ?>
            <td>
				<?php echo number_format($recieved_quantity,2,".",""); ?>
                <input type="hidden" id="curr_rec_qty" name="curr_rec_qty[]" value="<?=$recieved_quantity;?>" />
            </td>
            <td>
				<?php echo number_format($prev_rec_qty,2,".",""); ?>
                <input type="hidden" id="prev_rec_qty" name="prev_rec_qty[]" value="<?=$prev_rec_qty;?>" />
            </td>
            <td>
				<?php
					$tot_rec_qty = $recieved_quantity+$prev_rec_qty;
					if((number_format($tot_rec_qty,2,".","") > number_format($for_stk_qty,2,".","")) && $recieved_quantity != 0 ){
				?>
                	<textarea id="excess_indent_rmks" name="excess_indent_rmks[]" required="required" class="form-control"></textarea>
                <?php } else {?>
                	<input type="hidden" id="excess_indent_rmks" name="excess_indent_rmks[]" value="">
                <?php } ?>
            </td>
            <td><?php echo $entry_no; ?></td>
            <td><?php echo $remarks; ?></td>
            <td>
            	<?php
					$sql_tc_att = "select * from tipldb..gateentry_item_rec_table a, tipldb..tc_upload_po_wise b 
					where a.po_num = b.po_num and a.item_code = b.item_code 
					and a.po_line_no = b.po_line_no and a.entry_no = b.entry_no 
					and a.po_num = '$po_num'
					and a.item_code = '$item_code' and a.po_line_no = '$po_line_no' and status = 'Pending For HSN Check By Purchaser'";
					
					$qry_tc_att = $this->db->query($sql_tc_att);
					
					foreach($qry_tc_att->result() as $row){
						echo $uploaded_tc = $row->uploaded_tc;
						
				?>
                <a href=""></a>
                <?php
					}	
				?>
            </td>
            <?php } ?>
            
            <td>
            	<a href="<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>" target='_blank'>
                <?php
                    echo $costing."  ".$costing_currency;
					echo "<input type='hidden' name='costing' value='$costing' />";
					echo "<input type='hidden' name='costing_currency' value='$costing_currency' />";
                ?>
                </a> 
            </td>
            <td style="background:#FF0;">
                <?php 
                    echo number_format($last_price,2);
					echo "<input type='hidden' name='last_price[]' value='$last_price' >"; 
                ?>
            </td>
            <td style="background:#FF0;"><?php echo number_format($current_price,2); ?></td>
            <td><?php echo number_format($line_level_charges,2); ?></td>
            <td><?php echo number_format($line_level_discount,2); ?></td>
            <td>
                <?php
                    $item_value_new = ($item_value+$line_level_charges)-$line_level_discount; 
                    echo number_format($item_value_new,2); 
                ?>
            </td>
            <td style="background-color:<?php echo $color; ?>">
                <?php
                    echo $ipr_need_date;
                    echo "<input type='hidden' name='ipr_need_date[]' value='$ipr_need_date' />"; 
                ?> 
            </td>
            <td style="background-color:<?php echo $color; ?>" ><?php echo substr($po_need_date,0,11); ?></td>
            <td>
				<?php if($ipr_type != ''){ echo "<b>PR TYPE - </b>".$ipr_type; } ?><br /><br />
                <?php if($why_spcl_rmks != ''){ echo "<b>SPCL RMKS - </b>".$why_spcl_rmks; } ?><br /><br />
                <?php if($category != ''){ echo "<b>CATEGORY - </b>".$category; }?>
            </td>
            <td>
            	<a href="http://live.tipl.com/tipl_project1/so_wo_linkage/so_tree.php?so_no=<?php echo $sono; ?>">
					<?php if($sono != ''){ echo "<b>SONO - </b>".$sono; } ?>
                </a><br /><br />
                
            	<a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                	<?php if($atac_no != ''){  echo "<b>ATAC NO - </b>".$atac_no; }?>
                </a><br /><br />
                
                <?php if($atac_ld_date != ''){  echo "<b>ATAC LD DATE - </b>".$atac_ld_date; } ?><br /><br />
                <?php if($atac_need_date != ''){  echo "<b>ATAC NEED DATE - </b>".$atac_need_date; } ?><br /><br />
                <?php if($atac_payment_terms != ''){  echo "<b>ATAC PAY TERM - </b>".$atac_payment_terms; } ?><br /><br />
                <?php if($pm_group != ''){  echo "<b>PM GROUP - </b>".$pm_group; } ?> <br /><br />
                <?php if($project_name != ''){  echo "<b>PROJECT NAME - </b>".$project_name; } ?><br /><br />
            </td>
            <td>
				<?php if($ipr_remarks != ''){ echo "<b>Planning - </b>".$ipr_remarks; } ?>
                <?php if($item_remarks != ''){ echo "<b>Purchase - </b>".$item_remarks; } ?>
            </td>
            <td><?php echo $supp_item_remarks; ?></td>
            <td>
            	<?php if($manufact_clrnce != ''){ echo "<b>MF -</b>".$manufact_clrnce; } ?><br /><br />
                <?php if($dispatch_inst != ''){ echo "<b>DI -</b>".$dispatch_inst; } ?><br /><br />
                <?php echo "<input type='hidden' name='manufact_clrnce[]' id='manufact_clrnce' value='$manufact_clrnce' />"; ?>
            </td>
            <td><?php echo $wh_code; ?></td>
            <td>
                <?php if($SuppName != ''){ echo "<b>Supp Name - </b>".$SuppName;  } ?><br /><br />
                <?php if($SuppRate != ''){ echo "<b>Supp Rate - </b>".number_format($SuppRate,2); } ?><br /><br />
                <?php if($SuppLeadTime != ''){ echo "<b>Supp Lead Time - </b>".number_format($SuppLeadTime,2); } ?><br /><br />
                <?php 
					if($last_grcpt_date != ''){
						echo substr($last_grcpt_date,0,11); 
						echo "<input type='hidden' name='last_grcpt_date[]' value='$last_grcpt_date' />";
					}
                ?>
            </td>
            <?php if($po_first_three == 'SRV'){ ?>
            <td><a href="<?php echo base_url(); ?>uploads/<?php echo $spo_cost_sheet; ?>"><?php echo $spo_cost_sheet; ?></a></td>
            <?php } ?>
          </tr>
           <?php } ?>
        </tbody>
      </table>
</div>
</div><br /><br />

<div class="row">
	<div class="col-lg-12">
    	<h4>Store Recieved Documents</h4>
    </div>
</div><br />


<?php
$sql_doc = "select dup_trans,org_buyer,bill_dup_copy,doc_copy,del_chal,
pack_list,vat_form,test_certi_file,vat_form_file,pack_list_file,
del_chal_file,doc_copy_file,bill_dup_file,org_buyer_file,dup_trans_file,
test_certi_recv,vat_form_recv,pack_list_recv,del_chal_recv,doc_copy_recv,
bill_dup_recv,dup_trans_recv,org_buyer_recv 
from tipldb..gateentry where entry_no = '$entry_no'";
$qry_doc = $this->db->query($sql_doc);

foreach($qry_doc->result() as $row){
	
	$dup_trans = $row->dup_trans;
	$org_buyer = $row->org_buyer;
	$bill_dup_copy = $row->bill_dup_copy;
	$doc_copy = $row->doc_copy;
	$del_chal = $row->del_chal;
	$pack_list = $row->pack_list;
	$vat_form = $row->vat_form;
	$test_certi_file = $row->test_certi_file;
	$vat_form_file = $row->vat_form_file;
	$pack_list_file = $row->pack_list_file;
	$del_chal_file = $row->del_chal_file;
	$doc_copy_file = $row->doc_copy_file;
	$bill_dup_file = $row->bill_dup_file;
	$org_buyer_file = $row->org_buyer_file;
	
	$dup_trans_file = $row->dup_trans_file;
	$test_certi_recv = $row->test_certi_recv;
	$pack_list_recv = $row->pack_list_recv;
	$del_chal_recv = $row->del_chal_recv;
	$doc_copy_recv = $row->doc_copy_recv;
	
	$bill_dup_recv = $row->bill_dup_recv;
	$dup_trans_recv = $row->dup_trans_recv;
	$org_buyer_recv = $row->org_buyer_recv;
	
}
?>
<div class="row">
	<div class="col-lg-2">
    	<b>Duplicate For Transporter :</b><br />
        <?php if($dup_trans_file != ''){ ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/miduploadpage_cns.php?filenm=<?=$dup_trans_file; ?>&foldnm=po_based_attach" target="_blank">
            <?php echo $dup_trans_file; ?>
            </a>
        <?php } else { ?>
        	<?php echo $dup_trans; ?>
        <?php } ?>
    </div>
    <div class="col-lg-2">
    	<b>Original For Buyer :</b><br />
        <?php if($org_buyer_file != ''){ ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/miduploadpage_cns.php?filenm=<?=$org_buyer_file; ?>&foldnm=po_based_attach" target="_blank">
            <?php echo $org_buyer_file; ?>
            </a>
        <?php } else { ?>
        	<?php echo $org_buyer; ?>
        <?php } ?>
    </div>
    <div class="col-lg-2">
    	<b>Docket Copy :</b><br />
        <?php if($doc_copy_file != ''){ ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/miduploadpage_cns.php?filenm=<?=$doc_copy_file; ?>&foldnm=po_based_attach" target="_blank">
            <?php echo $doc_copy_file; ?>
            </a>
        <?php } else { ?>
        	<?php echo $doc_copy; ?>
        <?php } ?>
    </div>
    <div class="col-lg-2">
    	<b>Delivery Challan :</b><br />
        <?php if($del_chal_file != ''){ ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/miduploadpage_cns.php?filenm=<?=$del_chal_file; ?>&foldnm=po_based_attach" target="_blank">
            <?php echo $del_chal_file; ?>
            </a>
        <?php } else { ?>
        	<?php echo $del_chal; ?>
        <?php } ?>
    </div>
    <div class="col-lg-2">
    	<b>Packing List :</b><br />
        <?php if($pack_list_file != ''){ ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/miduploadpage_cns.php?filenm=<?=$pack_list_file; ?>&foldnm=po_based_attach" target="_blank">
            <?php echo $pack_list_file; ?>
        	</a>
        <?php } else { ?>
        	<?php echo $pack_list; ?>
        <?php } ?>
    </div>
    <div class="col-lg-2">
    	<b>E Way-Bill Form :</b><br />
        <?php if($vat_form_file != ''){ ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/miduploadpage_cns.php?filenm=<?=$vat_form_file; ?>&foldnm=po_based_attach" target="_blank">
            <?php echo $vat_form_file; ?>
            </a>
        <?php } else { ?>
        	<?php echo $vat_form; ?>
        <?php } ?>
    </div>
</div><br /><br />


<!--********* REMARKS *********--> 
<?php if(substr($po_num,1,3) == 'FPO'){ ?>
              
<div class="row">
  <div class='col-lg-3'> 
    <b>Purchase Attachments</b><b style="color:#F00">&nbsp;*</b>
  </div>
  <div class='col-lg-6'>
    <div class="controls">   
        <div class="entry input-group col-xs-6">
        	<table class="table table-bordered">
            	<tr>
                	<td><input type="text" id="doc_type" name="doc_type[]" value="" size="30" required="required" placeholder="Document Type" /></td>
                    <td><input type="file" class="btn btn-primary" name="po_ge_attchment[]" id="po_ge_attchment" multiple required="required"></td>
                    <td>
                        <span class="input-group-btn">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>   
  </div>
  <div class="col-lg-3"></div>
</div> <br />

<? } else { ?>

<div class="row">
  <div class='col-lg-3'> 
    <b>Purchase Attachments</b><b style="color:#F00">&nbsp;*</b>
  </div>
  <div class='col-lg-6'>
    <div class="controls">   
        <div class="entry input-group col-xs-6">
        	<table class="table table-bordered">
            	<tr>
                	<td><input type="text" id="doc_type" name="doc_type[]" value="" size="30" placeholder="Document Type" /></td>
                    <td><input type="file" class="btn btn-primary" name="po_ge_attchment[]" id="po_ge_attchment" multiple></td>
                    <td>
                        <span class="input-group-btn">
                            <button class="btn btn-success btn-add" type="button">
                                <span class="glyphicon glyphicon-plus"></span>
                            </button>
                        </span>
                    </td>
                </tr>
            </table>
        </div>
    </div>   
  </div>
  <div class="col-lg-3"></div>
</div> <br />

<?php } ?>



<!-- Multiple File Select Box -->

<script type="text/javascript">
	$(function()
	{
		$(document).on('click', '.btn-add', function(e)
		{
			e.preventDefault();
	
			var controlForm = $('.controls:first'),
				currentEntry = $(this).parents('.entry:first'),
				newEntry = $(currentEntry.clone()).appendTo(controlForm);
	
			newEntry.find('input').val('');
			controlForm.find('.entry:not(:last) .btn-add')
				.removeClass('btn-add').addClass('btn-remove')
				.removeClass('btn-success').addClass('btn-danger')
				.html('<span class="glyphicon glyphicon-minus"></span>');
		}).on('click', '.btn-remove', function(e)
		{
		  $(this).parents('.entry:first').remove();
	
			e.preventDefault();
			return false;
		});
	});
</script>
