<?
/******* Procedure created by Chandra Narayan Sharma 23-05-2018 ****************/  

/*--- Updating PO Master Table -----*/
$sql ="UPDATE tipldb..po_master_table SET po_value_greater = 'Yes' WHERE convert(decimal(16,2),po_total_value) > '1500'";
$query = $this->db->query($sql);

$sql ="UPDATE tipldb..po_master_table SET costing_blank = 'Yes' WHERE po_num in(
	select distinct po_num from tipldb..insert_po WHERE costing = 0 and last_price = 0
)";
$query = $this->db->query($sql);

$sql ="UPDATE tipldb..po_master_table SET last_price_blank = 'Yes'                                                                    
WHERE po_num in(select distinct po_num from tipldb..insert_po WHERE costing = 0 and last_price = 0) ";
$query = $this->db->query($sql);

$sql ="UPDATE tipldb..po_master_table SET is_special = 'Yes'                                                                     
where po_num in(SELECT DISTINCT po_num FROM tipldb..insert_po a, tipldb..pr_submit_table b WHERE a.po_ipr_no = b.pr_num AND b.usage = 'special')";
$query = $this->db->query($sql);


$sql ="UPDATE tipldb..po_master_table SET po_value_greater = 'No' WHERE po_value_greater  is NULL";
$query = $this->db->query($sql);                                                                   
                                                                    
$sql ="UPDATE tipldb..po_master_table SET costing_blank = 'No' WHERE costing_blank  is NULL";
$query = $this->db->query($sql);                                                                   
                                                                    
$sql ="UPDATE tipldb..po_master_table SET last_price_blank = 'No' WHERE last_price_blank  is NULL";
$query = $this->db->query($sql);                                                                    
                                                                    
$sql ="UPDATE tipldb..po_master_table SET is_special = 'No' WHERE is_special  is NULL";
$query = $this->db->query($sql);                                                                          
                                                        
$sql ="UPDATE tipldb..insert_po SET ipr_qty = a.prqit_reqdqty FROM scmdb..prq_prqit_item_detail a WHERE a.prqit_prno = tipldb..insert_po.po_ipr_no";
$query = $this->db->query($sql);                                                          
                                                        
$sql ="delete from tipldb..po_line_wise_pr_qty";
$query = $this->db->query($sql);                           
        
/*$sql ="
create table tipldb..po_line_wise_pr_qty(                  
	 sno int identity not null,                 
	 po_num varchar(255) not null,               
	 po_line_no int,                                                        
	 po_qty decimal(16,8),                                                        
	 pr_tot_qty decimal(16,8),                                             
	 po_qty_greater varchar(255)                          
)";
$query = $this->db->query($sql); */                                
                                                        
$sql ="insert into tipldb..po_line_wise_pr_qty (po_num,po_line_no)                                                        
select distinct po_num, po_line_no from tipldb..insert_po";
$query = $this->db->query($sql);                                                        
                                                        
$sql ="update tipldb..po_line_wise_pr_qty set po_qty = a.for_stk_quantity from tipldb..insert_po a                                 
where a.po_num = tipldb..po_line_wise_pr_qty.po_num and a.po_line_no =  tipldb..po_line_wise_pr_qty.po_line_no";
$query = $this->db->query($sql);                                                    
                                                        
$sql ="update tipldb..po_line_wise_pr_qty set pr_tot_qty = (select sum(a.ipr_qty) from tipldb..insert_po a                                                         
where a.po_num = tipldb..po_line_wise_pr_qty.po_num and a.po_line_no =  tipldb..po_line_wise_pr_qty.po_line_no and a.ipr_qty is not null)";
$query = $this->db->query($sql);                                      
                                                      
$sql ="update tipldb..po_line_wise_pr_qty set po_qty_greater = 'Yes' where po_qty > pr_tot_qty";
$query = $this->db->query($sql);                                                      
                                                      
$sql ="update tipldb..po_line_wise_pr_qty set po_qty_greater = 'No' where po_qty <= pr_tot_qty";
$query = $this->db->query($sql);                                                     
                                                    
$sql ="update tipldb..po_master_table set poqty_greater_prqty = 'Yes'                                                     
where  po_num in(select distinct po_num from po_line_wise_pr_qty where po_qty_greater = 'Yes')";
$query = $this->db->query($sql);                                                     
                                                    
$sql ="update tipldb..po_master_table set poqty_greater_prqty = 'No' where poqty_greater_prqty is NULL";
$query = $this->db->query($sql);                                                                                                      
                                                    
/*--- LVL 3 APP CONDITIONS -----*/                                                                                                                  
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No'                                                                     
WHERE costing_blank = 'Yes' and  last_price_blank = 'Yes'                                                                     
and substring(po_num,1,3) in('SRV','CGP')";
$query = $this->db->query($sql);                                                                   
                                                               
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE costing_blank = 'Yes' and  last_price_blank = 'Yes'                                                                     
and substring(po_num,1,3) in('FPO')";
$query = $this->db->query($sql);                                                    
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                
WHERE costing_blank = 'Yes' and  last_price_blank = 'Yes'                                                                     
and substring(po_num,1,3) in('IPO','LPO')                                                                 
and po_category not in('AVAZONIC','IT')";
$query = $this->db->query($sql);                                                                                  
                                                                
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                    
WHERE is_special = 'Yes' and po_value_greater = 'Yes'                        
and substring(po_num,1,3) in('FPO')                                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                   
                
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE is_special = 'Yes' and po_value_greater = 'Yes'                                                                    
and substring(po_num,1,3) in('IPO','LPO')                                 
and po_category not in('AVAZONIC','IT')                                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No'                                                                     
WHERE poqty_greater_prqty = 'Yes'                                                                    
and substring(po_num,1,3) in('SRV','CGP')                                            
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                    
                                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE poqty_greater_prqty = 'Yes'                                                                     
and substring(po_num,1,3) in('FPO')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                    
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE poqty_greater_prqty = 'Yes'                                                                    
and substring(po_num,1,3) in('IPO','LPO')                                                                 
and po_category not in('AVAZONIC','IT')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                       
                                      
                                      
/*--- Preorder cases L3 Approval -----*/                                   
$sql ="update tipldb..po_master_table set is_preorder_pr = 'Yes'                                         
where po_num in(select distinct po_num from tipldb..insert_po where type_of_pr = 'Project Pre Order')";
$query = $this->db->query($sql);                                    
                                    
$sql ="update tipldb..po_master_table set is_preorder_pr = 'No'                                      
where po_num in(select distinct po_num from tipldb..insert_po where type_of_pr != 'Project Pre Order')                                    
and is_preorder_pr is NULL";
$query = $this->db->query($sql);                                     
                                    
$sql ="update tipldb..po_master_table set is_preorder_pr = 'No'                                      
where is_preorder_pr is NULL";
$query = $this->db->query($sql);                                     
                                      
                                      
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No'                                                              
WHERE is_preorder_pr = 'Yes'                                                                    
and substring(po_num,1,3) in('SRV','CGP')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                    
                                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE is_preorder_pr = 'Yes'                                                                     
and substring(po_num,1,3) in('FPO')                           
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                    
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE is_preorder_pr = 'Yes'                                                                    
and substring(po_num,1,3) in('IPO','LPO')                                                       
and po_category not in('AVAZONIC','IT')                                       
and lvl3_app_req is NULL";
$query = $this->db->query($sql);       
 
/*--- TCD Changes L3 Approval Conditions -----*/   
$sql ="update tipldb..po_master_table set tcd_increased = 'Yes'     
where total_tcd > (    
 select max(total_tcd) from tipldb..po_master_table_amend_history where po_num = po_master_table.po_num    
)";
$query = $this->db->query($sql);    
    
$sql ="update tipldb..po_master_table set tcd_increased = 'No' where tcd_increased is null";
$query = $this->db->query($sql);    
    
    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No'                                                              
WHERE tcd_increased = 'Yes'                                                                    
and substring(po_num,1,3) in('SRV','CGP')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                    
                                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE tcd_increased = 'Yes'                                                                     
and substring(po_num,1,3) in('FPO')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                   
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE tcd_increased = 'Yes'                                                                    
and substring(po_num,1,3) in('IPO','LPO')                                                       
and po_category not in('AVAZONIC','IT')                                       
and lvl3_app_req is NULL";
$query = $this->db->query($sql);    
 
/*--- PO Value Increased approval L3 -----*/ 
   
$sql ="update a     
set diff_curr_hist_poval = (a.po_total_value-b.po_total_value)     
from tipldb..po_master_table a, tipldb..po_master_table_amend_history b    
where a.po_num = b.po_num     
and b.po_total_value = (select max(po_total_value) from tipldb..po_master_table_amend_history where po_num = b.po_num)   
and b.po_s_no = (SELECT MAX(po_s_no) FROM tipldb..po_master_table_amend_history WHERE po_num = b.po_num)";
$query = $this->db->query($sql);   
    
$sql ="update tipldb..po_master_table set diff_curr_hist_poval_greater = 'Yes' where diff_curr_hist_poval > 1500";
$query = $this->db->query($sql);
  
$sql ="update tipldb..po_master_table set diff_curr_hist_poval_greater = 'No' where diff_curr_hist_poval <= 1500";
$query = $this->db->query($sql);  
 
$sql ="update tipldb..po_master_table set diff_curr_hist_poval_greater = 'No' where diff_curr_hist_poval_greater is NULL";
$query = $this->db->query($sql);    
    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No'                                                              
WHERE diff_curr_hist_poval_greater = 'Yes'                                                                    
and substring(po_num,1,3) in('SRV','CGP')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                     
                                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE diff_curr_hist_poval_greater = 'Yes'                                                                     
and substring(po_num,1,3) in('FPO')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                     
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                       
WHERE diff_curr_hist_poval_greater = 'Yes'                                                                    
and substring(po_num,1,3) in('IPO','LPO')                                                       
and po_category not in('AVAZONIC','IT')                                       
and lvl3_app_req is NULL";
$query = $this->db->query($sql);     
  

/*--- Freight Amount Increase 100 Approval condition -----*/
$sql ="UPDATE a set  a.frt_inc_amt = a.frt_amt_erp-b.frt_amt_erp   
FROM tipldb..po_master_table a, tipldb..po_master_table_amend_history b   
WHERE a.po_num = b.po_num  
and b.frt_amt_erp = (SELECT MAX(frt_amt_erp) FROM tipldb..po_master_table_amend_history WHERE po_num = b.po_num)  
and b.po_s_no = (SELECT MAX(po_s_no) FROM tipldb..po_master_table_amend_history WHERE po_num = b.po_num)  
and a.frt_amt_erp is not null";
$query = $this->db->query($sql);   
  
$sql ="update tipldb..po_master_table set frt_amt_inc = 'Yes' where frt_inc_amt > 100";
$query = $this->db->query($sql);  
 
$sql ="update tipldb..po_master_table set frt_amt_inc = 'No' where frt_inc_amt <= 100";
$query = $this->db->query($sql);  
 
$sql ="update tipldb..po_master_table set frt_amt_inc = 'No' where frt_inc_amt is null";
$query = $this->db->query($sql);   
  
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No'                                                              
WHERE frt_amt_inc = 'Yes'                                                                    
and substring(po_num,1,3) in('SRV','CGP')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                    
                                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE frt_amt_inc = 'Yes'                                                                     
and substring(po_num,1,3) in('FPO')                                                    
and lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                    
                                                    
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'Yes'                                                                    
WHERE frt_amt_inc = 'Yes'                                                                    
and substring(po_num,1,3) in('IPO','LPO')                                                       
and po_category not in('AVAZONIC','IT')                                       
and lvl3_app_req is NULL";
$query = $this->db->query($sql);   
    
 
/*--- Preorder cases L3 Approval ends -----*/                           
$sql ="UPDATE tipldb..po_master_table SET lvl3_app_req = 'No' WHERE lvl3_app_req is NULL";
$query = $this->db->query($sql);                                                                   
                                                                  
$sql ="UPDATE tipldb..po_master_table SET level3_mail_to = 'abhinav.toshniwal', level3_approval_by = 'abhinav.toshniwal' WHERE lvl3_app_req = 'Yes'";
$query = $this->db->query($sql);                                                                
                                                              
$sql ="UPDATE tipldb..po_master_table SET lvl1_approval_date = a.po_approveddate_lvl1 FROM tipldb..insert_po a
WHERE po_master_table.po_num = a.po_num and a.po_approval_lvl1 = 'Approve'";
$query = $this->db->query($sql);                                                 
                                                              
$sql ="UPDATE tipldb..po_master_table SET lvl2_approval_date = a.po_approvaldate_lvl2 FROM tipldb..insert_po a
WHERE po_master_table.po_num = a.po_num and a.po_approval_lvl2 = 'Approve'";
$query = $this->db->query($sql);                                                              
                         
$sql ="UPDATE tipldb..po_master_table SET lvl3_approval_date = a.po_approvaldate_lvl3 FROM tipldb..insert_po a
WHERE po_master_table.po_num = a.po_num and a.po_approval_lvl3 = 'Approve'";
$query = $this->db->query($sql);                                                   
                          
/*--- LVL 3 APP CONDITIONS -----*/                                              
                                                

/*--- Updating Customer Code and Customer Name in insert_po table and insert_po backup table -----*/                                               
$sql ="UPDATE tipldb..insert_po SET customer_code = a.customer_code, customer_name = a.customer_name                                                  
FROM tipldb..pr_submit_table a WHERE a.pr_num = tipldb..insert_po.po_ipr_no";
$query = $this->db->query($sql);                                                
                                                
$sql ="UPDATE tipldb..insert_po_amend_history SET customer_code = a.customer_code, customer_name = a.customer_name
FROM tipldb..pr_submit_table a WHERE a.pr_num = tipldb..insert_po_amend_history.po_ipr_no";
$query = $this->db->query($sql);                                                
                                          
 
/*--- MC & DI Update In PO Master -----*/                                
$sql ="update tipldb..po_master_table set mc_req = 'No', di_req = 'No'                                   
where po_category in('TOOLS','PRODUCTION CONSUMABLES','NON PRODUCTION CONSUMABLES','IT','SENSORS')";
$query = $this->db->query($sql);                            
                            
$sql ="update tipldb..po_master_table set mc_given = 'No' where mc_req = 'Yes' and mc_given is NULL";
$query = $this->db->query($sql); 
                                            
$sql ="update tipldb..po_master_table set di_given = 'No' where di_req = 'Yes' and di_given is NULL";
$query = $this->db->query($sql);                                    
                                                  

/*--- Road Permit Conditions -----*/                                        
$sql ="update tipldb..po_master_table set road_permit_req = a.road_permit                                           
from tipldb..road_permit_state a, scmdb..supp_addr_address b                                          
where a.tin_two_digits = b.supp_addr_state                       
and b.supp_addr_supcode = tipldb..po_master_table.supplier_code";
$query = $this->db->query($sql);                                          
                                          
$sql ="update tipldb..po_master_table set road_permit_req = 'NO' where substring(po_num, 1,3) in('FPO')";
$query = $this->db->query($sql);                                          
                                          
$sql ="update tipldb..po_master_table set road_permit_req = 'NO' where road_permit_req = ''";
$query = $this->db->query($sql);                                           
                                           

/*--- Updating PO Master Table Ends -----*/                                 
   
/*--- Updating SO Number in PO Master Table -----*/                            
$sql ="update tipldb..insert_po set sono = a.sono                                 
from tipldb..pr_submit_table a                                 
where a.pr_num =  tipldb..insert_po.po_ipr_no";
$query = $this->db->query($sql);                                
                                
$sql ="update tipldb..insert_po_amend_history set sono = a.sono                                 
from tipldb..pr_submit_table a                
where a.pr_num =  tipldb..insert_po_amend_history.po_ipr_no";
$query = $this->db->query($sql);                           
                          
 
/*--- Updating Acknowledge Updated Date and acknowledge updated by  -----*/                         
$sql ="update po_master_table                           
set ack_supp_by = a.ack_supp_by, ack_supp_date = a.ack_supp_date                           
from insert_po a                           
where a.po_num = po_master_table.po_num";
$query = $this->db->query($sql);  
 
/*--- Updating L1,L2,L3 Approval Date -----*/                       
$sql ="update po_master_table                         
set lvl1_approval_date = a.po_approveddate_lvl1                        
from tipldb..insert_po a                         
where a.po_num = po_master_table.po_num                        
and po_master_table.lvl1_approval_date is null";
$query = $this->db->query($sql);                       
                        
$sql ="update po_master_table                         
set lvl2_approval_date = a.po_approvaldate_lvl2                        
from tipldb..insert_po a                         
where a.po_num = po_master_table.po_num                        
and po_master_table.lvl2_approval_date is null";
$query = $this->db->query($sql);                        
                        
$sql ="update po_master_table                         
set lvl3_approval_date = a.po_approvaldate_lvl3                        
from tipldb..insert_po a                         
where a.po_num = po_master_table.po_num                        
and po_master_table.lvl3_approval_date is null";
$query = $this->db->query($sql);                            
                                                          

/*--- updating PM Group for services cases -----*/                    
$sql ="update insert_po set pm_group = a.spo_pm_group, ipr_category = a.po_category from po_master_table a                     
where a.po_num = insert_po.po_num and substring(a.po_num,1,3) in('SRV')";
$query = $this->db->query($sql);                      
                    
$sql ="update tipldb..insert_po set pm_group = 'No PM Group' where pm_group = ''";
$query = $this->db->query($sql);
                    
$sql ="update tipldb..insert_po set pm_group = 'No PM Group' where pm_group is null";
$query = $this->db->query($sql);           
          
$sql ="update tipldb..po_master_table set mc_given = a.mc_given, leadtime_supp = a.leadtime_supp,                   
leadtime_supp_rmks = a.leadtime_supp_rmks, leadtime_updated_by=a.leadtime_updated_by, leadtime_update_date=a.leadtime_update_date,                   
mc_plan_rmks = a.mc_plan_rmks, mc_plan_by = a.mc_plan_by, mc_plan_date = a.mc_plan_date,                  
mc_pur_rmks = a.mc_pur_rmks, mc_pur_by = a.mc_pur_by, mc_pur_date = a.mc_pur_date                  
from tipldb..po_master_table_amend_history a                   
where a.po_num = po_master_table.po_num               
and a.po_amend_no < po_master_table.po_amend_no                  
and a.po_s_no = (              
 select max(po_s_no) from tipldb..po_master_table_amend_history               
 where po_num = a.po_num and po_amend_no < po_master_table.po_amend_no           
)          
and a.mc_given = 'Yes'";
$query = $this->db->query($sql);         
          
$sql ="update tipldb..po_master_table set di_given = a.di_given, di_plan_rmks = a.di_plan_rmks, di_plan_by = a.di_plan_by,           
di_plan_date = a.di_plan_date, di_pur_rmks = a.di_pur_rmks,                   
di_pur_by = a.di_pur_by, di_pur_date = a.di_pur_date                  
from tipldb..po_master_table_amend_history a                   
where a.po_num = po_master_table.po_num               
and a.po_amend_no < po_master_table.po_amend_no                  
and a.po_s_no = (              
 select max(po_s_no) from tipldb..po_master_table_amend_history               
 where po_num = a.po_num and po_amend_no < po_master_table.po_amend_no              
)          
and a.di_given = 'Yes'";
$query = $this->db->query($sql);            
            
$sql ="update po_master_table set mc_plan_rmks = a.mc_planning_rmks,             
mc_plan_by = a.mc_planning_by, mc_plan_date = a.mc_planning_date, mc_given='Yes'            
from insert_po a where a.po_num = po_master_table.po_num             
and a.mc_planning_date is not NULL";
$query = $this->db->query($sql);                
 
/*--- System Calculated Material rec date -----*/              
$sql ="update tipldb..po_master_table set leadtime_week = substring(leadtime_supp,1,2) where leadtime_supp is not null and leadtime_supp != 'Below 1 Week'";
$query = $this->db->query($sql);
                
$sql ="update tipldb..po_master_table set leadtime_week = 1 where leadtime_supp is not null and leadtime_supp = 'Below 1 Week'";
$query = $this->db->query($sql);
                  
$sql ="update tipldb..po_master_table set supp_for_pur_date = a.supp_for_pur_date from tipldb..insert_po a where a.po_num = po_master_table.po_num";
$query = $this->db->query($sql);                  
                
$sql ="update tipldb..po_master_table set system_mat_rec_date =                 
case                
 when di_req = 'Yes' and di_given = 'Yes' and di_plan_date is not NULL then DATEADD(week,leadtime_week,di_plan_date)                
 when mc_req = 'Yes' and mc_given = 'Yes' and mc_plan_date is not NULL Then DATEADD(week,leadtime_week,mc_plan_date)                
 when mc_req = 'No' and  supp_for_pur_date is not NULL then DATEADD(week,leadtime_week,supp_for_pur_date)                
 when di_req = 'No' and  supp_for_pur_date is not NULL then DATEADD(week,leadtime_week,supp_for_pur_date)                
 when mc_req = NULL and  supp_for_pur_date is not NULL then DATEADD(week,leadtime_week,supp_for_pur_date)                
 when di_req = NULL and  supp_for_pur_date is not NULL then DATEADD(week,leadtime_week,supp_for_pur_date)                
 when supp_for_pur_date is not NULL then NULL                
end";
$query = $this->db->query($sql);         
        
   
/*--- Byepass system Code -----*/       
$sql ="update tipldb..po_master_table set         
gate_entry_no = a.gate_entry_no, gate_entry_date = a.gate_entry_date, ack_rmks = a.ack_rmks, 
ack_supp_by = a.ack_supp_by, ack_supp_date = a.ack_supp_date,        
mcdi_ra_rmks = a.mcdi_ra_rmks, mcdi_ra_app_inst = a.mcdi_ra_app_inst, 
mcdi_ra_app_by = a.mcdi_ra_app_by, mcdi_ra_app_date = a.mcdi_ra_app_date,        
mcdi_pi_rmks = a.mcdi_pi_rmks, mcdi_pi_app_inst = a.mcdi_pi_app_inst, 
mcdi_pi_app_by = a.mcdi_pi_app_by, mcdi_pi_app_date = a.mcdi_pi_app_date,        
mcdi_cf1_rmks = a.mcdi_cf1_rmks, mcdi_cf1_app_inst = a.mcdi_cf1_app_inst,
 mcdi_cf1_app_by = a.mcdi_cf1_app_by, mcdi_cf1_app_date = a.mcdi_cf1_app_date,        
mcdi_cf2_rmks = a.mcdi_cf2_rmks, mcdi_cf2_app_inst = a.mcdi_cf2_app_inst, mcdi_cf2_app_by = a.mcdi_cf2_app_by, mcdi_cf2_app_date = a.mcdi_cf2_app_date        
from tipldb..po_master_table_amend_history a        
where  a.mcdi_cf2_app_inst = 'APPROVE'         
and a.mcdi_cf2_app_date is not null        
and a.po_s_no = (select max(po_s_no) from tipldb..po_master_table_amend_history where po_num = a.po_num)        
and a.po_num = tipldb..po_master_table.po_num";
$query = $this->db->query($sql);        
        
        
$sql ="update tipldb..po_master_table set wo_mcdi_app = 'Yes' where mc_req = 'Yes' and mc_given = 'No' and wo_mcdi_app is null";
$query = $this->db->query($sql);
        
$sql ="update tipldb..po_master_table set wo_mcdi_app = 'Yes' where di_req = 'Yes' and di_given = 'No' and wo_mcdi_app is null";
$query = $this->db->query($sql);
       
$sql ="update tipldb..po_master_table set wo_mcdi_app = 'No' where mcdi_cf2_app_inst = 'APPROVE'";
$query = $this->db->query($sql);
       
$sql ="update tipldb..po_master_table set wo_mcdi_app = 'No' where wo_mcdi_app is null";
$query = $this->db->query($sql);        
        
$sql ="update a set gate_entry_no = b.entry_no, gate_entry_date = b.entry_date         
from tipldb..po_master_table a, tipldb..gateentry_item_rec_master b         
where a.po_num = b.po_num         
and a.gate_entry_no is null        
and b.entry_date = (select max(entry_date) from tipldb..gateentry_item_rec_master where entry_no = b.entry_no and po_num = b.po_num)";
$query = $this->db->query($sql);        
        
$sql ="update a set a.status = c.status         
from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b, tipldb..gateentry_item_rec_master c      
where a.po_num = b.pomas_pono        
and a.po_num = c.po_num        
and a.gate_entry_no = c.entry_no        
and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)        
and a.gate_entry_no is not null        
and a.status not in('Draft','PO Send For Level 1 Authorization','PO Disapproved At Level 1',        
'PO Send For Level 2 Authorization','PO Disapproved At Level 2',        
'PO Send For Level 3 Authorization','PO Disapproved At Level 3')        
and c.status not in('GRCPT_Created')        
and b.pomas_podocstatus in('OP')";
$query = $this->db->query($sql);

?>