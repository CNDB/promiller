<?php include'header.php'; ?>

<section id="main-content">
  <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                  <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">MRP ITEM REQUEST REPORT</h4>
            </div>
        </div><br />
        
        <div class="row">   
            <div class="col-lg-12">
                <table align="center" cellpadding="0" cellspacing="0" class="table table-bordered" border="1">
                	<tr style="background:#00CCFF">
                    	<td><b>SNO.</b></td>
                        <td><b>ITEM CODE</b></td>
                        <td><b>ITEM DESC</b></td>
                        <td><b>CATEGORY</b></td>
                        <td><b>STOCK UOM</b></td>
                        <td><b>PURCHASE UOM</b></td>
                        <td><b>MRP QUANTITY</b></td>
                        <td><b>PR TYPE</b></td>
                        <td><b>PR REQ. QTY</b></td>
                        <td><b>REMARKS</b></td>
                        <td><b>CREATED BY</b></td>
                        <td><b>STATUS</b></td>
                        <td><b>AGE</b></td>
                        <td><b>ACTION</b></td>
                    </tr>
                    <?php
						$srno = 0;
						
						foreach($mrp_item_report->result() as $row){
							$srno++;
							$item_code = $row->item_code;
							$item_desc = $row->item_desc;
							$category = $row->category;
							$stock_uom = $row->stock_uom;
							$purchase_uom = $row->purchase_uom;
							$mrp_required_qty = $row->mrp_required_qty;
							$pr_type = $row->pr_type;
							$pr_req_qty = $row->pr_req_qty;
							$remarks = $row->remarks;
							$created_by = $row->requested_by;
							$status = $row->status;
							$age = $row->diff;
							$sno = $row->sno;
							
					?>
                    <tr>
                    	<td><?php echo $srno; ?></td>
                        <td><?php echo $item_code; ?></td>
                        <td><?php echo $item_desc; ?></td>
                        <td><?php echo $category; ?></td>
                        <td><?php echo $stock_uom; ?></td>
                        <td><?php echo $purchase_uom; ?></td>
                        <td><?php echo $mrp_required_qty; ?></td>
                        <td><?php echo $pr_type; ?></td>
                        <td><?php echo $pr_req_qty; ?></td>
                        <td><?php echo $remarks; ?></td>
                        <td><?php echo $created_by; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo $age; ?></td>
                        <td>
                            <a href="<?php echo base_url(); ?>index.php/mrp_reportc/delete_item_view?id=<?php echo $sno; ?>">
                            	<img src="<?php echo base_url('assets/admin/img/');?>edit_icon.png" width="25px" height="25px" />
                            </a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->

<?php include 'footer.php'; ?>