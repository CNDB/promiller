
<?php 
include('header.php');

$username = $_SESSION['username'];

$sql_user = "select * from tipldb..login where email like'%$username%'";
$qry_user = $this->db->query($sql_user);

foreach($qry_user->result() as $row){
	$user_id = $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
} 

$item_code = $this->uri->segment(3);

if(strpos($item_code, 'chandra') !== false){
	$item_code2 = str_replace("chandra","%2F",$item_code);
}else{
	$item_code2 = $item_code;
}

$itm_code = urldecode($item_code2);

$query_str = "http://live.tipl.com/tipl_project1/rkg/pendalcard_detail.php?itemcode=".rawurlencode($itm_code)."&usernm=".$username."&name=".$name."&user_type=".$user_type."&user_id=".$user_id;

header('Location:'.$query_str);

die;
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">P e n d a l C a r d</h4>
        </div>
    </div><br />
	<?php
        foreach ($pendal->result() as $row){
			$item_code = $row->ItemCode;
			$item_category = $row->ItemCategory;
			$item_type = $row->ItemType;
			$supp_code = $row->SuppCode;
			$wip_tran_type = $row->WipTranType;
			$item_description = $row->ItemDescription;
			$purchase_uom  = $row->ItemPurcaseUom;
			$manufact_uom = $row->ItemMnfgUom;
			$sale_uom = $row->ItemPurcaseUom;
			$stock_uom = $row->ItemStockUom;
			$drawing_no = $row->DrawingNo;
			$item_usage = $row->ItemUsage;
			$drawing_date = $row->DrwaingDate;
			$source = $row->ItemSource; 
			$warehouse = $row->ItemWarehouse;
			$safety_stock = $row->ItemSafetyStock;
			$costing_method = $row->ItemCostingMethod;
			$reorder_level = $row->ItemSafetyStockOL;
			$lead_time = $row->ItemLeadTime;
			$reorder_qty = $row->ItemSafetyStockOQ;
			$status = $row->ItemStkWarehouse;
			$stock_rate = $row->ItemStockRate;
			$last_recipt_qty = $row->PendPoSchQty;
			$last_issue_qty = $row->LastYrConsumQty;
			$last_recipt_date1 = substr($row->PendPODate,0,11);
			$last_issue_date1 = substr($row->LastMoveDt,0,11);
			
			$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";
			
			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {
			  foreach ($query->result() as $row) {
				  $ucon_confact_ntr = $row->ucon_confact_ntr;
				  $ucon_confact_dtr = $row->ucon_confact_dtr;
				  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
			  }
			} else {
				   $conversion_factor = "";  
			}
     ?>
    <div class="row"  style="background:#00CCFF; margin-top:-20px; padding-top:5px; padding-bottom:5px;">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:13px;">
              <div class="col-lg-1">Item Code: </div>
          
              <div class="col-lg-1">
              <a href="http://live.tipl.com/tipl_project1/goods_receipt/reorder_update_page.php?item_code=<?php echo urlencode($item_code); ?>&user_id=<?php echo $user_id; ?>&user_type=<?php echo $user_type?>&name=<?php echo $name; ?>">
                <?php echo $item_code; ?>
              </a>
              </div>
              
              <div class="col-lg-1">Item Cat: </div>
              
              <div class="col-lg-1"><?php echo $item_category; ?></div>
              
              <div class="col-lg-1">Item Type:</div>
              
              <div class="col-lg-1"><?php echo $item_type; ?></div>
              
              <div class="col-lg-1">Location:</div>
              
              <div class="col-lg-1"><?php echo $supp_code; ?></div>
              
              <div class="col-lg-2">ITEM ABC/XYZ-Priority:</div>
              
              <div class="col-lg-2"><b style="font-size:36px; color:red"><?php echo $wip_tran_type; ?></b></div>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:13px;">
            ITEM DESCRIPTION: &nbsp; <?php echo mb_convert_encoding($item_description, "ISO-8859-1", "UTF-8"); ?>
        </div>
    </div><br />
     <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-6">
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>UOM Type</th>
                        <th>UOM</th>
                        <th>Conversion Factor With Stock UOM</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Purchase Uom</td>
                        <td><?php echo $purchase_uom; ?></td>
                        <td><?php echo number_format($conversion_factor,4); ?></td>                            
                      </tr>
                       <tr>
                        <td>Manufacturing Uom</td>
                        <td><?php echo $manufact_uom; ?></td>
                        <td></td>
                      </tr>
                       <tr>
                        <td>Sale Uom</td>
                        <td><?php echo $sale_uom; ?></td>
                        <td></td>
                      </tr>
                       <tr>
                        <td>Stock Uom</td>
                        <td><?php echo $stock_uom; ?></td>
                        <td></td>
                      </tr>
                    </tbody>
               </table>
            </div>
            <div class="col-lg-6">
                <table class="table table-bordered" id="dataTable" border="1">
                    <tbody>
                        <tr>
                            <td><b>Drawing No.</b></td>
                            <td><?php echo $drawing_no; ?></td>
                            <td><b>Usage</b></td>
                            <td><?php echo $item_usage; ?></td>
                        </tr>
                        <tr>
                            <td><b>Drawing Date</b></td>
                            <td><?php echo $drawing_date; ?></td>
                            <td><b>Source</b></td>
                            <td><?php echo $source; ?></td>
                        </tr>
                        <tr>
                            <td><b>Warehouse</b></td>
                            <td><?php echo $warehouse; ?></td>
                            <td><b>Safety Stock</b></td>
                            <td><?php echo number_format($safety_stock,2); ?></td>
                        </tr>
                        <tr>
                            <td><b>Costing Method</b></td>
                            <td><?php echo $costing_method; ?></td>
                            <td><b>Reorder Level</b></td>
                            <td><?php echo number_format($reorder_level,2); ?></td>
                        </tr>
                        <tr>
                            <td><b>Lead Time</b></td>
                            <td><?php echo number_format($lead_time,2); ?></td>
                            <td><b>Reorder Quantity</b></td>
                            <td><?php echo number_format($reorder_qty,2); ?></td>
                        </tr>
                        <tr>
                            <td><b>Status</b></td>
                            <td><?php echo $status; ?></td>
                            <td><b>Stock Rate</b></td>
                            <td><?php echo number_format($stock_rate,2); ?></td>
                        </tr>
                        <tr>
                            <td><b>Last Receipt Date</b></td>
                            <td>
                            <?php 
								if($last_recipt_date1 != ''){
									$last_recipt_date = date("d-m-Y", strtotime($last_recipt_date1)); 
									echo $last_recipt_date;
								} else {
									$last_recipt_date = ''; 
									echo $last_recipt_date;
								}
                            ?>
                            </td>
                            <td><b>Last Issue Date</b></td>
                            <td>
                            <?php 
                                if($last_issue_date1 != ''){
									$last_issue_date = date("d-m-Y", strtotime($last_issue_date1));
									echo $last_issue_date; 
								} else {
									$last_issue_date = '';
									echo $last_issue_date; 
								}
                            ?>
                            </td>
                        </tr>
                        <tr>
                            <td><b>Last Receipt Qty</b></td>
                            <td><?php echo number_format($last_recipt_qty,2); ?></td>
                            <td><b>Last Issue Qty</b></td>
                            <td><?php echo number_format($last_issue_qty,2); ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div><br />
    <?php } ?>
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            <a href="http://live.tipl.com/tipl_project1/rkg/itemldg.php?item=<?php echo $item_code; ?>" target="_blank">VIEW ITEM LEDGER</a>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            REORDER DETAILS
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered" id="dataTable" border="1">
                <thead> 
                <tr>
                	<th></th>
                    <th>Reorder Level</th>
                    <th>Reorder Quantity</th>
                    <th>Safety Stock</th>
                </tr>
                </thead>
                <tbody>
                    <tr>
                    	<td style="background-color: rgb(204, 204, 204);"><b>ERP</b></td>
                        <td><?php echo number_format($reorder_level,2); ?></td>
                        <td><?php echo number_format($reorder_qty,2); ?></td>
                        <td><?php echo number_format($safety_stock,2); ?></td>
                    </tr>
                    <tr>
                    	<td style="background-color: rgb(204, 204, 204);"><b>SUGGESTED</b></td>    
                        <?php
                        $per_day_consumption = 0;
                        $avg_age = 0;
                        $buffer_stock = 0;
                        $system_reorder_level = 0;
                        
                        foreach($cons_quantity->result() as $row){
                            $ConsTotalQty = $row->ConsTotalQty;
                            $per_day_consumption = $ConsTotalQty/300;	
                        }
                        
                        foreach($lead_time_qry->result() as $row){
                            $avg_age = $row->avg_age;	
                        }
                        
                        $buffer_stock = ($per_day_consumption*$avg_lead_time)*10/100;
                        $system_reorder_level = ($per_day_consumption*$avg_lead_time) + $buffer_stock;
                        
                        ?>
                        
                        <td><?php echo number_format($system_reorder_level,2); ?></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            ITEM CREATION HISTORY
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered" id="dataTable" border="1">
                <thead> 
                <tr>
                    <th>S.No.</th>
                    <th>Remark</th>
                    <th>Remark By</th>
                    <th>Remark Date</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $seq=1;
                foreach($item_creation_hist->result() as $row){
                    $remark = $row->remark;
                    $remark_by = $row->remark_by;
                    $remark_dt = $row->remark_dt;
                    $status = $row->status;
                ?>
                    <tr>
                        <td align="center"><?php echo $seq; ?></td>
                        <td><?php echo $remark; ?></td>
                        <td><?php echo $remark_by; ?></td>
                        <td><?php echo date("d-m-Y h:i A", strtotime($remark_dt)); ?></td>
                        <td><?php echo $status; ?></td>
                    </tr>
                <?php $seq++; } ?>
                </tbody>
            </table>
        </div>
    </div><br />
    
    <?php
        foreach($item_drawing->result() as $row){
            $fetch1 = $row->file_title;
        }
        
        if($fetch1 != ''){	
    ?>
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            Drawing Attachments
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered" id="dataTable" border="1">
                <thead> 
                <tr>
                    <th>S.No.</th>
                    <th>File Title</th>
                    <th>Uploaded By</th>
                    <th>Uploaded Time</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $seq=1;
                foreach($item_drawing_det->result() as $row){
                    $file_name = $row->file_name;
                    $file_title = $row->file_title;
                    $uploaded_by = $row->upload_by;
                    $upload_time = $row->upload_time;
                ?>
                    <tr align="center">
                        <td><?php echo $seq; ?></td>
                        <td>
                        <a href="http://live.tipl.com/tipl_project1/itemCreation/miduploadpage.php?filenm=<?php echo $file_name; ?>&foldnm=<?php echo 'upload_attachment'; ?>" target="_blank">
                            <?php echo $file_title; ?>
                        </a>
                        </td>
                        <td><?php echo $uploaded_by; ?></td>
                        <td><?php echo date("d-m-Y h:i A", strtotime($upload_time)); ?></td>
                    </tr>
                <? $seq++; } ?>
                </tbody>
            </table>
        </div>
    </div><br />
    
    <?php } ?>
    
    
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            CURRENT WAREHOUSE STOCK BALANCE
        </div>
    </div><br />
             
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>Warehouse</th>
                        <th>Accepted</th>
                        <th>Allocated</th>
                        <th>Rejected</th>
                        <th>Held</th>
                        <th>FREE</th>
                        <th>Total</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($pendalstkblnc->result() as $row) { ?>
                      <tr>
                        <td><?php echo $row->ItemStkWarehouse; ?></td>
                        <td><?php echo number_format($row->ItemStkAccepted,2); ?></td>
                        <td><?php echo number_format($row->ItemStkAllocated,2); ?></td>
                        <td><?php echo number_format($row->ItemStkRejected,2); ?></td>
                        <td><?php echo number_format($row->ItemStkHeld,2); ?></td>
                        <td><?php echo number_format($row->ItemStkFree,2); ?></td>
                        <td><?php echo number_format($row->ItemStkTotal,2); ?></td>                             
                      </tr>
                    </tbody>
                   <?php } ?>
               </table>
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            PENDING P.O./ S.C.O./W.O./PR/SCR
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>PO/SCO/WO</th>
                        <th>Transaction No.</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Order Qty.</th>
                        <th>Firmed Qty.</th>
                        <th>Unfirmed Qty</th>
                        <th>Due Date</th>
                        <th>Line Level Notes/Doc Level Notes</th>
                      </tr>
                    </thead>
                   
                    
                    <tbody>
                    <?php 
                        if($pndposopr->result() != '')
                        {
                            $tordqty="0"; $tfrmqty="0"; $tufmqty="0";
                        foreach ($pndposopr->result() as $row) { 
                        
                        $trantype = $row->PendTranType;
                        $tranno = $row->PendTranNo;
                        $trandt1 = substr($row->PendPODate,0,11);
						if($trandt1 != ''){
                        	$trandt = date("d-m-Y", strtotime($trandt1));
						} else {
							$trandt = '';
						}
                        $status = $row->PendStatus;
                        $ordqty = $row->PendPoSchQty;
                        $firmqty = $row->PendFirmQty;
                        $unfirmqty = $row->PendUnFirmQty;
                        $duedt = substr($row->PendDueDate,0,11);
						if($duedt != ''){
                        	$duedt1 = date("d-m-Y", strtotime($duedt));
						} else {
							$duedt1 = '';
						}
                        $docnote = $row->PendDocNotes;
                        $itemnote = $row->PendItemNotes;
                        
                        $tordqty=$tordqty + $ordqty;
                        $tfrmqty=$tfrmqty + $firmqty;
                        $tufmqty=$tufmqty + $unfirmqty;
                    
                    ?>
                      <tr>
                        <td><?php echo $trantype; ?></td>
                        <td><?php echo $tranno; ?></td>
                        <td><?php echo $trandt; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo number_format($ordqty,2); ?></td>
                        <td><?php echo number_format($firmqty,2); ?></td>
                        <td><?php echo number_format($unfirmqty,2); ?></td>
                        <td><?php echo $duedt1; ?></td>
                        <td><?php echo $docnote." ".$itemnote; ?></td>                             
                      </tr>
                    <?php } } ?>
                    <?php 
                        if($pndposopr_new->result() != '')
                        {
                            //$tordqty="0"; $tfrmqty="0"; $tufmqty="0";
                        foreach ($pndposopr_new->result() as $row) { 
                        
                        $trantype = $row->PendTranType;
                        $tranno = $row->PendTranNo;
                        $trandt1 = substr($row->PendPODate,0,11);
						if($trandt1 != ''){
                        	$trandt = date("d-m-Y", strtotime($trandt1));
						} else {
							$trandt = '';
						}
                        $status = $row->PendStatus;
                        $ordqty = $row->PendPoSchQty;
                        $firmqty = $row->PendFirmQty;
                        $unfirmqty = $row->PendUnFirmQty;
                        $duedt = substr($row->PendDueDate,0,11);
						if($duedt != ''){
                        	$duedt1 = date("d-m-Y", strtotime($duedt));
						} else {
							$duedt1 = '';
						}
                        $docnote = $row->PendDocNotes;
                        $itemnote = $row->PendItemNotes;
                        
                        $tordqty=$tordqty + $ordqty;
                        $tfrmqty=$tfrmqty + $firmqty;
                        $tufmqty=$tufmqty + $unfirmqty;
                    
                    ?>
                      <tr>
                        <td><?php echo $trantype; ?></td>
                        <td><?php echo $tranno; ?></td>
                        <td><?php echo $trandt; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo number_format($ordqty,2); ?></td>
                        <td><?php echo number_format($firmqty,2); ?></td>
                        <td><?php echo number_format($unfirmqty,2); ?></td>
                        <td><?php echo $duedt1; ?></td>
                        <td><?php echo $docnote." ".$itemnote; ?></td>                             
                      </tr>
                      <?php } } ?>
                      
                      <tr>
                        <td colspan="4" style="text-align:right">Total</td>
                        <td><?php echo number_format($tordqty,2); ?></td>
                        <td><?php echo number_format($tfrmqty,2); ?></td>
                        <td><?php echo number_format($tufmqty,2); ?></td>
                        <td colspan="2"></td>                             
                      </tr>
                    </tbody> 
               </table>
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            ALLOCATION DETAILS
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>WO/MR No</th>
                        <th>Status</th>
                        <th>Due Date</th>
                        <th>Firm Req.</th>
                        <th>Unfirmed Req.</th>
                        <th>Allocated</th>
                        <th>Issue Qty</th>
                        <th>Pending Allocation</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                      foreach ($allocat->result() as $row) 
                        {
                            $wo_mr_no               = $row->AllocTranNo;
                            $status                 = $row->AllocTranStatus;
                            $due_date              = substr($row->AllocDueDate,0,10);
                            //$due_date               = date("d-m-Y", strtotime($duedt1));
                            $firm_req               = number_format($row->AllocFirmReq,2);
                            $unfirm_req             = number_format($row->AllocUnFirmReq,2);
                            $allocated              = number_format($row->Allocated,2);
                            $issue_qty              = number_format($row->AllocIssueQty,2);
                            $pending_allocation     = number_format($row->AllocPending,2);
                      ?>
                      <tr>
                        <td><?php echo $wo_mr_no; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo $due_date; ?></td>
                        <td><?php echo $firm_req; ?></td>
                        <td><?php echo $unfirm_req; ?></td>
                        <td><?php echo $allocated; ?></td>
                        <td><?php echo $issue_qty; ?></td>
                        <td><?php echo $pending_allocation; ?></td>                             
                      </tr>
                      <?php } ?>
                      <?php 
                          foreach ($allocat_tot->result() as $row) 
                            {
                                $firm_req_tot           = number_format($row->AllocTotFirmReq,2);
                                $unfirm_req_tot         = number_format($row->AllocTotUnFirmReq,2);
                                $allocated_tot          = number_format($row->AllocatedTot,2);
                                $issue_qty_tot          = number_format($row->AllocIssueQtyTot,2);
                                $pending_allocation_tot = number_format($row->AllocPendingTot,2); 
                      ?>
                       <tr>
                        <td colspan="3" style="text-align:right">Total</td>
                        <td><?php echo $firm_req_tot; ?></td>
                        <td><?php echo $unfirm_req_tot; ?></td>
                        <td><?php echo $allocated_tot; ?></td>
                        <td><?php echo $issue_qty_tot; ?></td>
                        <td><?php echo $pending_allocation_tot; ?></td>                             
                      </tr>
                      <?php } ?>
                    </tbody>
               </table>
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            WIP DETAILS
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>WO No.</th>
                        <th>WO Qty</th>
                        <th>Due Date</th>
                        <th>WO Status</th>
                        <th>Tran No.</th>
                        <th>Date</th>
                        <th>Recipt Qty</th>
                        <th>Pending Qty</th>
                      </tr>
                    </thead>
                    <tbody>
                        <?php
						$pend_qty_tot = 0; 
						foreach ($wipdet->result() as $row) { 
							$wono=$row->WipWONo;
							$qty=$row->WipWOQty;
							$duedate=$row->WipWoDate;
							$tranno=$row->WipTranNo;
							$trandt=$row->WipTranDate;
							$rcptqty=$row->WipRcptQty;
							$pendqty=$row->WipPendQty;
							$pend_qty_tot = $pend_qty_tot+$pendqty;
						?>
                      <tr>
                        <td><?php echo $wono; ?></td>
                        <td><?php echo number_format($qty,2,".",""); ?></td>
                        <td><?php echo substr($duedate,0,11); ?></td>
                        <td><?php echo substr($duedate,0,11); ?></td>
                        <td><?php echo $trnno; ?></td>
                        <td><?php echo $trandt; ?></td>
                        <td><?php echo $rcptqty; ?></td>
                        <td><?php echo number_format($pendqty,2,".",""); ?></td>                             
                      </tr>
                       <?php } ?>
                       <tr>
                        <td colspan="6" style="text-align:right">Total</td>
                        <td></td>
                        <td><?php echo number_format($pend_qty_tot,2,".",""); ?></td>                            
                      </tr>
                    </tbody>
               </table>
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">LAST TEN TRANSANCTIONS</div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1" style="font-size:10px">
                    <thead>
                      <tr>
                        <th>PO/SCO/WO No</th>
                        <th>GRCPT/Production Receipt</th>
                        <th>Status</th>
                        <th>Rate</th>
                        <th>Supplier Name</th>
                        <th>PR/SCR No</th>
                        <th>Creation Of PR/SCR Date</th>
                        <th>Need Date</th>
                        <th>GRCPT(Freeze And Move Date)</th>
                        <th>Lead Time</th>
                        <th>Receipt Qty</th>
                        <th>Accepted Qty</th>
                        <th>Rejected Qty</th>
                        <th>Payment Term</th>
                    	<th>Freight Term</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          foreach ($lastfivetrans->result() as $row) 
                            {
                                $po_sco_wo_no = $row->LastPOGRTran;
                                $production_recipt = $row->LastReceipt;
                                $status = $row->AllocTranStatus;
                                $last_rate = number_format($row->LastRate,2);
                                $last_supplier = $row->LastSupplier;
                                $last_prscrtran = $row->LastPRSCRTran;
								
                                $last_prscrdate1 = substr($row->LastPRSCRDate,0,10);
								if($last_prscrdate1 != ''){
                                	$last_prscrdate = date("d-m-Y", strtotime($last_prscrdate1));
								} else {
									$last_prscrdate = '';
								}
								
                                $last_prscrneeddate1 = substr($row->LastPRSCRNeedDt,0,10);
								if($last_prscrneeddate1 != ''){
                                	$last_prscrneeddate = date("d-m-Y", strtotime($last_prscrneeddate1));
								} else {
									$last_prscrneeddate = '';
								}
								
                                $last_move_date1 = substr($row->LastMoveDt,0,10);
								if($last_move_date1 != ''){
                                	$last_move_date = date("d-m-Y", strtotime($last_move_date1));
								} else {
									$last_move_date = '';
								}
								
                                $last_lead_time = $row->LastLeadTime;
                                $last_recipt_qty = number_format($row->LastTranQty,2);
                                $last_accepted_qty = number_format($row->CurrYrConsRecptJan,2);
                                $last_rejected_qty = number_format($row->CurrYrConsRecptFeb,2);
								$payterm = $row->payterm;
								$incoterm = $row->incoterm; 
                      ?>
                      <tr>
                        <td><?php echo $po_sco_wo_no; ?></td>
                        <td><?php echo $production_recipt; ?></td>
                        <td><?php echo $status; ?></td>
                        <td><?php echo $last_rate; ?></td>
                        <td><?php echo $last_supplier; ?></td>
                        <td><?php echo $last_prscrtran; ?></td>
                        <td><?php echo $last_prscrdate; ?></td>
                        <td><?php echo $last_prscrneeddate; ?></td> 
                        <td><?php echo $last_move_date; ?></td>
                        <td><?php echo $last_lead_time; ?></td>
                        <td><?php echo $last_recipt_qty; ?></td>
                        <td><?php echo $last_accepted_qty; ?></td>
                        <td><?php echo $last_rejected_qty; ?></td> 
                        <td><?php echo $payterm; ?></td>
                    	<td><?php echo $incoterm; ?></td>                           
                      </tr>
                      <?php } ?>
                    </tbody>
               </table>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">
            CURRENT YEAR CONSUMPTION AND RECIPT
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>April</th>
                        <th>May</th>
                        <th>June</th>
                        <th>July</th>
                        <th>August</th>
                        <th>Sep</th>
                        <th>Oct</th>
                        <th>Nov</th>
                        <th>Dec</th>
                        <th>Jan</th>
                        <th>Feb</th>
                        <th>Mar</th>
                        <th>Total</th>
                        <th>Avg</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($mnthwsecurryrcons->result() as $row) 
                      { 
					  $fin_yr  = $row->SuppName;
                      $apr 	 = $row->CurrYrConsRecptApril;
                      $apr_new = number_format($row->CurrYrConsRecptApril,2);
                      $may     = $row->CurrYrConsRecptMay;
                      $may_new = number_format($row->CurrYrConsRecptMay,2);
                      $jun     = $row->CurrYrConsRecptJune;
                      $jun_new = number_format($row->CurrYrConsRecptJune,2);
                      $jul     = $row->CurrYrConsRecptJuly;
                      $jul_new = number_format($row->CurrYrConsRecptJuly,2);
                      $aug     = $row->CurrYrConsRecptAugust;
                      $aug_new = number_format($row->CurrYrConsRecptAugust,2);
                      $sep     = $row->CurrYrConsRecptSeptember;
                      $sep_new = number_format($row->CurrYrConsRecptSeptember,2);
                      $oct     = $row->CurrYrConsRecptOctober;
                      $oct_new = number_format($row->CurrYrConsRecptOctober,2);
                      $nov     = $row->CurrYrConsRecptNovember;
                      $nov_new = number_format($row->CurrYrConsRecptNovember,2);
                      $dec     = $row->CurrYrConsRecptDecember;
                      $dec_new = number_format($row->CurrYrConsRecptDecember,2);
                      $jan     = $row->CurrYrConsRecptJan;
                      $jan_new = number_format($row->CurrYrConsRecptJan,2);
                      $feb     = $row->CurrYrConsRecptFeb;
                      $feb_new = number_format($row->CurrYrConsRecptFeb,2);
                      $mar     = $row->CurrYrConsRecptMarch;
                      $mar_new = number_format($row->CurrYrConsRecptMarch,2);
					  
					  if($apr == '' || $apr == NULL || $apr == 0){
						  $div_apr = 0;
					  } else {
						  $div_apr = 1;
					  }
					  
					  if($may == '' || $may == NULL || $may == 0){
						  $div_may = 0;
					  } else {
						  $div_may = 1;
					  }
					  
					  if($jun == '' || $jun == NULL || $jun == 0){
						  $div_jun = 0;
					  } else {
						  $div_jun = 1;
					  }
					  
					  if($jul == '' || $jul == NULL || $jul == 0){
						  $div_jul = 0;
					  } else {
						  $div_jul = 1;
					  }
					  
					  if($aug == '' || $aug == NULL || $aug == 0){
						  $div_aug = 0;
					  } else {
						  $div_aug = 1;
					  }
					  
					  if($sep == '' || $sep == NULL || $sep == 0){
						  $div_sep = 0;
					  } else {
						  $div_sep = 1;
					  }
					  
					  if($oct == '' || $oct == NULL || $oct == 0){
						  $div_oct = 0;
					  } else {
						  $div_oct = 1;
					  }
					  
					  if($nov == '' || $nov == NULL || $nov == 0){
						  $div_nov = 0;
					  } else {
						  $div_nov = 1;
					  }
					  
					  if($dec == '' || $dec == NULL || $dec == 0){
						  $div_dec = 0;
					  } else {
						  $div_dec = 1;
					  }
					  
					  if($jan == '' || $jan == NULL || $jan == 0){
						  $div_jan = 0;
					  } else {
						  $div_jan = 1;
					  }
					  
					  if($feb == '' || $feb == NULL || $feb == 0){
						  $div_feb = 0;
					  } else {
						  $div_feb = 1;
					  }
					  
					  if($mar == '' || $mar == NULL || $mar == 0){
						  $div_mar = 0;
					  } else {
						  $div_mar = 1;
					  }
					  
                      $tot     = $apr+$may+$jun+$jul+$aug+$sep+$oct+$nov+$dec+$jan+$feb+$mar;
					  $div_tot = $div_apr+$div_may+$div_jun+$div_jul+$div_aug+$div_sep+$div_oct+$div_nov+$div_dec+$div_jan+$div_feb+$div_mar;
                      $tot_new = number_format($tot,2);
                      $avg     = $tot/$div_tot;
                      $avg_new = number_format($avg,2);
                      ?>
                      <tr>
                        <td>Consumption ( <?php echo $fin_yr; ?> )</td>
                        
                        <td><?php echo $apr_new; ?></td>
                        <td><?php echo $may_new; ?></td>
                        <td><?php echo $jun_new; ?></td>
                        <td><?php echo $jul_new; ?></td>
                        <td><?php echo $aug_new; ?></td>
                        <td><?php echo $sep_new; ?></td>
                        <td><?php echo $oct_new; ?></td>
                        <td><?php echo $nov_new; ?></td>
                        <td><?php echo $dec_new; ?></td>
                        <td><?php echo $jan_new; ?></td>
                        <td><?php echo $feb_new; ?></td>
                        <td><?php echo $mar_new; ?></td>
                        <td><?php echo $tot_new; ?></td>
                        <td><?php echo $avg_new; ?></td>                          
                      </tr>
                      <?php } ?>
                      <?php foreach ($mnthwsecurryrrcpt->result() as $row){ 
					  
					  $fin_yr1  = $row->SuppName;
                      $apr1     = $row->CurrYrConsRecptApril;
                      $apr1_new = number_format($row->CurrYrConsRecptApril,2);
                      $may1     = $row->CurrYrConsRecptMay;
                      $may1_new = number_format($row->CurrYrConsRecptMay,2);
                      $jun1     = $row->CurrYrConsRecptJune;
                      $jun1_new = number_format($row->CurrYrConsRecptJune,2);
                      $jul1     = $row->CurrYrConsRecptJuly;
                      $jul1_new = number_format($row->CurrYrConsRecptJuly,2);
                      $aug1     = $row->CurrYrConsRecptAugust;
                      $aug1_new = number_format($row->CurrYrConsRecptAugust,2);
                      $sep1     = $row->CurrYrConsRecptSeptember;
                      $sep1_new = number_format($row->CurrYrConsRecptSeptember,2);
                      $oct1     = $row->CurrYrConsRecptOctober;
                      $oct1_new = number_format($row->CurrYrConsRecptOctober,2);
                      $nov1     = $row->CurrYrConsRecptNovember;
                      $nov1_new = number_format($row->CurrYrConsRecptNovember,2);
                      $dec1     = $row->CurrYrConsRecptDecember;
                      $dec1_new = number_format($row->CurrYrConsRecptDecember,2);
                      $jan1     = $row->CurrYrConsRecptJan;
                      $jan1_new = number_format($row->CurrYrConsRecptJan,2);
                      $feb1     = $row->CurrYrConsRecptFeb;
                      $feb1_new = number_format($row->CurrYrConsRecptFeb,2);
                      $mar1     = $row->CurrYrConsRecptMarch;
                      $mar1_new = number_format($row->CurrYrConsRecptMarch,2);
					  
					  if($apr1 == '' || $apr1 == NULL || $apr1 == 0){
						  $div_apr1 = 0;
					  } else {
						  $div_apr1 = 1;
					  }
					  
					  if($may1 == '' || $may1 == NULL || $may1 == 0){
						  $div_may1 = 0;
					  } else {
						  $div_may1 = 1;
					  }
					  
					  if($jun1 == '' || $jun1 == NULL || $jun1 == 0){
						  $div_jun1 = 0;
					  } else {
						  $div_jun1 = 1;
					  }
					  
					  if($jul1 == '' || $jul1 == NULL || $jul1 == 0){
						  $div_jul1 = 0;
					  } else {
						  $div_jul1 = 1;
					  }
					  
					  if($aug1 == '' || $aug1 == NULL || $aug1 == 0){
						  $div_aug1 = 0;
					  } else {
						  $div_aug1 = 1;
					  }
					  
					  if($sep1 == '' || $sep1 == NULL || $sep1 == 0){
						  $div_sep1 = 0;
					  } else {
						  $div_sep1 = 1;
					  }
					  
					  if($oct1 == '' || $oct1 == NULL || $oct1 == 0){
						  $div_oct1 = 0;
					  } else {
						  $div_oct1 = 1;
					  }
					  
					  if($nov1 == '' || $nov1 == NULL || $nov1 == 0){
						  $div_nov1 = 0;
					  } else {
						  $div_nov1 = 1;
					  }
					  
					  if($dec1 == '' || $dec1 == NULL || $dec1 == 0){
						  $div_dec1 = 0;
					  } else {
						  $div_dec1 = 1;
					  }
					  
					  if($jan1 == '' || $jan1 == NULL || $jan1 == 0){
						  $div_jan1 = 0;
					  } else {
						  $div_jan1 = 1;
					  }
					  
					  if($feb1 == '' || $feb1 == NULL || $feb1 == 0){
						  $div_feb1 = 0;
					  } else {
						  $div_feb1 = 1;
					  }
					  
					  if($mar1 == '' || $mar1 == NULL || $mar1 == 0){
						  $div_mar1 = 0;
					  } else {
						  $div_mar1 = 1;
					  }
					  
                      $tot1     = $apr1+$may1+$jun1+$jul1+$aug1+$sep1+$oct1+$nov1+$dec1+$jan1+$feb1+$mar1;
					  $div_tot1 = $div_apr1+$div_may1+$div_jun1+$div_jul1+$div_aug1+$div_sep1+$div_oct1+$div_nov1+$div_dec1+$div_jan1+$div_feb1+$div_mar1;
                      $tot1_new = number_format($tot1,2);
                      $avg1     = $tot1/$div_tot1;
                      $avg1_new = number_format($avg1,2);
                      ?>
                      <tr>
                        <td>Receipt ( <?php echo $fin_yr1; ?> )</td>
                        <td><?php echo $apr1_new; ?></td>
                        <td><?php echo $may1_new; ?></td>
                        <td><?php echo $jun1_new; ?></td>
                        <td><?php echo $jul1_new; ?></td>
                        <td><?php echo $aug1_new; ?></td>
                        <td><?php echo $sep1_new; ?></td>
                        <td><?php echo $oct1_new; ?></td>
                        <td><?php echo $nov1_new; ?></td>
                        <td><?php echo $dec1_new; ?></td>
                        <td><?php echo $jan1_new; ?></td>
                        <td><?php echo $feb1_new; ?></td>
                        <td><?php echo $mar1_new; ?></td>
                        <td><?php echo $tot1_new; ?></td>
                        <td><?php echo $avg1_new; ?></td>                          
                      </tr>
                      <?php } ?>
                    </tbody>
               </table>
        </div>
    </div><br />

<?php
	$curr_date_year = date("Y-m-d");
	$curr_fin_year = substr($curr_date_year,2,2);
	$last_fin_year = $curr_fin_year - 1;
	$last_fin_yr = "( FY-".$last_fin_year."-".$curr_fin_year." )";
?>
    
    <div class="row">
        <div class="col-lg-6" style="font-weight:bold; text-transform:uppercase; font-size:15px;">LAST YEAR CONSUMPTION AND RECIPT</div>
        <div class="col-lg-6" style="font-weight:bold; text-transform:uppercase; font-size:15px;">SUPPLIER</div>
    </div><br>
    <div class="row">
        <div class="col-lg-6">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>Type</th>
                        <th>Qty</th>
                        <th>Value</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($lstyrcumnrec->result() as $row) { ?>
                      <tr>
                        <td>Consumption <?php echo $last_fin_yr; ?></td>
                        <td><?php echo number_format($row->ConsTotalQty,2); ?></td>
                        <td><?php echo number_format($row->ConsTotalVal,2); ?></td>                          
                      </tr>
                      <tr>
                        <td>Receipt <?php echo $last_fin_yr; ?></td>
                        <td><?php echo number_format($row->RecptTotalQty,2); ?></td>
                        <td><?php echo number_format($row->RecptTotalVal,2); ?></td>                          
                      </tr>
                      <?php } ?>
                    </tbody>
               </table>
        </div>
        <div class="col-lg-6">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>Supplier</th>
                        <th>Purchase Rate</th>
                        <th>Lead Time</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($supplier->result() as $row) { ?>
                      <tr>
                        <td><?php echo $row->SuppName; ?></td>
                        <td><?php echo number_format($row->SuppRate,2); ?></td>
                        <td><?php echo $row->SuppLeadTime; ?></td>                       
                      </tr>
                      <?php } ?>
                    </tbody>
               </table>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">USED IN BOM DETAILS</div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr>
                        <th>Product Structure No</th>
                        <th>Output Item</th>
                        <th>Item Description</th>
                        <th>Required Quantity</th>
                        <th>Warehouse</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($bomdetail->result() as $row) { ?>
                      <tr>
                        <td><?php echo $row->PSNo; ?></td>
                        <td><?php echo $row->FgCode; ?></td>
                        <td><?php 
                        
                        $FgCodeDesc = $row->FgCodeDesc;
                        $fgdec = explode(':',$FgCodeDesc);
                        if ($fgdec[0] == 'LOC'){
                        $dec = $fgdec[1]; }
                        else {
                        $dec = $fgdec[0]; }
                        
                        
                        echo $dec; ?></td>
                        <td><?php echo number_format($row->BomItemQty,2); ?></td>
                        <td><?php echo $row->BomWhCode; ?></td>                         
                      </tr>
                      <?php } ?>
                    </tbody>
               </table>
        </div>
    </div><br />
    <!-- PICS ENTRY DETAILS-->
    <div class="row">
        <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">PICS ENRTY DETAILS</div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">	
                <table class="table table-bordered" id="dataTable" border="1">
                    <thead>
                      <tr bgcolor="#ADDFFF">
						<td><b>SNO.</b></td>
                        <td><b>TRAN NUMBER</b></td>
                        <td><b>ITEM CODE</b></td>
                        <td><b>ITEM DESC</b></td>
                        <td><b>CATEGORY</b></td>
                        <td><b>UOM</b></td>
                        <td><b>QUANTITY AVAILABLE IN STORE</b></td>
                        <td><b>ERP STOCK</b></td>
                        <td><b>LOCATION</b></td>
                        <td><b>CREATED BY</b></td>
                        <td><b>CREATE DATE</b></td> 
					</tr>
                    </thead>
                    <tbody>
                    <?php
					$sql_pics_det = "select * from TIPLDB..pics_entry where item_code = '$item_code' order by sno asc";
					
					$qry_pics_det = $this->db->query($sql_pics_det);
					
					$sno = 0;
					foreach($qry_pics_det->result() as $row){	
						$sno++;
						$tran_no       = $row->tran_no;
						$item_code     = $row->item_code;
						$item_desc     = $row->item_desc;
						$available_qty = $row->available_qty;
						$erp_stock     = $row->erp_stock;
						$location      = $row->location;
						$created_by    = $row->created_by;
						$created_date  = $row->created_date;
						$modified_by   = $row->modified_by;
						$modified_date = $row->modified_date;
						$category_desc = $row->category_desc;
						$uom 		   = $row->uom;
					?>
                    <tr>
						<td><?php echo $sno; ?></td>
						<td><?php echo $tran_no; ?></td>
						<td><?php echo $item_code; ?></td>
						<td><?php echo $item_desc; ?></td>
						<td><?php echo $category_desc; ?></td>
						<td><?php echo $uom; ?></td>
						<td><?php echo number_format($available_qty,2,".",""); ?></td>
						<td><?php echo number_format($erp_stock,2,".",""); ?></td>
						<td><?php echo $location; ?></td>
						<td><?php echo $created_by; ?></td>
						<td><?php echo substr($created_date,0,11); ?></td>
					</tr>
                    <?php } ?>
                    </tbody>
               </table>
        </div>
    </div><br />
    <div class="row">
    <div class="col-lg-12" style="font-weight:bold; text-transform:uppercase; font-size:15px;">CUT PIECE DETAILS</div>
</div><br />
<div class="row">
    <div class="col-lg-12"> 
            <table class="table table-bordered" id="dataTable" border="1">
                <thead>
                  <tr bgcolor="#ADDFFF">
                    <td><b>SNO</b></td>
                    <td><b>ITEM CODE</b></td>
                    <td><b>WAREHOUSE</b></td>
                    <td><b>QUANTITY AVAILABLE IN STORE</b></td>
                    <td><b>CREATED BY</b></td>
                    <td><b>CREATE DATE</b></td> 
                    <td><b>MODIFIED DATE</b></td> 
                    <td><b>MODIFIED BY</b></td> 
                </tr>
                </thead>
                <tbody>
                <?php
                //$sql_pics_det = "select * from TIPLDB..pics_entry where item_code = '$item_code' order by sno asc";
                $sql_cut_peice_det = "select *,convert(date,create_dt) as createdt, convert(date,modified_dt) as modified
                        from TIPLDB..stock_cut_piece 
                        where item_code = '$item_code' 
                        order by ID asc";
                
                $qry_cut_peice_det = $this->db->query($sql_cut_peice_det);
                
                $sno = 0;
                foreach($qry_cut_peice_det->result() as $row){   
                    $sno++;
                    $item_code  = $row->item_code;
                    $wh         = $row->wh;
                    $live_stock = $row->live_stock;
                    $created_by = $row->created_by;
                    $modified_by= $row->modified_by;
                    $createdt   = $row->createdt;
                    $modified   = $row->modified;
                ?>
                <tr>
                    <td><?php echo $sno; ?></td>
                    <td><?php echo $item_code; ?></td>
                    <td><?php echo $wh; ?></td>
                    <td><?php echo $live_stock; ?></td>
                    <td><?php echo $created_by; ?></td>
                    <td><?php echo $createdt; ?></td>
                    <td><?php echo $modified; ?></td>
                    <td><?php echo $modified_by; ?></td>
                </tr>
                <?php } ?>
                </tbody>
           </table>
    </div>
</div><br />
  </section>
</section>

<?php include'footer.php' ?>
