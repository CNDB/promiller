<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=export.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>

<table class="table table-bordered" id="all_pr" border="1">
  <tr style="background-color:#CCC">
    <td>S.NO.</td>
    <td>PR NO.</td>
    <td>ITEM CODE</td>
    <td>ITEM DESC.</td>
    <td>CATEGORY</td>
    <td>PROJECT NAME</td>
    <td>PR QTY</td>
    <td>COSTING</td>
    <td>PR DATE</td>
    <td>NEED DATE</td>
    <td>PR AGE</td>
    <td>LIVE CREATED BY</td>
  </tr>
  <?php 
    $shown=1;
    foreach($pr_po_report->result() as $row)
        {  
            $prno = $row->preqm_prno; 
            $prqit_balqty = $row->prqit_balqty;
            $pr_auth_qty = $row->prqit_authqty;
            $pr_itemcode = $row->prqit_itemcode;
            //$loi_itemdesc1 = $row->loi_itemdesc;
            $loi_itemdesc_length = strlen($loi_itemdesc1);
            $loi_itemdesc = $row->loi_itemdesc;
            $pr_createdby = $row->preqm_createdby;
            $pr_lastmodifiedby = $row->preqm_lastmodifiedby;
            $pr_createdDate = $row->pr_createdDate;
            $pr_needDate = $row->pr_needDate;
            $prqit_puom = $row->prqit_puom;
            $prqit_reqdqty = $row->prqit_reqdqty;
            $pr_age = $row->pr_age;
            $pr_create_date = $row->pr_createdDate;
            $pr_quantity = $row->prqit_reqdqty;
            
            $sql = "SELECT category, need_date, created_by, project_name, costing from TIPLDB..pr_submit_table where pr_num = '$prno'";
        
            $query = $this->db->query($sql)->row();
            $category = $query->category;
            $need_date = $query->need_date;
            if($need_date != ''){
                $need_date = date("d-m-Y", strtotime($need_date));
            }
            $project_name = $query->project_name;
            $created_by = $query->created_by;
            $costing= $query->costing;
            //$need_date1 = date("d-m-Y", strtotime($need_date));
            
            $max_no='';
            $sql1 = "SELECT max(pomas_poamendmentno) as max1  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
                      WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono";
            $query1 =$this->db->query($sql1)->row();
            
            $max_no = $query1->max1;
            
            $query2 = "SELECT sum(a.poprq_pocovqty) as poprq_pocovqty
            FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b,
            SCMDB..po_poitm_item_detail c
            WHERE a.poprq_prno = '$prno' and a.poprq_pono = b.pomas_pono
            and b.pomas_poamendmentno ='$max_no' and b.pomas_pono = c.poitm_pono
            and c.poitm_polineno = a.poprq_polineno
            and c.poitm_poamendmentno = a.poprq_poamendmentno
            and c.poitm_poamendmentno = b.pomas_poamendmentno
            and c.poitm_itemcode = '$pr_itemcode' and b.pomas_podocstatus not in('DE','CA','OP')
            group by c.poitm_itemcode";
             
            $sql2 =$this->db->query($query2)->row();
            
            $po_itm_qty = $sql2->poprq_pocovqty;
            
            if($po_itm_qty == ''){
                
                $po_itm_qty = 0;
                
            }
            
            $query3 = "SELECT pomas_podocstatus,pomas_pono  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
                     WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono
                     and b.pomas_poamendmentno = '$max_no'";
                     
            $query3 =$this->db->query($sql3)->row();
            
            
            $po_status = $query3->pomas_podocstatus;
            $po_no = $query3->pomas_pono;
            
            //echo "po--".$po_itm_qty."--pr".$pr_auth_qty."--".$prno."<br/>";
            if($po_itm_qty < $pr_auth_qty){
            
            
  ?>
  <tr>
    <td><?php echo $shown; ?></td>
    <td><a href="<?php echo base_url(); ?>index.php/createpoc/view_pr_auth_pur/<?php echo $prno; ?>" target="_blank">
    <?php echo $prno; ?></a></td><!--PR Number-->
    <td><?php echo $pr_itemcode; ?></td><!--Item Code-->
    <td><?php echo $loi_itemdesc; ?></td>
    <td><?php echo $category; ?></td><!--Category-->
    <td><?php echo $project_name; ?></td><!--Project Name-->
    <td><?php echo number_format($pr_quantity,2); ?></td><!--PR Quantity-->
    <td><?php echo number_format($costing,2); ?></td><!--Costing-->
    <td><?php echo date("d-m-Y", strtotime($pr_create_date)); ?></td><!--pr_create_date-->
    <td><?php echo $need_date; ?></td><!--Need Date-->
    <td><?php echo $pr_age;  ?></td><!--Age (In Days)-->
    <td><?php echo $created_by; ?></td><!-- Live Created By -->
  </tr>
  <?php $shown++; } } ?>
</table>

</body>
</html>
