<?php 
	include'header.php'; 
	$user_name = $_SESSION['username'];
?>

<section id="main-content">
  <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                  <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">SPECIAL ITEM REQUEST</h4>
            </div>
        </div><br />
        
        <form action="<?php echo base_url(); ?>index.php/mrp_reportc/mrp_spcl_item_entry" method="post" id="myform" onSubmit="return reqd()">
        
        <div class="row" style="font-size:14px;">   
            <div class="col-lg-2">
                <b>ITEM CODE</b><br />
                <input type="text" id="item_code" name="item_code" class='form-control' onChange="item_details(this.value)">
            </div>
            <div id="item_details_div">
                <div class="col-lg-2">
                    <b>ITEM DESCRIPTION</b><br />
                    <input type="text" name="item_desc" id="item_desc" value="<?php echo $item_desc; ?>" class="form-control" />
                </div>
                <div class="col-lg-2">
                    <b>CATEGORY</b><br />
                     <input type="text" name="live_category" id="live_category" value="" class="form-control" />
                </div>
                <div class="col-lg-2">
                    <b>STOCK UOM</b><br />
                    <input type="text" name="StockUom" id="StockUom" value="" class="form-control" />
                </div>
                <div class="col-lg-2">
                    <b>PURCHASE UOM</b><br />
                    <input type="text" name="PurchaseUom" id="PurchaseUom" value="" class="form-control" />
                </div>
            </div>
            <div class="col-lg-2">
            	<b>USER NAME</b><br />
                <input type="text" name="user_name" id="user_name" value="<?php echo $user_name; ?>" class="form-control" readonly />
            </div>
        </div><br /><br />
        
        <div class="row" style="font-size:14px;">   
            <div class="col-lg-2">
                <b>ITEM USAGE TYPE</b><br />
                <select id="spcl_item_usage_type" name="spcl_item_usage_type" class="form-control" onChange="atac_div(this.value);">
                	<option value="">--Select--</option>
                    <option value="Project Pre Order">Project Pre Order</option>
                    <option value="Special">Special</option>
                </select>
            </div>
            <div class="col-lg-2"> 
            	<div id="atac_block" style="display:none;">
            	<b>SELECT ATAC </b><br> 
                <select id="atac_no" name="atac_no" class="form-control" onChange="atac_details(this.value);" style="width:100%">
                	<option value="">--Select--</option>
                    <?php
						
						$sql_atac ="select atac_no from tipldb..master_atac where so_no is null 
and status not in ('Deleted from crm','Approved by Order booking Dept.','Fresh') 
and sent_to_cc_review_date is not null
union
select atac_no from tipldb..master_atac where so_no is not null and status in ('Approved by Order booking Dept.') 
and so_no not in (SELECT woh_so_no from scmdb..pmd_twoh_wo_headr)
order by atac_no";
						$query_atac = $this->db->query($sql_atac);
						
						foreach($query_atac->result() as $row){
							echo $atac_no = $row->atac_no;
        			?> 
                    <option value="<?php echo $atac_no; ?>"><?php echo $atac_no; ?></option>
                    <?php
						}
					?>
                </select>
                </div> 
            </div>
            <div class="col-lg-8">   
            </div>
        </div><br /><br />
        
        <div id="atac_details_div" style="display:block;"></div>
        
        <div class="row" style="font-size:14px;">
            <div class="col-lg-2"><b>ITEM REQ. QUANTITY</b><b style="color:red">&nbsp;*</b></div>
            <div class="col-lg-2">
            <input type="text" name="item_req_qty" id="item_req_qty" class="form-control" value="" onkeypress='return validateQty(event);'/>
            </div>
            <div class="col-lg-2"><b>REMARKS</b></div>
            <div class="col-lg-4"><input type="text" name="remarks" id="remarks" class="form-control" value=""/></div>
            <div class="col-lg-2"></div>  
        </div><br /><br />
        
        <div class="row" style="font-size:14px;">
            <div class="col-lg-5"></div>
            <div class="col-lg-2"><input type="submit" name="submit_data" id="submit_data" class="form-control" value="SAVE" /></div>
            <div class="col-lg-5"></div>   
        </div><br /><br />
    </div>
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->

<?php include('footer.php'); ?>

<script>

// Allow To Insert Only Numbers
function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;

	if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39  ) {
		return true;
	}
	else if ( key != 46 && key > 31 && (key < 48 || key > 57) ) {
		return false;
	}
	else return true;
}

//Validations 

function reqd(){
	
	var item_code = document.getElementById("item_code").value;
	var spcl_item_usage_type = document.getElementById("spcl_item_usage_type").value;
	var item_req_qty = document.getElementById("item_req_qty").value;
	var remarks = document.getElementById("remarks").value;
	var atac_no = document.getElementById("atac_no").value;
	//alert(item_code);
	
	if(item_code == ""){
		alert("Please Select Item Code");
		document.getElementById("item_code").focus;
		return false;	
	}
	
	if(spcl_item_usage_type == ""){
		alert("Please Select Item Usage Type.");
		document.getElementById("spcl_item_usage_type").focus;
		return false;	
	}
	
	if(spcl_item_usage_type == "Project Pre Order" && atac_no == ""){
		alert("Please Select ATAC No.");
		document.getElementById("atac_no").focus;
		return false;	
	}
	
	if(item_req_qty == ""){
		alert("Please Enter Item Required Quantity");
		document.getElementById("item_req_qty").focus;
		return false;	
	}
	
	if(remarks == ""){
		alert("Please Enter Remarks");
		document.getElementById("remarks").focus;
		return false;	
	}
	
}

// In your Javascript (external .js resource or <script> tag)
$(document).ready(function(){
	$('#atac_no').select2();
});

function atac_div(spcl_item_usage_type){
	
	if(spcl_item_usage_type == 'Project Pre Order'){
		$('#atac_block').show();
		$('#atac_details_div').show();
		
		$('#atac_no').val('').change();
		$('#atac_ld_date').val('');
		$('#atac_need_date').val('');
		$('#payment_term').val('');
		$('#customer_payment_terms').val('');
		$('#project_name').val('');
		$('#customer_name').val('');
		$('#pm_group').val('');
		$('#sono').val('');
	} else {
		$('#atac_block').hide();
		$('#atac_details_div').hide();
		
		$('#atac_no').val('').change();
		$('#atac_ld_date').val('');
		$('#atac_need_date').val('');
		$('#payment_term').val('');
		$('#customer_payment_terms').val('');
		$('#project_name').val('');
		$('#customer_name').val('');
		$('#pm_group').val('');
		$('#sono').val('');
	}
}

//Item Details Ajax
function atac_details(atac_no){
  
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
  
	xmlhttp.onreadystatechange=function()
	{
	
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('atac_details_div').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('atac_details_div').innerHTML="<strong>Waiting For Server...</strong>";
		}
	}
	
	var queryString="?atac_no="+atac_no;

	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/mrp_reportc/atac_details_mrp"+queryString, true);
	xmlhttp.send();
	
}

//Item Details
function item_details(item_code){
	//alert(item_code);
	
	//Ajax Function Call
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
  
	xmlhttp.onreadystatechange=function()
	{
	
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('item_details_div').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('item_details_div').innerHTML="<strong>Waiting For Server...</strong>";
		}
	}
	
	var queryString="?item_code="+encodeURIComponent(item_code);

	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/mrp_reportc/mrp_item_details_ajax"+queryString, true);
	xmlhttp.send();
}

</script>

<script type="text/javascript">
$(function() {
    
    //autocomplete
    $("#item_code").autocomplete({
		select: function(event, ui) {
			"use strict";
			alert("Select Item Code");
        },
        source: "<?php echo base_url(); ?>index.php/mrp_reportc/search",
        minLength: 1,
		change: function(event, ui) {
			/*if (ui.item) {
				alert("ui.item.value: " + ui.item.value);
			} else {
				alert("ui.item.value is null");
			}*/
        console.log("this.value: " + this.value);
    	}
    });  
	
	$("#item_code").trigger('change');            

});
</script>