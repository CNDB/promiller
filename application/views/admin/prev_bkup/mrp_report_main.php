<?php 
include_once('header.php');
// include_once('cut_peice_match.php'); 

//$sql_updt = "update tipldb..pendingissuetbl set cp_status = 'Cut Peice Not Matched' where cp_status is NULL";
//$qry_updt=$this->db->query($sql_updt);
?>
 
<?php $this->load->helper('mrp_helper'); ?>
<!--main content start-->
<!--********* ITEM INFORMATION *********-->

<?php 
//Dates 
$sql="select 
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()), 0) as cmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE()), -1) as cmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0) as lfmsd, DATEADD(MONTH, DATEDIFF(MONTH, -1, GETDATE())-1, -1) as lfmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-2, 0) as lsmsd, DATEADD(MONTH, DATEDIFF(MONTH, -2, GETDATE())-2, -1) as lsmed,
DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-3, 0) as ltmsd, DATEADD(MONTH, DATEDIFF(MONTH, -3, GETDATE())-3, -1) as ltmed";

$qry = $this->db->query($sql);

foreach($qry->result() as $row){
	$cmsd  = substr($row->cmsd,0,11);
	$cmed  = substr($row->cmed,0,11);
	$lfmsd = substr($row->lfmsd,0,11);
	$lfmed = substr($row->lfmed,0,11);
	$lsmsd = substr($row->lsmsd,0,11);
	$lsmed = substr($row->lsmed,0,11);
	$ltmsd = substr($row->ltmsd,0,11);
	$ltmed = substr($row->ltmed,0,11);
	
}

$sub_url = "index.php/mrp_reportc/mrp_view_avg_itm?";

?>

<section id="main-content">
  <section class="wrapper">
  
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">MRP REPORT</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-2"></div>
        <div class="col-lg-8">
        	  <h3 align="center">MRP LOGIC</h3>
              <table class="table table-bordered" align="center">
              		<tr style="background-color:#CCC; font-weight:bold">
                    	<td>TYPE</td>
                        <td>FORMULA</td>
                    </tr>
                    <tr>
                    	<td>Required Quantity</td>
                        <td>(WIP Issue + MR Issue + Sub Contract Issue + SO Issue + Other Issue) + Reorder Level</td>
                    </tr>
                    <tr>
                    	<td>Total Receipt</td>
                        <td>WIP Receipt + PO Receipt + PR Receipt + GRCPT Receipt </td>
                    </tr>
                    <tr>
                    	<td>Available Quantity</td>
                        <td> Free Stock + Allocated Stock + Total Receipt + Other Stock</td>
                    </tr>
                    <tr>
                    	<td>Quantity to Order</td>
                        <td>Required Quantity - Available Quantity</td>
                    </tr>
                    <tr>
                    	<td colspan="2">If Quantity To Order is greater than 0 then item comes in MRP. </td>
                    </tr>
              </table>
        </div>
        <div class="col-lg-2">
        	<table class="table table-bordered" align="center">
            	<tr style="background-color:#CCC; font-weight:bold">
                	<td>COLOR CODE</td>
                    <td>COLOR CODE DESCRIPTION</td>
                </tr>
                <tr>
                	<td style="background-color:#0F9"></td>
                    <td>Stock Available But Not in Required Length</td>
                </tr>
                <tr>
                	<td style="background-color:#F90"></td>
                    <td>Stock Not Available</td>
                </tr>
            </table>
        </div>
    </div><br />
     
    <!-- Filter According To Category ----->
    <div class="row">
    	<div class="col-lg-3"></div>
    	<div class="col-lg-6">
        	<form method="post" onSubmit="">
                <table cellspacing="8px" cellpadding="4" cellspacing="4" align="center" border="0" width="100%">
                    <tr>
                        <td><b style="font-size:14px; font-weight:bolder">SELECT CATEGORY :</b></td>
                        <td> 
                            <select name="rpt_ty" id="rpt_ty" class="form-control">		
                                <option value="">Select</option>
                                <option value="All">All</option>
                                <option value="Avazonic">Avazonic</option>
                                <option value="Capital Goods">Capital Goods</option>
                                <option value="Instrumentation">Instrumentation</option>
                                <option value="Non Production Consumables">Non Production Consumables</option>
                                <option value="Packing">Packing</option>
                                <option value="Production Consumables">Production Consumables</option>
                                <option value="Security">Security</option>
                                <option value="Sensor">Sensor</option>
                                <option value="Tools">Tools</option>
                                <option value="IT">IT</option>
                                <option value="No Category">No Category</option>
                            </select>
                         </td>
                         <td>
                            <input type="hidden" name="user_name" id="user_name" value="<?php echo $user_name; ?>" class="form-control">
                            <input type="button" value="Submit" onClick="category_items()" class="form-control" style="margin-left:10px">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div class="col-lg-3"></div>
    </div><br>
    
     <div class="row">
    	<div class="col-lg-3"></div>
        <div class="col-lg-6" style="text-align:center">
        	  <b style="color:#F00">Note: Exception For Sensor Category (Reorder Items Handled by Mr. Abhishek Lal)</b>
        </div>
        <div class="col-lg-3"></div>
     </div><br />
    
    <!-- Summary Of MRP -->
    <div class="row">
    	<div class="col-lg-12">
        <table align="center" cellpadding="1" cellspacing="1" class="table table-bordered">
            <tr style="background-color:#CCC">
                <td colspan="3"><b>Last 3 Months Average<br>(In Days)</b></td>
                <td rowspan="2"><b>Current Month Average<br>(In Days)</b></td>
                <td rowspan="2"><b>Category Owner</b></td>
                <td rowspan="2"><b>ERP Category Code</b></td>
                <td rowspan="2"><b>Live Category</b></td>
                <td rowspan="2"><b>Age <= 1 Day (A)</b></td>
                <td rowspan="2"><b>Age > 1 Day (B)</b></td>
                
                <!--- Display Project & Reorder Items Separately -->
                <td rowspan="2"><b>Project</b></td>
                <td rowspan="2"><b>Reorder</b></td>
                <!--- Display Project & Reorder Items Separately -->
                
                <!--- Pics & Length Mismatched Items --->
                <td rowspan="2"><b>Required & PICS Mismatched (Length Items)</b></td>
                <td rowspan="2"><b>Required & Length Mismatched (Length Items)</b></td>
                <!--- Pics & Length Mismatched Items --->
                
                <td rowspan="2"><b>Disapproved PR'S</b></td>
                <td rowspan="2"><b>Total Items (C=A+B)</b></td>
                <td rowspan="2"><b>Open Items Average (C)<br>(In Days)</b></td>
           </tr>
           <tr style="background-color:#CCC">
                <td><b><?php month_year($ltmsd); ?></b></td>
                <td><b><?php month_year($lsmsd); ?></b></td>
                <td><b><?php month_year($lfmsd); ?></b></td>
           </tr>
           
           <?php 
		   	$sub_url="index.php/mrp_reportc/mrp_view_avg_itm?fun=avg_monthwise_det&";
			$sub_url1="index.php/mrp_reportc/mrp_view_avg_itm?fun=avg_open_items_det&";  
		   ?>
           
           <?php
		    $sql1="select * from tipldb..mrp_category_owner_master order by category";
		
			$qry1 = $this->db->query($sql1);
			
			foreach($qry1->result() as $row){
				$erp_cat = $row->erp_cat;
				$erp_cat_disp = $row->erp_cat_disp;
				$live_cat = $row->category;
				$category_owner = $row->category_owner;
				$live_cat_disp = $row->category_disp;
				
		   ?>
           
           <tr>
                <td>
                <a href="<?php echo base_url(); ?><?php echo $sub_url; ?>erp_cat=<?php echo $erp_cat; ?>&cat=<?php echo $live_cat; ?>&fd=<? echo $ltmsd; ?>&td=<? echo $ltmed; ?>" target="_blank">
					<?php echo avg_monthwise($erp_cat,$live_cat,$ltmsd,$ltmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?><?php echo $sub_url; ?>erp_cat=<?php echo $erp_cat; ?>&cat=<?php echo $live_cat; ?>&fd=<? echo $lsmsd; ?>&td=<? echo $lsmed; ?>" target="_blank">
					<?php echo avg_monthwise($erp_cat,$live_cat,$lsmsd,$lsmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?><?php echo $sub_url; ?>erp_cat=<?php echo $erp_cat; ?>&cat=<?php echo $live_cat; ?>&fd=<? echo $lfmsd; ?>&td=<? echo $lfmed; ?>" target="_blank">
					<?php echo avg_monthwise($erp_cat,$live_cat,$lfmsd,$lfmed); ?>
                </a>
                </td>
                <td>
                <a href="<?php echo base_url(); ?><?php echo $sub_url; ?>erp_cat=<?php echo $erp_cat; ?>&cat=<?php echo $live_cat; ?>&fd=<? echo $cmsd; ?>&td=<? echo $cmed; ?>" target="_blank">
					<?php echo avg_monthwise($erp_cat,$live_cat,$cmsd,$cmed); ?>
                </a>
                </td>
                <td><b><?php echo $category_owner; ?></b></td>
                <td><b><?php echo $erp_cat_disp; ?></b></td>
                <td><b><?php echo $live_cat_disp; ?></b></td>
                <td style="background-color:#F90"><?php echo category_age_less($erp_cat,$live_cat); ?></td>
                <td style="background-color:#F90"><?php echo category_age_greater($erp_cat,$live_cat); ?></td>
                
                <!--- Display Project & Reorder Items Separately -->
                <td><?php echo category_count_project($erp_cat,$live_cat); ?></td>
                <td><?php echo category_count_reorder($erp_cat,$live_cat); ?></td>
                <!--- Display Project & Reorder Items Separately -->
                
                <!--- Pics & Length Mismatched Items -->
                <td style="background-color:#0F9">
                <a href="<?= base_url(); ?>index.php/mrp_reportc/itm_stk_na?erp_cat=<?=$erp_cat; ?>&live_cat=<?=$live_cat; ?>&cp_stat=Item Pics Mismatched" target="_blank">
                    <?php echo cp_pics_na($erp_cat,$live_cat,'Item Pics Mismatched'); ?>
                </a>
                </td>
                <td style="background-color:#0F9">
                <a href="<?= base_url(); ?>index.php/mrp_reportc/itm_stk_na?erp_cat=<?=$erp_cat; ?>&live_cat=<?=$live_cat; ?>&cp_stat=Cut Peice Not Matched" target="_blank">
					<?php echo cp_pics_na($erp_cat,$live_cat,'Cut Peice Not Matched'); ?>
                </a>
                </td>
                <!--- Pics & Length Mismatched Items -->
                
                <td>
                <a href="<?= base_url(); ?>index.php/mrp_reportc/mrp_disapp_pr?erp_cat=<?=$erp_cat; ?>&live_cat=<?=$live_cat; ?>" target="_blank">
					<?php echo disapp_pr($erp_cat,$live_cat); ?>
                </a>
                </td>
                
                <td style="background-color:#F90">
                    <a href="<?php echo base_url(); ?><?php echo $sub_url1; ?>erp_cat=<?php echo $erp_cat; ?>&cat=<?php echo $live_cat; ?>" target="_blank">
                        <?php echo category_count($erp_cat,$live_cat); ?>
                    </a>
                </td>
                <td style="background-color:#F90">
                    <a href="<?php echo base_url(); ?><?php echo $sub_url1; ?>erp_cat=<?php echo $erp_cat; ?>&cat=<?php echo $live_cat; ?>" target="_blank">
                        <?php echo avg_open_items($erp_cat,$live_cat); ?>
                    </a>
                </td>
            </tr>
           <?php } ?>
           <tr>
                <td colspan="6" style="background-color:#CCC"></td>
                <td><b>Total</b></td>
                <td style="background-color:#F90"><?php echo category_age_less("","All"); ?></td>
                <td style="background-color:#F90"><?php echo category_age_greater("","All"); ?></td>
                <!--- Display Project & Reorder Items Separately -->
                <td><?php echo category_count_project("","All"); ?></td>
                <td><?php echo category_count_reorder("","All"); ?></td>
                <!--- Display Project & Reorder Items Separately -->
                <!--- Pics & Length Mismatched Items -->
                <td style="background-color:#0F9">
                <a href="<?= base_url(); ?>index.php/mrp_reportc/itm_stk_na?erp_cat=All&live_cat=All&cp_stat=Item Pics Mismatched" target="_blank">
                    <?php echo cp_pics_na("","All","Item Pics Mismatched"); ?>
                </a>
                </td>
                <td style="background-color:#0F9">
                <a href="<?= base_url(); ?>index.php/mrp_reportc/itm_stk_na?erp_cat=All&live_cat=All&cp_stat=Cut Peice Not Matched" target="_blank">
                    <?php echo cp_pics_na("","All","Cut Peice Not Matched"); ?>
                </a>
                </td>
                <!--- Pics & Length Mismatched Items -->
                
                <td>
                <a href="<?= base_url(); ?>index.php/mrp_reportc/mrp_disapp_pr?erp_cat=All&live_cat=All" target="_blank">
					<?php echo disapp_pr("","All"); ?>
                </a>
                </td>
                
                <td style="background-color:#F90"><?php echo category_count("","All"); ?></td>
                <td style="background-color:#F90"></td>
            </tr>
        </table>
        </div>
    </div>
    <!--Ajax Div-->
    <div class="row">
    	<div id="category_items" class="col-lg-12"></div>
    </div>
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->

<script type="text/javascript" language="javascript">

function category_items(){
	
  var rpt_ty = document.getElementById("rpt_ty").value;
  
  if(rpt_ty == ''){
	alert("Select Category First..");
	document.getElementById("rpt_ty").focus;
	return false;
  }
  
$("#category_items").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
   
  if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
	xmlhttp=new XMLHttpRequest();
  }else{// code for IE6, IE5
	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	  
 xmlhttp.onreadystatechange=function(){
	
	if(xmlhttp.readyState==4 && xmlhttp.status==200){
		document.getElementById('category_items').innerHTML=xmlhttp.responseText;
	}
 }

var queryString="?category="+encodeURIComponent(rpt_ty);

xmlhttp.open("GET","<?php echo base_url(); ?>index.php/mrp_reportc/mrp_report_ajax" + queryString,true);
xmlhttp.send();

}

</script>
      
<?php include('footer.php'); ?>