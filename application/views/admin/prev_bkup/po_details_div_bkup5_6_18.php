<!--********* PO INFORMATION *********-->
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>PO DETAILS</h3>
    </div>
</div>

<?php $total_order_value_with_taxes = 0;
 foreach ($view_po->result() as $row){ 
   $po_num                       = $row->po_num;
   $pr_num                       = $row->po_ipr_no; 
   $supp_name                    = $row->po_supp_name;
   $supp_addr                    = $row->po_supp_add;
   $supp_email                   = $row->po_supp_email;
   $supp_phone                   = $row->supp_phone;
   $contact_person               = $row->contact_person;
   $supp_addr                    = $row->po_supp_add;
   $order_value                  = $row->pomas_pobasicvalue;
   $cst_tax                      = $row->pomas_tcdtotalrate;
   $total_tax                    = $row->pomas_tcal_total_amount;
   $total_order_value_with_taxes = ($order_value + $cst_tax + $total_tax);
   $po_age                       = $row->diff;
   $payment_term                 = $row->payterm;
   $freight_place                = $row->freight_place;
   $carrier_name                 = $row->carrier_name;
   $deli_type                    = $row->po_deli_type;
   $lead_time                    = $row->po_lead_time;
   $manufact_clearnace           = $row->po_manfact_clernce;
   $dispatch_instruction         = $row->po_dispatch_inst;
   $freight                      = $row->freight;
   $insurance_liablity           = $row->insurance_liablity;
   $transport_mode               = $row->trans_mode;
   $freight_type                 = $row->freight_type;
   $approx_freight               = $row->approx_freight;
   $po_spcl_inst_frm_supp        = $row->po_spcl_inst_frm_supp;
   $ld_applicable                = $row->ld_applicable;
   $currency                     = $row->currency;
   $po_created_in_live           = $row->po_approvedby_lvl0;
   $po_created_in_live_date      = $row->po_approveddate_lvl0;
   $po_created_in_erp            = $row->pomas_createdby;
   $po_created_in_erp_date       = $row->pomas_createddate;
   $lvl1_app_req                 = $row->lvl1_app_req;
   $lvl2_app_req                 = $row->lvl2_app_req;
   
   echo "<input type='hidden' id='lvl1_app_req' name='lvl1_app_req' value='$lvl1_app_req'>";
   echo "<input type='hidden' id='lvl2_app_req' name='lvl2_app_req' value='$lvl2_app_req'>";
   
   //Service PO Feilds Starts
   
   $spo_type = $row->spo_type;
   $spo_category = $row->po_category;
   $spo_usage_type = $row->spo_usage_type;
   $spo_atac_no = $row->spo_atac_no;
   $spo_atac_ld_date = $row->spo_atac_ld_date;
   $spo_atac_need_date = $row->spo_atac_need_date;
   $spo_atac_payment_terms = $row->spo_atac_payment_terms;
   $spo_atac_customer_payment_terms = $row->spo_atac_customer_payment_terms;
   $spo_project_name = $row->spo_project_name;
   $spo_customer_name = $row->spo_customer_name;
   $spo_pm_group = $row->spo_pm_group;
   $spo_so_no = $row->spo_so_no;
   $spo_why_spcl_rmks = $row->spo_why_spcl_rmks;
   
   //Advance Feilds
   $advance_amount = $row->advance_amount;
   $advance_remarks = $row->advance_remarks;
   $advance_entered_by = $row->advance_entered_by;
   $advance_entry_date = $row->advance_entry_date;
   
   
   
   //Service PO Feilds Ends
    if($freight == "FOR")
	{
		$freight1 = $freight.",&nbsp;Ajmer";
	}
	else if($freight == "FORD")
	{
		$freight1 = $freight.",&nbsp;TIPL Ajmer";
	}
	else
	{
		$freight1 = $freight."&nbsp;";
	}
	
	$sql1 ="select *, datediff(DAY, preqm_prdate,getdate()) as diff1 from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'";
	$query1 = $this->db->query($sql1);
	
    foreach ($query1->result() as $row) {
	  $pr_age = $row->diff1;
    }					
?>
<div class="row">
    <div class="col-lg-2">
        <b>Supplier Name:</b><br />
        <?php echo $supp_name; ?>   	
    </div>
    <div class="col-lg-2">
        <b>Supplier Address:</b><br />
        <?php echo $supp_addr; ?>
    </div>
    <div class="col-lg-2">
        <b>Supplier Email:</b><br />
        <?php echo $supp_email; ?>
    </div>
    <div class="col-lg-2">
        <b>Supplier Phone:</b><br />
        <?php echo $supp_phone; ?>
    </div>
    <div class="col-lg-2">
        <b>Contact Person:</b><br />
        <?php echo $contact_person;?>
    </div>
	<div class="col-lg-2">
        <b>Order Value:</b><br />
		<?php echo number_format($total_order_value_with_taxes,2); ?>
    </div>
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
        <b>PO Age:</b><br />
        <?php echo $po_age."&nbsp; Days"; ?>
    </div>
	<div class="col-lg-2">
        <b>PR Age:</b><br />
        <?php echo $pr_age."&nbsp; Days"; ?>
    </div>
    <div class="col-lg-2">
        <b>Payment Terms:</b><br />
        <?php echo $payment_term; ?>   	
    </div>
	<div class="col-lg-2">
        <b>Freight Terms:</b><br />
        <?php echo $freight1; ?>
    </div>
    <div class="col-lg-2">
        <b>Freight Place:</b><br />
        <?php echo $freight_place; ?>
    </div>
	<div class="col-lg-2">
        <b>Carrier Name:</b><br />
        <?php echo $carrier_name; ?>
    </div> 
</div><br /><br />

<div class="row">
	<div class="col-lg-2">
        <b>Insurance Term:</b><br />
        <?php echo $insurance_liablity; ?>
    </div>
	<div class="col-lg-2">
        <b>Delivery Type:</b><br />
        <?php echo $deli_type; ?>
    </div>
    <div class="col-lg-2">
        <b>Expected Material Recipt Date:</b><br />
        <?php echo $lead_time; ?>
    </div>
	<div class="col-lg-2">
        <b>Mode Of Transport:</b><br />
        <?php echo $transport_mode; ?>
    </div> 
    <div class="col-lg-2">
        <b>Freight Type:</b><br />
        <?php echo $freight_type; ?>
    </div>
	<div class="col-lg-2">
    	<b>Approximate Freight:</b><br />
        <?php echo $approx_freight; ?>
    </div>   
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
        <b>LD Applicable:</b><br />
        <?php echo $ld_applicable; ?>
    </div>
    <div class="col-lg-2">
        <b>PO Special Instructions For Supplier:</b><br />
        <?php echo $po_spcl_inst_frm_supp; ?>
    </div>
    <div class="col-lg-2">
    	<b>Currency :</b><br />
		<?php echo $currency; ?>
    </div>
    <div class="col-lg-2">
        <b>PO Created ERP By: </b><br />
        <?php echo $po_created_in_erp; ?>
    </div>
	<div class="col-lg-2">
    	<b>PO Created ERP Date:</b><br />
        <?php echo date("d-m-Y", strtotime($po_created_in_erp_date)); ?>
    </div>
    <div class="col-lg-2">
    	<b>PO Created Live By:</b><br />
        <?php echo strtoupper($po_created_in_live); ?>
        <input type="hidden" name="live_created_by" id="live_created_by" value="<?php echo $po_created_in_live; ?>" />
    </div>
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
    	<b>PO Created Live Date:</b><br />
        <?php echo date("d-m-Y", strtotime($po_created_in_live_date)); ?>
    </div>
</div><br /><br />
<!-- Service PO Details Starts --->
<?php 
	$po_first_three = substr($po_num,0,3); 
	if($po_first_three == 'SRV'){ 
?>
<div class="row">
    <div class="col-lg-12">
    	<h4>Service PO Live Filled Details</h4>
    </div>
</div><br /><br />

<div class="row">
    <div class="col-lg-2">
    	<b>Service PO Type</b><br />
        <?php echo $spo_type; ?>
    </div>
    <div class="col-lg-2">
    	<b>Service PO Category</b><br />
        <?php echo $spo_category; ?>
    </div>
    <div class="col-lg-2">
    	<b>Service PO Usage Type</b><br />
        <?php echo $spo_usage_type; ?>
    </div>
    <div class="col-lg-6"></div>	
</div><br /><br />

<?php if($spo_usage_type == 'Special'){ ?>

<div class="row">
    <div class="col-lg-2">
    	<b>Special Remarks</b><br />
        <?php echo $spo_why_spcl_rmks; ?>
    </div>
    <div class="col-lg-10"></div>
</div><br /><br />

<?php } else { ?>

<!-- ATAC Details Starts -->
<div class="row">
	<div class="col-lg-2">
    	<b>ATAC No.</b><br />
        <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $spo_atac_no; ?>">
			<?php echo $spo_atac_no; ?>
        </a>
    </div>
    <div class="col-lg-2">
    	<b>ATAC LD Date</b><br />
        <?php echo $spo_atac_ld_date; ?>
    </div>
    <div class="col-lg-2">
    	<b>ATAC Need Date</b><br />
        <?php echo $spo_atac_need_date; ?>
    </div>
    <div class="col-lg-2">
    	<b>ATAC Payment Terms</b><br />
        <?php echo $spo_atac_payment_terms; ?>
    </div>
    <div class="col-lg-2">
    	<b>Customer Payment Terms</b><br />
        <?php echo $spo_atac_customer_payment_terms; ?>
    </div>
    <div class="col-lg-2">
    	<b>Project Name</b><br />
        <?php echo $spo_project_name; ?>
    </div>
</div><br /><br />

<div class="row">
	<div class="col-lg-2">
    	<b>Customer Name</b><br />
        <?php echo $spo_customer_name; ?>
    </div>
    <div class="col-lg-2">
    	<b>PM Group</b><br />
        <?php echo $spo_pm_group; ?>
    </div>
    <div class="col-lg-2">
    	<b>SO Number</b><br />
        <?php echo $spo_so_no; ?>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>
</div><br /><br />

<?php } ?>

<!--- ATAC Details Ends --->

<!-- Payment Milestones Starts  --->
<div class="row">
	<div class="col-lg-12">
    	<h4>Payment Milestones</h4>
    </div>
</div><br />
<div class="row">
	<div class="col-lg-12">
    	<table class="table table-bordered" align="center" cellpadding="0" cellspacing="0" border="1">
        	<tr style="background-color:#CCC">
            	<td><b>SNO</b></td>
                <td><b>Description</b></td>
                <td><b>Target Date</b></td>
                <td><b>Percentage Of PO Value</b></td>
                <td><b>Amount</b></td>
                <td><b>Payment Term</b></td>
                <td><b>Payment Method</b></td>
                <td><b>PDC Days</b></td>
            </tr>
            <?php
				$sql_paymt_milestone = "select * from TIPLDB..spo_payment_milestones where po_num = '$po_num'  order by s_no asc";
				$qry_paymt_milestone = $this->db->query($sql_paymt_milestone);
				
				$sno = 0;
				foreach($qry_paymt_milestone->result() as $row){
					$sno++;
			?>
            <tr>
            	<td><?php echo $sno; ?></td>
            	<td><?php echo $desc = $row->description; ?></td>
                <td><?php echo $targt_date = $row->target_date; ?></td>
                <td><?php echo $per_po_val = $row->percentage_po_value; ?></td>
                <td><?php echo $amt = $row->amount; ?></td>
                <td><?php echo $paytrm = $row->payterm; ?></td>
                <td><?php echo $paymthd = $row->payment_method; ?></td>
                <td><?php echo $pdc_day = $row->pdc_days; ?></td>
            </tr>
            <?php
				}
			?>
        </table>
    </div>
</div><br /><br />

<?php } ?>
<!-- Payment Milestones Ends --->

<!--- Service PO Details Ends ---->
<?php break; } ?>

<!--********* ITEM INFORMATION *********-->
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>ITEM DETAILS</h3>
    </div>
</div>
<div class="row">
  <div class="col-lg-12" style=" overflow-x:auto;">
     <table class="table table-bordered" id="dataTable" border="1">
        <thead>
          <tr>
            <th>IPR No.</th> 
            <th>Item Code</th>                             
            <th>Item Desc.</th>
            <th>UOM</th>
            <th>PR Qty</th>
            <th>Costing</th>
            <th>Costing Currency</th>
            <th>Last Price</th> 
            <th>Current Price</th>
            <th>Charges</th>
            <th>Discount</th> 
            <th>Total Item Value</th>
            <th>PR Need Date</th>
            <th>PO Need Date</th>  
            <th>PR Type</th>
            <th>Why Special Remarks</th> 
            <th>SO No.</th> 
            <th>ATAC No.</th>
            <th>ATAC LD Date.</th>
            <th>ATAC Need Date.</th>
            <th>ATAC Payment Terms.</th>
            <th>PM Group</th> 
            <th>Category</th> 
            <th>Project Name</th> 
            <th>PR Remarks</th>  
            <th>Attached Cost Sheet</th>
            <th>Item Remarks for Supplier</th>
            <th>Manufacturing Clearnace</th>
            <th>Dispatch Instruction</th>                                                            
            <th>Last 1 Yr. Cons.</th>
            <th>Current Yr. Cons.</th>
            <th>Current Stock</th>
            <th>Total Incoming Stock</th>                                                           
            <th>Reservation Qty</th>                
            <th>Warehouse Code</th>
            <th>Conversion Factor With Stock UOM</th>
            <th>Supplier Lead Time(AVG)</th>
            <th>Last Supplier</th>
            <th>Last Supplier Rate</th>
            <th>Last Supplier Lead Time</th>
            <th>Reorder Level</th>
            <th>Reorder Qty</th>
            <th>Last GRCPT(Freeze And Move Date)</th>
            
            <th>Item Remarks</th>
            <?php if($po_first_three == 'SRV'){ ?>
            <th>SPO ITEM COST SHEET</th> 
            <?php } ?>
          </tr>
        </thead>
        <tbody>
		 <?php
           foreach ($view_po->result() as $row)  { 
            $po_line_no  = $row->po_line_no;
            $po_s_no  = $row->po_s_no;
            $item_code = $row->po_item_code;
            
            $item_code1 = urlencode($item_code);
            
            if(strpos($item_code1, '%2F') !== false)
            {
                $item_code2 = str_replace("%2F","chandra",$item_code1);
            }
            else 
            {
                $item_code2 = $item_code1;
            }
    
            $po_ipr_no = $row->po_ipr_no;
            $po_num = $row->po_num;
            $item_desc = $row->po_itm_desc;
            $wh_code = $row->po_wh_code;
            $uom  = $row->po_uom;
            $current_price = $row->current_price;
            $item_value = $row->total_item_value;
            $item_remarks = $row->item_remarks;
            $for_stk_qty = $row->for_stk_quantity;
            $po_drwg_refno = $row->po_drwg_refno;
            $totalcost = round($row->po_qty, 2) * round($row->po_cost_pr_unt, 2);
			$spo_cost_sheet = $row->attach_cost_sheet;
            
            $sql1 ="exec tipldb..pendalcard '$item_code'";
			$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
			$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
			$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
			
			$sql5 ="select top 1 poitm_po_cost, gr_lin_fadate, gr_lin_fmdate, gr_lin_frdate, gr_hdr_grdate, gr_lin_linestatus,
* from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, 
scmdb..gr_hdr_grmain c, scmdb..gr_lin_details d where 
b.poitm_itemcode = '$item_code'
and c.gr_hdr_grno = d.gr_lin_grno
and b.poitm_itemcode = d.gr_lin_itemcode
and a.pomas_pono = c.gr_hdr_orderno
and c.gr_hdr_orderdoc = 'PO'
and c.gr_hdr_grstatus not in('DL')
and a.pomas_pono = b.poitm_pono and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE')
and a.pomas_poamendmentno = b.poitm_poamendmentno 
and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr WHERE pomas_pono = a.pomas_pono AND pomas_poou = a.pomas_poou)
and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO')
order by pomas_poauthdate desc";
			
			$sql6 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no' and item_code = '$item_code'";
			$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
			$sql8 ="select * from TIPLDB..road_permit_state a, scmdb..supp_addr_address b where a.state_code = b.supp_addr_state 
			and supp_addr_supcode = '$supp_code'";
			$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
			$sql_current_yr_con = "select RecptTotalQty from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$item_code'";
			$sql_last_supplier_details = "select SuppName, SuppRate, SuppLeadTime from tipldb..pendalcard_rkg where Flag='SuppDetail' 
			and ItemCode='$item_code'";
			$sql_reorder_details = "select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
			
			//Charges & Discount
		
			$sql_linelvl_charges = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
			where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
			and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode != 'PDISCOUNTS'";
									
			$sql_linelvl_discount = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
			where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
			and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode = 'PDISCOUNTS'";
			
			//PO Need Date
			$sql_po_nd = "select * from scmdb..po_poitm_item_detail where poitm_pono = '$po_num' and poitm_polineno = '$po_line_no'";
			
			$query1 = $this->db->query($sql1);
			$this->db->close();
			$this->db->initialize();
			$query2 = $this->db->query($sql2);
			$query3 = $this->db->query($sql3);
			$query4 = $this->db->query($sql4);
			$query5 = $this->db->query($sql5);
			$query6 = $this->db->query($sql6);
			$query7 = $this->db->query($sql7);
			$query8 = $this->db->query($sql8);
			$query_draw = $this->db->query($sql_draw);
			$query_current_yr_con = $this->db->query($sql_current_yr_con);
			$query_last_supplier_details = $this->db->query($sql_last_supplier_details);
			$query_reorder_details = $this->db->query($sql_reorder_details);
			$query_po_nd = $this->db->query($sql_po_nd);
			
			//charges and discount starts
		
			$query_linelvl_charges = $this->db->query($sql_linelvl_charges);
			$query_linelvl_discount = $this->db->query($sql_linelvl_discount);
			
			if ($query_linelvl_charges->num_rows() > 0) {
			  foreach ($query_linelvl_charges->result() as $row) {
				  $line_level_charges = $row->potcd_tcdamount;
				}
			} else {
				  $line_level_charges = 0;
			}
			
			if ($query_linelvl_discount->num_rows() > 0) {
			  foreach ($query_linelvl_discount->result() as $row) {
				  $line_level_discount = $row->potcd_tcdamount;
				}
			} else {
				  $line_level_discount = 0;
			}
			
			//charges and discount ends
			
			if ($query2->num_rows() > 0) {
			  foreach ($query2->result() as $row) {
				  $lastyr_cons = $row->ConsTotalQty;
				}
			} else {
				  $lastyr_cons = 0;
			}
			
			if ($query3->num_rows() > 0) {
				  $current_stk = 0;
			  foreach ($query3->result() as $row) {
				  $current_stock = $row->ItemStkAccepted;
				  $current_stk = $current_stk + $current_stock;
				}
			} else {
				  $current_stk = 0;
			}
			
			if ($query4->num_rows() > 0) {
			  foreach ($query4->result() as $row) {
				  $reservation_qty = $row->AllocPendingTot;
				}
			} else {
				  $reservation_qty = 0;
			}
			
			if ($query5->num_rows() > 0) {
			  foreach ($query5->result() as $row) {
				  $last_price = $row->poitm_po_cost;
				  $last_price1 = number_format($last_price,2);
				  $gr_lin_linestatus = $row->gr_lin_linestatus;
				  
				  if($gr_lin_linestatus == 'FA'){
					  $last_grcpt_date = $row->gr_lin_fadate;
				  } else if($gr_lin_linestatus == 'FM'){
					  $last_grcpt_date = $row->gr_lin_fmdate;
				  } else if($gr_lin_linestatus == 'FZ'){
					  $last_grcpt_date = $row->gr_lin_frdate;
				  } else if($gr_lin_linestatus == 'DR' || $gr_lin_linestatus == 'FR'){
					  $last_grcpt_date = $row->gr_hdr_grdate;
				  } 
				}
			} else {
				  $last_price = "";
				  $last_price1 = "";
				  $last_grcpt_date = "";
			}
			
			if ($query6->num_rows() > 0) {
			  foreach ($query6->result() as $row) {
					$ipr_type            = $row->usage;
					$sono                = $row->sono;
					$atac_no             = $row->atac_no;
					$atac_ld_date        = $row->atac_ld_date;
					$atac_need_date      = $row->atac_need_date;
					$atac_payment_terms  = $row->atac_payment_terms;
					$pm_group            = $row->pm_group;
					$category            = $row->category;
					$project_name        = $row->project_name;
					$costing             = $row->costing;
					$ipr_remarks         = $row->remarks;
					$attached_cost_sheet = $row->attch_cost_sheet;
					$ipr_need_date       = $row->need_date;
					$manufact_clrnce	 = $row->manufact_clearance;
					$dispatch_inst       = $row->dispatch_inst;
					$supp_item_remarks   = $row->pr_supp_remarks;
					$costing_currency    = $row->costing_currency;
					$why_spcl_rmks       = $row->why_spcl_rmks;
					$pr_qty              = number_format($row->required_qty,2);
				}
			} else {
					$ipr_type            = "";
					$sono                = "";
					$atac_no             = "";
					$pm_group            = "";
					$category            = "";
					$project_name        = "";
					$costing             = "";
					$ipr_remarks         = "";
					$ipr_need_date       = "";
					$atac_ld_date        = "";
					$atac_need_date      = "";
					$atac_payment_terms  = "";
					$attached_cost_sheet = "";
					$manufact_clrnce	 = "";
					$dispatch_inst       = "";
					$supp_item_remarks   = "";
					$costing_currency    = "";
					$why_spcl_rmks       = "";
					$pr_qty              = ""; 	  
			}
			
			if ($query7->num_rows() > 0) {
			  $incoming_qty_tot1 = 0;
			  foreach ($query7->result() as $row) {
				  $incoming_qty = $row->PendPoSchQty;
				  $incoming_qty_tot = $incoming_qty_tot1 + $incoming_qty;
				}
			} else {
				  $incoming_qty_tot = "";
			}
			
			if ($query8->num_rows() > 0) {
			  foreach ($query8->result() as $row) {
				  $supp_state = $row->state_code;
				  $road_permit_req = $row->road_permit;
				}
			} else {
				  $supp_state = "";
				  $road_permit_req = "";
			}
			
			if ($query_draw->num_rows() > 0) {
			  foreach ($query_draw->result() as $row) {
				  $drawing_no = $row->DrawingNo;
				  $purchase_uom  = $row->ItemPurcaseUom;
				  $manufact_uom = $row->ItemMnfgUom;
				}
			} else {
				  $drawing_no = "";
				  $purchase_uom  = "";
				  $manufact_uom = "";
			}
			
			if ($query_current_yr_con->num_rows() > 0) {
			  foreach ($query_current_yr_con->result() as $row) {
				  $current_yr_con = $row->RecptTotalQty;
				}
			} else {
				  $current_yr_con = "";
			}
			
			if ($query_last_supplier_details->num_rows() > 0) {
			  foreach ($query_last_supplier_details->result() as $row) {
				  $SuppName = $row->SuppName; 
				  $SuppRate = $row->SuppRate; 
				  $SuppLeadTime = $row->SuppLeadTime;
				}
			} else {
				  $SuppName = ""; 
				  $SuppRate = ""; 
				  $SuppLeadTime = "";
			}
			
			if ($query_reorder_details->num_rows() > 0) {
			  foreach ($query_reorder_details->result() as $row) {
				  $iou_reorderlevel = $row->iou_reorderlevel; 
				  $iou_reorderqty = $row->iou_reorderqty;
				}
			} else {
				  $iou_reorderlevel = ""; 
				  $iou_reorderqty = "";
			}
			
			$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";
	
			$query = $this->db->query($sql);
			
			if ($query->num_rows() > 0) {
			  foreach ($query->result() as $row) {
				  $ucon_confact_ntr = $row->ucon_confact_ntr;
				  $ucon_confact_dtr = $row->ucon_confact_dtr;
				  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
			  }
			} else {
				   $conversion_factor = "";  
			}
			/* */ 
			//Supplier Average Lead Time
						
			$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
			$query_supplier = $this->db->query($sql_supplier);
			
			if ($query_supplier->num_rows() > 0) {
				$supplier_lead_time_tot = 0;
				$counter = 0;
			  foreach ($query_supplier->result() as $row) {
				  $counter++;
				  $supplier_lead_time = $row->SuppLeadTime;
				  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
			  }
			  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
			} else {
				   $counter = 0;
				   $supplier_lead_time = "";
				   $supplier_lead_time_tot = ""; 
				   $supplier_avg_lead_time = 0;  
			}
			/* */
			
			foreach($query_po_nd->result() as $row){
				$po_need_date = $row->poitm_needdate;
			}
			
			$sql_supplier = "exec TIPLDB..supplier_details  '1' , '$item_code'  , '$po_ipr_no'";
			$query_supplier = $this->db->query($sql_supplier);
			
			$this->db->close();
			$this->db->initialize();
			
			$sql_supplier_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
			$query_supplier_dtl = $this->db->query($sql_supplier_dtl);
			
			 foreach ($query_supplier_dtl->result() as $row) {
				$diffdays    = $row->diffDays;
			 }
			 
			//echo "Cha==".$diffdays;
			
			if($diffdays != NULL){
				if($diffdays > 0){
					$color = "red";
				} else if($diffdays < 0){
					$color = "green";
				} else if($diffdays = 0){
					$color = "blue";
				}
			} else {
				$color = "yellow";
			}
         ?>
            <tr>
                <?php 
                    echo "<input type='hidden' name='po_s_no' value='$po_s_no' />"; 
                    echo "<input type='hidden' name='po_num' value='$po_num' />";	
                ?>
            <td>
                <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
                <?php echo $po_ipr_no; ?></a>
            </td>
            <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" 
                target="_blank"><?php echo $item_code; ?></a>
            </td>
            <td><?php echo mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8"); ?> </td>
            <td><?php echo $uom; ?></td>
            <td><?php echo $pr_qty; ?></td>
            <td>
                <?php
                    echo $costing;
					echo "<input type='hidden' name='costing' value='$costing' />";
                ?> 
            </td>
            <td>
                <?php
                    echo $costing_currency;
					echo "<input type='hidden' name='costing_currency' value='$costing_currency' />";
                ?> 
            </td>
            <td>
                <?php 
                    echo number_format($last_price,2);
					echo "<input type='hidden' name='last_price[]' value='$last_price' >"; 
                ?>
            </td>
            <td><?php echo number_format($current_price,2); ?></td>
            <td><?php echo number_format($line_level_charges,2); ?></td>
            <td><?php echo number_format($line_level_discount,2); ?></td>
            <td>
                <?php
                    $item_value_new = ($item_value+$line_level_charges)-$line_level_discount; 
                    echo number_format($item_value_new,2); 
                ?>
            </td>
            <td style="background-color:<?php echo $color; ?>">
                <?php
                    echo $ipr_need_date;
                    echo "<input type='hidden' name='ipr_need_date[]' value='$ipr_need_date' />"; 
                ?> 
            </td>
            <td style="background-color:<?php echo $color; ?>" ><?php echo substr($po_need_date,0,11); ?></td>
            <td><?php echo $ipr_type; ?></td>
            <td><?php echo $why_spcl_rmks; ?></td>
            <td><?php echo $sono; ?></td>
            <td> 
                <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                <?php echo $atac_no; ?>
                </a>
            </td>
            <td><?php echo $atac_ld_date; ?></td>
            <td><?php echo $atac_need_date; ?></td>
            <td><?php echo $atac_payment_terms; ?></td>
            <td>
                <?php
                    echo $pm_group; 
                ?> 
            </td>
            <td>
                <?php
                    echo $category; 
                ?> 
            </td>
            <td>
                <?php
                    echo $project_name; 
                ?> 
            </td>
            <td>
                <?php
                    echo $ipr_remarks;
                ?> 
            </td>
            <td>
				<?php echo "<input type='hidden' name='ipr_need_date[]' value='$attached_cost_sheet' />"; ?> 
                <a href="<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>" target='_blank'><?php echo $attached_cost_sheet; ?></a>
        	</td>
            <td>
            	<?php
					echo $supp_item_remarks;
				?>
            </td>
            <td>
            	<?php
					echo $manufact_clrnce;
					echo "<input type='hidden' name='manufact_clrnce[]' id='manufact_clrnce' value='$manufact_clrnce' />";
				?>
            </td>
            <td>
            	<?php
					echo $dispatch_inst;
				?>
            </td>
            <td>
                <?php
                    echo  number_format($lastyr_cons,2);
                ?>
            </td>
            <td>
				<?php
                    echo number_format($current_yr_con,2); 
                ?>
        	</td>
            <td>
                <?php 
                    echo $current_stk;
                ?>
            </td>
            <td>
                <?php
                    echo $incoming_qty_tot;
                ?> 
            </td>
            <td>
                 <?php 
                    echo number_format($reservation_qty,2);
                 ?>
            </td>
            <td>
                <?php 
                    echo $wh_code; 
                ?>
            </td>
            <td>
				<?php 
                    echo $conversion_factor; 
                ?>
        	</td>
            <td>
                <?php 
                    echo number_format($supplier_avg_lead_time,2);
					echo "<input type='hidden' name='supplier_avg_lead_time' value='$supplier_avg_lead_time' >";
                ?>
            </td>
            <td>
                <?php 
                    echo $SuppName; 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($SuppRate,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($SuppLeadTime,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($iou_reorderlevel,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($iou_reorderqty,2); 
                ?>
            </td>
            <td>
				<?php 
                    echo substr($last_grcpt_date,0,11); 
                    echo "<input type='hidden' name='last_grcpt_date[]' value='$last_grcpt_date' />";

                ?>
        	</td>
            <td><?php echo $item_remarks; ?></td>
            <?php if($po_first_three == 'SRV'){ ?>
            <td>
                <a href="<?php echo base_url(); ?>uploads/<?php echo $spo_cost_sheet; ?>"><?php echo $spo_cost_sheet; ?></a>
            </td>
            <?php } ?>
          </tr>
           <?php } ?>
        </tbody>
      </table>
</div>
</div><br /><br />
