<?php 
	include'header.php'; 
	
	$po_num = $this->uri->segment(3);
	$nik = $this->uri->segment(3);
?>
<!--main content start-->
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">DISPATCH INSTRUCTION PURCHASE</h4>
        </div>
    </div><br />
    
    <?php include('po_header.php'); ?>
    
    <form action="<?php echo base_url(); ?>index.php/di_purchasec/insert_di_pur" method="post">
    
    <?php include('po_details_div.php'); ?>
    
    <?php include('po_supplier_quotes.php'); ?>
    
    <!---- DI Purchase Columns ----->
    
    <input type="hidden" id="po_num" name="po_num" value="<?php echo $po_num; ?>">
    <br><br>
    <div class="row">
    	<div class="col-lg-3"><b>Remarks:</b></div>
        <div class="col-lg-3"><textarea id="remarks" name="remarks" class="form-control"></textarea></div>
        <div class="col-lg-2"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Mail To Supplier</button></div>
        <div class="col-lg-4"></div>
    </div>
    
    <!---- DI Purchase Ends --->

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title" style="font-weight:bold; color:#000; text-align:center">Mail Box</h3>
                </div>
                
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2"><b>TO</b></div>
                        <div class="col-lg-6">
                            <?php
                            foreach ($view_po->result() as $row){ 
                                $supp_email1 = $row->po_supp_email;
                                $supp_email2 = str_replace('"','',$supp_email1);
                                $supp_email  = str_replace("'","",$supp_email2)
                            ?>
                            <input type="text" name="to_mail" value="<?php echo str_replace("'","",$supp_email); ?>" class="form-control" />
                            <?php
                                break;
                            }
                            ?>
                        </div>
                        <div class="col-lg-2"></div>
                    </div><br />
                    
                    <!-- From Email -->
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2"><b>FROM</b></div>
                        <div class="col-lg-6">
                            <?php echo "<b>".$_SESSION['username']."@tipl.com</b>"; ?>
                        </div>
                        <div class="col-lg-2"></div>
                    </div><br />
                    
                    <!-- CC Email -->
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2"><b>COPY TO</b></div>
                        <div class="col-lg-6">
							<?php 
                            $cc_email = "purchase@tipl.com";
                            echo "<b>".$cc_email."</b>"; 
                            ?>
                        </div>
                        <div class="col-lg-2"></div>
                    </div><br />
                    
                    <!-- attached po Email -->
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2">
                        	<b>ATT. PO</b>
                        </div>
                        <div class="col-lg-6">
                            <b>
                                <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_num; ?>" target="_blank">
                                    <?php echo $po_num; ?>
                                </a>
                            </b>
                        </div>
                        <div class="col-lg-2"></div>
                    </div><br />
                    
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2">
                            <b>SUBJECT</b>
                        </div>
                        <div class="col-lg-6">
                        	<?php $subject_mail = $po_num." - "."Toshniwal Industries Pvt. Ltd."; ?>
                            <input type="text" name="subject" value="<?php echo $subject_mail; ?>" class="form-control" />
                        </div>
                        <div class="col-lg-2"></div>
                    </div><br />
                    
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2"><b>MESSAGE</b></div>
                        <div class="col-lg-6">
                            <textarea name="message_text" style="width: 325px; height: 270px;" class="form-control">PO No. -<? echo $po_num; ?>

Please Find the attached Purchase Order & Dispatch the material as soon as possible.
							</textarea>
                        </div>
                        <div class="col-lg-2"></div>
                    </div><br />
                    
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2"></div>
                        <div class="col-lg-7">
                            <input type="submit" name="SEND" value="Mail To Supplier" class="form-control" />
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div> 
        </div>
    </div>

    </form>
    
    <?php include('po_chat_history.php'); ?>
    
    <?php include('po_action_timing.php'); ?>
          
  </section>
</section>
</body>
<!-- -main content end -->
<?php include('footer.php'); ?>