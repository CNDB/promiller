<?php
	include'header.php';	
	$nik = $this->uri->segment(3);
	$po_entered_from = $this->uri->segment(4);                    
?>

<!--main content start-->
<body onLoad="check(<?php echo $nik; ?>,'from_pr');">
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Create Purchase Order In Live</h4>
        </div>
    </div><br />
    
    <?php include('po_header.php'); ?>
    
  </section>
</section>
      
<script type="text/javascript">
	function check(str,whre_nik)
	{
		$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	
	
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		xmlhttp.onreadystatechange=function()
		{
			if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_pr')
			{
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
				$( "#datepicker1" ).datepicker();
							   
			} 
		}
		
		
		var queryString="?q="+str;
		if(whre_nik == 'from_pr') {		 	
			xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpodc/view_po" + queryString,true); 
		} 
			    
		xmlhttp.send();	
	}

</script>

<script type="text/javascript" >

	function reqd()
	{
	  var delivery_type = document.getElementById("po_deli_type").value;
	  var po_lead_time = document.getElementById("datepicker1").value;
	  var currency = document.getElementById("currency").value;
	  var freight_type = document.getElementById("freight_type").value;
	  var approx_freight = document.getElementById("approx_freight").value;
	  var ld_applicable = document.getElementById("ld_applicable").value;
	  var spcl_inst_supp = document.getElementById("spcl_inst_supp").value;
	  var po_quote_frmsupp = document.getElementById("po_quote_frmsupp").value;
	  var supplier_email = document.getElementById("supplier_email").value;
	  var supplier_phone_no = document.getElementById("supplier_phone_no").value;
	  var contact_person = document.getElementById("contact_person").value;
	  
	  if( supplier_email == ""){
		  
		  alert("Please Enter Supplier Email Address");
		  document.getElementById("supplier_email").focus();
		  return false;
	  }
	  
	  if( supplier_phone_no == ""){
		  
		  alert("Please Enter Supplier Phone No.");
		  document.getElementById("supplier_phone_no").focus();
		  return false;
	  }
	  
	  if( contact_person == ""){
		  
		  alert("Please Enter Contact Person Name");
		  document.getElementById("contact_person").focus();
		  return false;
	  }
	  
	  if( delivery_type == ""){
		  
		  alert("Select Delivery Type");
		  document.getElementById("po_deli_type").focus();
		  return false;
	  }
	  
	  if( currency == ""){
		  
		  alert("Select Currency");
		  document.getElementById("currency").focus();
		  return false;
	  } 
	  
	  if( po_lead_time == ""){
		  
		  alert("Enter Expected Matrial Landed Date");
		  document.getElementById("datepicker1").focus();
		  return false;
	  } 
	  
	  if( freight_type == ""){
		  
		  alert("Select Freight Type");
		  document.getElementById("freight_type").focus();
		  return false;
	  } 
	  
	  if( approx_freight == ""){
		  
		  alert("Enter Approximate Freight");
		  document.getElementById("po_deli_type").focus();
		  return false;
	  } 
	  
	  if( ld_applicable == ""){
		  
		  alert("Select LD Applicable");
		  document.getElementById("ld_applicable").focus();
		  return false;
	  } 
	  
	  /*if( spcl_inst_supp == ""){
		  
		  alert("Enter Special Instructions Given To Supplier");
		  document.getElementById("spcl_inst_supp").focus();
		  return false;
	  }*/ 
	  
	  if( po_quote_frmsupp == ""){
		  
		  alert("Attach Supplier Quotes");
		  document.getElementById("po_quote_frmsupp").focus();
		  return false;
	  }
	}
</script>

<script type="text/javascript" >
	$(document).ready(function(){	
		check('<? echo $nik; ?>','from_pr');
	});
</script>
 
<script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>

<script type="text/javascript">
	$(function()
	{
		$(document).on('click', '.btn-add', function(e)
		{
			e.preventDefault();
	
			var controlForm = $('.controls:first'),
				currentEntry = $(this).parents('.entry:first'),
				newEntry = $(currentEntry.clone()).appendTo(controlForm);
	
			newEntry.find('input').val('');
			controlForm.find('.entry:not(:last) .btn-add')
				.removeClass('btn-add').addClass('btn-remove')
				.removeClass('btn-success').addClass('btn-danger')
				.html('<span class="glyphicon glyphicon-minus"></span>');
		}).on('click', '.btn-remove', function(e)
		{
		  $(this).parents('.entry:first').remove();
	
			e.preventDefault();
			return false;
		});
	});
</script>

<?php include('footer.php'); ?>