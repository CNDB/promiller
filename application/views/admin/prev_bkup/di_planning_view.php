<?php 
	include'header.php'; 
	
	$po_num = $this->uri->segment(3);
	$nik = $this->uri->segment(3);
?>
<!--main content start-->
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">DISPATCH INSTRUCTION PLANNING</h4>
        </div>
    </div><br />
    
    <?php include('po_header.php'); ?>
    
    <form action="<?php echo base_url(); ?>index.php/di_planningc/insert_di_plan" method="post">
    
    <?php include('po_details_div.php'); ?>
    
    <?php include('po_supplier_quotes.php'); ?>
    
    <?php
	if($pm_group == ''){
		$pm_group = "No PM Group";
	}
	?>
    
    <input type="hidden" id="po_num" name="po_num" value="<?php echo $po_num; ?>">
    <br><br>
    
	<?php
		$sql_pi = "select * from tipldb..pm_approval_master where category = '".$category."' and pm_group = '".$pm_group."'"; 
		$qry_pi = $this->db->query($sql_pi);
		
		foreach($qry_pi->result() as $row){
			$pi_app_by = strtoupper($row->pi_app_by);
		}
		
		$username = strtoupper($_SESSION['username']);
		
		if(in_array($username, array('ADMIN','PRITI.TOSHNIWAL','ABHINAV.TOSHNIWAL','RAJEEV.TOSHNIWAL','PRIYANKA.VIJAY','CHANDRA.SHARMA')) || $username == $pi_app_by){
    ?>
    
    <div class="row">
    	<div class="col-lg-3"><b>Remarks:</b></div>
        <div class="col-lg-3"><textarea id="remarks" name="remarks" class="form-control"></textarea></div>
        <div class="col-lg-2"><input type="submit" id="submit" name="submit" value="SUBMIT" class="form-control"></div>
        <div class="col-lg-4"></div>
    </div>
    
    <?php
		} else {
			echo "<h3 style='color:red; font-weight:bold; text-align:center'>You are not authorized to provide Dispatch Instruction.</h3>";
		}
	?>
    
    <!---- Lead Time Columns ----->
    
    <!---- Lead Time Columns End --->
    
    </form>
    
    <?php include('po_chat_history.php'); ?>
    
    <?php include('po_action_timing.php'); ?>
          
  </section>
</section>
</body>
<!-- -main content end -->
<?php include('footer.php'); ?>