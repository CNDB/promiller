<?php 
	include'header.php';
	$this->load->helper('flow'); 
?>

<style>
	button.accordion {
		background-color:#ddd;
		color: #444;
		cursor: pointer;
		padding: 2px;
		width: 100%;
		border: none;
		text-align: center;
		font-weight:bold;
		outline: none;
		font-size: 12px;
		transition: 0.4s;
		border-radius:8px;
	}
	
	button.accordion.active, button.accordion:hover {
		background-color: #999999;
	}
	
	button.accordion:after {
		content: '\02795';
		font-size: 13px;
		color: #777;
		float: right;
		margin-left: 5px;
	}
	
	button.accordion.active:after {
		content: "\2796";
	}
	
	div.panel {
		padding: 0 5px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: 0.6s ease-in-out;
		opacity: 0;
		margin-bottom:4px;
	}
	
	div.panel.show {
		opacity: 1;
		max-height: 300px;
	}
	
	table thead tr{
		display:block;
	}
	
	table th,table td{
		width:300px;
	}
	
	table  tbody{		
		display:block;
		height:200px;
		overflow:auto;
	}
</style>

<?php include 'flow_ajax.php'; ?>

<?php

$sql_proc = "exec tipldb..gateentry_master_proc";
$qry_proc = $this->db->query($sql_proc);
$this->db->close();
$this->db->initialize();

$sql_pr_po_proc = "exec tipldb..pr_po_pending";
$qry_pr_po_proc = $this->db->query($sql_pr_po_proc);
$this->db->close();
$this->db->initialize();


$username = $_SESSION['username'];

$sql = "select * from TIPLDB..purchase_right_master where emp_email like'%$username%'";

$query = $this->db->query($sql);

foreach($query->result() as $row) {
		//PR
		$emp_name = $row->emp_name;
		$emp_email = $row->emp_email;
		$fresh_pr_creation = $row->fresh_pr_creation;
		$attach_drawing = $row->attach_drawing;
		$pr_lvl1_authorize = $row->pr_lvl1_authorize;
		$disapprove_pr_lvl1 = $row->disapprove_pr_lvl1;
		$pr_lvl2_authorize = $row->pr_lvl2_authorize;
		$disapprove_pr_lvl2 = $row->disapprove_pr_lvl2;
		$erp_authorization_pending = $row->erp_authorization_pending;
		$pr_po_nt_created = $row->pr_po_nt_created;
		
		//PO
		$fresh_po = $row->fresh_po;
		$auth_po_l1 = $row->auth_po_l1;
		$dis_po_l1 = $row->dis_po_l1;
		$auth_po_l2 = $row->auth_po_l2;
		$dis_po_l2 = $row->dis_po_l2;
		$erp_auth_pend_po = $row->erp_auth_pend_po;
		$erp_amend_po = $row->erp_amend_po;
		$supp_for_purchase = $row->supp_for_purchase;
		$mc_planning1 = $row->mc_planning;
		$mc_purchase1 = $row->mc_purchase;
		$acknowledgement = $row->acknowledgement;
		$tc_upload = $row->tc_upload;
		$po_advance = $row->advance;
		$pi_upload = $row->pi_upload;
		$pi_approval = $row->pi_approval;
		$pi_payment = $row->pi_payment;
		$pdc_creation = $row->pdc_creation;
		$po_freight = $row->freight;
		$road_permit = $row->road_permit;
		$delivery_detail = $row->delivery_detail;
		$gate_entry = $row->gate_entry;
	}
	
	$sql_pr_stat_updt = "update tipldb..pr_submit_table set pr_status = 'AUTHORIZED' where pr_status not in('PR Disapproved At Purchase Level')
and pr_num in(select distinct preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'AU')";

	$qry_pr_stat_updt = $this->db->query($sql_pr_stat_updt);
	
	//Disapproved PR PO Created And Not Deleted
	$sql_pr_stat_updt2 = "update tipldb..pr_submit_table set pr_status = 'AUTHORIZED' where pr_status in('PR Disapproved At Purchase Level')
and pr_num in(select distinct preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'AU')
and pr_num in(select distinct po_ipr_no from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b 
where a.po_num = b.pomas_pono 
and b.pomas_podocstatus not in('DE','SC')
and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono))";

	$qry_pr_stat_updt2 = $this->db->query($sql_pr_stat_updt2);

?>

<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">
              Complete History Of Purchase Order And Purchase Request</h4>
        </div>
    </div><br />
    <!-- ********* Fresh Purchase Requests ************ -->
    <div class="row">
    	<div class="col-lg-2">
        </div>
        <div class="col-lg-8">
        
        	<!-- ********* MRP ITEM REQUEST STARTS ************ -->
            <?php //if($fresh_pr_creation == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;">
            1. MATERIAL REQUIREMENT PLANNING (M.R.P.)
            </button>
            <div class="panel">
                <div id="mrp">
                	<h5 style="text-align:center"><a href="<?php echo base_url(); ?>index.php/mrp_reportc" target="_blank">Run MRP Report</a></h5>
                </div>
            </div>
            <? //} ?>
            <!-- ********* MRP ITEM REQUEST ENDS ************ -->
        	
            <!-- ********* Fresh Purchase Requests start ************ -->
            <?php //if($fresh_pr_creation == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;" onMouseDown="getfreshpr()">
            2. ERP FRESH PR PENDING FOR LIVE PR CREATION (<?php echo count_fresh_pr(); ?>)
            </button>
            <div class="panel">
                <div id="fresh_pr"></div>
            </div>
            <? //} ?>
            <!-- ********* Fresh Purchase Requests end ************ -->
            
            
            <!-- ********* Attach Drawing in purchase request starts ************ -->
            <?php //if($attach_drawing == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;" onMouseDown="getattach_drawing()">
            3. ATTACH DRAWING (<?php echo count_drawing(); ?>)
            </button>
            <div class="panel">
                <div id="attach_drawing"></div>
            </div>
            <? //} ?>
            <!-- ********* Attach Drawing in purchase request end ************ -->
            
            
            <!-- ********* Live purchase Request Pending For Live Authorization Level1 ************ -->
            <?php //if($pr_lvl1_authorize == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_pr()">
            4. LIVE PURCHASE REQUEST PENDING FOR AUTHORIZATION LEVEL1 (<?php echo count_pr_l1(); ?>)
            </button>
            <div class="panel">
                <div id="auth_pr"></div>
            </div>
            <? //} ?>
            <!-- ********* Live purchase Request Pending For Live Authorization Level1 ************ -->
            
            <!-- ********* Disapproved Purchase Request Planning Level1 ************ -->
            <?php //if($disapprove_pr_lvl1 == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pr_plan()">
            5. DISAPPROVED PURCHASE REQUEST AT LEVEL1 (<?php echo count_dis_pr_l1(); ?>)
            </button>
            <div class="panel">
                <div id="dis_pr_plan"></div>
            </div>
            <? //} ?>
            <!-- ********* Live purchase Request Pending For Live Authorization Level1 ************ -->
            
            <!-- ********* Live purchase Request Pending For Live Authorization Level2 ************ -->
            <?php //if($pr_lvl2_authorize == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_pr_lvl2()">
            6. LIVE PURCHASE REQUEST PENDING FOR AUTHORIZATION LEVEL2 (<?php echo count_pr_l2(); ?>)
            </button>
            <div class="panel">
                <div id="auth_pr_lvl2"></div>
            </div>
            <? //} ?>
            <!-- ********* Live purchase Request Pending For Live Authorization Level2 ************ -->
            
            <!-- ********* Disapproved Purchase Request Planning Level2 ************ -->
            <?php //if($disapprove_pr_lvl2 == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pr_plan_lvl2()">
            7. DISAPPROVED PURCHASE REQUEST AT LEVEL2 (<?php echo count_dis_pr_l2(); ?>)
            </button>
            <div class="panel">
                <div id="dis_pr_plan_lvl2"></div>
            </div>
            <? //} ?>
            <!-- ********* Disapproved Purchase Request Planning Level2 ************ -->
            
            <!-- ********* Live Aurhorized purchase Request Pending For ERP Authorization ************ -->
            <?php //if($erp_authorization_pending == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="geterp_not_auth_pr()">
            8. LIVE AUTHORIZED PURCHASE REQUEST PENDING FOR ERP AUTHORIZATION (<?php echo count_erp_not_auth_pr(); ?>)
            </button>
            <div class="panel">
                <div id="erp_not_auth_pr"></div>
            </div>
            <? //} ?>
            <!-- ********* Live Aurhorized Purchase Request Pending For ERP Authorization ************ -->
            
            <!-- ********* ERP Authorized Purchase Requests Whose Purchase Order Is Not Created************ -->
            <?php //if($pr_po_nt_created == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getpr_po_report()">
            9. ERP AUTHORIZED PR PENDING FOR ERP PO CREATION (<?php count_pr_po_pend(); ?>)
            </button>
            <div class="panel">
                <div id="pr_po_report"></div>
            </div>
            <? //} ?>
            <!-- ********* ERP Authorized Purchase Requests end ************ -->
            
            <!-- ********* Disapproved Purchase Request Purchase ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pr_pur()">
            10. DISAPPROVED PURCHASE REQUESTS BY PURCHASE DEPARTMENT (<?php echo count_dis_pr_pur(); ?>)
            </button>
            <div class="panel">
                <div id="dis_pr_pur"></div>
            </div>
            <!-- ********* Disapproved Purchase Request Purchase ************ -->
            
            <!-- ********* Fresh Purchase Order ************ -->
            <?php //if($fresh_po == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getfresh_po()">
            11. ERP FRESH PO'S PENDING FOR LIVE PO CREATION (<?php echo count_fresh_po(); ?>)
            </button>
            <div class="panel">
              <div id="fresh_po"></div>
            </div>
            <? //} ?>
            <!-- ********* Fresh Purchase Order end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 1 ************ -->
            <?php //if($auth_po_l1 == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_po_lvl1()">
            12. LIVE PO PENDING FOR AUTHORIZATION LEVEL1 (<?php echo count_auth_po_l1(); ?>)		            
            </button>
            <div class="panel">
              <div id="auth_po_lvl1"></div>
            </div>
            <? //} ?>
            <!-- ********* Authorize Purchase Order Level 1 end ************ -->
            
            <!-- ********* Disapproved Purchase Order Level 1 ************ -->
            <?php //if($dis_po_l1 == 'Yes'){ ?>
            <?php
			  if (count_dis_po_l1() > 0)
			  { 
				echo '<style>			
						#label 
							{
								background-color: yellow;
								animation-name: example;
								animation-duration: 2s;
								animation-iteration-count: infinite;
							}
						
						@keyframes example 
							{
								0%   {background-color: yellow;}
								100%  {background-color: red;}
							}
					  </style>';
			   }
            ?>
            <button class="accordion" id="label" style="color:black; font-weight:bold;"  onMouseDown="getdis_po_lvl1()">
            13. DISAPPROVED LIVE PO'S AT LEVEL 1 (<?php echo count_dis_po_l1(); ?>)
            </button>
            <div class="panel">
              <div id="dis_po_lvl1"></div>
            </div>
            <? //} ?>
            <!-- ********* Disapproved Purchase Order Level 1 end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 2 ************ -->
            <?php //if($auth_po_l2 == 'Yes'){ ?>                              
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_po_lvl2()">
            14. LIVE PO'S PENDING FOR AUTHORIZATION LEVEL2 (<?php echo count_auth_po_l2(); ?>)
            </button>
            <div class="panel">
                <div id="auth_po_lvl2"></div>
            </div>
            <? //} ?>
            <!-- ********* Authorize Purchase Order Level 2 end ************ --> 
            
            <!-- ********* Disapproved Purchase Order Level 2 ************ -->
            <?php //if($dis_po_l2 == 'Yes'){ ?>
            <?php  
			  if (count_dis_po_l2() >0)
			  { 
				echo '<style>
						#label1 
						{
							background-color: yellow;
							animation-name: example;
							animation-duration: 2s;
							animation-iteration-count: infinite;
						}
						
						@keyframes example 
						{
							0%   {background-color: yellow;}
							100%  {background-color: red;}	
						}
					  </style>';
			   }
            ?>
            <button class="accordion" id="label1" style="color:black; font-weight:bold;"  onMouseDown="getdis_po_lvl2()">
            15. DISAPPROVED LIVE PO'S AT LEVEL 2 (<?php echo count_dis_po_l2(); ?>)
            </button>
            <div class="panel">
              <div id="dis_po_lvl2"></div>
            </div>
            <? //} ?>
            <!-- ********* Disapproved Purchase Order Level 2 end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 3 ************ -->
            <?php //if($auth_po_l2 == 'Yes'){ ?>                              
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_po_lvl3()">
            16. LIVE PO'S PENDING FOR AUTHORIZATION LEVEL3 (<?php echo count_auth_po_l3(); ?>)
            </button>
            <div class="panel">
                <div id="auth_po_lvl3"></div>
            </div>
            <? //} ?>
            <!-- ********* Authorize Purchase Order Level 3 end ************ --> 
            
            <!-- ********* Disapproved Purchase Order Level 3 ************ -->
            <?php //if($dis_po_l2 == 'Yes'){ ?>
            <?php 
			  if (count_dis_po_l3() > 0){ 
				echo '<style>
						#label2 
						{
							background-color: yellow;
							animation-name: example;
							animation-duration: 2s;
							animation-iteration-count: infinite;
						}
						
						@keyframes example 
						{
							0%   {background-color: yellow;}
							100%  {background-color: red;}	
						}
					  </style>';
			   }
            ?>
            <button class="accordion" id="label2" style="color:black; font-weight:bold;"  onMouseDown="getdis_po_lvl3()">
            17. DISAPPROVED LIVE PO'S AT LEVEL 3  (<?php echo count_dis_po_l3(); ?>)
            </button>
            <div class="panel">
              <div id="dis_po_lvl3"></div>
            </div>
            <? //} ?>
            <!-- ********* Disapproved Purchase Order Level 3 end ************ -->
            
            <!-- ********* Live Authorized Purchase Order Pending For ERP Authorization ************ -->
            <?php //if($supp_for_purchase == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;" onMouseDown="getsupp_for_pur()">
            18. ERP NOT AUTH PO (<?php echo count_po_supp_old(); ?>)
            </button>
            <div class="panel">
                <div id="supp_for_pur"></div>
            </div>
            <!-- ********* Live Authorized Purchase Order Pending For ERP Authorization ************ -->
            
            <!-- ********* PO Send Supplier For Purchase ************ -->
            <?php //if($erp_auth_pend_po == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getnot_auth_po()">
            19. LIVE AUTHORIZED PO PENDING FOR SEND TO SUPPLIER FOR PURCHASE  (<?php echo count_not_auth_po(); ?>)
            </button>
            <div class="panel">
                <div id="not_auth_po"></div>
            </div> 
            <!-- ********* PO Send Supplier For Purchase end ************ -->
            
            <!-- ********* ERP Amended PO'S ************ -->
            <?php //if($erp_amend_po == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getamend_po()">
            20. ERP AMENDED PO'S (<?php echo count_amend_po(); ?>)
            </button>
            <div class="panel">
                <div id="amend_po"></div>
            </div> 
            <!-- ********* ERP Amended PO'S ************ -->
            
            <!-- ********* PO Pending For Lead Time Update By Purchase ************ -->
            <?php //if($supp_for_purchase == 'Yes'){ ?>
            <?php /*?><button class="accordion" style="color:black; font-weight:bold;" onMouseDown="getlead_time_update()">
            21. LIVE PO'S PENDING FOR LEAD TIME UPDATE FROM PURCHASE (<?php echo count_lead_time_update(); ?>)
            </button>
            <div class="panel">
                <div id="lead_time_update"></div>
            </div><?php */?>
            <? //} ?> 
            <!-- ********* PO Pending For Lead Time Update By Purchase end ************ -->
            
            <!-- ********* PO pending for issuance of manufacturing clearance from planning ************ -->
            <?php //if($mc_planning1 == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmc_planning()">
            22. LIVE PO'S PENDING FOR ISSUANCE OF MANUFACTURING CLEARNACE FROM PROJECT INCHARGE (<?php echo count_mc_planning(); ?>)
            </button>
            <div class="panel">
                <div id="mc_planning"></div>
            </div>
            <? //} ?> 
            <!-- ********* PO pending for issuance of manufacturing clearance from planning end ************ -->
                
            <!-- ********* PO pending for issuance of manufacturing clearance from Purchase dept. ************ -->
            <?php //if($mc_purchase1 == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmc_purchase()">
            23. LIVE PO'S PENDING FOR ISSUANCE OF MANUFACTURING CLEARANCE FROM PURCHASE (<?php echo count_mc_purchase(); ?>)
            </button>
            <div class="panel">
                <div id="mc_purchase"></div>
            </div> 
            <? //} ?> 
            <!-- ********* PO pending for issuance of manufacturing clearance from Purchase dept. end ************ -->
             
            <!-- ********* PO awaited for acknowledgement from supplier ************ --> 
            <?php //if($acknowledgement == 'Yes'){ ?>                         
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getack_frm_supp()">
            24. PENDING IN ACKNOWLEDGEMENT FROM THE SUPPLIER (<?php echo count_ack_supp(); ?>)
            </button>
            <div class="panel">
                <div id="ack_frm_supp"></div>
            </div> 
            <? //} ?> 
            <!-- ********* PO awaited for acknowledgement from supplier end ************ -->
            
            <!-- ********* PO pending for test certificate upload ************ -->
            <?php //if($tc_upload == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="gettc_upload()">
            25. PENDING FOR TEST CERTIFICATES IF REQ. (<?php echo count_tc_attch(); ?>)
            </button>
            <div class="panel">
                <div id="tc_upload"></div>
            </div>
            <? //} ?> 
            <!-- ********* PO pending for test certificate upload end ************ -->
            
            <!-- ********* Pending For Issuance Dispatch Instruction By Planning Starts ************ -->
            <?php //if($road_permit == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdi_planning()">
            26. LIVE PO'S PENDING FOR ISSUANCE OF DISPATCH INSTRUCTION FROM PROJECT INCHARGE  (<?php echo count_di_planning(); ?>)
            </button>
            <div class="panel">
                <div id="di_planning"></div>
            </div>
            <? //} ?>
            <!-- ********* Pending For Issuance Dispatch Instruction By Planning Ends ************ -->
            
            <!-- ********* Pending For Issuance Dispatch Instruction By Purchase Starts ************ -->
            <?php //if($road_permit == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdi_purchase()">
            27. LIVE PO'S PENDING FOR ISSUANCE OF DISPATCH INSTRUCTION FROM PURCHASE (<?php echo count_di_purchase(); ?>)
            </button>
            <div class="panel">
                <div id="di_purchase"></div>
            </div>
            <? //} ?>
            <!-- ********* Pending For Issuance Dispatch Instruction By Purchase Ends ************ -->
             
            <!-- ********* PO awaited for road permit from supplier ************ -->
            <?php //if($road_permit == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdispatch_inst()">
            28. LIVE PO'S AWAITED FOR ROAD PERMIT  (<?php echo count_dispatch(); ?>)
            </button>
            <div class="panel">
                <div id="dispatch_inst"></div>
            </div>
            <? //} ?>
            <!-- ********* PO awaited for road permit from supplier end ************ -->
             
            <!-- ********* PO awaited for Delivery Details ************ -->
            <?php //if($delivery_detail == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdeli_details()">
            29. LIVE PO'S PENDING FOR DELIVERY DETAILS  (<?php echo count_delivery_det(); ?>)
            </button>
            <div class="panel">
                <div id="deli_details"></div>
            </div>
            <? //} ?> 
            <!-- ********* PO awaited for Delivery Details end ************ -->
            
            <!-- ********* PO pending for gate entry start ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getgate_entry()">
            30. LIVE PO'S PENDING FOR GATE ENTRY (<?php echo count_gate_entry(); ?>)
            </button>
            <div class="panel">
              <div id="gate_entry"></div>
            </div>
            <? //} ?> 
            <!-- ********* PO pending for gate entry end ************ -->
            
            <!-- ********* LIVE PO'S PENDING FOR HSN & QTY CHECK BEFORE GRCPT ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="gethsn_check()">
            31. LIVE PO'S PENDING FOR HSN & QTY CHECK BEFORE GRCPT (<?php echo count_hsn_check(); ?>)
            </button>
            <div class="panel">
              <div id="hsn_check"></div>
            </div>
            <? //} ?> 
            
            <!--- MC AND DI Clause --->
            
            <!-- ********* WITHOUT MC AND DI PENDING FOR APPROVAL FROM REPORTING AUTHORITY ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmcdi_ra()">
            32. WITHOUT MC AND DI PENDING FOR APPROVAL FROM PURCHASE HEAD (<?php echo count_mcdi_ra(); ?>)
            </button>
            <div class="panel">
              <div id="mcdi_ra"></div>
            </div>
            <? //} ?> 
            
            <!-- ********* DISAPPROVED PO'S FROM REPORTING AUTHORITY ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_ra()">
            33. DISAPPROVED PO'S FROM PURCHASE HEAD (<?php echo count_dis_ra(); ?>)
            </button>
            <div class="panel">
              <div id="dis_ra"></div>
            </div>
            <? //} ?>
            
            <!-- ********* WITHOUT MC AND DI PENDING FOR APPROVAL FROM PROJECT INCHARGE ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <?php /*?><button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmcdi_pi()">
            34. WITHOUT MC AND DI PENDING FOR APPROVAL FROM PRODUCT MANAGER (<?php echo count_mcdi_pi(); ?>)
            </button>
            <div class="panel">
              <div id="mcdi_pi"></div>
            </div><?php */?>
            <? //} ?> 
            
            <!-- ********* DISAPPROVED PO'S FROM PROJECT INCHARGE ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <?php /*?><button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pi()">
            35. DISAPPROVED PO'S FROM PRODUCT MANAGER (<?php echo count_dis_pi(); ?>)
            </button>
            <div class="panel">
              <div id="dis_pi"></div>
            </div><?php */?>
            <? //} ?>
            
            <!-- ********* WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L1************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <?php /*?><button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmcdi_cf1()">
            36. WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L1 (<?php echo count_mcdi_cf1(); ?>)
            </button>
            <div class="panel">
              <div id="mcdi_cf1"></div>
            </div><?php */?>
            <? //} ?> 
            
            <!-- ********* DISAPPROVED PO'S FROM CASHFLOW INCHARGE L1 ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <?php /*?><button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_cf1()">
            37. DISAPPROVED PO'S FROM CASHFLOW INCHARGE L1 (<?php echo count_dis_cf1(); ?>)
            </button>
            <div class="panel">
              <div id="dis_cf1"></div>
            </div><?php */?>
            <? //} ?>
            
            <!-- ********* WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L2 ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmcdi_cf2()">
            38. WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L2 (<?php echo count_mcdi_cf2(); ?>)
            </button>
            <div class="panel">
              <div id="mcdi_cf2"></div>
            </div>
            <? //} ?>
            
            <!-- ********* DISAPPROVED PO'S FROM CASHFLOW INCHARGE L2 ************ -->
            <?php //if($gate_entry == 'Yes'){ ?>
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_cf2()">
            39. DISAPPROVED PO'S FROM CASHFLOW INCHARGE L2 (<?php echo count_dis_cf2(); ?>)
            </button>
            <div class="panel">
              <div id="dis_cf2"></div>
            </div>
            <? //} ?> 
            
            <!--- MC & DI Clause Ends --->
            
        </div>
        <div class="col-lg-2">
        </div>
    </div><br />
  </section>
</section>


<script type="text/javascript">
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
	  }
	}
</script>

<script src="<?php echo base_url(); ?>assets/admin/js/flow_javascripts.js"></script>

<?php include'footer.php'; ?>