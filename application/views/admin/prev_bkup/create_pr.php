<?php
	include'header.php';
	ini_set("SMTP","mail.tipl.com"); 							
	ini_set("smtp_port","25");
	ini_set('date.timezone', 'Asia/Calcutta');
	
	if (isset($_REQUEST['email'])){	
	  $to = $_REQUEST['email'];
	  $email = "chandra.sharma@tipl.com";
	  $subject = $_REQUEST['subject'];
	  $message = $_REQUEST['comment'];
	  $headers = "From: $email";
	  $ok = @mail($to, $subject, $message, $headers);
    }
	
    $nik = $this->uri->segment(3);
?>
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Create Purchase Request</h4>
        </div>
    </div><br />
    <div class="row">
        <div class="col-lg-12">
          <div class="col-lg-2">
          	<b>PR No.</b>
          </div>
          <div class="col-lg-2">
                <form action="" method="post" role="form">
                    <select name="selectpr" id="selectpr" class="form-control" onChange="check(this.value,'from_pr');" readonly>
						<?php if ($nik != '') { ?>
                        <option selected="selected" value="<?php echo $nik; ?>" disabled="disabled"><?php echo $nik; ?></option>
                        <?	} else {  ?>
                        <option value="">--Select--</option>
                        <?	foreach ($h->result() as $row){ $prno = $row->prqit_prno; 	?>  
                        <option  value="<?php echo $prno; ?>"><?php echo $prno; ?></option>  
                        <?php }} ?>
                    </select>
                </form>
          </div>
          <div class="col-lg-2"></div>
          <div class="col-lg-6">
            <table border="1" align="center" class="table table-bordered" style="font-size:9px">
                <tr>
                    <td><b>CONDITION</b></td>
                    <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                    <td>FIRST TIME PURCHASE</td>
                </tr>
                <tr>
                	<td><b>COLOR</b></td>
                    <td style="background-color:red;"></td>
                    <td style="background-color:green;"></td>
                    <td style="background-color:blue;"></td>
                    <td style="background-color:yellow;"></td>
                </tr>
            </table>
          </div>
        </div>
    </div><br />     
    <div id="detail"></div> 
  </section>
</section>
</section>
</body>     
<script >
function check(str,whre_nik){	
$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();	
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");	
	}
	  
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_pr'){
		  document.getElementById('detail').innerHTML=xmlhttp.responseText;
		  $("#project_target_date").datepicker();
		  $("#atac_no_select").select2();	
		} 
	}
	
	var queryString="?q="+str;
	if(whre_nik == 'from_pr') {		 	
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/view_pr" + queryString,true);    		 
	}		
	xmlhttp.send();	
}

function check1(str,whre_nik,item_code){	
var usage_type = document.getElementById("usage").value;	
$("#detail_work_new").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");	
	}
	  
	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_wo_new'){			
		  document.getElementById('detail_work_new').innerHTML=xmlhttp.responseText;	
		} 
	}
	
	var queryString="?q="+str+"&item_code="+encodeURIComponent(item_code);
	
	if (whre_nik == 'from_wo_new'){
		if(usage_type == 'Project'){
			xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/wrkordr_ser_res_new" + queryString,true);
		} else {
			xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/wrkordr_ser_res_new1" + queryString,true);
		} 			 
	}		
	xmlhttp.send();	
}
</script>
<script type="text/javascript"> $("#atac_no_select").select2(); </script>
<script type="text/javascript" >
function reqd(){
  var usage_type = document.getElementById("usage").value;
  
  if (usage_type == ""){
	  alert("Please Select Usage Type");
	  document.getElementById("usage").focus();
	  return false;
  }
  
  //COSING CHECK
  var cost_available = document.getElementById("cost_available").value;
  var costing = document.getElementById("costing").value;
  var cost_cal_rmks = document.getElementById("costing_remarks").value;
  var cost_not_available = document.getElementById("costing_not_available_remarks").value;
  var last_price = document.getElementById("last_price").value;
  
  //Costing Validation Start
  if(usage_type == "Project" || usage_type == "Project Pre Order"){
	  if(cost_available == ""){
		  alert("Please Select Cost Available Options ...");
		  document.getElementById("cost_available").focus();
		  return false;
	  } 
	  
	  if(cost_available == "Yes" && costing == ""){
		  alert("Please Enter Cost...");
		  document.getElementById("costing").focus();
		  return false;
	  } else if(cost_available == "Yes" && cost_cal_rmks == ""){
		  alert("Please Enter How you calculate costing...");
		  document.getElementById("costing_remarks").focus();
		  return false;
	  } else if(cost_available == "No" && cost_not_available == ""){
		  alert("Please Enter Reason Why Costing Not Available...");
		  document.getElementById("costing_not_available_remarks").focus();
		  return false;
	  }
	  
  } else if((usage_type == "Special" || usage_type == "Against Reorder") && (last_price == "" )){
	  if(cost_available == ""){
		  alert("Please Select Cost Available Options ...");
		  document.getElementById("cost_available").focus();
		  return false;
	  } 
	  
	  if(cost_available == "Yes" && costing == ""){
		  alert("Please Enter Cost...");
		  document.getElementById("costing").focus();
		  return false;
	  } else if(cost_available == "Yes" && cost_cal_rmks == ""){
		  alert("Please Enter How you calculate costing...");
		  document.getElementById("costing_remarks").focus();
		  return false;
	  } else if(cost_available == "No" && cost_not_available == ""){
		  alert("Please Enter Reason Why Costing Not Available...");
		  document.getElementById("costing_not_available_remarks").focus();
		  return false;
	  }
  }
  //Costing Validation Ends
  var manual_atac = document.getElementById("manual_atac").value;
  
  if(usage_type == 'Project'){
	  var atac_no_select = document.getElementById("atac_no_select").value;
	  
	  if(atac_no_select == ''){
		  alert("Select ATAC No.");
		  document.getElementById("atac_no_select").focus();
		  return false;
	  }
  }
  
  if(usage_type == 'Project Pre Order'){
	  var atac_no_select = document.getElementById("atac_no_select").value;
	  
	  if(atac_no_select == ''){
		  alert("Select ATAC No.");
		  document.getElementById("atac_no_select").focus();
		  return false;
	  }
  }
  
  var category = document.getElementById("category").value;
  var why_spcl_rmks = document.getElementById("why_spcl_rmks").value;
    
  if(usage_type == 'Special'){
	  var category = document.getElementById("category").value;
	  var why_spcl_rmks = document.getElementById("why_spcl_rmks").value;
	  
	  if(category == ''){
		  alert("Select Category");
		  document.getElementById("category").focus();
		  return false;
	  }
	  
	  if(why_spcl_rmks == ''){
		  alert("Enter Why Special Remarks...");
		  document.getElementById("why_spcl_rmks").focus();
		  return false;
	  }
  }
  
  if(usage_type == 'Against Reorder'){
	  var category = document.getElementById("category").value;
	  
	  if(category == ''){
		  alert("Select Category");
		  document.getElementById("category").focus();
		  return false;
	  }
  }
  
  var test_cert = document.getElementById("test_cert").value;
  
  if(test_cert == ""){
	  alert("Please Select Test Certificate Required...");
	  document.getElementById("test_cert").focus();
	  return false;
  }
  
  var project_target_date = document.getElementById("project_target_date").value;
  
  if(project_target_date == "" && usage_type == "Project"){
	  alert("Please Enter Project Target Date Required...");
	  document.getElementById("project_target_date").focus();
	  return false;
  }
  
  var dispatch_instruction = document.getElementById("dispatch_inst").value;
  
  if(dispatch_instruction == ""){
	  alert("Please Select Dispatch Instruction...");
	  document.getElementById("dispatch_inst").focus();
	  return false;
  }
  
  if (usage_type == "Project" || usage_type == "Project Pre Order"){
	  var attch_cost_sheet = document.getElementById("attch_cost_sheet").value;
	  
	  if(attch_cost_sheet == ""){
		  alert("Please Attach Cost Sheet...");
		  document.getElementById("attch_cost_sheet").focus();
		  return false;
	  }
  }
  
  //TC Remarks New Validation
  if (usage_type == "Project" && test_cert == "no"){
	  var tc_rmks = document.getElementById("tc_rmks").value;
	  
	  if(tc_rmks == ""){
		  alert("Please Enter Remarks Why Test Certificate is Not Required");
		  document.getElementById("tc_rmks").focus();
		  return false;
	  }
  }
  
  //Manufacturing Clearance & Dispatch Instruction Validation
  var manufact_clrnce = document.getElementById("manufact_clrnce").value;
  var dispatch_inst = document.getElementById("dispatch_inst").value;
  
  if(manufact_clrnce == ""){
	  alert("Please select Manufacturing Clearance Required");
	  document.getElementById("manufact_clrnce").focus();
	  return false;
  }
  
  if(dispatch_inst == ""){
	  alert("Please select Dispatch Instruction Required");
	  document.getElementById("dispatch_inst").focus();
	  return false;
  }
  
  //Changing MC & DI Value 
  var cat_esc = ['PRODUCTION CONSUMABLES', 'NON PRODUCTION CONSUMABLE', 'IT', 'TOOLS'];
  
  if(usage_type == 'Special' && manufact_clrnce == 'yes'){
	  
		if (cat_esc.indexOf(category) !== -1) {
			document.getElementById("manufact_clrnce").value = "no";
		}
		
  }
  
  if(usage_type == 'Special' && dispatch_inst == 'yes'){
	  
		if (cat_esc.indexOf(category) !== -1) {
			document.getElementById("dispatch_inst").value = "no";
		}
		
  }
  
}
</script>
<script type="text/javascript"> $(document).ready(function(){ check('<? echo $nik; ?>','from_pr'); }); </script>
<script>
function hidecontent1(a){
	var po_first_three = document.getElementById("po_first_three").value;
	if(a == 'Yes'){
		$('#costing').show();
		$('#currency').show();
		$('#costing_remarks').show();
		$('#costing_not_available_remarks').hide();
		
		if(po_first_three == 'FPR' || po_first_three == 'CGP'){
			$("#currency").html("<option value='INR'>INR</option><option value='USD'>USD</option><option value='GBP'>GBP</option><option value='YEN'>YEN</option><option value='EURO'>EURO</option>");
		} else if(po_first_three == 'IPR' || po_first_three == 'LPR'){
			$("#currency").html("<option value='INR'>INR</option>");
		}
		
		$('#costing').val('');
		$('#costing_remarks').val('');
		$('#costing_not_available_remarks').val('');
		
	} else {
		$('#costing').hide();
		$('#currency').hide();
		$('#costing_remarks').hide();
		$('#costing_not_available_remarks').show();
		
		$('#costing').val('');
		$('#costing_remarks').val('');
		$('#costing_not_available_remarks').val('');
	}
}

function hidecontent(a,itm_code){
	if(a == 'Project'){
		$("#manual_atac_label").show();
		$("#manual_atac_label").html("<b >LINK ATAC :</b><b style='color:#F00'>&nbsp;*</b>");
		$("#manual_atac").show();
		$('#manual_atac').html("<option value='Yes'>Yes</option>");
		$("#category_label").hide();
		$("#category").hide();
		$("#test_cert").html("<option value='yes'>yes</option><option value='no'>no</option>");
		$("#manufact_clrnce").html("<option value='yes'>yes</option>");
		$("#dispatch_inst").html("<option value='yes'>yes</option>");
		$('#workorder_div2').show();
		$('#atac_no_label').show();
		$("#atac_no_label").html("<b>Select ATAC No.</b><b style='color:#F00'>&nbsp;*</b>");
		$('#atac_no_select').show();
		$('#select_workorder').show();
		$('#project_target_date_label').show();
		$('#project_target_date').show();
		$('#cost_available').show();
		$('#cost_available').val('');
		$('#costing').val('');
		$('#currency').val('');
		$('#cost_calculation_remarks').val('');
		$('#cost_not_available_remarks').val('');
		document.getElementById("why_spcl_label").style.display="none";
		document.getElementById("why_spcl_rmks").style.display="none";
		document.getElementById("complaint_label").style.display="none";
		document.getElementById("complain_no").style.display="none";
		$("#spcl_pm_div").hide();
		
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		  
		xmlhttp.onreadystatechange=function(){
			if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('atac_selection_div').innerHTML=xmlhttp.responseText;
				$("#atac_no_select").select2();
			} else {
				document.getElementById('atac_selection_div').innerHTML="<strong>Waiting For Server...</strong>";
			}
		}
		
		var queryString="?item_code="+encodeURIComponent(itm_code);
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/atac_project" + queryString,true);			
		xmlhttp.send();
	} else if(a == 'Project Pre Order'){
		$("#manual_atac_label").show();
		$("#manual_atac_label").html("<b >LINK ATAC :</b><b style='color:#F00'>&nbsp;*</b>");
		$("#manual_atac").show();
		$('#manual_atac').html("<option value='Yes'>Yes</option>");
		$("#category_label").hide();
		$("#category").hide();
		$("#test_cert").html("<option value='yes'>yes</option><option value='no'>no</option>");
		$("#manufact_clrnce").html("<option value='yes'>yes</option>");
		$("#dispatch_inst").html("<option value='yes'>yes</option>");
		$('#workorder_div2').show();
		$('#atac_no_label').show();
		$("#atac_no_label").html("<b>Select ATAC No.</b><b style='color:#F00'>&nbsp;*</b>");
		$('#atac_no_select').show();
		$('#select_workorder').hide();
		$('#project_target_date_label').show();
		$('#project_target_date').show();
		$('#cost_available').show();
		$('#cost_available').val('');
		$('#costing').val('');
		$('#currency').val('');
		$('#cost_calculation_remarks').val('');
		$('#cost_not_available_remarks').val('');
		document.getElementById("why_spcl_label").style.display="none";
		document.getElementById("why_spcl_rmks").style.display="none";
		document.getElementById("complaint_label").style.display="none";
		document.getElementById("complain_no").style.display="none";
		$("#spcl_pm_div").hide();
		
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		  
		xmlhttp.onreadystatechange=function(){
			if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('atac_selection_div').innerHTML=xmlhttp.responseText;
				$("#atac_no_select").select2();
			} else {
				document.getElementById('atac_selection_div').innerHTML="<strong>Waiting For Server...</strong>";
			}
		}
		
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/atac_project_preorder",true);			
		xmlhttp.send();
	} else if(a == 'Special') {
		$("#manual_atac_label").show();
		$("#manual_atac_label").html("<b >LINK ATAC :</b><b style='color:#F00'>&nbsp;*</b>");
		$("#manual_atac").show();
		$('#manual_atac').html("<option value='No'>No</option>");
		$("#category_label").show();
		$("#category").show();
		$('#category').html("<option value=''>--Select--</option><option value='SECURITY'>SECURITY</option><option value='SENSORS'>SENSORS</option><option value='INSTRUMENTATION'>INSTRUMENTATION</option><option value='PRODUCTION CONSUMABLES'>PRODUCTION CONSUMABLES</option><option value='PACKING'>PACKING</option><option value='TOOLS'>TOOLS</option><option value='IT'>IT</option><option value='NON PRODUCTION CONSUMABLES'>NON PRODUCTION CONSUMABLES</option><option value='AVAZONIC'>AVAZONIC</option>");
		$("#test_cert").html("<option value='yes'>yes</option><option value='no'>no</option>");	
		$("#manufact_clrnce").html("<option value=''>--select--</option><option value='yes'>yes</option><option value='no'>no</option>");
		$("#dispatch_inst").html("<option value=''>--select--</option><option value='yes'>yes</option><option value='no'>no</option>");
		$('#workorder_div2').hide();
		$('#atac_no_label').hide();
		$("#atac_no_label").html("<b>Select ATAC No.</b><b style='color:#F00'>&nbsp;*</b>");
		$('#atac_no_select').hide();
		$('#select_workorder').hide();
		$('#project_target_date_label').hide();
		$('#project_target_date').hide();
		$('#cost_available').show();
		$('#cost_available').val('');
		$('#costing').val('');
		$('#currency').val('');
		$('#cost_calculation_remarks').val('');
		$('#cost_not_available_remarks').val('');		
		document.getElementById("why_spcl_label").style.display="block";
		document.getElementById("why_spcl_rmks").style.display="block";
		document.getElementById("complaint_label").style.display="block";
		document.getElementById("complain_no").style.display="block";
		$("#spcl_pm_div").show();
		
		if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		} else {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
		  
		xmlhttp.onreadystatechange=function(){
			if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('atac_selection_div').innerHTML=xmlhttp.responseText;
				$("#atac_no_select").select2();
			} else {
				document.getElementById('atac_selection_div').innerHTML="<strong>Waiting For Server...</strong>";
			}
		}
		
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/atac_special",true);			
		xmlhttp.send();	
	} else {
		$("#manual_atac_label").hide();
		$('#atac_no_label').hide();
		$("#atac_no_select").hide();
		document.getElementById('detail_work_new').innerHTML='';
		$("#manual_atac").hide();
		$("#category_label").show();
		$("#category").show();
		$('#category').html("<option value=''>--Select--</option><option value='SECURITY'>SECURITY</option><option value='SENSORS'>SENSORS</option><option value='INSTRUMENTATION'>INSTRUMENTATION</option><option value='PRODUCTION CONSUMABLES'>PRODUCTION CONSUMABLES</option><option value='PACKING'>PACKING</option><option value='TOOLS'>TOOLS</option><option value='IT'>IT</option><option value='NON PRODUCTION CONSUMABLES'>NON PRODUCTION CONSUMABLES</option><option value='AVAZONIC'>AVAZONIC</option>");
		$("#test_cert").html("<option value='no'>no</option><option value='yes'>yes</option>");	
		$("#manufact_clrnce").html("<option value='no'>no</option>");
		$("#dispatch_inst").html("<option value='no'>no</option>");
		$('#select_workorder').hide();	
		$('#project_target_date_label').hide();
		$('#project_target_date').hide();
		$('#cost_available').show();
		document.getElementById("why_spcl_label").style.display="none";
		document.getElementById("why_spcl_rmks").style.display="none";
		document.getElementById("complaint_label").style.display="none";
		document.getElementById("complain_no").style.display="none";
		$("#spcl_pm_div").hide();
	}
}

function hidecontent_new(b){
	if(b == 'Yes'){
		$('#workorder_div2').show();
		$('#atac_no_label').show();
		$("#atac_no_label").html("<b>Select ATAC No.</b><b style='color:#F00'>&nbsp;*</b>");
		$('#atac_no_select').show();
		$("#category_label").hide();
		$("#category").hide();
	}else{
		$('#atac_no_label').hide();
		$('#workorder_div2').hide();
		$('#atac_no_label').hide();
		$('#atac_no_select').hide();
		$("#category_label").show();
		$("#category").show();
		$('#category').html("<option value=''>--Select--</option><option value='PRODUCTION CONSUMABLES'>PRODUCTION CONSUMABLES</option><option value='PACKING'>PACKING</option><option value='TOOLS'>TOOLS</option><option value='IT'>IT</option><option value='NON PRODUCTION CONSUMABLES'>NON PRODUCTION CONSUMABLES</option><option value='AVAZONIC'>AVAZONIC</option>");
	}
}
</script>
<script>
function get_wo_qty(val1){
	console.log("abcd "+val1);
	$('#wo_qty').val(val1);
}

//Restricting Only to insert Numbers
function isNumberKey(evt){
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
  	return false;

  return true;
  
}
</script>

<script>
function hide(a){
	document.getElementById(a).style.display="none";
}
</script>
<script type="text/javascript">
function getExtension1(path){
	var basename = path.split(/[\\/]/).pop();
	var res = basename.split(".");
	var ext = res[res.length-1];
	
	if(ext != 'gif' && ext != 'jpg' && ext != 'jpeg' && ext != 'png' && ext != 'doc' && ext != 'docx' && ext != 'xls' && ext != 'xlsx' && ext != 'ppt' && ext != 'pptx' && ext != 'csv' && ext != 'ods' && ext != 'odt' && ext != 'odp' && ext != 'pdf' && ext != 'txt'){
		alert("File Format Is Not Correct. Only gif, jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx, csv, ods, odt, odp, pdf, txt are allowed.");
		document.getElementById("attch_cost_sheet").value = "";
		return false;	
	}
}

function getExtension2(path) {
	
	var basename = path.split(/[\\/]/).pop();
	var res = basename.split(".");
	var ext = res[res.length-1];
	
	if(ext != 'gif' && ext != 'jpg' && ext != 'jpeg' && ext != 'png' && ext != 'doc' && ext != 'docx' && ext != 'xls' && ext != 'xlsx' && ext != 'ppt' && ext != 'pptx' && ext != 'csv' && ext != 'ods' && ext != 'odt' && ext != 'odp' && ext != 'pdf' && ext != 'txt'){
		alert("File Format Is Not Correct. Only gif, jpg, jpeg, png, doc, docx, xls, xlsx, ppt, pptx, csv, ods, odt, odp, pdf, txt are allowed.");
		document.getElementById("other_attachment").value = "";
		return false;		
	}		
}
</script>
<script>
function test_cert_rmks(a){
	var usage_type = document.getElementById("usage").value;
	if(a == 'no' && usage_type == 'Project'){
		$('#tc_rmks_div').show();
	} else {
		$('#tc_rmks_div').hide();
	}
}
</script>
<script type="text/javascript" language="javascript">
function spcl_cases_pm(a){
	var usage = document.getElementById("usage").value;
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	} else {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	
	xmlhttp.onreadystatechange=function(){
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('spcl_pm_div').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('spcl_pm_div').innerHTML="<strong>Waiting For Server...</strong>";
		}
	}
	
	var queryString="?category="+a+"&usage="+usage;
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc/pr_spcl_pm_sel" + queryString,true);
	xmlhttp.send();
}
</script>
<?php include('footer.php'); ?>