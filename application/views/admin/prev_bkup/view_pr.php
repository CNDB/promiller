<?php
$username = $_SESSION['username'];

$sql_user_det = "select id,user_type,name,* from tipldb..login where email like '$username%' and  emp_active = 'Yes'";
$qry_user_det = $this->db->query($sql_user_det);

foreach($qry_user_det->result() as $row){
	$user_id =  $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
} 
?>
<!--********* ITEM INFORMATION **********-->
<form action="<?php echo base_url(); ?>index.php/createpoc/insert_pr_sub" method="post" enctype="multipart/form-data" onsubmit="return reqd()">
<b style="color:red; font-size:16px">Note:- Costing Should be entered in per piece or per unit (If Available).</b>
<div class="row">
    <div class="col-lg-12" style=" overflow-x:auto;">
     <table class="table table-bordered" id="dataTable" border="1">
        <thead>
          <tr>
            <th>Item Code</th>
            <th>Item Description</th>
            <th>Last Yr Cons.</th>
            <th>Current Stock</th>
            <th>Total Incoming Stock</th>
            <th>Reservation QTY</th>
            <th>PR QTY</th>
            <th>Last Price</th>
            <th>Transanction UOM</th>
            <th>Conversion Factor With Stock UOM</th>
            <th>Supplier Avg. Lead Time</th>
            <th>Need Date</th>
            <th>Warehouse Code</th>
            <th>Cost Available</th>
            <th>Costing (Per Piece)<b style="color:#F00">&nbsp;*</b></th>
            <th>Currency<b style="color:#F00">&nbsp;*</b></th>
            <th>Remarks (On Calculating Cost)<b style="color:#F00">&nbsp;*</b></th>
          </tr>
        </thead>
        <tbody>
         <?php 
		 		$count = 0;
				foreach ($v->result() as $row){
					$count = $count+1; 
					$pr_date1 = substr($row->preqm_prdate,0, 11);
					$pr_date = date("d-m-Y", strtotime($pr_date1));
					$pr_num = $row->prqit_prno;
					$item_code = $row->prqit_itemcode;
					$item_code1 = urlencode($item_code);
					$pr_num_first_three = substr($pr_num,0,3);
					
					if(strpos($item_code1, '%2F') !== false){
						$item_code2 = str_replace("%2F","chandra",$item_code1);	
					} else {	
						$item_code2 = $item_code1;
					}
					
					$req_qty = $row->prqit_reqdqty;
					$uom = $row->prqit_puom;
					$need_date = substr($row->prqit_needdate,0,11);
					$wh_code = $row->prqit_warehousecode;
					//Pendal Card New Proc
					$sql_proc = "exec tipldb..pendalcard_cns '$item_code', '$user_id'";
					$sql_draw = "select * from tipldb..pendalcard_cns_pr where flag='ItemMaster' and itemcode='$item_code' and user_id = '$user_id'";
					$sql2 ="select * from tipldb..pendalcard_cns_pr where Flag='LastYrConsRecptDetail' and ItemCode='$item_code' and user_id = '$user_id'";
					$sql3 ="select * from tipldb..pendalcard_cns_pr where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code' and user_id = '$user_id'";
					$sql4 ="select * from tipldb..pendalcard_cns_pr where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code' and user_id = '$user_id'";
					$sql5 ="select top 1 poitm_po_cost,* from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b 
					where poitm_itemcode = '$item_code' and a.pomas_pono = b.poitm_pono and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE')
					and a.pomas_poamendmentno = b.poitm_poamendmentno 
					and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr WHERE pomas_pono = a.pomas_pono 
					AND pomas_poou = a.pomas_poou) and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO')
					order by pomas_poauthdate desc";
					$sql6 ="select isnull(ml_itemVardesc ,'')+'  '+isnull(lov_matlspecification,' ') AS itm_desc,* from 
					scmdb..itm_ml_multilanguage,                                  
					scmdb..itm_lov_varianthdr,                                  
					scmdb..itm_iou_itemvarhdr,                                  
					scmdb..itm_ibu_itemvarhdr,
					scmdb..itm_loi_itemhdr                                  
					where ml_langid  = 1                                  
					and  ml_itemcode  = lov_itemcode                                   
					and  ml_variantCode = lov_variantcode                    
					AND  ml_itemcode  = iou_itemcode                                                       
					AND  ml_variantCode = iou_variantcode                                   
					AND  ml_itemcode  = ibu_itemcode                                                       
					AND  ml_variantCode = ibu_variantcode
					AND  ml_itemcode  = loi_itemcode                                     
					AND  ml_itemcode  = '$item_code'";
					
					$sql7 ="select * from tipldb..pendalcard_cns_pr 
					where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN' and user_id = '$user_id'";
					
					$query_proc = $this->db->query($sql_proc);
					$query2 = $this->db->query($sql2);
					$query3 = $this->db->query($sql3);
					$query4 = $this->db->query($sql4);
					$query5 = $this->db->query($sql5);
					$query6 = $this->db->query($sql6);
					$query7 = $this->db->query($sql7);
					$query_draw = $this->db->query($sql_draw);
						
					if ($query2->num_rows() > 0) {
						foreach ($query2->result() as $row) {
							$lastyr_cons = $row->ConsTotalQty;
						}
					} else {
						$lastyr_cons = 0;
					}
					
					if ($query3->num_rows() > 0) {
						$current_stk = 0;
						$accepted_stk = 0;
						foreach ($query3->result() as $row) {
						  $current_stock = $row->ItemStkAccepted;
						  $current_stk = $current_stk + $current_stock;
						  $accepted_stock = $row->ItemStkAccepted;
						  $accepted_stk = $accepted_stk + $accepted_stock;
						}
					} else {
						$current_stk = 0.00;
						$accepted_stk = 0.00;
					}
					
					if ($query4->num_rows() > 0) {
						foreach ($query4->result() as $row) {
							$reservation_qty = $row->AllocPendingTot;
							$pending_allocation = $row->Allocated;
						}
					} else {
						$reservation_qty = 0.00;
						$pending_allocation = 0.00;
					}
					
					if ($query5->num_rows() > 0) {
						foreach ($query5->result() as $row) {
							$last_price = $row->poitm_po_cost;
						}
					} else {
						  $last_price = 0.00;
					}
					
					if ($query6->num_rows() > 0) {
						foreach ($query6->result() as $row) {
							$item_desc1 = $row->itm_desc;
							$item_desc = str_replace("'","",$item_desc1);
							$item_desc = mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");
						}
					} else {
						  $item_desc1 = "";
						  $item_desc = "";
					}
					
					if ($query7->num_rows() > 0) {
					  $incoming_qty_tot1 = 0;
					  foreach ($query7->result() as $row) {
						  $incoming_qty = $row->PendPoSchQty;
						  $incoming_qty_tot = $incoming_qty_tot + $incoming_qty;
						}
					} else {
						  $incoming_qty_tot = 0.00;
					}
					
					if ($query_draw->num_rows() > 0) {
						foreach ($query_draw->result() as $row) {
							$drawing_no = $row->DrawingNo;
							$purchase_uom  = $row->ItemPurcaseUom;
							$manufact_uom = $row->ItemMnfgUom;
						}
					} else {
						  $drawing_no = "";
						  $purchase_uom  = "";
						  $manufact_uom = "";
					}
					
					$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' 
					and ucon_itemcode='$item_code'";
					$query = $this->db->query($sql);
					
					if ($query->num_rows() > 0) {
						foreach ($query->result() as $row) {
							$ucon_confact_ntr = $row->ucon_confact_ntr;
							$ucon_confact_dtr = $row->ucon_confact_dtr;
							$conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
						}
					} else {
						   $conversion_factor = "";  
					}
					
					//Supplier Average Lead Time
					$sql_supplier = "select * from tipldb..pendalcard_cns_pr where Flag='SuppDetail' and ItemCode='$item_code' and user_id = '$user_id'";
					$query_supplier = $this->db->query($sql_supplier);
					
					if (count($query_supplier) > 0) {
						$supplier_lead_time_tot = 0;
						$counter = 0;
					  foreach ($query_supplier->result() as $row) {
						  $counter++;
						  $supplier_lead_time = $row->SuppLeadTime;
						  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
					  }
					  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
					} else {
						   $counter = 0;
						   $supplier_lead_time = "";
						   $supplier_lead_time_tot = ""; 
						   $supplier_avg_lead_time = 0;  
					}
					
					$username1 = $_SESSION['username'];
                
         ?>
          <tr>
                <?php
				   echo "<input type='hidden' name='pr_num' value='$pr_num' />";
                   echo "<input type='hidden' name='pr_date' value='$pr_date1' />";
				   echo "<input type='hidden' name='drawing_no' value='$drawing_no' />";
				   echo "<input type='hidden' name='username' value='$username1' />"
                ?>
            <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
					<?php echo $item_code; echo "<input type='hidden' name='item_code' value='$item_code' />";?>
                </a>
            </td>
            <td>
                <?php 
                    echo strip_tags($item_desc); 
                    echo "<input type='hidden' name='itm_desc' value='$item_desc' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($lastyr_cons,2); 
                    echo "<input type='hidden' name='lastyr_cons' value='$lastyr_cons' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($current_stk,2); 
                    echo "<input type='hidden' name='current_stk' value='$current_stk' />";		  
                ?>
            </td>
            <td>
                <?php
                   echo number_format($incoming_qty_tot,2);
                   echo "<input type='hidden' name='incoming_qty_tot' value='$incoming_qty_tot' />";
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($reservation_qty,2); 
                    echo "<input type='hidden' name='reservation_qty' value='$reservation_qty' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($req_qty,2); 
                    echo "<input type='hidden' name='required_qty' value='$req_qty' />";
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($last_price,2); 
                    echo "<input type='hidden' id='last_price' name='last_price' value='$last_price' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $uom; 
                    echo "<input type='hidden' name='trans_uom' value='$uom' />";
                ?>
            </td>
             <td>
                <?php 
                    echo $conversion_factor; 
                    echo "<input type='hidden' name='conversion_factor' value='$conversion_factor' />";
                ?>
            </td> 
            <td>
                <?php 
                    echo number_format($supplier_avg_lead_time,2); 
                    echo "<input type='hidden' name='supplier_avg_lead_time' value='$supplier_avg_lead_time' />";
                ?>
            </td>
            <?php
				$sql_supp_dtl = "select * from TIPLDB..supplier_dtl where user_id=1 ";
				$query_supp_dtl = $this->db->query($sql_supp_dtl);
				
				 foreach ($query_supp_dtl->result() as $row) {
					$diffdays    = $row->diffDays;
				 }
				
				if($diffdays != NULL){
					if($diffdays > 0){
						$color = "red";
					} else if($diffdays < 0){
						$color = "green";
					} else if($diffdays = 0){
						$color = "blue";
					}
				} else {
					$color = "yellow";
				}
			?>
            <td style=" background-color:<?php echo $color; ?>;">
                <?php
                    echo $need_date;
                    echo "<input type='hidden' name='need_date' value='$need_date' />";
					echo "<input type='hidden' name='color' value='$color' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $wh_code; 
                    echo "<input type='hidden' name='warehse_code' value='$wh_code' />";
                ?>
            </td>
            <input type="hidden" name="po_first_three" id="po_first_three" value="<?php echo $pr_num_first_three; ?>" />
            <td>
            	<?php if($pr_num_first_three != 'CGP'){ ?>
                <select id="cost_available" name="cost_available"  style="width:100px; margin-top:-2px;" class="form-control" onChange="hidecontent1(this.value);">
                    <option value="">select</option>
                    <option value="Yes">Yes</option>
                </select>
                <?php } ?>
            </td>
            <td>
            	<?php if($pr_num_first_three != 'CGP'){ ?>
                <input type="text" name="costing" id="costing" style="display:none; width: 100px;" value="" class="form-control" 
                onkeypress="return isNumberKey(event)" />
                <?php } ?>
            </td>
            <td><select id='currency' name='currency' style='display:none; width:100px; margin-top:-2px;' class = 'form-control'></select></td>
            <td>
            	<?php if($pr_num_first_three != 'CGP'){ ?>
                <input type="text" name="cost_calculation_remarks" id="costing_remarks" style="display:none;  width: 100px;" value="" class="form-control" />
                <?php } ?>
            </td>
            <input type="hidden" name="cost_not_available_remarks" id="costing_not_available_remarks" style="display:none; width: 100px;" class="form-control" />                         
          </tr>
           <?php }  ?> 
        </tbody>
     </table>
  </div>
</div><br /><br />
<?php
	foreach ($v->result() as $row){
		$created_by = $row->preqm_createdby;
		$created_date = $row->preqm_createddate;
		$modified_by = $row->preqm_lastmodifiedby;
		$modified_date = $row->preqm_lastmodifieddate;
?>
<div class="row">
	<div class="col-lg-3">
    	<b>ERP Created By :- <?php echo $created_by; ?><?php echo "<input type='hidden' name ='created_by' value='$created_by' />"; ?></b>
    </div>
    <div class="col-lg-3"><b>ERP Created Date :- <?php echo date("d-m-Y", strtotime($created_date)); ?></b></div>
    <div class="col-lg-3"><b>ERP Modified BY :- <?php echo $created_by; ?></b></div>
    <div class="col-lg-3"><b>ERP Modified Date :- <?php echo date("d-m-Y", strtotime($modified_date)); ?></b></div>
</div><br /><br />
<?php
	break; }
?>
<!-- Required Quantity = Reorder Level + Pending Allocation -->
<?php   
   $sql10 = "select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
   $query10 = $this->db->query($sql10);
  
   if ($query10->num_rows() > 0) {
	  foreach ($query10->result() as $row) {
		  $reorder_level = $row->iou_reorderlevel;
		  $reorder_qty =  $row->iou_reorderqty;
		  echo "<input type='hidden' name='reorder_level' value='$reorder_level' >";
		  echo "<input type='hidden' name='reorder_qty' value='$reorder_qty' >";
		}
   } else {
	      $reorder_level = 0.00;
		  $reorder_qty =  0.00;
		  echo "<input type='hidden' name='reorder_level' value='$reorder_level' >";
		  echo "<input type='hidden' name='reorder_qty' value='$reorder_qty' >";
   }

	//Available Quantity = Accepted Stock + Pending PR, PO Quantity + GRCPT Quantity(Freeze Acceptance Accepted Quantity, Freeze Recipt Recipt Quantity)
	echo "<input type='hidden' name='accepted_stock' value='$accepted_stk' />";
	echo "<input type='hidden' name='po_pr_quantity' value='$incoming_qty_tot' />";
?>

<?php   
   $sql_grcpt = "select * from tipldb..pendalcard_cns_pr where Flag='ItemLastFiveTrans' and ItemCode='$item_code' and user_id = '$user_id'";
   $query_grcpt = $this->db->query($sql_grcpt);
  
   if($query_grcpt->num_rows() > 0){
      $last_recipt_qty_tot = 0;
      $last_accepted_qty_tot = 0;
	  
	  foreach ($query_grcpt->result() as $row) {
		   $status = $row->AllocTranStatus;
		   
		   if($status == 'Freeze Recipt'){
			   $last_recipt_qty = $row->LastTranQty;
		  	   $last_recipt_qty_tot = $last_recipt_qty_tot+$last_recipt_qty;
		   } 
		   
		   if($status == 'Freeze Acceptance'){
			   $last_accepted_qty = $row->CurrYrConsRecptJan;
		  	   $last_accepted_qty_tot = $last_accepted_qty_tot+$last_accepted_qty;
		   }
		}
   }
   echo "<input type='hidden' name='grcpt_recipt_qty' value='$last_recipt_qty_tot' />";
   echo "<input type='hidden' name='grcpt_accepted_qty' value='$last_accepted_qty_tot' />";
?>

<?php if($pr_num_first_three != 'CGP'){ ?>
<div class="row">
  <div class="col-lg-2"><b>Select Usage Type</b><b style="color:#F00">&nbsp;*</b></div>
  <div class="col-lg-2">
    <select class="form-control" id="usage" name="usage" onchange="hidecontent(this.value,'<?php echo $item_code; ?>');" style="font-size:12px;">
		<?php if($reorder_level == '' || $reorder_level == NULL){ ?>
    	<option value="">--Select--</option>
        <option value="Project">Against Project</option>
        <option value="Special">Special</option>
        <option value="Project Pre Order">Project Pre Order</option>
		<?php } else { ?>
	    <option value="">--Select--</option>
        <option value="Against Reorder">Against Reorder</option>
    	<?php } ?>
    </select>
  </div>
  <div class="col-lg-2">
  	<div id="manual_atac_label"><b >LINK ATAC :</b></div> 
  </div> 
  <div class="col-lg-2">
    <select id="manual_atac" name="manual_atac" class="form-control" onchange="hidecontent_new(this.value);" style="font-size:12px;"></select>  
  </div>
  <div class="col-lg-2">
      <div id="category_label"><b>Select Category</b><b style="color:#F00">&nbsp;*</b></div>
  </div>
  
  <div class="col-lg-2">
    <select class="form-control" id="category" name="category" style="font-size:12px;" onchange="spcl_cases_pm(this.value);"></select>
  </div>
</div><br />

<!--- Special PR PM Selection Div --->
<div id="spcl_pm_div" style="display:none;"></div><br />           
<div id="atac_selection_div">           
    <div class="row" id="workorder_div2" style="display:none;">
        <div class="col-lg-2"></div>
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
        	<div id="detail_work_new"></div>
        </div>
    </div><br />
</div>
<div class="row" id="select_workorder" style="display:none;"></div><br />                
<div class="row">
    <div class="col-lg-2"><b>Special Remarks For Supplier</b></div>
    <div class="col-lg-2"><input type="text" name="pr_supp_remarks" id="pr_supp_remarks" value=""  class="form-control"/></div>
    <div class="col-lg-2"><b>Test Certificate Required</b><b style="color:#F00">&nbsp;*</b></div>
    <div class="col-lg-2"><select name="test_cert" id="test_cert" class="form-control" onchange="test_cert_rmks(this.value);"></select></div>
    <div class="col-lg-2"><b id="project_target_date_label">Project Target Date:<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-2"><input type="text" name="project_target_date" id="project_target_date" class="form-control" autocomplete="off"/></div>
</div><br />

<div class="row">
    <div class="col-lg-2"><b>Manufacturing Clearnace</b><b style="color:#F00">&nbsp;*</b></div>
    <div class="col-lg-2"><select id="manufact_clrnce" name="manufact_clrnce" class="form-control"></select></div>
    <div class="col-lg-2"><b>Dispatch Instruction</b><b style="color:#F00">&nbsp;*</b></div>
    <div class="col-lg-2"><select id="dispatch_inst" name="dispatch_inst" class="form-control"></select></div>
    <div class="col-lg-2"><b>Attach Cost Sheet</b></div>
    <div class="col-lg-2">
    	<input type="file" name="cost_sheet"  id="attch_cost_sheet" class="form-control" size = "20" onchange="getExtension1(this.value)"/>
    </div>
</div><br />

<?php
	} else { 
		echo "<input type='hidden' name='usage' value='Capital'>";
		echo "<input type='hidden' name='category' value='Capital Goods'>";
		echo "<input type='hidden' name='test_cert' value='no'>";
		echo "<input type='hidden' name='manufact_clrnce' value='no'>";
		echo "<input type='hidden' name='dispatch_inst' value='no'>";
	}
	//CGPR Condition
?>

<div class="row">
    <div class="col-lg-2"><b>Other Attachment</b></div>
    <div class="col-lg-2">
     	<input type="file" name="other_attachment"  id="other_attachment" class="form-control" size = "20" onchange="getExtension2(this.value)"/>
    </div>
    <div class="col-lg-2"><b>PR Remarks</b></div>
    <div class="col-lg-4"><input type="text" name="remarks" id="remarks" value=""  class="form-control"/></div>
    <div class="col-lg-6"></div>    
</div><br />

<!--- why indent pr in special Remarks ---->
<div class="row">
	<div class="col-lg-2"><b id="why_spcl_label" style="display:none;">Why Indent Special Remarks<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-4"><input type="text" name="why_spcl_rmks" id="why_spcl_rmks" value="" class="form-control" style="display:none;" /></div>
    <div class="col-lg-2"><b id="complaint_label" style="display:none;">Complaint Number</b></div>
    <div class="col-lg-2"><input type="text" name="complain_no" id="complain_no" value="" class="form-control" style="display:none;" /></div>
    <div class="col-lg-2"></div>
</div><br />

<div class="row" style="display:none;" id="tc_rmks_div">
	<div class="col-lg-2"><b>Enter Why TC Not Required<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-4"><input type="text" name="tc_rmks" id="tc_rmks" value="" class="form-control"/></div>
    <div class="col-lg-6"></div>
</div><br />

<!--Supplier-->
<?php 
foreach ($supplier1->result() as $row){
	$supplier_name = $row->SuppName;
	$supplier_rate = number_format($row->SuppRate,2); 
	$supplier_lead_time = $row->SuppLeadTime;
	
	echo "<input type='hidden' name='supplier_name' value='$supplier_name' >";
	echo "<input type='hidden' name='supplier_rate' value='$supplier_rate' >";
	echo "<input type='hidden' name='supplier_lead_time' value='$supplier_lead_time' >";
break; } 
?>

<?php
// $required_qty_total = $reorder_level+$reservation_qty;
$required_qty_total = $reorder_qty+$reservation_qty;
$available_qty_total = $accepted_stk+$incoming_qty_tot+$last_recipt_qty_tot+$last_accepted_qty_tot;
$balance_qty = $required_qty_total - $available_qty_total;

if($req_qty > $balance_qty){
?>
    <div class="row">
        <div class="col-lg-2"><b>Reason For Excess Indent<i style="color:#F00">&nbsp;*</i></b></div>
        <div class="col-lg-2"><textarea id="excess_indt_rmks" name="excess_indt_rmks" class="form-control" required></textarea></div>
    </div><br />
<?php
} else {
	echo "<input type='hidden' id='excess_indt_rmks' name='excess_indt_rmks' value=''>";
}
?>
<!--- Freight Percentage & Duty --->
<?php 
if($reorder_level > 0){ 
	echo "<input type='hidden' id='freight_amt' name='freight_amt' value='0.00' >"; 
	echo "<input type='hidden' id='duty_amt' name='duty_amt' value='0.00' >";
} else { 
?>
<div class="row">
	<?php if($pr_num_first_three == 'FPR'){ ?>
	<div class="col-lg-2"><b>Duty Amount(%)<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-2">
        <select id="duty_amt" name="duty_amt" class="form-control" required>
        	<option value="" selected disabled>--Select--</option>
            <option value="0">0</option>
            <option value="8.25">8.25</option>
            <option value="11">11</option>
            <option value="11">13.75</option>
            <option value="16.5">16.5</option>
            <option value="11">22</option>
        </select>
    </div>
    <?php } else { 
			echo "<input type='hidden' id='duty_amt' name='duty_amt' value='0.00' >"; 
		  } 
	?>
    <div class="col-lg-2"><b>Freight Amount(%)<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-2">
    	<input type="text" id="freight_amt" name="freight_amt" value="" class="form-control" onkeypress="return isNumberKey(event)" required />
    </div>
</div><br />
<?php } ?>

<div class="row">
	<?php if($pr_num_first_three == 'FPR'){ ?>
    <div class="col-lg-2"><b>Transport Mode<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-2">
    	<select id="trans_mode" name="trans_mode" class="form-control" required>
        	<option value="" selected disabled>--Select--</option>
            <option value="Air">Air</option>
            <option value="Sea">Sea</option>
            <option value="Road">Road</option>
        </select>
    </div>
    <?php } else {
			echo "<input type='hidden' id='trans_mode' name='trans_mode' value='Road' >"; 
    	  } 
    ?>
    <div class="col-lg-2"><b>Item Lead Time (In Days)<i style="color:#F00">&nbsp;*</i></b></div>
    <div class="col-lg-2">
    	<input type="text" id="item_lead_time" name="item_lead_time" class="form-control" value="" onkeypress="return isNumberKey(event)" required />
    </div>
</div><br /><br />
<?php
	if($count > 1){	
		echo "<br /><h3 style='color:red; text-align:center'>One Purchase Request Contains Only One Item.</h3><br />";	
	} else {
?>

<?php
//LPR LOCK
$pr_num_lpr = substr($pr_num,0,3);
if($pr_num_lpr != 'LPR'){
	
//Reorder Cycle Lock 
$sql_reorder_cyc = "SELECT DATEDIFF(DD,ERP_UPDATE_DATE,GETDATE()) as reorder_time 
FROM TIPLDB..reorder_chat_history 
WHERE sno = (
	SELECT TOP 1 sno FROM TIPLDB..reorder_chat_history 
	WHERE item_code = '$item_code' 
	AND instruction = 'Updated In ERP'
	ORDER BY sno DESC
)
AND DATEDIFF(DD,ERP_UPDATE_DATE,GETDATE()) < 92";

$qry_reorder_cyc = $this->db->query($sql_reorder_cyc);
$reorder_cycle_flag = 0;

foreach($qry_reorder_cyc->result() as $row){
	$reorder_time = $row->reorder_time;
	
	if($reorder_time != '' && $reorder_time < 92 ){
		$reorder_cycle_flag++;
	}
}

if($reorder_level > 0 && $reorder_cycle_flag == 0){
	echo "<h3 style='color:red; text-align:center'>Please run Reorder Update cycle First</h3>";
} else {

?>
<?php
	//Warehouse Lock
	if($pr_num_lpr == 'IPR' && $wh_code == 'IC-EXWH'){
		echo "<br /><h3 style='color:red; text-align:center'>Warehouse (IC-EXWH) Not Allowed In Indian Purchase Requests.</h3><br />";
	} else {
?>            
<div class="row">
    <div class="col-lg-5"></div>
    <div class="col-lg-2"> 
        <input type="submit" name="submt" value="SUBMIT" class="form-control" id="submit" style="font-weight:bold; background:#000000; color:#FFFFFF;"/>     
    </div>
    <div class="col-lg-5"></div>   
</div>
<?php } //Warehouse Lock Ends ?>
<?php } //Reorder Lock Ends ?>
<?php
} else {
	echo "<br><h3 style='color:red; text-align:center;'>Please Create FPR Or IPR instead of LPR</h3><br>";
}
//LPR Lock Ends 
} 
?>
</form>

<!-- CHAT HISTORY -->
<div class="row">
	<div class="col-lg-12">
    	<h4>Chat History</h4>
    	<table class="table table-bordered" cellpadding="0" cellspacing="0" align="center" border="1">
			<tr style="background-color:#CCC; font-weight:bold;">
            	<td>LEVEL</td>
                <td>NAME</td>
                <td>COMMENT</td>
                <td>INSTRUCTION</td>
                <td>DATE TIME</td>
            </tr>
            <?php
				foreach ($chat_history->result() as $row){
					$level = $row->level;
					$name = $row->comment_by;
					$comment = $row->comment;
					$instruction = $row->instruction;
					$date_time = $row->datetime;
			?>
            <tr>
            	<td><?php echo $level; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $comment; ?></td>
                <td><?php echo $instruction; ?></td>
                <td><?php echo $date_time; ?></td>
            </tr>  
            <?php } ?>    
        </table>
    </div>
</div>

<!-- CHAT HISTORY -->         
<!--************* PENDAL CARD INFORMATION **********-->
<!--*********** Current Year Consumption & Receipt **********-->
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">Month Wise Current Year Consumption &amp; Receipt</div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>TYPE‎</th>
                <th>APR‎</th>
                <th>MAY‎‎</th>
                <th>JUN</th>
                <th>JUL</th>
                <th>AUG‎</th>
                <th>SEP‎</th>
                <th>OCT‎</th>
                <th>NOV‎</th>
                <th>DEC</th>
                <th>JAN‎</th>
                <th>FEB</th>
                <th>MAR</th>
                <th>TOTAL‎</th>
                <th>AVERAGE</th>
              </tr>
            </thead>
            <tbody>
			  <?php 
              foreach ($mnthwsecurryrcons1->result() as $row){
              $fin_yr  = $row->SuppName; 
              $apr 	 = $row->CurrYrConsRecptApril;
              $apr_new = number_format($row->CurrYrConsRecptApril,2);
              $may     = $row->CurrYrConsRecptMay;
              $may_new = number_format($row->CurrYrConsRecptMay,2);
              $jun     = $row->CurrYrConsRecptJune;
              $jun_new = number_format($row->CurrYrConsRecptJune,2);
              $jul     = $row->CurrYrConsRecptJuly;
              $jul_new = number_format($row->CurrYrConsRecptJuly,2);
              $aug     = $row->CurrYrConsRecptAugust;
              $aug_new = number_format($row->CurrYrConsRecptAugust,2);
              $sep     = $row->CurrYrConsRecptSeptember;
              $sep_new = number_format($row->CurrYrConsRecptSeptember,2);
              $oct     = $row->CurrYrConsRecptOctober;
              $oct_new = number_format($row->CurrYrConsRecptOctober,2);
              $nov     = $row->CurrYrConsRecptNovember;
              $nov_new = number_format($row->CurrYrConsRecptNovember,2);
              $dec     = $row->CurrYrConsRecptDecember;
              $dec_new = number_format($row->CurrYrConsRecptDecember,2);
              $jan     = $row->CurrYrConsRecptJan;
              $jan_new = number_format($row->CurrYrConsRecptJan,2);
              $feb     = $row->CurrYrConsRecptFeb;
              $feb_new = number_format($row->CurrYrConsRecptFeb,2);
              $mar     = $row->CurrYrConsRecptMarch;
              $mar_new = number_format($row->CurrYrConsRecptMarch,2);
			  
			  if($apr == '' || $apr == NULL || $apr == 0){
				  $div_apr = 0;
			  } else {
				  $div_apr = 1;
			  }
			  
			  if($may == '' || $may == NULL || $may == 0){
				  $div_may = 0;
			  } else {
				  $div_may = 1;
			  }
			  
			  if($jun == '' || $jun == NULL || $jun == 0){
				  $div_jun = 0;
			  } else {
				  $div_jun = 1;
			  }
			  
			  if($jul == '' || $jul == NULL || $jul == 0){
				  $div_jul = 0;
			  } else {
				  $div_jul = 1;
			  }
			  
			  if($aug == '' || $aug == NULL || $aug == 0){
				  $div_aug = 0;
			  } else {
				  $div_aug = 1;
			  }
			  
			  if($sep == '' || $sep == NULL || $sep == 0){
				  $div_sep = 0;
			  } else {
				  $div_sep = 1;
			  }
			  
			  if($oct == '' || $oct == NULL || $oct == 0){
				  $div_oct = 0;
			  } else {
				  $div_oct = 1;
			  }
			  
			  if($nov == '' || $nov == NULL || $nov == 0){
				  $div_nov = 0;
			  } else {
				  $div_nov = 1;
			  }
			  
			  if($dec == '' || $dec == NULL || $dec == 0){
				  $div_dec = 0;
			  } else {
				  $div_dec = 1;
			  }
			  
			  if($jan == '' || $jan == NULL || $jan == 0){
				  $div_jan = 0;
			  } else {
				  $div_jan = 1;
			  }
			  
			  if($feb == '' || $feb == NULL || $feb == 0){
				  $div_feb = 0;
			  } else {
				  $div_feb = 1;
			  }
			  
			  if($mar == '' || $mar == NULL || $mar == 0){
				  $div_mar = 0;
			  } else {
				  $div_mar = 1;
			  }
			  
              $tot     = $apr+$may+$jun+$jul+$aug+$sep+$oct+$nov+$dec+$jan+$feb+$mar;
			  $div_tot = $div_apr+$div_may+$div_jun+$div_jul+$div_aug+$div_sep+$div_oct+$div_nov+$div_dec+$div_jan+$div_feb+$div_mar;
              $tot_new = number_format($tot,2);
              $avg     = $tot/$div_tot;
              $avg_new = number_format($avg,2);
              ?>
              <tr>
                <td>Consumption ( <?php echo $fin_yr; ?> )</td>
                <td><?php echo $apr_new; ?></td>
                <td><?php echo $may_new; ?></td>
                <td><?php echo $jun_new; ?></td>
                <td><?php echo $jul_new; ?></td>
                <td><?php echo $aug_new; ?></td>
                <td><?php echo $sep_new; ?></td>
                <td><?php echo $oct_new; ?></td>
                <td><?php echo $nov_new; ?></td>
                <td><?php echo $dec_new; ?></td>
                <td><?php echo $jan_new; ?></td>
                <td><?php echo $feb_new; ?></td>
                <td><?php echo $mar_new; ?></td>
                <td><?php echo $tot_new; ?></td>
                <td><?php echo $avg_new; ?></td>                          
              </tr>
              <?php } ?>
              <?php 
			  foreach ($mnthwsecurryrrcpt1->result() as $row){
			  $fin_yr1  = $row->SuppName; 
              $apr1     = $row->CurrYrConsRecptApril;
              $apr1_new = number_format($row->CurrYrConsRecptApril,2);
              $may1     = $row->CurrYrConsRecptMay;
              $may1_new = number_format($row->CurrYrConsRecptMay,2);
              $jun1     = $row->CurrYrConsRecptJune;
              $jun1_new = number_format($row->CurrYrConsRecptJune,2);
              $jul1     = $row->CurrYrConsRecptJuly;
              $jul1_new = number_format($row->CurrYrConsRecptJuly,2);
              $aug1     = $row->CurrYrConsRecptAugust;
              $aug1_new = number_format($row->CurrYrConsRecptAugust,2);
              $sep1     = $row->CurrYrConsRecptSeptember;
              $sep1_new = number_format($row->CurrYrConsRecptSeptember,2);
              $oct1     = $row->CurrYrConsRecptOctober;
              $oct1_new = number_format($row->CurrYrConsRecptOctober,2);
              $nov1     = $row->CurrYrConsRecptNovember;
              $nov1_new = number_format($row->CurrYrConsRecptNovember,2);
              $dec1     = $row->CurrYrConsRecptDecember;
              $dec1_new = number_format($row->CurrYrConsRecptDecember,2);
              $jan1     = $row->CurrYrConsRecptJan;
              $jan1_new = number_format($row->CurrYrConsRecptJan,2);
              $feb1     = $row->CurrYrConsRecptFeb;
              $feb1_new = number_format($row->CurrYrConsRecptFeb,2);
              $mar1     = $row->CurrYrConsRecptMarch;
              $mar1_new = number_format($row->CurrYrConsRecptMarch,2);
			  
			  if($apr1 == '' || $apr1 == NULL || $apr1 == 0){
				  $div_apr1 = 0;
			  } else {
				  $div_apr1 = 1;
			  }
			  
			  if($may1 == '' || $may1 == NULL || $may1 == 0){
				  $div_may1 = 0;
			  } else {
				  $div_may1 = 1;
			  }
			  
			  if($jun1 == '' || $jun1 == NULL || $jun1 == 0){
				  $div_jun1 = 0;
			  } else {
				  $div_jun1 = 1;
			  }
			  
			  if($jul1 == '' || $jul1 == NULL || $jul1 == 0){
				  $div_jul1 = 0;
			  } else {
				  $div_jul1 = 1;
			  }
			  
			  if($aug1 == '' || $aug1 == NULL || $aug1 == 0){
				  $div_aug1 = 0;
			  } else {
				  $div_aug1 = 1;
			  }
			  
			  if($sep1 == '' || $sep1 == NULL || $sep1 == 0){
				  $div_sep1 = 0;
			  } else {
				  $div_sep1 = 1;
			  }
			  
			  if($oct1 == '' || $oct1 == NULL || $oct1 == 0){
				  $div_oct1 = 0;
			  } else {
				  $div_oct1 = 1;
			  }
			  
			  if($nov1 == '' || $nov1 == NULL || $nov1 == 0){
				  $div_nov1 = 0;
			  } else {
				  $div_nov1 = 1;
			  }
			  
			  if($dec1 == '' || $dec1 == NULL || $dec1 == 0){
				  $div_dec1 = 0;
			  } else {
				  $div_dec1 = 1;
			  }
			  
			  if($jan1 == '' || $jan1 == NULL || $jan1 == 0){
				  $div_jan1 = 0;
			  } else {
				  $div_jan1 = 1;
			  }
			  
			  if($feb1 == '' || $feb1 == NULL || $feb1 == 0){
				  $div_feb1 = 0;
			  } else {
				  $div_feb1 = 1;
			  }
			  
			  if($mar1 == '' || $mar1 == NULL || $mar1 == 0){
				  $div_mar1 = 0;
			  } else {
				  $div_mar1 = 1;
			  }
			  
              $tot1     = $apr1+$may1+$jun1+$jul1+$aug1+$sep1+$oct1+$nov1+$dec1+$jan1+$feb1+$mar1;
			  $div_tot1 = $div_apr1+$div_may1+$div_jun1+$div_jul1+$div_aug1+$div_sep1+$div_oct1+$div_nov1+$div_dec1+$div_jan1+$div_feb1+$div_mar1;
              $tot1_new = number_format($tot1,2);
              $avg1     = $tot1/$div_tot1;
              $avg1_new = number_format($avg1,2);
              ?>
              <tr>
                <td>Receipt ( <?php echo $fin_yr1; ?> ) </td>
                <td><?php echo $apr1_new; ?></td>
                <td><?php echo $may1_new; ?></td>
                <td><?php echo $jun1_new; ?></td>
                <td><?php echo $jul1_new; ?></td>
                <td><?php echo $aug1_new; ?></td>
                <td><?php echo $sep1_new; ?></td>
                <td><?php echo $oct1_new; ?></td>
                <td><?php echo $nov1_new; ?></td>
                <td><?php echo $dec1_new; ?></td>
                <td><?php echo $jan1_new; ?></td>
                <td><?php echo $feb1_new; ?></td>
                <td><?php echo $mar1_new; ?></td>
                <td><?php echo $tot1_new; ?></td>
                <td><?php echo $avg1_new; ?></td>                          
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />
            
<!--********* Yearly Consumption Of Past 1 Years *******-->
          
<!--***** RECORDER LEVEL RECORDER QUANTITY ******-->
          
<div class="row">
    <div class="col-lg-6" style="font-weight:bold; font-size:15px;">Last year Consumption‎ &amp; Receipt</div>
    <div class="col-lg-6" style="font-weight:bold; font-size:15px;">Reorder Level Reorder Quantity</div>
</div>

<?php
	$curr_date_year = date("Y-m-d");
	$curr_fin_year = substr($curr_date_year,2,2);
	$last_fin_year = $curr_fin_year - 1;
	$last_fin_yr = "( FY-".$last_fin_year."-".$curr_fin_year." )";
?>
          		
 <div class="row">
    <div class="col-lg-6">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Ty‎pe‎</th>
                <th>Qty‎.‎</th>
                <th>Value‎</th>
              </tr>
            </thead>
            <tbody>
              <?php 
			  foreach ($lstyrcumnrec1->result() as $row) {
				  $cons_tot_qty = $row->ConsTotalQty;
				  $cons_tot_val = $row->ConsTotalVal;
				  $recpt_qty    = $row->RecptTotalQty;
				  $recpt_val    = $row->RecptTotalVal;
			  ?>
              <tr>
                <td>Consumption <?php echo $last_fin_yr; ?></td>
                <td><?php echo number_format($cons_tot_qty,2); ?></td>
                <td><?php echo number_format($cons_tot_val,2); ?></td>                          
              </tr>
              <tr>
                <td>Receipt <?php echo $last_fin_yr; ?> </td>
                <td><?php echo number_format($recpt_qty,2); ?></td>
                <td><?php echo number_format($recpt_val,2); ?></td>                          
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
    
    <div class="col-lg-6">
      <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>REORDER LEVEL</th>
                <th>REORDER QUANTITY</th>
              </tr>
            </thead>
            <?php   
			foreach ($reorder->result() as $row) { 
				$reorder_level =  $row->iou_reorderlevel;
				$reorder_qty =  $row->iou_reorderqty;
			?>
            <tbody>
              <tr>
                <td><?php echo number_format($reorder_level, 2); ?></td><!--RECORDER LEVEL-->
                <td><?php echo number_format($reorder_qty, 2); ?></td><!--RECORDER QUANTITY-->                         
              </tr>
            </tbody>
            <?php } ?>
      </table>
    </div>
 </div>
            
<!--******* Last 5 Purchases With Rate, Supplier, City, Lead, Time(days) *********-->        
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">Last Five Purchases</div>      
</div> 		
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1" style="font-size:10px">
            <thead>
              <tr>
                <th>PO‎/‎SCO‎/WO No‎</th>
                <th>GRCPT‎/‎Productio‎n Receipt</th>
                <th>Status</th>
                <th>Rate</th>
                <th>Supplier Name‎</th>
                <th>PR‎/‎SCR No‎</th>
                <th>Creation of PR‎/‎SCR Date</th>
                <th>Need ‎Date‎</th>
                <th>GRCPT ‎(‎Freeze & ‎Move Date‎)</th>
                <th>Lead ‎Time‎</th>
                <th>Receipt Qty‎.</th>
                <th>Accept‎ed Qty‎.</th>
                <th>Rejected Qty‎.</th>
              </tr>
            </thead>
            <tbody>
              <?php 
				foreach ($lastfivetrans1->result() as $row){ 
					$po_sco_wo_no = $row->LastPOGRTran;
					$grcpt_prod_recipt = $row->LastReceipt;
					$status = $row->AllocTranStatus;
					$rate = number_format($row->LastRate,2);
					$supp_name = $row->LastSupplier;
					$pr_scr_no = $row->LastPRSCRTran;
					$pr_scr_createdate1 = substr($row->LastPRSCRDate,0,10);
					$pr_scr_createdate = date("d-m-Y", strtotime($pr_scr_createdate1));
					$pr_need_date1 = substr($row->LastPRSCRNeedDt,0,10);
					$pr_need_date = date("d-m-Y", strtotime($pr_need_date1));
					$grcpt_freeze_move_dt1 = substr($row->LastMoveDt,0,10);
					$grcpt_freeze_move_dt = date("d-m-Y", strtotime($grcpt_freeze_move_dt1));
					$lead_time = $row->LastLeadTime;
					$receipt_qty = round($row->LastTranQty,2);
					$accepted_qty = round($row->CurrYrConsRecptJan,2);
					$rejected_qty = round($row->CurrYrConsRecptFeb,2);
              ?>
              <tr>
                <td><?php echo $po_sco_wo_no; ?></td>
                <td><?php echo $grcpt_prod_recipt; ?></td>
                <td><?php echo $status; ?></td>
                <td><?php echo $rate; ?></td>
                <td><?php echo $supp_name; ?></td>
                <td><?php echo $pr_scr_no; ?></td>
                <td><?php echo $pr_scr_createdate; ?></td>
                <td><?php echo $pr_need_date; ?></td> 
                <td><?php echo $grcpt_freeze_move_dt; ?></td>
                <td><?php echo $lead_time; ?></td>
                <td><?php echo number_format($receipt_qty,2); ?></td>
                <td><?php echo number_format($accepted_qty,2); ?></td>
                <td><?php echo number_format($rejected_qty,2); ?></td>                            
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />

<!--******* SUPPLIER *********-->
<div class="row">
	<div class="col-lg-6" style="font-weight:bold; font-size:15px;">
    	Supplier
    </div>
    <div class="col-lg-6">
    </div>
</div>
<div class="row">
	<div class="col-lg-6">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Suppliers‎‎‎</th>
                <th>Purchase Rate‎‎</th>
                <th>Lead Time‎‎‎</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($supplier1->result() as $row) { ?>
              <tr>
                <td><?php echo $row->SuppName; ?></td>
                <td><?php echo number_format($row->SuppRate,2); ?></td>
                <td><?php echo $row->SuppLeadTime; ?></td>                       
              </tr>
              <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-lg-6">
    </div>
</div><br />

<!--******* USED IN BOM DETAIL *********-->
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">Used In BOM Detail</div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Product ‎Structure No</th>
                <th>Output Item‎‎</th>
                <th>Item‎ desc.‎‎</th>
                <th>Required Qty‎</th>
                <th>Warehouse‎</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($bomdetail1->result() as $row){ ?>
              <tr>
                <td><?php echo $row->PSNo; ?></td>
                <td><?php echo $row->FgCode; ?></td>
                <td>
				<?php 
					$FgCodeDesc = $row->FgCodeDesc;
					$fgdec = explode(':',$FgCodeDesc);
					
					if ($fgdec[0] == 'LOC'){
						$dec = $fgdec[1]; }
					else {
						$dec = $fgdec[0]; 
					}
					
					echo $dec; 
				?>
                </td>
                <td><?php echo number_format($row->BomItemQty,2); ?></td>
                <td><?php echo $row->BomWhCode; ?></td>                         
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />  

<?php //Action Timing Report ?>
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">Action Timing Report</div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>PR Created In ERP By</th>
                <th>PR Created In ERP Date‎‎</th>
                <th>PR Modified In ERP By</th>
                <th>PR Modified In ERP Date‎‎</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($prerp->result() as $row) { ?>
              <tr>
                <td><?php echo $row->preqm_createdby; ?></td>
                <td><?php echo $row->preqm_createddate; ?></td>
                <td><?php echo $row->preqm_lastmodifiedby; ?></td>
                <td><?php echo $row->preqm_lastmodifieddate; ?></td>                         
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br /> 
<?php //Action Timing Report ?>