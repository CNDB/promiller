<?php include'header.php'; ?>
 
<?php $this->load->helper('mrp_helper'); ?>
<!--main content start-->
<!--********* ITEM INFORMATION *********-->

<?php 
//Dates
$fun      = $_GET['fun'];
$category = $_GET['cat'];
$erp_cat  = $_GET['erp_cat'];
if($fun != "avg_open_items_det"){
	$fd       = $_GET['fd'];
	$td       = $_GET['td'];
}

?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEMS IN MRP HISTORY</h4>
        </div>
    </div><br />
    
    
    <div class="row">
    	<div class="col-lg-1"></div>
    	<div class="col-lg-10">
        	<table align="center" cellpadding="1" cellspacing="1" width="100%" class="table table-bordered">
            	<tr style="background-color:#CCC">
               		<td><b>SNO</b></td>
                    <td><b>ITEM CODE</b></td>
                    <td><b>ITEM DESC</b></td>
                    <td><b>ERP CAT CODE</b></td>
                    <td><b>CATEGORY</b></td>
                    <td><b>IN MRP DATE</b></td>
                    <td><b>LAST VISIBLE DATE</b></td>
                    <td><b>AGE</b></td>
               </tr>
               <?php
			   if($fun == "avg_open_items_det"){
			   		$fun($erp_cat,$category);
			   } else {
				    $fun($erp_cat,$category,$fd,$td);
			   }
			   ?>
            </table>
        </div>
        <div class="col-lg-1"></div>
    </div>
  </section>
</section>   
      
<?php include('footer.php'); ?>