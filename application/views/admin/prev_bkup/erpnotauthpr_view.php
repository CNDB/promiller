<?php
	include'header.php';
	
    $pr_num = $this->uri->segment(3);
	$username_pr = $_SESSION['username'];
?>
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PURCHASE REQUEST NOT AUTHORIZED IN ERP</h4>
        </div>
    </div><br />

<form action="#" method="post" enctype="multipart/form-data" onSubmit="return reqd()">

<? include('pr_item_details.php'); ?>

<? //live fetched Details ?>
<table class="table table-bordered">

<?php
	foreach ($v_auth->result() as $row){
		$created_by = $row->preqm_createdby;
		$created_date = $row->preqm_createddate;
		$modified_by = $row->preqm_lastmodifiedby;
		$modified_date = $row->preqm_lastmodifieddate;
		$live_created_by = $row->created_by;
		$live_created_date = $row->create_date;
?>
    <tr>
        <td><b>ERP Created By : <?php echo $created_by; ?></b></td>
        <td><b>ERP Created Date : <?php echo date("d-m-Y", strtotime($created_date)); ?></b></td>
        <td><b>ERP Modified BY : <?php echo $created_by; ?></b></td>
        <td><b>ERP Modified Date : <?php echo date("d-m-Y", strtotime($modified_date)); ?></b></td>
        <td><b>LIVE Created BY : <?php echo $live_created_by; ?></b></td>
        <td><b>LIVE Created Date : <?php echo date("d-m-Y", strtotime($live_created_date)); ?></b></td>
    </tr>
<?php
	break; }
?>

<?php  /*reorder_level*/  ?>
<?php   
$sql10 = "select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
$query10 = $this->db->query($sql10);

if ($query10->num_rows() > 0) {
  foreach ($query10->result() as $row) {
	  $reorder_level = $row->iou_reorderlevel;
	  $reorder_qty =  $row->iou_reorderqty;
	  echo "<input type='hidden' name='reorder_level' value='$reorder_level' >";
	  echo "<input type='hidden' name='reorder_qty' value='$reorder_qty' >";
	}
} else {
	  $reorder_level = 0.00;
	  $reorder_qty =  0.00;
	  echo "<input type='hidden' name='reorder_level' value='$reorder_level' >";
	  echo "<input type='hidden' name='reorder_qty' value='$reorder_qty' >";
}
?>

<?php 
  	foreach ($v_auth->result() as $row){
		
		$usage = $row->usage;
		$category = $row->category;
		$workorder_no = $row->work_odr_no;
		$so_no = $row->sono;
		$customer_name = $row->customer_name;
		$drawing_no = $row->drawing_no;
		$pm_group = $row->pm_group;
		$project_name = $row->project_name;
		$atac_no = $row->atac_no;
		$atac_ld_date = $row->atac_ld_date;
		$atac_need_date = $row->atac_need_date;
		$atac_payment_terms = $row->atac_payment_terms;
		$pr_remarks = $row->remarks;
		$special_inst_supp = $row->pr_supp_remarks;
		$project_target_date = $row->project_target_date;
		//check empty project target date
		if($project_target_date == '1900-01-01'){
			$project_target_date = '';
		}
		
		$test_cert_req = $row->test_cert;
		$manufact_clearance = $row->manufact_clearance;
		$dispatch_inst = $row->dispatch_inst;
		$attached_cost_sheet = $row->attch_cost_sheet;
		$attached_drawing = $row->attach_drawing;
		$excess_indt_rmks = $row->excess_indt_rmks;
		
	}
?>

<?php 
foreach ($supplier1->result() as $row){
	$supplier_name = $row->SuppName;
	$supplier_rate = number_format($row->SuppRate,2); 
	$supplier_lead_time = $row->SuppLeadTime;
	
	echo "<input type='hidden' name='supplier_name' value='$supplier_name' >";
	echo "<input type='hidden' name='supplier_rate' value='$supplier_rate' >";
	echo "<input type='hidden' name='supplier_lead_time' value='$supplier_lead_time' >";
break; } 
?>

    <tr>
        <td><b>Usage Type : </b><?php echo $usage; ?></td>
        <td><b>Category : </b><?php echo $category; ?><?php echo "<input type='hidden' name='category' value='$category'>"; ?></td>
        <td><b>WO Number : </b><?php echo $workorder_no; ?></td>
        <td><b>SO Number : </b><?php echo $so_no; ?></td>
        <td><b>Customer Name : </b><?php echo $customer_name; ?></td>
        <td><b>Drawing Number : </b><?php echo $drawing_no; ?></td>
    </tr>
    <tr>
        <td><b>PM Group : </b><?php echo $pm_group; ?><?php echo "<input type='hidden' name='pm_group' value='$pm_group'>"; ?></td>
        <td><b>Project Name : </b><?php echo $project_name; ?><?php echo "<input type='hidden' name='pro_ordr_name' value='$project_name'>"; ?></td>
        <td><b>ATAC Number : </b><?php echo $atac_no; ?></td>
        <td><b>ATAC LD Date : </b><?php echo $atac_ld_date; ?></td>
        <td><b>ATAC Need Date : </b><?php echo $atac_need_date; ?></td>
        <td><b>ATAC Payment Terms : </b><?php echo $atac_payment_terms; ?></td>
    </tr>
    <tr>
        <td><b>PR Remarks : </b><?php echo $pr_remarks; ?></td>
        <td><b>Supplier Spcl Remarks : </b><?php echo $special_inst_supp; ?></td>
        <td><b>Project Target Date : </b><?php echo $project_target_date; ?></td>
        <td><b>Test Cert Req : </b><?php echo $test_cert_req; ?></td>
        <td><b>Manufact Clearance : </b><?php echo $manufact_clearance; ?></td>
        <td><b>Dispatch Inst : </b><?php echo $dispatch_inst; ?> </td>
    </tr>
    <tr>
        <td><b>Attached Cost Sheet : </b><?php echo $attached_cost_sheet; ?></td>
        <td><b>Attached Drawing : </b><?php echo $attached_drawing; ?></td>
        <td colspan="4">
        	<b>Excess Indent Remarks : </b>
            <?php echo $excess_indt_rmks; ?>
        </td>
    </tr>

</table>

<? //live fetched Details ?>

</form>

<?php include('pr_footer.php'); ?>
