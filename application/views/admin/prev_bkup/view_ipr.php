 <?php
	include'header.php';
	
    $pr_num = $this->uri->segment(3);
	$username_pr = strtolower($_SESSION['username']);
?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">IPR DETAILS</h4>
        </div>
    </div><br />

    <?php include('pr_item_details.php'); ?>
    
     <?php include('po_details_pr.php'); ?>
    
    <?php include('pr_live_filled_details.php'); ?>
 
    <!-- CHAT HISTORY -->
    <div class="row">
        <div class="col-lg-12">
            <h4>Chat History</h4>
            <table class="table table-bordered" cellpadding="0" cellspacing="0" align="center" border="1">
                <tr style="background-color:#CCC; font-weight:bold;">
                    <td>LEVEL</td>
                    <td>NAME</td>
                    <td>COMMENT</td>
                    <td>INSTRUCTION</td>
                    <td>DATE TIME</td>
                </tr>
                <?php
                    foreach ($chat_history->result() as $row){
                        $level = $row->level;
                        $name = $row->comment_by;
                        $comment = $row->comment;
                        $instruction = $row->instruction;
                        $date_time = $row->datetime;
                ?>
                <tr>
                    <td><?php echo $level; ?></td>
                    <td><?php echo $name; ?></td>
                    <td><?php echo $comment; ?></td>
                    <td><?php echo $instruction; ?></td>
                    <td><?php echo $date_time; ?></td>
                </tr>  
                <?php } ?>    
            </table>
        </div>
    </div>
    <!-- CHAT HISTORY -->

	<!-- PENDAL CARD DETAILS -->

	<div class="row">
      <div class="col-lg-12">
        <h3>PENDAL CARD DETAILS</h3>
        <hr/>
      </div>
    </div><br />
    
<!--*********** CURRENT YEAR CONSUMPTION AND RECEIPT **********-->
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
        Month Wise Current Year Consumption &amp; Receipt
    </div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>TYPE</th>
                <th>APR</th>
                <th>MAY</th>
                <th>JUN</th>
                <th>JUL</th>
                <th>AUG</th>
                <th>SEP</th>
                <th>OCT</th>
                <th>NOV</th>
                <th>DEC</th>
                <th>JAN</th>
                <th>FEB</th>
                <th>MAR</th>
                <th>TOTAL</th>
                <th>AVERAGE</th>
              </tr>
            </thead>
            <tbody>
			  <?php 
              foreach ($mnthwsecurryrcons1->result() as $row){
              $fin_yr  = $row->SuppName; 
              $apr 	 = $row->CurrYrConsRecptApril;
              $apr_new = number_format($row->CurrYrConsRecptApril,2);
              $may     = $row->CurrYrConsRecptMay;
              $may_new = number_format($row->CurrYrConsRecptMay,2);
              $jun     = $row->CurrYrConsRecptJune;
              $jun_new = number_format($row->CurrYrConsRecptJune,2);
              $jul     = $row->CurrYrConsRecptJuly;
              $jul_new = number_format($row->CurrYrConsRecptJuly,2);
              $aug     = $row->CurrYrConsRecptAugust;
              $aug_new = number_format($row->CurrYrConsRecptAugust,2);
              $sep     = $row->CurrYrConsRecptSeptember;
              $sep_new = number_format($row->CurrYrConsRecptSeptember,2);
              $oct     = $row->CurrYrConsRecptOctober;
              $oct_new = number_format($row->CurrYrConsRecptOctober,2);
              $nov     = $row->CurrYrConsRecptNovember;
              $nov_new = number_format($row->CurrYrConsRecptNovember,2);
              $dec     = $row->CurrYrConsRecptDecember;
              $dec_new = number_format($row->CurrYrConsRecptDecember,2);
              $jan     = $row->CurrYrConsRecptJan;
              $jan_new = number_format($row->CurrYrConsRecptJan,2);
              $feb     = $row->CurrYrConsRecptFeb;
              $feb_new = number_format($row->CurrYrConsRecptFeb,2);
              $mar     = $row->CurrYrConsRecptMarch;
              $mar_new = number_format($row->CurrYrConsRecptMarch,2);
			  
			  if($apr == '' || $apr == NULL || $apr == 0){
				  $div_apr = 0;
			  } else {
				  $div_apr = 1;
			  }
			  
			  if($may == '' || $may == NULL || $may == 0){
				  $div_may = 0;
			  } else {
				  $div_may = 1;
			  }
			  
			  if($jun == '' || $jun == NULL || $jun == 0){
				  $div_jun = 0;
			  } else {
				  $div_jun = 1;
			  }
			  
			  if($jul == '' || $jul == NULL || $jul == 0){
				  $div_jul = 0;
			  } else {
				  $div_jul = 1;
			  }
			  
			  if($aug == '' || $aug == NULL || $aug == 0){
				  $div_aug = 0;
			  } else {
				  $div_aug = 1;
			  }
			  
			  if($sep == '' || $sep == NULL || $sep == 0){
				  $div_sep = 0;
			  } else {
				  $div_sep = 1;
			  }
			  
			  if($oct == '' || $oct == NULL || $oct == 0){
				  $div_oct = 0;
			  } else {
				  $div_oct = 1;
			  }
			  
			  if($nov == '' || $nov == NULL || $nov == 0){
				  $div_nov = 0;
			  } else {
				  $div_nov = 1;
			  }
			  
			  if($dec == '' || $dec == NULL || $dec == 0){
				  $div_dec = 0;
			  } else {
				  $div_dec = 1;
			  }
			  
			  if($jan == '' || $jan == NULL || $jan == 0){
				  $div_jan = 0;
			  } else {
				  $div_jan = 1;
			  }
			  
			  if($feb == '' || $feb == NULL || $feb == 0){
				  $div_feb = 0;
			  } else {
				  $div_feb = 1;
			  }
			  
			  if($mar == '' || $mar == NULL || $mar == 0){
				  $div_mar = 0;
			  } else {
				  $div_mar = 1;
			  }
			  
              $tot     = $apr+$may+$jun+$jul+$aug+$sep+$oct+$nov+$dec+$jan+$feb+$mar;
			  $div_tot = $div_apr+$div_may+$div_jun+$div_jul+$div_aug+$div_sep+$div_oct+$div_nov+$div_dec+$div_jan+$div_feb+$div_mar;
              $tot_new = number_format($tot,2);
              $avg     = $tot/$div_tot;
              $avg_new = number_format($avg,2);
              ?>
              <tr>
                <td>Consumption ( <?php echo $fin_yr; ?> )</td>
                <td><?php echo $apr_new; ?></td>
                <td><?php echo $may_new; ?></td>
                <td><?php echo $jun_new; ?></td>
                <td><?php echo $jul_new; ?></td>
                <td><?php echo $aug_new; ?></td>
                <td><?php echo $sep_new; ?></td>
                <td><?php echo $oct_new; ?></td>
                <td><?php echo $nov_new; ?></td>
                <td><?php echo $dec_new; ?></td>
                <td><?php echo $jan_new; ?></td>
                <td><?php echo $feb_new; ?></td>
                <td><?php echo $mar_new; ?></td>
                <td><?php echo $tot_new; ?></td>
                <td><?php echo $avg_new; ?></td>                          
              </tr>
              <?php } ?>
              <?php 
			  foreach ($mnthwsecurryrrcpt1->result() as $row){
			  $fin_yr1  = $row->SuppName; 
              $apr1     = $row->CurrYrConsRecptApril;
              $apr1_new = number_format($row->CurrYrConsRecptApril,2);
              $may1     = $row->CurrYrConsRecptMay;
              $may1_new = number_format($row->CurrYrConsRecptMay,2);
              $jun1     = $row->CurrYrConsRecptJune;
              $jun1_new = number_format($row->CurrYrConsRecptJune,2);
              $jul1     = $row->CurrYrConsRecptJuly;
              $jul1_new = number_format($row->CurrYrConsRecptJuly,2);
              $aug1     = $row->CurrYrConsRecptAugust;
              $aug1_new = number_format($row->CurrYrConsRecptAugust,2);
              $sep1     = $row->CurrYrConsRecptSeptember;
              $sep1_new = number_format($row->CurrYrConsRecptSeptember,2);
              $oct1     = $row->CurrYrConsRecptOctober;
              $oct1_new = number_format($row->CurrYrConsRecptOctober,2);
              $nov1     = $row->CurrYrConsRecptNovember;
              $nov1_new = number_format($row->CurrYrConsRecptNovember,2);
              $dec1     = $row->CurrYrConsRecptDecember;
              $dec1_new = number_format($row->CurrYrConsRecptDecember,2);
              $jan1     = $row->CurrYrConsRecptJan;
              $jan1_new = number_format($row->CurrYrConsRecptJan,2);
              $feb1     = $row->CurrYrConsRecptFeb;
              $feb1_new = number_format($row->CurrYrConsRecptFeb,2);
              $mar1     = $row->CurrYrConsRecptMarch;
              $mar1_new = number_format($row->CurrYrConsRecptMarch,2);
			  
			  if($apr1 == '' || $apr1 == NULL || $apr1 == 0){
				  $div_apr1 = 0;
			  } else {
				  $div_apr1 = 1;
			  }
			  
			  if($may1 == '' || $may1 == NULL || $may1 == 0){
				  $div_may1 = 0;
			  } else {
				  $div_may1 = 1;
			  }
			  
			  if($jun1 == '' || $jun1 == NULL || $jun1 == 0){
				  $div_jun1 = 0;
			  } else {
				  $div_jun1 = 1;
			  }
			  
			  if($jul1 == '' || $jul1 == NULL || $jul1 == 0){
				  $div_jul1 = 0;
			  } else {
				  $div_jul1 = 1;
			  }
			  
			  if($aug1 == '' || $aug1 == NULL || $aug1 == 0){
				  $div_aug1 = 0;
			  } else {
				  $div_aug1 = 1;
			  }
			  
			  if($sep1 == '' || $sep1 == NULL || $sep1 == 0){
				  $div_sep1 = 0;
			  } else {
				  $div_sep1 = 1;
			  }
			  
			  if($oct1 == '' || $oct1 == NULL || $oct1 == 0){
				  $div_oct1 = 0;
			  } else {
				  $div_oct1 = 1;
			  }
			  
			  if($nov1 == '' || $nov1 == NULL || $nov1 == 0){
				  $div_nov1 = 0;
			  } else {
				  $div_nov1 = 1;
			  }
			  
			  if($dec1 == '' || $dec1 == NULL || $dec1 == 0){
				  $div_dec1 = 0;
			  } else {
				  $div_dec1 = 1;
			  }
			  
			  if($jan1 == '' || $jan1 == NULL || $jan1 == 0){
				  $div_jan1 = 0;
			  } else {
				  $div_jan1 = 1;
			  }
			  
			  if($feb1 == '' || $feb1 == NULL || $feb1 == 0){
				  $div_feb1 = 0;
			  } else {
				  $div_feb1 = 1;
			  }
			  
			  if($mar1 == '' || $mar1 == NULL || $mar1 == 0){
				  $div_mar1 = 0;
			  } else {
				  $div_mar1 = 1;
			  }
			  
              $tot1     = $apr1+$may1+$jun1+$jul1+$aug1+$sep1+$oct1+$nov1+$dec1+$jan1+$feb1+$mar1;
			  $div_tot1 = $div_apr1+$div_may1+$div_jun1+$div_jul1+$div_aug1+$div_sep1+$div_oct1+$div_nov1+$div_dec1+$div_jan1+$div_feb1+$div_mar1;
              $tot1_new = number_format($tot1,2);
              $avg1     = $tot1/$div_tot1;
              $avg1_new = number_format($avg1,2);
              ?>
              <tr>
                <td>Receipt ( <?php echo $fin_yr1; ?> ) </td>
                <td><?php echo $apr1_new; ?></td>
                <td><?php echo $may1_new; ?></td>
                <td><?php echo $jun1_new; ?></td>
                <td><?php echo $jul1_new; ?></td>
                <td><?php echo $aug1_new; ?></td>
                <td><?php echo $sep1_new; ?></td>
                <td><?php echo $oct1_new; ?></td>
                <td><?php echo $nov1_new; ?></td>
                <td><?php echo $dec1_new; ?></td>
                <td><?php echo $jan1_new; ?></td>
                <td><?php echo $feb1_new; ?></td>
                <td><?php echo $mar1_new; ?></td>
                <td><?php echo $tot1_new; ?></td>
                <td><?php echo $avg1_new; ?></td>                          
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />

<?php
	$curr_date_year = date("Y-m-d");
	$curr_fin_year = substr($curr_date_year,2,2);
	$last_fin_year = $curr_fin_year - 1;
	$last_fin_yr = "( FY-".$last_fin_year."-".$curr_fin_year." )";
?>
            
<!--********* YEARLY CONSUMPTION OF LAST 1 YEAR *******-->
          
<!--***** RECORDER LEVEL RECORDER QUANTITY ******-->
          
<div class="row">
    <div class="col-lg-6" style="font-weight:bold; font-size:15px;">
          Last year Consumption &amp; Receipt
    </div>
    <div class="col-lg-6" style="font-weight:bold; font-size:15px;">
          Reorder Level Reorder Quantity
    </div>
</div>
          		
 <div class="row">
    <div class="col-lg-6">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Type</th>
                <th>Qty.</th>
                <th>Value</th>
              </tr>
            </thead>
            <tbody>
              <?php 
			  foreach ($lstyrcumnrec1->result() as $row) {
				  $cons_tot_qty = $row->ConsTotalQty;
				  $cons_tot_val = $row->ConsTotalVal;
				  $recpt_qty    = $row->RecptTotalQty;
				  $recpt_val    = $row->RecptTotalVal;
			  ?>
              <tr>
                <td>Consumption <?php echo $last_fin_yr; ?></td>
                <td><?php echo number_format($cons_tot_qty,2); ?></td>
                <td><?php echo number_format($cons_tot_val,2); ?></td>                          
              </tr>
              <tr>
                <td>Receipt <?php echo $last_fin_yr; ?></td>
                <td><?php echo number_format($recpt_qty,2); ?></td>
                <td><?php echo number_format($recpt_val,2); ?></td>                          
              </tr>
              <?php 
			  } 
			  ?>
            </tbody>
       </table>
    </div>
    
    <div class="col-lg-6">
      <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>REORDER LEVEL</th>
                <th>REORDER QUANTITY</th>
              </tr>
            </thead>
            <?php   
			foreach ($reorder->result() as $row) { 
				$reorder_level =  $row->iou_reorderlevel;
				$reorder_qty =  $row->iou_reorderqty;
			?>
            <tbody>
              <tr>
                <td><?php echo number_format($reorder_level, 2); ?></td><!--RECORDER LEVEL-->
                <td><?php echo number_format($reorder_qty, 2); ?></td><!--RECORDER QUANTITY-->                         
              </tr>
            </tbody>
            <?php 
			}  
			?>
      </table>
    </div>
 </div>
            
<!--******* LAST FIVE PURCHASES WITH RATE, SUPPLIER, CITY, LEAD TIME(DAYS)  *********-->  
          
<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
          Last Five Purchases
    </div>      
</div> 		
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1" style="font-size:10px">
            <thead>
              <tr>
                <th>PO/SCO/WO No</th>
                <th>GRCPT/Production Receipt</th>
                <th>Status</th>
                <th>Rate</th>
                <th>Supplier Name</th>
                <th>PR/SCR No</th>
                <th>Creation of PR/SCR Date</th>
                <th>Need Date</th>
                <th>GRCPT (Freeze & Move Date)</th>
                <th>Lead Time</th>
                <th>Receipt Qty.</th>
                <th>Accepted Qty.</th>
                <th>Rejected Qty.</th>
              </tr>
            </thead>
            <tbody>
              <?php 
              foreach ($lastfivetrans1->result() as $row) 
                { 
                    $po_sco_wo_no = $row->LastPOGRTran;
                    $grcpt_prod_recipt = $row->LastReceipt;
                    $status = $row->AllocTranStatus;
                    $rate = number_format($row->LastRate,2);
                    $supp_name = $row->LastSupplier;
                    $pr_scr_no = $row->LastPRSCRTran;
                    $pr_scr_createdate = substr($row->LastPRSCRDate,0,10);
                    $pr_need_date1 = substr($row->LastPRSCRNeedDt,0,10);
                    $pr_need_date = date("d-m-Y", strtotime($pr_need_date1));
                    $grcpt_freeze_move_dt = substr($row->LastMoveDt,0,10);
                    $lead_time = $row->LastLeadTime;
                    $receipt_qty = round($row->LastTranQty,2);
                    $accepted_qty = round($row->CurrYrConsRecptJan,2);
                    $rejected_qty = round($row->CurrYrConsRecptFeb,2);
              ?>
              <tr>
                <td><?php echo $po_sco_wo_no; ?></td>
                <td><?php echo $grcpt_prod_recipt; ?></td>
                <td><?php echo $status; ?></td>
                <td><?php echo $rate; ?></td>
                <td><?php echo $supp_name; ?></td>
                <td><?php echo $pr_scr_no; ?></td>
                <td><?php echo $pr_scr_createdate; ?></td>
                <td><?php echo $pr_need_date; ?></td> 
                <td><?php echo $grcpt_freeze_move_dt; ?></td>
                <td><?php echo $lead_time; ?></td>
                <td><?php echo number_format($receipt_qty,2); ?></td>
                <td><?php echo number_format($accepted_qty,2); ?></td>
                <td><?php echo number_format($rejected_qty,2); ?></td>                            
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />

<!--******* SUPPLIER *********-->

<div class="row">
	<div class="col-lg-6" style="font-weight:bold; font-size:15px;">
    	Supplier
    </div>
    <div class="col-lg-6">

    </div>
</div>
<div class="row">
	<div class="col-lg-6">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Suppliers</th>
                <th>Purchase Rate</th>
                <th>Lead Time</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($supplier1->result() as $row) { ?>
              <tr>
                <td><?php echo $row->SuppName; ?></td>
                <td><?php echo number_format($row->SuppRate,2); ?></td>
                <td><?php echo $row->SuppLeadTime; ?></td>                       
              </tr>
              <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="col-lg-6">
    </div>
</div><br />

<!--******* USED IN BOM DETAIL *********-->

<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
       Used In BOM Detail
    </div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>Product Structure No</th>
                <th>Output Item</th>
                <th>Item desc.</th>
                <th>Required Qty</th>
                <th>Warehouse</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($bomdetail1->result() as $row) { ?>
              <tr>
                <td><?php echo $row->PSNo; ?></td>
                <td><?php echo $row->FgCode; ?></td>
                <td><?php 
                
                $FgCodeDesc = $row->FgCodeDesc;
                $fgdec = explode(':',$FgCodeDesc);
                if ($fgdec[0] == 'LOC'){
                $dec = $fgdec[1]; }
                else {
                $dec = $fgdec[0]; }
                
                
                echo $dec; ?></td>
                <td><?php echo number_format($row->BomItemQty,2); ?></td>
                <td><?php echo $row->BomWhCode; ?></td>                         
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br />

<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12" style="font-weight:bold; font-size:15px;">
       Action Timing Report
    </div>
</div>
<div class="row">
    <div class="col-lg-12">	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>ERP Created By</th>
                <th>ERP Created Date</th>
                <th>ERP Modified By</th>
                <th>ERP Modified Date</th>
                <th>Live Created By</th>
                <th>Live Created Date</th>
                <th>Live Authorised By</th>
                <th>Live Authorised Date</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($prerplive->result() as $row) { ?>
              <tr>
                <td><?php echo $row->preqm_createdby; ?></td>
                <td><?php echo $row->preqm_createddate; ?></td>
                <td><?php echo $row->preqm_lastmodifiedby; ?></td>
                <td><?php echo $row->preqm_lastmodifieddate; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->created_by; ?></td>
                <td><?php echo $row->create_date; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->pr_approval_by; ?></td>
                <td><?php echo $row->pr_approval_date; ?></td>                         
              </tr>
              <?php } ?>
            </tbody>
       </table>
    </div>
</div><br /> 

<?php //Action Timing Report ?> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>