<?php 
include_once('header.php');  
$this->load->helper('mrp_helper'); 

echo "chandra narayan sharma";
die;

$live_cat = $_REQUEST['live_cat'];
$erp_cat = $_REQUEST['erp_cat'];
$cp_stat = $_REQUEST['cp_stat'];
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
            	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Disapproved Purchase Requests</h4>
            </div>
        </div><br />
        
        <div class="row">
        	<div class="col-lg-12">
            <table class="table table-bordered">
                <tr style="background-color:#CCC; font-weight:bold">
                    <td>SN.</td>
                    <td>Item Code</td>
                    <td>Description </td>
                    <td>Category </td>
                    <td>Item Type</td>
                    <td>Source</td>
                    <td>Reorder Status</td>
                    <td>Total Requirement</td>
                </tr>
            <?php 
            if($category == 'All'){
					
				$sql="select * from tipldb..pr_submit_table c, scmdb..prq_preqm_pur_reqst_hdr d, 
				SCMDB..itm_ibu_itemvarhdr e, TIPLDB..erp_live_category f
				where c.pr_status = 'PR Disapproved At Purchase Level'
				and c.pr_num = d.preqm_prno
				and d.preqm_status  = 'AU'
				and c.item_code = e.ibu_itemcode
				and e.ibu_category = f.erp_cat_code
				and c.pr_num not in(
					select distinct poprq_prno from scmdb..po_poprq_poprcovg_detail a, scmdb..po_pomas_pur_order_hdr b 
					where a.poprq_poamendmentno = b.pomas_poamendmentno
					and a.poprq_pono = b.pomas_pono
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and b.pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT')
					and poprq_pocovqty = c.required_qty
				)";
				
			} else {
				
				$sql="select * from tipldb..pr_submit_table c, scmdb..prq_preqm_pur_reqst_hdr d, 
				SCMDB..itm_ibu_itemvarhdr e, TIPLDB..erp_live_category f
				where c.pr_status = 'PR Disapproved At Purchase Level'
				and c.pr_num = d.preqm_prno
				and d.preqm_status  = 'AU'
				and c.item_code = e.ibu_itemcode
				and e.ibu_category = f.erp_cat_code
				and f.erp_cat_code in('$erp_cat')
				and f.live_category in('$category')
				and c.pr_num not in(
					select distinct poprq_prno from scmdb..po_poprq_poprcovg_detail a, scmdb..po_pomas_pur_order_hdr b 
					where a.poprq_poamendmentno = b.pomas_poamendmentno
					and a.poprq_pono = b.pomas_pono
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and b.pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT')
					and poprq_pocovqty = c.required_qty
				)";
					
			}
			
			$query1=$this->db->query($sql);
            $i=1;
            foreach ($query1->result() as $row) {
               $ItemCode = $row->ItemCode;
               $ItemCode1 = urlencode($ItemCode); 
			   
               if(strpos($ItemCode1, '%2F') !== false){ 
                  $ItemCode2 = str_replace("%2F","chandra",$ItemCode1);
               } else {
                  $ItemCode2 = $ItemCode1;
               }   
               
               $ReOrderLevel = $row->ReOrderLevel;
               $ReOrderQty = $row->ReOrderQty;
               
               if($ReOrderLevel == 0 && $ReOrderQty == 0){
                   $item_type = "Project";
               } else {
                   $item_type = "Reorder";
               }
               
               $Manufactured = $row->Manufactured;
               $Purchased = $row->Purchased;
               $SubContracted = $row->SubContracted;
			   $item_category_live = $row->live_category;
			   $item_desc = nl2br($row->Descriptions);
			   $req_qty = $row->stk_notin_cp;
			   $cp_status = $row->cp_status;
               
               if($Manufactured == 'Yes' && $Purchased == 'No' && $SubContracted == 'No'){
                   $source = "Manufacturing";
               } else if($Manufactured == 'No' && $Purchased == 'Yes' && $SubContracted == 'No'){
                   $source = "Purchase";
               } else if($Manufactured == 'No' && $Purchased == 'No' && $SubContracted == 'Yes'){
                   $source = "Subcontract";
               } else if($Manufactured == 'Yes' && $Purchased == 'Yes' && $SubContracted == 'No'){
                   $source = "Manufacturing, Purchase";
               } else if($Manufactured == 'No' && $Purchased == 'Yes' && $SubContracted == 'Yes'){
                   $source = "Purchase, Subcontract";
               } else if($Manufactured == 'Yes' && $Purchased == 'No' && $SubContracted == 'Yes'){
                   $source = "Manufacturing, Subcontract";
               } else if($Manufactured == 'Yes' && $Purchased == 'Yes' && $SubContracted == 'Yes'){
                   $source = "Manufacturing, Purchase, Subcontract";
               }
			   
				//Getting Reorder Status
				$sql_reorder_stat = "select * from tipldb..reorder_update_entry  where item_code = '$ItemCode'";
				$qry_reorder_stat = $this->db->query($sql_reorder_stat);
				
				if($qry_reorder_stat->num_rows() > 0){
					foreach($qry_reorder_stat->result() as $row){
						$status = $row->status;
					}
				} else {
					$status = "Project Item";
				}
            ?>
                <tr>
                    <td><? echo $i;?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $ItemCode2; ?>" target="_blank">
                            <?php echo $ItemCode;?>
                        </a>
                    </td>
                    <td><?php echo mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8"); ?></td>
                    <td><?php echo $item_category_live; ?></td>
                    <td><?php echo $item_type; ?></td>
                    <td><?php echo $source; ?></td>
                    <td><?php echo $status; ?></td>
                    <td>
                        <a href="<?= base_url(); ?>index.php/mrp_reportc/itm_stk_na_det?item_code=<?=rawurlencode($item_code); ?>&cp_status=<?=$cp_status; ?>" target="_blank">
                            <?php echo $req_qty; ?>
                        </a>
                    </td>
                </tr>
            <?php $i++;} ?>
            </table>
            </div>
        </div><br />
    </section>
</section>     		
<!--main content end-->
<?php include_once('footer.php'); ?>