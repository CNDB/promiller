 <?php include'header.php'; ?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEM DETAILS</h4>
        </div>
    </div><br />
    
    <div class="row">
      <div class="col-lg-12">
       	<table class="table table-bordered">
        	<tr style="background-color:#CCC">
            	<td><b>S.No.</b></td>
                <td><b>Item Code</b></td>
                <td><b>Item Desc</b></td>
                <td><b>Item Category</b></td>
                <td><b>Current Stock(Accepted)</b></td>
                <td><b>Reorder Level</b></td>
                <td><b>Reorder Qty</b></td>
                <td><b>Last Yr Cons.</b></td>
                <td><b>Curr Yr Cons.</b></td>
                <td><b>Lead Time(Avg)</b></td>
                <td><b>Unit Rate(Avg)</b></td>
                <td><b>Source</b></td>
                <td><b>Priority</b></td>
            </tr>
            <?php 
			$s_no = 0;
			foreach ($h->result() as $row){
				$s_no++;
				$item_code = $row->item_code;
				$item_desc = $row->item_desc;
				$priority = $row->priority;
				
				$reorder_lvl = $row->reorder_lvl;
				$reorder_qty = $row->reorder_qty;
				$current_stk = $row->current_stk;
				
				$last_yr_cons = $row->last_yr_cons;
				$curr_yr_cons = $row->current_yr;
				$lead_time = $row->lead_time;
				
				$unit_rate = $row->unit_rate;
				$source = $row->source;
				
				$item_category = $row->item_category;
				$item_category_desc = $row->item_class_desc;
				
				
				$item_code1 = urlencode($item_code);
					
				if(strpos($item_code1, '%2F') !== false)
				{
					$item_code2 = str_replace("%2F","chandra",$item_code1);
				}
				else 
				{
					$item_code2 = $item_code1;
				}
				
?>
            <tr>
            	<td><?php echo $s_no; ?></td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank"><?php echo $item_code; ?></a>
                </td>
                <td><?php echo $item_desc; ?></td>
                <td><?php echo $item_category_desc; ?></td>
                <td><?php echo number_format($current_stk,2); ?></td>
                <td><?php echo number_format($reorder_lvl,2); ?></td>
                <td><?php echo number_format($reorder_qty,2); ?></td>
                <td><?php echo number_format($last_yr_cons,2); ?></td>
                <td><?php echo number_format($curr_yr_cons,2); ?></td>
                <td><?php echo number_format($lead_time,2); ?></td>
                <td><?php echo number_format($unit_rate,2); ?></td>
                <td><?php echo $source; ?></td> 
                <td><?php echo $priority; ?></td>
            </tr>
            <?php } ?>
        </table>
      </div>
    </div><br />
    

<?php //Action Timing Report ?> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>