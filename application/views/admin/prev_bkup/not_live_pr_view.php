<?php include'header.php'; ?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Not Live Approved PR</h4>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12">
          <table align="center" cellpadding="0" cellspacing="0" class="table table-bordered">
          	<tr style="background-color:#0CF">
            	<td><b>SNO.</b></td>
                <td><b>PR Number</b></td>
                <td><b>Item Code</b></td>
                <td><b>Item Description</b></td>
                <td><b>Category</b></td>
                <td><b>Created Date</b></td>
                <td><b>Created By</b></td>
            </tr>
            <?php
				$sno = 0;
				foreach($pr_data->result() as $row){
					$sno++;
			?>
            <tr>
            	<td><?php echo $sno; ?></td>
                <td><?php echo $pr_num = $row->pr_num; ?></td>
                <td><?php echo $item_code = $row->item_code; ?></td>
                <td><?php echo $item_desc = $row->itm_desc; ?></td>
                <td><?php echo $catagory = $row->category; ?></td>
                <td><?php echo $create_date = substr($row->create_date,0,11); ?></td>
                <td><?php echo $created_by = $row->created_by; ?></td>
            </tr>
            <?php
				}
			?>
          </table>
        </div>
    </div><br />
    
  </section>
</section>     
<?php include('footer.php'); ?>
