<?php include'header.php' ?>      
<!--main content start-->
<section id="main-content">
  <section class="wrapper">
      <!-- page start-->
      <div class="row">
         <div class="col-lg-12">
            <section class="panel" style="text-align:center; text-transform:uppercase;">
                  <h3>Cost Sheet</h3>
            </section>
            <div class="col-lg-2">
            </div>
            <div class="col-lg-8">
            <section class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                        <tr style="background-color:#CCC">
                            <td style="color:#000;">Sale Order No.</td>
                            <td style="color:#000;">Project Name</td>
                            <td style="color:#000;">Customer Name</td>
                            <td style="color:#000;">Attached Cost Sheet</td>
                        </tr>
                    <?php 
					foreach ($cost_sheet->result() as $row){ 
                		$sono = $row->sono;
						$project_name = $row->project_name;
						$customer_name = $row->customer_name;
						$attached_cost_sheet = $row->attch_cost_sheet;
					?>
                        <tr>
                            <td><?php echo $sono; ?></td>
                            <td><?php echo $project_name; ?></td>
                            <td><?php echo $customer_name; ?></td>
                            <td>
                                <a href="http://live1.tipl.com/pr/uploads/<?php echo $attached_cost_sheet; ?>" target="_blank">
                                	<?php echo $attached_cost_sheet; ?>
                                </a>    
                            </td>          
                        </tr>    
                    <?php
					}
					?>
                </table>
            </section>
            </div>
            <div class="col-lg-2">
            </div>
         </div>
      </div>
      <!-- page end-->
  </section>
</section>
<!--main content end-->
<?php include('footer.php'); ?>