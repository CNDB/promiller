<?php 
include'header.php'; 
$po_num = $this->uri->segment(3);
$nik = $this->uri->segment(3);
$entry_no = $this->uri->segment(4);
?>
<!--main content start-->
<body>
    <section id="main-content">
        <section class="wrapper">
            <div class="row"  style="margin-top:-10px">
                <div class="col-lg-12" style="background-color:#333333; padding:2px">
                	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">
                    	without MC & DI pending for approval from Reporting Authority
                    </h4>
                </div>
            </div><br>
            
            <?php include('po_header.php'); ?>
    
            <form action="<?php echo base_url(); ?>index.php/mcdi_approvalc/mcdi_ra_submit" method="post">
            
            <input type="hidden" id="entry_no" name="entry_no" value="<?php echo $entry_no; ?>">
            
            <?php include('po_details_div.php'); ?>
            
            <?php include('po_supplier_quotes.php'); ?><br>
            
            <div class="row">
            	<div class="col-lg-3"><b>Reason / Remarks:</b></div>
                <div class="col-lg-3">
                	<textarea id="ra_rmks" name="ra_rmks" class="form-control"></textarea>
                </div>
                
                <?php
					$username = $_SESSION['username'];
					
					foreach($view_po->result() as $row){
						$created_by = $row->created_by;
						
						$sql2="select emp_suprv from tipldb..employee where emp_email like'%$created_by%'";
						$query2 = $this->db->query($sql2);
						
						if($query2->num_rows() > 0){
							foreach ($query2->result() as $row){
								$emp_suprv = $row->emp_suprv;
								
								$sql3="select emp_email from tipldb..employee where emp_name like'%$emp_suprv%'";
								$query3 = $this->db->query($sql3);
								
								if($query3->num_rows() > 0){
									foreach ($query3->result() as $row){
										$emp_email = $row->emp_email;
										$pending_on = explode("@",$emp_email);
									}
								}
							}
						}
						
						//echo $pending_on[0];
						//die;
						
						if($username == 'sandeep.tak' || $username == 'admin' || $username == 'priyanka.vijay'){
				?>
                
                <div class="col-lg-3"><input type="submit" id="approval" name="approval" value="APPROVE" class="form-control"></div>
                <div class="col-lg-3"><input type="submit" id="approval" name="approval" value="DISAPPROVE" class="form-control"></div>
                
                <?php
						} else {
							echo "<div class='col-lg-6'></div></div>";
							echo "
							  <div class='row'>
								<div class='col-lg-12' style='text-align:center; color:red'>
									<h3>You Are Not Authorized To Approve Or Disapprove This PO...</h3>
								</div>";
						}
					break;}
				?>
            </div>
            
            </form>
            
            <?php include('po_chat_history.php'); ?>
            
            <?php include('po_action_timing.php'); ?>
            
        </section>
    </section>
</body>

<?php include('footer.php'); ?>