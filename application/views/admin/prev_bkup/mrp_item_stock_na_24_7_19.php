<?php 
include_once('header.php');  
$this->load->helper('mrp_helper'); 

$live_cat = $_REQUEST['live_cat'];
$erp_cat = $_REQUEST['erp_cat'];
$cp_stat = $_REQUEST['cp_stat'];
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
            	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEMS NOT IN MRP AND REQUIRED</h4>
            </div>
        </div><br />
        
        <div class="row">
        	<div class="col-lg-12">
            <table class="table table-bordered">
                <tr style="background-color:#CCC; font-weight:bold">
                    <td>SN.</td>
                    <td>Item Code</td>
                    <td>Description </td>
                    <td>Category </td>
                    <td>Item Type</td>
                    <td>Source</td>
                    <td>Total Requirement</td>
                </tr>
            <?php 
			
			//echo "Cha====".$cp_stat;
            
            if($erp_cat == 'All'){
				$sql="select distinct a.ItemCode,ReOrderLevel,ReOrderQty,Manufactured,Purchased,SubContracted,Descriptions,live_category,stk_notin_cp,cp_status
				from TIPLDB..pendingissuetbl a, SCMDB..itm_ibu_itemvarhdr b, 
				TIPLDB..erp_live_category c, scmdb..all_items_cns d
				where a.ItemCode = b.ibu_itemcode 
				and b.ibu_category = c.erp_cat_code 
				and a.ItemCode = d.ItemCode
				and a.cp_status = '$cp_stat'";
			} else {
				$sql="select distinct a.ItemCode,ReOrderLevel,ReOrderQty,Manufactured,Purchased,SubContracted,Descriptions,live_category,stk_notin_cp,cp_status
				from TIPLDB..pendingissuetbl a, SCMDB..itm_ibu_itemvarhdr b, 
				TIPLDB..erp_live_category c, scmdb..all_items_cns d
				where a.ItemCode = b.ibu_itemcode 
				and b.ibu_category = c.erp_cat_code 
				and a.ItemCode = d.ItemCode 
				and a.cp_status = '$cp_stat'
				and c.live_category in('$live_cat')
				and c.erp_cat_code in('$erp_cat')";
			}
			
			$query1=$this->db->query($sql);
            $i=1;
            foreach ($query1->result() as $row) {
               $item_code = $row->ItemCode;
               $item_code1 = urlencode($item_code); 
               if(strpos($item_code1, '%2F') !== false){ 
                  $item_code2 = str_replace("%2F","chandra",$item_code1);
               } else {
                  $item_code2 = $item_code1;
               }   
               
               $ReOrderLevel = $row->ReOrderLevel;
               $ReOrderQty = $row->ReOrderQty;
               
               if($ReOrderLevel == 0 && $ReOrderQty == 0){
                   $item_type = "Project";
               } else {
                   $item_type = "Reorder";
               }
               
               $Manufactured = $row->Manufactured;
               $Purchased = $row->Purchased;
               $SubContracted = $row->SubContracted;
			   $item_category_live = $row->live_category;
			   $item_desc = nl2br($row->Descriptions);
			   $req_qty = $row->stk_notin_cp;
			   $cp_status = $row->cp_status;
               
               if($Manufactured == 'Yes' && $Purchased == 'No' && $SubContracted == 'No'){
                   $source = "Manufacturing";
               } else if($Manufactured == 'No' && $Purchased == 'Yes' && $SubContracted == 'No'){
                   $source = "Purchase";
               } else if($Manufactured == 'No' && $Purchased == 'No' && $SubContracted == 'Yes'){
                   $source = "Subcontract";
               } else if($Manufactured == 'Yes' && $Purchased == 'Yes' && $SubContracted == 'No'){
                   $source = "Manufacturing, Purchase";
               } else if($Manufactured == 'No' && $Purchased == 'Yes' && $SubContracted == 'Yes'){
                   $source = "Purchase, Subcontract";
               } else if($Manufactured == 'Yes' && $Purchased == 'No' && $SubContracted == 'Yes'){
                   $source = "Manufacturing, Subcontract";
               } else if($Manufactured == 'Yes' && $Purchased == 'Yes' && $SubContracted == 'Yes'){
                   $source = "Manufacturing, Purchase, Subcontract";
               }
            ?>
                <tr>
                    <td><? echo $i;?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
                            <?php echo $item_code;?>
                        </a>
                    </td>
                    <td><?php echo mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8"); ?></td>
                    <td><?php echo $item_category_live; ?></td>
                    <td><?php echo $item_type; ?></td>
                    <td><?php echo $source; ?></td>
                    <td>
                        <a href="<?= base_url(); ?>index.php/mrp_reportc/itm_stk_na_det?item_code=<?=rawurlencode($item_code); ?>&cp_status=<?=$cp_status; ?>" target="_blank">
                            <?php echo $req_qty; ?>
                        </a>
                    </td>
                </tr>
            <?php $i++;} ?>
            </table>
            </div>
        </div><br />
    </section>
</section>     		
<!--main content end-->
<?php include_once('footer.php'); ?>