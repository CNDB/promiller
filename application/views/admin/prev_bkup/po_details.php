<?php
	include'header.php';
	
	$po_num = $this->uri->segment(3);                    
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PO DETAILS</h4>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-6">
        	<h4>PO No. &nbsp;&nbsp;<?php echo $po_num;  ?></h4>
        </div>
        <div class="col-lg-6">
            <table border="1" align="center" class="table table-bordered" style="font-size:9px">
                <tr>
                    <td><b>CONDITION</b></td>
                    <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                    <td>FIRST TIME PURCHASE</td>
                </tr>
                <tr>
                    <td><b>COLOR</b></td>
                    <td style="background-color:red;"></td>
                    <td style="background-color:green;"></td>
                    <td style="background-color:blue;"></td>
                    <td style="background-color:yellow;"></td>
                </tr>
            </table>
        </div>
    </div><br />
    
	<?php include('po_details_div.php'); ?>
        
    <!--****** REMARKS *******--> 
    <?php
      foreach ($view_po->result() as $row)  {
          $po_num_new = $row->po_num; 
      }
    ?>                      
    <div class="row">
        <div class='col-lg-3'> 
        <b>Attach Quotes From Supplier</b>
        </div>
        <div class='col-lg-3'> 
        <?php 	
            $sql8 ="select * from TIPLDB..po_supplier_quotes where po_num = '$po_num_new'";
            $query8 = $this->db->query($sql8);
                            
            if ($query8->num_rows() > 0) {
              foreach ($query8->result() as $row) {
                  $supp_quotes = $row->attached_supp_quotes;
                  echo "<a href='http://live.tipl.com/pr/uploads/$supp_quotes' target='_blank'>$supp_quotes</a><br />";
                }
            } else {
                  $supp_quotes = "";
            }	
        ?>
        </div>
        <div class="col-lg-1">
         <b> Remarks:</b>
        </div>
        <div class="col-lg-5">
        <?php
            foreach ($view_po->result() as $row){ 
        ?>
          <b><?php echo $row->po_rmks; ?></b>
        <?php
            break;} 
        ?>
        </div>
    </div><br /> 
    
     <?php //chat history ?>
 <div class="row">
  <div class="col-lg-12">
    <h3>Chat History</h3>	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>LEVEL</th>
                <th>NAME</th>
                <th>COMMENT</th>
                <th>INSTRUCTION</th>
                <th>DATE TIME</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
                $query1 = $this->db->query($sql1);
                if ($query1->num_rows() > 0) {
				foreach ($query1->result() as $row) {
				  $level = $row->level;
				  $name  = $row->comment_by;
				  $comment = $row->comment;
				  $instruction = $row->instruction;
				  $datentime = $row->datentime;
                                
               ?>
              <tr>
                <td><?php echo $level; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $comment; ?></td>
                <td><?php echo $instruction; ?></td>
                <td><?php echo $datentime; ?></td>                            
              </tr>
              <?php 
                } } 
              ?>
            </tbody>
       </table>    
    </div>
 </div>
 <?php //chat history ?>
 
<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12">
    	<h3>Action Timing</h3>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<table align="center" class="table table-bordered">
        	<thead>
            	<th>ERP CREATED</th>
                <th>ERP CREATED DATE</th>
                <th>ERP LAST MODIFIED</th>
                <th>ERP LAST MODIFIED DATE</th>
                <th>LIVE CREATED</th>
                <th>LIVE CREATED DATE</th>
            </thead>
         	<?php foreach($view_po->result() as $row){ ?>
            <tbody>
            	<td><?php echo $row->pomas_createdby; ?></td>
                <td><?php echo $row->pomas_createddate; ?></td>
                <td><?php echo $row->pomas_lastmodifiedby; ?></td>
                <td><?php echo $row->pomas_lastmodifieddate; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->po_approvedby_lvl0; ?></td>
                <td><?php echo $row->po_approveddate_lvl0; ?></td>
            </tbody>
            <?php break; } ?>
        </table>    
    </div>
</div>

<?php //Action Timing Report ?>
    
  </section>
</section>

<?php include('footer.php'); ?>