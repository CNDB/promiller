 <?php include'header.php'; ?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEM REPORT</h4>
        </div>
    </div><br />
    
    <div class="row">
      <div class="col-lg-12">
       	<table class="table table-bordered">
        	<tr style="background-color:#CCC">
            	<td><b>S.No.</b></td>
                <td><b>Item Code</b></td>
                <td><b>Item Desc.</b></td>
                <td><b>Item Value</b></td>
                <td><b>Lead Time</b></td>
                <td><b>Item Currency</b></td>
                <td><b>Monthwise Avg Consumption</b></td>
                <td><b>Freq. Of Stocking</b></td>
                <td><b>PM Category</b></td>
                <td><b>Source</b></td>
                <td><b>Last Year Cons.</b></td>
                <td><b>2016-2017 Cons.</b></td>
                <td><b>Present Stock</b></td>
                <td><b>Alloc. Stock</b></td>
                <td><b>Pending For Allocation</b></td>
                <td><b>Reorder Level</b></td>
                <td><b>Reorder Quantity</b></td>
            </tr>
            <?php
				$sno = 0;
				foreach($item_details->result() as $row){
					$sno++;
					$item_code = $row->item_code;
					$item_desc = $row->item_desc;
					$live_category = $row->live_category;
			?>
            <tr>
            	<td><?php echo $sno; ?></td>
            	<td><?php echo $item_code; ?></td>
                <td><?php //echo $item_desc; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td><?php echo $live_category; ?></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <?php } ?>
        </table>
      </div>
    </div><br />
    

<?php //Action Timing Report ?> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>