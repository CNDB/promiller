 <?php include'header.php'; ?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Authorize Performa Invoice</h4>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-4">
        	<h4>Purchase Order No - <?php echo $po_number = $this->uri->segment(3); ?></h4>
        </div>
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
        	<table border="1" align="center" class="table table-bordered" style="font-size:9px">
                <tr>
                    <td><b>CONDITION</b></td>
                    <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                    <td>FIRST TIME PURCHASE</td>
                </tr>
                <tr>
                	<td><b>COLOR</b></td>
                    <td style="background-color:red;"></td>
                    <td style="background-color:green;"></td>
                    <td style="background-color:blue;"></td>
                    <td style="background-color:yellow;"></td>
                </tr>
            </table>
        </div>
	</div><br />
    
     <!--- PO PRINT PDF -->
    
    <div class="row">
        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_number; ?>" 
            target="_blank" style="font-size:16px; font-weight:bold;">
            <button type="button" class="form-control">PRINT PO</button></a>
        </div>
        <div class="col-lg-10"></div>
	</div><br />
    
    <!--- PO Details Start-->
    
    <?php foreach ($po_details->result() as $row){ ?>
    
    	<?php include('po_details_div.php'); ?>
    
    <?php break;} ?>
    
    <!--- PO Details Ends-->
    
    <!-- PO Item Details Start -->
    
    <?php include('po_item_details_div.php'); ?>
    
    <!--- PO Item Details Ends -->
    
    <!--- PO Attached Supplier Quotes & Remarks Starts -->
    
    <form action="<?php echo base_url(); ?>index.php/createpic/insert_po_sub_lvl2" method="post" onsubmit="return validate()">
  
	<?php
      foreach ($po_item_details->result() as $row){
          $po_num_new = $row->po_num;
		  echo "<input type='hidden' name='po_num' value='$po_num_new'>"; 
      break;}
    ?>
    <div class="row">
        <div class='col-lg-3'> 
        <b>Attach Quotes From Supplier: </b>
        </div>
        <div class='col-lg-3'> 
        <?php 	
            $sql8 ="select * from TIPLDB..po_supplier_quotes where po_num = '$po_num_new'";
            $query8 = $this->db->query($sql8);
                            
            if ($query8->num_rows() > 0) {
              foreach ($query8->result() as $row) {
                  $supp_quotes = $row->attached_supp_quotes;
                  echo "<a href='http://live.tipl.com/pr/uploads/$supp_quotes' target='_blank'>$supp_quotes</a><br />";
                }
            } else {
                  $supp_quotes = "";
            }	
        ?>
        </div>
        <div class="col-lg-3"></div>
        <div class="col-lg-3"></div>
    </div><br />
    
	<!--- PO Attached Supplier Quotes & Remarks Ends -->
    
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-3"><input type="submit" name="pi_create_inst_lvl2" value="PI Approve" class="form-control"/></div>
        <div class="col-lg-3"><input type="submit" name="pi_create_inst_lvl2" value="PI Disapprove" class="form-control"/></div>
        <div class="col-lg-3"></div>
    </div>
</form>
	
<?php include('po_footer.php'); ?>
    
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
<?php include('footer.php'); ?>