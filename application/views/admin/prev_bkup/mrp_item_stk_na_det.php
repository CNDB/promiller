<?php 
include_once('header.php');  
$this->load->helper('mrp_helper'); 

$item_code = $_REQUEST['item_code'];
$cp_status = $_REQUEST['cp_status'];
$actual_link = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
            	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEM DETAILS</h4>
            </div>
        </div><br />
        
        <div class="row">
        	<div class="col-lg-12">
            	<table class="table table-bordered">
                	<tr style="background-color:#CCC; font-weight:bold">
                    	<td>ItemCode</td>
                        <td>TranNo</td>
                        <td>TranQty</td>
                        <td>WO Status</td>
                        <td>Reorder Status</td>
                        <td>Remarks</td>
                        <td>Remarks Updated By</td>
                        <td>Remarks Update Date</td>
                        <td></td>
                    </tr>
                    <?php
						$sql="select * from tipldb..pendingissuetbl where itemcode = '$item_code' and cp_status = '$cp_status'";
						$qry=$this->db->query($sql);
						
						foreach($qry->result() as $row){
							$ItemCode = $row->ItemCode;
							$item_code1 = urlencode($ItemCode); 
							if(strpos($item_code1, '%2F') !== false){ 
							  $item_code2 = str_replace("%2F","chandra",$item_code1);
							} else {
							  $item_code2 = $item_code1;
							} 
							$TranNo = $row->TranNo;
							$TranQty = $row->TranQty;
							$TranStatus = $row->TranStatus;
							$TranType = $row->TranType;
							$TranStatusDesc = "";
							
							if($TranType == 'WO'){
								if($TranStatus == 'D'){
									$TranStatusDesc = 'Requested';
								} else if($TranStatus == 'E'){
									$TranStatusDesc = 'Accepted';
								} else if($TranStatus == 'F'){
									$TranStatusDesc = 'Firmed';
								} else if($TranStatus == 'G'){
									$TranStatusDesc = 'Released';
								} else if($TranStatus == 'S'){
									$TranStatusDesc = 'Order/acty started';
								} else if($TranStatus == 'U'){
									$TranStatusDesc = 'Order/acty finished';
								} else if($TranStatus == 'V'){
									$TranStatusDesc = 'Order closed';
								} else if($TranStatus == 'W'){
									$TranStatusDesc = 'Acty short closed';
								}
							} else {
								$TranStatusDesc = "";
							}
							
							//Getting Reorder Status
							$sql_reorder_stat = "select * from tipldb..reorder_update_entry  where item_code = '$ItemCode'";
							$qry_reorder_stat = $this->db->query($sql_reorder_stat);
							
							if($qry_reorder_stat->num_rows() > 0){
								foreach($qry_reorder_stat->result() as $row){
									$status = $row->status;
								}
							} else {
								$status = "Project Item";
							}
							
							//Previous Remarks
							$sql_prev_rmks = "select rmks,rmks_by,rmks_date from tipldb..mrp_item_stk_na_req 
							where item_code = '$ItemCode' 
							and TranNo = '$TranNo'
							and rmks_date = (
								select max(rmks_date) from tipldb..mrp_item_stk_na_req 
								where item_code = '$ItemCode' and TranNo = '$TranNo'
							)";
							
							$qry_prev_rmks = $this->db->query($sql_prev_rmks);
							
							if($qry_prev_rmks->num_rows() > 0){
								foreach($qry_prev_rmks->result() as $row){
									$rmks = $row->rmks;
									$rmks_by = $row->rmks_by;
									$rmks_date = $row->rmks_date;
								}
							} else {
								$rmks = "";
								$rmks_by = "";
								$rmks_date = "";
							}
					?>
                    <form action="<?= $actual_link; ?>" method="post">
                    <tr>
                    	<td>
                        	<a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
								<?php echo $item_code;?>
                                <input type="hidden" id="item_code" name="item_code" value="<?=$item_code; ?>" />
                            </a>
                        </td>
                        <td>
                        	<?php if($TranType == 'WO'){ ?>
                                <a href="<?php echo $live_url; ?>so_wo_linkage/print_workorder.php?wo=<?=$TranNo;?>" target="_blank">
                                    <?=$TranNo;?>
                                    <input type="hidden" id="TranNo" name="TranNo" value="<?=$TranNo; ?>" />
                                </a>
                            <?php } else { ?>
                            	<?=$TranNo;?>
                                <input type="hidden" id="TranNo" name="TranNo" value="<?=$TranNo; ?>" />
                            <?php } ?>
                        </td>
                        <td><?=number_format($TranQty,4,".","");?><input type="hidden" id="TranQty" name="TranQty" value="<?=$TranQty; ?>" /></td>
                        <td><?=$TranStatusDesc;?><input type="hidden" id="TranStatusDesc" name="TranStatusDesc" value="<?=$TranStatusDesc; ?>" /></td>
                        <td><?=$status; ?></td>
                        <td><textarea id="rmks1" name="rmks1" class="form-control"><?=$rmks; ?></textarea></td>
                        <td><?=$rmks_by; ?></td>
                        <td><?=$rmks_date; ?></td>
                        <td><input type="submit" name="submit" id="submit" value="submit" class="form-control" onclick="fun_sbmt();" /></td>
                    </tr>
                    </form>
                    <?php } ?>
                </table>
            </div>
        </div><br />
    </section>
</section>     		
<!--main content end-->
<?php
if(isset($_POST['submit'])){
	$item_code = $_REQUEST["item_code"];
	$TranNo = $_REQUEST["TranNo"];
	$TranQty = $_REQUEST["TranQty"];
	$TranStatusDesc = $_REQUEST["TranStatusDesc"];
	$rmks1 = $_REQUEST["rmks1"];
	$rmks1 = str_replace("'","",$rmks1);
	$rmks_by = $_SESSION['username'];
	$rmks_date = date('Y-m-d H:i:s');
	
	$sql = "insert into tipldb..mrp_item_stk_na_req(item_code,TranNo,TranQty,TranStatusDesc,rmks,rmks_by,rmks_date) 
	values('$item_code','$TranNo','$TranQty','$TranStatusDesc','$rmks1','$rmks_by','$rmks_date')";
	
	$qry = $this->db->query($sql);
}
?>
<?php include_once('footer.php'); ?>
<!--<script type="text/javascript">
	function fun_sbmt(){
		var Item_Code = document.getElementById("item_code").value;
		var TranNo = document.getElementById("TranNo").value;
		var TranQty = document.getElementById("TranQty").value;
		var TranStatusDesc = document.getElementById("TranStatusDesc").value;
		var rmks = document.getElementById("rmks").value;
		
		
	}
</script>-->