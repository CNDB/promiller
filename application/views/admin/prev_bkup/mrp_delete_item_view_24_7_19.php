<?php include'header.php'; ?>
<section id="main-content">
  <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                  <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">DELETE ITEM ENTRY</h4>
            </div>
        </div><br />
        <?php
		foreach($delete_item_view ->result() as $row){
			$sno = $row->sno;
			$item_code = $row->item_code;
			$item_desc = $row->item_desc;
			$category = $row->category;
			$stock_uom = $row->stock_uom;
			$purchase_uom = $row->purchase_uom;
			$mrp_required_qty = $row->mrp_required_qty;
			$pr_type = $row->pr_type;
			$pr_req_qty = $row->pr_req_qty;
			$remarks = $row->remarks;
			$requested_by = $row->requested_by;
			$requested_date = substr($row->requested_date,0,11);
			$atac_no = $row->atac_no;
			$atac_ld_date = $row->atac_ld_date;
			$atac_need_date = $row->atac_need_date;
			$payment_term = $row->payment_term;
			$customer_payment_terms = $row->customer_payment_terms;
			$project_name = $row->project_name;
			$customer_name = $row->customer_name;
			$pm_group = $row->pm_group;
			$status = $row->status;
			$age = $row->diff;
		}
		?>
        <form action="<?php echo base_url(); ?>index.php/mrp_reportc/delete_item_entry?sno=<?php echo $sno; ?>" method="post" onSubmit="return reqd()">
        <div class="row">
        	<div class="col-lg-2">
            	<b>ITEM CODE</b><br><?php echo $item_code; ?>
            </div>
            <div class="col-lg-2">
            	<b>ITEM DESC</b><br><?php echo $item_desc; ?>
            </div>
            <div class="col-lg-2">
            	<b>CATEGORY</b><br><?php echo $category; ?>
            </div>
            <div class="col-lg-2">
            	<b>STOCK UOM</b><br><?php echo $stock_uom; ?>
            </div>
            <div class="col-lg-2">
            	<b>PURCHASE UOM</b><br><?php echo $purchase_uom; ?>
            </div>
            <div class="col-lg-2">
            	<b>MRP QUANTITY</b><br><?php echo $mrp_required_qty; ?>
            </div>
        </div><br><br>
        
        <div class="row">
        	<div class="col-lg-2">
            	<b>PR TYPE</b><br><?php echo $pr_type; ?>
            </div>
            <div class="col-lg-2">
            	<b>ATAC NO</b><br><?php echo $atac_no; ?>
            </div>
            <div class="col-lg-2">
            	<b>ATAC LD DATE</b><br><?php echo $atac_ld_date; ?>
            </div>
            <div class="col-lg-2">
            	<b>ATAC NEED DATE</b><br><?php echo $atac_need_date; ?>
            </div>
            <div class="col-lg-2">
            	<b>ATAC PAYMENT TERM</b><br><?php echo $payment_term; ?>
            </div>
            <div class="col-lg-2">
            	<b>CUSTOMER PAYMENT TERM</b><br><?php echo $customer_payment_terms; ?>
            </div>
        </div><br><br>
        
        <div class="row">
            <div class="col-lg-2">
            	<b>PROJECT NAME</b><br><?php echo $project_name; ?>
            </div>
            <div class="col-lg-2">
            	<b>CUSTOMER NAME</b><br><?php echo $customer_name; ?>
            </div>
            <div class="col-lg-2">
            	<b>PM GROUP</b><br><?php echo $pm_group; ?>
            </div>
            <div class="col-lg-2">
            	<b>PR REQ. QTY</b><br><?php echo $pr_req_qty; ?>
            </div>
            <div class="col-lg-2">
            	<b>REMARKS</b><br><?php echo $remarks; ?>
            </div>
            <div class="col-lg-2">
            	<b>CREATED BY</b><br><?php echo $requested_by; ?>
            </div>
         </div><br><br>
         
         <div class="row">
            <div class="col-lg-2">
            	<b>CREATED DATE</b><br><?php echo $requested_date; ?>
            </div>
            <div class="col-lg-2">
            	<b>STATUS</b><br><?php echo $status; ?>
            </div>
            <div class="col-lg-2">
            	<b>AGE</b><br><?php echo $age; ?>
            </div>
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
            </div>
        </div>
        
        <input type="hidden" name="sno" id="sno" value="<?php echo $sno; ?>">
        
        <div class="row">
            <div class="col-lg-5"></div>
            <div class="col-lg-2"><input type="submit" name="delete" id="delete" value="DELETE" class="form-control"></div>
            <div class="col-lg-5"></div>    
        </div>
    </form>
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
<?php include 'footer.php'; ?>

<script>
function reqd(){
	var r = confirm("Do you want to delete the item entry...");
	if(!r){
		return false;
	}
}
</script>