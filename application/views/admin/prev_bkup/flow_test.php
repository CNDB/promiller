<?php include'header.php'; ?>
<style>
	button.accordion {
		background-color:#ddd;
		color: #444;
		cursor: pointer;
		padding: 2px;
		width: 100%;
		border: none;
		text-align: center;
		font-weight:bold;
		outline: none;
		font-size: 12px;
		transition: 0.4s;
		border-radius:8px;
	}
	
	button.accordion.active, button.accordion:hover {
		background-color: #999999;
	}
	
	button.accordion:after {
		content: '\02795';
		font-size: 13px;
		color: #777;
		float: right;
		margin-left: 5px;
	}
	
	button.accordion.active:after {
		content: "\2796";
	}
	
	div.panel {
		padding: 0 5px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: 0.6s ease-in-out;
		opacity: 0;
		margin-bottom:4px;
	}
	
	div.panel.show {
		opacity: 1;
		max-height: 300px;
	}
	
	table thead tr{
		display:block;
	}
	
	table th,table td{
		width:300px;
	}
	
	table  tbody{		
		display:block;
		height:200px;
		overflow:auto;
	}
</style>

<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">
              Complete History Of Purchase Order And Purchase Request</h4>
        </div>
    </div><br />
    <!-- ********* Fresh Purchase Requests ************ -->
    <div class="row">
    	<div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <button class="accordion" style="color:black; font-weight:bold;">ERP FRESH PR PENDING FOR LIVE PR CREATION
            <?php $counter1 = 0; foreach ($h->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; }?>
            <?php echo " (".$counter1.") "; ?>
            </button>
            <div class="panel">
                <!--<lable>SORT : </lable>
                <select>
                    <option>ALL</option>
                    <option>FPR</option>
                    <option>IPR</option>
                    <option>LPR</option>
                </select>-->
                <table class="table table-bordered" id="dataTable" border="1">
                      <tr style="background-color:#CCC">
                        <td>PR NO.</td>
                        <td>ITEM CODE</td>
                        <td>ITEM DESC.</td>
                        <td>PR AGE</td>
                        <td>ERP CREATED BY</td>
                      </tr>
					  <?php 
                        foreach($h->result() as $row){
                            $prnum        = $row->preqm_prno;
                            //$item_code    = $row->prqit_itemcode;
                            //$item_desc    = $row->lov_itmvardesc;
                            $age          = $row->diff;
							$erp_created_by = $row->preqm_createdby;
							
							$sql = "select * from scmdb..prq_prqit_item_detail a, scmdb..itm_lov_varianthdr b where a.prqit_itemcode = b.lov_itemcode
									and a.prqit_prno = '$prnum'";
							$query = $this->db->query($sql);
							foreach ($query->result() as $row) {
								$item_code    = $row->prqit_itemcode;
								$item_desc    = $row->lov_itmvardesc;
							}
                            
                      ?>
                      <tr>
                        <td><a href="<?php echo base_url(); ?>index.php/createpoc/create_pr/<?php echo $prnum; ?>" target="_blank">
                            <?php echo $prnum; ?></a></td><!--PR Number-->
                        <td><?php echo $item_code; ?></td><!--Item Code-->
                        <td><?php echo $item_desc; ?></td><!--Item Desc-->
                        <td><?php echo $age;  ?></td><!--Age (In Months)-->
                        <td><?php echo $erp_created_by;  ?></td><!--ERP Created By-->
                      </tr>
                      <?php } ?>
                  </table> 
            </div>
            <!-- ********* Fresh Purchase Requests end ************ -->
            
            <!-- ********* Attach Drawing in purchase request ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">ATTACH DRAWING
            <?php $counter = 0; foreach ($draw->result() as $row) {  $counter++; $prnum = $row->pr_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                      <tr style="background-color:#CCC">
                        <td>PR NO.</td>
                        <td>ITEM CODE</td>
                        <td>ITEM DESC</td>
                        <td>DRAWING NO.</td>
                        <td>PR AGE</td>
                      </tr>
					  <?php foreach($draw->result() as $row){
                              $prnum = $row->pr_num;
                              $item_code    = $row->item_code;
                              $item_desc    = $row->itm_desc;
                              $drawing_no = $row->drawing_no;
                              $diff = $row->diff;
                      ?>
                      <tr>
                        <td><a href="<?php echo base_url(); ?>index.php/drawingc/create_pr/<?php echo $prnum; ?>" target="_blank">
                            <?php echo $prnum; ?></a></td><!--PR Number-->
                        <td><?php echo $item_code; ?></td><!--Itemcode-->
                        <td><?php echo $item_desc; ?></td><!--Item Desc-->
                        <td><?php echo $drawing_no; ?></td><!--Drawing No-->
                        <td><?php echo $diff;  ?></td><!--Age (In Days)-->
                      </tr>
                      <?php } ?>
                  </table>
            </div>
            <!-- ********* Attach Drawing in purchase request end ************ -->
            
            <!-- ********* Live purchase Request Pending For Live Authorization ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">Live purchase Request Pending For Live Authorization
            <?php $counter1 = 0; foreach ($auth_pr->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>CATEGORY</td>
                    <td>PROJECT NAME</td>
                    <td>PR NO.</td>
                    <td>ITEM CODE</td>
                    <!--<td>ITEM DESC.</td>-->
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					foreach($auth_pr->result() as $row)
						{  
						  $category = $row->category;
						  $project_name = $row->project_name;
						  $prnum = $row->pr_num;
						  $item_code = $row->item_code;
						  $item_desc = $row->itm_desc;
						  $age = $row->diff;
                  ?>
                  <tr>
                    <td><?php echo $category; ?></td><!--Category-->
                    <td><?php echo $project_name; ?></td><!--Project Name-->
                    <td><a href="<?php echo base_url(); ?>index.php/createpoc/view_pr_auth/<?php echo $prnum; ?>" target="_blank">
                    <?php echo $prnum; ?></a></td><!--PR Number-->
                    <td><?php echo $item_code; ?></td><!--Item Code-->
                    <?php /*?><td><?php echo $item_desc; ?></td><?php */?>
                    <td><?php echo $age;  ?></td><!--Age (In Days)-->
                  </tr>
                  <?php } ?>
                </table>
            </div>
            <!-- ********* Live purchase Request Pending For Live Authorization ************ -->
            
            <!-- ********* ERP Authorized Purchase Requests Whose Purchase Order Is Not Created************ -->
            <button class="accordion" style="color:black; font-weight:bold;">ERP AUTHORIZED PR PENDING FOR ERP PO CREATION
            <?php 
					$shown=1; 
					foreach ($j->result() as $row) {
						$prno = $row->preqm_prno;
						$pr_itemcode = $row->prqit_itemcode;
						$pr_auth_qty = $row->prqit_authqty;
						
						$max_no='';
						$sql1 = "SELECT max(pomas_poamendmentno) as max1  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
								  WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono";
						
						$query1 =$this->db->query($sql1)->row();
						
						$max_no = $query1->max1;
						
						$query2 = "SELECT sum(a.poprq_pocovqty) as poprq_pocovqty
						FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b,
						SCMDB..po_poitm_item_detail c
						WHERE a.poprq_prno = '$prno' and a.poprq_pono = b.pomas_pono
						and b.pomas_poamendmentno ='$max_no' and b.pomas_pono = c.poitm_pono
						and c.poitm_polineno = a.poprq_polineno
						and c.poitm_poamendmentno = a.poprq_poamendmentno
						and c.poitm_poamendmentno = b.pomas_poamendmentno
						and c.poitm_itemcode = '$pr_itemcode' and b.pomas_podocstatus not in('DE','CA','OP')
						group by c.poitm_itemcode";
							 
						$sql2 =$this->db->query($query2)->row();
						
						$po_itm_qty = $sql2->poprq_pocovqty;
						
						if($po_itm_qty == ''){
							
							$po_itm_qty = 0;
							
						}
						
						$query3 = "SELECT pomas_podocstatus,pomas_pono  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
						 WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono and b.pomas_poamendmentno = '$max_no'";
									 
						$query3 =$this->db->query($sql3)->row();
						
						$po_status = $query3->pomas_podocstatus;
						$po_no = $query3->pomas_pono;
						
						if($po_itm_qty < $pr_auth_qty){
							echo "<input type='hidden' name=pr_num value='$prno' />";
							$shown++;
						}	
					} 
					echo "( ".$shown. " )";
			?>
            </button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <!--<td>CATEGORY</td>
                    <td>PROJECT NAME</td>-->
                    <td>S.No.</td>
                    <td>PR NO.</td>
                    <td>ITEM CODE</td>
                    <td>ITEM DESC.</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
				  	$shown=1;
					foreach($j->result() as $row)
						{  
						    $prno = $row->preqm_prno; 
							$prqit_balqty = $row->prqit_balqty;
							$pr_auth_qty = $row->prqit_authqty;
							$pr_itemcode = $row->prqit_itemcode;
							$loi_itemdesc = $row->loi_itemdesc;
							$pr_createdby = $row->preqm_createdby;
							$pr_lastmodifiedby = $row->preqm_lastmodifiedby;
							$pr_createdDate = $row->pr_createdDate;
							$pr_needDate = $row->pr_needDate;
							$prqit_puom = $row->prqit_puom;
							$prqit_reqdqty = $row->prqit_reqdqty;
							$pr_age = $row->pr_age;
							
							$max_no='';
							$sql1 = "SELECT max(pomas_poamendmentno) as max1  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
									  WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono";
							$query1 =$this->db->query($sql1)->row();
							
							$max_no = $query1->max1;
							
							$query2 = "SELECT sum(a.poprq_pocovqty) as poprq_pocovqty
							FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b,
							SCMDB..po_poitm_item_detail c
							WHERE a.poprq_prno = '$prno' and a.poprq_pono = b.pomas_pono
							and b.pomas_poamendmentno ='$max_no' and b.pomas_pono = c.poitm_pono
							and c.poitm_polineno = a.poprq_polineno
							and c.poitm_poamendmentno = a.poprq_poamendmentno
							and c.poitm_poamendmentno = b.pomas_poamendmentno
							and c.poitm_itemcode = '$pr_itemcode' and b.pomas_podocstatus not in('DE','CA','OP')
							group by c.poitm_itemcode";
							 
							$sql2 =$this->db->query($query2)->row();
							
							$po_itm_qty = $sql2->poprq_pocovqty;
							
							if($po_itm_qty == ''){
								
								$po_itm_qty = 0;
								
							}
							
							$query3 = "SELECT pomas_podocstatus,pomas_pono  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
									 WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono
									 and b.pomas_poamendmentno = '$max_no'";
									 
							$query3 =$this->db->query($sql3)->row();
							
							
							$po_status = $query3->pomas_podocstatus;
							$po_no = $query3->pomas_pono;
							
							//echo "po--".$po_itm_qty."--pr".$pr_auth_qty."--".$prno."<br/>";
							if($po_itm_qty < $pr_auth_qty){
							
							
                  ?>
                  <tr>
                    <?php /*?><td><?php echo $category; ?></td><!--Category-->
                    <td><?php echo $project_name; ?></td><!--Project Name--><?php */?>
                    <td><?php echo $shown; ?></td>
                    <td><a href="<?php echo base_url(); ?>index.php/createpoc_lvl1/create_pr/<?php echo $prno; ?>" target="_blank">
                    <?php echo $prno; ?></a></td><!--PR Number-->
                    <td><?php echo $pr_itemcode; ?></td><!--Item Code-->
                    <td><?php echo $loi_itemdesc; ?></td>
                    <td><?php echo $pr_age;  ?></td><!--Age (In Days)-->
                  </tr>
                  <?php $shown++; } } ?>
                </table>
            </div>
            <!-- ********* ERP Authorized Purchase Requests end ************ -->
            
            <!-- ********* Fresh Purchase Order ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">ERP FRESH PO'S PENDING FOR LIVE PO CREATION
            <?php $counter = 0; foreach ($i->result() as $row) {  $counter++; $pono = $row->pomas_pono; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  foreach ($i->result() as $row) { 
					  $pono = $row->pomas_pono;
					  $po_basic_value = $row->pomas_pobasicvalue;
					  $po_cst_value = $row->pomas_tcdtotalrate; 
					  $po_tax_value = $row->pomas_tcal_total_amount;
					  $po_total_value = $po_basic_value + $po_cst_value + $po_tax_value;
					  $po_age = $row->diff;
					  
					  $sql1 = "select *, datediff(DAY,c.preqm_prdate,getdate()) as diff1 from scmdb..po_pomas_pur_order_hdr a, 
							   scmdb..po_poprq_poprcovg_detail b, scmdb..prq_preqm_pur_reqst_hdr c,
							   scmdb..supp_spmn_suplmain d where a.pomas_pono = b.poprq_pono and b.poprq_prno = c.preqm_prno 
							   and a.pomas_suppliercode = d.supp_spmn_supcode and b.poprq_pono = '$pono'";
					  $query1 = $this->db->query($sql1);
					  foreach ($query1->result() as $row) {
						$supp_name = $row->supp_spmn_supname;
					  	$pr_age = $row->diff1;
					  }
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/createpodc/create_po/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--Supplier Name-->
                    <td>
                        <?php
                            echo number_format($po_total_value,2);
                        ?>
                    </td><!--Order Value-->
                    <td><?php echo $po_age;  ?></td><!--PO Age (In Days)-->
                    <td><?php echo $pr_age;  ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?>
              </table>
            </div>
            <!-- ********* Fresh Purchase Order end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 1 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO PENDING FOR AUTHORIZATION LEVEL1 <!--Gaurav Mangal Sir-->
            <?php $counter = 0; foreach ($m_lvl1->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
                    $counter = 0; 
                    foreach ($m_lvl1->result() as $row) 
                        {  
                            $counter++; 
                            $pono = $row->po_num;
                            $supp_name = $row->po_supp_name;
                            $po_basic_value = $row->pomas_pobasicvalue;
                            $po_cst_value   = $row->pomas_tcdtotalrate; 
                            $po_tax_value   = $row->pomas_tcal_total_amount;
                            $po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);

                  ?>
                  <tr>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/createpodc/create_po_lvl1/<?php echo $pono; ?>" target="_blank">
                            <?php echo $pono; ?>
                        </a>
                    </td><!--PO Number-->
                    <td><?php echo $row->po_supp_name; ?></td><!--Supplier Name-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--Order Value-->
                    <td><?php echo $row->diff; ?></td><!--PO Age (In Days)-->
                    <td><?php echo $row->diff1;  ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?>
              </table>
            </div>
            <!-- ********* Authorize Purchase Order Level 1 end ************ -->
            
            <!-- ********* Disapproved Purchase Order Level 1 ************ -->
            <?php $counter = 0; foreach ($disaprve_lvl1->result() as $row) 
			{
			  $counter++; $pono = $row->po_num; 
			  if ($counter >0)
			  { 
				echo '<style>			
						#label 
							{
								background-color: yellow;
								animation-name: example;
								animation-duration: 2s;
								animation-iteration-count: infinite;
							}
						
						@keyframes example 
							{
								0%   {background-color: yellow;}
								100%  {background-color: red;}
							}
					  </style>';
			   }
			 }
            ?>
            <button class="accordion" id="label" style="color:black; font-weight:bold;">DISAPPROVED LIVE PO'S AT LEVEL 1
            <?php echo " ( ".$counter." )"; ?>
            </button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php foreach ($disaprve_lvl1->result() as $row) { 
                    $pono           = $row->po_num;
                    $po_basic_value = $row->pomas_pobasicvalue;
                    $po_cst_value   = $row->pomas_tcdtotalrate; 
                    $po_tax_value   = $row->pomas_tcal_total_amount;
                    $po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value); 
                  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/disapproved_poc_lvl1/create_po/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $row->po_supp_name; ?></td><!--PR Number-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--ITEM CODE-->
                    <td><?php echo $row->diff; ?></td><!--TRANSANCTION UOM-->
                    <td><?php echo $row->diff;  ?></td><!--Age (In Days)-->
                  </tr>
                  <?php } ?>
              </table>
            </div>
            <!-- ********* Disapproved Purchase Order Level 1 end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 2 ************ -->                              
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S PENDING FOR AUTHORIZATION LEVEL2 <!--Abninav Sir-->
            <?php $counter = 0; foreach ($m->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
				  <?php 
                    $counter = 0; 
                    foreach ($m->result() as $row) 
                    {  
					$pono = $row->po_num;
					$supp_name = $row->po_supp_name;
					$po_age  = $row->diff;
					 
							 
					$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
					$query2 = $this->db->query($sql2);
					foreach ($query2->result() as $row) {
						$amend_no = $row->amend_no;
                    }
					
					$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
					scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
							 
					$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
							 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
							 
					$query3 = $this->db->query($sql3);
					foreach ($query3->result() as $row) {
						$po_basic_value = $row->pomas_pobasicvalue;
						$po_cst_value   = $row->pomas_tcdtotalrate; 
						$po_tax_value   = $row->pomas_tcal_total_amount;
						$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
                    }
					
					$query4 = $this->db->query($sql4);
					foreach ($query4->result() as $row) {
						$pr_age = $row->diff1;
                    }
                  ?>
                  <tr>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/createpodc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                            <?php echo $pono; ?>
                        </a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
				  <?php 
                   }
                  ?>
                </table>
            </div>
            <!-- ********* Authorize Purchase Order Level 2 end ************ --> 
            
            <!-- ********* Disapproved Purchase Order Level 2 ************ -->
            <?php $counter = 0; foreach ($disaprve->result() as $row) 
            {
			  $counter++; $pono = $row->po_num; 
			  if ($counter >0)
			  { 
				echo '<style>
						#label1 
						{
							background-color: yellow;
							animation-name: example;
							animation-duration: 2s;
							animation-iteration-count: infinite;
						}
						
						@keyframes example 
						{
							0%   {background-color: yellow;}
							100%  {background-color: red;}	
						}
					  </style>';
			   }
             }
            ?>
            <button class="accordion" id="label1" style="color:black; font-weight:bold;">DISAPPROVED LIVE PO'S AT LEVEL 2
            <?php echo " ( ".$counter." )"; ?>
            </button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php foreach ($disaprve->result() as $row) { 
                    $pono = $row->po_num;
					$supp_name = $row->po_supp_name;
                    $po_total_value = $row->po_total_value;
                    $po_age         = $row->diff;
                    $sql1 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
                                     where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
                    $query1 = $this->db->query($sql1);
                    foreach ($query1->result() as $row) {
                        $pr_age = $row->diff1;
                    } 
                  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/disapproved_poc/create_po/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--Purchase Order Date-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--ORDER VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age;  ?></td><!--PR Age (In Months)-->
                  </tr>
                  <?php } ?>
              </table>
            </div>
            <!-- ********* Disapproved Purchase Order Level 2 end ************ -->
            
            <!-- ********* PO Send Supplier For Purchase ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S PENDING IN SEND TO SUPPLIER FOR PURCHASE 
            <?php $counter = 0; foreach ($n->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
				  	$counter = 0; 
                    foreach ($n->result() as $row) 
                    {  
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
						 
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/sendsupppurc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?>
                </table>
            </div> 
            <!-- ********* PO Send Supplier For Purchase end ************ -->
            
            <!-- ********* PO pending for issuance of manufacturing clearance from planning ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S PENDING IN ISSUANCE OF MANUFACTURING CLEARNACE 
            FROM PLANNING 
            <?php $counter = 0; foreach ($mc_planning->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?>
            </button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>PROJECT NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
				  $counter = 0; 
				  foreach ($mc_planning->result() as $row){ 
				   
					$counter++; 
					$pono = $row->po_num;
					$supp_name = $row->po_supp_name;
					$po_age  = $row->diff;
							 
					$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
					$query2 = $this->db->query($sql2);
					foreach ($query2->result() as $row) {
						$amend_no = $row->amend_no;
					}
					
					$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
					scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
							 
					$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
							 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
					
					$sql5 = "select project_name from tipldb..insert_po a, tipldb..pr_submit_table b where a.po_ipr_no = b.pr_num and a.po_num = '$pono'";
							 
					$query3 = $this->db->query($sql3);
					foreach ($query3->result() as $row) {
						$po_basic_value = $row->pomas_pobasicvalue;
						$po_cst_value   = $row->pomas_tcdtotalrate; 
						$po_tax_value   = $row->pomas_tcal_total_amount;
						$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
					}
					
					$query4 = $this->db->query($sql4);
					foreach ($query4->result() as $row) {
						$project_name = $row->diff1;
						$pr_age = $row->diff1;
					}
					
					$query5 = $this->db->query($sql5);
					foreach ($query5->result() as $row) {
						$project_name = $row->project_name;
					}
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/mc_planningc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo $project_name; ?></td><!--PROJECT NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?>
                </table>
            </div>
            <!-- ********* PO pending for issuance of manufacturing clearance from planning end ************ -->
                
            <!-- ********* PO pending for issuance of manufacturing clearance from Purchase dept. ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S PENDING FOR ISSUANCE OF MANUFACTURING CLEARANCE
            <?php $counter = 0; foreach ($o->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>PROJECT NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  $counter = 0; 
					  foreach ($o->result() as $row) {  
						$counter++; 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
						 
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								
						$sql5 = "select project_name from tipldb..insert_po a, tipldb..pr_submit_table b where a.po_ipr_no = b.pr_num and a.po_num = '$pono'";	
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						}
						
						$query5 = $this->db->query($sql5);
						foreach ($query5->result() as $row) {
							$project_name = $row->project_name;
						} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/manufact_clerancec/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo $project_name; ?></td><!--PROJECT NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?>
                </table>
            </div> 
            <!-- ********* PO pending for issuance of manufacturing clearance from Purchase dept. end ************ -->
             
             <!-- ********* PO awaited for acknowledgement from supplier ************ -->                          
            <button class="accordion" style="color:black; font-weight:bold;">PENDING IN ACKNOWLEDGEMENT FROM THE SUPPLIER
            <?php $counter = 0; foreach ($r->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
				  $counter = 0; 
				  foreach ($r->result() as $row) {  
					$counter++; 
					$pono = $row->po_num;
					$supp_name = $row->po_supp_name;
					$po_age  = $row->diff;
							 
					$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
					$query2 = $this->db->query($sql2);
					foreach ($query2->result() as $row) {
						$amend_no = $row->amend_no;
					}
					
					$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
					scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
							 
					$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
							 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
							 
					$query3 = $this->db->query($sql3);
					foreach ($query3->result() as $row) {
						$po_basic_value = $row->pomas_pobasicvalue;
						$po_cst_value   = $row->pomas_tcdtotalrate; 
						$po_tax_value   = $row->pomas_tcal_total_amount;
						$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
					}
					
					$query4 = $this->db->query($sql4);
					foreach ($query4->result() as $row) {
						$pr_age = $row->diff1;
					} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/acknowledgec/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?>
                </table>
            </div> 
            <!-- ********* PO awaited for acknowledgement from supplier end ************ -->
            
            <!-- ********* PO pending for test certificate upload ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">PENDING FOR TEST CERTIFICATES IF REQ.
            <?php $counter = 0; foreach ($tc->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  $counter = 0; 
					  foreach ($tc->result() as $row) {  
						$counter++; 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						}
						   
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/test_certc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?> 
                </table>
            </div>
            <!-- ********* PO pending for test certificate upload end ************ -->
            
            <!-- ********* PO awaited for performa invoice from supplier ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">AWAITED PERFORMA INVOICE FROM SUPPLIER 
            <?php $counter = 0; foreach ($pi->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  foreach ($pi->result() as $row) { 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/createpic/create_po/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?> 
              </table>
             </div>
             <!-- ********* PO awaited for performa invoice from supplier end ************ -->
             
             <!-- ********* PO awaited for authorization of performa invoice if performa invoice amount in greater than 50000 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">PENDING IN PERFORMA INVOICE AUTHORIZATION (IF PI AMOUNT > 50,000) 
            <?php $counter = 0; foreach ($pi2->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  foreach ($pi2->result() as $row) { 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/createpic/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?> 
              </table>
             </div>
            <!-- ********* PO awaited for authorization of performa invoice if performa invoice amount in greater than 50000 end ************ -->
            
            <!-- ********* Performa invoice payment to supplier by accounts department ************ --> 
            <button class="accordion" style="color:black; font-weight:bold;">PI PAYMENT TO SUPPLIER 
            <?php $counter = 0; foreach ($pips->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  foreach ($pips->result() as $row) { 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/pipayment_suppc/create_po/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?> 
              </table>
             </div>
             <!-- ********* Performa invoice payment to supplier by accounts department end************ --> 
             
             <!-- ********* PO awaited for issuance of post dated cheque ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">PENDING FOR ISSURANCE OF PDC
            <?php $counter = 0; foreach ($pdc->result() as $row) {  $counter++; $pono = $row->pomas_pono; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
				  foreach ($pdc->result() as $row) {
					$pono = $row->po_num;
					$supp_name = $row->po_supp_name;
					$po_age  = $row->diff;
							 
					$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
					$query2 = $this->db->query($sql2);
					foreach ($query2->result() as $row) {
						$amend_no = $row->amend_no;
					}
					
					$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
					scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
							 
					$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
							 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
							 
					$query3 = $this->db->query($sql3);
					foreach ($query3->result() as $row) {
						$po_basic_value = $row->pomas_pobasicvalue;
						$po_cst_value   = $row->pomas_tcdtotalrate; 
						$po_tax_value   = $row->pomas_tcal_total_amount;
						$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
					}
					
					$query4 = $this->db->query($sql4);
					foreach ($query4->result() as $row) {
						$pr_age = $row->diff1;
					}  
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/createpdcc/create_po/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?> 
              </table>
             </div>
             <!-- ********* PO awaited for issuance of post dated cheque end ************ -->
             
             <!-- ********* PO awaited for freight payment by accounts department if freight terms is ex-works ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"> PENDING FOR FREIGHT PAYMENT TO SUPPLIER 
            <?php $counter = 0; foreach ($freight->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  foreach ($freight->result() as $row) { 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/createfreightc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php } ?>
              </table>
             </div>
             <!-- ********* PO awaited for freight payment by accounts department if freight terms is ex-works end ************ -->
             
            <!-- ********* PO awaited for road permit from supplier ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S AWAITED FOR ROAD PERMIT
            <?php $counter = 0; foreach ($t->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
				  $counter = 0; 
				  foreach ($t->result() as $row) {  
					$counter++; 
					$pono = $row->po_num;
					$supp_name = $row->po_supp_name;
					$po_age  = $row->diff;
							 
					$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
					$query2 = $this->db->query($sql2);
					foreach ($query2->result() as $row) {
						$amend_no = $row->amend_no;
					}
					
					$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
					scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
							 
					$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
							 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
							 
					$query3 = $this->db->query($sql3);
					foreach ($query3->result() as $row){
						$po_basic_value = $row->pomas_pobasicvalue;
						$po_cst_value   = $row->pomas_tcdtotalrate; 
						$po_tax_value   = $row->pomas_tcal_total_amount;
						$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
					}
					
					$query4 = $this->db->query($sql4);
					foreach ($query4->result() as $row){
						$pr_age = $row->diff1;
					} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/dispatchc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?> 
                </table>
            </div>
            <!-- ********* PO awaited for road permit from supplier end ************ -->
             
            <!-- ********* PO awaited for Dipatch Detail ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S PENDING FOR DELIVERY DETAILS
            <?php $counter = 0; foreach ($s->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  $counter = 0; 
					  foreach ($s->result() as $row) {  
					  	$counter++; 
						$pono = $row->po_num;
						$supp_name = $row->po_supp_name;
						$po_age  = $row->diff;
								 
						$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
						$query2 = $this->db->query($sql2);
						foreach ($query2->result() as $row) {
							$amend_no = $row->amend_no;
						}
						
						$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
						scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
								 
						$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
								 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
								 
						$query3 = $this->db->query($sql3);
						foreach ($query3->result() as $row) {
							$po_basic_value = $row->pomas_pobasicvalue;
							$po_cst_value   = $row->pomas_tcdtotalrate; 
							$po_tax_value   = $row->pomas_tcal_total_amount;
							$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
						}
						
						$query4 = $this->db->query($sql4);
						foreach ($query4->result() as $row) {
							$pr_age = $row->diff1;
						}
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/deliveryc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?>
                </table> 
            </div> 
            <!-- ********* PO awaited for Dipatch Detail end ************ -->
            
            <!-- ********* PO pending for gate entry start ************ -->
            <button class="accordion" style="color:black; font-weight:bold;">LIVE PO'S PENDING FOR GATE ENTRY
            <?php $counter = 0; foreach ($z->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) ";?></button>
            <div class="panel">
              <table class="table table-bordered" id="dataTable" border="1">
                  <tr style="background-color:#CCC">
                    <td>PO NO.</td>
                    <td>SUPPLIER NAME</td>
                    <td>ORDER VALUE</td>
                    <td>PO AGE</td>
                    <td>PR AGE</td>
                  </tr>
                  <?php 
					  $counter = 0; 
					  foreach ($z->result() as $row) {  
						  	$counter++; 
						  	$pono = $row->po_num;
							$supp_name = $row->po_supp_name;
							$po_age  = $row->diff;
									 
							$sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
							$query2 = $this->db->query($sql2);
							foreach ($query2->result() as $row) {
								$amend_no = $row->amend_no;
							}
							
							$sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
							scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
									 
							$sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
									 where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
									 
							$query3 = $this->db->query($sql3);
							foreach ($query3->result() as $row) {
								$po_basic_value = $row->pomas_pobasicvalue;
								$po_cst_value   = $row->pomas_tcdtotalrate; 
								$po_tax_value   = $row->pomas_tcal_total_amount;
								$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
							}
							
							$query4 = $this->db->query($sql4);
							foreach ($query4->result() as $row) {
								$pr_age = $row->diff1;
							} 
				  ?>
                  <tr>
                    <td><a href="<?php echo base_url(); ?>index.php/grcptc/create_po_lvl2/<?php echo $pono; ?>" target="_blank">
                        <?php echo $pono; ?></a>
                    </td><!--PO Number-->
                    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
                    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
                    <td><?php echo $po_age; ?></td><!--PO AGE-->
                    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
                  </tr>
                  <?php }?>
              </table>
            </div>
            <!-- ********* PO pending for gate entry end ************ -->
        </div>
        <div class="col-lg-2">
        </div>
    </div><br />
  </section>
</section>


<script>
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
	  }
	}
</script>
<?php include'footer.php'; ?>