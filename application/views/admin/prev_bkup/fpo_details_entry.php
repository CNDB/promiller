<?php
	$sql_prev_det = "select top 1 * from tipldb..po_master_table a, tipldb..insert_po b where a.po_num = '$po_num' and a.po_num = b.po_num";
	$qry_prev_det = $this->db->query($sql_prev_det);
	foreach($qry_prev_det->result() as $row){
		$order_value = $row->po_total_value;
		$freight_type = $row->freight_type;
		$exp_mat_rec_date = $row->po_lead_time;
		$currency = $row->currency;
		$deli_type = $row->po_deli_type;
		$approx_freight = $row->approx_freight;
		$ld_applicable = $row->ld_applicable;
		$po_spcl_inst_frm_supp = $row->po_spcl_inst_frm_supp;
		$payment_fpo = $row->payment_fpo;
		$po_rmks = $row->po_rmks;
	}
?>
<div class="row">
    <div class="col-lg-2">
        <b>Supplier Name:</b><br />
        <?php  echo $supp_name; ?>
        <input type="hidden" name="po_supp_name" value="<?php echo $supp_name; ?>" />   	
    </div>
    <div class="col-lg-2">
        <b>Supplier Address: </b><br />
        <?php echo $comp_add; ?>
        <input type='hidden' name='po_supp_add' value='<?php echo $comp_add; ?>' />
    </div>
    <div class="col-lg-2">
        <b>Supp Email Address: </b><b style="color:#F00">&nbsp;*</b><br />
        <?php
			echo $supplier_email;
            echo "<input type='hidden' name='supplier_email' id='supplier_email' value='$supplier_email' />";
        ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Phone Number: </b><b style="color:#F00">&nbsp;*</b><br />
        <?php
			echo $supplier_phone_no;
			echo "<input type='hidden' name='supplier_phone_no' id='supplier_phone_no' value='$supplier_phone_no' />";
        ?>
    </div>
    <div class="col-lg-2">
        <b>Contact Person:</b><b style="color:#F00">&nbsp;*</b><br />
        <?php
			echo $contact_person;
            echo "<input type='hidden' name='contact_person' id='contact_person' value='$contact_person' />"; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Order Value: </b><br />
        <input type="text" name="po_total_value" id="po_total_value" value="<?php echo $grand_total_po; ?>" class="form-control" readonly />
    </div>
</div><br /><br />
<div class="row">
	<div class="col-lg-2">
        <b>PO Age: </b><br />
        <?php 
            echo $po_age."&nbsp;Days"; 
        ?>
    </div>
	<div class="col-lg-2">
        <b>IPR Age: </b><br />
        <?php 
            echo $ipr_age."&nbsp;Days"; 
        ?>
    </div>
    <div class="col-lg-2 higlight_cls">
        <b>Payment Terms :</b><br />
        <?php echo $payterm; ?>
        <input type="hidden" name="payterm" value="<?php echo $payterm; ?>" />
		<input type="hidden" name="payterm_desc" value="<?php echo $payterm_desc; ?>" />	
    </div>
    <div class="col-lg-2 higlight_cls">
        <b>Freight Terms:</b><br />
        <?php echo $freight1; ?>
        <input type="hidden" name="freight" value="<?php echo $freight; ?>" />
    </div>
    <div class="col-lg-2 higlight_cls">
        <b>Freight Place : </b><br />
        <?php echo $freight_place; ?>
        <input type="hidden" name="freight_place" value="<?php echo $freight_place; ?>" /> 
    </div>
    <div class="col-lg-2">
        <b>Carrier Name : </b><br />
        <?php echo $carrier_name; ?>
        <input type="hidden" name="carrier_name" value="<?php echo $carrier_name; ?>" />
    </div>
</div><br /><br />
<div class="row">
	<div class="col-lg-2 higlight_cls">
        <b>Insurance Term : </b><br />
        <?php echo $insurance_liablity; ?>
        <input type="hidden" name="insurance_liablity" value="<?php echo $insurance_liablity; ?>" />
    </div>
	<div class="col-lg-2 higlight_cls">
    	<b>Mode Of Transport :</b><br />
		<?php echo $transport_mode; ?>
    </div>
    <div class="col-lg-2">
    	<b>Freight Type</b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2"> 
        <select class='form-control' id='freight_type' name='freight_type' onChange='freight_cond(this.value);'>
        <?php
        if($freight == 'EX WORKS' || $freight == 'EXW')
        {   
			if($freight_type != ''){      
				echo " <option value='".$freight_type."'>".$freight_type."</option>
					   <option value=''>-- Select --</option>
					   <option value='To Pay'>To Pay</option>
					   <option value='Paid Billed'>Paid Billed</option>
					   <option value='Hand Delivery'>Hand Delivery</option>";
			} else {
				echo " <option value=''>-- Select --</option>
					   <option value='To Pay'>To Pay</option>
					   <option value='Paid Billed'>Paid Billed</option>
					   <option value='Hand Delivery'>Hand Delivery</option>";
			}
        }
        else if($freight == 'FOR')
        {
            echo "<option value='Prepaid By Supplier'>Prepaid By Supplier</option>";
        }
        else if($freight == 'FORD')
        {
            echo "<option value='Prepaid By Supplier'>Prepaid By Supplier</option>";
        }
        else
        {
            echo "<option value='select'>-- Select --</option>";
        }
        ?>
    </select> 
    </div>
    <div class="col-lg-2">
    	<b>Enter Expected Material Recipt Date</b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2">          
    	<input type="text" name="po_lead_time" id="datepicker1" value="<?php echo $exp_mat_rec_date; ?>" class="form-control" autocomplete="off"/>
    </div>
</div><br />
<div class="row">
	<div class="col-lg-2">
    	<b>Select Currency</b>
    </div>  
    <div class="col-lg-2">
		<?php
			if($po_three_letters == 'FPO'){
				
				if($currency != ''){
					echo"<select name='currency' id='currency' class='form-control'>
							<option value='".$currency."'>".$currency."</option>
							<option value=''>Select</option>
							<option value='INR'>INR</option>
							<option value='USD'>USD</option>
							<option value='GBP'>GBP</option>
							<option value='YEN'>YEN</option>
							<option value='EURO'>EURO</option>
							<option value='CNY'>CNY</option>
						</select>";
				} else {
					echo"<select name='currency' id='currency' class='form-control'>
							<option value=''>Select</option>
							<option value='INR'>INR</option>
							<option value='USD'>USD</option>
							<option value='GBP'>GBP</option>
							<option value='YEN'>YEN</option>
							<option value='EURO'>EURO</option>
						</select>";
				}
				
			} else {
			   echo"<input type='text' name='currency' id='currency' value='INR' readonly='readonly' class='form-control' />";
			}
        ?>
    </div>
    <div class="col-lg-2">
        <b>Delivery Type : </b><b style="color:#F00">&nbsp;*</b>
    </div>
    <div class="col-lg-2">
    <select class='form-control' name='po_deli_type' id='po_deli_type'>
     <?php
        if($freight == 'EX WORKS' || $freight == 'EXW')
        {
            if($deli_type != ''){
				echo "<option value='".$deli_type."'>".$deli_type."</option>
					  <option value=''>--Select--</option>";
			} else {
				echo "<option value=''>--Select--</option><option value=''>--Select--</option>";
			}
        } 
        else if($freight == 'FOR') 
        {
            echo "<option value='Godown Delivery'>Godown Delivery</option>";
        } 
        else if ($freight == 'FORD')
        {
            echo "<option value='Door Delivery'>Door Delivery</option>";
        }
		else 
		{
			echo "<option value='select'>--Select--</option>";
		}
     ?>
     </select>
    </div>
    <div class="col-lg-2">
    	<b>Enter Approx Freight : </b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2">
    <?php 
	if($freight == 'EX WORKS' || $freight == 'EXW')
	{   
		if($approx_freight != ''){   
        	echo "<input type='text' name='approx_freight' id='approx_freight' value='".$approx_freight."' class='form-control' onkeypress='return isNumber(event)'/>";
		} else {
			echo "<input type='text' name='approx_freight' id='approx_freight' value='' class='form-control' />";
		}
	} 
	else
	{
		echo "<input type='hidden' name='approx_freight' id='approx_freight' value='No Value' class='form-control' />";
	}
	?>
    </div>
</div><br />
<div class="row">
	<div class="col-lg-2">
    	<b>LD Applicable : </b><b style="color:#F00">&nbsp;*</b>
    </div>
    <div class="col-lg-2">
    	<select name="ld_applicable" id="ld_applicable" class="form-control">
        	<?php if($ld_applicable != ''){ ?>
            <option value="<?php echo $ld_applicable; ?>"><?php echo $ld_applicable; ?></option>
        	<option value="" disabled="disabled">--select--</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
            <?php } else { ?>
            <option value="" disabled="disabled">--select--</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
            <?php } ?>
        </select>
    </div> 
    <div class="col-lg-2">
    	<b>Enter Special Instructions For Supplier : </b>
    </div>
    <div class="col-lg-2">
    	<input type="text" name="spcl_inst_supp" id="spcl_inst_supp" value="<?php echo $po_spcl_inst_frm_supp; ?>"  class="form-control"/>
    </div> 
    <div class="col-lg-2">
    	<b>Freight Terms Reason:</b><b style="color:#F00">&nbsp;*</b>
    </div> 
    <div class="col-lg-2">
		<?php
            if($freight == 'EXW'){
                    echo "<textarea id='freight_rmks' name='freight_rmks' class='form-control'></textarea>";
            }
        ?>
    </div> 
</div><br />