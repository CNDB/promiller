 <?php include'header.php'; ?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PO DETAILS</h4>
        </div>
    </div><br />
    
    <?php /*?><div class="row">
    	<div class="col-lg-4">
        	<a href="<?php echo base_url(); ?>index.php/po_fy_reportc/export/" style="font-size:14px; font-weight:bold"><i>Export data to Excel</i></a>
        </div>
        <div class="col-lg-4">
        </div>
        <div class="col-lg-4">
        </div>
    </div><br><?php */?>
    
    <!--<div class="row">
    	<div class="col-lg-4">
        </div>
        <div class="col-lg-2">
        	<b>Select Financial Year :</b>
        </div>
        <div class="col-lg-2">
        	<select id="fy" name="fy" onChange="show_content(this.value);" class="form-control">
            	<option value="">select</option>
                <option value="FY-2016-17">FY-2016-17</option>
                <option value="FY-2015-16">FY-2015-16</option>
            </select>
        </div>
        <div class="col-lg-4">
        </div>
    </div><br>-->
    <?php //FY-2016-17 ?>
    <h1>FIN YEAR - 2016-17</h1>
    <div class="row" id="fy_16_17">
      <div class="col-lg-12">
       	<table class="table table-bordered" style="font-size:10px">
        	<tr style="background-color:#CCC">
            	<td><b>S. NO.</b></td>
            	<td><b>PO NUMBER</b></td>
                <td><b>SUPPLIER NAME</b></td>
                <td><b>ITEM CODE</b></td>
                <td><b>ITEM DESC</b></td>
                <td><b>ITEM CATEGORY</b></td>
                <td><b>ITEM QTY</b></td>
                <td><b>ITEM UNIT PRICE</b></td>
                <td><b>PO TOTAL VALUE</b></td>
                <td><b>FREIGHT CHARGES</b></td>
                <td><b>TOTAL TAX VALUE</b></td>
                <td><b>EXCISE</b></td>
                <td><b>SERVICE</b></td>
                <td><b>VAT</b></td>
            </tr>
            <?php
				$s_no = 0;
				foreach($all_po_2016_17->result() as $row){
					$s_no++;
					 $po_number        = $row->pomas_pono;
					 $supplier_name    = str_replace("'","",$row->supp_spmn_supname);
					 $item_code        = $row->poitm_itemcode;
					 $item_desc        = str_replace("'","",$row->ml_itemvardesc);
					 $category         = $row->class_desc;
					 $item_quantity    = $row->poitm_order_quantity;
					 $item_unit_price  = $row->poitm_po_cost;
					 $po_total_value   = $row->pomas_pobasicvalue;
					 $freight_charges  = $row->pomas_tcdtotalrate;
					 $total_tax        = $row->pomas_tcal_total_amount;
					 $fin_year         = "FY 2016-17";
					 
					 $sql_excise = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'EXCISE' and  tran_no='$po_number'";
					 $sql_service = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'SER' and  tran_no='$po_number'";
					 $sql_vat = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'VAT' and  tran_no='$po_number'";
					 
					 $query_excise = $this->db->query($sql_excise);
					 $query_service = $this->db->query($sql_service);
					 $query_vat = $this->db->query($sql_vat);
					 
					 foreach ($query_excise->result() as $row) {
						 $excise_tax_incl_amt    = $row->tax_incl_amt;
						 $excise_tax_excl_amt    = $row->tax_excl_amt;
						 $total_excise = $excise_tax_incl_amt - $excise_tax_excl_amt;
					 }
					 
					 foreach ($query_service->result() as $row) {
						 $service_tax_incl_amt    = $row->tax_incl_amt;
						 $service_tax_excl_amt    = $row->tax_excl_amt;
						 $total_service = $service_tax_incl_amt - $service_tax_excl_amt;
					 }
					 
					 foreach ($query_vat->result() as $row) {
						 $vat_tax_incl_amt    = $row->tax_incl_amt;
						 $vat_tax_excl_amt    = $row->tax_excl_amt;
						 $total_vat = $vat_tax_incl_amt - $vat_tax_excl_amt;
					 }
			?>
            <tr>
            	<td><?php echo $s_no; ?></td>
            	<td><?php echo $po_number; ?></td>
                <td><?php echo $supplier_name; ?></td>
                <td><?php echo $item_code; ?></td>
                <td><?php echo $item_desc; ?></td>
                <td><?php echo $category; ?></td>
                <td><?php echo number_format($item_quantity,2); ?></td>
                <td><?php echo number_format($item_unit_price,2); ?></td>
                <td><?php echo number_format($po_total_value,2); ?></td>
                <td><?php echo number_format($freight_charges,2); ?></td>
                <td><?php echo number_format($total_tax,2); ?></td>
                <td><?php echo number_format($total_excise,2); ?></td>
                <td><?php echo number_format($total_service,2); ?></td>
                <td><?php echo number_format($total_vat,2); ?></td>
            </tr>
            
            
            <?php
				}
			?>
        </table>
      </div>
    </div>
    <?php //FY-2015-16 ?>
    <h1>FIN YEAR - 2015-16</h1>
    <div class="row" id="fy_15_16">
      <div class="col-lg-12">
       	<table class="table table-bordered" style="font-size:10px">
        	<tr style="background-color:#CCC">
            	<td><b>S. NO.</b></td>
            	<td><b>PO NUMBER</b></td>
                <td><b>SUPPLIER NAME</b></td>
                <td><b>ITEM CODE</b></td>
                <td><b>ITEM DESC</b></td>
                <td><b>ITEM CATEGORY</b></td>
                <td><b>ITEM QTY</b></td>
                <td><b>ITEM UNIT PRICE</b></td>
                <td><b>PO TOTAL VALUE</b></td>
                <td><b>FREIGHT CHARGES</b></td>
                <td><b>TOTAL TAX VALUE</b></td>
                <td><b>EXCISE</b></td>
                <td><b>SERVICE</b></td>
                <td><b>VAT</b></td>
            </tr>
            <?php
				$s_no = 0;
				foreach($all_po_2015_16->result() as $row){
					$s_no++;
					 $po_number        = $row->pomas_pono;
					 $supplier_name    = str_replace("'","",$row->supp_spmn_supname);
					 $item_code        = $row->poitm_itemcode;
					 $item_desc        = str_replace("'","",$row->ml_itemvardesc);
					 $category         = $row->class_desc;
					 $item_quantity    = $row->poitm_order_quantity;
					 $item_unit_price  = $row->poitm_po_cost;
					 $po_total_value   = $row->pomas_pobasicvalue;
					 $freight_charges  = $row->pomas_tcdtotalrate;
					 $total_tax        = $row->pomas_tcal_total_amount;
					 $fin_year         = "FY 2015-16";
					 
					 $sql_excise = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'EXCISE' and  tran_no='$po_number'";
					 $sql_service = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'SER' and  tran_no='$po_number'";
					 $sql_vat = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'VAT' and  tran_no='$po_number'";
					 
					 $query_excise = $this->db->query($sql_excise);
					 $query_service = $this->db->query($sql_service);
					 $query_vat = $this->db->query($sql_vat);
					 
					 foreach ($query_excise->result() as $row) {
						 $excise_tax_incl_amt    = $row->tax_incl_amt;
						 $excise_tax_excl_amt    = $row->tax_excl_amt;
						 $total_excise = $excise_tax_incl_amt - $excise_tax_excl_amt;
					 }
					 
					 foreach ($query_service->result() as $row) {
						 $service_tax_incl_amt    = $row->tax_incl_amt;
						 $service_tax_excl_amt    = $row->tax_excl_amt;
						 $total_service = $service_tax_incl_amt - $service_tax_excl_amt;
					 }
					 
					 foreach ($query_vat->result() as $row) {
						 $vat_tax_incl_amt    = $row->tax_incl_amt;
						 $vat_tax_excl_amt    = $row->tax_excl_amt;
						 $total_vat = $vat_tax_incl_amt - $vat_tax_excl_amt;
					 }
			?>
            <tr>
            	<td><?php echo $s_no; ?></td>
            	<td><?php echo $po_number; ?></td>
                <td><?php echo $supplier_name; ?></td>
                <td><?php echo $item_code; ?></td>
                <td><?php echo $item_desc; ?></td>
                <td><?php echo $category; ?></td>
                <td><?php echo number_format($item_quantity,2); ?></td>
                <td><?php echo number_format($item_unit_price,2); ?></td>
                <td><?php echo number_format($po_total_value,2); ?></td>
                <td><?php echo number_format($freight_charges,2); ?></td>
                <td><?php echo number_format($total_tax,2); ?></td>
                <td><?php echo number_format($total_excise,2); ?></td>
                <td><?php echo number_format($total_service,2); ?></td>
                <td><?php echo number_format($total_vat,2); ?></td>
            </tr>
            
            <?php /*?><?php

			$sql_insert = "insert into tipldb..po_fy_report (po_number, supplier_name, item_code, item_desc, category, item_quantity, item_unit_price,   	            po_total_value, freight_charges, total_tax, total_excise, total_service, total_vat, fin_year) values ('".$po_number."','".$supplier_name."','".   	            $item_code."','".$item_desc."','".$category."','".$item_quantity."','".$item_unit_price."','".$po_total_value."','".$freight_charges."',
			'".$total_tax."','".$total_excise."','".$total_service."','".$total_vat."', '".$fin_year."')";
			
			//$this->db->query($sql_insert);
			
			?><?php */?>
            
            <?php
				}
			?>
        </table>
      </div>
    </div>
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->

<!--<script>
function show_content(a){
	if(a == 'FY-2016-17'){
		$('#fy_16_17').show();
		$('#fy_15_16').hide();
	} else if (a == 'FY-2015-16') {
		$('#fy_16_17').hide();
		$('#fy_15_16').show();
	} else {
		$('#fy_16_17').hide();
		$('#fy_16_17').hide();
	}
}
</script>-->
      
<?php include('footer.php'); ?>