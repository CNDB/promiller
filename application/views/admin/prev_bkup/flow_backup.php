<?php include'header.php'; ?>
<style>
	button.accordion {
		background-color:#ddd;
		color: #444;
		cursor: pointer;
		padding: 2px;
		width: 100%;
		border: none;
		text-align: center;
		font-weight:bold;
		outline: none;
		font-size: 12px;
		transition: 0.4s;
		border-radius:8px;
	}
	
	button.accordion.active, button.accordion:hover {
		background-color: #999999;
	}
	
	button.accordion:after {
		content: '\02795';
		font-size: 13px;
		color: #777;
		float: right;
		margin-left: 5px;
	}
	
	button.accordion.active:after {
		content: "\2796";
	}
	
	div.panel {
		padding: 0 5px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: 0.6s ease-in-out;
		opacity: 0;
		margin-bottom:4px;
	}
	
	div.panel.show {
		opacity: 1;
		max-height: 300px;
	}
	
	table thead tr{
		display:block;
	}
	
	table th,table td{
		width:300px;
	}
	
	table  tbody{		
		display:block;
		height:200px;
		overflow:auto;
	}
</style>

<!-- Ajax Javascripts  -->

<?php // Fresh Purchase Requests ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getfreshpr(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('fresh_pr').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('fresh_pr').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/fresh_pr", true);
	xmlhttp.send();
	
	}

</script>

<?php // Attach Drawing ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getattach_drawing(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('attach_drawing').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('attach_drawing').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/attach_drawing", true);
	xmlhttp.send();
	
	}

</script>

<?php // Authorize Purchase Request planning level1?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_pr(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_pr').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_pr').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_pr", true);
	xmlhttp.send();
	
	}

</script>

<?php // Disapproved Purchase Request planning level1?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pr_plan(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_pr_plan').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_pr_plan').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pr_plan", true);
	xmlhttp.send();
	
	}

</script>

<?php // Authorize Purchase Request planning level2?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_pr_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_pr_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_pr_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_pr_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php // Disapproved Purchase Request planning level2?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pr_plan_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_pr_plan_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_pr_plan_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pr_plan_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php // Live authorized pr pending for erp authorisation ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function geterp_not_auth_pr(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('erp_not_auth_pr').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('erp_not_auth_pr').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/erp_not_auth_pr", true);
	xmlhttp.send();
	
	}

</script>

<?php //Authorized Purchase Requests whose po is not created in erp ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getpr_po_report(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('pr_po_report').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('pr_po_report').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/pr_po_report", true);
	xmlhttp.send();
	
	}

</script>


<?php // Authorize Purchase Request Purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_pr_pur(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_pr_pur').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_pr_pur').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_pr_pur", true);
	xmlhttp.send();
	
	}

</script>

<?php //Disapproved Purchase Request Purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_pr_pur(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_pr_pur').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_pr_pur').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_pr_pur", true);
	xmlhttp.send();
	
	}

</script>

<?php //Fresh ERP Purchase Orders pending for live po creation ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getfresh_po(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('fresh_po').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('fresh_po').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/fresh_po", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO authorization level 1 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_po_lvl1(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_po_lvl1').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_po_lvl1').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_po_lvl1", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO disapproval level1	 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_po_lvl1(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_po_lvl1').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_po_lvl1').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_po_lvl1", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO authorization level 2 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getauth_po_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('auth_po_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('auth_po_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/auth_po_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO disapproval level2 ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdis_po_lvl2(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dis_po_lvl2').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dis_po_lvl2').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dis_po_lvl2", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO Pending for authorization in ERP ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getnot_auth_po(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('not_auth_po').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('not_auth_po').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/not_auth_po", true);
	xmlhttp.send();
	
	}

</script>

<?php //ERP Amended PO'S ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getamend_po(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('amend_po').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('amend_po').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/amend_po", true);
	xmlhttp.send();
	
	}

</script>

<?php //Purchase order send to supplier for purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getsupp_for_pur(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('supp_for_pur').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('supp_for_pur').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/supp_for_pur", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for acknowledgement from supplier ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getack_frm_supp(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ack_frm_supp').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('ack_frm_supp').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/ack_frm_supp", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for manufacturing clearance planning ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmc_planning(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mc_planning').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mc_planning').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mc_planning", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for manufacturing clearance purchase ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getmc_purchase(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('mc_purchase').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('mc_purchase').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/mc_purchase", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for test certificate uploadion ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function gettc_upload(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('tc_upload').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('tc_upload').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/tc_upload", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending Advance Given To Supplier ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getadvance_supplier(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('advance_supplier').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('advance_supplier').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/advance_supplier", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for pi creation ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getpi_creation(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('pi_creation').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('pi_creation').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/pi_creation", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for pi authorization ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getpi_authorize(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('pi_authorize').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('pi_authorize').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/pi_authorize", true);
	xmlhttp.send();
	
	}

</script>

<?php //PI payment tab ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getpi_payment_supp(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('pi_payment_supp').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('pi_payment_supp').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/pi_payment_supp", true);
	xmlhttp.send();
	
	}

</script>

<?php //PDC creation tab ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getcreate_pdc(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('create_pdc').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('create_pdc').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/create_pdc", true);
	xmlhttp.send();
	
	}

</script>

<?php //FREIGHT Payment Tab ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getfreight_payment(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('freight_payment').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('freight_payment').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/freight_payment", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO pending for dispatch instruction ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdispatch_inst(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('dispatch_inst').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('dispatch_inst').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/dispatch_inst", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO Pending for delivery details ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getdeli_details(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('deli_details').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('deli_details').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/deli_details", true);
	xmlhttp.send();
	
	}

</script>

<?php //PO Pending for gate entry ?>
<script type="text/javascript" language="javascript">

	//xmlhttp = new XMLHttpRequest();
	function getgate_entry(){
		
	  if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('gate_entry').innerHTML=xmlhttp.responseText;
		} else {
			document.getElementById('gate_entry').innerHTML="<strong>Waiting For Server...</strong>";
		}
	 }
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/flowc/gate_entry", true);
	xmlhttp.send();
	
	}

</script>

<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">
              Complete History Of Purchase Order And Purchase Request</h4>
        </div>
    </div><br />
    <!-- ********* Fresh Purchase Requests ************ -->
    <div class="row">
    	<div class="col-lg-2">
        </div>
        <div class="col-lg-8">
            <button class="accordion" style="color:black; font-weight:bold;" onMouseDown="getfreshpr()">
            ERP FRESH PR PENDING FOR LIVE PR CREATION
            <?php $counter1 = 0; foreach ($h->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; }?>
            <?php echo " (".$counter1.") "; ?>
            </button>
            <div class="panel">
                <div id="fresh_pr"></div>
            </div>
            <!-- ********* Fresh Purchase Requests end ************ -->
            
            <!-- ********* Attach Drawing in purchase request ************ -->
            <button class="accordion" style="color:black; font-weight:bold;" onMouseDown="getattach_drawing()">ATTACH DRAWING
            <?php $counter = 0; foreach ($draw->result() as $row) {  $counter++; $prnum = $row->pr_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
                <div id="attach_drawing"></div>
            </div>
            <!-- ********* Attach Drawing in purchase request end ************ -->
            
            <!-- ********* Live purchase Request Pending For Live Authorization Level1 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_pr()">
            LIVE PURCHASE REQUEST PENDING FOR AUTHORIZATION LEVEL1
            <?php $counter1 = 0; foreach ($auth_pr->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="auth_pr"></div>
            </div>
            <!-- ********* Live purchase Request Pending For Live Authorization Level1 ************ -->
            
            <!-- ********* Disapproved Purchase Request Planning Level1 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pr_plan()">
            DISAPPROVED PURCHASE REQUEST AT LEVEL1
            <?php $counter1 = 0; foreach ($dis_pr_plan->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="dis_pr_plan"></div>
            </div>
            <!-- ********* Live purchase Request Pending For Live Authorization Level1 ************ -->
            
            <!-- ********* Live purchase Request Pending For Live Authorization Level2 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_pr_lvl2()">
            LIVE PURCHASE REQUEST PENDING FOR AUTHORIZATION LEVEL2
            <?php $counter1 = 0; foreach ($auth_pr_lvl2->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="auth_pr_lvl2"></div>
            </div>
            <!-- ********* Live purchase Request Pending For Live Authorization Level2 ************ -->
            
            <!-- ********* Disapproved Purchase Request Planning Level2 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pr_plan_lvl2()">
            DISAPPROVED PURCHASE REQUEST AT LEVEL2
            <?php $counter1 = 0; foreach ($dis_pr_plan_lvl2->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="dis_pr_plan_lvl2"></div>
            </div>
            <!-- ********* Disapproved Purchase Request Planning Level2 ************ -->
            
            <!-- ********* Live Aurhorized purchase Request Pending For ERP Authorization ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="geterp_not_auth_pr()">
            LIVE AUTHORIZED PURCHASE REQUEST PENDING FOR ERP AUTHORIZATION
            <?php $counter1 = 0; foreach ($erp_not_auth_pr->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="erp_not_auth_pr"></div>
            </div>
            <!-- ********* Live Aurhorized Purchase Request Pending For ERP Authorization ************ -->
            
            <!-- ********* ERP Authorized Purchase Requests Whose Purchase Order Is Not Created************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getpr_po_report()">ERP AUTHORIZED PR PENDING FOR ERP PO CREATION
            <?php 
					$shown=1; 
					foreach ($j->result() as $row) {
						$prno = $row->preqm_prno;
						$pr_itemcode = $row->prqit_itemcode;
						$pr_auth_qty = $row->prqit_authqty;
						
						$max_no='';
						$sql1 = "SELECT max(pomas_poamendmentno) as max1  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
								  WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono";
						
						$query1 =$this->db->query($sql1)->row();
						
						$max_no = $query1->max1;
						
						$query2 = "SELECT sum(a.poprq_pocovqty) as poprq_pocovqty
						FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b,
						SCMDB..po_poitm_item_detail c
						WHERE a.poprq_prno = '$prno' and a.poprq_pono = b.pomas_pono
						and b.pomas_poamendmentno ='$max_no' and b.pomas_pono = c.poitm_pono
						and c.poitm_polineno = a.poprq_polineno
						and c.poitm_poamendmentno = a.poprq_poamendmentno
						and c.poitm_poamendmentno = b.pomas_poamendmentno
						and c.poitm_itemcode = '$pr_itemcode' and b.pomas_podocstatus not in('DE','CA','OP')
						group by c.poitm_itemcode";
							 
						$sql2 =$this->db->query($query2)->row();
						
						$po_itm_qty = $sql2->poprq_pocovqty;
						
						if($po_itm_qty == ''){
							
							$po_itm_qty = 0;
							
						}
						
						$query3 = "SELECT pomas_podocstatus,pomas_pono  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
						WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono and b.pomas_poamendmentno = '$max_no'";
									 
						$query3 =$this->db->query($sql3)->row();
						
						$po_status = $query3->pomas_podocstatus;
						$po_no = $query3->pomas_pono;
						
						if($po_itm_qty < $pr_auth_qty){
							echo "<input type='hidden' name=pr_num value='$prno' />";
							$shown++;
						}	
					}
					$shown1 = $shown-1; 
					echo "( ".$shown1. " )";
			?>
            </button>
            <div class="panel">
                <div id="pr_po_report"></div>
            </div>
            <!-- ********* ERP Authorized Purchase Requests end ************ -->
            
            <!-- ********* Live purchase Request Pending For Live Authorization Purchase ************ -->
            <?php /*?><button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_pr_pur()">
            Erp Authorized Purchase Request Pending For Authorization By Purchase
            <?php $counter1 = 0; foreach ($auth_pr_pur->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="auth_pr_pur"></div>
            </div><?php */?>
            <!-- ********* Live purchase Request Pending For Live Authorization Purchase ************ -->
            
            <!-- ********* Disapproved Purchase Request Purchase ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdis_pr_pur()">
            DISAPPROVED PURCHASE REQUESTS BY PURCHASE DEPARTMENT
            <?php $counter1 = 0; foreach ($dis_pr_pur->result() as $row) {  $counter1++; $prnum = $row->preqm_prno; } $count1 =$counter1;  ?>
            <?php echo " (".$counter1.") " ?>
            </button>
            <div class="panel">
                <div id="dis_pr_pur"></div>
            </div>
            <!-- ********* Disapproved Purchase Request Purchase ************ -->
            
            <!-- ********* Fresh Purchase Order ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getfresh_po()">ERP FRESH PO'S PENDING FOR LIVE PO CREATION
            <?php $counter = 0; foreach ($i->result() as $row) {  $counter++; $pono = $row->pomas_pono; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="fresh_po"></div>
            </div>
            <!-- ********* Fresh Purchase Order end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 1 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_po_lvl1()">LIVE PO PENDING FOR AUTHORIZATION LEVEL1 		            <!--Gaurav Mangal Sir-->
            <?php $counter = 0; foreach ($m_lvl1->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="auth_po_lvl1"></div>
            </div>
            <!-- ********* Authorize Purchase Order Level 1 end ************ -->
            
            <!-- ********* Disapproved Purchase Order Level 1 ************ -->
            <?php $counter = 0; foreach ($disaprve_lvl1->result() as $row) 
			{
			  $counter++; $pono = $row->po_num; 
			  if ($counter >0)
			  { 
				echo '<style>			
						#label 
							{
								background-color: yellow;
								animation-name: example;
								animation-duration: 2s;
								animation-iteration-count: infinite;
							}
						
						@keyframes example 
							{
								0%   {background-color: yellow;}
								100%  {background-color: red;}
							}
					  </style>';
			   }
			 }
            ?>
            <button class="accordion" id="label" style="color:black; font-weight:bold;"  onMouseDown="getdis_po_lvl1()">DISAPPROVED LIVE PO'S AT LEVEL 1
            <?php echo " ( ".$counter." )"; ?>
            </button>
            <div class="panel">
              <div id="dis_po_lvl1"></div>
            </div>
            <!-- ********* Disapproved Purchase Order Level 1 end ************ -->
            
            <!-- ********* Authorize Purchase Order Level 2 ************ -->                              
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getauth_po_lvl2()">LIVE PO'S PENDING FOR AUTHORIZATION LEVEL2 <!--Abninav Sir-->
            <?php $counter = 0; foreach ($m->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
                <div id="auth_po_lvl2"></div>
            </div>
            <!-- ********* Authorize Purchase Order Level 2 end ************ --> 
            
            <!-- ********* Disapproved Purchase Order Level 2 ************ -->
            <?php $counter = 0; foreach ($disaprve->result() as $row) 
            {
			  $counter++; $pono = $row->po_num; 
			  if ($counter >0)
			  { 
				echo '<style>
						#label1 
						{
							background-color: yellow;
							animation-name: example;
							animation-duration: 2s;
							animation-iteration-count: infinite;
						}
						
						@keyframes example 
						{
							0%   {background-color: yellow;}
							100%  {background-color: red;}	
						}
					  </style>';
			   }
             }
            ?>
            <button class="accordion" id="label1" style="color:black; font-weight:bold;"  onMouseDown="getdis_po_lvl2()">DISAPPROVED LIVE PO'S AT LEVEL 2
            <?php echo " ( ".$counter." )"; ?>
            </button>
            <div class="panel">
              <div id="dis_po_lvl2"></div>
            </div>
            <!-- ********* Disapproved Purchase Order Level 2 end ************ -->
            
            <!-- ********* Live Authorized Purchase Order Pending For ERP Authorization ************ -->
            
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getnot_auth_po()">LIVE AUTHORIZED PO PENDING FOR ERP AUTHORIZATION
            <?php $counter = 0; foreach ($not_auth_po->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="not_auth_po"></div>
            </div> 
            <!-- ********* Live Authorized Purchase Order Pending For ERP Authorization ************ -->
            
            <!-- ********* ERP Amended PO'S ************ -->
            
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getamend_po()">ERP AMENDED PO'S
            <?php $counter = 0; foreach ($amend_po->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="amend_po"></div>
            </div> 
            <!-- ********* ERP Amended PO'S ************ -->
            
            <!-- ********* PO Send Supplier For Purchase ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getsupp_for_pur()">LIVE PO'S PENDING IN SEND TO SUPPLIER FOR PURCHASE 
            <?php $counter = 0; foreach ($n->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="supp_for_pur"></div>
            </div> 
            <!-- ********* PO Send Supplier For Purchase end ************ -->
            
            <!-- ********* PO pending for issuance of manufacturing clearance from planning ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmc_planning()">LIVE PO'S PENDING IN ISSUANCE OF MANUFACTURING CLEARNACE FROM PLANNING 
            <?php $counter = 0; foreach ($mc_planning->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?>
            </button>
            <div class="panel">
                <div id="mc_planning"></div>
            </div>
            <!-- ********* PO pending for issuance of manufacturing clearance from planning end ************ -->
                
            <!-- ********* PO pending for issuance of manufacturing clearance from Purchase dept. ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getmc_purchase()">LIVE PO'S PENDING FOR ISSUANCE OF MANUFACTURING CLEARANCE
            <?php $counter = 0; foreach ($o->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="mc_purchase"></div>
            </div> 
            <!-- ********* PO pending for issuance of manufacturing clearance from Purchase dept. end ************ -->
             
             <!-- ********* PO awaited for acknowledgement from supplier ************ -->                          
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getack_frm_supp()">PENDING IN ACKNOWLEDGEMENT FROM THE SUPPLIER
            <?php $counter = 0; foreach ($r->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="ack_frm_supp"></div>
            </div> 
            <!-- ********* PO awaited for acknowledgement from supplier end ************ -->
            
            <!-- ********* PO pending for test certificate upload ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="gettc_upload()">PENDING FOR TEST CERTIFICATES IF REQ.
            <?php $counter = 0; foreach ($tc->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="tc_upload"></div>
            </div>
            <!-- ********* PO pending for test certificate upload end ************ -->
            
            <!-- ********* PO Pending For Advance Given To supplier ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getadvance_supplier()">PENDING FOR ADVANCE GIVEN TO SUPPLIER 
            <?php $counter = 0; foreach ($advance->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="advance_supplier"></div>
             </div>
             <!-- ********* PO Pending For Advance Given To supplier end ************ -->
            
            <!-- ********* PO awaited for performa invoice from supplier ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getpi_creation()">AWAITED PERFORMA INVOICE FROM SUPPLIER 
            <?php $counter = 0; foreach ($pi->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="pi_creation"></div>
             </div>
             <!-- ********* PO awaited for performa invoice from supplier end ************ -->
             
             <!-- ********* PO awaited for authorization of performa invoice if performa invoice amount in greater than 50000 ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getpi_authorize()">PENDING IN PERFORMA INVOICE AUTHORIZATION (IF PI AMOUNT > 50,000) 
            <?php $counter = 0; foreach ($pi2->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="pi_authorize"></div>
            </div>
            <!-- ********* PO awaited for authorization of performa invoice if performa invoice amount in greater than 50000 end ************ -->
            
            <!-- ********* Performa invoice payment to supplier by accounts department ************ --> 
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getpi_payment_supp()">PI PAYMENT TO SUPPLIER 
            <?php $counter = 0; foreach ($pips->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="pi_payment_supp"></div>
             </div>
             <!-- ********* Performa invoice payment to supplier by accounts department end************ --> 
             
             <!-- ********* PO awaited for issuance of post dated cheque ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getcreate_pdc()">PENDING FOR ISSURANCE OF PDC
            <?php $counter = 0; foreach ($pdc->result() as $row) {  $counter++; $pono = $row->pomas_pono; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="create_pdc"></div>
            </div>
             <!-- ********* PO awaited for issuance of post dated cheque end ************ -->
             
             <!-- ********* PO awaited for freight payment by accounts department if freight terms is ex-works ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getfreight_payment()"> PENDING FOR FREIGHT PAYMENT TO SUPPLIER 
            <?php $counter = 0; foreach ($freight->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." )"; ?></button>
            <div class="panel">
              <div id="freight_payment"></div>
            </div>
             <!-- ********* PO awaited for freight payment by accounts department if freight terms is ex-works end ************ -->
             
            <!-- ********* PO awaited for road permit from supplier ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdispatch_inst()">LIVE PO'S AWAITED FOR ROAD PERMIT
            <?php $counter = 0; foreach ($t->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="dispatch_inst"></div>
            </div>
            <!-- ********* PO awaited for road permit from supplier end ************ -->
             
            <!-- ********* PO awaited for Delivery Details ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getdeli_details()">LIVE PO'S PENDING FOR DELIVERY DETAILS
            <?php $counter = 0; foreach ($s->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) "; ?></button>
            <div class="panel">
                <div id="deli_details"></div>
            </div> 
            <!-- ********* PO awaited for Delivery Details end ************ -->
            
            <!-- ********* PO pending for gate entry start ************ -->
            <button class="accordion" style="color:black; font-weight:bold;"  onMouseDown="getgate_entry()">LIVE PO'S PENDING FOR GATE ENTRY
            <?php $counter = 0; foreach ($z->result() as $row) {  $counter++; $pono = $row->po_num; } echo " ( ".$counter." ) ";?></button>
            <div class="panel">
              <div id="gate_entry"></div>
            </div>
            <!-- ********* PO pending for gate entry end ************ -->
        </div>
        <div class="col-lg-2">
        </div>
    </div><br />
  </section>
</section>


<script>
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].onclick = function(){
			this.classList.toggle("active");
			this.nextElementSibling.classList.toggle("show");
	  }
	}
</script>

<!-- FRESH PURCHASE REQUEST -->
<script>
function show_content_fresh_pr(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}
</script>

<!-- AUTHORIZE PURCHASE REQUEST PLANNING -->
<script>
function show_content_auth_pr_plan(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}
</script>

<!-- DISAPPROVED PURCHASE REQUEST PLANNING -->
<script>
function show_content_dis_pr_plan(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}
</script>

<!-- ERP NOT AUTHORIZED PURCHASE REQUESTS -->
<script>
function show_content_erp_not_auth_pr(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}
</script>

<!-- ERP AUTHORIZED PURCHASE REQUESTS WHOSE PO NOT CREATED -->
<script>
function show_content_pr_po_report(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}
</script>

<!-- ERP Fresh Purchase Orders Pending For Live PO Creation -->
<script>
function show_content_fresh_po(a){
	if(a == 'ALL'){
		
		$('#all_fresh_po').show();
		$('#all_fresh_fpo').hide();
		$('#all_fresh_ipo').hide();
		$('#all_fresh_lpo').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_fresh_po').hide();
		$('#all_fresh_fpo').show();
		$('#all_fresh_ipo').hide();
		$('#all_fresh_lpo').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_fresh_po').hide();
		$('#all_fresh_fpo').hide();
		$('#all_fresh_ipo').show();
		$('#all_fresh_lpo').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_fresh_po').hide();
		$('#all_fresh_fpo').hide();
		$('#all_fresh_ipo').hide();
		$('#all_fresh_lpo').show();
		
	}
}
</script>

<!-- Live PO Pending For Authorization LVL1 -->
<script>
function show_content_auth_po_lvl1(a){
	if(a == 'ALL'){
		
		$('#all_fresh_po_lvl1').show();
		$('#all_fresh_fpo_lvl1').hide();
		$('#all_fresh_ipo_lvl1').hide();
		$('#all_fresh_lpo_lvl1').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_fresh_po_lvl1').hide();
		$('#all_fresh_fpo_lvl1').show();
		$('#all_fresh_ipo_lvl1').hide();
		$('#all_fresh_lpo_lvl1').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_fresh_po_lvl1').hide();
		$('#all_fresh_fpo_lvl1').hide();
		$('#all_fresh_ipo_lvl1').show();
		$('#all_fresh_lpo_lvl1').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_fresh_po_lvl1').hide();
		$('#all_fresh_fpo_lvl1').hide();
		$('#all_fresh_ipo_lvl1').hide();
		$('#all_fresh_lpo_lvl1').show();
	}
}
</script>

<!-- Live PO Creation Pending For Authorization LVL2 -->
<script>
function show_content_auth_po_lvl2(a){
	if(a == 'ALL'){
		
		$('#all_fresh_po_lvl2').show();
		$('#all_fresh_fpo_lvl2').hide();
		$('#all_fresh_ipo_lvl2').hide();
		$('#all_fresh_lpo_lvl2').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_fresh_po_lvl2').hide();
		$('#all_fresh_fpo_lvl2').show();
		$('#all_fresh_ipo_lvl2').hide();
		$('#all_fresh_lpo_lvl2').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_fresh_po_lvl2').hide();
		$('#all_fresh_fpo_lvl2').hide();
		$('#all_fresh_ipo_lvl2').show();
		$('#all_fresh_lpo_lvl2').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_fresh_po_lvl2').hide();
		$('#all_fresh_fpo_lvl2').hide();
		$('#all_fresh_ipo_lvl2').hide();
		$('#all_fresh_lpo_lvl2').show();
		
	}
}
</script>

<!-- Live Authorized Purchase Order Pending For Send Supplier For Purchase -->
<script>
function show_content_supp_for_pur(a){
	if(a == 'ALL'){
		
		$('#all_po_supp_for_pur').show();
		$('#all_po_supp_for_pur_fpo').hide();
		$('#all_po_supp_for_pur_ipo').hide();
		$('#all_po_supp_for_pur_lpo').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_po_supp_for_pur').hide();
		$('#all_po_supp_for_pur_fpo').show();
		$('#all_po_supp_for_pur_ipo').hide();
		$('#all_po_supp_for_pur_lpo').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_po_supp_for_pur').hide();
		$('#all_po_supp_for_pur_fpo').hide();
		$('#all_po_supp_for_pur_ipo').show();
		$('#all_po_supp_for_pur_lpo').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_po_supp_for_pur').hide();
		$('#all_po_supp_for_pur_fpo').hide();
		$('#all_po_supp_for_pur_ipo').hide();
		$('#all_po_supp_for_pur_lpo').show();
		
	}
}
</script>

<?php include'footer.php'; ?>