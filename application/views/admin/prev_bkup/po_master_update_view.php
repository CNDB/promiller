<?php include'header.php'; ?>
<body>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
        	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PO Approval Master Update</h4>
        </div>
    </div><br />
    
    <form action="<?php echo base_url(); ?>index.php/po_master_updatec/po_master_update" method="post">
    
    <div class="row">
    	<div class="col-lg-12">
        	<table class="table table-bordered" align="center">
            	<thead>
                	<tr>
                    	<th>SNO</th>
                        <th>CATEGORY</th>
                        <th>PO TYPE</th>
                        <th>L1 APPROVAL REQUIRED</th>
                        <th>L1 APPROVAL BY</th>
                        <th>L1 APPROVAL VALUE</th>
                        <th>L2 APPROVAL BY</th>
                        <th>L2 APPROVAL REQUIRED</th>
                        <th>L2 APPROVAL VALUE</th>
                        <th>LAST MODIFIED BY</th>
                        <th>LAST MODIFIED DATE</th>
                    </tr>
                </thead>
                <tbody>
                	<?php
						$sno = 0;
						foreach ($po_app_mstr_view->result() as $row){
							$sno++;
							$category = $row->category;
							$po_type = $row->po_type;
							$lvl1_app_req = $row->lvl1_app_req;
							$lvl1_approval_by = $row->lvl1_approval_by;
							$lvl1_app_val = $row->lvl1_app_val;
							$lvl2_app_req = $row->lvl2_app_req;
							$lvl2_approval_by = $row->lvl2_approval_by;
							$lvl2_app_val = $row->lvl2_app_val;
							$last_modified_by = $row->last_modified_by;
							$last_modified_date = $row->last_modified_date;
					?>
                	<tr>
                    	<td><?php echo $sno; ?></td>
                        <td>
							<?php echo $category; ?>
                            <input type="hidden" id="category" name="category[]" value="<?php echo $category; ?>">
                        </td>
                        <td>
							<?php echo $po_type; ?>
                            <input type="hidden" id="po_type" name="po_type[]" value="<?php echo $po_type; ?>">
                        </td>
                        <td><?php echo $lvl1_app_req; ?></td>
                        <td><?php echo $lvl1_approval_by; ?></td>
                        <td>
                            <?php if($lvl1_app_req == 'Yes'){ ?>
                            <input type="text" id="lvl1_app_val" name="lvl1_app_val[]" value="<?php echo number_format($lvl1_app_val,2,".",""); ?>" 
                            class="form-control" required>
                            <?php } else { ?>
                            <input type="hidden" id="lvl1_app_val" name="lvl1_app_val[]" value="<?php echo number_format($lvl1_app_val,2,".",""); ?>" 
                            class="form-control">
                            <?php } ?>
                        </td>
                        <td><?php echo $lvl2_app_req; ?></td>
                        <td><?php echo $lvl2_approval_by; ?></td>
                        <td>
                        	<?php if($lvl2_app_req == 'Yes'){ ?>
                            <input type="text" id="lvl2_app_val" name="lvl2_app_val[]" value="<?php echo number_format($lvl2_app_val,2,".",""); ?>" 
                            class="form-control" required>
                            <?php } else { ?>
                            <input type="hidden" id="lvl2_app_val" name="lvl2_app_val[]" value="<?php echo number_format($lvl2_app_val,2,".",""); ?>" 
                            class="form-control">
                            <?php } ?>
                        </td>
                        <td><?php echo $last_modified_by; ?></td>
                        <td><?php echo $last_modified_date; ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-5"></div>
    	<div class="col-lg-2">
        	<input type="submit" id="submit" name="submit" value="SUBMIT" class="form-control">
        </div>
        <div class="col-lg-5"></div>
    </div><br />
    
    
    </form>
  </section>
</section>
</body>
      
<?php include('footer.php'); ?>