<?php include'header.php'; ?>
 
<?php $this->load->helper('not_live_po_helper'); ?>
 
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Pending PR / PO Report</h4>
        </div>
    </div><br><br>
    
    <!------------------------ PR REPORT --------------------------->
    
	<?php 
    //TRUNCATE PR PO REPORT TABLE
    
    $sql_trun = "truncate table tipldb..pr_po_report";
    $query_trun = $this->db->query($sql_trun);
    
    foreach($j->result() as $row)
        {  
            $prno = $row->preqm_prno; 
            $prqit_balqty = $row->prqit_balqty;
            $pr_auth_qty = $row->prqit_authqty;
            $pr_itemcode = $row->prqit_itemcode;
            $loi_itemdesc1 = $row->loi_itemdesc;
			$loi_itemdesc = substr("'","",$row->loi_itemdesc);
            $pr_createdby = $row->preqm_createdby;
            $pr_lastmodifiedby = $row->preqm_lastmodifiedby;
            $pr_createdDate = $row->pr_createdDate;
            $pr_needDate = $row->pr_needDate;
            $prqit_puom = $row->prqit_puom;
            $prqit_reqdqty = $row->prqit_reqdqty;
            $pr_age = $row->pr_age;
            
            $sql = "SELECT * from TIPLDB..pr_submit_table where pr_num = '$prno'";
        
            $query = $this->db->query($sql)->row();
            $category = $query->category;
            $need_date = $query->need_date;
            $costing = $query->costing;
            if($need_date != ''){
                $need_date = date("d-m-Y", strtotime($need_date));
            }
            $project_name = $query->project_name;
            $created_by = $query->created_by;
            $pr_status = $query->pr_status;
            
            $max_no='';
            $sql1 = "SELECT max(pomas_poamendmentno) as max1  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
                      WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono";
            $query1 =$this->db->query($sql1)->row();
            
            $max_no = $query1->max1;
            
            $query2 = "SELECT sum(a.poprq_pocovqty) as poprq_pocovqty
            FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b,
            SCMDB..po_poitm_item_detail c
            WHERE a.poprq_prno = '$prno' and a.poprq_pono = b.pomas_pono
            and b.pomas_poamendmentno ='$max_no' and b.pomas_pono = c.poitm_pono
            and c.poitm_polineno = a.poprq_polineno
            and c.poitm_poamendmentno = a.poprq_poamendmentno
            and c.poitm_poamendmentno = b.pomas_poamendmentno
            and c.poitm_itemcode = '$pr_itemcode' and b.pomas_podocstatus not in('DE','CA','OP')
            group by c.poitm_itemcode";
             
            $sql2 =$this->db->query($query2)->row();
            
            $po_itm_qty = $sql2->poprq_pocovqty;
            
            if($po_itm_qty == ''){
                
                $po_itm_qty = 0;
                
            }
            
            $query3 = "SELECT pomas_podocstatus,pomas_pono  FROM SCMDB..po_poprq_poprcovg_detail a,scmdb..po_pomas_pur_order_hdr b
            WHERE a.poprq_prno='$prno' and a.poprq_pono = b.pomas_pono and b.pomas_poamendmentno = '$max_no'";
                     
            $query3 =$this->db->query($sql3)->row();
            
            $po_status = $query3->pomas_podocstatus;
            $po_no = $query3->pomas_pono;
            
            if($po_itm_qty < $pr_auth_qty){
                
            $sql4 = "insert into tipldb..pr_po_report (preqm_prno, item_code, item_desc, category, project_name, costing, need_date, pr_age, pr_status)
            values ('".$prno."','".$pr_itemcode."','".$loi_itemdesc."','".$category."','".$project_name."','".$costing."','".$need_date."'
            ,'".$pr_age."','".$pr_status."')";
            
            $query4 =$this->db->query($sql4);
                
            } }
    ?>
    
     <div class="row">
        <div class="col-lg-12">
        	 <h3 style="text-align:center">Purchase Request Report</h3><br>
             
             <?php 
			 
			 $all_pr = "CGP','FPR','IPR','LPR";
			 $all_cat = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES','PACKING','AVAZONIC',
			 'NON PRODUCTION CONSUMABLES','IT','OTHERS";
			 
			 ?>
             
             <table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
             	<tr style="background-color:#0CF">
                	<td></td>
                    <td><b>Capital Goods</b></td>
                    <td><b>Sensors</b></td>
                    <td><b>Security</b></td>
                    <td><b>Instrumentaion</b></td>
                    <td><b>Tools</b></td>
                    <td><b>Production Consumables</b></td>
                    <td><b>Packing</b></td>
                    <td><b>Avazonic</b></td>
                    <td><b>Non Production Consumables</b></td>
                    <td><b>IT</b></td>
                    <td><b>Others</b></td>
                    <td><b>Total</b></td> 
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>Fresh PR</b></td>
                    <td><?php echo erp_fresh_pr('CGP'); ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><?php echo erp_fresh_pr($all_pr); ?></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>Level1 Pending</b></td>
                    <td><?php echo live_pending_pr('CGP','CAPITAL GOODS','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SENSORS','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SECURITY','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'INSTRUMENTATION','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'TOOLS','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PRODUCTION CONSUMABLES','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PACKING','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'AVAZONIC','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'NON PRODUCTION CONSUMABLES','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'IT','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'OTHERS','Pending For PR Authorize'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,$all_cat,'Pending For PR Authorize'); ?></td>
                </tr>
				<tr>
                	<td style="background-color:#0CF"><b>Level1 Disapproved</b></td>
                    <td><?php echo live_pending_pr('CGP','CAPITAL GOODS','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SENSORS','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SECURITY','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'INSTRUMENTATION','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'TOOLS','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PRODUCTION CONSUMABLES','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PACKING','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'AVAZONIC','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'NON PRODUCTION CONSUMABLES','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'IT','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'OTHERS','PR Disapproved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,$all_cat,'PR Disapproved At Planning Level1'); ?></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>Level2 Pending</b></td>
                    <td><?php echo live_pending_pr('CGP','CAPITAL GOODS','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SENSORS','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SECURITY','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'INSTRUMENTATION','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'TOOLS','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PRODUCTION CONSUMABLES','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PACKING','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'AVAZONIC','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'NON PRODUCTION CONSUMABLES','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'IT','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'OTHERS','PR Approved At Planning Level1'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,$all_cat,'PR Approved At Planning Level1'); ?></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>Level2 Disapproved</b></td>
                    <td><?php echo live_pending_pr('CGP','CAPITAL GOODS','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SENSORS','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SECURITY','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'INSTRUMENTATION','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'TOOLS','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PRODUCTION CONSUMABLES','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PACKING','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'AVAZONIC','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'NON PRODUCTION CONSUMABLES','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'IT','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'OTHERS','PR Disapproved At Planning Level2'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,$all_cat,'PR Disapproved At Planning Level2'); ?></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>Live Auth. ERP Not Auth PR</b></td>
                    <td><?php echo live_pending_pr('CGP', 'CAPITAL GOODS','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SENSORS','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'SECURITY','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'INSTRUMENTATION','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'TOOLS','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PRODUCTION CONSUMABLES','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'PACKING','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'AVAZONIC','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'NON PRODUCTION CONSUMABLES','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'IT','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,'OTHERS','Pending For PR Authorize In ERP'); ?></td>
                    <td><?php echo live_pending_pr($all_pr,$all_cat,'Pending For PR Authorize In ERP'); ?></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>ERP Auth. PR PO Not Created</b></td>
                    <td><?php echo pr_po_not_created('CGP','CAPITAL GOODS'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'SENSORS'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'SECURITY'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'INSTRUMENTATION'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'TOOLS'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'PACKING'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'AVAZONIC'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'IT'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,'OTHERS'); ?></td>
                    <td><?php echo pr_po_not_created($all_pr,$all_cat); ?></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>Total</b></td>
                    <td><?php echo total_pending_pr('CGP','CAPITAL GOODS'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'SENSORS'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'SECURITY'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'INSTRUMENTATION'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'TOOLS'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'PACKING'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'AVAZONIC'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'IT'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,'OTHERS'); ?></td>
                    <td><?php echo total_pending_pr($all_pr,$all_cat); ?></td>
                </tr>
             </table> 
        </div>
    </div>
    
    
    <!------------------------ PO REPORT --------------------------->
    
    <?php
	
	$all_po = "CGP','FPO','IPO','LPO";
 	$all_po_cat = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES','SERVICE','PACKING','AVAZONIC','NON PRODUCTION CONSUMABLES','IT";
	
	$all_po_stat = "DF','FR','OP','AM";
	
	?>
    
    
    <div class="row">
        <div class="col-lg-12">
        	 <h3 style="text-align:center">Purchase Order Report</h3><br>
            
             <table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
             	<tr style="background-color:#0CF">
                	<td></td>
                    <td><b>Capital Goods</b></td>
                    <td><b>Sensors</b></td>
                    <td><b>Security</b></td>
                    <td><b>Instrumentaion</b></td>
                    <td><b>Tools</b></td>
                    <td><b>Production Consumables</b></td>
                    <td><b>Service</b></td>
                    <td><b>Packing</b></td>
                    <td><b>Avazonic</b></td>
                    <td><b>Non Production Consumables</b></td>
                    <td><b>IT</b></td>
                    <td><b>Total</b></td>
                    
                </tr>
                <!-- Draft PO's -->
                <tr>
                	<td style="background-color:#0CF"><b>Draft</b></td>
                    <td><?php echo pending_po('DF',$all_po,'CAPITAL GOODS'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'SENSORS'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'SECURITY'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'INSTRUMENTATION'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'TOOLS'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('DF','SRV',$all_po_cat); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'PACKING'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'AVAZONIC'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'IT'); ?></td>
                    <td><?php echo pending_po('DF',$all_po,'ALL'); ?></td>
                </tr>
                <!-- Fresh PO's -->
                <tr>
                	<td style="background-color:#0CF"><b>Fresh</b></td>
                    <td><?php echo pending_po('FR',$all_po,'CAPITAL GOODS'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'SENSORS'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'SECURITY'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'INSTRUMENTATION'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'TOOLS'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('FR','SRV',$all_po_cat); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'PACKING'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'AVAZONIC'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'IT'); ?></td>
                    <td><?php echo pending_po('FR',$all_po,'ALL'); ?></td>
                </tr>
                <!-- Open PO's -->
                <tr>
                	<td style="background-color:#0CF"><b>Open</b></td>
                    <td><?php echo pending_po('OP',$all_po,'CAPITAL GOODS'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'SENSORS'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'SECURITY'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'INSTRUMENTATION'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'TOOLS'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('OP','SRV',$all_po_cat); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'PACKING'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'AVAZONIC'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'IT'); ?></td>
                    <td><?php echo pending_po('OP',$all_po,'ALL'); ?></td>
                </tr>
                 <!-- Under Amendment PO's -->
                 <tr>
                	<td style="background-color:#0CF"><b>Under Amendment</b></td>
                    <td><?php echo pending_po('AM',$all_po,'CAPITAL GOODS'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'SENSORS'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'SECURITY'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'INSTRUMENTATION'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'TOOLS'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('AM','SRV',$all_po_cat); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'PACKING'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'AVAZONIC'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'IT'); ?></td>
                    <td><?php echo pending_po('AM',$all_po,'ALL'); ?></td>
                </tr>
                <!--Total-->
                <tr>
                	<td style="background-color:#0CF"><b>Total</b></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'CAPITAL GOODS'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'SENSORS'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'SECURITY'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'INSTRUMENTATION'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'TOOLS'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po($all_po_stat,'SRV',$all_po_cat); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'PACKING'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'AVAZONIC'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'NON PRODUCTION CONSUMABLES'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'IT'); ?></td>
                    <td><?php echo pending_po($all_po_stat,$all_po,'ALL'); ?></td>
                </tr>
             </table> 
        </div>
    </div>
    
    <!------------------------ NOT LIVE PR --------------------------->
    
    <div class="row">
        <div class="col-lg-12">
        	 <h3 style="text-align:center">Not In Live Purchase Request</h3><br>
            
             <table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
             	<tr style="background-color:#0CF">
                	<td></td>
                    <td><b>Capital Goods</b></td>
                    <td><b>Sensors</b></td>
                    <td><b>Security</b></td>
                    <td><b>Instrumentation</b></td>
                    <td><b>Tools</b></td>
                    <td><b>Production Consumables</b></td>
                    <td><b>Packing</b></td>
                    <td><b>Avazonic</b></td>
                    <td><b>Non Production Consumables</b></td>
                    <td><b>IT</b></td>
                    <td><b>Others</b></td>
                    <td><b>No Category</b></td>
                    <td><b>Total</b></td>
                </tr>
                <!-- PR Not Created In Live & Not Authorized In Live -->
                <?php
				
				$cgpr = "'CGP'";
				$other_pr = "'FPR','IPR','LPR'";
				$no_pr_typ = "'ABC'";
				$all_pr = "'CGP','FPR','IPR','LPR'";
				$category = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES','PACKING','AVAZONIC','NON PRODUCTION CONSUMABLES','IT','OTHERS','NO CATEGORY";
				
				?>
                <tr>
                	<td style="background-color:#0CF"><b>PR Not Created In Live</b></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $cgpr; ?>&category=<?php echo ""; ?>">
                        	<?php echo prnotcreatedlive($cgpr); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
					<td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
					<td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $no_pr_typ; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($no_pr_typ); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($other_pr); ?>
                        </a>
                    </td>
                    <td>
                    	<a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $all_pr; ?>&category=<?php echo ""; ?>">
							<?php echo prnotcreatedlive($all_pr); ?>
                        </a>
                    </td>
                </tr>
                <!-- PR Created In Live But Not Authorized In Live -->
                <tr>
                	<td style="background-color:#0CF"><b>PR Created In Live But Not Authorized In Live</b></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $cgpr; ?>&category=<?php echo "CAPITAL GOODS";?>">
                            <?php echo prnotauthlive($cgpr, 'CAPITAL_GOODS'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "SENSORS";?>">
                            <?php echo prnotauthlive($other_pr, 'SENSORS'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "SECURITY";?>">
                            <?php echo prnotauthlive($other_pr, 'SECURITY'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "INSTRUMENTATION";?>">
                            <?php echo prnotauthlive($other_pr, 'INSTRUMENTATION'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "TOOLS";?>">
                            <?php echo prnotauthlive($other_pr, 'TOOLS'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "PRODUCTION CONSUMABLES";?>">
                            <?php echo prnotauthlive($other_pr, 'PRODUCTION CONSUMABLES'); ?>
                        </a>
                    </td>
					<td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "PACKING";?>">
                            <?php echo prnotauthlive($other_pr, 'PACKING'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "AVAZONIC";?>">
                            <?php echo prnotauthlive($other_pr, 'AVAZONIC'); ?>
                        </a>
                    </td>

                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "NON PRODUCTION CONSUMABLES";?>">
                            <?php echo prnotauthlive($other_pr, 'NON PRODUCTION CONSUMABLES'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "IT";?>">
                            <?php echo prnotauthlive($other_pr, 'IT'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "OTHERS";?>">
                            <?php echo prnotauthlive($other_pr, 'OTHERS'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $other_pr; ?>&category=<?php echo "NO CATEGORY";?>">
                            <?php echo prnotauthlive($other_pr, 'NO CATEGORY'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_pr?pr_type=<?php echo $all_pr; ?>&category=<?php echo $category;?>">
                            <?php echo prnotauthlive($all_pr, $category); ?>
                        </a>
                    </td>
                </tr>
             </table> 
        </div>
    </div>
    
     <!------------------------ NOT LIVE PR --------------------------->
     
     <!------------------------ NOT LIVE PO --------------------------->
     
	<?php
            
    $cgpo = "'CGP'";
	$srvpo = "'SRV'";
    $other_po = "'FPO','IPO','LPO'";
	$no_po_typ = "'ABC'";
	$all_po = "'CGP','FPO','IPO','LPO','SRV'";
	$category = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES','PACKING','AVAZONIC','NON PRODUCTION CONSUMABLES','IT','OTHERS','NO CATEGORY";
    
    ?>
    
    <div class="row">
        <div class="col-lg-12">
        	 <h3 style="text-align:center">Not In Live Purchase Order</h3><br>
            
             <table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
             	<tr style="background-color:#0CF">
                	<td></td>
                    <td><b>Capital Goods</b></td>
                    <td><b>Sensors</b></td>
                    <td><b>Security</b></td>
                    <td><b>Instrumentation</b></td>
                    <td><b>Tools</b></td>
                    <td><b>Production Consumables</b></td>
                    <td><b>Service</b></td>
                    <td><b>Packing</b></td>
                    <td><b>Avazonic</b></td>
                    <td><b>Non Production Consumables</b></td>
                    <td><b>IT</b></td>
                    <td><b>No Category</b></td>
                    <td><b>Total</b></td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>PO Not Created And Authorized In Live</b></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $cgpo; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($cgpo); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
					<td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $srvpo; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($srvpo); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
					<td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $no_po_typ; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($no_po_typ); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($other_po); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $all_po; ?>&category=<?php echo "";?>">
                        	<?php echo ponotcreatedlive($all_po); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                	<td style="background-color:#0CF"><b>PO Created In Live But Not Authorized In Live</b></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $cgpo; ?>&category=<?php echo "CAPITAL GOODS";?>">
                        	<?php echo ponotauthlive($cgpo, 'CAPITAL GOODS'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "SENSORS";?>">
                        	<?php echo ponotauthlive($other_po, 'SENSORS'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "SECURITY";?>">
                        	<?php echo ponotauthlive($other_po, 'SECURITY'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "INSTRUMENTATION";?>">
                        	<?php echo ponotauthlive($other_po, 'INSTRUMENTATION'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "TOOLS";?>">
                        	<?php echo ponotauthlive($other_po, 'TOOLS'); ?>
                        </a>
                    </td>
					<td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "PRODUCTION CONSUMABLES";?>">
                        	<?php echo ponotauthlive($other_po, 'PRODUCTION CONSUMABLES'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $srvpo; ?>&category=<?php echo "SERVICE";?>">
                        	<?php echo ponotauthlive($srvpo, 'SERVICE'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "PACKING";?>">
                        	<?php echo ponotauthlive($other_po, 'PACKING'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "AVAZONIC";?>">
                        	<?php echo ponotauthlive($other_po, 'AVAZONIC'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "NON PRODUCTION CONSUMABLES";?>">
                        	<?php echo ponotauthlive($other_po, 'NON PRODUCTION CONSUMABLES'); ?>
                        </a>
                    </td>
					<td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "IT";?>">
                        	<?php echo ponotauthlive($other_po, 'IT'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $other_po; ?>&category=<?php echo "NO CATEGORY";?>">
                        	<?php echo ponotauthlive($other_po, 'NO CATEGORY'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/pending_poc/not_live_po?po_type=<?php echo $all_po; ?>&category=<?php echo $category;?>">
                        	<?php echo ponotauthlive($all_po, $category); ?>
                        </a>
                    </td>
                </tr>
             </table> 
        </div>
    </div>
    
     <!------------------------ NOT LIVE PO --------------------------->
    
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>