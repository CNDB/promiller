<?php 
$rpt_ty = $_REQUEST['category']; 
$username = $_SESSION['username'];

$sql_user = "select * from tipldb..login where email like'%$username%'";
$qry_user = $this->db->query($sql_user);

foreach($qry_user->result() as $row){
	$user_id = $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
}
?>
	
<div align="center" style="margin:0px auto; font-size:16px; font-weight:bold;"><?php echo $rpt_ty." Items"; ?></div>

<div class="row">
	<div class="col-lg-1"></div>
    <div class="col-lg-10">
        <table cellpadding="3" cellspacing="2" width="80%" style="font-size:12px; text-align:left;" align="center" border="1" class="table table-bordered">
            <tr style="background-color:#CCC; font-weight:bold">
                <td>SN.</td>
                <td>Item Code</td>
                <td>Description </td>
                <td>ERP Category</td>
                <td>Category </td>
                <td>Item Type</td>
                <td>Source</td>
                <td>In MRP Since</td>
                <td>Age</td>
                <td>Action</td>
                <td>Check Re-Order/MOQ/Re-Order Qty</td>
            </tr>
        <?php 
        
        if($rpt_ty=='All'){
            
            $sup_query1="select * from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns";
            
            $query1 = $this->db->query($sup_query1);
            
        }else{
            
            $sup_query1="select * from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, 
            scmdb..itm_ibu_itemvarhdr b, tipldb..erp_live_category c
            where a.ItemCode = b.ibu_itemcode and b.ibu_category = c.erp_cat_code 
            and c.live_category = '$rpt_ty'";
            
            $query1 = $this->db->query($sup_query1);
            
        }
        
        $i=1;
        foreach ($query1->result() as $row) {
           $item_code = $row->ItemCode;
           
           $item_code1 = urlencode($item_code);
            
           if(strpos($item_code1, '%2F') !== false){ 
              $item_code2 = str_replace("%2F","chandra",$item_code1);
           } else {
              $item_code2 = $item_code1;
           }   
		   
		   $ReOrderLevel = $row->ReOrderLevel;
		   $ReOrderQty = $row->ReOrderQty;
		   
		   if($ReOrderLevel == 0 && $ReOrderQty == 0){
			   $item_type = "Project";
		   } else {
			   $item_type = "Reorder";
		   }
		   
		   $Manufactured = $row->Manufactured;
		   $Purchased = $row->Purchased;
		   $SubContracted = $row->SubContracted;
		   
		   if($Manufactured == 'Yes' && $Purchased == 'No' && $SubContracted == 'No'){
			   $source = "Manufacturing";
		   } else if($Manufactured == 'No' && $Purchased == 'Yes' && $SubContracted == 'No'){
			   $source = "Purchase";
		   } else if($Manufactured == 'No' && $Purchased == 'No' && $SubContracted == 'Yes'){
			   $source = "Subcontract";
		   } else if($Manufactured == 'Yes' && $Purchased == 'Yes' && $SubContracted == 'No'){
			   $source = "Manufacturing, Purchase";
		   } else if($Manufactured == 'No' && $Purchased == 'Yes' && $SubContracted == 'Yes'){
			   $source = "Purchase, Subcontract";
		   } else if($Manufactured == 'Yes' && $Purchased == 'No' && $SubContracted == 'Yes'){
			   $source = "Manufacturing, Subcontract";
		   } else if($Manufactured == 'Yes' && $Purchased == 'Yes' && $SubContracted == 'Yes'){
			   $source = "Manufacturing, Purchase, Subcontract";
		   }
        ?>
            <tr>
                <td><? echo $i;?></td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
                        <?php echo $item_code;?>
                    </a>
                </td>
                <td><?php $item_desc = $row->Descriptions; echo mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8"); ?></td>
                <td>
                	<?php
                        $sup_query2="select * from scmdb..itm_ibu_itemvarhdr where ibu_itemcode = '$item_code'";
                        
                        $query2 = $this->db->query($sup_query2);
                        
                        foreach ($query2->result() as $row) {
                            echo $item_category = $row->ibu_category;
                        }	
                    ?>
                </td>
                <td>
                    <?php
                        $sup_query2="select * from scmdb..itm_ibu_itemvarhdr where ibu_itemcode = '$item_code'";
                        
                        $query2 = $this->db->query($sup_query2);
                        
                        foreach ($query2->result() as $row) {
                            $item_category = $row->ibu_category;
                        }
                        
                        $sup_query3="select * from tipldb..erp_live_category where erp_cat_code = '$item_category'";
                        
                        $query3 = $this->db->query($sup_query3);
                        
                        foreach ($query3->result() as $row) {
                             echo $item_category_live = $row->live_category;
                        }	
                    ?>
                </td>
                <td><?php echo $item_type; ?></td>
                <td><?php echo $source; ?></td>
                <?php
                    $sup_query4="select datediff(day,create_date,getdate()) as age,* from tipldb..mrp_report_history 
                    where last_visible_date is null and item_code='$item_code'";
                    
                    $query4 = $this->db->query($sup_query4);
                    
                    foreach ($query4->result() as $row) {
                       $age = $row->age;
                       $create_date = $row->create_date;
                    }
                    
                    if($age > 1){
                        $color = 'red';
                    } else {
                        $color = 'white';
                    }
                                            
                ?>
                <td><?php echo substr($create_date,0,11); ?></td>
                <td style="background-color:<?php echo $color; ?>"><?php echo $age; ?></td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/mrp_reportc/mrp_item_view/<?php echo $item_code2; ?>">
                        <img src="<?php echo base_url('assets/admin/img/');?>edit_icon.png" width="25px" height="25px" />
                    </a>
                </td>
                <td>
                <a href="http://live.tipl.com/tipl_project1/goods_receipt/reorder_update_page.php?item_code=<?php echo urlencode($item_code); ?>&user_id=<?php echo $user_id; ?>&user_type=<?php echo $user_type?>&name=<?php echo $name; ?>">
                    <img src="<?php echo base_url('assets/admin/img/');?>edit_icon.png" width="25px" height="25px" />
                </a>
                </td>
            </tr>
        <?php $i++;} ?>
        </table><br><br>
    </div>
    <div class="col-lg-1"></div>
</div>