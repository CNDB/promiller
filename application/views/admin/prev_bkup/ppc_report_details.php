<?php include'header.php'; ?>

<?php $this->load->helper('ppc_report_helper'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC DETAILS</h4>
        </div>
    </div><br />
    
<?php 

$category = $_REQUEST['category'];

$prtype = $_REQUEST['pr_type'];

$fun = $_REQUEST['fun'];

$start_date = $_REQUEST['start_date'];

$end_date = $_REQUEST['end_date'];

//die;


?>
    
    <div class="row">
        <div class="col-lg-12">
              <table align="center" width="80%" height="auto" cellpadding="0px" cellspacing="0px" class="table table-bordered" style="font-size:10px">
              	<?php 
					if($fun == 'det_ld_queans'){
						det_ld_queans($category, $prtype);
					}else if($fun == 'det_delay_queans'){
						det_delay_queans($category, $prtype);
					}else if($fun == 'det_ontime_queans'){
						det_ontime_queans($category, $prtype);
					}else if($fun == 'det_total_queans'){
						det_total_queans($category, $prtype);
					}else if($fun == 'det_ld_prnotauth'){
						det_ld_prnotauth($category, $prtype);
					} else if($fun == 'det_delay_prnotauth'){
						det_delay_prnotauth($category, $prtype);
					} else if($fun == 'det_ontime_prnotauth'){
						det_ontime_prnotauth($category, $prtype);
					} else if($fun == 'det_total_prnotauth'){
						det_total_prnotauth($category, $prtype);
					} else if($fun == 'det_ld_prauth'){
						det_ld_prauth($category, $prtype);
					} else if($fun == 'det_delay_prauth'){
						det_delay_prauth($category, $prtype);
					} else if($fun == 'det_ontime_prauth'){
						det_ontime_prauth($category, $prtype);
					} else if($fun == 'det_total_prauth'){
						det_total_prauth($category, $prtype);
					} else if($fun == 'det_ld_prdis'){
						det_ld_prdis($category, $prtype);
					} else if($fun == 'det_delay_prdis'){
						det_delay_prdis($category, $prtype);
					} else if($fun == 'det_ontime_prdis'){
						det_ontime_prdis($category, $prtype);
					} else if($fun == 'det_total_prdis'){
						det_total_prdis($category, $prtype);
					} else if($fun == 'det_ld_currdate'){
						det_ld_currdate($category, $prtype);
					} else if($fun == 'det_delay_currdate'){
						det_delay_currdate($category, $prtype);
					} else if($fun == 'det_ontime_currdate'){
						det_ontime_currdate($category, $prtype);
					} else if($fun == 'det_total_currdate'){
						det_total_currdate($category, $prtype);
					}  else if($fun == 'det_ld_prauth_weekwise'){
						det_ld_prauth_weekwise($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_delay_prauth_weekwise'){
						det_delay_prauth_weekwise($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_ontime_prauth_weekwise'){
						det_ontime_prauth_weekwise($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_total_prauth_weekwise'){
						det_total_prauth_weekwise($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_ld_grand_total'){
						det_ld_grand_total($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_delay_grand_total'){
						det_delay_grand_total($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_ontime_grand_total'){
						det_ontime_grand_total($category, $prtype, $start_date, $end_date);
					} else if($fun == 'det_total_grand_total'){
						det_total_grand_total($category, $prtype, $start_date, $end_date);
					} else {
						echo "aaa";
					}
				?>
              </table>
        </div>
    </div><br />
  </section>
</section>

<?php include('footer.php'); ?>