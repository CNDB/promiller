<?php include'header.php'; ?>

    <section id="main-content">
      <section class="wrapper">
      
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">CURRENCY RATES</h4>
            </div>
        </div><br />
        
        <div class="row">
            <div class="col-lg-4">  
            </div>
            <div class="col-lg-4"> 
            	<h3 align="center">Insert Currency Rates</h3> 
            </div>
            <div class="col-lg-4">  
            </div>
        </div><br>
        
        <form action="<?php echo base_url(); ?>index.php/currencyc/currency_submit" method="post">
            <div class="row">
                <div class="col-lg-4">  
                </div>
                <div class="col-lg-2"> 
                    <b>Currency Code :</b> 
                </div>
                <div class="col-lg-2"> 
                    <input type="text" name="curr_code" value="" class="form-control"> 
                </div>
                <div class="col-lg-4">  
                </div>
            </div><br>
            
            <div class="row">
                <div class="col-lg-4">  
                </div>
                <div class="col-lg-2"> 
                    <b>Currency Name :</b>
                </div>
                <div class="col-lg-2"> 
                    <input type="text" name="curr_name" value="" class="form-control"> 
                </div>
                <div class="col-lg-4">  
                </div>
            </div><br>
            
            <div class="row">
                <div class="col-lg-4">  
                </div>
                <div class="col-lg-2"> 
                    <b>Currency Value :</b>
                </div>
                <div class="col-lg-2"> 
                    <input type="text" name="curr_value" value="" class="form-control"> 
                </div>
                <div class="col-lg-4">  
                </div>
            </div><br>
            
            <div class="row">
                <div class="col-lg-4">  
                </div>
                <div class="col-lg-4"> 
                    <input type="submit" name="submit" value="SUBMIT" class="form-control" >
                </div>
                <div class="col-lg-4">  
                </div>
            </div><br>
        </form>
        
        <div class="row">
        		<div class="col-lg-3">  
                </div>
                <div class="col-lg-6">  
                	<h3 align="center">Currecy Rates</h3>
                </div>
                <div class="col-lg-3">  
                </div>
        </div><br>
        
        <div class="row">
        		<div class="col-lg-3">  
                </div>
                <div class="col-lg-6">  
                	<table class="table table-bordered" cellpadding="0" cellspacing="0">
        				<thead>
                        	<tr>
                            	<th>S.No.</th>
                                <th>Currency Code</th>
                                <th>Currency Name</th>
                                <th>Currency Rate</th>
                            </tr>
                        </thead>
                        <?php
							$sno = 0;
							foreach ($h->result() as $row){ 
								$curr_code = $row->curr_code;
								$curr_name = $row->curr_name;
								$curr_rate = $row->curr_rate;
								$sno = $sno+1;
						?>
                        <tbody>
                        	<tr>
                            	<td><?php echo $sno; ?></td>
                            	<td><?php echo $curr_code; ?></td>
                                <td><?php echo $curr_name; ?></td>
                                <td><?php echo $curr_rate; ?></td>
                            </tr>
                        </tbody>
                        <?php
							}
						?>
        			</table>
                </div>
                <div class="col-lg-3">  
                </div>
        </div><br>
         
      </section>
    </section>
</body>

<?php include('footer.php'); ?>