<!--main content start-->
<form action="<?php echo base_url(); ?>index.php/sendsupppurc/insert_po_sub_lvl2" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?>
                                    
<!--****** REMARKS & SUPPLIER QUOTES *******-->

<?php include('po_supplier_quotes.php'); ?>

<?php
	
	$from_email = $_SESSION['username'];
	
	//echo "cha==".$from_email;
	
	echo "<br>";
	
	$sql_emp_details = "select * from TIPLDB..employee where emp_active = 'yes' and emp_email like '%$from_email%'";
	
	//$sql1 ="select *, datediff(DAY, preqm_prdate,getdate()) as diff1 from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'";
	$query_emp_details = $this->db->query($sql_emp_details);
	
    foreach ($query_emp_details->result() as $row) {
	 $emp_name = $row->emp_name;
	 $mobile_no = $row->mobile_no;
	 $emp_email = $row->emp_email;
	 
	 echo "<input type='hidden' name='emp_name' value='$emp_name' />";
	 echo "<input type='hidden' name='mobile_no' value='$mobile_no' />";
    }

?>
            
<!-- Mail Box Popup -->
<div class="row">
	<div class="col-lg-3"></div>
    <div class="col-lg-6">
		<?php
			$sql_pdf = "select po_pdf_name from TIPLDB..po_master_table where po_num = '$po_num'";
			
			$query_pdf = $this->db->query($sql_pdf);
			
			foreach($query_pdf->result() as $row){
				
				$po_pdf_name = $row->po_pdf_name;
			}
			
			if($po_pdf_name == '' || $po_pdf_name == NULL){
				
				echo "<h4 style='color:red;'>ERROR: Please Print PO PDF First And Reload The Page...</h4>";
				
			} else {
		?>
			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Mail To Supplier</button>
        <?php
        	}
        ?>
    </div>
    <div class="col-lg-3"></div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
<div class="modal-dialog">

  <!-- Modal content-->
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal">&times;</button>
      <h3 class="modal-title" style="font-weight:bold; color:#000; text-align:center">Mail Box</h3>
    </div>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
                <b>TO</b>
            </div>
            <div class="col-lg-6">
            	<?php
					foreach ($view_po->result() as $row){ 
						$supp_email1 = $row->po_supp_email;
						$supp_email2 = str_replace('"','',$supp_email1);
						$supp_email  = str_replace("'","",$supp_email2)
    			?>
                <input type="text" name="to_mail" value="<?php echo str_replace("'","",$supp_email); ?>" class="form-control" />
                <?php
					break;}
				?>
            </div>
            <div class="col-lg-2">
            </div>
        </div><br />
        <!-- From Email -->
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
            	<b>FROM</b>
            </div>
            <div class="col-lg-6">
                <?php echo "<b>".$from_email."@tipl.com</b>"; ?>
            </div>
            <div class="col-lg-2">
            </div>
        </div><br />
        
        <!-- CC Email -->
        <div class="row">
        	<div class="col-lg-2">
            </div>
            <div class="col-lg-2">
            	<b>COPY TO</b>
            </div>
            <div class="col-lg-6">
            	<?php $cc_email = "purchase@tipl.com"; ?>
                <input type="text" name="cc_mail" value="<?php echo str_replace("'","",$cc_email); ?>" class="form-control" />
            </div>
            <div class="col-lg-2">
            </div>
        </div><br />
        
        <!-- attached po Email -->
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
            	<b>ATT. PO</b>
            </div>
            <div class="col-lg-6">
                <b><a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_num; ?>" 
               target="_blank"><?php echo $po_num; ?></a></b>
            </div>
            <div class="col-lg-2">
            </div>
        </div><br />
        
        <div class="row">
            <div class="col-lg-2">
            </div>
            <div class="col-lg-2">
                <b>SUBJECT</b>
            </div>
            <?php $subject_mail = $po_num." - "."Toshniwal Industries Pvt. Ltd."; ?>
            <div class="col-lg-6">
                <input type="text" name="subject" value="<?php echo $subject_mail; ?>" class="form-control" />
            </div>
            <div class="col-lg-2">
            </div>
        </div><br />
        <?php
		$purchase_order = "<b>PO No. -".$po_num."</b>";
		
		$greetings = "Sir/Madam";
		
		$message = "Please Find the attached Purchase Order & kindly acknowledge the same.<br>Kindly arrange to provide copy of this PO duly 
		signed & stamped as your acceptance.<br> If we do not receive ​Any communication within 7 days, it shall be deemed that the PO along 
		with all the above mentioned​ ​ terms are acceptable to you.";
		
		$thank_message = " Thank You";
		
		$regards = "<b>Regards,</b>";
		
		$sender_name = "<b>".$emp_name."</b>";
		
		$sender_company = "<b>TIPL, Ajmer</b>";
		
		$sender_mob = "<b>Mo: </b>".$mobile_no;
		
		$message_text = $purchase_order."<br/><br/>".$greetings."<br /><br/>".$message."<br /><br/>";
			
		?>
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-2">
                <b>MESSAGE</b>
            </div>
            <div class="col-lg-6">
                <textarea name="message_text" style="width: 325px; height: 270px;text-align:left" class="form-control">Hello Sir/Madam,

PO No. -<? echo $po_num; ?>
                
Please Find the attached Purchase Order & kindly acknowledge the same.</textarea>  
            </div>
            <div class="col-lg-2"></div>
        </div><br />
        <!---- Other Attachments In PO ---->
        <div class="row">
        	<div class="col-lg-2"></div>
            <div class="col-lg-2"><b>ATTACHMENTS</b></div>
            <div class="col-lg-6">
            	<!--<input type="file" id="other_att" name="other_att[]" class="form-control"/>-->
                <div class="controls">   
                    <div class="entry input-group col-xs-3">
                        <input type="file" class="btn btn-primary" name="other_att[]" id="other_att" multiple />
                        <span class="input-group-btn">
                        <button class="btn btn-success btn-add" type="button">
                            <span class="glyphicon glyphicon-plus"></span>
                        </button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-2"></div>
        </div><br />
        <!---- Other Attachment In PO ---->
        
        <div class="row">
            <div class="col-lg-2"></div>
            <div class="col-lg-2"></div>
            <div class="col-lg-7">
            <input type="submit" name="SEND" value="Send Supplier For Purchase" class="form-control" />
            </div>
            <div class="col-lg-1"></div>
        </div>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div> 
</div>
</div>         
</form>

<?php //chat history ?>
 
<?php include('po_chat_history.php'); ?>
 
<?php //Action Timing Report ?>

<?php include('po_action_timing.php'); ?>

<?php //Action Timing Report ?>
      		
<?php include('footer.php'); ?>