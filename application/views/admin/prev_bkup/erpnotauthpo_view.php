<!--main content start-->

<?php
	include'header.php';
	$purchase_order_no = $this->uri->segment(3);                      
?>

<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">LIVE AUTHORIZED ERP NOT AUTHORIZED PO</h4>
        </div>
    </div><br />

<!--********* PO INFORMATION *********--> 
<div class="row">
    <div class="col-lg-6" style="text-align:left; font-weight:bold;">
        <h4>PO NO. - <?php echo $purchase_order_no; ?></h4>
    </div>
    <div class="col-lg-6" style="text-align:left; font-weight:bold;">
        <table border="1" align="center" class="table table-bordered" style="font-size:9px">
            <tr>
                <td><b>CONDITION</b></td>
                <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
                <td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                <td>FIRST TIME PURCHASE</td>
            </tr>
            <tr>
                <td><b>COLOR</b></td>
                <td style="background-color:red;"></td>
                <td style="background-color:green;"></td>
                <td style="background-color:blue;"></td>
                <td style="background-color:yellow;"></td>
            </tr>
        </table>
    </div>
</div>

<div class="row">
    <div class="col-lg-2" style="text-align:left; font-weight:bold;">
       <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $purchase_order_no; ?>" 
       target="_blank" style="font-size:16px; font-weight:bold;">
       <button type="button" class="form-control">PRINT PO</button></a>
    </div>
</div>

<form action="<?php echo base_url(); ?>index.php/erpnotauthpoc/insert_po_sub_lvl2" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?>

<?php /*?><div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>PO DETAILS</h3>
    </div>
</div>   
<?php $total_order_value_with_taxes = 0;
 foreach ($view_po->result() as $row){ 
   $po_num                       = $row->po_num;
   $pr_num                       = $row->po_ipr_no; 
   $supp_name                    = $row->po_supp_name;
   $supp_addr                    = $row->po_supp_add;
   $supp_email                   = $row->po_supp_email;
   $supp_phone                   = $row->supp_phone;
   $contact_person               = $row->contact_person;
   $order_value                  = $row->pomas_pobasicvalue;
   $cst_tax                      = $row->pomas_tcdtotalrate;
   $total_tax                    = $row->pomas_tcal_total_amount;
   $total_order_value_with_taxes = ($order_value + $cst_tax + $total_tax);
   $po_age                       = $row->diff;
   $payment_term                 = $row->payterm;
   $freight_place                = $row->freight_place;
   $carrier_name                 = $row->carrier_name;
   $deli_type                    = $row->po_deli_type;
   $lead_time                    = $row->po_lead_time;
   $manufact_clearnace           = $row->po_manfact_clernce;
   $dispatch_instruction         = $row->po_dispatch_inst;
   $freight                      = $row->freight;
   $insurance_liablity           = $row->insurance_liablity;
   $transport_mode               = $row->trans_mode;
   $freight_type                 = $row->freight_type;
   $approx_freight               = $row->approx_freight;
   $po_spcl_inst_frm_supp        = $row->po_spcl_inst_frm_supp;
   $ld_applicable                = $row->ld_applicable;
   $currency                     = $row->currency;
   $po_created_in_live           = $row->po_approvedby_lvl0;
   $po_created_in_live_date      = $row->po_approveddate_lvl0;
   $po_created_in_erp            = $row->pomas_createdby;
   $po_created_in_erp_date       = $row->pomas_createddate;
   
   if($freight == "FOR")
		{
			$freight1 = $freight.",&nbsp;Ajmer";
		}
	else if($freight == "FORD")
		{
			$freight1 = $freight.",&nbsp;TIPL Ajmer";
		}
	else
		{
			$freight1 = $freight."&nbsp;";
		}
	$sql1 ="select *, datediff(DAY, preqm_prdate,getdate()) as diff1 from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'";
	$query1 = $this->db->query($sql1);
	
    foreach ($query1->result() as $row) {
	  $pr_age = $row->diff1;
    }					
?>
<div class="row">
    <div class="col-lg-3">
        <b>Supplier Name:</b><br />
        <?php 
            echo $supp_name; 
        ?>   	
    </div>
    <div class="col-lg-3">
        <b>Supplier Address:</b><br />
        <?php 
            echo $supp_addr;
        ?>
    </div>
    <div class="col-lg-3">
        <b>Supplier Email:</b><br />
        <?php 
            echo $supp_email;
        ?>
    </div>
    <div class="col-lg-3">
        <b>Supplier Phone:</b><br />
        <?php 
            echo $supp_phone;
        ?>
    </div>
</div><br /><br />

<div class="row">
	<div class="col-lg-3">
        <b>Contact Person:</b><br />
        <?php 
            echo $contact_person;
        ?>
    </div>
	<div class="col-lg-3">
        <b>Order Value:</b><br />
        <?php 
            echo number_format($total_order_value_with_taxes,2);
        ?>
    </div>
    <div class="col-lg-3">
        <b>PO Age:</b><br />
        <?php echo $po_age."&nbsp; Days"; ?>
    </div>
	<div class="col-lg-3">
        <b>PR Age:</b><br />
        <?php
		echo $pr_age."&nbsp; Days"; 
	?>
    </div>
</div><br /><br />

<div class="row">
	<div class="col-lg-3">
        <b>Payment Terms:</b><br />
        <?php 
            echo $payment_term; 
        ?>   	
    </div>
	<div class="col-lg-3">
        <b>Freight Terms:</b><br />
        <?php 
            echo $freight1;
        ?>
    </div>
    <div class="col-lg-3">
        <b>Freight Place:</b><br />
        <?php 
            echo $freight_place;
        ?>
    </div>
	<div class="col-lg-3">
        <b>Carrier Name:</b><br />
        <?php 
            echo $carrier_name;
        ?>
    </div>
</div><br /><br />

<div class="row">
	<div class="col-lg-3">
        <b>Insurance Term:</b><br />
        <?php 
            echo $insurance_liablity;
        ?>
    </div>
	<div class="col-lg-3">
        <b>Delivery Type:</b><br />
        <?php 
            echo $deli_type; 
        ?>
    </div>
    <div class="col-lg-3">
        <b>Expected Material Recipt Date:</b><br />
        <?php
            echo $lead_time; 
        ?>
    </div>
	<div class="col-lg-3">
        <b>Mode Of Transport:</b><br />
        <?php
            echo $transport_mode; 
        ?>
    </div>
    
</div><br /><br />

<div class="row">
	<div class="col-lg-3">
        <b>Freight Type:</b><br />
        <?php
			echo $freight_type; 
		?>
    </div>
	<div class="col-lg-3">
    	<b>Approximate Freight:</b><br />
        <?php
			echo $approx_freight; 
		?>
    </div>
    <div class="col-lg-3">
        <b>LD Applicable:</b><br />
        <?php
            echo $ld_applicable; 
        ?>
    </div>
    <div class="col-lg-3">
        <b>PO Special Instructions For Supplier:</b><br />
        <?php
            echo $po_spcl_inst_frm_supp; 
        ?>
    </div>
</div><br /><br />

<div class="row" style="font-weight:bold; text-transform:uppercase">
	<div class="col-lg-3">
    	<b>Currency:</b><br />
		<?php
            echo $currency;
        ?>
    </div>
    <div class="col-lg-3">
        <b>PO Created ERP By:</b><br />
        <?php
            echo $po_created_in_erp; 
        ?>
    </div>
	<div class="col-lg-3">
    	<b>PO Created ERP Date:</b><br />
        <?php
            echo date("d-m-Y", strtotime($po_created_in_erp_date));
        ?>
    </div>
    <div class="col-lg-3">
    	<b>PO Created Live By:</b><br />
        <?php
            echo $po_created_in_live; 
        ?>
    </div>
</div><br /><br />
<div class="row" style="font-weight:bold; text-transform:uppercase">
    <div class="col-lg-3">
    	<b>PO Created Live Date:</b><br />
        <?php
            echo date("d-m-Y", strtotime($po_created_in_live_date));  
        ?>
    </div>
</div><br /><br />
<?php break; } ?>
<!--********* ITEM INFORMATION *********-->
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>ITEM DETAILS</h3>
    </div>
</div>
<div class="row">
  <div class="col-lg-12" style=" overflow-x:auto;">
     <table class="table table-bordered" id="dataTable" border="1">
        <thead>
          <tr>
            <th>IPR No.</th> 
            <th>Item Code</th>                             
            <th>Item Desc.</th> 
            <th>PR Type</th> 
            <th>SO No.</th> 
            <th>ATAC No.</th>
            <th>ATAC LD Date.</th>
            <th>ATAC Need Date.</th>
            <th>ATAC Payment Terms.</th>
            <th>PM Group</th> 
            <th>Category</th> 
            <th>Project Name</th> 
            <th>Costing</th> 
            <th>PR Remarks</th> 
            <th>PR Need Date</th>  
            <th>Attached Cost Sheet</th>
            <th>Item Remarks for Supplier</th>
            <th>Manufacturing Clearnace</th>
            <th>Dispatch Instruction</th>                                                            
            <th>Last 1 Yr. Cons.</th>
            <th>Current Yr. Cons.</th>
            <th>Current Stock</th>
            <th>Total Incoming Stock</th>                                                           
            <th>Reservation Qty</th>                                                              
            <th>Order Quantity</th>
            <th>Warehouse Code</th>                                                    
            <th>UOM</th>
            <th>Conversion Factor With Stock UOM</th>
            <th>Supplier Lead Time(AVG)</th>
            <th>Last Supplier</th>
            <th>Last Supplier Rate</th>
            <th>Last Supplier Lead Time</th>
            <th>Reorder Level</th>
            <th>Reorder Qty</th>
            <th>Last Price</th>
            <th>Current Price</th> 
            <th>Total Item Value</th>
            <th>Item Remarks</th> 
          </tr>
        </thead>
        <tbody>
		 <?php
           foreach ($view_po->result() as $row)  { 
            $po_line_no  = $row->po_line_no;
            $po_s_no  = $row->po_s_no;
            $item_code = $row->po_item_code;
            
            $item_code1 = urlencode($item_code);
            
            if(strpos($item_code1, '%2F') !== false)
            {
                $item_code2 = str_replace("%2F","chandra",$item_code1);
            }
            else 
            {
                $item_code2 = $item_code1;
            }
    
            $po_ipr_no = $row->po_ipr_no;
            $po_num = $row->po_num;
            $item_desc = $row->po_itm_desc;
            $wh_code = $row->po_wh_code;
            $uom  = $row->po_uom;
            $current_price = $row->current_price;
            $item_value = number_format($row->total_item_value,2);
            $item_remarks = $row->item_remarks;
            $for_stk_qty = number_format($row->for_stk_quantity,2);
            $po_drwg_refno = $row->po_drwg_refno;
            $totalcost = round($row->po_qty, 2) * round($row->po_cost_pr_unt, 2);
            
            $sql1 ="exec tipldb..pendalcard '$item_code'";
		$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
		$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
		$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
		//$sql5 ="select MAX(LastRate) as LastRate from tipldb..pendalcard_rkg where flag='ItemLastFiveTrans' and itemcode='$item_code'";
		$sql5 ="select top 1 * from tipldb..pendalcard_rkg a where flag='ItemLastFiveTrans' and itemcode='$item_code' and LastMoveDt is not null  
		order by a.LastMoveDt desc";
		$sql6 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no' and item_code = '$item_code'";
		$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
		$sql8 ="select * from TIPLDB..road_permit_state a, scmdb..supp_addr_address b where a.state_code = b.supp_addr_state 
		and supp_addr_supcode = '$supp_code'";
		$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
		$sql_current_yr_con = "select RecptTotalQty from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$item_code'";
		$sql_last_supplier_details = "select SuppName, SuppRate, SuppLeadTime from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'
";
		$sql_reorder_details = "select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
		
		$query1 = $this->db->query($sql1);
		$this->db->close();
		$this->db->initialize();
		$query2 = $this->db->query($sql2);
		$query3 = $this->db->query($sql3);
		$query4 = $this->db->query($sql4);
		$query5 = $this->db->query($sql5);
		$query6 = $this->db->query($sql6);
		$query7 = $this->db->query($sql7);
		$query8 = $this->db->query($sql8);
		$query_draw = $this->db->query($sql_draw);
		$query_current_yr_con = $this->db->query($sql_current_yr_con);
		$query_last_supplier_details = $this->db->query($sql_last_supplier_details);
		$query_reorder_details = $this->db->query($sql_reorder_details);
			
		if ($query2->num_rows() > 0) {
		  foreach ($query2->result() as $row) {
			  $lastyr_cons = $row->ConsTotalQty;
			  $lastyr_cons1 = number_format($lastyr_cons,2);
			}
		} else {
			  $lastyr_cons = 0;
			  $lastyr_cons1 = 0.00;
		}
		
		if ($query3->num_rows() > 0) {
			  $current_stk = 0;
		  foreach ($query3->result() as $row) {
			  $current_stock = $row->ItemStkAccepted;
			  $current_stk = $current_stk + $current_stock;
			  $current_stk1 = number_format($current_stk,2);
			}
		} else {
			  $current_stk = 0;
			  $current_stk1 = 0.00;
		}
		
		if ($query4->num_rows() > 0) {
		  foreach ($query4->result() as $row) {
			  $reservation_qty = $row->AllocPendingTot;
			  $reservation_qty1 = number_format($reservation_qty,2);
			}
		} else {
			  $reservation_qty = 0;
			  $reservation_qty1 = 0.00;
		}
		
		if ($query5->num_rows() > 0) {
		  foreach ($query5->result() as $row) {
			  $last_price = $row->LastRate;
			  $last_price1 = number_format($last_price,2);
			}
		} else {
			  $last_price = "";
			  $last_price1 = "";
		}
		
		if ($query6->num_rows() > 0) {
		  foreach ($query6->result() as $row) {
				$ipr_type            = $row->usage;
				$sono                = $row->sono;
				$atac_no             = $row->atac_no;
				$atac_ld_date        = $row->atac_ld_date;
				$atac_need_date      = $row->atac_need_date;
				$atac_payment_terms  = $row->atac_payment_terms;
				$pm_group            = $row->pm_group;
				$category            = $row->category;
				$project_name        = $row->project_name;
				$costing             = $row->costing;
				$ipr_remarks         = $row->remarks;
				$attached_cost_sheet = $row->attch_cost_sheet;
				$ipr_need_date1      = $row->need_date;
				$ipr_need_date       = date("d-m-Y", strtotime($ipr_need_date1));
				$manufact_clrnce	 = $row->manufact_clearance;
				$dispatch_inst       = $row->dispatch_inst;
				$supp_item_remarks   = $row->pr_supp_remarks; 
			}
		} else {
				$ipr_type            = "";
				$sono                = "";
				$atac_no             = "";
				$pm_group            = "";
				$category            = "";
				$project_name        = "";
				$costing             = "";
				$ipr_remarks         = "";
				$ipr_need_date       = "";
				$atac_ld_date        = "";
				$atac_need_date      = "";
				$atac_payment_terms  = "";
				$attached_cost_sheet = "";
				$manufact_clrnce	 = "";
				$dispatch_inst       = "";
				$supp_item_remarks   = ""; 	  
		}
		
		if ($query7->num_rows() > 0) {
		  $incoming_qty_tot1 = 0;
		  foreach ($query7->result() as $row) {
			  $incoming_qty = $row->PendPoSchQty;
			  $incoming_qty_tot1 = $incoming_qty_tot1 + $incoming_qty;
			  $incoming_qty_tot  = number_format($incoming_qty_tot1,2);
			}
		} else {
			  $incoming_qty_tot = "";
		}
		
		if ($query8->num_rows() > 0) {
		  foreach ($query8->result() as $row) {
			  $supp_state = $row->state_code;
			  $road_permit_req = $row->road_permit;
			}
		} else {
			  $supp_state = "";
			  $road_permit_req = "";
		}
		
		if ($query_draw->num_rows() > 0) {
		  foreach ($query_draw->result() as $row) {
			  $drawing_no = $row->DrawingNo;
			  $purchase_uom  = $row->ItemPurcaseUom;
			  $manufact_uom = $row->ItemMnfgUom;
			}
		} else {
			  $drawing_no = "";
			  $purchase_uom  = "";
			  $manufact_uom = "";
		}
		
		if ($query_current_yr_con->num_rows() > 0) {
		  foreach ($query_current_yr_con->result() as $row) {
			  $current_yr_con = $row->RecptTotalQty;
			}
		} else {
			  $current_yr_con = "";
		}
		
		if ($query_last_supplier_details->num_rows() > 0) {
		  foreach ($query_last_supplier_details->result() as $row) {
			  $SuppName = $row->SuppName; 
			  $SuppRate = $row->SuppRate; 
			  $SuppLeadTime = $row->SuppLeadTime;
			}
		} else {
			  $SuppName = ""; 
			  $SuppRate = ""; 
			  $SuppLeadTime = "";
		}
		
		if ($query_reorder_details->num_rows() > 0) {
		  foreach ($query_reorder_details->result() as $row) {
			  $iou_reorderlevel = $row->iou_reorderlevel; 
			  $iou_reorderqty = $row->iou_reorderqty;
			}
		} else {
			  $iou_reorderlevel = ""; 
			  $iou_reorderqty = "";
		}
		
		$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";

		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0) {
		  foreach ($query->result() as $row) {
			  $ucon_confact_ntr = $row->ucon_confact_ntr;
			  $ucon_confact_dtr = $row->ucon_confact_dtr;
			  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
		  }
		} else {
			   $conversion_factor = "";  
		}
		//Supplier Average Lead Time
					
		$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
		$query_supplier = $this->db->query($sql_supplier);
		
		if ($query_supplier->num_rows() > 0) {
			$supplier_lead_time_tot = 0;
			$counter = 0;
		  foreach ($query_supplier->result() as $row) {
			  $counter++;
			  $supplier_lead_time = $row->SuppLeadTime;
			  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
		  }
		  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
		} else {
			   $counter = 0;
			   $supplier_lead_time = "";
			   $supplier_lead_time_tot = ""; 
			   $supplier_avg_lead_time = 0;  
		}
		
		$sql_supplier = "exec TIPLDB..supplier_details  '1' , '$item_code'  , '$po_ipr_no'";
		$query_supplier = $this->db->query($sql_supplier);
		
		$this->db->close();
		$this->db->initialize();
		
		$sql_supplier_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
		$query_supplier_dtl = $this->db->query($sql_supplier_dtl);
		
		 foreach ($query_supplier_dtl->result() as $row) {
			$diffdays    = $row->diffDays;
		 }
		 
		//echo "Cha==".$diffdays;
		
		if($diffdays != NULL){
			if($diffdays > 0){
				$color = "red";
			} else if($diffdays < 0){
				$color = "green";
			} else if($diffdays = 0){
				$color = "blue";
			}
		} else {
			$color = "yellow";
		}
         ?>
            <tr>
                <?php 
                    echo "<input type='hidden' name='po_s_no' value='$po_s_no' />"; 
                    echo "<input type='hidden' name='po_num' value='$po_num' />";	
                ?>
            <td>
                <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
                <?php echo $po_ipr_no; ?></a>
            </td>
            <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" 
                target="_blank"><?php echo $item_code; ?></a>
            </td>
            <td>
                <?php
                    echo $item_desc; 
                ?> 
            </td>
            <td>
                <?php
                    echo $ipr_type; 
                ?> 
            </td>
            <td>
                <?php
                    echo $sono; 
                ?> 
            </td>
            <td> 
                <a href="http://live1.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                <?php echo $atac_no; ?>
                </a>
            </td>
            <td>
            	<?php
					echo $atac_ld_date;
				?>
            </td>
            <td>
            	<?php
					echo $atac_need_date;
				?>
            </td>
            <td>
            	<?php
					echo $atac_payment_terms;
				?>
            </td>
            <td>
                <?php
                    echo $pm_group; 
                ?> 
            </td>
            <td>
                <?php
                    echo $category; 
                ?> 
            </td>
            <td>
                <?php
                    echo $project_name; 
                ?> 
            </td>
            <td>
                <?php
                    echo $costing; 
                ?> 
            </td>
            <td>
                <?php
                    echo $ipr_remarks;
                ?> 
            </td>
            <td style="background-color:<?php echo $color; ?>">
                <?php
                    echo $ipr_need_date;
                    echo "<input type='hidden' name='ipr_need_date[]' value='$ipr_need_date' />"; 
                ?> 
            </td>
            <td>
				<?php
                    echo "<input type='hidden' name='ipr_need_date[]' value='$attached_cost_sheet' />";
                    echo "<a href='http://live1.tipl.com/pr/uploads/$attached_cost_sheet' target='_blank'>$attached_cost_sheet</a>"; 
                ?> 
        	</td>
            <td>
            	<?php
					echo $supp_item_remarks;
				?>
            </td>
            <td>
            	<?php
					echo $manufact_clrnce;
				?>
            </td>
            <td>
            	<?php
					echo $dispatch_inst;
				?>
            </td>
            <td>
                <?php
                    echo  number_format($lastyr_cons,2);
                ?>
            </td>
            <td>
				<?php
                    echo number_format($current_yr_con,2); 
                ?>
        	</td>
            <td>
                <?php 
                    echo $current_stk;
                ?>
            </td>
            <td>
                <?php
                    echo $incoming_qty_tot;
                ?> 
            </td>
            <td>
                 <?php 
                    echo $reservation_qty;
                 ?>
            </td>
            
            <td>
                <?php 
                    echo $for_stk_qty; 
                ?>
            </td>
            <td>
                <?php 
                    echo $wh_code; 
                ?>
            </td>
            <td>
                <?php 
                    echo $uom; 
                ?>
            </td>
            <td>
				<?php 
                    echo $conversion_factor; 
                ?>
        	</td>
            <td>
                <?php 
                    echo number_format($supplier_avg_lead_time,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo $SuppName; 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($SuppRate,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($SuppLeadTime,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($iou_reorderlevel,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($iou_reorderqty,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo $last_price1; 
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($current_price,2); 
                ?>
            </td>
            <td>
                <?php 
                    echo $item_value; 
                ?>
            </td>
            <td>
                <?php
                    echo $item_remarks;
                ?>
            </td>
          </tr>
           <?php } ?>
        </tbody>
      </table>
</div>
</div><br /><br /><?php */?>
                                    
<!--****** REMARKS *******-->
  
<?php
  foreach ($view_po->result() as $row){
	  $po_num_new = $row->po_num; 
  }
?>
<div class="row">
    <div class='col-lg-3'> 
    <b>Attach Quotes From Supplier: </b>
    </div>
    <div class='col-lg-3'> 
    <?php 	
		$sql8 ="select * from TIPLDB..po_supplier_quotes where po_num = '$po_num_new'";
		$query8 = $this->db->query($sql8);
						
		if ($query8->num_rows() > 0) {
		  foreach ($query8->result() as $row) {
			  $supp_quotes = $row->attached_supp_quotes;
			  echo "<a href='http://live1.tipl.com/pr/uploads/$supp_quotes' target='_blank'>$supp_quotes</a><br />";
			}
		} else {
			  $supp_quotes = "";
		}	
    ?>
    </div>
    <div class="col-lg-1">
     <b> Remarks: </b>
    </div>
    <div class="col-lg-5">
    <?php
    	foreach ($view_po->result() as $row){ 
    ?>
      <b><?php echo $row->po_rmks; ?></b>
    <?php 
		break;} 
	?>
    </div>
</div><br />         
</form>

 <?php //chat history ?>
 <div class="row">
  <div class="col-lg-12">
    <h3>Chat History</h3>	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>LEVEL</th>
                <th>NAME</th>
                <th>COMMENT</th>
                <th>INSTRUCTION</th>
                <th>DATE TIME</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
                $query1 = $this->db->query($sql1);
                if ($query1->num_rows() > 0) {
				foreach ($query1->result() as $row) {
				  $level = $row->level;
				  $name  = $row->comment_by;
				  $comment = $row->comment;
				  $datentime = $row->datentime;
				  $instruction = $row->instruction;
                                
               ?>
              <tr>
                <td><?php echo $level; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $comment; ?></td>
                <td><?php echo $instruction; ?></td>
                <td><?php echo $datentime; ?></td>                            
              </tr>
              <?php 
                } } 
              ?>
            </tbody>
       </table>    
    </div>
 </div>
 
<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12">
    	<h3>Action Timing</h3>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<table align="center" class="table table-bordered">
        	<thead>
            	<th>ERP CREATED</th>
                <th>ERP CREATED DATE</th>
                <th>ERP LAST MODIFIED</th>
                <th>ERP LAST MODIFIED DATE</th>
                <th>LIVE CREATED</th>
                <th>LIVE CREATED DATE</th>
                <th>LIVE AUTHORIZED LVL1</th>
                <th>LIVE AUTHORIZED LVL1 DATE</th>
                <th>LIVE AUTHORIZED LVL2</th>
                <th>LIVE AUTHORIZED LVL2 DATE</th>
            </thead>
         	<?php foreach($view_po->result() as $row){ ?>
            <tbody>
            	<td><?php echo $row->pomas_createdby; ?></td>
                <td><?php echo $row->pomas_createddate; ?></td>
                <td><?php echo $row->pomas_lastmodifiedby; ?></td>
                <td><?php echo $row->pomas_lastmodifieddate; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->po_approvedby_lvl0; ?></td>
                <td><?php echo $row->po_approveddate_lvl0; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->po_approvedby_lvl1; ?></td>
                <td><?php echo $row->po_approveddate_lvl1; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->po_approvalby_lvl2; ?></td>
                <td><?php echo $row->po_approvaldate_lvl2; ?></td>
            </tbody>
            <?php break; } ?>
        </table>    
    </div>
</div>

<?php //Action Timing Report ?>


</section>
</section>
      		
<!--main content end-->
  
<!-- container section end -->

      
<?php include('footer.php'); ?>