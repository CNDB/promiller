<?php include'header.php'; ?>

<?php $this->load->helper('not_live_po_helper'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDING PR LIST</h4>
        </div>
    </div><br><br>
    <?php
		$pr_type= $_REQUEST['pr_type'];
		$category= $_REQUEST['category'];
		//echo "PR TYPE==".$pr_type;
		//echo "<BR>";
		//echo "CATEGORY==".$category;
	?>
    <div class="row">
    	<div class="col-lg-12">
        	<table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
            	<tr style="background-color:#0CF">
                	<td><b>S.NO.</b></td>
                	<td><b>PR NUM</b></td>
                    <td><b>ITEM CODE</b></td>
                    <td><b>ITEM DESCRIPTION</b></td>
                    <td><b>CATEGORY</b></td>
                    <td><b>PROJECT NAME</b></td>
                    <td><b>PR QUANTITY</b></td>
                    <td><b>UOM</b></td>
                    <td><b>COSTING</b></td>
                    <td><b>PR DATE</b></td>
                    <td><b>NEED DATE</b></td>
                    <td><b>PR AGE</b></td>
                    <td><b>LIVE CREATED BY</b></td>
                    <td><b>LIVE STATUS</b></td>
                </tr>
                <?php
				if($category == ''){ 
					echo prnotcreatedlive_view($pr_type); 
				} else {
					echo prnotauthlive_view($pr_type,$category);
				}
				?>
            </table>
            
        </div>
    </div> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>