<?php
	include'header.php';
	
	$nik = $this->uri->segment(3);                     
?>

      <!--main content start-->
      <body onLoad="check(<?php echo $nik; ?>,'from_pr');">
      <section id="main-content">
          <section class="wrapper">
            <div class="row"  style="margin-top:-10px">
				<div class="col-lg-12" style="background-color:#333333; padding:2px">
                      <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Create Purchase Order</h4>
				</div>
			</div><br />
            
            <div class="row">
				<div class="col-lg-12">
                      <div class="col-lg-2">
                      <b>Select PO No.</b>
                      </div>
                      <div class="col-lg-2">
                      <form action="" method="post" role="form">
                          <select name="selectpr" id="selectpr" class="form-control" onChange="check(this.value,'from_pr');">
                            
                            <?php if ($nik != '') {
							?>
                            <option selected="selected" value="<?php echo $nik; ?>" disabled="disabled"><?php echo $nik; ?></option> 
                             <?	} else {  ?>
                            
                            <option value="">--Select--</option>
                            <?php
                            foreach ($h->result() as $row)  
                             {  
                             $pono = $row->pomas_pono;
                                ?> 
                                <option  value="<?php echo $pono; ?>"><?php echo $pono; ?></option>  
                             <?php }  
                                } ?>  
                          
                          </select>
                      </form>
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
				</div>
			</div><br />    
                  <div id="detail"></div>
            <?php /*?><a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po/<?php echo $nik; ?>" target="_blank" style="font-size:16px; font-weight:bold;">
           View PO</a><?php */?>
          </section>
      </section>
      <!--main content end-->
  </section>
  <!-- container section end -->
      
	<script type="text/javascript">
		function check(str,whre_nik)
		{
		 $("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
		 
		 
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_pr')
			{
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
				$( "#datepicker1" ).datepicker();			   
		    }
		  }
		
		
		 var queryString="?q="+str;
		 if(whre_nik == 'from_pr') {		 	
		 	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/disapproved_poc/view_po" + queryString,true); 
		 }	    
		 xmlhttp.send();	
		}

</script>
 <script type="text/javascript" >
function reqd()
{
  var freight = document.getElementById("freight").value;
  var item_remarks = document.getElementById("item_remarks").value;
  var currency = document.getElementById("currency").value;
  var po_rmks = document.getElementById("po_rmks").value;
  var po_deli_type = document.getElementById("po_deli_type").value;
  var po_lead_time = document.getElementById("datepicker1").value;
  var po_manfact_clernce = document.getElementById("po_manfact_clernce").value;
  var po_dispatch_inst = document.getElementById("po_dispatch_inst").value;

  
  if(freight == "Select" || freight == ""){
  alert("Please select Incoterm!!");
  document.getElementById("freight").focus;
  return false;
  }
  
  /*if(pnf == ""){
  alert("Please enter packing and forwarding value !!");
  document.getElementById("pnf").focus;
  return false;
  }*/
  
  if(currency == "select" || currency == ""){
  alert("Please select currency !!");
  document.getElementById("currency").focus;
  return false;
  }
  
  if(item_remarks == ""){
  alert("Please enter item remarks !!");
  document.getElementById("item_remarks").focus;
  return false;
  }
  
  
  if(po_rmks == ""){
  alert("Please enter po remarks !!");
  document.getElementById("po_rmks").focus;
  return false;
  }
  
  if(po_deli_type == ""){
  alert("Please select delivery type !!");
  document.getElementById("po_deli_type").focus;
  return false;
  }
  
  if(po_lead_time == ""){
  alert("Please enter lead time in days !!");
  document.getElementById("datepicker1").focus;
  return false;
  }
  
  if(po_manfact_clernce == ""){
  alert("Please select manufacturing clearnace !!");
  document.getElementById("po_manfact_clernce").focus;
  return false;
  }
  
  if(po_dispatch_inst == ""){
  alert("Please select dispatch instruction !!");
  document.getElementById("po_dispatch_inst").focus;
  return false;
  }
}
</script>
<script type="text/javascript" >
	$(document).ready(function(){	
		check('<? echo $nik; ?>','from_pr');
	});
</script>
<script type="text/javascript">
	function qty()
	{
		  var qty           = document.getElementById("po_qty").value;
		  var bas_total     = document.getElementById("po_basic_val").value;
		  var tot_total     = document.getElementById("po_tot_val").value;
		  var cost_per_unit = document.getElementById("po_cost_pr_unt").value;
		  
		  if( qty != ""){
		  bas_total = qty * cost_per_unit;
		  tot_total = qty * cost_per_unit;
		  return false;
		  }
		  
	}
</script>
 
 <script type="text/javascript" src="http://code.jquery.com/jquery.js"></script>

<script type="text/javascript">
	function project(ni)  
		  {	 
				if(ni == 'Yes'){
					document.getElementById("div_name").style.display = 'block';
					document.getElementById("div_name1").style.display = 'block';
					
					
				} else {
					document.getElementById("div_name").style.display = 'none';
					document.getElementById("div_name1").style.display = 'none';
				}
				 
		  }
</script>
<script>
	function hidecontent(a){
		if(a == 'EXW'){
			$('#carrier_name').show();
		}else{
			$('#carrier_name').hide();
		}
	}
</script>

<?php include('footer.php'); ?>