 <?php include'header.php'; ?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ADVANCE DETAILS</h4>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-4">
        	<h4>Purchase Order No - <?php echo $po_number = $this->uri->segment(3); ?></h4>
        </div>
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
        	<table border="1" align="center" class="table table-bordered" style="font-size:9px">
                <tr>
                    <td><b>CONDITION</b></td>
                    <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                    <td>FIRST TIME PURCHASE</td>
                </tr>
                <tr>
                	<td><b>COLOR</b></td>
                    <td style="background-color:red;"></td>
                    <td style="background-color:green;"></td>
                    <td style="background-color:blue;"></td>
                    <td style="background-color:yellow;"></td>
                </tr>
            </table>
        </div>
	</div><br />
    
     <!--- PO PRINT PDF -->
    
    <div class="row">
        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_number; ?>" 
            target="_blank" style="font-size:16px; font-weight:bold;">
            <button type="button" class="form-control">PRINT PO</button></a>
        </div>
        <div class="col-lg-10"></div>
	</div><br />
    
    <form action="<?php echo base_url(); ?>index.php/advancec/insert_advance" method="post" onsubmit="return validate()">
    
    <?php include('po_details_div.php'); ?>
    
    <!--****** REMARKS & SUPPLIER QUOTES *******-->

	<?php include('po_supplier_quotes.php'); ?>
    
    <div class="row">
        <div class='col-lg-3'><b>Enter Advance Amount:</b></div>
        <div class='col-lg-3'><input type="text" id="advance_amount" name="advance_amount" value="" class="form-control"></div>
        <div class="col-lg-3"><b>Select Payment Method:</b></div>
        <div class="col-lg-3">
        	<select id="payment_method" name="payment_method" id="payment_method" class="form-control" onchange="show_content(this.value);">
            	<option value="">--Select--</option>
                <option value="Bank Transfer">Bank Transfer</option>
                <option value="Cheque">Cheque</option>
                <option value="Cash">Cash</option>
            </select>
        </div>
    </div><br />
    
    <!--Bank Transfer-->
    
    <?php foreach ($view_po->result() as $row){?>
    
    <div class="row">
        <div class="col-lg-12" style="display:none;" id="bank_transfer">
        	<table width="100%" height="auto" cellpadding="5px" cellspacing="5px" align="center">
            	<tr>
                	<td><b>Supplier Name:</b></td>
                    <td><?php echo $supplier_name = $row->po_supp_name; ?></td>
                    <td><b>Transanction ID:</b></td>
                    <td><input type="text" id="trans_id" name="trans_id" value="" class="form-control"></td>
                    <td><b>Transaction Date:</b></td>
                    <td><input type="text" id="trans_date" name="trans_date" value="" class="form-control"></td>
                </tr>
            </table><br><br>
        </div>
    </div>
    
    <?php } ?>
    
    <?php foreach ($view_po->result() as $row){?>
    
    <!--Cheque-->
    <div class="row">
        <div class="col-lg-12" style="display:none;" id="cheque">
        	<table width="100%" height="auto" cellpadding="5px" cellspacing="5px" align="center">
            	<tr>
                	<td><b>Supplier Name:</b></td>
                    <td><?php echo $supplier_name = $row->po_supp_name; ?></td>
                    <td><b>Enter Cheque No:</b></td>
                    <td><input type="text" id="cheque_no" name="cheque_no" value="" class="form-control"></td>
                    <td><b>Enter Cheque Date:</b></td>
                    <td><input type="text" id="cheque_date" name="cheque_date" value="" class="form-control"></td>
                </tr>
            </table><br><br>
        </div>
    </div>
    
    <?php } ?>
    
    <?php foreach ($view_po->result() as $row){?>
    
    <!--Cash-->
    <div class="row">
        <div class="col-lg-12" style="display:none;" id="cash">
        	<table width="100%" height="auto" cellpadding="5px" cellspacing="5px" align="center">
            	<tr>
                	<td><b>Supplier Name:</b></td>
                    <td><?php echo $supplier_name = $row->po_supp_name; ?></td>
                    <td><b>Enter Voucher No:</b></td>
                    <td><input type="text" id="voucher_no" name="voucher_no" value="" class="form-control"></td>
                    <td><b>Enter Voucher Date:</b></td>
                    <td><input type="text" id="voucher_date" name="voucher_date" value="" class="form-control"></td>
                </tr>
            </table><br><br>
        </div>
    </div>
    
    <?php }  ?>
    
    <div class="row">
        <div class="col-lg-5"></div>
        <div class="col-lg-2"><input type="submit" name="submit" value="SUBMIT" class="form-control"></div>
        <div class="col-lg-5"></div>
    </div><br />
	
    <?php //chat history ?>
 
	<?php include('po_chat_history.php'); ?>
     
    <?php //Action Timing Report ?>
    
    <?php include('po_action_timing.php'); ?>
    
    <?php //Action Timing Report ?>
                
    <?php include('footer.php'); ?>
    
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
<?php //include('footer.php'); ?>

<script>
function show_content(a){
	if(a == 'Bank Transfer'){
		
		$('#bank_transfer').show();
		$('#cheque').hide();
		$('#cash').hide();
		
		$('#cheque_no').val('');
		$('#cheque_date').val('');
		$('#voucher_no').val('');
		$('#voucher_date').val('');
		
	} else if (a == 'Cheque') {
		
		$('#bank_transfer').hide();
		$('#cheque').show();
		$('#cash').hide();
		
		$('#trans_id').val('');
		$('#trans_date').val('');
		$('#voucher_no').val('');
		$('#voucher_date').val('');
		
	} else if (a == 'Cash') {
		
		$('#bank_transfer').hide();
		$('#cheque').hide();
		$('#cash').show();
		
		$('#trans_id').val('');
		$('#trans_date').val('');
		$('#cheque_no').val('');
		$('#cheque_date').val('');
		
	} 
}
</script>

<!-- Date Picker --->
<script>

	$( "#trans_date" ).datepicker({});
	$( "#cheque_date" ).datepicker({});
	$( "#voucher_date" ).datepicker({});

</script>

<!--- validations --->
<script>
function validate(){
	var advance_amount = document.getElementById('advance_amount').value;
	var payment_method = document.getElementById('payment_method').value;
	var trans_id = document.getElementById('trans_id').value;
	var trans_date = document.getElementById('trans_date').value;
	var cheque_no = document.getElementById('cheque_no').value;
	var cheque_date = document.getElementById('cheque_date').value;
	var voucher_no = document.getElementById('voucher_no').value;
	var voucher_date = document.getElementById('voucher_date').value;
	
	if(advance_amount == ''){
		alert("Advance Amount Cannot Be Blank...");
		document.getElementById("advance_amount").focus();
	  	return false;	
	}
	
	if(payment_method == ''){
		alert("Select Payment Method...");
		document.getElementById("payment_method").focus();
	  	return false;	
	}
	
	if(payment_method == 'Bank Transfer'){
		if(trans_id == ''){
			alert("Transanction ID Cannot Be Blank...");
			document.getElementById("trans_id").focus();
	  		return false;
		}
		
		if(trans_date == ''){
			alert("Transanction Date Cannot Be Blank...");
			document.getElementById("trans_date").focus();
	  		return false;
		}
	}
	
	if(payment_method == 'Cheque'){
		if(cheque_no == ''){
			alert("Cheque No. Cannot Be Blank...");
			document.getElementById("cheque_no").focus();
	  		return false;
		}
		
		if(cheque_date == ''){
			alert("Cheque Date Cannot Be Blank...");
			document.getElementById("cheque_date").focus();
	  		return false;
		}
	}
	
	if(payment_method == 'Cash'){
		if(voucher_no == ''){
			alert("Voucher No. Cannot Be Blank...");
			document.getElementById("voucher_no").focus();
	  		return false;
		}
		
		if(voucher_date == ''){
			alert("Voucher Date Cannot Be Blank...");
			document.getElementById("voucher_date").focus();
	  		return false;
		}
	}
	
}
</script>
<!--- validations --->