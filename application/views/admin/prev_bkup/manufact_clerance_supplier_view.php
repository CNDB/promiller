<!--main content start-->
<form action="<?php echo base_url(); ?>index.php/manufact_clerancec/insert_po_sub_lvl2" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?>
                                   
<!--****** REMARKS & SUPPLIER QUOTES *******-->

<?php include('po_supplier_quotes.php'); ?>

<?php $po_num = $_REQUEST['q'];?>

<input type="hidden" name="po_num" id="po_num" value="<?php echo $po_num; ?>" />

<div class="row">
	<div class="col-lg-2">
    	<b>Enter MC Purchase Remarks :</b><b style="color:#F00">&nbsp;&nbsp;*</b>
    </div>
    <div class="col-lg-4">
    	<textarea name="mc_purchase_rmks" id="mc_purchase_rmks" class="form-control"></textarea>
    </div>
    <div class="col-lg-3">
    	<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Mail To Supplier</button>
    </div>
    <div class="col-lg-3"></div>
</div><br />

<!--- Mail POPUP ---->

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title" style="font-weight:bold; color:#000; text-align:center">Mail Box</h3>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"><b>TO</b></div>
                    <div class="col-lg-6">
                        <?php
                        foreach ($view_po->result() as $row){ 
                            $supp_email1 = $row->po_supp_email;
                            $supp_email2 = str_replace('"','',$supp_email1);
                            $supp_email  = str_replace("'","",$supp_email2)
                        ?>
                        <input type="text" name="to_mail" value="<?php echo str_replace("'","",$supp_email); ?>" class="form-control" />
                        <?php
                            break;
                        }
                        ?>
                    </div>
                    <div class="col-lg-2"></div>
                </div><br />
                
                <!-- From Email -->
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"><b>FROM</b></div>
                    <div class="col-lg-6">
                        <?php echo "<b>".$_SESSION['username']."@tipl.com</b>"; ?>
                    </div>
                    <div class="col-lg-2"></div>
                </div><br />
                
                <!-- CC Email -->
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"><b>COPY TO</b></div>
                    <div class="col-lg-6">
                        <?php 
                        $cc_email = "purchase@tipl.com";
                        echo "<b>".$cc_email."</b>"; 
                        ?>
                    </div>
                    <div class="col-lg-2"></div>
                </div><br />
                
                <!-- attached po Email -->
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2">
                        <b>ATT. PO</b>
                    </div>
                    <div class="col-lg-6">
                        <b>
                            <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_num; ?>" target="_blank">
                                <?php echo $po_num; ?>
                            </a>
                        </b>
                    </div>
                    <div class="col-lg-2"></div>
                </div><br />
                
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2">
                        <b>SUBJECT</b>
                    </div>
                    <div class="col-lg-6">
                        <?php $subject_mail = $po_num." - "."Toshniwal Industries Pvt. Ltd."; ?>
                        <input type="text" name="subject" value="<?php echo $subject_mail; ?>" class="form-control" />
                    </div>
                    <div class="col-lg-2"></div>
                </div><br />
                
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"><b>MESSAGE</b></div>
                    <div class="col-lg-6">
                        <textarea name="message_text" style="width: 325px; height: 170px;" class="form-control">PO No. -<? echo $po_num; ?>

Please Find the attachment PO and proceed for manufacturing for mentioned items in attachment.
                        </textarea>
                    </div>
                    <div class="col-lg-2"></div>
                </div><br />
                
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-7">
                        <input type="submit" name="SEND" value="Mail To Supplier" class="form-control" />
                    </div>
                    <div class="col-lg-1"></div>
                </div>
                
            </div>
        </div> 
    </div>
</div>

<!--- Mail POPUP Ends --->

</form> 

<?php //chat history ?>
 
<?php include('po_chat_history.php'); ?>
 
<?php //Action Timing Report ?>

<?php include('po_action_timing.php'); ?>

<?php //Action Timing Report ?>
    
<?php include('footer.php'); ?>