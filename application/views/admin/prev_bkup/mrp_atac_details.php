<?php
$atac_no = $_REQUEST['atac_no'];

$sql_atac_details ="select * from TIPLDB..master_atac a, TIPLDB..main_master_atac b where a.atac_no = b.atac_no and a.atac_no = '$atac_no'";

$query_atac_details = $this->db->query($sql_atac_details);

foreach($query_atac_details->result() as $row){
	$atac_no = $row->atac_no;			 
	$sono = $row->so_no;
	$atac_ld_date = $row->ld_date;  
	$atac_need_date = $row->need_date;
	$payment_term = $row->payment_term;
	$customer_payment_terms = $row->customer_payment_term;
	$project_name = $row->project_name;
	$pm_group = $row->pm_vertical;
	$isProject = $row->is_project;
	$cust_code = $row->account_no;
	$customer_name = $row->acc_name;
?>

<div class="row">
	<div class="col-lg-12"><h4>ATAC DETAILS</h4></div>
</div><br><br>

<div class="row" style="font-size:12px;">
	<div class="col-lg-2">
        <b>ATAC LD DATE</b><br /><?php echo $atac_ld_date; ?>
        <input type="hidden" name="atac_ld_date" id="atac_ld_date" value="<?php echo $atac_ld_date; ?>" />
    </div>
    <div class="col-lg-2">
        <b>ATAC NEED DATE</b><br /><?php echo $atac_need_date; ?>
        <input type="hidden" name="atac_need_date" id="atac_need_date" value="<?php echo $atac_need_date; ?>" />
    </div>
    <div class="col-lg-2">
        <b>ATAC PAYMENT TERM</b><br /><?php echo $payment_term; ?>
        <input type="hidden" name="payment_term" id="payment_term" value="<?php echo $payment_term; ?>" />
    </div>
    <div class="col-lg-2">
        <b>CUSTOMER PAYMENT TERM</b><br /><?php echo $customer_payment_terms; ?>
        <input type="hidden" name="customer_payment_terms" id="customer_payment_terms" value="<?php echo $customer_payment_terms; ?>" />
    </div>
    <div class="col-lg-2">
        <b>PROJECT NAME</b><br /><?php echo $project_name; ?>
        <input type="hidden" name="project_name" id="project_name" value="<?php echo $project_name; ?>" />
    </div>
    <div class="col-lg-2">
        <b>CUSTOMER NAME</b><br /><?php echo $customer_name; ?>
        <input type="hidden" name="customer_name" id="customer_name" value="<?php echo $customer_name; ?>" />
    </div>
</div><br><br>

<div class="row" style="font-size:12px;">
	<div class="col-lg-2">
        <b>PM GROUP</b><br /><?php echo $pm_group; ?>
        <input type="hidden" name="pm_group" id="pm_group" value="<?php echo $pm_group; ?>" />
    </div>
    <div class="col-lg-2">
        <?php /*?><b>SO NUM.</b><br /><?php echo $sono; ?><?php */?>
        <input type="hidden" name="sono" id="sono" value="<?php echo $sono; ?>" />
    </div>
    <div class="col-lg-8"></div>
</div><br><br>
<?php } ?>