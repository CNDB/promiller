<table class="table table-bordered">
    <thead>
        <tr>
            <th><b>SNO</b></th>
            <th><b>SUPPLIER CODE</b></th>
            <th><b>SUPPLIER NAME</b></th>
            <th><b>STATE</b></th>
            <th><b>NO. OF OPEN PO</b></th>
            <th><b>VALUE OF OPEN PO</b></th>
            <th><b>NO. OF PO RELEASED IN LAST 6 MONTHS</b></th>
            <th><b>VALUE OF PO PURCHASED IN LAST 6 MONTHS</b></th>
            <th><b>LAST PAYMENT TERM</b></th>
            <th><b>CURRENT PAYMENT TERM</b></th>
            <th><b>LAST FREIGHT TERM</b></th>
            <th><b>CURRENT FREIGHT TERM</b></th>
            <th><b>CATEGORIES</b></th>
            <th><b>SUPPLIER EMAIL</b></th>
            <th><b>SUPPLIER PHONE</b></th>
            <th><b>CONTACT PERSON</b></th>
        </tr>
    </thead>
    <tbody>
    <?php
        $sno = 0;
        foreach($all_supplier_data_ajax->result() as $row){
            $sno++;
            $supp_code = $row->supp_code;
            $supp_name = $row->supp_name;
            $supp_state = $row->supp_state;
            $po_open = $row->po_open;
            $po_open_value = $row->po_open_value;
            $released_po_last6_month = $row->released_po_last6_month;
            $purchase_po_last6_month_val = $row->purchase_po_last6_month_val;
            $last_payment_term = $row->last_payment_term;
            $current_payment_term = $row->current_payment_term;
            $last_frt_term = $row->last_frt_term;
            $current_frt_term = $row->current_frt_term;
            $category = $row->category;
			$supplier_email = $row->supp_email;
			$supplier_phone = $row->supp_phone;
			$contact_person = $row->contact_person;
    ?>
        <tr>
            <td><?php echo $sno; ?></td>
            <td><?php echo $supp_code; ?></td>
            <td><?php echo $supp_name; ?></td>
            <td><?php echo $supp_state; ?></td>
            <td><?php echo $po_open; ?></td>
            <td><?php echo number_format($po_open_value,2); ?></td>
            <td><?php echo $released_po_last6_month; ?></td>
            <td><?php echo number_format($purchase_po_last6_month_val,2); ?></td>
            <td><?php echo $last_payment_term; ?></td>
            <td><?php echo $current_payment_term; ?></td>
            <td><?php echo $last_frt_term; ?></td>
            <td><?php echo $current_frt_term; ?></td>
            <td><?php echo $category; ?></td>
            <td><?php echo $supplier_email; ?></td>
            <td><?php echo $supplier_phone; ?></td>
            <td><?php echo $contact_person; ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>