<?php
	include'header.php';
	$nik = $this->uri->segment(3);                      
?>

<!--main content start-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">SEND SUPPLIER FOR PURCHASE</h4>
        </div>
    </div><br />
    
    <?php include('po_header.php'); ?>
    
  </section>
</section>

<script type="text/javascript">

function check(str,whre_nik){
	$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
			 
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange=function()
	{
		if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_pr')
		{
			document.getElementById('detail').innerHTML=xmlhttp.responseText;			   
		}
}


	var queryString="?q="+str;
	if(whre_nik == 'from_pr') {		 	
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/sendsupppurc/view_po_lvl2" + queryString,true); 
	}	    
	xmlhttp.send();	
}

</script>

<script type="text/javascript" >
function reqd()
  {
	  var po_pdf = document.getElementById("po_pdf").value;

	  if(po_pdf == ""){
	  	alert("Please Print PO First!!");
	  return false;
	  }
  }
 </script>

<script type="text/javascript">
	$(document).ready(function(){	
		check('<? echo $nik; ?>','from_pr');
	});
</script>

<script type="text/javascript">
	$(function()
	{
		$(document).on('click', '.btn-add', function(e)
		{
			e.preventDefault();
	
			var controlForm = $('.controls:first'),
				currentEntry = $(this).parents('.entry:first'),
				newEntry = $(currentEntry.clone()).appendTo(controlForm);
	
			newEntry.find('input').val('');
			controlForm.find('.entry:not(:last) .btn-add')
				.removeClass('btn-add').addClass('btn-remove')
				.removeClass('btn-success').addClass('btn-danger')
				.html('<span class="glyphicon glyphicon-minus"></span>');
		}).on('click', '.btn-remove', function(e)
		{
		  $(this).parents('.entry:first').remove();
	
			e.preventDefault();
			return false;
		});
	});
</script>

<?php include('footer.php'); ?>