<!--main content start-->  
<?php

	function ms_escape_string($data) {
		if ( !isset($data) or empty($data) ) return '';
		if ( is_numeric($data) ) return $data;

		$non_displayables = array(
			'/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
			'/%1[0-9a-f]/',             // url encoded 16-31
			'/[\x00-\x08]/',            // 00-08
			'/\x0b/',                   // 11
			'/\x0c/',                   // 12
			'/[\x0e-\x1f]/'             // 14-31
		);
		foreach ($non_displayables as $regex )
		$data = preg_replace( $regex, '', $data );
		$data = str_replace("'", "''", $data );
		return $data;
	}

?> 

<?php /*?><?php echo $_REQUEST['po_from']; ?><?php */?>
   
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>PO DETAILS</h3>
    </div>
</div>
<!--******* PO INFORMATION ******-->
<form action="<?php echo base_url(); ?>index.php/createpodc/insert_po_sub" method="post" enctype="multipart/form-data" onsubmit="return reqd()">
<?php foreach ($v->result() as $row){
	$po_number         = $row->poitm_pono;
	$supp_name         = $row->supp_spmn_supname;
	$address1          = $row->supp_addr_address1;
	$address2          = $row->supp_addr_address2;
	$address3          = $row->supp_addr_address3;
	$city              = $row->supp_addr_city;
	$state             = $row->supp_addr_state;
	$country           = $row->supp_addr_country;
	$zip               = $row->supp_addr_zip;
	$supplier_email    = $row->supp_addr_email;
	$supplier_phone_no = $row->supp_addr_phone;
	$contact_person    = $row->supp_addr_contperson;
	$comp_add          = $address1.$address2."<br>".$address3.$city." (".$state.") ".$country." ".$zip;
	$po_total_value    = $row->pomas_pobasicvalue;
	$cst               = $row->pomas_tcdtotalrate;
	$tax               = $row->pomas_tcal_total_amount;
	$grand_total_po    = $po_total_value+$cst+$tax;
	$carrier_name      = $row->paytm_carrier;
	$freight           = $row->paytm_incoterm;
	$ipr_age1          = $row->diff1;
	$po_erp_created_by = $row->pomas_createdby;
	$po_erp_created_date = $row->pomas_createddate;
	$po_amend_no       = $row->pomas_poamendmentno;
	
	//echo '<h2>Po amend No: -'.$po_amend_no.'</h2>';
	
	if($ipr_age1 == '' || $ipr_age1 == NULL){
		
		$ipr_age = "No IPR age";
		
	} else {
		
		$ipr_age = $ipr_age." Days";
		
	}
	$freight_place     = $row->paytm_incoplace; 
	$payterm           = $row->paytm_payterm;
	
	$sql_payterm_desc = "select pt_description from scmdb..pt_payterm_hdr_vw where pt_paytermcode = '$payterm' 
and pt_version_no = (select max(pt_version_no) from scmdb..pt_payterm_hdr_vw where pt_paytermcode = '$payterm')";

	$payterm_desc_qry = $this->db->query($sql_payterm_desc);
	
	foreach ($payterm_desc_qry->result() as $row) {
		
		$payterm_desc      = $row->pt_description;
		
	}
	
	echo "<input type='hidden' name='payterm_desc' value='$payterm_desc' />";
	
	$insurance_liablity= $row->paytm_insuranceliability;
	$transport_mode    = $row->paytm_transmode;
	if($freight == "FOR")
	{
		$freight1 = $freight.",&nbsp;Ajmer";
	}
	else if($freight == "FORD")
	{
		$freight1 = $freight.",&nbsp;TIPL Ajmer";
	}
	else
	{
		$freight1 = $freight."&nbsp;";
	}
?>
<div class="row">
    <div class="col-lg-2">
        <b>Supplier Name:</b><br />
        <?php 
            echo $supp_name;
            echo "<input type='hidden' name='po_supp_name' value='$supp_name' />"; 
        ?>   	
    </div>
    <div class="col-lg-2">
        <b>Supplier Address: </b><br />
        <?php 
            echo $comp_add;
            echo "<input type='hidden' name='po_supp_add' value='$comp_add' />"; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Email Address: </b><b style="color:#F00">&nbsp;*</b><br />
        <?php 
            echo $supplier_email;
			//echo "<input type='hidden' name='supplier_email' value='$supplier_email' />";
			
			if($supplier_email != ''){
            	echo "<input type='hidden' name='supplier_email' id='supplier_email' value='$supplier_email' />"; 
			} else {
				echo "<input type='text' name='supplier_email' id='supplier_email'  value='$supplier_email' />";
			}
        ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Phone Number: </b><b style="color:#F00">&nbsp;*</b><br />
        <?php 
            echo $supplier_phone_no;
            //echo "<input type='hidden' name='supplier_phone_no' value='$supplier_phone_no' />"; 
			
			if($supplier_phone_no != ''){
            	echo "<input type='hidden' name='supplier_phone_no' id='supplier_phone_no' value='$supplier_phone_no' />";  
			} else {
				echo "<input type='text' name='supplier_phone_no' id='supplier_phone_no' 
				onkeypress='return event.charCode >= 48 && event.charCode <= 57' value='$supplier_phone_no' />";
			}
        ?>
    </div>
    <div class="col-lg-2">
        <b>Contact Person:</b><b style="color:#F00">&nbsp;*</b><br />
        <?php 
            echo $contact_person;
            //echo "<input type='hidden' name='contact_person' value='$contact_person' />"; 
			
			if($contact_person != ''){
            	echo "<input type='hidden' name='contact_person' id='contact_person' value='$contact_person' />";   
			} else {
				echo "<input type='text' name='contact_person' id='contact_person' value='$contact_person' />"; 
			}
        ?>
    </div>
    <div class="col-lg-2">
        <b>Order Value: </b><br />
        <?php 
            echo number_format($grand_total_po,2);
            echo "<input type='hidden' name='po_total_value' value='$grand_total_po' />"; 
        ?>
    </div>
</div><br /><br />
<div class="row">
	<div class="col-lg-2">
        <b>PO Age: </b><br />
        <?php 
            echo $row->diff."&nbsp;Days"; 
        ?>
    </div>
	<div class="col-lg-2">
        <b>IPR Age: </b><br />
        <?php 
            echo $ipr_age; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Payment Terms :</b><br />
        <?php 
            echo $payterm; 
            echo "<input type='hidden' name='payterm' value='$payterm' />";
			echo "<input type='hidden' name='payterm_desc' value='$payterm_desc' />"; 
        ?>  	
    </div>
    <div class="col-lg-2">
        <b>Freight Terms:</b><br />
        <?php
            echo $freight1;
            echo "<input type='hidden' name='freight' value='$freight' />"; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Freight Place : </b><br />
        <?php
            echo $freight_place;
            echo "<input type='hidden' name='freight_place' value='$freight_place' />"; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Carrier Name : </b><br />
        <?php 
		  	echo $carrier_name;
			echo "<input type='hidden' name='carrier_name' value='$carrier_name' />"; 
	    ?>
    </div>
</div><br /><br />
<div class="row">
	<div class="col-lg-2">
        <b>Insurance Term : </b><br />
        <?php
            echo $insurance_liablity;
            echo "<input type='hidden' name='insurance_liablity' value='$insurance_liablity' />"; 
        ?>
    </div>
	<div class="col-lg-2">
    	<b>Mode Of Transport :- </b><br />
		<?php echo $transport_mode; ?>
    </div>
    <div class="col-lg-2">
        <b>Delivery Type : </b><b style="color:#F00">&nbsp;*</b>
    </div>
    <div class="col-lg-2">
     <?php
        if($freight == 'EX WORKS' || $freight == 'EXW')
        {
            echo "<select class='form-control' name='po_deli_type' id='po_deli_type'>
                    <option value=''>--Select--</option>
                    <option value='Door Delivery'>Door Delivery</option>
                    <option value='Godown Delivery'>Godown Delivery</option>
                   </select>";
        } 
        else if($freight == 'FOR') 
        {
            echo "<select class='form-control' name='po_deli_type' id='po_deli_type' readonly='readonly'>
                    <option value='select'>--Select--</option>
                   </select>";
        } 
        else if ($freight == 'FORD')
        {
            echo "<select class='form-control' name='po_deli_type' id='po_deli_type'>
                    <option value='Door Delivery'>Door Delivery</option>
                   </select>";
        }
		else 
		{
			echo "<select class='form-control' name='po_deli_type' id='po_deli_type' readonly='readonly'>
                    <option value='select'>--Select--</option>
                   </select>";
		}
     ?>
    </div>
    <div class="col-lg-2">
    	<b>Enter Expected Material Recipt Date</b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2">          
    	<input type="text" name="po_lead_time" id="datepicker1" value="" class="form-control" autocomplete="off"/>
    </div>
</div><br />
<div class="row">
	<div class="col-lg-2">
    	<b>Select Currency</b>
    </div>  
    <div class="col-lg-2">
		<?php 
			$po_three_letters  = substr($po_number,0,3);
			if($po_three_letters == 'FPO'){
				echo"<select name='currency' id='currency' class='form-control'>
						<option value=''>Select</option>
						<option value='INR'>INR</option>
						<option value='USD'>USD</option>
						<option value='GBP'>GBP</option>
						<option value='YEN'>YEN</option>
					</select>";
			} else {
			   echo"<input type='text' name='currency' id='currency' value='INR' readonly='readonly' class='form-control' />";
			}
        ?>
    </div>
    <div class="col-lg-2">
    	<b>Freight Type</b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2"> 
    <?php
	if($freight == 'EX WORKS' || $freight == 'EXW')
	{         
       	echo "<select class='form-control' id='freight_type' name='freight_type'>
        	   <option value=''>-- Select --</option>
			   <option value='To Pay'>To Pay</option>
			   <option value='Paid Billed'>Paid Billed</option>
             </select>";
	}
	else if($freight == 'FOR')
	{
		echo "<select class='form-control' id='freight_type' name='freight_type'>
        	   <option value='Prepaid By Supplier'>Prepaid By Supplier</option>
             </select>";
	}
	else if($freight == 'FORD')
	{
		echo "<select class='form-control' id='freight_type' name='freight_type'>
        	   <option value='Prepaid By Supplier'>Prepaid By Supplier</option>
             </select>";
	}
	else
	{
		echo "<select class='form-control' id='freight_type' name='freight_type'>
        	   <option value='select'>-- Select --</option>
             </select>";
	}
	?> 
    </div>
    <div class="col-lg-2">
    	<b>Enter Approximate Freight : </b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2">
    <?php 
	if($freight == 'EX WORKS' || $freight == 'EXW')
	{         
        echo "<input type='text' name='approx_freight' id='approx_freight' value='' class='form-control' />";
	} 
	else
	{
		echo "<input type='hidden' name='approx_freight' id='approx_freight' value='No Value' class='form-control' />";
	}
	?>
    </div>
</div><br />
<div class="row">
	<div class="col-lg-2">
    	<b>LD Applicable : </b><b style="color:#F00">&nbsp;*</b>
    </div>
    <div class="col-lg-2">
    	<select name="ld_applicable" id="ld_applicable" class="form-control">
        	<option value="">--select--</option>
            <option value="Yes">Yes</option>
            <option value="No">No</option>
        </select>
    </div>
	<div class="col-lg-2">
    	<b>Enter Special Instructions For Supplier : </b>
    </div>
    <div class="col-lg-6">
    	<input type="text" name="spcl_inst_supp" id="spcl_inst_supp" value=""  class="form-control"/>
    </div>  
</div><br />
<div class="row" style="font-weight:bold; text-transform:uppercase;">
	<div class="col-lg-4">
    	<b>PO ERP CREATED BY :- </b><?php echo $po_erp_created_by; ?>
    </div>
    <div class="col-lg-4">
    	<b>PO ERP CREATED DATE :- </b><?php echo date("d-m-Y", strtotime($po_erp_created_date)); ?>
    </div>
    <div class="col-lg-4">
    </div>
</div><br />
<?php break;} ?>
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>ITEM DETAILS</h3>
    </div>
</div>
<!--******** ITEM INFORMATION ********-->
<div class="row" style=" overflow-x:auto;">
<table class="table table-bordered" id="dataTable" border="1">
    <thead>
      <tr>
        <th>IPR No.</th> 
        <th>Item Code</th>                             
        <th>Item Desc.</th> 
        <th>PR Type</th> 
        <th>SO No.</th> 
        <th>ATAC No.</th>
        <th>ATAC LD Date.</th>
        <th>ATAC Need Date.</th>
        <th>ATAC Payment Terms.</th>
        <th>PM Group</th> 
        <th>Category</th> 
        <th>Project Name</th> 
        <th>Costing</th> 
        <th>PR Remarks</th> 
        <th>PR Need Date</th>  
        <th>Attached Cost Sheet</th>
        <th>Item Remarks for Supplier</th>
        <th>Manufacturing Clearnace</th>
        <th>Dispatch Instruction</th>                                                            
        <th>Last 1 Yr. Cons.</th>
        <th>Current Yr. Cons.</th>
        <th>Current Stock</th>
        <th>Total Incoming Stock</th>                                                           
        <th>Reservation Qty</th>                                                              
        <th>Order Quantity</th>
        <th>Warehouse Code</th>                                                    
        <th>UOM</th>
        <th>Conversion Factor With Stock UOM</th>
        <th>Supplier Lead Time(AVG)</th>
        <th>Last Supplier</th>
        <th>Last Supplier Rate</th>
        <th>Last Supplier Lead Time</th>
        <th>Reorder Level</th>
        <th>Reorder Qty</th>
        <th>Last Price</th>
        <th>Current Price</th> 
        <th>Total Item Value</th>
        <th>Item Remarks</th>
      </tr>
    </thead>
    <tbody>
<?php
   foreach ($v->result() as $row)  
	{   
		$po_line_no        = $row->poprq_polineno;
		$item_desc1        = $row->ml_itemvardesc;
		$item_desc2        = $row->lov_matlspecification;
		$item_desc         = $item_desc1.' '.$item_desc2;
		//$item_desc_new     = ms_escape_string($item_desc);
		$item_desc_new     = str_replace("'","",$item_desc);
		
		$po_num            = $row->poitm_pono;
		$supp_code         = $row->supp_spmn_supcode;
		$supp_name         = $row->supp_spmn_supname;
		$po_ipr_no         = $row->poprq_prno;
		$item_code         = $row->poitm_itemcode;
		$po_date           = $row->pomas_podate;
		
		$item_code1 = urlencode($item_code);
				
		if(strpos($item_code1, '%2F') !== false)
		{
			$item_code2 = str_replace("%2F","chandra",$item_code1);
		}
		else 
		{
			$item_code2 = $item_code1;
		}
		
		$carrier_name      = $row->paytm_carrier;
		$freight           = $row->paytm_incoterm; 
		$payterm           = $row->paytm_payterm;
		$po_quantity       = $row->poitm_order_quantity;
		$for_stk_qty       = $row->poitm_order_quantity;
		$for_stk_qty1      = number_format($for_stk_qty,2);
		$uom               = $row->poitm_puom;
		$current_price     = $row->poitm_po_cost;
		$item_value        = $row->poitm_itemvalue; 
		$po_total_value    = $row->pomas_pobasicvalue;
		$po_date           = $row->pomas_podate;
		$cost_pr_unt       = $row->poitm_costper;
		$need_date         = $row->poitm_needdate;
		$wh_code           = $row->poitm_warehousecode;
		$odr_qty           = $row->poitm_order_quantity;
		$totalcost         = $odr_qty * $cost_pr_unt;
		$address1          = $row->supp_addr_address1;
		$address2          = $row->supp_addr_address2;
		$address3          = $row->supp_addr_address3;
		$city              = $row->supp_addr_city;
		$state             = $row->supp_addr_state;
		$country           = $row->supp_addr_country;
		$zip               = $row->supp_addr_zip;
		$comp_add          = $address1.$address2."<br>".$address3.$city." (".$state.") ".$country." ".$zip;
		$supp_email        = $row->supp_addr_email;
		$currency          = $row->pomas_pocurrency;
		//new pdf elements
		$po_amend_no 	   = $row->pomas_poamendmentno;
		$po_type 		   = $row->pomas_potype;
		$supp_phone        = $row->supp_addr_phone;
		$contact_person    = $row->supp_addr_contperson;
		$pay_mode          = $row->paytm_paymode;
		$trans_mode        = $row->paytm_transmode;
		$partial_ship      = $row->paytm_shippartial;
			
		$sql1 ="exec tipldb..pendalcard '$item_code'";
		$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
		$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
		$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
		//$sql5 ="select MAX(LastRate) as LastRate from tipldb..pendalcard_rkg where flag='ItemLastFiveTrans' and itemcode='$item_code'";
		$sql5 ="select LastRate,* from tipldb..pendalcard_rkg where flag='ItemLastFiveTrans' and itemcode='$item_code' 
and LastPRSCRDate = (select MAX(LastPRSCRDate) from TIPLDB..pendalcard_rkg where itemcode = '$item_code')";
		$sql6 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no' and item_code = '$item_code'";
		$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
		$sql8 ="select * from TIPLDB..road_permit_state a, scmdb..supp_addr_address b where a.state_code = b.supp_addr_state 
		and supp_addr_supcode = '$supp_code'";
		$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
		$sql_current_yr_con = "select RecptTotalQty from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$item_code'";
		$sql_last_supplier_details = "select SuppName, SuppRate, SuppLeadTime from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'
";
		$sql_reorder_details = "select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
		
		$query1 = $this->db->query($sql1);
		$this->db->close();
		$this->db->initialize();
		$query2 = $this->db->query($sql2);
		$query3 = $this->db->query($sql3);
		$query4 = $this->db->query($sql4);
		$query5 = $this->db->query($sql5);
		$query6 = $this->db->query($sql6);
		$query7 = $this->db->query($sql7);
		$query8 = $this->db->query($sql8);
		$query_draw = $this->db->query($sql_draw);
		$query_current_yr_con = $this->db->query($sql_current_yr_con);
		$query_last_supplier_details = $this->db->query($sql_last_supplier_details);
		$query_reorder_details = $this->db->query($sql_reorder_details);
			
		if ($query2->num_rows() > 0) {
		  foreach ($query2->result() as $row) {
			  $lastyr_cons = $row->ConsTotalQty;
			  $lastyr_cons1 = number_format($lastyr_cons,2);
			}
		} else {
			  $lastyr_cons = 0;
			  $lastyr_cons1 = 0.00;
		}
		
		if ($query3->num_rows() > 0) {
			  $current_stk = 0;
		  foreach ($query3->result() as $row) {
			  $current_stock = $row->ItemStkAccepted;
			  $current_stk = $current_stk + $current_stock;
			  $current_stk1 = number_format($current_stk,2);
			}
		} else {
			  $current_stk = 0;
			  $current_stk1 = 0.00;
		}
		
		if ($query4->num_rows() > 0) {
		  foreach ($query4->result() as $row) {
			  $reservation_qty = $row->AllocPendingTot;
			  $reservation_qty1 = number_format($reservation_qty,2);
			}
		} else {
			  $reservation_qty = 0;
			  $reservation_qty1 = 0.00;
		}
		
		if ($query5->num_rows() > 0) {
		  foreach ($query5->result() as $row) {
			  $last_price = $row->LastRate;
			  $last_price1 = number_format($last_price,2);
			}
		} else {
			  $last_price = "";
			  $last_price1 = "";
		}
		
		if ($query6->num_rows() > 0) {
		  foreach ($query6->result() as $row) {
				$ipr_type            = $row->usage;
				$sono                = $row->sono;
				$atac_no             = $row->atac_no;
				$atac_ld_date        = $row->atac_ld_date;
				$atac_need_date      = $row->atac_need_date;
				$atac_payment_terms  = $row->atac_payment_terms;
				$pm_group            = $row->pm_group;
				$category            = $row->category;
				$project_name        = $row->project_name;
				$costing             = $row->costing;
				$ipr_remarks         = $row->remarks;
				$attached_cost_sheet = $row->attch_cost_sheet;
				$ipr_need_date1      = $row->need_date;
				$ipr_need_date       = date("d-m-Y", strtotime($ipr_need_date1));
				$manufact_clrnce	 = $row->manufact_clearance;
				$dispatch_inst       = $row->dispatch_inst;
				$supp_item_remarks   = $row->pr_supp_remarks; 
			}
		} else {
				$ipr_type            = "";
				$sono                = "";
				$atac_no             = "";
				$pm_group            = "";
				$category            = "";
				$project_name        = "";
				$costing             = "";
				$ipr_remarks         = "";
				$ipr_need_date       = "";
				$atac_ld_date        = "";
				$atac_need_date      = "";
				$atac_payment_terms  = "";
				$attached_cost_sheet = "";
				$manufact_clrnce	 = "";
				$dispatch_inst       = "";
				$supp_item_remarks   = ""; 	  
		}
		
		if ($query7->num_rows() > 0) {
		  $incoming_qty_tot1 = 0;
		  foreach ($query7->result() as $row) {
			  $incoming_qty = $row->PendPoSchQty;
			  $incoming_qty_tot1 = $incoming_qty_tot1 + $incoming_qty;
			  $incoming_qty_tot  = number_format($incoming_qty_tot1,2);
			}
		} else {
			  $incoming_qty_tot = "";
		}
		
		if ($query8->num_rows() > 0) {
		  foreach ($query8->result() as $row) {
			  $supp_state = $row->state_code;
			  $road_permit_req = $row->road_permit;
			}
		} else {
			  $supp_state = "";
			  $road_permit_req = "";
		}
		
		if ($query_draw->num_rows() > 0) {
		  foreach ($query_draw->result() as $row) {
			  $drawing_no = $row->DrawingNo;
			  $purchase_uom  = $row->ItemPurcaseUom;
			  $manufact_uom = $row->ItemMnfgUom;
			}
		} else {
			  $drawing_no = "";
			  $purchase_uom  = "";
			  $manufact_uom = "";
		}
		
		if ($query_current_yr_con->num_rows() > 0) {
		  foreach ($query_current_yr_con->result() as $row) {
			  $current_yr_con = $row->RecptTotalQty;
			}
		} else {
			  $current_yr_con = "";
		}
		
		if ($query_last_supplier_details->num_rows() > 0) {
		  foreach ($query_last_supplier_details->result() as $row) {
			  $SuppName = $row->SuppName; 
			  $SuppRate = $row->SuppRate; 
			  $SuppLeadTime = $row->SuppLeadTime;
			}
		} else {
			  $SuppName = ""; 
			  $SuppRate = ""; 
			  $SuppLeadTime = "";
		}
		
		if ($query_reorder_details->num_rows() > 0) {
		  foreach ($query_reorder_details->result() as $row) {
			  $iou_reorderlevel = $row->iou_reorderlevel; 
			  $iou_reorderqty = $row->iou_reorderqty;
			}
		} else {
			  $iou_reorderlevel = ""; 
			  $iou_reorderqty = "";
		}
		
		$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";

		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0) {
		  foreach ($query->result() as $row) {
			  $ucon_confact_ntr = $row->ucon_confact_ntr;
			  $ucon_confact_dtr = $row->ucon_confact_dtr;
			  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
		  }
		} else {
			   $conversion_factor = "";  
		}
		/* */ 
		//Supplier Average Lead Time
					
		$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
		$query_supplier = $this->db->query($sql_supplier);
		
		if ($query_supplier->num_rows() > 0) {
			$supplier_lead_time_tot = 0;
			$counter = 0;
		  foreach ($query_supplier->result() as $row) {
			  $counter++;
			  $supplier_lead_time = $row->SuppLeadTime;
			  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
		  }
		  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
		} else {
			   $counter = 0;
			   $supplier_lead_time = "";
			   $supplier_lead_time_tot = ""; 
			   $supplier_avg_lead_time = 0;  
		}
        /* */
		
		$sql_supplier = "exec TIPLDB..supplier_details  '1' , '$item_code'  , '$po_ipr_no'";
		$query_supplier = $this->db->query($sql_supplier);
		
		$this->db->close();
		$this->db->initialize();
		
		$sql_supplier_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
		$query_supplier_dtl = $this->db->query($sql_supplier_dtl);
		
		 foreach ($query_supplier_dtl->result() as $row) {
			$diffdays    = $row->diffDays;
		 }
		 
		//echo "Cha==".$diffdays;
		
		if($diffdays != NULL){
			if($diffdays > 0){
				$color = "red";
			} else if($diffdays < 0){
				$color = "green";
			} else if($diffdays = 0){
				$color = "blue";
			}
		} else {
			$color = "yellow";
		}
		
		
?>         
      <tr>
			<?php 
                echo "<input type='hidden' name='po_num' value='$po_num' />";
				echo "<input type='hidden' name='po_date' value='$po_date' />";
                echo "<input type='hidden' name='po_line_no[]' value='$po_line_no' />";
                echo "<input type='hidden' name='po_supp_code' value='$supp_code' />";
                echo "<input type='hidden' name='po_qty' value='$po_quantity' />";
                echo "<input type='hidden' name='po_amend_no' value='$po_amend_no' />"; 
                echo "<input type='hidden' name='po_type' value='$po_type' />";
                echo "<input type='hidden' name='supp_phone' value='$supp_phone' />";
                echo "<input type='hidden' name='contact_person' value='$contact_person' />"; 
                echo "<input type='hidden' name='pay_mode' value='$pay_mode' />";
                echo "<input type='hidden' name='trans_mode' value='$trans_mode' />";
                echo "<input type='hidden' name='partial_ship' value='$partial_ship' />";
				echo "<input type='hidden' name='po_date' value='$po_date' />";
				echo "<input type='hidden' name='po_cost_pr_unt' value='$cost_pr_unt' />";
				echo "<input type='hidden' name='po_need_date' value='$need_date' />";
				echo "<input type='hidden' name='po_wh_code' value='$wh_code' />";
				echo "<input type='hidden' name='po_basic_val' value='$totalcost' />";
				echo "<input type='hidden' name='po_tot_val' value='$totalcost' />";
				echo "<input type='hidden' name='po_supp_email' value='$supp_email' />";
				echo "<input type='hidden' name='supp_state' value='$supp_state' />";
				echo "<input type='hidden' name='road_permit_req' value='$road_permit_req' />";
            ?>            
        <td>
            <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
            <?php echo $po_ipr_no; ?></a>
            <?php  
                echo "<input type='hidden' name='po_ipr_no[]' value='$po_ipr_no' />";
            ?>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" 
            target="_blank"><?php echo $item_code; ?></a>
            <?php  
                echo "<input type='hidden' name='po_item_code[]' value='$item_code' />";
            ?>
        </td>
        <td>
            <?php
                echo $item_desc;
                echo "<input type='hidden' name='item_desc[]' value='$item_desc_new' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $ipr_type;
                echo "<input type='hidden' name='ipr_type[]' value='$ipr_type' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $sono;
                echo "<input type='hidden' name='sono[]' value='$sono' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo "<input type='hidden' name='atac_no[]' value='$atac_no' />"; 
            ?>
            <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                <?php echo $atac_no; ?>
            </a> 
        </td>
        <td>
            <?php
                echo $atac_ld_date;
                echo "<input type='hidden' name='atac_ld_date[]' value='$atac_ld_date' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $atac_need_date;
                echo "<input type='hidden' name='atac_need_date[]' value='$atac_need_date' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $atac_payment_terms;
                echo "<input type='hidden' name='atac_payment_terms[]' value='$atac_payment_terms' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $pm_group;
                echo "<input type='hidden' name='pm_group[]' value='$pm_group' />";
				 echo "<input type='hidden' name='pm_group1' value='$pm_group' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $category;
                echo "<input type='hidden' name='category[]' value='$category' />"; 
				echo "<input type='hidden' name='category1' value='$category' />";
            ?> 
        </td>
        <td>
            <?php
                echo $project_name;
                echo "<input type='hidden' name='project_name[]' value='$project_name' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $costing;
                echo "<input type='hidden' name='costing[]' value='$costing' />"; 
            ?> 
        </td>
        <td>
            <?php
                echo $ipr_remarks;
                echo "<input type='hidden' name='ipr_remarks[]' value='$ipr_remarks' />"; 
            ?> 
        </td>
        <td style="background-color:<?php echo $color; ?>">
            <?php
                echo $ipr_need_date;
                echo "<input type='hidden' name='ipr_need_date[]' value='$ipr_need_date' />"; 
				echo "<input type='hidden' name='color[]' value='$color' />";
            ?> 
        </td>
        <td>
            <?php
                echo "<input type='hidden' name='attached_cost_sheet[]' value='$attached_cost_sheet' />";
				echo "<a href='http://live.tipl.com/pr/uploads/$attached_cost_sheet' target='_blank'>$attached_cost_sheet</a>"; 
            ?> 
        </td>
        <td>
        	<?php 
				echo $supp_item_remarks; 
				echo "<input type='hidden' name='supp_item_remarks[]' id='supp_item_remarks' value='$supp_item_remarks'  />";
			?>
        </td>
        <td>
        	<?php 
				echo $manufact_clrnce; 
				echo "<input type='hidden' name='manufact_clrnce[]' id='manufact_clrnce' value='$manufact_clrnce'  />";
			?>
        </td>
        <td>
        	<?php 
				echo $dispatch_inst; 
				echo "<input type='hidden' name='dispatch_inst[]' id='dispatch_inst' value='$dispatch_inst'  />";
			?>
        </td>
        <td>
            <?php
                echo $lastyr_cons1; 
                echo "<input type='hidden' name='lastyr_cons[]' value='$lastyr_cons' />";
            ?>
        </td>
        <td>
            <?php
                echo number_format($current_yr_con,2); 
                echo "<input type='hidden' name='current_yr_con[]' value='$current_yr_con' />";
            ?>
        </td>
        <td>
            <?php 
                echo $current_stk1;
                echo "<input type='hidden' name='current_stk[]' value='$current_stk' />";
            ?>
        </td>
        <td>
            <?php
                echo $incoming_qty_tot;
                echo "<input type='hidden' name='incoming_qty_tot[]' value='$incoming_qty_tot1' />"; 
            ?> 
        </td>
        <td>
             <?php 
                echo $reservation_qty1; 
                echo "<input type='hidden' name='reservation_qty[]' value='$reservation_qty' />";
             ?>
        </td>
        <td>
            <?php 
                echo $for_stk_qty1; 
                echo "<input type='hidden' name='for_stk_quantity[]' value='$for_stk_qty' />";
            ?>
        </td>
        <td>
            <?php 
                echo $wh_code; 
                echo "<input type='hidden' name='wh_code' value='$wh_code' />";
            ?>
        </td>
        <td>
            <?php 
                echo $uom; 
                echo "<input type='hidden' name='po_uom[]' value='$uom' />";
            ?>
        </td>
        <td>
			<?php 
                echo $conversion_factor; 
                echo "<input type='hidden' name='conversion_factor[]' value='$conversion_factor' />";
            ?>
        </td>
        <? /*Avegrage Lead Time of Supplier */ ?>
        <td>
			<?php 
                echo number_format($supplier_avg_lead_time,2); 
                echo "<input type='hidden' name='supplier_avg_lead_time[]' value='$supplier_avg_lead_time' />";
            ?>
        </td>
        <? /*Avegrage Lead Time of Supplier */ ?>
        <? /*Last Supplier Details */ ?>
        <td>
			<?php 
                echo $SuppName; 
                echo "<input type='hidden' name='SuppName[]' value='$SuppName' />";
            ?>
        </td>
        <td>
			<?php 
                echo number_format($SuppRate,2); 
                echo "<input type='hidden' name='SuppRate[]' value='$SuppRate' />";
            ?>
        </td>
        <td>
			<?php 
                echo number_format($SuppLeadTime,2); 
                echo "<input type='hidden' name='SuppLeadTime[]' value='$SuppLeadTime' />";
            ?>
        </td>
        <? /*Last Supplier Details */ ?>
        <? /*Reorder Details */ ?>
        <td>
			<?php 
                echo number_format($iou_reorderlevel,2); 
                echo "<input type='hidden' name='iou_reorderlevel[]' value='$iou_reorderlevel' />";
            ?>
        </td>
        <td>
			<?php 
                echo number_format($iou_reorderqty,2); 
                echo "<input type='hidden' name='iou_reorderqty[]' value='$iou_reorderqty' />";
            ?>
        </td>
        <? /*Reorder Details */ ?>
        <td>
            <?php 
                echo $last_price1; 
                echo "<input type='hidden' name='last_price[]' value='$last_price' />";
            ?>
        </td>
        <td>
            <?php 
                echo number_format($current_price,2); 
                echo "<input type='hidden' name='current_price[]' value='$current_price' />";
            ?>
        </td>
        <td>
            <?php 
                echo number_format($item_value,2); 
                echo "<input type='hidden' name='item_value[]' value='$item_value' />";
            ?>
        </td>
        <td>
            <input type="text" name="item_remarks[]" id="item_remarks" value=""  />
        </td>
      </tr>
    <?php }?> 
 </tbody>
</table>
</div>	
		
<!--********* REMARKS *********-->  
  
<br /><br />                
<div class="row">
  <div class='col-lg-3'> 
    <b>Attach Quotes From Supplier</b><b style="color:#F00">&nbsp;*</b>
  </div>
  <div class='col-lg-4'> 
    <!--<input type="file" name="po_quote_frmsupp[]"  id="po_quote_frmsupp" class="form-control" multiple />-->
    <div class="controls">   
        <div class="entry input-group col-xs-3">
            <input type="file" class="btn btn-primary" name="po_quote_frmsupp[]" id="po_quote_frmsupp" multiple>
            <span class="input-group-btn">
            <button class="btn btn-success btn-add" type="button">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            </span>
        </div>
    </div>  
  </div> 
  <div class="col-lg-2">
     <b> Enter Remarks</b>
  </div>
  <div class="col-lg-3">
      <input type="text" name="po_rmks" id="po_rmks" value=""  class="form-control"/>
  </div>
</div> <br />
            
<div class="row">
    <div class="col-lg-3">    
    </div>
    
    <div class="col-lg-3"> 
        <input type="submit" name="po_approval_lvl0" value="Send PO For Approval" class="form-control" 
        style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px" />     
    </div>
    
    <div class="col-lg-3">    
    </div>
</div><br /><br /><br />

</form>

<?php //chat history ?>
 <div class="row">
  <div class="col-lg-12">
    <h3>Chat History</h3>	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>LEVEL</th>
                <th>NAME</th>
                <th>COMMENT</th>
                <th>DATE TIME</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
                $query1 = $this->db->query($sql1);
                if ($query1->num_rows() > 0) {
				foreach ($query1->result() as $row) {
				  $level = $row->level;
				  $name  = $row->comment_by;
				  $comment = $row->comment;
				  $datentime = $row->datentime;
                                
               ?>
              <tr>
                <td><?php echo $level; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $comment; ?></td>
                <td><?php echo $datentime; ?></td>                            
              </tr>
              <?php 
                } } 
              ?>
            </tbody>
       </table>    
    </div>
 </div>
<?php //chat history ?>
 

<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12">
    	<h3>Action Timing</h3>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<table align="center" class="table table-bordered">
        	<thead>
            	<th>ERP CREATED</th>
                <th>ERP CREATED DATE</th>
                <th>ERP LAST MODIFIED</th>
                <th>ERP LAST MODIFIED DATE</th>
            </thead>
         	<?php foreach($v->result() as $row){ ?>
            <tbody>
            	<td><?php echo $row->pomas_createdby; ?></td>
                <td><?php echo $row->pomas_createddate; ?></td>
                <td><?php echo $row->pomas_lastmodifiedby; ?></td>
                <td><?php echo $row->pomas_lastmodifieddate; ?></td>
            </tbody>
            <?php break; } ?>
        </table>    
    </div>
</div>

<?php //Action Timing Report ?>	
<!--main content end-->
<!-- container section end -->
      
<?php //include('footer.php'); ?>