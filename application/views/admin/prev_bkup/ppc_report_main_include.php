<?php

if($condition != ''){
	
	$pr_type = $condition;
	
} else {

	$pr_type="CGP','IPR','FPR";

}

//echo $pr_type;
//die;

$current_date = date("Y-m-d");

$sql1="select DATEADD(dd, -(DATEPART(dw, '$current_date')-1), '$current_date') as current_week_start_date";

$qry1 = $this->db->query($sql1);

foreach($qry1->result() as $row){
	$cwsd=substr($row->current_week_start_date,0,11);
}

$sql2="select DATEADD(dd, 7-(DATEPART(dw, '$current_date')), '$current_date') as current_week_end_date";

$qry2 = $this->db->query($sql2);

foreach($qry2->result() as $row){
	$cwed=substr($row->current_week_end_date,0,11);
}

//First Week Start & End Date

$fwd = date('Y-m-d', strtotime("+7 days"));

$sql3="select DATEADD(dd, -(DATEPART(dw, '$current_date')-8), '$current_date') as first_week_start_date";

$qry3 = $this->db->query($sql3);

foreach($qry3->result() as $row){
	$fwsd=substr($row->first_week_start_date,0,11);
}

$sql4="select DATEADD(dd, 14-(DATEPART(dw, '$current_date')), '$current_date') as first_week_end_date"; 

$qry4 = $this->db->query($sql4);

foreach($qry4->result() as $row){
	$fwed=substr($row->first_week_end_date,0,11);
}

//Second Week Start & End Date

$swd=date('Y-m-d', strtotime("+14 days"));

$sql3="select DATEADD(dd, -(DATEPART(dw, '$current_date')-15), '$current_date') as second_week_start_date";

$qry3 = $this->db->query($sql3);

foreach($qry3->result() as $row){
	$swsd=substr($row->second_week_start_date,0,11);
}

$sql4="select DATEADD(dd, 21-(DATEPART(dw, '$current_date')), '$current_date') as second_week_end_date"; 

$qry4 = $this->db->query($sql4);

foreach($qry4->result() as $row){
	$swed=substr($row->second_week_end_date,0,11);
}

//Third Week Start & End Date

$twd=date('Y-m-d', strtotime("+21 days"));

$sql5="select DATEADD(dd, -(DATEPART(dw, '$current_date')-22), '$current_date') as third_week_start_date";

$qry5 = $this->db->query($sql5);

foreach($qry5->result() as $row){
	$twsd=substr($row->third_week_start_date,0,11);
}

$sql6="select DATEADD(dd, 28-(DATEPART(dw, '$current_date')), '$current_date') as third_week_end_date";

$qry6 = $this->db->query($sql6);

foreach($qry6->result() as $row){
	$twed=substr($row->third_week_end_date,0,11);
}

//Fourth Week Start & End Date

$ftwd=date('Y-m-d', strtotime("+28 days"));

$sql7="select DATEADD(dd, -(DATEPART(dw, '$current_date')-29), '$current_date') as fourth_week_start_date";

$qry7 = $this->db->query($sql7);

foreach($qry7->result() as $row){
	$ftwsd=substr($row->fourth_week_start_date,0,11);
}

$sql8="select DATEADD(dd, 35-(DATEPART(dw, '$current_date')), '$current_date') as fourth_week_end_date"; 

$qry8 = $this->db->query($sql8);

foreach($qry8->result() as $row){
	$ftwed=substr($row->fourth_week_end_date,0,11);
}

//die;
?>

<?php 
//All Categories 
$all_category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing"; 
?>
    
    <div class="row">
        <div class="col-lg-12" id="detail">
             <table align="center" width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
             	<!--*********************Question And Answer*****************-->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Requested Inputs by Planning(CT for Response 4hrs)</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
						<?php count_ld_queans('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_queans">
					<?php count_ld_queans($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delayed</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_queans">
						<?php count_delay_queans($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td  style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_queans">
						<?php count_ontime_queans($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                  	<a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
             	<tr style="background-color:#0CF;">
                	<td><b>Category</b></td>
                    <td></td>
                    <td><b>Avazonic</b></td>
                    <td><b>Capital Goods</b></td>
                    <td><b>Instrumentation</b></td>
                    <td><b>Non Production Consumables</b></td>
                    <td><b>Production Consumables</b></td>
                    <td><b>Security</b></td>
                    <td><b>Service</b></td>
                    <td><b>Sensors</b></td>
                    <td><b>Tools</b></td>
                    <td><b>Packing</b></td>
                    <td><b>Total</b></td>
                </tr>
                <!--**************************NOT Authorized PR'S************************-->
                <tr>
                	<td rowspan="4"  style="background-color:#0CF;"><b>Non Authorised PR'S</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prnotauth">
					<?php count_ld_prnotauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                            <?php count_delay_prnotauth('Avazonic',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                            <?php count_delay_prnotauth('Capital Goods',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Instrumentation',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Non Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                            <?php count_delay_prnotauth('Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Security',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Service'); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Sensors',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Tools',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth('Packing',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prnotauth">
                        	<?php count_delay_prnotauth($all_category,$pr_type); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Avazonic',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Capital Goods',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Instrumentation',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Non Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Security',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Service',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Sensors',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Tools',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth('Packing',$pr_type); ?>
                        </a>
                    </td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prnotauth">
                        	<?php count_ontime_prnotauth($all_category,$pr_type); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <!--************************Pending Target Allocation For Freeze Movement (No) (Pending For Date Update)******************-->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Pending Target Allocation for Freeze Movement (No)</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">	
                            <?php count_ld_prauth('Avazonic',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                            <?php count_ld_prauth('Capital Goods',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Instrumentation',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Non Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Security',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Service',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Sensors',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Tools',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth('Packing',$pr_type); ?>

                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth">
                        	<?php count_ld_prauth($all_category,$pr_type); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_delay_prauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth">
						<?php count_ontime_prauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth">
						<?php count_ontime_prauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <!--************************Disapproved Purchase Requests By Purchase******************-->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Disapproved PR By Purchase</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">	
                            <?php count_ld_prdis('Avazonic',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                            <?php count_ld_prdis('Capital Goods',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Instrumentation',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Non Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Security',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Service',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Sensors',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Tools',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis('Packing',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prdis">
                        	<?php count_ld_prdis($all_category,$pr_type); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prdis">
						<?php count_delay_prdis($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prdis">
						<?php count_ontime_prdis($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                
                <!--Pending Target Allocation For Freeze Movement (No) (Date Updated and less than current date)-->
                <tr>
                	<td rowspan="3" style="background-color:#0CF;"><b>Cases < (<?php echo $current_date; ?>)</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">	
                            <?php count_ld_currdate('Avazonic',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                            <?php count_ld_currdate('Capital Goods',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Instrumentation',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Non Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Production Consumables',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Security',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Service',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Sensors',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Tools',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate('Packing',$pr_type); ?>
                        </a>
                    </td>
                    <td style="background-color:#F90;">
                        <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_currdate">
                        	<?php count_ld_currdate($all_category,$pr_type); ?>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_currdate">
						<?php count_delay_currdate($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_currdate">
						<?php count_total_currdate($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <!----------- Current Week Start Date - End Date ------------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Current Week<br>(<?php echo $current_date; ?> - <?php echo $cwed; ?> )</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Avazonic',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Capital Goods',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Instrumentation',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Non Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Security',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Service',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Sensors',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $current_date; ?>">
						<?php count_ld_prauth_weekwise('Tools',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise('Packing',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ld_prauth_weekwise($all_category,$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                </tr>
                
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Avazonic',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Capital Goods',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Instrumentation',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Non Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Security',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Service',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Sensors',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Tools',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise('Packing',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_delay_prauth_weekwise($all_category,$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                </tr>
                
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Avazonic',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Capital Goods',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Instrumentation',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Non Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Security',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Service',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Sensors',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Tools',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise('Packing',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_ontime_prauth_weekwise($all_category,$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Avazonic',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Capital Goods',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Instrumentation',$pr_type,$cwsd,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Non Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Production Consumables',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Security',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Service',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Sensors',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Tools',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise('Packing',$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $current_date; ?>&&end_date=<?php echo $cwed; ?>">
						<?php count_total_prauth_weekwise($all_category,$pr_type,$current_date,$cwed); ?>
                    </a>
                    </td>
                </tr>
                <!----------- First Week Start Date - End Date ------------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>First Week<br>(<?php echo $fwsd; ?> - <?php echo $fwed; ?> )</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Avazonic',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Capital Goods',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Instrumentation',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Non Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Security',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Service',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Sensors',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Tools',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise('Packing',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ld_prauth_weekwise($all_category,$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Avazonic',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Capital Goods',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Instrumentation',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Non Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Security',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Service',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Sensors',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Tools',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise('Packing',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_delay_prauth_weekwise($all_category,$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Avazonic',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Capital Goods',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Instrumentation',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Non Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Security',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Service',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Sensors',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Tools',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise('Packing',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_ontime_prauth_weekwise($all_category,$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Avazonic',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Capital Goods',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Instrumentation',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Non Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Production Consumables',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Security',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Service',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Sensors',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Tools',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise('Packing',$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $fwsd; ?>&&end_date=<?php echo $fwed; ?>">
						<?php count_total_prauth_weekwise($all_category,$pr_type,$fwsd,$fwed); ?>
                    </a>
                    </td>
                </tr>
                <!---------- Second Week Start Date - End Date ------------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Second Week <br>(<?php echo $swsd; ?> - <?php echo $swed; ?> )</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Avazonic',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
					<?php count_ld_prauth_weekwise('Capital Goods',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Instrumentation',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Non Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Security',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Service',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Sensors',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Tools',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise('Packing',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ld_prauth_weekwise($all_category,$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Avazonic',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Capital Goods',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Instrumentation',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Non Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Security',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Service',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Sensors',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Tools',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise('Packing',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_delay_prauth_weekwise($all_category,$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Avazonic',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Capital Goods',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Instrumentation',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Non Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Security',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Service',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Sensors',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Tools',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise('Packing',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_ontime_prauth_weekwise($all_category,$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Avazonic',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Capital Goods',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Instrumentation',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Non Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Production Consumables',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Security',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Service',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Sensors',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Tools',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise('Packing',$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $swsd; ?>&&end_date=<?php echo $swed; ?>">
						<?php count_total_prauth_weekwise($all_category,$pr_type,$swsd,$swed); ?>
                    </a>
                    </td>
                </tr>
                <!------------- Third Week Start Date - End Date --------------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Third Week <br>(<?php echo $twsd; ?> - <?php echo $twed; ?> )</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Avazonic',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Capital Goods',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Instrumentation',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Non Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
					<?php count_ld_prauth_weekwise('Security',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Service',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Sensors',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Tools',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise('Packing',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ld_prauth_weekwise($all_category,$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Avazonic',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Capital Goods',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Instrumentation',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Non Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Security',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Service',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Sensors',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Tools',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise('Packing',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_delay_prauth_weekwise($all_category,$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Avazonic',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Capital Goods',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Instrumentation',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Non Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Security',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Service',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Sensors',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Tools',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise('Packing',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_ontime_prauth_weekwise($all_category,$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Avazonic',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Capital Goods',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Instrumentation',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Non Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Production Consumables',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Security',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Service',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Sensors',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Tools',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise('Packing',$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $twsd; ?>&&end_date=<?php echo $twed; ?>">
						<?php count_total_prauth_weekwise($all_category,$pr_type,$twsd,$twed); ?>
                    </a>
                    </td>
                </tr>
                <!-------------- Fourth Week Start Date - End Date ----------------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Fourth Week <br>(<?php echo $ftwsd; ?> - <?php echo $ftwed; ?> )</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Avazonic',$pr_type,$ftwsd,$ftwed); ?>
                    
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Capital Goods',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
					<?php count_ld_prauth_weekwise('Instrumentation',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Non Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Security',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Service',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Sensors',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Tools',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise('Packing',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise($all_category,$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Avazonic',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Capital Goods',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Instrumentation',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Non Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Security',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Service',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Sensors',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Tools',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise('Packing',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise($all_category,$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Avazonic',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Capital Goods',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Instrumentation',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Non Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Security',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Service',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Sensors',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Tools',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise('Packing',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise($all_category,$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Avazonic',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Capital Goods',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Instrumentation',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Non Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Production Consumables',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Security',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Service',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Sensors',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Tools',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise('Packing',$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo $ftwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise($all_category,$pr_type,$ftwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <!---------- Cases Greater Than Fourth Week ----------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>Cases > (<?php echo $ftwed; ?>)</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Avazonic","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Capital Goods","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Instrumentation","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Non Production Consumables","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Production Consumables","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Security","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Service","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Sensors","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Tools","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise("Packing","no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_prauth_weekwise($all_category,$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Avazonic",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Capital Goods",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Instrumentation",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Non Production Consumables",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Production Consumables",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Security",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Service",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Sensors",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Tools",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise("Packing",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_prauth_weekwise($all_category,$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Avazonic",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Capital Goods",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Instrumentation",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Non Production Consumables",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Production Consumables",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Security",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Service",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Sensors",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Tools",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise("Packing",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_prauth_weekwise($all_category,$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Avazonic",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Capital Goods",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Instrumentation",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Non Production Consumables",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Production Consumables",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Security",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Service",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Sensors",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Tools",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise("Packing",$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth_weekwise&&start_date=<?php echo "no_date"; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_prauth_weekwise($all_category,$pr_type,"no_date",$ftwed); ?>
                    </a>
                    </td>
                </tr>
                
                <!---------- AUTHORIZED PR GRAND TOTAL ----------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>GRAND TOTAL FROM PR AUTHORIZED</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
					<?php count_ld_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
             </table>
        </div>
    </div><br />
    
  </section>
</section>