<?php 
include_once('header.php');  
$this->load->helper('mrp_helper'); 

$live_cat = $_REQUEST['live_cat'];
$erp_cat = $_REQUEST['erp_cat'];
?>
<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
            	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Disapproved Purchase Requests</h4>
            </div>
        </div><br />
        
        <div class="row">
        	<div class="col-lg-12">
            <table class="table table-bordered">
                <tr style="background-color:#CCC; font-weight:bold">
                    <td>SN.</td>
                    <td>PR NUMBER</td>
                    <td>ITEM CODE</td>
                    <td>ITEM DESC</td>
                    <td>REQUIRED QUANTITY</td>
                    <td>UOM</td>
                    <td>WAREHOUSE CODE</td>
                    <td>CREATED BY</td>
                    <td>CREATED DATE</td>
                    <td>STATUS</td>
                    <td>PURCHASE DISAPPROVED RMKS</td>
                    <td>PURCHASE DISAPPROVED BY</td>
                    <td>PURCHASE DISAPPROVED DATE</td>
                </tr>
            <?php 
            if($erp_cat == 'All'){
					
				$sql="select pr_num,item_code,itm_desc,required_qty,trans_uom,warehse_code,created_by,create_date,pr_status,
				purchase_auth_by,purchase_auth_date,purchase_rmks 
				from tipldb..pr_submit_table c, scmdb..prq_preqm_pur_reqst_hdr d, 
				SCMDB..itm_ibu_itemvarhdr e, TIPLDB..erp_live_category f
				where c.pr_status = 'PR Disapproved At Purchase Level'
				and c.pr_num = d.preqm_prno
				and d.preqm_status  = 'AU'
				and c.item_code = e.ibu_itemcode
				and e.ibu_category = f.erp_cat_code
				and c.pr_num not in(
					select distinct poprq_prno from scmdb..po_poprq_poprcovg_detail a, scmdb..po_pomas_pur_order_hdr b 
					where a.poprq_poamendmentno = b.pomas_poamendmentno
					and a.poprq_pono = b.pomas_pono
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and b.pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT')
					and poprq_pocovqty = c.required_qty
				)";
				
			} else {
				
				$sql="select pr_num,item_code,itm_desc,required_qty,trans_uom,warehse_code,created_by,create_date,pr_status,
				purchase_auth_by,purchase_auth_date,purchase_rmks
				from tipldb..pr_submit_table c, scmdb..prq_preqm_pur_reqst_hdr d, 
				SCMDB..itm_ibu_itemvarhdr e, TIPLDB..erp_live_category f
				where c.pr_status = 'PR Disapproved At Purchase Level'
				and c.pr_num = d.preqm_prno
				and d.preqm_status  = 'AU'
				and c.item_code = e.ibu_itemcode
				and e.ibu_category = f.erp_cat_code
				and f.erp_cat_code in('$erp_cat')
				and f.live_category in('$live_cat')
				and c.pr_num not in(
					select distinct poprq_prno from scmdb..po_poprq_poprcovg_detail a, scmdb..po_pomas_pur_order_hdr b 
					where a.poprq_poamendmentno = b.pomas_poamendmentno
					and a.poprq_pono = b.pomas_pono
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and b.pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT')
					and poprq_pocovqty = c.required_qty
				)";
					
			}
			
			$query1=$this->db->query($sql);
            $i=1;
            foreach ($query1->result() as $row) {
               $pr_num = $row->pr_num;
			   $item_code = $row->item_code;
			   $item_code1 = urlencode($item_code);
               if(strpos($item_code1, '%2F') !== false){ 
                  $item_code2 = str_replace("%2F","chandra",$item_code1);
               } else {
                  $item_code2 = $item_code1;
               }  
			   
			   $itm_desc = $row->itm_desc;
			   $required_qty = $row->required_qty;
			   $trans_uom = $row->trans_uom;
			   $warehse_code = $row->warehse_code;
			   $created_by = $row->created_by;
			   $create_date = $row->create_date;
			   $pr_status = $row->pr_status;
			   $purchase_auth_by = $row->purchase_auth_by;
			   $purchase_auth_date = $row->purchase_auth_date;
			   $purchase_rmks = $row->purchase_rmks;
               
            ?>
                <tr>
                    <td><? echo $i;?></td>
                    <td><?php echo $pr_num; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
                            <?php echo $item_code;?>
                        </a>
                    </td>
                    <td><?php echo strip_tags(mb_convert_encoding($itm_desc, "ISO-8859-1", "UTF-8")); ?></td>
                    <td><?php echo $required_qty; ?></td>
                    <td><?php echo $trans_uom; ?></td>
                    <td><?php echo $warehse_code; ?></td>
                    <td><?php echo $created_by; ?></td>
                    <td><?php echo $create_date; ?></td>
                    <td><?php echo $pr_status; ?></td>
                    <td><?php echo $purchase_rmks; ?></td>
                    <td><?php echo $purchase_auth_by; ?></td>
                    <td><?php echo $purchase_auth_date; ?></td>
                </tr>
            <?php $i++;} ?>
            </table>
            </div>
        </div><br />
    </section>
</section>     		
<!--main content end-->
<?php include_once('footer.php'); ?>