<?php
	$emp_name = $_REQUEST['q'];
	
	$sql = "select * from tipldb..purchase_right_master where emp_name = '".$emp_name."'";
	
	$query = $this->db->query($sql);
	
	foreach($query->result() as $row) {
		$fresh_pr_creation = $row->fresh_pr_creation;
		$attach_drawing = $row->attach_drawing;
		$pr_lvl1_authorize = $row->pr_lvl1_authorize;
		$disapprove_pr_lvl1 = $row->disapprove_pr_lvl1;
		$pr_lvl2_authorize = $row->pr_lvl2_authorize;
		$disapprove_pr_lvl2 = $row->disapprove_pr_lvl2;
		$erp_authorization_pending = $row->erp_authorization_pending;
		$pr_po_nt_created = $row->pr_po_nt_created;
		
		$fresh_po = $row->fresh_po;
		$auth_po_l1 = $row->auth_po_l1;
		$dis_po_l1 = $row->dis_po_l1;
		$auth_po_l2 = $row->auth_po_l2;
		$dis_po_l2 = $row->dis_po_l2;
		$erp_auth_pend_po = $row->erp_auth_pend_po;
		$erp_amend_po = $row->erp_amend_po;
		$supp_for_purchase = $row->supp_for_purchase;
		
		$mc_planning = $row->mc_planning;
		$mc_purchase = $row->mc_purchase;
		$acknowledgement = $row->acknowledgement;
		$tc_upload = $row->tc_upload;
		$advance = $row->advance;
		$pi_upload = $row->pi_upload;
		$pi_approval = $row->pi_approval;
		$pi_payment = $row->pi_payment;
		
		$pdc_creation = $row->pdc_creation;
		$freight = $row->freight;
		$road_permit = $row->road_permit;
		$delivery_detail = $row->delivery_detail;
		$gate_entry = $row->gate_entry;
	}
	
	$sql2 = "select * from tipldb..login where name = '".$emp_name."' and emp_active = 'yes'";
	
	$query2 = $this->db->query($sql2);
	
	foreach($query2->result() as $row) {
		$emp_id = $row->emp_id;
		$emp_email = $row->email;
	}
	
	echo "<input type='hidden' name='emp_id' value='$emp_id' />";
	echo "<input type='hidden' name='emp_name' value='$emp_name' />";
	echo "<input type='hidden' name='emp_email' value='$emp_email' />";
?>

<div class="row">
    <div class="col-lg-12">	
          <h4>Purchase Request <input type="checkbox" id="pr" name="pr" value="" onclick="check_all_pr()" /></h4>
          <div class="col-lg-3">
          	<?php if($fresh_pr_creation == 'Yes'){ $check="checked"; } else { $check = "";}?>
            <input type='checkbox' name='create_pr' id='create_pr' value='' <?php echo $check; ?>>
            <b>Create PR</b>
          </div>
          <div class="col-lg-3">
          	<?php if($attach_drawing == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='attach_drawing' id='attach_drawing' value='' <?php echo $check; ?>>
            <b>Attach Drawing</b>
          </div>
          <div class="col-lg-3">
          	 <?php if($pr_lvl1_authorize == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='lvl1_approval' id='lvl1_approval' value='' <?php echo $check; ?>>
            <b>LVL1 Approval</b>
          </div>
          <div class="col-lg-3">
          	<?php if($disapprove_pr_lvl1 == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='lvl1_disapprove' id='lvl1_disapprove' value=''<?php echo $check; ?>>
            <b>LVL1 Disapproved</b>
          </div>
          <div class="col-lg-3">
          	<?php if($pr_lvl2_authorize == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='lvl2_approval' id='lvl2_approval' value='' <?php echo $check; ?>>
            <b>LVL2 Approval</b>
          </div>
          <div class="col-lg-3">
          	 <?php if($disapprove_pr_lvl2 == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='lvl2_disapprove' id='lvl2_disapprove' value='' <?php echo $check; ?>>
            <b>LVL2 Disapproved</b>
          </div>
          <div class="col-lg-3">
          	<?php if($erp_authorization_pending == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='erp_auth_pending' id='erp_auth_pending' value='' <?php echo $check; ?>>
            <b>ERP Auth Pending</b>
          </div>
          <div class="col-lg-3">
          	<?php if($pr_po_nt_created == 'Yes'){ $check="checked"; } else { $check = "";} ?>
            <input type='checkbox' name='pr_po_nt_created' id='pr_po_nt_created' value='' <?php echo $check; ?>>
            <b>Auth PR PO Not Created</b>
          </div>
    </div>
</div><br>

<div class="row">
    <div class="col-lg-12">	
          	<h4>Purchase Order <input type="checkbox" id="po" name="po" value="" onclick="check_all_po()" /></h4>
            <div class="col-lg-3">
            	<?php if($fresh_po == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="fresh_po" id="fresh_po" value="" <?php echo $check; ?>>
                <b>Fresh PO Creation</b>
            </div>
            <div class="col-lg-3">
            	<?php if($auth_po_l1 == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="auth_po_l1" id="auth_po_l1" value="" <?php echo $check; ?>>
                <b>Approve PO L1</b>
            </div>
            <div class="col-lg-3">
            	<?php if($dis_po_l1 == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="dis_po_l1" id="dis_po_l1" value="" <?php echo $check; ?>>
                <b>Disapproved PO L1</b>
            </div>
            <div class="col-lg-3">
            	<?php if($auth_po_l2 == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="auth_po_l2" id="auth_po_l2" value="" <?php echo $check; ?>>
                <b>Approve PO L2</b>
            </div>
            <div class="col-lg-3">
            	<?php if($dis_po_l2 == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="dis_po_l2" id="dis_po_l2" value="" <?php echo $check; ?>>
                <b>Disapproved PO L2</b>
            </div>
            <div class="col-lg-3">
            	<?php if($erp_auth_pend_po == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="erp_auth_pend_po" id="erp_auth_pend_po" value="" <?php echo $check; ?>>
                <b>ERP Auth. Pending PO</b>
            </div>
            <div class="col-lg-3">
            	<?php if($erp_amend_po == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="erp_amend_po" id="erp_amend_po" value="" <?php echo $check; ?>>
                <b>ERP Amend PO</b>
            </div>
            <div class="col-lg-3">
            	<?php if($supp_for_purchase == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="supp_for_purchase" id="supp_for_purchase" value="" <?php echo $check; ?>>
                <b>Supplier For Purchase</b>
            </div>
            <div class="col-lg-3">
            	<?php if($mc_planning == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="mc_planning" id="mc_planning" value="" <?php echo $check; ?>>
                <b>MC Planning</b>
            </div>
            <div class="col-lg-3">
            	<?php if($mc_purchase == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="mc_purchase" id="mc_purchase" value="" <?php echo $check; ?>>
                <b>MC Purchase</b>
            </div>
            <div class="col-lg-3">
            	<?php if($acknowledgement == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="acknowledgement" id="acknowledgement" value="" <?php echo $check; ?>>
                <b>Acknowledgement</b>
            </div>
            <div class="col-lg-3">
            	<?php if($tc_upload == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="tc_upload" id="tc_upload" value="" <?php echo $check; ?>>
                <b>TC</b>
            </div>
            <div class="col-lg-3">
            	<?php if($advance == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="advance" id="advance" value="" <?php echo $check; ?>>
                <b>Advance To Supplier</b>
            </div>
            <div class="col-lg-3">
            	<?php if($pi_upload == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="pi_upload" id="pi_upload" value="" <?php echo $check; ?>>
                <b>PI</b>
            </div>
            <div class="col-lg-3">
            	<?php if($pi_approval == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="pi_approval" id="pi_approval" value="" <?php echo $check; ?>>
                <b>PI Approval</b>
            </div>
            <div class="col-lg-3">
            	<?php if($pi_payment == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="pi_payment" id="pi_payment" value="" <?php echo $check; ?>>
                <b>PI Payment</b>
            </div>
            <div class="col-lg-3">
            	<?php if($pdc_creation == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="pdc_creation" id="pdc_creation" value="" <?php echo $check; ?>>
                <b>PDC</b>
            </div>
            <div class="col-lg-3">
            	<?php if($freight == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="freight" id="freight" value="" <?php echo $check; ?>>
                <b>Freight</b>
            </div>
            <div class="col-lg-3">
            	<?php if($road_permit == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="road_permit" id="road_permit" value="" <?php echo $check; ?>>
                <b>Road Permit</b>
            </div>
            <div class="col-lg-3">
            	<?php if($delivery_detail == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="delivery_detail" id="delivery_detail" value="" <?php echo $check; ?>>
                <b>Delivery Details</b>
            </div>
            <div class="col-lg-3">
            	<?php if($gate_entry == 'Yes'){ $check="checked"; } else { $check = "";} ?>
                <input type="checkbox" name="gate_entry" id="gate_entry" value="" <?php echo $check; ?>>
                <b>Gate Entry</b>
            </div>
    </div>
</div><br>


<!--- Submit Button --->

<div class="row">
	<div class="col-lg-2"><input type="submit" name="submit" value="SUBMIT" class="form-control"></div>
	<div class="col-lg-5"></div>
    <div class="col-lg-5"></div>
</div><br>