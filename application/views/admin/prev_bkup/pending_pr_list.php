<?php include'header.php'; ?>
<?php $this->load->helper('not_live_po_helper'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDING PR LIST</h4>
        </div>
    </div><br><br>
    
    <?php
		$fun = $_REQUEST['fun'];
		$pr_type = $_REQUEST['pr_type'];
		$category = $_REQUEST['category'];
		$live_status = $_REQUEST['live_status'];
		
		if($category == "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES','PACKING','AVAZONIC', 'NON PRODUCTION CONSUMABLES','IT','OTHERS"){
			$category1 = 'ALL';
		} else {
			$category1=$category;
		}
	?>
    
    <div class="row">
    	<div class="col-lg-5"></div>
        <div class="col-lg-1"><b style="font-size:16px">Filter:</b></div>
        <div class="col-lg-2">
        	<select id="pr_type_sort" name="pr_type_sort" class="form-control" onChange="filter(this.value,'<?php echo $fun; ?>','<?php echo $category1; ?>','<?php echo $live_status; ?>')">
            	<option value="" disabled selected>--Select--</option>
                <option value="IPR">IPR</option>
                <option value="FPR">FPR</option>
                <option value="CGP">CGPR</option>
            </select>
        </div>
        <div class="col-lg-4"></div>
    </div><br><br> 
    
    <div class="row">
    	<div class="col-lg-12" id="ajax_div">
        	<table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
            	<?php
					if($fun == 'erp_fresh_pr_det'){
						erp_fresh_pr_det($pr_type);
					} else if($fun == 'live_pending_pr_det'){
						live_pending_pr_det($pr_type,$category,$live_status);
					} else if($fun == 'pr_po_not_created_det'){
						pr_po_not_created_det($pr_type,$category);
					} else if($fun == 'total_pending_pr_det'){
						total_pending_pr_det($pr_type,$category);
					}
				?>
            </table>
        </div>
    </div> 
  </section>
</section> 

<script type="text/javascript">
function filter(pr_type_sort,fun,category,live_status){
	
	$("#ajax_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ajax_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	if(fun != 'pr_po_not_created_det' || fun != 'total_pending_pr_det') { 
		var queryString="?pr_type="+pr_type_sort+"&fun="+fun+"&category="+category+"&live_status="+live_status;	
	} else {
		var queryString="?pr_type="+pr_type_sort+"&fun="+fun+"&category="+category;
	}
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pending_poc/pending_pr_list_ajax" + queryString, true);
	
	xmlhttp.send();
	
}
</script>


      
<?php include('footer.php'); ?>