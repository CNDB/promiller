<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=export.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Excel</title>
</head>

<body>

<table class="table table-bordered" style="font-size:10px">
    <tr style="background-color:#CCC">
        <td><b>S. NO.</b></td>
        <td><b>PO NUMBER</b></td>
        <td><b>SUPPLIER NAME</b></td>
        <td><b>ITEM CODE</b></td>
        <td><b>ITEM DESC</b></td>
        <td><b>ITEM CATEGORY</b></td>
        <td><b>ITEM QTY</b></td>
        <td><b>ITEM UNIT PRICE</b></td>
        <td><b>PO TOTAL VALUE</b></td>
        <td><b>FREIGHT CHARGES</b></td>
        <td><b>TOTAL TAX VALUE</b></td>
        <td><b>EXCISE</b></td>
        <td><b>SERVICE</b></td>
        <td><b>VAT</b></td>
    </tr>
    <?php
        $s_no = 0;
        foreach($excel->result() as $row){
            $s_no++;
             $po_number        = $row->pomas_pono;
             $supplier_name    = $row->supp_spmn_supname;
             $item_code        = $row->poitm_itemcode;
             $item_desc        = $row->ml_itemvardesc;
             $category         = $row->class_desc;
             $item_quantity    = $row->poitm_order_quantity;
             $item_unit_price  = $row->poitm_po_cost;
             $po_total_value   = $row->pomas_pobasicvalue;
             $freight_charges  = $row->pomas_tcdtotalrate;
             $total_tax        = $row->pomas_tcal_total_amount;
             
             $sql_excise = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'EXCISE' and  tran_no='$po_number'";
             $sql_service = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'SER' and  tran_no='$po_number'";
             $sql_vat = "select tax_incl_amt, tax_excl_amt from scmdb..tcal_tax_hdr where  tax_type = 'VAT' and  tran_no='$po_number'";
             
             $query_excise = $this->db->query($sql_excise);
             $query_service = $this->db->query($sql_service);
             $query_vat = $this->db->query($sql_vat);
             
             foreach ($query_excise->result() as $row) {
                 $excise_tax_incl_amt    = $row->tax_incl_amt;
                 $excise_tax_excl_amt    = $row->tax_excl_amt;
                 $total_excise = $excise_tax_incl_amt - $excise_tax_excl_amt;
             }
             
             foreach ($query_service->result() as $row) {
                 $service_tax_incl_amt    = $row->tax_incl_amt;
                 $service_tax_excl_amt    = $row->tax_excl_amt;
                 $total_service = $service_tax_incl_amt - $service_tax_excl_amt;
             }
             
             foreach ($query_vat->result() as $row) {
                 $vat_tax_incl_amt    = $row->tax_incl_amt;
                 $vat_tax_excl_amt    = $row->tax_excl_amt;
                 $total_vat = $vat_tax_incl_amt - $vat_tax_excl_amt;
             }
    ?>
    <tr>
        <td><?php echo $s_no; ?></td>
        <td><?php echo $po_number; ?></td>
        <td><?php echo $supplier_name; ?></td>
        <td><?php echo $item_code; ?></td>
        <td><?php echo $item_desc; ?></td>
        <td><?php echo $category; ?></td>
        <td><?php echo number_format($item_quantity,2); ?></td>
        <td><?php echo number_format($item_unit_price,2); ?></td>
        <td><?php echo number_format($po_total_value,2); ?></td>
        <td><?php echo number_format($freight_charges,2); ?></td>
        <td><?php echo number_format($total_tax,2); ?></td>
        <td><?php echo number_format($total_excise,2); ?></td>
        <td><?php echo number_format($total_service,2); ?></td>
        <td><?php echo number_format($total_vat,2); ?></td>
    </tr>
    <?php
        }
    ?>
</table>

</body>
</html>