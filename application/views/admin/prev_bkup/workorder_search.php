<div class="row">
 <?php
    foreach ($wrkordr_ser_res->result() as $row){
	 $wono                  = $row->woh_wo_no;
	 $woso                  = $row->woh_so_no;
	 $cust_code             = $row->woh_cust_name;
	 $wo_cust_nm            = $row->clo_cust_name;
	 $wo_dw_no              = $row->DrawingNo;
	 $pm_group              = $row->pm_vertical;
	 $isProject             = $row->is_project;
	 $project_name          = $row->project_name;
	 $payment_term          = $row->payment_term;
	 $atac_no               = $row->atac_no;
	 $customer_payment_term = $row->customer_payment_term;
	 $atac_ld_date1         = $row->ld_date;
	 
	 if( $atac_ld_date1 != ''){
	 	$atac_ld_date          = date("d-m-Y", strtotime($atac_ld_date1));
	 } else {
		 $atac_ld_date = '';
	 }
	 
	 $atac_need_date1       = $row->need_date;
	 
	 if( $atac_need_date1 != ''){
	 	$atac_need_date          = date("d-m-Y", strtotime($atac_ld_date1));
	 } else {
		 $atac_need_date = '';
	 }
	 
	 echo "<input type='hidden' name='sono' value='$woso' />";
	 echo "<input type='hidden' name='customer_code' value='$cust_code' />";
 ?>
    <div class="col-lg-3">
    <b> W.O. No. </b>&nbsp;<?php echo $wono; echo "<input type='hidden' name='work_odr_no' value='$wono' />";?>
    </div>
    <div class="col-lg-3">
    <b> Customer Name </b>&nbsp;<?php echo $wo_cust_nm; echo "<input type='hidden' name='customer_name' value='$wo_cust_nm' />";?>
    </div>
    <div class="col-lg-3">
     <b>Drawing No.</b> &nbsp;<?php echo $wo_dw_no; echo "<input type='hidden' name='drawing_no' value='$wo_dw_no' />";?>
    </div>  
    <div class="col-lg-3">
     <b>PM Group</b> &nbsp;<?php echo $pm_group; echo "<input type='hidden' name='pm_group' value='$pm_group' />";?>
    </div>
</div><BR /> 

<div class="row">
    <div class="col-lg-3">
        <b>Project Name:</b>
        <?php
            if($project_name == '')
             {
                 echo "<input type='text' name='pro_ordr_name' id='pro_ordr_name' class='form-control' value='' />";
             }
            else
             {
                 echo $project_name;
                 echo "<input type='hidden' id='pro_ordr_name' name='pro_ordr_name' value='$project_name'/>";
             }
        ?>
    </div>
    <div class="col-lg-3">
        <b>Atac No: </b>
            <?php 
                echo $atac_no; 
                echo "<input type='hidden' name='atac_no' value='$atac_no' />"; 
            ?>
    </div>
    <div class="col-lg-3">
      	<b>ATAC LD Date: </b>
			<?php 
                echo $atac_ld_date; 
                echo "<input type='hidden' name='atac_ld_date' value='$atac_ld_date' />"; 
            ?>
      </div>
      <div class="col-lg-3">
      	<b>ATAC Need Date: </b>
			<?php 
                echo $atac_need_date; 
                echo "<input type='hidden' name='atac_need_date' value='$atac_need_date' />"; 
            ?>
      </div>	
  </div><br /><br />
  <div class="col-lg-12">
  	  <div class="col-lg-3">
        	<b>Payment Terms: </b>
				<?php 
					echo $payment_term; 
					echo "<input type='hidden' name='atac_payment_terms' value='$payment_term' />"; 
                ?>
        </div>
        <div class="col-lg-3">
        	<b>Customer Payment Terms: </b>
				<?php 
					echo $customer_payment_terms; 
					echo "<input type='hidden' name='customer_payment_terms' value='$customer_payment_terms' />"; 
                ?>
        </div>
      <div class="col-lg-6">
      </div>             
  </div>
<?php } ?>
