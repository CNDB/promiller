<?php
	include'header.php';
	 
	$nik = $this->uri->segment(3);                     
?>

      <!--main content start-->
      <body onLoad="check(<?php echo $nik; ?>,'from_pr');">
      <section id="main-content">
          <section class="wrapper">
            <div class="row"  style="margin-top:-10px">
				<div class="col-lg-12" style="background-color:#333333; padding:2px">
                      <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Authorized Purchase Request</h4>
				</div>
			</div><br />
            
            <div class="row">
				<div class="col-lg-12">
                      <div class="col-lg-2">
                      <b>Select PR No.</b>
                      </div>
                      <div class="col-lg-2">
                      <form action="" method="post" role="form">
                          <select name="selectpr" id="selectpr" class="form-control" onChange="check(this.value,'from_pr');" readonly>
                         
                            <?php 
								if ($nik != '') {
							?>
                            <option selected="selected" value="<?php echo $nik; ?>" disabled="disabled"><?php echo $nik; ?></option> 
                            <?php	} else {  ?>
                           <option value="">--Select--</option>
							<?php	
							foreach ($h->result() as $row)  
									 {  
									 	$prno = $row->prqit_prno; 	
							?>              
								<option  value="<?php echo $prno; ?>"><?php echo $prno; ?></option>  
							<?php } } ?>                             
                          
                          </select>
                      </form>
                 
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
                      <div class="col-lg-2">
                      
                      </div>
				</div>
			</div><br />
                      
                  <div id="detail"></div>
            
          </section>
      </section>
      <!--main content end-->
  </section>
  </body>
  <!-- container section end -->
      
	<script type="text/javascript">
		function check(str,whre_nik)
		{
		 $("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
		 
		 
		if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
		  xmlhttp=new XMLHttpRequest();
		  }
		else
		  {// code for IE6, IE5
		  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
		xmlhttp.onreadystatechange=function()
		  {
		  if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_pr')
			{
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
				$( "#datepicker1" ).datepicker();
							   
		    }
		  }
		
		
		 var queryString="?q="+str;
		 if(whre_nik == 'from_pr') {		 	
		 	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpoc_lvl1/view_pr" + queryString,true); 
		 }	    
		 xmlhttp.send();	
		}

</script>
 
<script type="text/javascript" >
	$(document).ready(function(){	
		check('<? echo $nik; ?>','from_pr');
	});
</script>
 

<?php include('footer.php'); ?>