<!--********* PO INFORMATION *********-->

<form action="<?php echo base_url(); ?>index.php/deliveryc/insert_po_sub_lvl2" method="post" enctype="multipart/form-data" onSubmit="return reqd()">

<?php include('po_details_div.php'); ?> 
            
<!--****** REMARKS & SUPPLIER QUOTES *******-->

<?php include('po_supplier_quotes.php'); ?>

<?php 
foreach ($view_po->result() as $row) { 
	$upload_ack = $row->uploaded_ack;
	$att_road_permit = $row->att_road_premit;
?>
            
<!--********** Uploaded Acknowledgement And Road Permit *********-->
            
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-3">
            <b>Uploaded Acknowledgment:</b>
        </div>
        <div class="col-lg-3">          
        	<a href="<?php echo base_url(); ?>uploads/<?php echo $upload_ack; ?>" target="_blank"><?php echo $upload_ack; ?></a>
        </div>
        <div class="col-lg-3">
            <b>Uploaded Road Permit:</b>
        </div>  
        <div class="col-lg-3">
        	<a href="<?php echo base_url(); ?>uploads/<?php echo $att_road_permit; ?>" target="_blank"><?php echo $att_road_permit; ?></a>
        </div>
    </div>
</div><br />
<?php break; } ?>

<!--********** Delivery Details *********-->
            
<div class="row">
    <div class="col-lg-12">
        <div class="col-lg-2">
            <b>Enter Docket Detail:</b><b style="color:#F00">&nbsp;*</b>  
        </div>
        <div class="col-lg-2">
            <input type="text" name="docket_detail" id="docket_detail" value="" class="form-control" />   
        </div>
        <div class="col-lg-2">
            <b>Enter Transporter:</b><b style="color:#F00">&nbsp;*</b>
        </div>
        <div class="col-lg-2">
            <input type="text" name="transporter" id="transporter" value="" class="form-control" />
        </div>
        <div class="col-lg-2">
            <b>Expected Date Of Delivery:</b><b style="color:#F00">&nbsp;*</b>
        </div>
        <div class="col-lg-2" >
            <input type="text" name="delivery_date" value="" class="form-control" id="datepicker1"/>
        </div>
    </div>
</div><br />

<div class="row">
  <div class="col-lg-4">    
  </div>
  <div class="col-lg-4"> 
  <input type="submit" name="deli_apprve" value="Submit Delivery" class="form-control" 
  style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px" /> 
  </div>
  <div class="col-lg-4">    
  </div>
</div>
          
</form> 
      		
<?php //Chat History ?>
 
<?php include('po_chat_history.php'); ?>
 
<?php //Action Timing Report ?>

<?php include('po_action_timing.php'); ?>

<?php //Footer ?>
      		
<?php include('footer.php'); ?>