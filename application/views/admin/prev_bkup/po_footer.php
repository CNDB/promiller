<?php //chat history ?>
<div class="row">
  <div class="col-lg-12">
	<h3>Chat History</h3>	
		<table class="table table-bordered" id="dataTable" border="1">
			<thead>
			  <tr>
				<th>LEVEL</th>
				<th>NAME</th>
				<th>COMMENT</th>
				<th>DATE TIME</th>
			  </tr>
			</thead>
			<tbody>
			  <?php 
				$sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
				$query1 = $this->db->query($sql1);
				if ($query1->num_rows() > 0) {
				foreach ($query1->result() as $row) {
				  $level = $row->level;
				  $name  = $row->comment_by;
				  $comment = $row->comment;
				  $datentime = $row->datentime;
								
			   ?>
			  <tr>
				<td><?php echo $level; ?></td>
				<td><?php echo $name; ?></td>
				<td><?php echo $comment; ?></td>
				<td><?php echo $datentime; ?></td>                            
			  </tr>
			  <?php 
				} } 
			  ?>
			</tbody>
	   </table>    
	</div>
 </div>
 
<?php //Action Timing Report ?>

<div class="row">
	<div class="col-lg-12">
		<h3>Action Timing</h3>    
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<table align="center" class="table table-bordered">
			<thead>
				<th>ERP Created</th>
				<th>ERP Create Date</th>
				<th>ERP Last Modified</th>
				<th>ERP Last Modified Date</th>
				<th>Live Created</th>
				<th>Live Created Date</th>
				<th>Live Authorized LVL1</th>
				<th>Live Authorized LVL1 Date</th>
				<th>Live Authorized LVL2</th>
				<th>Live Authorized LVL2 Date</th>
			</thead>
			<?php foreach($po_item_details->result() as $row){ ?>
			<tbody>
				<td><?php echo $row->pomas_createdby; ?></td>
				<td><?php echo $row->pomas_createddate; ?></td>
				<td><?php echo $row->pomas_lastmodifiedby; ?></td>
				<td><?php echo $row->pomas_lastmodifieddate; ?></td>
				<td style="text-transform:uppercase;"><?php echo $row->po_approvedby_lvl0; ?></td>
				<td><?php echo $row->po_approveddate_lvl0; ?></td>
				<td style="text-transform:uppercase;"><?php echo $row->po_approvedby_lvl1; ?></td>
				<td><?php echo $row->po_approveddate_lvl1; ?></td>
				<td style="text-transform:uppercase;"><?php echo $row->po_approvalby_lvl2; ?></td>
				<td><?php echo $row->po_approvaldate_lvl2; ?></td>
			</tbody>
			<?php break; } ?>
		</table>    
	</div>
</div>

<?php //Action Timing Report ?>
