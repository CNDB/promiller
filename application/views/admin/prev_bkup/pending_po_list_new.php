<?php include'header.php'; ?>

<?php $this->load->helper('not_live_po_helper'); ?>
 
<!--main content start-->
<!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDING PO LIST DETAILED</h4>
        </div>
    </div><br><br>
	<?php
		$fun = $_REQUEST['fun'];
		$po_stat = $_REQUEST['po_stat'];
		$po_type = $_REQUEST['po_type'];
		$category = $_REQUEST['category'];    
    ?>
    <div class="row">
    	<div class="col-lg-12" id="ajax_div">
        	<table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
            	<?php pending_po_det_new($po_stat, $po_type, $category); ?>
            </table>
        </div>
    </div> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>