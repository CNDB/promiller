<?php
	$fun = $_REQUEST['fun'];
	$pr_type = $_REQUEST['pr_type'];
	$category = $_REQUEST['category'];
	$live_status = $_REQUEST['live_status'];
	
	if($category == 'ALL'){
		$category1 = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES','PACKING','AVAZONIC', 'NON PRODUCTION CONSUMABLES','IT','OTHERS";
	} else {
		$category1 = $category;
	}
	
	//die;	
?>

<?php $this->load->helper('not_live_po_helper'); ?>


<div class="row">
    <div class="col-lg-12" id="ajax_div">
        <table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
            <?php
                if($fun == 'erp_fresh_pr_det'){
                    erp_fresh_pr_det($pr_type);
                } else if($fun == 'live_pending_pr_det'){
                    live_pending_pr_det($pr_type,$category1,$live_status);
                } else if($fun == 'pr_po_not_created_det'){
                    pr_po_not_created_det($pr_type,$category1);
                } else if($fun == 'total_pending_pr_det'){
                    total_pending_pr_det($pr_type,$category1);
                }
            ?>
        </table>
    </div>
</div> 