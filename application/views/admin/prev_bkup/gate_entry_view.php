<!--********* PO INFORMATION *********-->
<form action="<?php echo base_url(); ?>index.php/grcptc/insert_po_sub_lvl2" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?> 
            
<!--****** REMARKS & SUPPLIER QUOTES *******-->

<?php include('po_supplier_quotes.php'); ?>
            
<!--********** Uploaded Acknowledgement And Road Permit *********-->

<?php 
foreach ($view_po->result() as $row) {
	$upload_ack = $row->uploaded_ack;
	$att_road_permit = $row->att_road_premit;  
?>
            
<div class="row">
    <div class="col-lg-3"><b>Uploaded Acknowledgment:</b></div>
    <div class="col-lg-3"><a href="<?php echo base_url(); ?>uploads/<?php echo $upload_ack; ?>" target="_blank"><?php echo $upload_ack; ?></a></div>
    <div class="col-lg-3"><b>Uploaded Road Permit:</b></div>  
    <div class="col-lg-3"><a href="<?php echo base_url(); ?>uploads/<?php echo $att_road_permit; ?>" target="_blank"><?php echo $att_road_permit; ?></a></div>
</div><br /><br />
            
<div class="row">
    <div class="col-lg-2"><b>Docket Detail:</b></div>
    <div class="col-lg-2"><?php echo $row->docket_detail; ?></div>
    <div class="col-lg-2"><b>Transporter:</b></div>
    <div class="col-lg-2"><?php echo $row->transporter; ?></div>
    <div class="col-lg-2"><b>Expected Date Of Delivery:</b></div>
    <div class="col-lg-2"><?php echo $row->delivery_date; ?></div>
</div><br /><br />

<?php break; } ?> 
            
<div class="row">
    <div class="col-lg-2"><b>Enter GRCPT Number:</b></div>
    <div class="col-lg-2"><input type="text" name="grcpt_no" id="grcpt_no" value="" class="form-control"/></div>
    <div class="col-lg-2"><b>Recieved Qty</b></div>
    <div class="col-lg-2"><input type="text" name="grcpt_rec_qty" id="grcpt_rec_qty" value="" class="form-control"/></div>
    <div class="col-lg-2"><b>Recieved Date</b></div>
    <div class="col-lg-2"><input type="text" name="grcpt_rec_date" id="grcpt_rec_date" value="" class="form-control"/></div>
</div><br /><br />
            
<div class="row">
  <div class="col-lg-4"></div>
  <div class="col-lg-4"> 
      <input type="submit" name="grcpt_apprve" value="Submit GRCPT" class="form-control" 
      style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px" /> 
  </div>
  <div class="col-lg-4"></div>
</div>          
</form>

<?php //Chat History ?>
 
<?php include('po_chat_history.php'); ?>
 
<?php //Action Timing Report ?>

<?php include('po_action_timing.php'); ?>

<?php //Footer ?>
      		
<?php include('footer.php'); ?>