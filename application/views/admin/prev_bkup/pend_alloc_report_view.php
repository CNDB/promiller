<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">AUTHORIZED PR WITH NO PENDING ALLOCATION</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-4">
              <a href="<?php echo base_url(); ?>index.php/pend_alloc_reportc/pend_alloc_export" 
              style="font-size:16px; font-style:italic; font-weight:bold" target="_blank">
              	Export To Excel
              </a>  
        </div>
        <div class="col-lg-8"></div>
    </div><br>
    
    <div class="row">
    	<div class="col-lg-12">
                <table cellspacing="1" cellpadding="1" cellspacing="4" align="center" border="0" class="table table-bordered" style="font-size:11px">
                    <tr style="background-color:#0CF; font-weight:bold">
                    	<td>SNO</td>
                        <td>PR NUMBER</td>
                        <td>ITEM CODE</td>
                        <td>ITEM DESC</td>
                        <td>REQ. QTY</td>
                        <td>CATEGORY</td>
                        <td>PEND. ALLOC.</td>
                        <td>USAGE</td>
                        <td>PR REMARKS</td>
                        <td>ATAC NO</td>
                        <td>SO NO.</td>
                        <td>CUSTOMER NAME</td>
                        <td>PR STATUS</td> 
                    </tr>
                    <?php
					$sno = 0;
					foreach($pend_alloc->result() as $row){ 
					$sno++;
					$pr_num = $row->pr_num;
					//ITEM CODE ENCODING
					$item_code = $row->item_code;
					$item_code1 = urlencode($item_code);
					if(strpos($item_code1, '%2F') !== false)
					{
						$item_code2 = str_replace("%2F","chandra",$item_code1);
					}
					else 
					{
						$item_code2 = $item_code1;
					}
					//ITEM CODE ENCODING
					//ITEM DESCRIPTION
					$item_desc1 = $row->itm_desc;
					$item_desc2 = str_replace("'","",$item_desc1);
					$item_desc = mb_convert_encoding($item_desc2, "ISO-8859-1", "UTF-8");
					//ITEM DESCRIPTION
					
					$required_qty = $row->required_qty;
					$category = $row->category;
					$pend_alloc = $row->pend_alloc;
					$usage = $row->usage;
					$remarks = $row->remarks;
					$sono = $row->sono;
					$pr_status = $row->pr_status;
					
					//CUSTOMER NAME
					$atac_no = $row->atac_no;
					
					$sql="select acc_name from tipldb..pr_submit_table a, tipldb..master_atac b where a.atac_no = b.atac_no and a.atac_no = '$atac_no'";
					$qry = $this->db->query($sql);
					if($qry->num_rows() > 0){
						foreach($qry->result() as $row){
							$customer_name = $row->acc_name;
						}
					} else {
						$customer_name = "";
					}
					?>
                    <tr>
                    	<td><?php echo $sno; ?></td>
                        <td><a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo $pr_num; ?>"><?php echo $pr_num; ?></a></td>
                        <td>
                            <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>">
                            	<?php echo $item_code; ?>
                            </a>
                        </td>
                        <td><?php echo $item_desc; ?></td>
                        <td><?php echo number_format($required_qty,2); ?></td>
                        <td><?php echo $category; ?></td>
                        <td><?php echo $pend_alloc; ?></td>
                        <td><?php echo $usage; ?></td>
                        <td><?php echo $remarks; ?></td>
                        <td>
                            <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                            	<?php echo $atac_no; ?>
                            </a>
                        </td>
                        <td>
                            <a href="http://live.tipl.com/tipl_project1/so_wo_linkage/so_tree.php?so_no=<?php echo $row->sono; ?>">
                                <?php echo $sono; ?>
                            </a>
                        </td>
                        <td><?php echo $customer_name; ?></td>
                        <td><?php echo $pr_status; ?></td> 
                    </tr>
                    <? } ?>
                </table>
        </div>
    </div><br>
    
  </section>
</section>
