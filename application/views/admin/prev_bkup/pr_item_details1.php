<!--********* ITEM INFORMATION **********-->
<div class="row">
        <div class="col-lg-4">
        	<h4>PR No. <?php echo $pr_num; ?></h4>
        </div>
        <div class="col-lg-2">
        	
        </div>
        <div class="col-lg-6">
        	<table border="1" align="center" class="table table-bordered" style="font-size:9px">
                <tr>
                    <td><b>CONDITION</b></td>
                    <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                    <td>FIRST TIME PURCHASE</td>
                </tr>
                <tr>
                	<td><b>COLOR</b></td>
                    <td style="background-color:red;"></td>
                    <td style="background-color:green;"></td>
                    <td style="background-color:blue;"></td>
                    <td style="background-color:yellow;"></td>
                </tr>
            </table>
        </div>
</div><br />

<!-- PR ERP And LIVE Status Starts-->
<?php

//$pr_num = $_REQUEST['pr_number'];

$sql_check ="select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'";

$query_check = $this->db->query($sql_check);	

$sql_pr_live = "select * from tipldb..pr_submit_table where pr_num = '$pr_num'";

$qry_pr_live = $this->db->query($sql_pr_live);

foreach($qry_pr_live->result() as $row){
	$pr_status = $row->pr_status;
}

foreach($query_check->result() as $row){
	$pr_erp_status = $row->preqm_status;
	
	if($pr_erp_status == 'DR'){
		$pr_erp_status1 = 'DRAFT';
	} else if($pr_erp_status == 'HD'){
		$pr_erp_status1 = 'HOLD';
	} else if($pr_erp_status == 'FR'){
		$pr_erp_status1 = 'FRESH';
	} else if($pr_erp_status == 'CA'){
		$pr_erp_status1 = 'CANCELLED';
	} else if($pr_erp_status == 'DE'){
		$pr_erp_status1 = 'DELETED';
	} else if($pr_erp_status == 'AU'){
		$pr_erp_status1 = 'AUTHORIZED';
	} else {
		$pr_erp_status1 = $pr_erp_status1;
	}
}
?>

<div class="row">
	<div class="col-lg-6">
    	<h4 style="text-align:left;">PR LIVE STATUS - <?php echo $pr_status; ?></h4>
    </div>
    <div class="col-lg-6">
    	<h4 style="text-align:right;">PR ERP STATUS - <?php echo $pr_erp_status1; ?></h4>
    </div>
</div>

<!-- PR ERP And LIVE Status Ends -->

<div class="row" style="text-align:center">
	<h3>ITEM DETAILS</h3>
</div>

<div class="row">
    <div class="col-lg-12" style=" overflow-x:auto;">
     <table class="table table-bordered" id="dataTable" border="1">
        <thead>
          <tr>
            <th>Item Code</th>
            <th>Item Description</th>
            <th>Last Yr Cons.</th>
            <th>Current Stock</th>
            <th>Total Incoming Stock</th>
            <th>Reservation QTY</th>
            <th>PR QTY</th>
            <th>Last Price</th>
            <th>Transanction UOM</th>
            <th>Conversion Factor With Stock UOM</th>
            <th>Supplier Avg. Lead Time</th>
            <th>Need Date</th>
            <th>Warehouse Code</th>
            <th>Cost Available</th>
            <th>Cost</th>
            <th>Currency</th>
            <th>Remarks (On Calculating Cost)</th>
            <th>Costing Not Available Remarks</th>
          </tr>
        </thead>
        <tbody>
         <?php 
		 		$sql_pr_det = "select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..pr_submit_table c, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = c.pr_num  and a.prqit_prno = '$pr_num' 
		 and a.prqit_itemcode = d.iou_itemcode";
		 
				$qry_pr_det = $this->db->query($sql_pr_det);
		 		$count = 0;
				foreach ($qry_pr_det->result() as $row){
					
					$count = $count+1; 
					$pr_date = substr($row->preqm_prdate,0, 11);
					$pr_num = $row->prqit_prno;
					$item_code = $row->prqit_itemcode;
					$item_code1 = urlencode($item_code);
					
					if(strpos($item_code1, '%2F') !== false)
					{
						$item_code2 = str_replace("%2F","chandra",$item_code1);
					}
					else 
					{
						$item_code2 = $item_code1;
					}
					
					$req_qty = $row->prqit_reqdqty;
					$uom = $row->prqit_puom;
					$need_date = substr($row->prqit_needdate,0,11);
					//$need_date = date("d-m-Y", strtotime($need_date1));
					$wh_code = $row->prqit_warehousecode;

					$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
					$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
					$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
					//Last Rate
					$sql5 ="select top 1 poitm_po_cost,* from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b 
					where poitm_itemcode = '$item_code' and a.pomas_pono = b.poitm_pono and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE')
					and a.pomas_poamendmentno = b.poitm_poamendmentno 
					and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr WHERE pomas_pono = a.pomas_pono 
					AND pomas_poou = a.pomas_poou) and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO')
					order by pomas_poauthdate desc";
					
					$sql6 ="select isnull(ml_itemVardesc ,'')+'  '+isnull(lov_matlspecification,' ') AS itm_desc,* from 
					scmdb..itm_ml_multilanguage,                                  
					scmdb..itm_lov_varianthdr,                                  
					scmdb..itm_iou_itemvarhdr,                                  
					scmdb..itm_ibu_itemvarhdr,
					scmdb..itm_loi_itemhdr                                  
					where ml_langid  = 1                                  
					and  ml_itemcode  = lov_itemcode                                   
					and  ml_variantCode = lov_variantcode                    
					AND  ml_itemcode  = iou_itemcode                                                       
					AND  ml_variantCode = iou_variantcode                                   
					AND  ml_itemcode  = ibu_itemcode                                                       
					AND  ml_variantCode = ibu_variantcode
					AND  ml_itemcode  = loi_itemcode                                     
					AND  ml_itemcode  = '$item_code'";
					
					$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
					$sql8 ="select * from tipldb..pr_submit_table where pr_num = '$pr_num'";
					$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
					
					$query2 = $this->db->query($sql2);
					$query3 = $this->db->query($sql3);
					$query4 = $this->db->query($sql4);
					$query5 = $this->db->query($sql5);
					$query6 = $this->db->query($sql6);
					$query7 = $this->db->query($sql7);
					$query8 = $this->db->query($sql8);
					$query_draw = $this->db->query($sql_draw);
						
					if ($query2->num_rows() > 0) {
					  foreach ($query2->result() as $row) {
						  $lastyr_cons = $row->ConsTotalQty;
						}
					} else {
						  $lastyr_cons = 0;
					}
					
					if ($query3->num_rows() > 0) {
						  $current_stk = 0;
					  foreach ($query3->result() as $row) {
						  $current_stock = $row->ItemStkAccepted;
						  $current_stk = $current_stk + $current_stock;
						}
					} else {
						  $current_stk = 0;
					}
					
					if ($query4->num_rows() > 0) {
					  foreach ($query4->result() as $row) {
						  $reservation_qty = $row->AllocPendingTot;
						}
					} else {
						  $reservation_qty = 0.00;
					}
					
					if ($query5->num_rows() > 0) {
					  foreach ($query5->result() as $row) {
						  $last_price = $row->poitm_po_cost;
						}
					} else {
						  $last_price = 0.00;
					}
					
					if ($query6->num_rows() > 0) {
					  foreach ($query6->result() as $row) {
						  $item_desc1 = $row->itm_desc;
						  $item_desc = str_replace("'","",$item_desc1);
						}
					} else {
						  $item_desc1 = "";
						  $item_desc = "";
					}
					
					if ($query7->num_rows() > 0) {
					  $incoming_qty_tot1 = 0;
					  foreach ($query7->result() as $row) {
						  $incoming_qty = $row->PendPoSchQty;
						  $incoming_qty_tot = $incoming_qty_tot + $incoming_qty;
						}
					} else {
						  $incoming_qty_tot = "";
					}
					
					if ($query_draw->num_rows() > 0) {
					  foreach ($query_draw->result() as $row) {
						  $drawing_no = $row->DrawingNo;
						  $purchase_uom  = $row->ItemPurcaseUom;
						  $manufact_uom = $row->ItemMnfgUom;
						}
					} else {
						  $drawing_no = "";
						  $purchase_uom  = "";
						  $manufact_uom = "";
					}
					
					if ($query8->num_rows() > 0) {
					  foreach ($query8->result() as $row) {
						  $cost_available = $row->cost_available;
						  $costing = $row->costing;
						  $currency = $row->currency;
						  $cost_calculation_remarks = $row->cost_calculation_remarks;
						  $cost_not_available_remarks = $row->cost_not_available_remarks;
						}
					} else {
						  $cost_available = "";
						  $costing = "";
						  $cost_calculation_remarks = "";
						  $cost_not_available_remarks = "";
					}
					
					$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";
			
					$query = $this->db->query($sql);
					
					if ($query->num_rows() > 0) {
					  foreach ($query->result() as $row) {
						  $ucon_confact_ntr = $row->ucon_confact_ntr;
						  $ucon_confact_dtr = $row->ucon_confact_dtr;
						  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
					  }
					} else {
						   $conversion_factor = "";  
					}
					
					//Supplier Average Lead Time
					
					$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
					$query_supplier = $this->db->query($sql_supplier);
					
					if (count($query_supplier) > 0) {
						$supplier_lead_time_tot = 0;
						$counter = 0;
					  foreach ($query_supplier->result() as $row) {
						  $counter++;
						  $supplier_lead_time = $row->SuppLeadTime;
						  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
					  }
					  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
					} else {
						   $counter = 0;
						   $supplier_lead_time = "";
						   $supplier_lead_time_tot = ""; 
						   $supplier_avg_lead_time = 0;  
					}
					
					$username1 = $_SESSION['username'];
                
         ?>
          
          <tr>
                <?php
				   echo "<input type='hidden' name='pr_num' value='$pr_num' />";
                   echo "<input type='hidden' name='pr_date' value='$pr_date' />";
				   echo "<input type='hidden' name='drawing_no' value='$drawing_no' />";
				   echo "<input type='hidden' name='username' value='$username1' />"
                ?>
            <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
					<?php echo $item_code; echo "<input type='hidden' name='item_code' value='$item_code' />";?>
                </a>
            </td>
            <td>
                <?php 
                    echo mb_convert_encoding($item_desc1, "ISO-8859-1", "UTF-8"); 
                    echo "<input type='hidden' name='itm_desc' value='$item_desc' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($lastyr_cons,2); 
                    echo "<input type='hidden' name='lastyr_cons' value='$lastyr_cons' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($current_stk,2); 
                    echo "<input type='hidden' name='current_stk' value='$current_stk' />";		  
                ?>
            </td>
            <td>
                <?php
                   echo number_format($incoming_qty_tot,2);
                   echo "<input type='hidden' name='incoming_qty_tot' value='$incoming_qty_tot' />";
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($reservation_qty,2); 
                    echo "<input type='hidden' name='reservation_qty' value='$reservation_qty' />";		  
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($req_qty,2); 
                    echo "<input type='hidden' name='required_qty' value='$req_qty' />";
                ?>
            </td>
            <td>
                <?php 
                    echo number_format($last_price,2); 
                    echo "<input type='hidden' name='last_price' value='$last_price' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $uom; 
                    echo "<input type='hidden' name='trans_uom' value='$uom' />";
                ?>
            </td> 
            <td>
                <?php 
                    echo $conversion_factor; 
                    echo "<input type='hidden' name='conversion_factor' value='$conversion_factor' />";
                ?>
            </td> 
            <td>
                <?php 
                    echo number_format($supplier_avg_lead_time,2); 
                    echo "<input type='hidden' name='supplier_avg_lead_time' value='$supplier_avg_lead_time' />";
                ?>
            </td>
            <?php
				$sql_supp_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
				$query_supp_dtl = $this->db->query($sql_supp_dtl);
				
				 foreach ($query_supp_dtl->result() as $row) {
					$diffdays    = $row->diffDays;
				 }
				
				if($diffdays != NULL){
					if($diffdays > 0){
						$color = "red";
					} else if($diffdays < 0){
						$color = "green";
					} else if($diffdays = 0){
						$color = "blue";
					}
				} else {
					$color = "yellow";
				}
				//echo $diffdays;
			?>
            <td style=" background-color:<?php echo $color; ?>;">
                <?php
                    echo $need_date;
                    echo "<input type='hidden' name='need_date' value='$need_date' />";
					echo "<input type='hidden' name='color' value='$color' />";
                ?>
            </td>
            <td>
                <?php 
                    echo $wh_code; 
                ?>
            </td>
            <td>
                <?php
					echo $cost_available;
					echo "<input type='hidden' name='cost_available' value='$cost_available' />";
				?>
            </td>
            <td>
                <?php
					echo $costing;
					echo "<input type='hidden' name='costing' value='$costing' />";
				?>
            </td>
            <td>
                <?php
					echo $currency;
					echo "<input type='hidden' name='currency' value='$currency' />";
				?>
            </td>
            <td>
                <?php
					echo $cost_calculation_remarks;
					echo "<input type='hidden' name='cost_calculation_remarks' value='$cost_calculation_remarks' />";
				?>
            </td>
            <td>
                <?php
					echo $cost_not_available_remarks;
					echo "<input type='hidden' name='cost_not_available_remarks' value='$cost_not_available_remarks' />";
				?>
            </td>                            
          </tr>
           <?php }  ?> 
        </tbody>
     </table>
  </div>
</div><br /><br />