<?php 
include'header.php'; 
$po_num = $this->uri->segment(3);
$nik = $this->uri->segment(3);
$entry_no = $this->uri->segment(4);
?>
<!--main content start-->
<body>
    <section id="main-content">
        <section class="wrapper">
            <div class="row"  style="margin-top:-10px">
                <div class="col-lg-12" style="background-color:#333333; padding:2px">
                	<h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">
                    	Without MC & DI Disapproved PO'S
                    </h4>
                </div>
            </div><br>
            
             <?php include('po_header.php'); ?>
    
            <form action="<?php echo base_url(); ?>index.php/mcdi_approvalc/mcdi_disapproved_submit" method="post">
            
            <input type="hidden" id="entry_no" name="entry_no" value="<?php echo $entry_no; ?>">
            
            <?php include('po_details_div.php'); ?>
            
            <?php include('po_supplier_quotes.php'); ?><br>
            
            <div class="row">
            	<div class="col-lg-3"><b>Reason / Remarks:</b></div>
                <div class="col-lg-3">
                	<textarea id="dis_rmks" name="dis_rmks" class="form-control"></textarea>
                </div>
                <?php
					$username = $_SESSION['username'];
					
					foreach($view_po->result() as $row){
						$created_by = $row->created_by;
						break;
					}
					
					if($username == $created_by  || $username = 'admin'){
				?>
                <div class="col-lg-3"><input type="submit" id="approval" name="approval" value="SEND FOR APPROVAL" class="form-control"></div>
                <div class="col-lg-3"></div>
                <?php
					} else {
						echo "<div class='col-lg-6'></div></div>";
						echo "
						  <div class='row'>
							<div class='col-lg-12' style='text-align:center; color:red'>
								<h3>You Are Not Authorized To Send This PO For Approval...</h3>
							</div>";
				 	} 
				?>
            </div>
            
            </form>
            
            <?php include('po_chat_history.php'); ?>
            
            <?php include('po_action_timing.php'); ?>
            
        </section>
    </section>
</body>

<?php include('footer.php'); ?>