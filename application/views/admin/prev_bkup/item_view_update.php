 <?php include'header.php'; ?>
 <!--main content start-->
 <!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEM DETAILS</h4>
        </div>
    </div><br />
    
    <div class="row">
      <div class="col-lg-12">
       	<table class="table table-bordered">
        	<tr style="background-color:#CCC">
            	<td><b>S.No.</b></td>
                <td><b>Item Code</b></td>
                <td><b>Item Desc</b></td>
                <td><b>Current Stock(Accepted)</b></td>
                <td><b>Reorder Level</b></td>
                <td><b>Reorder Qty</b></td>
                <td><b>Last Yr Cons.</b></td>
                <td><b>Curr Yr Cons.</b></td>
                <td><b>Lead Time</b></td>
                <td><b>Unit Rate</b></td>
                <td><b>Source</b></td>
                <td><b>Priority</b></td>
            </tr>
            <?php 
			$s_no = 0;
			foreach ($h->result() as $row){
				$s_no++;
				$item_code = $row->item_code;
				$item_desc = $row->item_desc;
				$priority = $row->priority;
				$item_code1 = urlencode($item_code);
					
				if(strpos($item_code1, '%2F') !== false)
				{
					$item_code2 = str_replace("%2F","chandra",$item_code1);
				}
				else 
				{
					$item_code2 = $item_code1;
				}
				
				$sql_reorder ="select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
				$query_reorder = $this->db->query($sql_reorder);
				
				if ($query_reorder->num_rows() > 0) {
				  foreach ($query_reorder->result() as $row) {
					  $reorder_lvl = $row->iou_reorderlevel;
					  $reorder_qty = $row->iou_reorderqty;
					}
				} else {
					  $reorder_lvl = "";
					  $reorder_qty = "";
				}
				
				$sql_current_stk ="select sum(sbn_quantity) as sbn_quantity_tot from scmdb..skm_stockbal_nonctrl where sbn_item_code='$item_code' 
and sbn_stock_status='ACC'";
				$query_current_stk = $this->db->query($sql_current_stk);
				
				if ($query_current_stk->num_rows() > 0) {
				  foreach ($query_current_stk->result() as $row) {
					  $current_stk = $row->sbn_quantity_tot;
					}
				} else {
					  $current_stk = "";
				}
				
				$sql_source ="select iou_mfg_source, iou_purchase_source, iou_subcntract_source, iou_others_usage from scmdb..itm_iou_itemvarhdr where iou_itemcode='$item_code'";
				$query_source = $this->db->query($sql_source);
				
			  foreach ($query_source->result() as $row) {
				  $manufacturing_source = $row->iou_mfg_source;
				  $purchase_source = $row->iou_purchase_source;
				  $subcontract_source = $row->iou_subcntract_source;
				  $others_source = $row->iou_others_usage;
				}
				
				if($manufacturing_source == 1){
					$source = ',manufacturing';
				} else {
					$source = '';
				}
				
				if($purchase_source == 1){
					$source .= ',purchase';
				} else {
					$source .= '';
				}
				
				if($subcontract_source == 1){
					$source .= ',subcontract';
				} else {
					$source .= '';
				}
				
				if($others_source == 1){
					$source .= ',others';
				} else {
					$source .= '';
				}
				
//Current Year Consumption
				
$sql_prod_return = "select SUM(srpd_stock_uom_qty) as tot_prod_return                          
from scmdb..str_production_hdr a, scmdb..str_production_det b                          
WHERE B.srpd_item_code  = '$item_code'                         
and  srph_ouinstid = srpd_ouinstid                           
and  srph_return_no = srpd_return_no                            
AND  srph_status  = 'AU'                          
and  srph_return_date between '2017-04-01' and '2018-03-31'";

$query_prod_return = $this->db->query($sql_prod_return);

if ($query_prod_return->num_rows() > 0) {
  foreach ($query_prod_return->result() as $row) {
	  $tot_prod_return = $row->tot_prod_return;
	}
} else {
	  $tot_prod_return = "";
}

$sql_production = "select SUM(ipd_issue_qty) as tot_production
from scmdb..issue_prd_detail a, scmdb..issue_prd_header b
where iph_ouinstid = ipd_ouinstid   
and ipd_item_code = '$item_code'
and  iph_ouinstid = ipd_ouinstid  
and  iph_issue_no = ipd_issue_no  
and  iph_status  = 'AU'  
and iph_issue_date between '2017-04-01' and '2018-03-31'";

$query_production = $this->db->query($sql_production);

if ($query_production->num_rows() > 0) {
  foreach ($query_production->result() as $row) {
	  $tot_production = $row->tot_production;
	}
} else {
	  $tot_production = "";
}

$sql_sales = "select SUM(isld_issue_qty) as tot_sales                     
from scmdb..issue_sal_header a, scmdb..issue_sal_detail b                         
WHERE B.isld_item_code = '$item_code'                            
and  islh_ouinstid = isld_ouinstid                          
and  islh_issue_no = isld_issue_no                          
and  islh_status  = 'AU'                            
and islh_issue_date between '2017-04-01' and '2018-03-31'";

$query_sales = $this->db->query($sql_sales);

if ($query_sales->num_rows() > 0) {
  foreach ($query_sales->result() as $row) {
	  $tot_sales = $row->tot_sales;
	}
} else {
	  $tot_sales = "";
}

$sql_stk_transfer = "select sum(istd_issue_qty) as tot_stk_transfer       
from scmdb..issue_skt_header a, scmdb..issue_skt_detail b	                         
WHERE istd_item_code  = '$item_code'                               
and  isth_ouinstid = istd_ouinstid                          
and  isth_issue_no = istd_issue_no                          
and  isth_status  = 'AU'                            
and isth_issue_date between '2017-04-01' and '2018-03-31'";

$query_stk_transfer = $this->db->query($sql_stk_transfer);

if ($query_stk_transfer->num_rows() > 0) {
  foreach ($query_stk_transfer->result() as $row) {
	  $tot_stk_transfer = $row->tot_stk_transfer;
	}
} else {
	  $tot_stk_transfer = "";
}

$sql_subcontracting = "select sum(iscd_issue_qty) as tot_subcon                           
from scmdb..issue_sub_header a, scmdb..issue_sub_detail b                          
WHERE iscd_item_code  = '$item_code'                               
and  isch_ouinstid = iscd_ouinstid                          
and  isch_issue_no = iscd_issue_no                          
and  isch_status  = 'AU'                            
and isch_issue_date between '2017-04-01' and '2018-03-31'";

$query_subcontracting = $this->db->query($sql_subcontracting);

if ($query_subcontracting->num_rows() > 0) {
  foreach ($query_subcontracting->result() as $row) {
	  $tot_subcon = $row->tot_subcon;
	}
} else {
	  $tot_subcon = "";
}

$sql_maintainence = "select sum(imd_issue_qty) as tot_maintanance                          
from scmdb..issue_mnt_header a, scmdb..issue_mnt_detail b                         
WHERE imd_item_code  = '$item_code'                               
and  imh_ouinstid = imd_ouinstid                          
and  imh_issue_no = imd_issue_no                          
and  imh_status  = 'AU'                             
and imh_issue_date between '2017-04-01' and '2018-03-31'";

$query_maintainence = $this->db->query($sql_maintainence);

if ($query_maintainence->num_rows() > 0) {
  foreach ($query_maintainence->result() as $row) {
	  $tot_maintanance = $row->tot_maintanance;
	}
} else {
	  $tot_maintanance = "";
}

$sql_inventory = "select sum(iid_issue_qty)  as tot_inventory            
from scmdb..issue_inv_header, scmdb..issue_inv_detail                         
WHERE iid_item_code  = '$item_code'                               
and  iih_ouinstid = iid_ouinstid         
and  iih_issue_no = iid_issue_no                          
and  iih_status  = 'AU'                             
and iih_issue_date between '2017-04-01' and '2018-03-31'";

$query_inventory = $this->db->query($sql_inventory);

if ($query_inventory->num_rows() > 0) {
  foreach ($query_inventory->result() as $row) {
	  $tot_inventory = $row->tot_inventory;
	}
} else {
	  $tot_inventory = "";
}

$sql_unplanned = "select sum(iud_issue_qty) as tot_unplanned                          
from scmdb..issue_unp_header, scmdb..issue_unp_detail                   
WHERE iud_item_code  = '$item_code'                               
and  iuh_ouinstid = iud_ouinstid                          
and  iuh_issue_no = iud_issue_no                          
and iuh_status  = 'AU'                              
and iuh_issue_date between '2017-04-01' and '2018-03-31'";

$query_unplanned = $this->db->query($sql_unplanned);

if ($query_unplanned->num_rows() > 0) {
  foreach ($query_unplanned->result() as $row) {
	  $tot_unplanned = $row->tot_unplanned;
	}
} else {
	  $tot_unplanned = "";
}

$curr_yr_cons = ($tot_production+$tot_sales+$tot_stk_transfer+$tot_subcon+$tot_maintanance+$tot_inventory+$tot_unplanned)-$tot_prod_return;

//echo $curr_yr_cons;


//current year consumption end

//Last Year Consumption start
				
$sql_prod_return = "select SUM(srpd_stock_uom_qty) as tot_prod_return                          
from scmdb..str_production_hdr a, scmdb..str_production_det b                          
WHERE B.srpd_item_code  = '$item_code'                         
and  srph_ouinstid = srpd_ouinstid                           
and  srph_return_no = srpd_return_no                            
AND  srph_status  = 'AU'                          
and  srph_return_date between '2016-04-01' and '2017-03-31'";

$query_prod_return = $this->db->query($sql_prod_return);

if ($query_prod_return->num_rows() > 0) {
  foreach ($query_prod_return->result() as $row) {
	  $tot_prod_return = $row->tot_prod_return;
	}
} else {
	  $tot_prod_return = "";
}

$sql_production = "select SUM(ipd_issue_qty) as tot_production
from scmdb..issue_prd_detail a, scmdb..issue_prd_header b
where iph_ouinstid = ipd_ouinstid   
and ipd_item_code = '$item_code'
and  iph_ouinstid = ipd_ouinstid  
and  iph_issue_no = ipd_issue_no  
and  iph_status  = 'AU'  
and iph_issue_date between '2016-04-01' and '2017-03-31'";

$query_production = $this->db->query($sql_production);

if ($query_production->num_rows() > 0) {
  foreach ($query_production->result() as $row) {
	  $tot_production = $row->tot_production;
	}
} else {
	  $tot_production = "";
}

$sql_sales = "select SUM(isld_issue_qty) as tot_sales                     
from scmdb..issue_sal_header a, scmdb..issue_sal_detail b                         
WHERE B.isld_item_code = '$item_code'                            
and  islh_ouinstid = isld_ouinstid                          
and  islh_issue_no = isld_issue_no                          
and  islh_status  = 'AU'                            
and islh_issue_date between '2016-04-01' and '2017-03-31'";

$query_sales = $this->db->query($sql_sales);

if ($query_sales->num_rows() > 0) {
  foreach ($query_sales->result() as $row) {
	  $tot_sales = $row->tot_sales;
	}
} else {
	  $tot_sales = "";
}

$sql_stk_transfer = "select sum(istd_issue_qty) as tot_stk_transfer       
from scmdb..issue_skt_header a, scmdb..issue_skt_detail b	                         
WHERE istd_item_code  = '$item_code'                               
and  isth_ouinstid = istd_ouinstid                          
and  isth_issue_no = istd_issue_no                          
and  isth_status  = 'AU'                            
and isth_issue_date between '2016-04-01' and '2017-03-31'";

$query_stk_transfer = $this->db->query($sql_stk_transfer);

if ($query_stk_transfer->num_rows() > 0) {
  foreach ($query_stk_transfer->result() as $row) {
	  $tot_stk_transfer = $row->tot_stk_transfer;
	}
} else {
	  $tot_stk_transfer = "";
}

$sql_subcontracting = "select sum(iscd_issue_qty) as tot_subcon                           
from scmdb..issue_sub_header a, scmdb..issue_sub_detail b                          
WHERE iscd_item_code  = '$item_code'                               
and  isch_ouinstid = iscd_ouinstid                          
and  isch_issue_no = iscd_issue_no                          
and  isch_status  = 'AU'                            
and isch_issue_date between '2016-04-01' and '2017-03-31'";

$query_subcontracting = $this->db->query($sql_subcontracting);

if ($query_subcontracting->num_rows() > 0) {
  foreach ($query_subcontracting->result() as $row) {
	  $tot_subcon = $row->tot_subcon;
	}
} else {
	  $tot_subcon = "";
}

$sql_maintainence = "select sum(imd_issue_qty) as tot_maintanance                          
from scmdb..issue_mnt_header a, scmdb..issue_mnt_detail b                         
WHERE imd_item_code  = '$item_code'                               
and  imh_ouinstid = imd_ouinstid                          
and  imh_issue_no = imd_issue_no                          
and  imh_status  = 'AU'                             
and imh_issue_date between '2016-04-01' and '2017-03-31'";

$query_maintainence = $this->db->query($sql_maintainence);

if ($query_maintainence->num_rows() > 0) {
  foreach ($query_maintainence->result() as $row) {
	  $tot_maintanance = $row->tot_maintanance;
	}
} else {
	  $tot_maintanance = "";
}

$sql_inventory = "select sum(iid_issue_qty)  as tot_inventory            
from scmdb..issue_inv_header, scmdb..issue_inv_detail                         
WHERE iid_item_code  = '$item_code'                               
and  iih_ouinstid = iid_ouinstid         
and  iih_issue_no = iid_issue_no                          
and  iih_status  = 'AU'                             
and iih_issue_date between '2016-04-01' and '2017-03-31'";

$query_inventory = $this->db->query($sql_inventory);

if ($query_inventory->num_rows() > 0) {
  foreach ($query_inventory->result() as $row) {
	  $tot_inventory = $row->tot_inventory;
	}
} else {
	  $tot_inventory = "";
}

$sql_unplanned = "select sum(iud_issue_qty) as tot_unplanned                          
from scmdb..issue_unp_header, scmdb..issue_unp_detail                   
WHERE iud_item_code  = '$item_code'                               
and  iuh_ouinstid = iud_ouinstid                          
and  iuh_issue_no = iud_issue_no                          
and iuh_status  = 'AU'                              
and iuh_issue_date between '2016-04-01' and '2017-03-31'";

$query_unplanned = $this->db->query($sql_unplanned);

if ($query_unplanned->num_rows() > 0) {
  foreach ($query_unplanned->result() as $row) {
	  $tot_unplanned = $row->tot_unplanned;
	}
} else {
	  $tot_unplanned = "";
}

$last_yr_cons = ($tot_production+$tot_sales+$tot_stk_transfer+$tot_subcon+$tot_maintanance+$tot_inventory+$tot_unplanned)-$tot_prod_return;

//Last Year Consumption end

//supplier rate and lead time

$sql_supplier1 = "SELECT TOP 5 RECEIPTdT,gr_hdr_suppcode                           
 FROM                           
 (select  gr_hdr_suppcode,max(rcgh_receipt_date) RECEIPTdT                          
 from   scmdb..gr_hdr_grmain (nolock)                          
    inner join                           
    scmdb..rct_purchase_hdr (nolock)                          
 On   gr_hdr_ouinstid  = rcgh_ouinstid                           
 and   gr_hdr_grno   = rcgh_ref_doc_no                          
    inner join                           
    scmdb..rct_purchase_det (nolock)                          
 On   rcgh_ouinstid  = rcgd_ouinstid                          
 and   rcgh_receipt_no  = rcgd_receipt_no                           
 where  gr_hdr_grstatus  = 'FM'                          
 and  rcgh_status  = 'AU'                          
 and   rcgd_item_code  = '$item_code'                        
 group by gr_hdr_suppcode)T                          
 ORDER BY RECEIPTdT";
 
 $query_supplier1 = $this->db->query($sql_supplier1);
 
 $count = 0;
 
 $total_rate = 0;
 $total_lead_time = 0;
 foreach ($query_supplier1->result() as $row) {
	 
	 $count ++;
	 $supp_code = $row->gr_hdr_suppcode;
	 $recipt_date = $row->RECEIPTdT;
	 
	 $sql_supplier2 = "select  rcgh_receipt_date,P.gr_hdr_grno,gr_hdr_suppcode,supp_spmn_supname,                          
    RCGD_RECEIPT_RATE, DATEDIFF(DD,GR_HDR_ORDERDATE,rcgh_receipt_date) as lead_time                         
 from   scmdb..gr_hdr_grmain P                          
    inner join                           
    scmdb..rct_purchase_hdr Q                         
 On   P.gr_hdr_ouinstid  = Q.rcgh_ouinstid                           
 and   P.gr_hdr_grno   = Q.rcgh_ref_doc_no                          
    inner join                           
    scmdb..rct_purchase_det R                          
 On   Q.rcgh_ouinstid  = R.rcgd_ouinstid                          
 and   Q.rcgh_receipt_no  = R.rcgd_receipt_no                          
    INNER JOIN                           
    scmdb..supp_spmn_suplmain                             
 ON   supp_spmn_supcode = gr_hdr_suppcode                         
 and   rcgh_receipt_date = '$recipt_date'                          
 and   gr_hdr_suppcode = '$supp_code'                          
 where  P.gr_hdr_grstatus  = 'FM'                          
 and   Q.rcgh_status   = 'AU'                          
 and   R.rcgd_item_code  = '$item_code'";
 
 $query_supplier2 = $this->db->query($sql_supplier2);
 	
	//$total_rate = 0;
	//$total_lead_time = 0;
	$count1 = 0;
	foreach ($query_supplier2->result() as $row) {
	  $rate = $row->RCGD_RECEIPT_RATE;
	  
	  $lead_time = $row->lead_time;
	  
	  $total_rate = $total_rate+$rate;
	  
	  $total_lead_time = $total_lead_time+$lead_time;
	  
	  $count1++;
	} 
 }
 
 $lead_time_avg =  $total_lead_time/$count;
 $average_rate =  $total_rate/$count;
 
//supplier rate and lead time
				
?>
            <tr>
            	<td><?php echo $s_no; ?></td>
                <td>
                <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank"><?php echo $item_code; ?></a>
                </td>
                <td><?php echo $item_desc; ?></td>
                <td><?php echo number_format($current_stk,2); ?></td>
                <td><?php echo number_format($reorder_lvl,2); ?></td>
                <td><?php echo number_format($reorder_qty,2); ?></td>
                <td><?php echo number_format($last_yr_cons,2); ?></td>
                <td><?php echo number_format($curr_yr_cons,2); ?></td>
                <td><?php echo number_format($lead_time_avg,2); ?></td>
                <td><?php echo number_format($average_rate,2); ?></td>
                <td><?php echo $source; ?></td> 
                <td><?php echo $priority; ?></td>
            </tr>
            
            <?php

			$sql_update = "update tipldb..item_details_report set reorder_lvl = '$reorder_lvl', 
			reorder_qty = '$reorder_qty', current_stk = '$current_stk', last_yr_cons = '$last_yr_cons', current_yr = '$curr_yr_cons', 
			lead_time = '$lead_time_avg', unit_rate = '$average_rate', source = '$source' where item_code = '$item_code'";
			
			$this->db->query($sql_update);
			
			?>
            
            <?php } ?>
        </table>
      </div>
    </div><br />
    

<?php //Action Timing Report ?> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->
      
<?php include('footer.php'); ?>