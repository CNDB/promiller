<?php include'header.php'; ?>
<?php $this->load->helper('not_live_po_helper'); ?>
<!--********* ITEM INFORMATION *********-->
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDING PO LIST</h4>
        </div>
    </div><br><br>
	<?php
        $fun = $_REQUEST['fun'];
        $po_stat = $_REQUEST['po_stat'];
        $po_type = $_REQUEST['po_type'];
        $category = $_REQUEST['category'];
    ?>
	<div class="row">
    	<div class="col-lg-5"></div>
        <div class="col-lg-1"><b style="font-size:16px">Filter:</b></div>
        <div class="col-lg-2">
        	<select id="pr_type_sort" name="pr_type_sort" class="form-control" onChange="filter(this.value,'<?php echo $fun; ?>','<?php echo $category; ?>','<?php echo rawurlencode($po_stat); ?>')">
            	<option value="" disabled selected>--Select--</option>
                <option value="FPO">FPO</option>
                <option value="IPO">IPO</option>
                <option value="LPO">LPO</option>
                <option value="CGP">CGPO</option>
                <!--<option value="SRV">SRVPO</option>-->
            </select>
        </div>
        <div class="col-lg-4"></div>
    </div><br><br> 
    
    <div class="row">
    	<div class="col-lg-12" id="ajax_div">
        	<table width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
            	<?php pending_po_det($po_stat,$po_type,$category); ?>
            </table>
        </div>
    </div> 
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->

<script type="text/javascript">
function filter(po_type,fun,category,po_stat){
	
	$("#ajax_div").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	//Ajax Function
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('ajax_div').innerHTML=xmlhttp.responseText;
		}
	 }
	 
	var queryString="?po_type="+po_type+"&fun="+fun+"&category="+category+"&po_stat="+po_stat;
	
	//alert(queryString);
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/pending_poc/pending_po_list_ajax" + queryString, true);
	
	xmlhttp.send();
	
}
</script>
      
<?php include('footer.php'); ?>