<?php
	include'header.php';
	$po_from_screen = $this->uri->segment(4);                    
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Create Purchase Order In Live</h4>
        </div>
    </div><br>
    <div class="row">
        <div class="col-lg-6">
        	<h4>PO No. &nbsp;&nbsp;<?php echo $po_num = $this->uri->segment(3);  ?></h4>
        </div>
        <div class="col-lg-6">
            <table border="1" align="center" class="table table-bordered" style="font-size:9px">
                <tr>
                    <td><b>CONDITION</b></td>
                    <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                    <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                    <td>FIRST TIME PURCHASE</td>
                </tr>
                <tr>
                    <td><b>COLOR</b></td>
                    <td style="background-color:red;"></td>
                    <td style="background-color:green;"></td>
                    <td style="background-color:blue;"></td>
                    <td style="background-color:yellow;"></td>
                </tr>
            </table>
        </div>
    </div><br>
    
    <!---- Amend PO --->
    
    <?php
	
	if($po_from_screen == 'fresh') {	
		$po_stat = "Fresh PO";	
	} else if($po_from_screen == 'disapprove_lvl1'){
		$po_stat = "Disapprove PO LVL1";
	} else if($po_from_screen == 'disapprove_lvl2'){
		$po_stat = "Disapprove PO LVL2";
	} else if($po_from_screen == 'amend_po'){
		$po_stat = "Amended PO";
	}
	
	?>
    
    <div class="row">
        <div class="col-lg-12">
               <h3><?php echo $po_stat; ?></h3>
         </div>
    </div><br>
    
    <?php $po_first_three = substr($po_num,0,3); ?>
    
    <?php
		$sql_drft = "select status, payment_fpo, delivery_fpo from tipldb..po_master_table where po_num = '$po_num'";
		$qry_drft = $this->db->query($sql_drft);
		
		foreach($qry_drft->result() as $row){
			$status = $row->status;
			$payment_fpo = $row->payment_fpo;
			$delivery_fpo = $row->delivery_fpo;
		}
    ?>
    
    <?php
		if($status == 'Draft' || $po_from_screen == 'amend_po' ||$po_from_screen == 'disapprove_lvl1' || $po_from_screen == 'disapprove_lvl2'){
	?>
    <div class="row">
        <div class="col-lg-2">
			<?php if($po_first_three == 'FPO'){ ?>
                   <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst_foreign/<?php echo $po_num; ?>" 
                   target="_blank" style="font-size:16px; font-weight:bold;">
                   <button type="button" class="form-control">PRINT PO</button></a>
            <?php } else { ?>
                    <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst/<?php echo $po_num; ?>" 
                   target="_blank" style="font-size:16px; font-weight:bold;">
                   <button type="button" class="form-control">PRINT PO</button></a>
            <?php } ?>
         </div>
    </div><br>
    <?php } ?>
    <!--- PO Details --->
    
<?php

function ms_escape_string($data) {
	if ( !isset($data) or empty($data) ) return '';
	if ( is_numeric($data) ) return $data;

	$non_displayables = array(
		'/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
		'/%1[0-9a-f]/',             // url encoded 16-31
		'/[\x00-\x08]/',            // 00-08
		'/\x0b/',                   // 11
		'/\x0c/',                   // 12
		'/[\x0e-\x1f]/'             // 14-31
	);
	foreach ($non_displayables as $regex )
	$data = preg_replace( $regex, '', $data );
	$data = str_replace("'", "''", $data );
	return $data;
}

?> 
   
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>PO DETAILS</h3>
    </div>
</div>
<!--******* PO INFORMATION ******-->
<form action="<?php echo base_url(); ?>index.php/createpodc/insert_po_sub" method="post" enctype="multipart/form-data" onsubmit="return reqd()">
<?php foreach ($view_po->result() as $row){
	$po_number         = $row->poitm_pono;
	$supp_name         = $row->supp_spmn_supname;
	$address1          = $row->supp_addr_address1;
	$address2          = $row->supp_addr_address2;
	$address3          = $row->supp_addr_address3;
	$city              = $row->supp_addr_city;
	$state             = $row->supp_addr_state;
	$country           = $row->supp_addr_country;
	$zip               = $row->supp_addr_zip;
	$supplier_email    = $row->supp_addr_email;
	$supplier_phone_no = $row->supp_addr_phone;
	$contact_person    = $row->supp_addr_contperson;
	$comp_add          = $address1.$address2."<br>".$address3.$city." (".$state.") ".$country." ".$zip;
	$po_total_value    = $row->pomas_pobasicvalue;
	$cst               = $row->pomas_tcdtotalrate;
	$tax               = $row->pomas_tcal_total_amount;
	$grand_total_po    = $po_total_value+$cst+$tax;
	$carrier_name      = $row->paytm_carrier;
	$freight           = $row->paytm_incoterm;
	$ipr_age           = $row->diff1;
	$po_erp_created_by = $row->pomas_createdby;
	$po_erp_created_date = $row->pomas_createddate;
	$po_amend_no       = $row->pomas_poamendmentno;
	$po_age            = $row->diff;
	$insurance_liablity= $row->paytm_insuranceliability;
	$transport_mode    = $row->paytm_transmode;
	$pomas_poamendmentno = $row->pomas_poamendmentno;	
	$freight_place     = $row->paytm_incoplace; 
	$payterm           = $row->paytm_payterm;
	$po_three_letters  = substr($po_number,0,3);
	
	$sql_payterm_desc = "select pt_description from scmdb..pt_payterm_hdr_vw where pt_paytermcode = '$payterm' 
	and pt_version_no = (select max(pt_version_no) from scmdb..pt_payterm_hdr_vw where pt_paytermcode = '$payterm')";

	$payterm_desc_qry = $this->db->query($sql_payterm_desc);
	
	foreach ($payterm_desc_qry->result() as $row) {
		
		$payterm_desc      = $row->pt_description;
		
	}
	
	echo "<input type='hidden' name='payterm_desc' value='$payterm_desc'>";
	
	if($freight == "FOR"){
		$freight1 = $freight.",&nbsp;Ajmer";
	} else if($freight == "FORD"){
		$freight1 = $freight.",&nbsp;TIPL Ajmer";
	} else{
		$freight1 = $freight."&nbsp;";
	}
?>

<?php if($po_three_letters != 'FPO'){ ?>

<div class="row">
    <div class="col-lg-2">
        <b>Supplier Name:</b><br>
        <?php 
            echo $supp_name;
            echo "<input type='hidden' name='po_supp_name' value='$supp_name' />"; 
        ?>   	
    </div>
    <div class="col-lg-2">
        <b>Supplier Address: </b><br>
        <?php 
            echo $comp_add;
            echo "<input type='hidden' name='po_supp_add' value='$comp_add' />"; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Email Address: </b><b style="color:#F00">&nbsp;*</b><br>
        <?php
			echo $supplier_email;
            echo "<input type='hidden' name='supplier_email' id='supplier_email' value='$supplier_email' />";
        ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Phone Number: </b><b style="color:#F00">&nbsp;*</b><br>
        <?php
			echo $supplier_phone_no;
			echo "<input type='hidden' name='supplier_phone_no' id='supplier_phone_no' value='$supplier_phone_no' />";
        ?>
    </div>
    <div class="col-lg-2">
        <b>Contact Person:</b><b style="color:#F00">&nbsp;*</b><br>
        <?php
			echo $contact_person;
            echo "<input type='hidden' name='contact_person' id='contact_person' value='$contact_person' />"; 
        ?>
    </div>
    <div class="col-lg-2" style="background-color:#FF0">
        <b>Order Value <br>(Including Taxes & TCD): </b><br>
        <?php 
            echo number_format($grand_total_po,2);
            echo "<input type='hidden' name='po_total_value' id='po_total_value' value='$grand_total_po' />";
			echo "<input type='hidden' name='tcd_tot' id='po_total_value' value='$cst' />";
			echo "<input type='hidden' name='tax_tot' id='po_total_value' value='$tax' />";
        ?>
        <br /><br />
        <b>Total TCD: </b><?= number_format($cst,2,".",""); ?><br />
        <b>Total TAXES: </b><?= number_format($tax,2,".",""); ?><br />
    </div>
</div><br><br>

<?php
if($supp_name == 'SUNDRY SUPPLIER'){
?>
<div class="row">
    <div class="col-lg-2">
        <b>Sundry Supplier Name:</b><br>
        <?php echo "<input type='text' id='sund_supp_name' name='sund_supp_name' value='' class='form-control'>"; ?>   	
    </div>
    <div class="col-lg-2">
        <b>Sundry Supplier Address: </b><br>
        <?php echo "<input type='text' id='sund_supp_addr' name='sund_supp_addr' value='' class='form-control'>"; ?>
    </div>
    <div class="col-lg-2">
        <b>Sundry Supplier Email: </b><br>
        <?php echo "<input type='text' id='sund_supp_email' name='sund_supp_email' value='' class='form-control'>"; ?>
    </div>
    <div class="col-lg-2">
        <b>Sundry Supplier Phone: </b><br>
        <?php echo "<input type='text' id='sund_supp_phone' name='sund_supp_phone' value='' class='form-control'>"; ?>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>
</div><br><br>
<?php } ?>

<div class="row">
	<div class="col-lg-2">
        <b>PO Age: </b><br>
        <?php 
            echo $po_age."&nbsp;Days"; 
        ?>
    </div>
	<div class="col-lg-2">
        <b>IPR Age: </b><br>
        <?php 
            echo $ipr_age."&nbsp;Days"; 
        ?>
    </div>
    <div class="col-lg-2" style="background-color:#FF0">
        <b>Payment Terms :</b><br>
        <?php 
            echo $payterm; 
            echo "<input type='hidden' name='payterm' value='$payterm' />";
			echo "<input type='hidden' name='payterm_desc' value='$payterm_desc' />"; 
        ?>  	
    </div>
    <div class="col-lg-2" style="background-color:#FF0">
        <b>Freight Terms:</b><br>
        <?php
            echo $freight1;
            echo "<input type='hidden' name='freight' value='$freight' />";
        ?>
    </div>
    <div class="col-lg-2" style="background-color:#FF0">
        <b>Freight Place : </b><br>
        <?php
            echo $freight_place;
            echo "<input type='hidden' name='freight_place' value='$freight_place' />"; 
        ?>
    </div>
    <div class="col-lg-2">
        <b>Carrier Name : </b><br>
        <?php 
		  	echo $carrier_name;
			echo "<input type='hidden' name='carrier_name' value='$carrier_name' />"; 
	    ?>
    </div>
</div><br><br>
<div class="row">
	<div class="col-lg-2" style="background-color:#FF0">
        <b>Insurance Term : </b><br>
        <?php
            echo $insurance_liablity;
            echo "<input type='hidden' name='insurance_liablity' value='$insurance_liablity' />"; 
        ?>
    </div>
	<div class="col-lg-2" style="background-color:#FF0">
    	<b>Mode Of Transport :</b><br>
		<?php echo $transport_mode; ?>
    </div>
    <div class="col-lg-2">
    	<b>Freight Type</b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2"> 
    <select class='form-control' id='freight_type' name='freight_type' onChange='freight_cond(this.value);'>
		<?php
        if($freight == 'EX WORKS' || $freight == 'EXW'){
				         
            echo " <option value=''>-- Select --</option>
                   <option value='To Pay'>To Pay</option>
                   <option value='Paid Billed'>Paid Billed</option>
                   <option value='Hand Delivery'>Hand Delivery</option>";
				   
        } else if($freight == 'FOR'){
				
            echo "<option value='Prepaid By Supplier'>Prepaid By Supplier</option>";
				
        } else if($freight == 'FORD'){
			
            echo "<option value='Prepaid By Supplier'>Prepaid By Supplier</option>";
			
        }else{
			
            echo "<option value='select'>-- Select --</option>";
			
        }
        ?>
    </select> 
    </div>
    <div class="col-lg-2">
    	<b>Enter Expected Material Recipt Date</b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2">          
    	<input type="text" name="po_lead_time" id="datepicker1" value="" class="form-control" autocomplete="off"/>
    </div>
</div><br>
<div class="row">
	<div class="col-lg-2">

    	<b>Select Currency</b>
    </div>  
    <div class="col-lg-2">
		<?php
			if($po_three_letters == 'FPO'){
				echo"<select name='currency' id='currency' class='form-control'>
						<option value=''>Select</option>
						<option value='INR'>INR</option>
						<option value='USD'>USD</option>
						<option value='GBP'>GBP</option>
						<option value='YEN'>YEN</option>
						<option value='EURO'>EURO</option>
						<option value='CNY'>CNY</option>
					</select>";
			} else {
			   echo "<input type='text' name='currency' id='currency' value='INR' readonly='readonly' class='form-control' />";
			}
        ?>
    </div>
    <div class="col-lg-2">
        <b>Delivery Type : </b><b style="color:#F00">&nbsp;*</b>
    </div>
    <div class="col-lg-2">
        <select class="form-control" name="po_deli_type" id="po_deli_type">
         <?php
            if($freight == 'EX WORKS' || $freight == 'EXW'){	
                echo "<option value=''>--Select--</option>";
            } else if($freight == 'FOR') {	
                echo "<option value='Godown Delivery'>Godown Delivery</option>";
            } else if ($freight == 'FORD'){	
                echo "<option value='Door Delivery'>Door Delivery</option>";
            } else {
                echo "<option value='select'>--Select--</option>";
            }
         ?>
         </select>
    </div>
    <div class="col-lg-2">
    	<b>Enter Approx Freight : </b><b style="color:#F00">&nbsp;*</b>
    </div>  
    <div class="col-lg-2">
		<?php 
        if($freight == 'EX WORKS' || $freight == 'EXW'){         
            echo "<input type='text' name='approx_freight' id='approx_freight' value='' class='form-control' onkeypress='return isNumber(event)'>";
        }else{
            echo "<input type='hidden' name='approx_freight' id='approx_freight' value='No Value' class='form-control'>";
        }
        ?>
    </div>
</div><br>
<div class="row">
	<div class="col-lg-2">
    	<b>LD Applicable : </b><b style="color:#F00">&nbsp;*</b>
    </div>
    <div class="col-lg-2">
    	<select name="ld_applicable" id="ld_applicable" class="form-control">
        	<option value="" disabled="disabled">--select--</option>
            <option value="No">No</option>
            <option value="Yes">Yes</option>
        </select>
    </div>
	<div class="col-lg-2">
    	<b>Enter Special Instructions For Supplier : </b>
    </div>
    <div class="col-lg-2">
    	<input type="text" name="spcl_inst_supp" id="spcl_inst_supp" value=""  class="form-control"/>
    </div> 
    <div class="col-lg-2">
    	<b>Freight Terms Reason:</b><b style="color:#F00">&nbsp;*</b>
    </div> 
    <div class="col-lg-2">
		<?php
			if($freight == 'EXW'){
				echo "<textarea id='freight_rmks' name='freight_rmks' class='form-control'></textarea>";
			} else {
				echo "<input type='hidden' id='freight_rmks' name='freight_rmks' value='No Remarks'>";
			}
        ?>
    </div>
</div><br>

<?php } else { ?>

<?php include('fpo_details_entry.php'); ?>

<?php } ?>

<!--- Amendment NO Purchase Order --->
<div class="row">
	<div class="col-lg-2" style="font-size:16px;"><b>PO Revision No :</b></div>
    <div class="col-lg-2" style="font-size:16px;"><?= $pomas_poamendmentno; ?></div>
    <div class="col-lg-2"><b>Enter Expected Lead Time(Supplier):</b><b style="color:red; font-size:18px">*</b></div>
    <div class="col-lg-2">
        <!--<input type="text" id="supp_lead_time" name="supp_lead_time" value="" class="form-control" required>-->
        <select id="supp_lead_time" name="supp_lead_time" class="form-control" required>
        	<option value="" selected="selected" disabled="disabled">--Select--</option>
            <?php
				$sql_week = "select * from tipldb..pr_week_master order by sno";
				$qry_week = $this->db->query($sql_week);
				foreach($qry_week->result() as $row){
					$weeks = $row->weeks;
			?>
            <option value="<?=$weeks; ?>"><?=$weeks; ?></option>
            <?php } ?>
        </select>
    </div>
    <div class="col-lg-2"><b>Enter Lead Time Remarks:</b></div>
    <div class="col-lg-2"><textarea id="remarks" name="remarks" class="form-control"></textarea></div>
</div><br>

<input type="hidden" name="po_first_three" id="po_first_three" value="<?php echo $po_three_letters; ?>">

<!--- Service Module --->
<?php if($po_three_letters == "SRV"){ ?>

<div class="row">
	<div class="col-lg-2"><b>Service PO Type</b></div>
    <div class="col-lg-2">
        <select class="form-control" name="spo_type" id="spo_type" onchange="spo_type_fun(this.value)">
        	<option value="" disabled="disabled" selected="selected">--Select--</option>
            <option value="Internal Service">Internal Service</option>
            <option value="Against Order">Against Order</option>
        </select>
    </div>
    <div class="col-lg-2"><b>Category</b></div>
    <div class="col-lg-2">
    	<select class="form-control" name="spo_category" id="spo_category">
        	<option value="" disabled="disabled" selected="selected">--Select--</option>
            <option value="AVAZONIC">AVAZONIC</option>
            <option value="SECURITY">SECURITY</option>
            <option value="INSTRUMENTATION">INSTRUMENTATION</option>
            <option value="SENSORS">SENSORS</option>
            <option value="IT">IT</option>
            <option value="PRODUCTION CONSUMABLES">PRODUCTION CONSUMABLES</option>
            <option value="BUILDING MAINTENANCE">BUILDING MAINTENANCE</option>
            <option value="OTHERS">OTHERS</option>
        </select>
    </div>
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>
</div><br>

<div class="row">
	<div class="col-lg-2"><b id="against_order_type_label" style="display:none;">Against Order Type</b></div>
    <div class="col-lg-2">
        <div id="spo_against_order_type_selection">
            <select name="spo_usage_type" id="spo_usage_type" class="form-control" onchange="spo_order_type(this.value)" style="display:none">
                <option value="" disabled="disabled" selected="selected">--Select--</option>
                <option value="Project">Project</option>
                <option value="Special">Special</option>
            </select>
        </div>
    </div>
    
    <div class="col-lg-2"><b id="atac_label" style="display:none;">Select ATAC No.</b></div>
    <div class="col-lg-2">
        <div id="atac_selection" style="display:none;">
            <select name="spo_atac_no" id="spo_atac_no" class="form-control" onchange="atac_details_div(this.value)" style="width:150px;">
                <option value="" disabled="disabled" selected="selected">--Select--</option>
				<?php
                    $sql_atac_no ="select a.atac_no from tipldb..main_master_atac a, tipldb..atac_project_execution b , tipldb..master_atac f
                    where a.deleted=0 
                    and a.atac_no = b.atac_no
                    and a.atac_no = f.atac_no
                    and f.status not in ('Deleted from CRM')
                    and b.install_comm in('Included','chargeable')
                    and a.order_type in ('P','R')
                    union
                    select g.atac_no from tipldb..main_master_atac e , tipldb..master_atac g
                    where e.order_type in ('A','I','O','S')
                    and e.atac_no = g.atac_no
                    and g.status not in ('Deleted from CRM')";
                
                    $query_atac_no = $this->db->query($sql_atac_no);
                    
                    $counter = 0;
                    if ($query_atac_no->num_rows() > 0) {
                        $counter++;
                      foreach ($query_atac_no->result() as $row) {
                          $atac_no = $row->atac_no;
                ?> 
                <option value="<?php echo $atac_no; ?>"><?php echo $atac_no; ?></option>
            	<?php }} ?>
           </select>
        </div>
    </div>
    <div class="col-lg-2" ><b id="special_label" style="display:none;">Why Special Remarks</b></div>
    <div class="col-lg-2"><input type="text" class="form-control" id="spo_special_remarks" name="spo_special_remarks" value="" style="display:none;"></div>
</div><br>

<div id="atac_details_div" style="display:none;">
	<div id="detail" style="border:solid 1px #000; padding:2px; border-radius:2px;"></div>
</div><br>

<div class="row" id="amc_div">
	<div class="col-lg-12">
    	<h3>Define Payment Milestones</h3>
    	<table class="table table-bordered" id="amc_schedule">
        	<tr>
            	<td><b>SNO</b></td>
                <td><b>Description</b></td>
                <td><b>Target Date</b></td>
                <td><b>Percentage Of PO Value</b></td>
                <td><b>Amount</b></td>
                <td><b>Payment Term</b></td>
                <td><b>Payment Method</b></td>
                <td><b>PDC Days</b></td>
                <td><img src="<?php echo base_url('assets/admin/img/');?>plus.png" width="20px" height="20px" onclick="return addrow()"></td>
            </tr>
            <tr>
            	<td>1</td>
                <td><input type="text" name="description[]" id="description1" value="" class="form-control"></td>
                <td><input type="text" name="date[]" id="date1" value="" class="form-control" autocomplete="off"></td>
                <td><input type="text" name="perct_po_value[]" id="perct_po_value1" class="form-control" onkeyup="cal_amount(this.value,'1')" onkeypress="return isNumber(event)" autocomplete="off">
                </td>
                <td><input type="text" name="amount[]" id="amount1" value="" class="form-control" readonly></td>
                <td>
                	<select id="spo_payterm1" name="spo_payterm[]" class="form-control">
                    	<option value="" disabled="disabled">--Select--</option>
                        <option value="PI">PI</option>
                        <option value="PDC">PDC</option>
                    </select>
                </td>
                <td>
                	<select id="paymode1" name="paymode[]" class="form-control" onchange="paymode_type()">
                    	<option value="" disabled="disabled">--Select--</option>
                        <option value="Bank Transfer">Bank Transfer</option>
                        <option value="PDC">PDC</option>
                        <option value="CDC">CDC</option>
                    </select>
                </td>
                <td>
                    <input type="text" name="pdc_days[]" id="pdc_days1" value="" class="form-control" onkeypress="return isNumber(event)" 
                    autocomplete="off" style="display:block;">
                </td>
                <td></td>
            </tr>
        </table>
    </div>
</div>
<?php } ?>
<!---Service Module---->

<?php break;} ?>

<!--******** ITEM INFORMATION ********-->

<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>ITEM DETAILS</h3>
    </div>
</div>
<div class="row" style="overflow-x:auto;">

<table class="table table-bordered" id="dataTable" border="1">
    <thead>
      <tr>
        <th>IPR No.</th> 
        <th>Item Code, Description & UOM</th>  
        <?php if($po_three_letters == 'FPO'){ ?>
        <th>Supplier Item Code</th> 
        <?php } ?> 
        <th>PR Qty</th>
        <th>Item PO Quantity</th>
        <th>Costing</th>
        <th>Last Price</th>
        <th>Current Price</th>
        <th>IF(Current Price > Costing) OR (Current Price > Last Price) Reason</th>
        <th>Charges</th>
        <th>Discount</th>  
        <th>Total Item Value</th>
        <th>PR Need Date</th> 
        <th>PO Need Date</th>
        <th>Need Date Mismatch Remarks</th> 
        <th>PR Type & Special Remarks, Category</th> 
        <th>SO & ATAC Details</th> 
        <th>PR Remarks (Planning & Purchase)</th> 
        <th>Supplier Remarks (Planning & Purchase)</th>
        <th>MC &amp; DI</th>
        <th>Warehouse Code</th>
        <th>Last Supplier Details</th>
        <th>Attach Cost Sheet</th>
      </tr>
    </thead>
    <tbody>
<?php
   $arr = array();
   $arr_hsn = array();
   $arr_pm_grp = array();
   $arr_usage_type = array();
   
   $i=0;
   //echo $view_po->num_rows();
   foreach ($view_po->result() as $row){   
		$i++;
		$po_line_no        = $row->poitm_polineno;
		$item_desc1        = $row->ml_itemvardesc;
		$item_desc2        = $row->lov_matlspecification;
		$item_desc         = $item_desc1.$item_desc2;
		$item_desc_new1    = str_replace("'","",$item_desc);
		$item_desc_new     = mb_convert_encoding($item_desc_new1, "ISO-8859-1", "UTF-8");
		$po_num            = $row->poitm_pono;
		$supp_code         = $row->supp_spmn_supcode;
		$supp_name         = $row->supp_spmn_supname;
		$po_ipr_no         = $row->poprq_prno;
		$item_code         = $row->poitm_itemcode;
		$po_date           = $row->pomas_podate;
		$po_need_date      = $row->poitm_needdate;
		
		$pr_num_arr = array($po_ipr_no);
		
		$item_code1 = urlencode($item_code);
				
		if(strpos($item_code1, '%2F') !== false){	
			$item_code2 = str_replace("%2F","chandra",$item_code1);
		}else{
			$item_code2 = $item_code1;
		}
		
		$carrier_name      = $row->paytm_carrier;
		$freight           = $row->paytm_incoterm; 
		$payterm           = $row->paytm_payterm;
		$po_quantity       = $row->poitm_order_quantity;
		$for_stk_qty       = $row->poitm_order_quantity;
		$for_stk_qty1      = number_format($for_stk_qty,2);
		$uom               = $row->poitm_puom;
		$current_price     = $row->poitm_po_cost;
		$item_value        = $row->poitm_itemvalue; 
		$po_total_value    = $row->pomas_pobasicvalue;
		$po_date           = $row->pomas_podate;
		$cost_pr_unt       = $row->poitm_costper;
		$need_date         = $row->poitm_needdate;
		$wh_code           = $row->poitm_warehousecode;
		$odr_qty           = $row->poitm_order_quantity;
		$totalcost         = $odr_qty * $cost_pr_unt;
		$address1          = $row->supp_addr_address1;
		$address2          = $row->supp_addr_address2;
		$address3          = $row->supp_addr_address3;
		$city              = $row->supp_addr_city;
		$state             = $row->supp_addr_state;
		$country           = $row->supp_addr_country;
		$zip               = $row->supp_addr_zip;
		$comp_add          = $address1.$address2."<br>".$address3.$city." (".$state.") ".$country." ".$zip;
		$supp_email        = $row->supp_addr_email;
		$currency          = $row->pomas_pocurrency;
		//new pdf elements
		$po_amend_no 	   = $row->pomas_poamendmentno;
		$po_type 		   = $row->pomas_potype;
		$supp_phone        = $row->supp_addr_phone;
		$contact_person    = $row->supp_addr_contperson;
		$pay_mode          = $row->paytm_paymode;
		$trans_mode        = $row->paytm_transmode;
		$partial_ship      = $row->paytm_shippartial;
			
		$sql1 ="exec tipldb..pendalcard '$item_code'";
		$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
		$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
		$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";

		$sql5 ="select top 1 poitm_po_cost, gr_lin_fadate, gr_lin_fmdate, gr_lin_frdate, gr_hdr_grdate, gr_lin_linestatus,
		* from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, 
		scmdb..gr_hdr_grmain c, scmdb..gr_lin_details d where 
		b.poitm_itemcode = '$item_code'
		and c.gr_hdr_grno = d.gr_lin_grno
		and b.poitm_itemcode = d.gr_lin_itemcode
		and a.pomas_pono = c.gr_hdr_orderno
		and c.gr_hdr_orderdoc = 'PO'
		and c.gr_hdr_grstatus not in('DL')
		and a.pomas_pono = b.poitm_pono and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE')
		and a.pomas_poamendmentno = b.poitm_poamendmentno 
		and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr 
									 WHERE pomas_pono = a.pomas_pono AND pomas_poou = a.pomas_poou)
		and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO')
		order by pomas_poauthdate desc";

		$sql6 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no' and item_code = '$item_code'";
		$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
		$sql8 ="select * from TIPLDB..road_permit_state a, scmdb..supp_addr_address b where a.tin_two_digits = b.supp_addr_state 
		and supp_addr_supcode = '$supp_code'";
		$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
		$sql_current_yr_con = "select RecptTotalQty from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$item_code'";
		$sql_last_supplier_details = "select SuppName, SuppRate, SuppLeadTime from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
	
		$sql_reorder_details = "select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
		
		//Charges & Discount
		
		$sql_linelvl_charges = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
		where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
		and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode != 'PDISCOUNTS'";
								
		$sql_linelvl_discount = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
		where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
		and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode = 'PDISCOUNTS'";
		
		//HSN CODE ITEM
		$sql_hsnsac = "select commoditycode from scmdb..trd_tax_group_dtl 
		where item_code='$item_code' and tax_group_code like'%GST%' 
		and commoditycode is not null";
		
		
		$query1 = $this->db->query($sql1);
		$this->db->close();
		$this->db->initialize();
		$query2 = $this->db->query($sql2);
		$query3 = $this->db->query($sql3);
		$query4 = $this->db->query($sql4);
		$query5 = $this->db->query($sql5);
		$query6 = $this->db->query($sql6);
		$query7 = $this->db->query($sql7);
		$query8 = $this->db->query($sql8);
		$query_draw = $this->db->query($sql_draw);
		$query_current_yr_con = $this->db->query($sql_current_yr_con);
		$query_last_supplier_details = $this->db->query($sql_last_supplier_details);
		$query_reorder_details = $this->db->query($sql_reorder_details);
		
		//charges and discount starts
		
		$query_linelvl_charges = $this->db->query($sql_linelvl_charges);
		$query_linelvl_discount = $this->db->query($sql_linelvl_discount);
		
		$query_hsnsac = $this->db->query($sql_hsnsac);
		
		if($query_hsnsac->num_rows() > 0){
			foreach($query_hsnsac->result() as $row){
				$hsncode = $row->commoditycode;
			}
		} else {
			$hsncode = "";
		}
		
		$arr_hsn[] = $hsncode;
		
		if ($query_linelvl_charges->num_rows() > 0) {
		  foreach ($query_linelvl_charges->result() as $row) {
			  $line_level_charges = $row->potcd_tcdamount;
			}
		} else {
			  $line_level_charges = 0;
		}
		
		if ($query_linelvl_discount->num_rows() > 0) {
		  foreach ($query_linelvl_discount->result() as $row) {
			  $line_level_discount = $row->potcd_tcdamount;
			}
		} else {
			  $line_level_discount = 0;
		}
		
		//charges and discount ends
			
		if ($query2->num_rows() > 0) {
		  foreach ($query2->result() as $row) {
			  $lastyr_cons = $row->ConsTotalQty;
			  $lastyr_cons1 = number_format($lastyr_cons,2);
			}
		} else {
			  $lastyr_cons = 0;
			  $lastyr_cons1 = 0.00;
		}
		
		if ($query3->num_rows() > 0) {
			  $current_stk = 0;
		  foreach ($query3->result() as $row) {
			  $current_stock = $row->ItemStkAccepted;
			  $current_stk = $current_stk + $current_stock;
			  $current_stk1 = number_format($current_stk,2);
			}
		} else {
			  $current_stk = 0;
			  $current_stk1 = 0.00;
		}
		
		if ($query4->num_rows() > 0) {
		  foreach ($query4->result() as $row) {
			  $reservation_qty = $row->AllocPendingTot;
			  $reservation_qty1 = number_format($reservation_qty,2);
			}
		} else {
			  $reservation_qty = 0;
			  $reservation_qty1 = 0.00;
		}
		
		if ($query5->num_rows() > 0) {
		  foreach ($query5->result() as $row) {
			  $last_price = $row->poitm_po_cost;
			  $last_price1 = number_format($last_price,2);
			  $gr_lin_linestatus = $row->gr_lin_linestatus;
			  
			  if($gr_lin_linestatus == 'FA'){
				  $last_grcpt_date = $row->gr_lin_fadate;
			  } else if($gr_lin_linestatus == 'FM'){
				  $last_grcpt_date = $row->gr_lin_fmdate;
			  } else if($gr_lin_linestatus == 'FZ'){
				  $last_grcpt_date = $row->gr_lin_frdate;
			  } else if($gr_lin_linestatus == 'DR' || $gr_lin_linestatus == 'FR'){
				  $last_grcpt_date = $row->gr_hdr_grdate;
			  }
			}
		} else {
			  $last_price = "";
			  $last_price1 = "";
			  $last_grcpt_date = "";
		}
		
		if ($query6->num_rows() > 0) {
		  foreach ($query6->result() as $row) {
				$ipr_type            = $row->usage;
				$sono                = $row->sono;
				$atac_no             = $row->atac_no;
				$atac_ld_date        = $row->atac_ld_date;
				$atac_need_date      = $row->atac_need_date;
				$atac_payment_terms  = $row->atac_payment_terms;
				$pm_group            = $row->pm_group;
				$category            = $row->category;
				$project_name        = $row->project_name;
				$costing             = $row->costing;
				$ipr_remarks         = $row->remarks;
				$attached_cost_sheet = $row->attch_cost_sheet;
				$ipr_need_date       = $row->need_date;
				$manufact_clrnce	 = $row->manufact_clearance;
				$dispatch_inst       = $row->dispatch_inst;
				$supp_item_remarks   = $row->pr_supp_remarks; 
				$costing_currency    = $row->costing_currency;
				$why_spcl_rmks       = $row->why_spcl_rmks;
				$pr_qty              = $row->required_qty;
				$tc_req              = $row->test_cert;
				
				$arr[] = $category;
			}
		} else {
				$ipr_type            = "";
				$sono                = "";
				$atac_no             = "";
				$pm_group            = "";
				$category            = "";
				$project_name        = "";
				$costing             = "";
				$ipr_remarks         = "";
				$ipr_need_date       = "";
				$atac_ld_date        = "";
				$atac_need_date      = "";
				$atac_payment_terms  = "";
				$attached_cost_sheet = "";
				$manufact_clrnce	 = "";
				$dispatch_inst       = "";
				$supp_item_remarks   = ""; 
				$costing_currency    = "";
				$why_spcl_rmks       = "";
				$pr_qty              = "";
				$tc_req              = ""; 	  
		}
		
		$arr_pm_grp[] = $pm_group;
		$arr_usage_type[] = $ipr_type;
		
		if ($query7->num_rows() > 0) {
		  $incoming_qty_tot1 = 0;
		  foreach ($query7->result() as $row) {
			  $incoming_qty = $row->PendPoSchQty;
			  $incoming_qty_tot1 = $incoming_qty_tot1 + $incoming_qty;
			  $incoming_qty_tot  = number_format($incoming_qty_tot1,2);
			}
		} else {
			  $incoming_qty_tot = "";
		}
		
		if ($query8->num_rows() > 0) {
		  foreach ($query8->result() as $row) {
			  $supp_state = $row->state_code;
			  $road_permit_req = $row->road_permit;
			}
		} else {
			  $supp_state = "";
			  $road_permit_req = "No";
		}
		
		if ($query_draw->num_rows() > 0) {
		  foreach ($query_draw->result() as $row) {
			  $drawing_no = $row->DrawingNo;
			  $purchase_uom  = $row->ItemPurcaseUom;
			  $manufact_uom = $row->ItemMnfgUom;
			}
		} else {
			  $drawing_no = "";
			  $purchase_uom  = "";
			  $manufact_uom = "";
		}
		
		if ($query_current_yr_con->num_rows() > 0) {
		  foreach ($query_current_yr_con->result() as $row) {
			  $current_yr_con = $row->RecptTotalQty;
			}
		} else {
			  $current_yr_con = "";
		}
		
		if ($query_last_supplier_details->num_rows() > 0) {
		  foreach ($query_last_supplier_details->result() as $row) {
			  $SuppName = $row->SuppName; 
			  $SuppRate = $row->SuppRate; 
			  $SuppLeadTime = $row->SuppLeadTime;
			}
		} else {
			  $SuppName = ""; 
			  $SuppRate = ""; 
			  $SuppLeadTime = "";
		}
		
		if ($query_reorder_details->num_rows() > 0) {
		  foreach ($query_reorder_details->result() as $row) {
			  $iou_reorderlevel = $row->iou_reorderlevel; 
			  $iou_reorderqty = $row->iou_reorderqty;
			}
		} else {
			  $iou_reorderlevel = ""; 
			  $iou_reorderqty = "";
		}
		
		$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' 
		and ucon_itemcode='$item_code'";

		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0) {
		  foreach ($query->result() as $row) {
			  $ucon_confact_ntr = $row->ucon_confact_ntr;
			  $ucon_confact_dtr = $row->ucon_confact_dtr;
			  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
		  }
		} else {
			   $conversion_factor = "";  
		}
		//Supplier Average Lead Time
					
		$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
		$query_supplier = $this->db->query($sql_supplier);
		
		if ($query_supplier->num_rows() > 0) {
			$supplier_lead_time_tot = 0;
			$counter = 0;
		  foreach ($query_supplier->result() as $row) {
			  $counter++;
			  $supplier_lead_time = $row->SuppLeadTime;
			  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
		  }
		  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
		} else {
			   $counter = 0;
			   $supplier_lead_time = "";
			   $supplier_lead_time_tot = ""; 
			   $supplier_avg_lead_time = 0;  
		}
		
		$sql_supplier = "exec TIPLDB..supplier_details  '1' , '$item_code'  , '$po_ipr_no'";
		$query_supplier = $this->db->query($sql_supplier);
		
		$this->db->close();
		$this->db->initialize();
		
		$sql_supplier_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
		$query_supplier_dtl = $this->db->query($sql_supplier_dtl);
		
		 foreach ($query_supplier_dtl->result() as $row) {
			$diffdays    = $row->diffDays;
		 }
		
		if($diffdays != NULL){
			if($diffdays > 0){
				$color = "red";
			} else if($diffdays < 0){
				$color = "green";
			} else if($diffdays = 0){
				$color = "blue";
			}
		} else {
			$color = "yellow";
		}
		
		
?>    

<?php if($po_three_letters != 'FPO'){ ?>
     
      <tr>
			<input type="hidden" name="po_num" value="<?php echo $po_num; ?>">
            <input type="hidden" name="po_date" value="<?php echo $po_date; ?>">
            <input type="hidden" name="po_line_no[]" value="<?php echo $po_line_no; ?>">
            <input type="hidden" name="po_supp_code" value="<?php echo $supp_code; ?>">
            <input type="hidden" name="po_qty" value="<?php echo $po_quantity; ?>">
            <input type="hidden" name="po_amend_no" value="<?php echo $po_amend_no; ?>"> 
            <input type="hidden" name="po_type" value="<?php echo $po_type; ?>">
            <input type="hidden" name="supp_phone" value="<?php echo $supp_phone; ?>">
            <input type="hidden" name="contact_person" value="<?php echo $contact_person; ?>"> 
            <input type="hidden" name="pay_mode" value="<?php echo $pay_mode; ?>">
            <input type="hidden" name="trans_mode" value="<?php echo $trans_mode; ?>">
            <input type="hidden" name="partial_ship" value="<?php echo $partial_ship; ?>">
            <input type="hidden" name="po_date" value="<?php echo $po_date; ?>">
            <input type="hidden" name="po_cost_pr_unt" value="<?php echo $cost_pr_unt; ?>">
            <input type="hidden" name="po_need_date" value="<?php echo $need_date; ?>">
            <input type="hidden" name="po_wh_code" value="<?php echo $wh_code; ?>">
            <input type="hidden" name="po_basic_val" value="<?php echo $totalcost; ?>">
            <input type="hidden" name="po_tot_val" value="<?php echo $totalcost; ?>">
            <input type="hidden" name="po_supp_email" value="<?php echo $supp_email; ?>">
            <input type="hidden" name="supp_state" value="<?php echo $supp_state; ?>">
            <input type="hidden" name="road_permit_req" value="<?php echo $road_permit_req; ?>">
            <input type="hidden" name="po_from_screen" value="<?php echo $po_from_screen; ?>">
            <input type="hidden" name="tc_req[]" value="<?php echo $tc_req; ?>">            
        <td>
            <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
            	<?php echo $po_ipr_no; ?>
            </a>
            <input type="hidden" name="po_ipr_no[]" value="<?php echo $po_ipr_no; ?>">
        </td>
        <td>
        	<!-- ITEM CODE ---->
            <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
				<?php  echo "<b>ITEM CODE - </b>".$item_code; ?>
            </a>
            <input type="hidden" name="po_item_code[]" value="<?php echo $item_code; ?>"><br><br>
            
            <!--- ITEM DESCRIPTION ---->
            <?php if($item_desc != ''){ echo "<b>ITEM DESC - </b>".mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");  } ?>
            <input type="hidden" name="item_desc[]" value="<?php echo $item_desc_new; ?>"><br><br>
             
            <!--- UOM ---->
            <?php if($uom != ''){ echo "<b>UOM - </b>".$uom; } ?>
            <input type="hidden" name="po_uom[]" value="<?php echo $uom; ?>"><br><br>
            
            <!--- HSN CODE ---->
            <?php if($hsncode != ''){ echo "<b>HSN - </b>".$hsncode; } ?>
            <input type="hidden" name="hsncode[]" value="<?php echo $hsncode; ?>">
            
        </td>
        
        <td>
			<?php if($pr_qty != ''){ echo number_format($pr_qty,2); } ?>
            <input type="hidden" name="pr_qty[]" value="<?php echo $pr_qty; ?>">
        </td>
        
        <td>
			<?php if($for_stk_qty != ''){ echo number_format($for_stk_qty,2); } ?>
            <input type="hidden" name="for_stk_quantity[]" value="<?php echo $for_stk_qty; ?>">
        </td>
        
        <td>
        	<?php /*?><a href="<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>" target="_blank"><?php */?>
				<?php if($costing != ''){ echo $costing."  ".$costing_currency; } ?>
            <?php /*?></a><?php */?>
            <input type="hidden" id="costing<?php echo $i; ?>" name="costing[]" value="<?php echo $costing; ?>">
            <input type="hidden" name="attached_cost_sheet[]" value="<?php echo $attached_cost_sheet; ?>">
            <input type="hidden" name="costing_currency[]" value="<?php echo $costing_currency; ?>"> 
            
        </td>
        
        <td>
            <?php echo $last_price1; ?>
            <input type="hidden" id="last_price<?php echo $i; ?>" name="last_price[]" value="<?php echo $last_price; ?>">
            <br /><br />
			<?php if(!empty($last_price1)){ ?>
            <b>Last 3 GRCPT</b>
            <table border="1">
                <tr style="font-weight:bold; background:#0CF">
                    <td>GRCPT NO</td>
                    <td>GRCPT STATUS</td>
                    <td>GRCPT DATE</td>
                    <td>LAST PRICE</td>
                </tr>
                <?php
                    //Last 3 GRCPT'S
                    $sql_last3_gr = "select top 3 gr_hdr_grno,gr_lin_linestatus,gr_hdr_grdate,poitm_po_cost 
            from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..gr_hdr_grmain c, scmdb..gr_lin_details d 
            where b.poitm_itemcode = '".$item_code."' and c.gr_hdr_grno = d.gr_lin_grno and b.poitm_itemcode = d.gr_lin_itemcode 
            and a.pomas_pono = c.gr_hdr_orderno and c.gr_hdr_orderdoc = 'PO' and c.gr_hdr_grstatus not in('DL') and a.pomas_pono = b.poitm_pono 
            and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE') and a.pomas_poamendmentno = b.poitm_poamendmentno 
            and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr 
                         WHERE pomas_pono = a.pomas_pono AND pomas_poou = a.pomas_poou) 
            and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO') 
            order by pomas_poauthdate desc";
            
                    $query_last3_gr = $this->db->query($sql_last3_gr);
                    if ($query_last3_gr->num_rows() > 0) {
                      foreach ($query_last3_gr->result() as $row) {
                          $last3_grno = $row->gr_hdr_grno;
                          $last3_grstat = $row->gr_lin_linestatus;
                          $last3_grdate = $row->gr_hdr_grdate;
                          $last3_price = $row->poitm_po_cost;
                ?>
                <tr>
                    <td><?=$last3_grno; ?></td>
                    <td><?=$last3_grstat; ?></td>
                    <td><?=substr($last3_grdate,0,11); ?></td>
                    <td><?=number_format($last3_price,2,".",""); ?></td>
                </tr>
                <?php
                    }
                    } else {
                          $last3_grno = "";
                          $last3_grstat = "";
                          $last3_grdate = "";
                          $last3_price = ""; 
                    }
                ?>
            </table>
            <?php } ?>
        </td>
        
        <td>
            <?php echo number_format($current_price,2); ?>
            <input type="hidden" name="current_price[]" id="current_price<?php echo $i; ?>" value="<?php echo $current_price; ?>">
        </td>
        
        <td>
        	<?php
			if($costing != '' && $current_price > $costing && ($category != 'CAPITAL GOODS' || $category != 'IT' || $category != 'OTHERS')){
			?>
				<input type="text" name="cost_over_reason[]" id="cost_over_reason<?php echo $i; ?>" value="">
			<?	
			} else if($costing == '' && $last_price != '' && $last_price < $current_price 
			&& ($category != 'CAPITAL GOODS' || $category != 'IT' || $category != 'OTHERS')){
			?>
				<input type="text" name="cost_over_reason[]" id="cost_over_reason<?php echo $i; ?>" value="">
			<?
			} else {
			?>
				<input type="hidden" name="cost_over_reason[]" id="cost_over_reason<?php echo $i; ?>" value="Correct">
			<? } ?>
        </td>
        
        <td><?php echo number_format($line_level_charges,2); ?></td>
        <td><?php echo number_format($line_level_discount,2); ?></td>
        <td>
            <?php
				$item_value_new = ($item_value+$line_level_charges)-$line_level_discount; 
                echo number_format($item_value_new,2);
            ?>
            <input type="hidden" name="item_value[]" id="item_value<?php echo $i; ?>" value="<?php echo $item_value; ?>">
        </td>
        
        <td style="background-color:<?php echo $color; ?>">
			<?php echo $ipr_need_date; ?> 
            <input type="hidden" name="ipr_need_date[]" value="<?php echo $ipr_need_date; ?>"> 
            <input type="hidden" name="color[]" value="<?php echo $color; ?>">
        </td>
        
        <td style="background-color:<?php echo $color; ?>">
			<?php echo substr($po_need_date,0,11); ?>
        </td>
        
        <?php if(strtotime($ipr_need_date) != strtotime($po_need_date)){ ?>
        
        	<td><input type="text" name="need_date_rmks[]" id="need_date_rmks<?php echo $i; ?>" value=""></td>
        
        <?php } else { ?>
        
        	<td><input type="hidden" name="need_date_rmks[]" id="need_date_rmks<?php echo $i; ?>" value="PR & PO Need Date Are Equal"></td>
        
        <?php } ?>
        <td>
        	<!-- PR TYPE -->
            <?php if($ipr_type != ''){ echo "<b>PR TYPE - </b>".$ipr_type; } ?>
            <input type="hidden" name="ipr_type[]" value="<?php echo $ipr_type; ?>"><br><br> 
            
            <!-- WHY SPECIAL REMARKS -->
            <?php if($why_spcl_rmks != ''){ echo "<b>SPCL RMKS - </b>".$why_spcl_rmks; } ?> 
            <input type="hidden" name="why_spcl_rmks[]" value="<?php echo $why_spcl_rmks; ?>"><br><br>
            
            <!-- CATEGORY -->
            <?php
                $po_first_three = substr($po_num,0,3);
                if($po_first_three == 'CGP'){
                    $category = 'CAPITAL GOODS';
                    $category1 = 'CAPITAL GOODS';
                }
                
                if($category != ''){ echo "<b>CATEGORY - </b>".$category; }
            ?> 
            <input type="hidden" name="category[]" value="<?php echo $category; ?>"> 
            <input type="hidden" name="category1" value="<?php echo $category; ?>">   
        </td>
        
        <td>
        	<!--- SO NUMBER --->
            <?php if($sono != ''){ echo "<b>SO NO - </b>".$sono; } ?>
            <input type="hidden" name="sono[]" value="<?php echo $sono; ?>"> <br><br>
            
            <!--- ATAC NUMBER --->
            <input type="hidden" name="atac_no[]" value="<?php echo $atac_no; ?>">
            <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>&user_id=<?= $user_id; ?>&name=<?= $name; ?>&user_type=<?= $user_type; ?>">
                <?php if($atac_no != ''){  echo "<b>ATAC NO - </b>".$atac_no; } ?>
            </a> <br><br>
            
            <!--- ATAC LD DATE --->
            <?php if($atac_ld_date != ''){  echo "<b>ATAC LD DATE - </b>".$atac_ld_date; } ?>
            <input type="hidden" name="atac_ld_date[]" value="<?php echo $atac_ld_date; ?>"> <br><br>
            
            <!--- ATAC NEED DATE --->
            <?php if($atac_need_date != ''){  echo "<b>ATAC NEED DATE - </b>".$atac_need_date; } ?> 
            <input type="hidden" name="atac_need_date[]" value="<?php echo $atac_need_date; ?>"><br><br>
            
            <!--- ATAC PAYMENT TERMS --->
            <?php if($atac_payment_terms != ''){  echo "<b>ATAC PAYTERM - </b>".$atac_payment_terms; } ?> 
            <input type="hidden" name="atac_payment_terms[]" value="<?php echo $atac_payment_terms; ?>"><br><br>
            
            <!--- PM GROUP --->
            <?php if($pm_group != ''){  echo "<b>PM GROUP - </b>".$pm_group; } ?> 
            <input type="hidden" name="pm_group[]" value="<?php echo $pm_group; ?>">
            <input type="hidden" name="pm_group1" value="<?php echo $pm_group; ?>"><br><br>
            
            <!--- PROJECT NAME --->
            <?php if($project_name != ''){  echo "<b>PROJECT NAME - </b>".$project_name; } ?> 
            <input type="hidden" name="project_name[]" value="<?php echo $project_name; ?>"><br><br> 
            
        </td>
        
        <td>
        	<!-- PR REMARKS PLANNING -->
            <?php if($ipr_remarks != ''){  echo "<b>PR REMARKS PLANNING - </b>".$ipr_remarks; } ?>
            <input type="hidden" name="ipr_remarks[]" value="<?php echo $ipr_remarks; ?>"><br><br>
            
            <!-- PR REMARKS PURCHASE -->
            <?php if($ipr_remarks != ''){  echo "<b>PR REMARKS PURCHASE - </b>"; } ?>
            <input type="text" name="item_remarks[]" id="item_remarks" value="">
        </td>
        
        <td>
            <?php echo $supp_item_remarks; ?>
            <input type="hidden" name="supp_item_remarks[]" id="supp_item_remarks" value="<?php echo $supp_item_remarks; ?>">
        </td>
        
        <td>
        	<!-- MANUFACTURING CLEARANCE -->
            <?php if($manufact_clrnce != ''){  echo "<b>MF - </b>".$manufact_clrnce; } ?>
            <input type="hidden" name="manufact_clrnce[]" id="manufact_clrnce" value="<?php echo $manufact_clrnce; ?>"  />
            
            <!-- DISPATCH INSTRUCTION -->
            <?php if($dispatch_inst != ''){  echo "<b>DI - </b>".$dispatch_inst; } ?>
            <input type="hidden" name="dispatch_inst[]" id="dispatch_inst" value="<?php echo $dispatch_inst; ?>"  />
        </td>
        
        <td>
            <?php if($wh_code != ''){  echo $wh_code; } ?>
            <input type="hidden" name="wh_code" value="<?php echo $wh_code; ?>">
        </td>
        
        <td>
        	<!-- LAST SUPPLIER -->
            <?php if($SuppName != ''){  echo "<b>SUPP NAME - </b>".$SuppName; } ?>
            <input type="hidden" name="SuppName[]" value="<?php echo $SuppName; ?>"><br><br>
            
            <!-- LAST SUPPLIER RATE -->
            <?php if($SuppRate != ''){  echo "<b>SUPP RATE - </b>".number_format($SuppRate,2); } ?>
            <input type="hidden" name="SuppRate[]" value="<?php echo $SuppRate; ?>"><br><br>
            
            <!-- LAST SUPPLIER LEAD TIME -->
            <?php if($SuppLeadTime != ''){  echo "<b>SUPP LEAD TIME - </b>".number_format($SuppLeadTime,2); } ?>
            <input type="hidden" name="SuppLeadTime[]" value="<?php echo $SuppLeadTime; ?>"><br><br>
            
            <!-- LAST SUPPLIER RATE -->
            <?php if($last_grcpt_date != ''){  echo "<b>LAST GRCPT FM DATE - </b>".substr($last_grcpt_date,0,11); } ?>
            <input type="hidden" name="last_grcpt_date[]" value="<?php echo $last_grcpt_date; ?>"><br><br>
        </td>
        
        <td><input type="file" name="attach_cost_sheet[]" id="attach_cost_sheet" value="" style="display:none"></td>
      </tr>
      
<?php } else { ?>

	<?php include('fpo_item_details.php'); ?>

<?php } ?>      
<?php } ?> 

 </tbody>
</table>
</div>
<br><br> 

<?php //echo $i; ?>

<input type="hidden" id="number_of_rows" name="number_of_rows" value="<?php echo $i; ?>">

<?php if($po_three_letters == 'FPO'){ ?>


<div class="row">
	<div class="col-lg-8">
    	<b>TERMS:</b>
    	<table class="table table-bordered" cellpadding="2" cellspacing="2" id="fpo_terms">
        	<tr>
            	<td>1</td>
            	<td>Incoterms :</td>
                <td><?php echo $freight1.",".$freight_place; ?></td>
            </tr>
            <tr>
            	<td>2</td>
            	<td>Shipment Through :</td>
                <td><?php echo $transport_mode; ?></td>
            </tr>
            <tr>
            	<td>3</td>
            	<td>Payment Terms :</td>
                <td><input type="text" name="payment_fpo" id="payment_fpo" value="<?php echo $payment_fpo; ?>" class="form-control"/></td>
            </tr>
            <tr>
            	<td>4</td>
            	<td>Delivery Date :</td>
                <td><input type="text" name="delivery_fpo" id="delivery_fpo" value="<?php echo $delivery_fpo; ?>" class="form-control"/></td>
            </tr>
            <tr>
            	<td>5</td>
                <td>Packing List :</td>
            	<td>
                Draft/estimated packing list after placing the purchase order is mandatorily require for arranging the 
                Freight forwarder / courier company pick up. 
                </td>
            </tr>
            <tr>
            	<td>6</td>
                <td>Documentation:</td>
            	<td>
                <p style="text-align:justify">
                i)Commercial Invoice, Packing List & Test certificates/Certificate of confirmation (COC) are mandatorily 
                require along with all the shipments.Please send the commercial invoice, air waybill & packing list for approval to us 
                before dispatch of the material. All documents must be in English language.<br><br>  
                ii)The shipper name,shipper address, buyer name, buyer address, value,quantity, etc. mentioned on your Commercial invoice & 
                the Airway bill must totally match with the Proforma invoice sent to us for payment/ or as per our Purchase order.<br><br> 
                iii)Our approved commercial invoice, air waybill only need to be handover to our Freight forwarder / Courier company. 
                Once we approve the documents, do not change the detail on your commercial invoice / Air Waybill while handing over the 
                material to our freight forwarder / courier company.<br><br>  
                iv)If INCOTERM / Freight term is Ex Works/FOB/FCA, do not handover the material to any freight forwarder/courier 
                company unless we confirm you the name of the same.<br><br>  
                v) In case if any of the above mentioned terms & conditions 
                are not followed by you, all the losses to us if happen due to this will be recovered from your payment or will be adjusted 
                from your due / subsequent orders payment.
                </p>
                </td>
            </tr>
        </table>
        <b>Add More Terms</b>
        <table class="table table-bordered" cellpadding="2" cellspacing="2" id="fpo_terms1">
            <tr>
            	<td><b>SNO</b></td>
            	<td><b>TERM</b></td>
                <td><img src='<?php echo base_url("assets/admin/img/");?>plus.png' width='20px' height='20px' onclick='return addrow_fpo_terms()'/></td>
            </tr>
        </table>
    </div>
</div><br>


<?php } ?>

<!--********* REMARKS *********-->               
<div class="row">
  <div class='col-lg-3'> 
    <b>Attach Quotes From Supplier</b><b style="color:#F00">&nbsp;*</b>
  </div>
  <div class='col-lg-4'> 
    <!--<input type="file" name="po_quote_frmsupp[]"  id="po_quote_frmsupp" class="form-control" multiple />-->
    <div class="controls">   
        <div class="entry input-group col-xs-3">
            <input type="file" class="btn btn-primary" name="po_quote_frmsupp[]" id="po_quote_frmsupp" multiple required="required">
            <span class="input-group-btn">
            <button class="btn btn-success btn-add" type="button">
                <span class="glyphicon glyphicon-plus"></span>
            </button>
            </span>
        </div>
    </div>  
  </div> 
  <div class="col-lg-2">
     <b> Enter Remarks</b>
  </div>
  <div class="col-lg-3">
      <input type="text" name="po_rmks" id="po_rmks" value=""  class="form-control"/>
  </div>
</div> <br>

<?php

$sql_quotes = "select count(*) as cnt from tipldb..po_supplier_quotes where po_num = '$po_number'";
$query_quotes = $this->db->query($sql_quotes)->row();
$count = $query_quotes->cnt;

if($count > 0){
?>
<!-- Previous Attachments -->
<div class="row">
	<div class='col-lg-7'>
    <h4>Previous Attachments</h4>
    <table class="table table-bordered">
    	<tr style="background-color:#CCCCCC">
        	<td><b>SNO</b></td>
            <td><b>ATTACHMENT</b></td>
            <td><b>DATE</b></td>
        </tr>
        <?php
			$sql_supplier_quotes = "select * from tipldb..po_supplier_quotes where po_num = '$po_number' order by date_time";
			$query_supplier_quotes = $this->db->query($sql_supplier_quotes);
			
			$sno = 0;
			foreach ($query_supplier_quotes->result() as $row) {
				$attached_supp_quote = $row->attached_supp_quotes;
			  	$date = substr($row->date_time,0,11);
				$sno++;
		?>
        <tr>
        	<td><?php echo $sno; ?></td>
            <td><a href="<?php echo base_url(); ?>uploads/<?php echo $attached_supp_quote; ?>"><?php echo $attached_supp_quote; ?></a></td>
            <td><?php echo $date; ?></td>
        </tr>
        <?php } ?>
    </table> 
  </div>
  
  <div class="col-lg-5">
  </div>
</div>
<?php } ?>            
<div class="row">
    
	<?php
		//Checking For IC-EXWH Warehouse
		$sql_wh_chk = "select count(*) as count_wh from scmdb..po_poitm_item_detail 
		where poitm_warehousecode is not null 
		and poitm_warehousecode = 'IC-EXWH'
		and substring(poitm_pono,1,3) not in('FPO') 
		and poitm_pono = '$po_num'
		and poitm_poamendmentno in(select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono in('IPO-00005-19')
		)";
		
		$qry_wh_chk = $this->db->query($sql_wh_chk)->row();
		$count_wh = $qry_wh_chk->count_wh;
	
		//Checking condition for Multiple PM Groups
		$pm_grp_chk = array_unique($arr_pm_grp);
		//print_r($pm_grp_chk);
		//die;
		$pm_grp_chk1 = count($pm_grp_chk);
		//echo $pm_grp_chk1;
		
		//Checking For Multiple Category
		$cha = array_unique($arr);
		$cha1 = count($cha);
		
		//Checking Condition For Multiple Usage Type
		$usage_type_chk = array_unique($arr_usage_type);
		$usage_type_chk1 = count($usage_type_chk);
		
		if(in_array('',$arr_hsn)){
			$hsncode_blank = 1;
		} else {
			$hsncode_blank = 0;
		}
		
		if($count_wh > 0){
			echo "<h3 style='text-align:center; color:red;'>Warehouse (IC-EXWH) Not Allowed In Indian Purchase Orders.</h3>";
		} else if($cha1 > 1){
			echo "<h3 style='text-align:center; color:red;'>Purchase Order has Multiple Category Items So It Can't be created.</h3>";
		}else if($hsncode_blank == 1){
			echo "<h3 style='text-align:center; color:red;'>HSN CODE of Line Item in PO cannot be blank.</h3>";
		}else if($pm_grp_chk1 > 1){
			echo "<h3 style='text-align:center; color:red;'>Purchase Order has Multiple PM Groups So It Can't be Created.</h3>";
		}else if($usage_type_chk1 > 1){
			echo "<h3 style='text-align:center; color:red;'>Purchase Order has Multiple PR Types So It Can't be Created.</h3>";
		} else {
    ?>
    <?php
		if($supp_email == '' || $supp_email == NULL){
			echo "<h3 style='text-align:center; color:red;'>Please Enter Supplier Email In ERP.</h3>";
		} else if($supp_phone == '' || $supp_phone == NULL){
			echo "<h3 style='text-align:center; color:red;'>Please Enter Supplier Contact Number In ERP.</h3>";
		} else if($contact_person == '' || $contact_person == NULL){

			echo "<h3 style='text-align:center; color:red;'>Please Enter Contact Person Name In ERP.</h3>";
		} else if(in_array("",$pr_num_arr) && $po_first_three != 'SRV'){
			echo "<h3 style='text-align:center; color:red;'>PR Number In Any Line Of PO Cannot Be Blank.</h3>";
		} else {
			
			$sql_muli_proj = "select count(distinct atac_no) as count1 from 
			scmdb..po_pomas_pur_order_hdr a, 
			scmdb..po_poitm_item_detail b, 
			scmdb..po_poprq_poprcovg_detail d,  
			scmdb..prq_preqm_pur_reqst_hdr i,
			tipldb..pr_submit_table j
			where a.pomas_pono = b.poitm_pono
			and a.pomas_pono = d.poprq_pono 
			and a.pomas_pono ='".$po_num."' 
			and b.poitm_polineno = d.poprq_polineno 
			and a.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '".$po_num."') 
			and a.pomas_poamendmentno = b.poitm_poamendmentno 
			and a.pomas_poamendmentno = d.poprq_poamendmentno 
			and d.poprq_prno = i.preqm_prno 
			and i.preqm_prno = j.pr_num";
			
			$qry_multi_proj = $this->db->query($sql_muli_proj);
			
			foreach ($qry_multi_proj->result() as $row){
				$count_proj = $row->count1;
			}
			
			if($count_proj > 1 && $po_num != 'FPO-00505-18'){
				echo "<h3 style='text-align:center; color:red;'>You Can't Link Multiple Projects PR's In A Single PO.</h3>";	
			} else {
	?>
        <div class="col-lg-4"></div>
        
        <!-- Draft Button --->
        <?php if($po_first_three == 'FPO'){ ?>
        <div class="col-lg-2">
            <input type="submit" name="po_approval_lvl0" value="Draft" class="form-control" id="po_approval_lvl0" onclick="hide('po_approval_lvl0')">
        </div>
        <?php } ?>
         
        <!---Send For Approval Button ---> 
        <div class="col-lg-2">
        	<input type="submit" name="po_approval_lvl0" value="Send PO For Approval" class="form-control" id="po_approval_lvl0" onclick="hide('po_approval_lvl0')">
        </div>
        
        <div class="col-lg-4"></div>
    
      <?php } ?>
    
    <?php } ?>
    
    <?php } ?>    
</div><br><br>

</form>

<?php //chat history ?>
 <div class="row">
  <div class="col-lg-12">
    <h3>Chat History</h3>	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>LEVEL</th>
                <th>NAME</th>
                <th>COMMENT</th>
                <th>INSTRUCTION</th>
                <th>DATE TIME</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
                $query1 = $this->db->query($sql1);
                if ($query1->num_rows() > 0) {
				foreach ($query1->result() as $row) {
				  $level = $row->level;
				  $name  = $row->comment_by;
				  $comment = $row->comment;
				  $datentime = $row->datentime;
				  $instruction = $row->instruction;
                                
               ?>
              <tr>
                <td><?php echo $level; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $comment; ?></td>
                <td><?php echo $instruction; ?></td>
                <td><?php echo $datentime; ?></td>                            
              </tr>
              <?php 
                } } 
              ?>
            </tbody>
       </table>    
    </div>
 </div>
<?php //chat history ?>
 

<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12">
    	<h3>Action Timing</h3>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<table align="center" class="table table-bordered">
        	<thead>
            	<th>ERP CREATED</th>
                <th>ERP CREATED DATE</th>
                <th>ERP LAST MODIFIED</th>
                <th>ERP LAST MODIFIED DATE</th>
            </thead>
         	<?php foreach($view_po->result() as $row){ ?>
            <tbody>
            	<td><?php echo $row->pomas_createdby; ?></td>
                <td><?php echo $row->pomas_createddate; ?></td>
                <td><?php echo $row->pomas_lastmodifiedby; ?></td>
                <td><?php echo $row->pomas_lastmodifieddate; ?></td>
            </tbody>
            <?php break; } ?>
        </table>    
    </div>
</div>

<?php //Action Timing Report ?>

    
  </section>
</section>

<?php include('footer.php'); ?>

<!-- Date Picker Code -->
<script>

	$( "#datepicker1" ).datepicker();
	$( "#date1" ).datepicker();

</script>
<script>
	function paymode_type(){
		var table = document.getElementById("amc_schedule");
	    var len = table.rows.length;
		len = len -1;
		
		console.log(len);
		
		for(var i=0;i<=len;i++){
			
			paymode = document.getElementById("paymode"+i).value;
			
			alert(paymode);
			//console.log(paymode)
			
			if(paymode == 'PDC'){
				document.getElementById("pdc_days"+i).style.display="block";
			} else {
				document.getElementById("pdc_days"+i).style.display="none";
			}
		}
	}
</script>
<script>
	function isNumber(evt) {
		evt = (evt) ? evt : window.event;
		var charCode = (evt.which) ? evt.which : evt.keyCode;
		if (charCode > 31 && (charCode < 48 || charCode > 57)) {
			return false;
		}
		return true;
	}
</script>
<script>
	function cal_amount(a,b){
			var po_total_value   = document.getElementById("po_total_value").value;
			
			var net_amount = 0;
			net_amount = Number((a*po_total_value)/100);
			  
			$('#amount'+b).val(net_amount);
	}
</script>
<!-- Page Validations -->
<script type="text/javascript">

	function reqd(){
		
	  var delivery_type = document.getElementById("po_deli_type").value;
	  var po_lead_time = document.getElementById("datepicker1").value;
	  var currency = document.getElementById("currency").value;
	  var freight_type = document.getElementById("freight_type").value;
	  var approx_freight = document.getElementById("approx_freight").value;
	  var ld_applicable = document.getElementById("ld_applicable").value;
	  var spcl_inst_supp = document.getElementById("spcl_inst_supp").value;
	  var po_quote_frmsupp = document.getElementById("po_quote_frmsupp").value;
	  var supplier_email = document.getElementById("supplier_email").value;
	  var supplier_phone_no = document.getElementById("supplier_phone_no").value;
	  var contact_person = document.getElementById("contact_person").value;
	  var po_total_value = document.getElementById("po_total_value").value;
	  
	  /* service */
	  var po_first_three = document.getElementById("po_first_three").value;
	  
	  /* Freight Remarks */
	  var freight_rmks = document.getElementById("freight_rmks").value;
	  
	  if(po_first_three == 'SRV'){
		  var spo_type = document.getElementById("spo_type").value;
		  var spo_category = document.getElementById("spo_category").value;
		  var spo_usage_type = document.getElementById("spo_usage_type").value;
		  var spo_atac_no = document.getElementById("spo_atac_no").value;
		  var spo_special_remarks = document.getElementById("spo_special_remarks").value;
		  var attach_cost_sheet = document.getElementById("attach_cost_sheet").value;
		  
		  if(spo_type == ''){
			  alert("Select Service PO Type");
			  document.getElementById("spo_type").focus();
			  return false;
		  }
		  
		  if(spo_category == ''){
			  alert("Select Service PO Category");
			  document.getElementById("spo_category").focus();
			  return false;
		  }
		  
		  if(spo_type == 'Against Order'){
		  
			  if(spo_usage_type == ''){
				  alert("Select Service PO Usage Type");
				  document.getElementById("spo_usage_type").focus();
				  return false;
			  }
		  
		  }
		  
		  if(spo_usage_type == 'Project'){
			  //Project
			  if(spo_atac_no == ''){
				  alert("Select ATAC Number.");
				  document.getElementById("spo_atac_no").focus();
				  return false;
			  }
			  
			  if(attach_cost_sheet == ''){
				  alert("Attach Cost Sheet.");
				  document.getElementById("attach_cost_sheet").focus();
				  return false;
			  }
		  
		  } else if(spo_usage_type == 'Special'){
		  
			  if(spo_special_remarks == ''){
				  //Special
				  alert("Enter Remarks Why Procure Special.");
				  document.getElementById("spo_special_remarks").focus();
				  return false;
			  }
		  
		  }
		  
		  var table = document.getElementById("amc_schedule");
		  var len = table.rows.length;
		 
		  len = len -1;
		  console.log("length is "+len);
		  
		  var per_tot = 0;
		  
		  for(var i=1;i<=len;i++){
			  
			  var description      = document.getElementById("description"+i).value;
			  var date             = document.getElementById("date"+i).value;
			  var perct_po_value   = document.getElementById("perct_po_value"+i).value;
			  var amount           = document.getElementById("amount"+i).value;
			  var spo_payterm      = document.getElementById("spo_payterm"+i).value;
			  var paymode          = document.getElementById("paymode"+i).value;
			  var pdc_days         = document.getElementById("pdc_days"+i).value;
			  var po_total_value   = document.getElementById("po_total_value").value;
			  
			  per_tot = Number(per_tot)+ Number(perct_po_value);
			  
			  if(description == ""){
				  alert("Enter Description At Line"+i);
				  document.getElementById("description"+i).focus();
				  return false;
			  }
			  
			  if(date == ""){
				  alert("Enter Target Date At Line"+i);
				  document.getElementById("date"+i).focus();
				  return false;
			  }
			  
			  if(perct_po_value == ""){
				  alert("Enter Percentage Of PO Value At Line"+i);
				  document.getElementById("perct_po_value"+i).focus();
				  return false;
			  }
			  
			  if(spo_payterm == ""){
				  alert("Select Payment Term At Line"+i);
				  document.getElementById("spo_payterm"+i).focus();
				  return false;
			  }
			  
			  if(paymode == ""){
				  alert("Select Payment Mode At Line"+i);
				  document.getElementById("paymode"+i).focus();
				  return false;
			  }
			  
			  if(paymode == "PDC"){
				  if(pdc_days == ""){
					  alert("Enter PDC Days At Line"+i);
					  document.getElementById("pdc_days"+i).focus();
					  return false;
				  }
			  }
			  
			 
		  }
		  
		  console.log(per_tot);
		  if(per_tot != 100){
			alert("Sum Of Percentage Of Total Value Should be 100");
			return false;
		  }
	  
	  }
	  
	  /* service */
	  
	  if(po_total_value == ""){
		  alert("PO Total Value Cannot be blank..");
		  document.getElementById("po_total_value").focus();
		  return false;
	  }
		
	  if(supplier_email == ""){
		  
		  alert("Please Enter Supplier Email Address");
		  document.getElementById("supplier_email").focus();
		  return false;
	  }
	  
	  if( supplier_phone_no == ""){
		  
		  alert("Please Enter Supplier Phone No.");
		  document.getElementById("supplier_phone_no").focus();
		  return false;
	  }
	  
	  if( contact_person == ""){
		  
		  alert("Please Enter Contact Person Name");
		  document.getElementById("contact_person").focus();
		  return false;
	  }
	  
	  if( currency == ""){
		  
		  alert("Select Currency");
		  document.getElementById("currency").focus();
		  return false;
	  }
	  
	  if( freight_type == ""){
		  
		  alert("Select Freight Type");
		  document.getElementById("freight_type").focus();
		  return false;
	  } 
	  
	  if( po_lead_time == ""){
		  
		  alert("Enter Expected Matrial Landed Date");
		  document.getElementById("datepicker1").focus();
		  return false;
	  }
	  
	  if( delivery_type == ""){
		  
		  alert("Select Delivery Type");
		  document.getElementById("po_deli_type").focus();
		  return false;
	  }
	  
	  if( approx_freight == ""){
		  
		  alert("Enter Approximate Freight");
		  document.getElementById("approx_freight").focus();
		  return false;
	  } 
	  
	  if( ld_applicable == ""){
		  
		  alert("Select LD Applicable");
		  document.getElementById("ld_applicable").focus();
		  return false;
	  } 
	  
	  
	  if( freight_rmks == ""){
		  
		  alert("Enter Freight Remarks...");
		  document.getElementById("freight_rmks").focus();
		  return false;
	  } 
	  
	  
	    //Current Price Check Of Foreign Terms
		var number_of_rows = document.getElementById("number_of_rows").value;
		var j;
		
		for(j=1; j<=number_of_rows; j++){
			var current_price = document.getElementById("current_price"+j).value;
			var item_value = document.getElementById("item_value"+j).value;
			var need_date_rmks = document.getElementById("need_date_rmks"+j).value;
			
			if(current_price == ''){
				alert("Current price cannot be blank at line no."+j);
				document.getElementById("current_price"+j).focus();
				return false;
			}
			
			if(item_value == ''){
				alert("Item value cannot be blank at line no."+j);
				document.getElementById("item_value"+j).focus();
				return false;
			}
			
			if(need_date_rmks == ''){
				alert("Please Enter Need Date Not Equal Remarks at Line No."+j);
				document.getElementById("need_date_rmks"+j).focus();
				return false;
			}
		}
		
		//Payment and delivery terms
		var payment_fpo = document.getElementById("payment_fpo").value;
		var delivery_fpo = document.getElementById("delivery_fpo").value;
		
		if(payment_fpo == ''){
			alert("Please enter payment terms.");
			document.getElementById("payment_fpo").focus();
			return false;
		}
		
		if(delivery_fpo == ''){
			alert("Please enter delivery terms.");
			document.getElementById("delivery_fpo").focus();
			return false;
		}
		
	    //Additional Terms	
	    var table = document.getElementById("fpo_terms1");
	    var len = table.rows.length;
	    len =len-1;
	  
	    var j;
	  
	    for(k=1; k<=len; k++){
		    var terms = document.getElementById("fpo_add_term"+k).value;
		    if(terms == ''){
			    alert("Please enter PO Terms at line no."+k);
			    document.getElementById("fpo_add_term"+k).focus();
				return false;
		    }
	    }
		
	    //Supplier Quotes
	    if( po_quote_frmsupp == ""){
		  
		    alert("Attach Supplier Quotes");
		    document.getElementById("po_quote_frmsupp").focus();
		    return false;
	    }
	  
	//return false;
	
	}
</script>

<!-- Multiple File Select Box -->

<script type="text/javascript">
	$(function()
	{
		$(document).on('click', '.btn-add', function(e)
		{
			e.preventDefault();
	
			var controlForm = $('.controls:first'),
				currentEntry = $(this).parents('.entry:first'),
				newEntry = $(currentEntry.clone()).appendTo(controlForm);
	
			newEntry.find('input').val('');
			controlForm.find('.entry:not(:last) .btn-add')
				.removeClass('btn-add').addClass('btn-remove')
				.removeClass('btn-success').addClass('btn-danger')
				.html('<span class="glyphicon glyphicon-minus"></span>');
		}).on('click', '.btn-remove', function(e)
		{
		  $(this).parents('.entry:first').remove();
	
			e.preventDefault();
			return false;
		});
	});
</script>

<script type="text/javascript">

	function freight_cond(a){
		
		//alert(a);
		
		if(a=='Hand Delivery'){
			
			$("#po_deli_type").html("<option value='Door Delivery'>Door Delivery</option>");
			
		} else if(a=='To Pay' || a=='Paid Billed'){
			
			$("#po_deli_type").html("<option value='' disabled='disabled'>--Select--</option><option value='Door Delivery'>Door Delivery</option><option value='Godown Delivery'>Godown Delivery</option>");
			
		} else{
			
			$("#po_deli_type").html("<option value='No Delivery Type'>No Delivery Type</option>");
			
		}
	  
	}
</script>

<!----- Service Module Javascript --------->

<!---Select 2 -->
<script>
	$(document).ready(function() {
		 $("#spo_atac_no").select2(); 
	});
</script>
<!--- Select 2 -->


<script type="text/javascript">
function spo_type_fun(spo_type){
	
	if(spo_type == 'Against Order'){
		document.getElementById("against_order_type_label").style.display = "block";
		document.getElementById("spo_usage_type").style.display = "block";
		document.getElementById("atac_label").style.display = "none";
		document.getElementById("atac_selection").style.display = "none";
		document.getElementById("special_label").style.display = "none";
		document.getElementById("spo_special_remarks").style.display = "none";
		document.getElementById("atac_details_div").style.display = "none";
		$('#spo_usage_type').val('');
		$('#spo_atac_no').val('').trigger('change');
		$('#spo_special_remarks').val('');
		$('#detail').html('');
		
	} else if(spo_type == 'Internal Service'){
		document.getElementById("against_order_type_label").style.display = "none";
		document.getElementById("spo_usage_type").style.display = "none";
		document.getElementById("atac_label").style.display = "none";
		document.getElementById("atac_selection").style.display = "none";
		document.getElementById("special_label").style.display = "none";
		document.getElementById("spo_special_remarks").style.display = "none";
		document.getElementById("atac_details_div").style.display = "none";
		$('#spo_usage_type').val('');
		$('#spo_atac_no').val('').trigger('change');
		$('#spo_special_remarks').val('');
		$('#detail').html('');
		
	} else{
		document.getElementById("against_order_type_label").style.display = "none";
		document.getElementById("spo_usage_type").style.display = "none";
		document.getElementById("atac_label").style.display = "none";
		document.getElementById("atac_selection").style.display = "none";
		document.getElementById("special_label").style.display = "none";
		document.getElementById("spo_special_remarks").style.display = "none";
		document.getElementById("atac_details_div").style.display = "none";
		$('#spo_usage_type').val('');
		$('#spo_atac_no').val('').trigger('change');
		$('#spo_special_remarks').val('');
		$('#detail').html('');
		
	}
}
</script>

<script type="text/javascript">
function spo_order_type(order_type){
	
	if(order_type == 'Project'){
		document.getElementById("atac_label").style.display = "block";
		document.getElementById("atac_selection").style.display = "block";
		document.getElementById("atac_details_div").style.display = "block";
		document.getElementById("special_label").style.display = "none";
		document.getElementById("spo_special_remarks").style.display = "none";
		document.getElementById("attach_cost_sheet").style.display = "block";
		
		$('#spo_atac_no').val('').trigger('change');
		$('#spo_special_remarks').val('');
		$('#detail').html('');
		
	} else if(order_type == 'Special'){
		document.getElementById("atac_label").style.display = "none";
		document.getElementById("atac_selection").style.display = "none";
		document.getElementById("special_label").style.display = "block";
		document.getElementById("spo_special_remarks").style.display = "block";
		document.getElementById("atac_details_div").style.display = "none";
		document.getElementById("attach_cost_sheet").style.display = "none";
		
		$('#spo_atac_no').val('').trigger('change');
		$('#spo_special_remarks').val('');
		$('#detail').html('');
		
	} else {
		document.getElementById("atac_label").style.display = "none";
		document.getElementById("atac_selection").style.display = "none";
		document.getElementById("special_label").style.display = "none";
		document.getElementById("spo_special_remarks").style.display = "none";
		document.getElementById("atac_details_div").style.display = "none";
		document.getElementById("attach_cost_sheet").style.display = "none";
		
		$('#spo_atac_no').val('').trigger('change');
		$('#spo_special_remarks').val('');
		$('#detail').html('');
		
	}
}
</script>

<script type="text/javascript">

function atac_details_div(atac_no){
	//alert(atac_no);
	if(atac_no != ""){
		
		document.getElementById("atac_details_div").style.display = "block";
		
		//Ajax Function
		  if (window.XMLHttpRequest)
		  {// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		  }
		  else
		  {// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		  }
			  
		xmlhttp.onreadystatechange=function()
		 {
			
			if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
			} else {
				document.getElementById('detail').innerHTML="<strong>Waiting For Server...</strong>";
			}
		 }
		
		var queryString="?atac_no="+atac_no;
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/createpodc/atac_details"+ queryString, true);
		xmlhttp.send();
	//Ajax Function	
	} else {
		document.getElementById("atac_details_div").style.display = "none";
	}
}

</script>

<script>
// Add Row Code
function addrow() {
    var table = document.getElementById("amc_schedule");
	var len = table.rows.length;
    var row = table.insertRow(len);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
	var cell5 = row.insertCell(4);
	var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
	var cell8 = row.insertCell(7);
	var cell9 = row.insertCell(8);
    cell1.innerHTML = "<td>"+len+"</td>";
    cell2.innerHTML = "<td colspan='3'><input type='text' name='description[]' id='description"+len+"' value='' class='form-control' /></td>";
	cell3.innerHTML = "<td><input type='text' name='date[]' id='date"+len+"' value='' class='form-control' autocomplete='off' /></td>";
    cell4.innerHTML = "<td><input type='text' name='perct_po_value[]' id='perct_po_value"+len+"' class='form-control' onkeyup='cal_amount(this.value,"+len+")' onkeypress='return isNumber(event)' autocomplete='off'/></td>";
	cell5.innerHTML = "<td><input type='text' name='amount[]' id='amount"+len+"' value='' class='form-control' readonly /></td>";
	cell6.innerHTML = "<td><select name='spo_payterm[]' id='spo_payterm"+len+"' class='form-control'><option value=''>--Select--</option><option value='PI'>PI</option><option value='PDC'>PDC</option></select></td>";
    cell7.innerHTML = "<td><select name='paymode[]' id='paymode"+len+"' class='form-control' onchange='paymode_type(this.value)'><option value=''>--Select--</option><option value='Bank Transfer'>Bank Transfer</option><option value='PDC'>PDC</option><option value='CDC'>CDC</option></select></td>";
	cell8.innerHTML = "<td><input type='text' name='pdc_days[]' id='pdc_days"+len+"' value='' class='form-control' onkeypress='return isNumber(event)' autocomplete='off' style='display:block;'/></td>";
	cell9.innerHTML = "<td><img src='<?php echo base_url("assets/admin/img/");?>minus.png' width='20px' height='20px' onclick='return deleterow()'/></td>";
	
	$( "#date"+len ).datepicker();
}

// Delete Row Code
function deleterow() {
	var table = document.getElementById("amc_schedule");
	var len = table.rows.length;
	console.log(len);
    document.getElementById("amc_schedule").deleteRow(len-1);
}
//Service Module Javascripts Ends


//Button Hide Javascript
function hide(a){
	//document.getElementById(a).style.display="none";
}

//Calculation Of foriegn Purchase Order Item Value
function calculate_item_val(cnt){
	//alert(cnt);
	var current_price  = document.getElementById("current_price"+cnt).value;
	var pr_qty  = document.getElementById("pr_qty"+cnt).value;
	
	var item_val = 0;
	
	item_val = current_price*pr_qty;
	
	$("#item_value"+cnt).val(parseFloat(item_val).toFixed(2));
	
	po_total();
}

function po_total(){
	var num_rows  = document.getElementById("number_of_rows").value;
	 
	var po_total_val = 0.00;
	
	for(i=1; i<=num_rows; i++){
		
		var item_value = document.getElementById("item_value"+i).value;
		po_total_val = Number(po_total_val)+Number(item_value);
		
	}
	
	$("#po_total_value").val(parseFloat(po_total_val).toFixed(2));
}

function addrow_fpo_terms() {
    var table = document.getElementById("fpo_terms1");
	var len = table.rows.length;
    var row = table.insertRow(len);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
	var cell3 = row.insertCell(2);
    cell1.innerHTML = "<td>"+len+"</td>";
    cell2.innerHTML = "<td colspan='2'><input type='text' name='fpo_add_term[]' id='fpo_add_term"+len+"' value='' class='form-control'></td>";
	cell3.innerHTML = "<td><img src='<?php echo base_url("assets/admin/img/");?>minus.png' width='20px' height='20px' onclick='return deleterow_fpo_terms()'/></td>";
}

function deleterow_fpo_terms() {
	var table = document.getElementById("fpo_terms1");
	var len = table.rows.length;
	console.log(len);
    document.getElementById("fpo_terms1").deleteRow(len-1);
}
	
</script>

