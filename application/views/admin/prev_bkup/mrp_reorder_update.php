<?php 
	include'header.php'; 
	$user_name = $_SESSION['username'];
?>

<section id="main-content">
  <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                  <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">ITEM REORDER UPDATE</h4>
            </div>
        </div><br />
        
        <form action="#" method="post" id="myform">
        
        <div class="row">
        	<div class="col-lg-12">
            	<table class="table table-bordered">
                	<tr>
                    	<td><b>SNO</b></td>
                        <td><b>ITEM CODE</b></td>
                        <td><b>ITEM DESC</b></td>
                        <td><b>ERP CATEGORY</b></td>
                        <td><b>LIVE CATEGORY</b></td>
                        <td><b>FQV</b></td>
                        <td><b>PRIORITY</b></td>
                        <td><b>ACCEPTED QTY</b></td>
                        <td><b>PENDING ALLOCATION</b></td>
                        <td><b>SUGGESTED RE-ORDER LEVEL</b></td>
                        <td><b>SUGGESTED RE-ORDER QTY</b></td>
                        <td><b>SUGGESTED MOQ</b></td>
                        <td><b>SAFETY STOCK</b></td>
                        <td><b>REORER LEVEL</b></td>
                        <td><b>SYSTEM REORDER LEVEL</b></td>
                        <td><b>REORDER DIFFERENCE</b></td>
                        <td><b>REORDER QTY</b></td>
                        <td><b>STATUS</b></td>
                        <td><b>MOQ</b></td>
                        <td><b>LAST SUPPLIER</b></td>
                        <td><b>AVG LEAD TIME</b></td>
                        <td><b>LAST LEAD TIME</b></td>
                        <td><b>LAST FY RECEIPT QTY</b></td>
                        <td><b>LAST FY RECEIPT FREQUENCY</b></td>
                        <td><b>LAST FY RECEIPT CONSUMPTION QTY</b></td>
                        <td><b>LAST FY RECEIPT CONSUMPTION VALUE</b></td>
                        <td><b>LAST FY RECEIPT CONSUMPTION FREQUENCY</b></td>
                        <td><b>CURRENT FY RECEIPT QTY</b></td>
                        <td><b>CURRENT FY RECEIPT VALUE</b></td>
                        <td><b>CURRENT FY RECEIPT FREQUENCY</b></td>
                        <td><b>CURRENT FY CONSUMPTION QTY</b></td>
                        <td><b>CURRENT FY CONSUMPTION VALUE</b></td>
                        <td><b>CURRENT FY CONSUMPTION FREQUENCY</b></td>
                        <td><b>LAST PICS DATE</b></td>
                    </tr>
                </table>
            </div>
        </div>
        
  </section>
</section>     		
<!--main content end-->
<!-- container section end -->

<?php include('footer.php'); ?>