<?php
	include'header.php';                    
?>
<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PO REPORT</h4>
        </div>
    </div><br />
    
<?php

$valueToSearch = $this->input->post('valueToSearch');

$from_date = $this->input->post('from_date');

$to_date = $this->input->post('to_date');

//echo $valueToSearch;

/*if($valueToSearch != ''){
	
	$sql_main = "select *,datediff(DAY, po_create_date,getdate()) as diff from TIPLDB..po_master_table 
	where (po_num+po_supp_name) like'%$valueToSearch%' order by po_create_date desc";
	
	$query_main = $this->db->query($sql_main);
	
} else {
	
	$sql_main = "select top 20 *,datediff(DAY, po_create_date,getdate()) as diff from TIPLDB..po_master_table order by po_create_date desc";
	
	$query_main = $this->db->query($sql_main);
	
}*/

if($from_date != '' && $to_date != ''){
	
	$sql_main = "select *,datediff(DAY, po_create_date,getdate()) as diff from TIPLDB..po_master_table 
where convert(date,po_create_date) BETWEEN '$from_date' and '$to_date' order by po_create_date desc";
	
	$query_main = $this->db->query($sql_main);
	
} else {
	
	$sql_main = "select TOP 100 *,datediff(DAY, po_create_date,getdate()) as diff from TIPLDB..po_master_table order by po_create_date desc";
	
	$query_main = $this->db->query($sql_main);
	
}

?>
    
    <!-----Filters------>
    
    <form action="<?php echo base_url(); ?>index.php/po_reportc/" method="post" onSubmit="return validate()">
    
    <!--<div class="row">
    	<div class="col-lg-4">
        	<input type="text" name="valueToSearch" placeholder="PO Number To Search" class="form-control" autocomplete="off"><br><br>
        </div>
        <div class="col-lg-2">
        	<input type="submit" name="search" value="Search" class="form-control"><br><br>
        </div>
    </div>-->
    
    <div class="row">
    	<div class="col-lg-1"><b>From Date</b></div>
        <div class="col-lg-2"><input type="text" name="from_date" id="from_date" value="" class="form-control" autocomplete="off"></div>
        <div class="col-lg-1"><b>To Date</b></div>
        <div class="col-lg-2"><input type="text" name="to_date" id="to_date" value="" class="form-control"  autocomplete="off"></div>
        <div class="col-lg-2"><input type="submit" name="sort" id="sort" value="Sort" class="form-control"></div>
        <div class="col-lg-2">
        </div>
    </div>
    
    <!----- PO Details -------->
    
    <div class="row">
    	<div class="col-lg-12">
        	<h3 style="text-align:center">Purchase Order History</h3>
        	<table cellpadding="0" cellspacing="0" align="center" class="table table-borderd" width="100%" id="myTable">
            	<tr style="background-color:#0CF; font-weight:bold;">
                	<td>S.NO.</td>
                	<td>PO NUMBER</td>
                    <td>SUPP NAME</td>
                    <td>CATEGORY</td>
                    <td>PO CREATED BY</td>
                    <td>PO CREATE DATE</td>
                    <td>PO NEED DATE</td>
                    <td>PO L2 APPROVAL BY</td>
                    <td>PO L2 APPROVAL DATE</td>
                    <td>PO SEND TO SUPPLIER DATE</td>
                    <td>SUPPLIER EMAIL</td>
                    <td>PO VALUE</td>
                    <td>PAYTERM</td>
                    <td>FREIGHT TERM</td>
                    <td>FREIGHT TYPE</td>
                    <td>DELIVERY TYPE</td>
                    <td>PO LIVE STATUS</td>
                    <td>PO ERP STATUS</td>
                    <td>PO AGE</td>
                </tr>
                <?php
					$sno = 0;
					foreach ($query_main->result() as $row){
						$sno++;
						$po_num = $row->po_num;
						$po_supp_name = $row->po_supp_name;
						$po_create_date = substr($row->po_create_date,0,11);
						$po_total_value = $row->po_total_value;
						$status = $row->status;
						$po_age = $row->diff;
						$po_category = $row->po_category;
						$created_by= $row->created_by;
						$payterm = $row->payterm;
						$freight = $row->freight;
						$freight_type = $row->freight_type;
						$delivery_type = $row->po_deli_type;
						
						if($freight_type == 'select'){
							$freight_type = "";
						}
						
						if($delivery_type == 'select'){
							$delivery_type = "";
						}
						
						$sql = "select * from tipldb..insert_po where po_num = '$po_num'";
						$query = $this->db->query($sql);
						
						foreach ($query->result() as $row) {
						  $po_need_date = $row->po_need_date;
						  $po_approval_lvl2 = $row->po_approval_lvl2;
						  $po_approval_by   = $row->po_approvalby_lvl2;
						  $po_approvaldate_lvl2 = $row->po_approvaldate_lvl2;
						  $po_send_to_supplier_date = $row->supp_for_pur_date;
						  $supp_email = $row->po_supp_email;
						}
						
						$sql1 = "select * from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num' and pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num')";
						$query1 = $this->db->query($sql1);
						
						foreach ($query1->result() as $row) {
						  $erp_status = $row->pomas_podocstatus;
						}
						
						if($erp_status == 'MD'){
							$erp_status_def = 'MADE IN DRAFT';
						} else if($erp_status == 'SC'){
							$erp_status_def = 'SHORT CLOSED';
						} else if($erp_status == 'FR'){
							$erp_status_def = 'FRESH';
						} else if($erp_status == 'DF'){
							$erp_status_def = 'DRAFT';
						} else if($erp_status == 'DE'){
							$erp_status_def = 'DELETED';
						} else if($erp_status == 'AM'){
							$erp_status_def = 'UNDER AMENDMENT';
						} else if($erp_status == 'CL'){
							$erp_status_def = 'CLOSED';
						} else if($erp_status == 'OP'){
							$erp_status_def = 'OPEN';
						} else if($erp_status == 'NT'){
							$erp_status_def = 'NT';
						} else if($erp_status == 'RT'){
							$erp_status_def = 'RT';
						}
				?>
                <tr>
                	<td><?php echo $sno; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>index.php/po_reportc/po_details/<?php echo $po_num; ?>" target="_blank">
                        	<?php echo $po_num; ?>
                        </a>
                    </td>
                    <td><?php echo $po_supp_name; ?></td>
                    <td><?php echo $po_category; ?></td>
                    <td><?php echo $created_by; ?></td>
                    <td><?php echo substr($po_create_date,0,11); ?></td>
                    <td><?php echo substr($po_need_date,0,11); ?></td>
                    <td><?php echo $po_approval_by; ?></td>
                    <td><?php echo substr($po_approvaldate_lvl2,0,11); ?></td>
                    <td><?php echo substr($po_send_to_supplier_date,0,11); ?></td>
                    <td><?php echo $supp_email; ?></td>
                    <td><?php echo number_format($po_total_value,2); ?></td>
                    <td><?php echo $payterm; ?></td>
                    <td><?php echo $freight; ?></td>
                    <td><?php echo $freight_type; ?></td>
                    <td><?php echo $delivery_type; ?></td>
                    <td><?php echo $status; ?></td>
                    <td><?php echo $erp_status_def; ?></td>
                    <td><?php echo $po_age; ?></td>
                </tr>
                <?php
					}
				?>
            </table>
        </div>
    </div>
    
    </form>
    
  </section>
</section>

<?php include('footer.php'); ?>

<script>
$( "#to_date" ).datepicker();
$( "#from_date" ).datepicker();

function validate(){
	var from_date = document.getElementById("from_date").value;
	var to_date = document.getElementById("to_date").value;
	
	if(from_date == ''){
		alert("From Date Cannot Be Blank");
		document.getElementById("from_date").focus;
		return false;
	}
	
	if(to_date == ''){
		alert("To Date Cannot Be Blank");
		document.getElementById("to_date").focus;
		return false;
	}
	
	if(from_date > to_date){
		alert("To Date Can't be less than From Date. Please Select Another Date Range");
		return false;
	}
}
</script>
