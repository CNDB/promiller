<!--- PO Type ----->
<?php
	foreach ($view_po->result() as $row){
		$po_from_screen = $row->po_from_screen;
		$is_special = $row->is_special;
		$po_value_greater = $row->po_value_greater;
		$costing_blank = $row->costing_blank;
		$last_price_blank = $row->last_price_blank;
		$lvl3_app_req = $row->lvl3_app_req;
		$poqty_greater_prqty =  $row->poqty_greater_prqty;
		$po_num = $row->po_num;
		$po_category = $row->po_category;
		$is_preorder_pr = $row->is_preorder_pr;
		$tcd_increased = $row->tcd_increased;
		$po_type = substr($po_num,0,3);
		
	}	
	
	if($po_from_screen == 'fresh') {	
		$po_stat = "Fresh PO";	
	} else if($po_from_screen == 'disapprove_lvl1'){
		$po_stat = "Disapprove PO LVL1";
	} else if($po_from_screen == 'disapprove_lvl2'){
		$po_stat = "Disapprove PO LVL2";
	} else if($po_from_screen == 'amend_po'){
		$po_stat = "Amended PO";
	}


?>

<div class="row">
    <div class="col-lg-12">
        <h3>
			<?php 
				$reason = "<h3 style='color:red'>Reason For L3 Approval :</h3>";
				
				if($is_preorder_pr == 'Yes'){
					$reason .="<h3 style='color:red'>Purchase Order Contains Pre-Order PR.</h3>";
				}
				
				if($costing_blank == 'Yes' && $last_price_blank == 'Yes' && $lvl3_app_req == 'Yes'){
					$reason .="<h3 style='color:red'>Last Price and costing both are Not Available.</h3>";
				} 
				
				if($is_special == 'Yes' && $po_value_greater == 'Yes' && $lvl3_app_req == 'Yes'){
				   $reason .="<h3 style='color:red'>PR type is Special and PO value > 1500.</h3>";
				}
				
				if($poqty_greater_prqty == 'Yes' && $lvl3_app_req == 'Yes'){
				   $reason .="<h3 style='color:red'>PO Quantity is Greater Than PR Quantity.</h3>";
				}
				
				if($tcd_increased == 'Yes' && $lvl3_app_req == 'Yes'){
				   $reason .="<h3 style='color:red'>TCD Charges is increased as after last amendment.</h3>";
				}
				
				if($frt_amt_inc == 'Yes' && $lvl3_app_req == 'Yes'){
				   $reason .="<h3 style='color:red'>Freight Charges Increased greater than 100 after last amendment.</h3>";
				}
				
				//Value Based PO Approval
				
				$sql_app_val = "select top 1 a.lvl1_app_val, a.lvl2_app_val, a.lvl1_app_req, a.lvl2_app_req, b.po_total_value 
				from tipldb..po_approval_master_bkup a, tipldb..po_master_table b 
				where a.category = '".$po_category."' 
				and a.po_type = '".$po_type."' 
				and a.category = b.po_category
				and b.po_num  = '".$po_num."'";
				
				$qry_app_val = $this->db->query($sql_app_val);
				
				foreach ($qry_app_val->result() as $row){
					$lvl1_app_val = $row->lvl1_app_val;
					$lvl2_app_val = $row->lvl2_app_val;
					$lvl1_app_req = $row->lvl1_app_req;
					$lvl2_app_req = $row->lvl2_app_req;
					$po_total_value = $row->po_total_value;
				}
				
				if($po_total_value > $lvl1_app_val && $lvl2_app_req == 'No'){
					$reason .="<h3 style='color:red'>PO Value is greater than Level 1 Approval Limit. ".number_format($lvl1_app_val,2,".","")."</h3>";
				}
				
				if($po_total_value > $lvl2_app_val && $lvl2_app_req == 'Yes'){
					$reason .="<h3 style='color:red'>PO Value is greater than Level 2 Approval Limit. ".number_format($lvl2_app_val,2,".","")."";
				}
				
				echo $reason;
				
            ?>
        </h3>
     </div>
</div><br />

<form action="<?php echo base_url(); ?>index.php/createpodc/insert_po_sub_lvl3" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?>
                                
<!--****** SUPPLIER QUOTES AND REMARKS *******-->  
<?php include("po_supplier_quotes.php"); ?>

<?php //lock for po approval by abhinav sir ?>
<?php 
	$username = strtolower($_SESSION['username']);
	
	$sql_po_approval = "select * from tipldb..po_master_table where po_num = '$po_num_new'";
	
	$query_approval_sql = $this->db->query($sql_po_approval);
	
	foreach ($query_approval_sql->result() as $row) {
	  $approval_auth_name = $row->level2_mail_to;
	}
?>


<?php 
if(in_array($username,array('admin','priti.toshniwal','abhinav.toshniwal','rajeev.toshniwal','priyanka.vijay','manisha.agarwal')) || $username == $approval_auth_name){
?>

<div class="row">
    <div class="col-lg-2">
     <b>Approve Disapprove Comments:</b>  
    </div>
    <div class="col-lg-4"> 
     <input type="text" name="app_disapp_comnts_lvl3" id="app_disapp_comnts_lvl3" class="form-control"  value="" />  
    </div>
    <div class="col-lg-3"> 
    <input type="submit" name="po_approval_lvl3" value="Approve" id="po_approval_lvl3_app" class="form-control" 
    style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px" onclick="hide('po_approval_lvl3_app')" />     
    </div>
    <div class="col-lg-3">
    <input type="submit" name="po_approval_lvl3" value="Disapporve" id="po_approval_lvl3_disapp" class="form-control" 
    style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px" onclick="hide('po_approval_lvl3_disapp')" />    
    </div>
</div><br /><br />
<?php
	} else {
		echo "<h3 style='color:red; text-align:center'>You are not authorized to approve/disapprove this PO</h3>";
	}
?>
</form>  
<?php //chat history ?>
<?php include('po_chat_history.php'); ?>
<?php //Action Timing Report ?>
<?php include('po_action_timing.php'); ?>     
<?php include('footer.php'); ?>