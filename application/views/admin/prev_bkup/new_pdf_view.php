 <?php
ob_start();

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//Set Fonts
$pdf->SetFont('dejavusans', '', 10);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// Add a page
$pdf->AddPage();

$html = '   	<div>
                      <h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px"><u>PURCHASE ORDER</u></h4>
				</div>';

$html .= '   	<table style="font-size:8px;">
					<tr valign="top">
						<td colspan="5">';
						foreach ($view_po_pdf->result() as $row){ 
						    $po_no = $row->po_num;
							$po_date1 = substr($row->po_date,0,11);
							$po_date = date("d-m-Y", strtotime($po_date1));
							$po_amend_no = $row->po_amend_no;
							$po_type = $row->po_type;
							$supp_code = $row->po_supp_code;
							$supp_name = $row->po_supp_name;
							$comp_add = $row->po_supp_add;
							$supp_phone = $row->supp_phone;
							$supp_email = $row->po_supp_email;
							$contact_person = $row->contact_person;
							
$html .= ' 				<table width="100%" height="350px" frame="box" rules="none" style="padding:3px; border:solid 2px black;">
								<tr style="background-color:#CCC;">
									<td colspan="4" style="font-weight:bold;">PO DETAILS</td>
								</tr>
								<tr>
									<td><b>PO No.</b></td>
									<td><b>'.$po_no.'</b></td>
									<td><b>PO Date</b></td>
									<td>'.$po_date.'</td>
								</tr>
								<tr>
									<td><b>PO Amend No</b></td>
									<td>'.$po_amend_no.'</td>
									<td><b>Amendment Date</b></td>
									<td></td>
								</tr>
								<tr>
									<td><b>PO Type</b></td>
									<td>'.$po_type.'</td>
									<td></td>
									<td></td>
								</tr>
								<tr style="background-color:#CCC;">
									<td colspan="4" style="font-weight:bold;">SUPPLIER DETAILS</td>
								</tr>
								<tr>
									<td><b>Supplier Code</b></td>
									<td colspan="3">'.$supp_code.'</td>
								</tr>
								<tr>
									<td><b>Supplier Name</b></td>
									<td colspan="3"><b>'.$supp_name.'</b></td>
								</tr>
								<tr>
									<td><b>Supplier Address</b></td>
									<td colspan="3">'.$comp_add.'</td>
								</tr>
								<tr>
									<td><b>Tel No</b></td>
									<td colspan="3">'.$supp_phone.'</td>
								</tr>
								<tr>
									<td><b>Email</b></td>
									<td colspan="3">'.$supp_email.'</td>
								</tr>
								<tr>
									<td><b>Contact Person</b></td>
									<td colspan="3">'.$contact_person.'</td>
								</tr>
							</table>';
						break;}
$html .= '				</td>
						
						<td colspan="4">';
						foreach ($view_po_pdf_payterm->result() as $row){
							$pay_term           = $row->payterm;
							$pay_term_desc      = $row->payterm_desc;
							$pay_mode           = $row->pay_mode;
							$trans_mode         = $row->trans_mode;
							$partial_ship       = $row->partial_ship;
							$insurance_liablity = $row->insurance_liablity;
							$transporter_name   = $row->carrier_name;
							$ipr_no             = $row->po_ipr_no;
							$ld_applicable      = $row->ld_applicable;
							
							if($partial_ship == 'Y')
							{
								$partial_ship1 = "Yes";
							} 
							else 
							{
								$partial_ship1 = "No";
							}
							
							$price_terms1 = $row->freight;
							
							if( $price_terms1 == 'FOR' )
							{
								$price_terms = $price_terms1.",&nbsp; Ajmer";
							} 
							else if( $price_terms1 == 'FORD' )
							{
								$price_terms = $price_terms1.",&nbsp; TIPL Ajmer";
							} 
							else 
							{
								$price_terms = $price_terms1."&nbsp;";
							}
							
							$sql1 ="select * from TIPLDB..pr_submit_table where pr_num = '$ipr_no'";
							$query1 = $this->db->query($sql1);
							if ($query1->num_rows() > 0) {
							  foreach ($query1->result() as $row) {
								  $manufact_clrnce = $row->manufact_clearance;
								  $dispatch_inst = $row->dispatch_inst;
								}
							} else {
								  $manufact_clrnce = "";
								  $dispatch_inst = "";
							}
							

$html .= '					<table width="100%" height="350px" frame="box" rules="none" style="padding:3px; border:solid 2px black" >
								<tr style="background-color:#CCC;">
									<td colspan="4" style="font-weight:bold;">TERMS AND CONDITIONS</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Payterm</td>
									<td colspan="2">'.$pay_term_desc.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Pay Mode</td>
									<td colspan="2">'.$pay_mode.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Mode of transport</td>
									<td colspan="2">'.$trans_mode.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Name Of Transporter</td>
									<td colspan="2">'.$transporter_name.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Partial Shipment</td>
									<td colspan="2">'.$partial_ship1.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Price Terms</td>
									<td colspan="2">'.$price_terms.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Insurance Liablity</td>
									<td colspan="2">'.$insurance_liablity.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Manufacturing Clearance</td>
									<td colspan="2">'.$manufact_clrnce.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Dispatch Instruction</td>
									<td colspan="2">'.$dispatch_inst.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">LD Applicable</td>
									<td colspan="2">'.$ld_applicable.'</td>
								</tr>
								<tr>
									<td colspan="2" style="font-weight:bold;">Qtn. Reference</td>
									<td colspan="2"></td>
								</tr>
								<tr>
									<td colspan="2"></td>
									<td colspan="2"></td>
								</tr>	
							</table>';
						break;}
							
$html .= '				</td>
					</tr>
					<tr style="font-size:8px">
						<td colspan="9">Please supply the following in accordance with the Specifications and Terms and Conditions as per the attached sheet.
						</td>
					</tr>';
					foreach ($view_po_pdf_payterm->result() as $row){
							$po_special_supp_inst = $row->po_spcl_inst_frm_supp;
$html .= '		   <tr style="font-size:8px">
						<td colspan="9">'.$po_special_supp_inst.'
						</td>
					</tr>';
							}
$html .= '		   
					<tr>
						<td colspan="9" style="text-align:center; font-weight:bolder; font-size:12px;">ITEM DETAILS</td>
					</tr>
					<tr>
						<td colspan="9">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="9">
						
						
				<table border="1" style="padding:3px;">
							<thead>
								<tr style="font-size:8px; background-color:#CCC;">
									<th style="font-weight:bold;">S.NO.</th>
									<th colspan="2" style="font-weight:bold;">ITEM CODE</th>
									<th colspan="2" style="font-weight:bold;">DESCRIPTION</th>
									<th colspan="2" style="font-weight:bold;">NEED DATE</th>
									<th style="font-weight:bold;">RMKS</th>
									<th style="font-weight:bold;">QTY</th>
									<th style="font-weight:bold;">UOM</th>
									<th colspan="2" style="font-weight:bold;">UNIT RATE</th>
									<th colspan="2" style="font-weight:bold;">VALUE</th>
								</tr>
							</thead>';
							$po_s_no = 1;
							$total_value1 = 0;
							foreach ($view_po_pdf_item->result() as $row){
								$po_line_no = $row->po_line_no;
								$item_code =  $row->po_item_code;
								$po_ipr_no = $row->po_ipr_no;
								$need_date1 = substr($row->po_need_date,0,11);
								$need_date = date("d-m-Y", strtotime($need_date1));
								$qty = $row->for_stk_quantity;
								$uom = $row->po_uom;
								$unit_rate = $row->current_price; 
								$item_value = $row->total_item_value;
								$total_value1 = $total_value1+$item_value;
								$item_desc1 = $row->po_itm_desc;
								$item_desc2 = '';
								$item_desc = $item_desc1.' '.$item_desc2;
								$item_remarks = $row->item_remarks;
								$curr_symbol = "&#x20B9;";
								
								$sql1 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no'";
								
								$query1 = $this->db->query($sql1);
								
								if ($query1->num_rows() > 0) {
								  foreach ($query1->result() as $row) {
									  $pr_supp_remarks = $row->pr_supp_remarks;
									}
								} else {
									  $pr_supp_remarks = "";
								}
							
$html .= '	    			<tr style="font-size:8px">
								<td>'.$po_s_no.'</td>
								<td colspan="2">'.$item_code.'</td>
								<td colspan="2">'.$item_desc.'</td>
								<td colspan="2">'.$need_date.'</td>
								<td>'.$pr_supp_remarks.'</td>
								<td>'.number_format($qty,2).'</td>
								<td>'.$uom.'</td>
								<td colspan="2">'.$curr_symbol.number_format($unit_rate,2).'</td>
								<td colspan="2" style="text-align:right">'.$curr_symbol.number_format($item_value,2).'</td>
							</tr></tbody>';
							$po_s_no++; }
$html .= '					<tr style="font-size:8px">
								<td colspan="10"></td>
								<td colspan="2" ><b>TOTAL</b></td>
								<td colspan="2" style="text-align:right"><b>'.$curr_symbol.number_format($total_value1,2).'</b></td>
							</tr>
						</table>
						
			</td>
					</tr>
					<tr>
						<td colspan="9">&nbsp;</td>
					</tr>';
					//CST Tax
					$cst_tax_new = 0;
					foreach ($view_po_pdf_cst->result() as $row){
						$cst_tax = $row->podoc_tcdamount;
						$cst_desc = $row->tcdcodevariantdesc;
						$cst_desc1 = explode(":",$cst_desc);
						$cst_tax_new = $cst_tax_new + $cst_tax;
					if($cst_tax_new == '' || $cst_tax_new == '0' || $cst_tax_new == '0.00'){
						echo "&nbsp;";
					} else {
					
$html .='	 		<tr style="font-size:8px">
						<td colspan="5"></td>
						<td colspan="3"><b>'.$cst_desc1[0].'</b></td>
						<td style="text-align:right; font-weight:bold;">';
						
						
$html .= ''.$curr_symbol.number_format($cst_tax_new,2).'</td>
					</tr>';
					
$html .='		    <tr>
						<td colspan="9">&nbsp;</td>
					</tr>';
					
					} }
					
					//Freight Charges
					$freight_charges_new = 0;
					foreach ($freight_charges->result() as $row){
						$freight_charges = $row->podoc_tcdamount;
						$freight_desc = $row->tcdcodevariantdesc;
						$freight_desc1 = explode(":",$freight_desc);
						$freight_charges_new = $freight_charges_new + $freight_charges;
					}
					
					if($freight_charges_new == '' || $freight_charges_new == '0' || $freight_charges_new == '0.00'){
						echo "&nbsp;";
					} else {
					
$html .='	 		<tr style="font-size:8px">
						<td colspan="5"></td>
						<td colspan="3"><b>'.$freight_desc1[0].'</b></td>
						<td style="text-align:right; font-weight:bold;">';
						
						
$html .= 'nikk=='.$curr_symbol.number_format($freight_charges_new,2).'</td>

					</tr>';
					
$html .='		    <tr>
						<td colspan="9">&nbsp;</td>
					</tr>';
					
					}
					//EXCISE TAX
					$excise_tax_new = 0;
					foreach ($view_po_pdf_excise->result() as $row){
						$tax_inclusive_amount = $row->tax_incl_amt;
						$tax_exclusive_amount = $row->tax_excl_amt;
						$excise_tax = ($tax_inclusive_amount) - ($tax_exclusive_amount);
						$excise_tax_new = $excise_tax_new + $excise_tax;
					}
					
					if($excise_tax_new == '' || $excise_tax_new == '0' || $excise_tax_new == '0.00'){
						echo "&nbsp;";
					} else {
					 
 $html .='  		   <tr style="font-size:8px">
						<td colspan="5"></td>
						<td colspan="3"><b>TOTAL EXCISE TAX</b></td>
						<td style="text-align:right;  font-weight:bold;">';
						
$html .=''.$curr_symbol.number_format($excise_tax_new,2).'</td>
					</tr>';
					
$html .='		    <tr>
						<td colspan="9">&nbsp;</td>
					</tr>';
					 
					}
					//SERVICE TAX
					$service_tax_new = 0;
					foreach ($view_po_pdf_ser->result() as $row){
						$tax_inclusive_amount = $row->tax_incl_amt;
						$tax_exclusive_amount = $row->tax_excl_amt;
						$service_tax = ($tax_inclusive_amount) - ($tax_exclusive_amount);
						$service_tax_new = $service_tax_new + $service_tax;
					}
					
					if($service_tax_new == '' || $service_tax_new == '0' || $service_tax_new == '0.00'){
						echo "&nbsp;";
					} else {
					 
$html .='		   <tr style="font-size:8px">
						<td colspan="5"></td>
						<td colspan="3"><b>TOTAL SERVICE TAX</b></td>
						<td style="text-align:right;  font-weight:bold;">';
						
$html .=''.$curr_symbol.number_format($service_tax_new,2).'</td>
					</tr>';
					
 $html .='          <tr>
						<td colspan="9">&nbsp;</td>
					</tr>';
					
					}
					//VAT
					$vat_new = 0;
					foreach ($view_po_pdf_vat->result() as $row){
						$tax_inclusive_amount = $row->tax_incl_amt;
						$tax_exclusive_amount = $row->tax_excl_amt;
						$vat = ($tax_inclusive_amount) - ($tax_exclusive_amount);
						$vat_new = $vat_new + $vat;
					}
					
					if($vat_new == '' || $vat_new == '0' || $vat_new == '0.00'){
						echo "&nbsp;";
					} else {
					 
 $html .='		   <tr style="font-size:8px">
						<td colspan="5"></td>
						<td colspan="3"><b>TOTAL VAT</b></td>
						<td style="text-align:right;  font-weight:bold;">';
						
$html .=''.$curr_symbol.number_format($vat_new,2).'</td>

					</tr>';
					
 $html .='		   <tr>
						<td colspan="9">&nbsp;</td>
					</tr>';
					
					}
					//Calculation Of GRAND TOTAL
					$grand_total = ($total_value1+$excise_tax_new+$service_tax_new+$vat_new+$cst_tax_new+$freight_charges_new);
					
					$grand_total1 = round($grand_total);
					
					//Coverting Grand Total Into Words
					   $number = $grand_total1;
					   $no = round($number);
					   $point = round($number - $no) * 100;
					   $hundred = null;
					   $digits_1 = strlen($no);
					   $i = 0;
					   $str = array();
					   $words = array(
						'0'  => '', 
						'1'  => 'One', 
						'2'  => 'Two',
						'3'  => 'Three', 
						'4'  => 'Four', 
						'5'  => 'Five', 
						'6'  => 'Six',
						'7'  => 'Seven', 
						'8'  => 'Eight', 
						'9'  => 'Nine',
						'10' => 'Ten', 
						'11' => 'Eleven', 
						'12' => 'Twelve',
						'13' => 'Thirteen', 
						'14' => 'Fourteen',
						'15' => 'Fifteen', 
						'16' => 'Sixteen', 
						'17' => 'Seventeen',
						'18' => 'Eighteen', 
						'19' => 'Nineteen', 
						'20' => 'Twenty',
						'30' => 'Thirty', 
						'40' => 'Forty', 
						'50' => 'Fifty',
						'60' => 'Sixty', 
						'70' => 'Seventy',
						'80' => 'Eighty', 
						'90' => 'Ninety');
					   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
					   while ($i < $digits_1) {
						 $divider = ($i == 2) ? 10 : 100;
						 $number = floor($no % $divider);
						 $no = floor($no / $divider);
						 $i += ($divider == 10) ? 1 : 2;
						 if ($number) {
							$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
							$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
							$str [] = ($number < 21) ? $words[$number] .
								" " . $digits[$counter] . $plural . " " . $hundred
								:
								$words[floor($number / 10) * 10]
								. " " . $words[$number % 10] . " "
								. $digits[$counter] . $plural . " " . $hundred;
						 } else $str[] = null;
					  }
					  $str = array_reverse($str);
					  $result = implode('', $str);
					  $points = ($point) ?
						"." . $words[$point / 10] . " " . 
							  $words[$point = $point % 10] : '';
					  $result . "Rupees  " . $points . " Paise";
					  
					//Coverting Grand Total Into Words
					 
$html .= '	        <tr style="font-size:8px">
						<td colspan="2" style="font-weight:bold">AMOUNT IN WORDS : </td>
						<td colspan="3" style="text-align:left;"><b>'.$result . "Rupees  " . $points . " Paise Only".'</b></td>
						<td colspan="3"><b>GRAND TOTAL</b></td>
						<td style=" text-align:right"><b>'.$curr_symbol.number_format($grand_total,2).'</b></td>
					</tr>
					<tr>
						<td colspan="9">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="4"></td>
						<td colspan="5" style="font-weight:bolder; font-size:10px; text-align:right">FOR TOSHNIWAL INDUSTRIES PVT. LTD.</td>
					</tr>
				</table>
				<br pagebreak="true"/>';

$html .= '   	<table style="font-size:8px;" cellpadding="0" cellspacing="0">
					<tr valign="top">
						<td colspan="9" style="text-align:center; font-size:10px; font-weight:bold">TERMS & CONDITIONS APPLICABLE FOR PURCHASE ORDER</td>
					</tr><br>
					<tr>
						<td colspan="9">
							<ol style="text-align:justify">
								<li>All the disputes arising out of this contract are subject to Ajmer jurisdiction only .</li><br>
								<li>Please note that the payment term as discussed and finalised and which has been mentioned in the aforesaid PO pertains to only the basic amount of the material. Payment against the GST portion will be released only after the credit for the same gets reflected in our GSTR 2A form on the basis of your filings for GSTR 1 for the respective month, or the due date of the PO as per the terms of payment, whichever is later.</li><br>
								<li>Orders must be confirmed within ten days time from the date of release of this order, otherwise the same
				would be deemed to have been received / accepted for all the terms and conditions of this order.</li><br>
								<li>Invoice in DUPLICATE should be sent immediately after despatch of consignment / parcel & these should
				contain our Purchase Order reference.</li><br>
								<li>Kindly ensure that authenticated duplicate copy of bill, meant for transporter, is safely delivered to us, failing
				which we shall not be able to pay the excise duty amount charged by you. Please indicate in the
				letter/challan whether the same has been sent through transporter or along with documents or put in the
				parcel to enable us to locate. If the copy is sent through transporter, it should be kept in a closed envelope
				and be firmly attached with driver copy with a remark “TO BE DELIVERED TO CONSIGNEE” (IMPORTANT
				DOCUMENT) and endorsement be made in the lorry receipt.</li><br>
								<li>If the excise invoice is not found in accordance with the Central Excise Rules, we will debit the CENVAT
				amount to you and you will be liable to refund the same in full.</li><br>
								<li>Please do not change the Mode of Despatch without our consent & do not book the consignment on COD/
				FOD/ Door delivery on Sunday / Holidays/after office hours.</li><br>
								<li>Quantity more than the ordered will not be accepted and will be returned at your cost.</li><br>
								<li>All the material should be despatched in one lot only, unless otherwise scheduled .</li><br>
								<li>Date of delivery shall be the essence of this contract. If you fail to supply according to the delivery schedule
				given by us, we may at our option, either recover from you liquidated damages@1/2% per week (2% in
				case of local suppliers) and maximum to the extent of 10% of the price of the stores in default OR purchase
				from elsewhere without notice on your account and risk , the stores in default OR other of similar
				description and charge the excess price to you OR cancel the order and hold you responsible for any
				resulting loss / damage.</li><br>
								<li>In executing this order, the responsibilities for any infringement of Registered design, Trade Mark, Patent
				Rights etc. shall be yours.</li><br>
								<li>Any breakage/shortage/loss during transit should be borne by you, unless otherwise specified and agreed by us.</li><br>
								<li>The supplies should be strictly in accordance with the specifications mentioned in this order and or in
				accordance with the samples approved by us. We reserve the right to reject any goods which is in material
				or in workmanship are not as per ordered specifications , such rejections will be intimated to you and
				should be replaced by other goods of the contracted description and quality, which again shall be subject to
				our approval. All rejected goods shall, pending removal by you, remain in our godown at your risk and if the
				goods are not removed by you within 15days from the receipt of our rejection memos, we shall be entitled to
				charge godown rent and insurance charges at customary rates.</li><br>
								<li>If the goods are supplied as per our drawings/samples, such drawings/samples must be returned to us at
				the time of supply, else payment will be held up. Dies/tools/ patterns/drawings etc. lying in your possession,
				should not be used for execution of orders from any other party. Non compliance of this condition shall
				amount to breach of trust</li><br>
								<li>The rates mentioned in this order shall be firm and binding on you till the entire supplies are completed .
				However, if rates are not acceptable, despatches shall be made only after obtaining written concurrence of
				revised rates. Requests for rate revision on expiry of the of the delivery date mentioned in Purchase Order
				will not be entertained.</li><br>
							</ol>
						</td>
					</tr>
				</table>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('uploads/pdf/'.$po_no.'_po.pdf', 'FI');
?>