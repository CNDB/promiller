<?php include'header.php'; ?>

<section id="main-content">
	<section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                  <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">Rights Management</h4>
            </div>
        </div><br />
        <form action="<?php echo base_url(); ?>index.php/rightc/right_submit" method="post" onSubmit="return validate()">
        <div class="row">
        	<div class="col-lg-2">	
                 <h4>Enter Name :</h4> 
            </div>
            <div class="col-lg-4">	
                 <select name="emp_name" id="emp_name" class="form-control" onChange="show_rights(this.value);">
                 	<option value="">Select</option>
                    <?php
						foreach ($emp_list->result() as $row){
							$emp_name = $row->name;
					?>
                    <option value="<?php echo $emp_name; ?>"><?php echo $emp_name; ?></option>
                    <?php
						}
					?>
                 </select> 
            </div>
            <div class="col-lg-6"></div>
        </div><br />
        
        <div id="right_detail"></div>
        
        </form>
	</section>
</section> 

<?php include('footer.php'); ?>

<script type="text/javascript">
	$("#emp_name").select2();
</script>

<script type="text/javascript">

function show_rights(str){

	//alert(q);
	//xmlhttp = new XMLHttpRequest();
	$("#right_detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	  else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
		  
	xmlhttp.onreadystatechange=function()
	 {
		
		if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('right_detail').innerHTML=xmlhttp.responseText;
		} 
	 }
	 
	 var queryString="?q="+str;
	
	
	xmlhttp.open("GET","<?php echo base_url(); ?>index.php/rightc/right_view_details"+ queryString, true);
	xmlhttp.send();

}

</script>


<script type="text/javascript">
	function validate(){
		
		if (document.getElementById("create_pr").checked == true){
			document.getElementById("create_pr").value = "Yes";
		} else {
			document.getElementById("create_pr").value = "No";
		}
		
		if (document.getElementById("attach_drawing").checked == true){
			document.getElementById("attach_drawing").value = "Yes";
		} else {
			document.getElementById("attach_drawing").value = "No";
		}
		
		if (document.getElementById("lvl1_approval").checked == true){
			document.getElementById("lvl1_approval").value = "Yes";
		} else {
			document.getElementById("lvl1_approval").value = "No";
		}
		
		if (document.getElementById("lvl1_disapprove").checked == true){
			document.getElementById("lvl1_disapprove").value = "Yes";
		} else {
			document.getElementById("lvl1_disapprove").value = "No";
		}
		
		if (document.getElementById("lvl2_approval").checked == true){
			document.getElementById("lvl2_approval").value = "Yes";
		} else {
			document.getElementById("lvl2_approval").value = "No";
		}
		
		if (document.getElementById("lvl2_disapprove").checked == true){
			document.getElementById("lvl2_disapprove").value = "Yes";
		} else {
			document.getElementById("lvl2_disapprove").value = "No";
		}
		
		if (document.getElementById("erp_auth_pending").checked == true){
			document.getElementById("erp_auth_pending").value = "Yes";
		} else {
			document.getElementById("erp_auth_pending").value = "No";
		}
		
		if (document.getElementById("pr_po_nt_created").checked == true){
			document.getElementById("pr_po_nt_created").value = "Yes";
		} else {
			document.getElementById("pr_po_nt_created").value = "No";
		}
		
		if (document.getElementById("fresh_po").checked == true){
			document.getElementById("fresh_po").value = "Yes";
		} else {
			document.getElementById("fresh_po").value = "No";
		}
		
		if (document.getElementById("auth_po_l1").checked == true){
			document.getElementById("auth_po_l1").value = "Yes";
		} else {
			document.getElementById("auth_po_l1").value = "No";
		}
		
		if (document.getElementById("dis_po_l1").checked == true){
			document.getElementById("dis_po_l1").value = "Yes";
		} else {
			document.getElementById("dis_po_l1").value = "No";
		}
		
		if (document.getElementById("auth_po_l2").checked == true){
			document.getElementById("auth_po_l2").value = "Yes";
		} else {
			document.getElementById("auth_po_l2").value = "No";
		}
		
		if (document.getElementById("dis_po_l2").checked == true){
			document.getElementById("dis_po_l2").value = "Yes";
		} else {
			document.getElementById("dis_po_l2").value = "No";
		}
		
		if (document.getElementById("erp_auth_pend_po").checked == true){
			document.getElementById("erp_auth_pend_po").value = "Yes";
		} else {
			document.getElementById("erp_auth_pend_po").value = "No";
		}
		
		if (document.getElementById("erp_amend_po").checked == true){
			document.getElementById("erp_amend_po").value = "Yes";
		} else {
			document.getElementById("erp_amend_po").value = "No";
		}
		
		if (document.getElementById("supp_for_purchase").checked == true){
			document.getElementById("supp_for_purchase").value = "Yes";
		} else {
			document.getElementById("supp_for_purchase").value = "No";
		}
		
		if (document.getElementById("mc_planning").checked == true){
			document.getElementById("mc_planning").value = "Yes";
		} else {
			document.getElementById("mc_planning").value = "No";
		}
		
		if (document.getElementById("mc_purchase").checked == true){
			document.getElementById("mc_purchase").value = "Yes";
		} else {
			document.getElementById("mc_purchase").value = "No";
		}
		
		if (document.getElementById("acknowledgement").checked == true){
			document.getElementById("acknowledgement").value = "Yes";
		} else {
			document.getElementById("acknowledgement").value = "No";
		}
		
		if (document.getElementById("tc_upload").checked == true){
			document.getElementById("tc_upload").value = "Yes";
		} else {
			document.getElementById("tc_upload").value = "No";
		}
		
		if (document.getElementById("advance").checked == true){
			document.getElementById("advance").value = "Yes";
		} else {
			document.getElementById("advance").value = "No";
		}
		
		if (document.getElementById("pi_upload").checked == true){
			document.getElementById("pi_upload").value = "Yes";
		} else {
			document.getElementById("pi_upload").value = "No";
		}
		
		if (document.getElementById("pi_approval").checked == true){
			document.getElementById("pi_approval").value = "Yes";
		} else {
			document.getElementById("pi_approval").value = "No";
		}
		
		if (document.getElementById("pi_payment").checked == true){
			document.getElementById("pi_payment").value = "Yes";
		} else {
			document.getElementById("pi_payment").value = "No";
		}
		
		if (document.getElementById("pdc_creation").checked == true){
			document.getElementById("pdc_creation").value = "Yes";
		} else {
			document.getElementById("pdc_creation").value = "No";
		}
		
		if (document.getElementById("freight").checked == true){
			document.getElementById("freight").value = "Yes";
		} else {
			document.getElementById("freight").value = "No";
		}
		
		if (document.getElementById("road_permit").checked == true){
			document.getElementById("road_permit").value = "Yes";
		} else {
			document.getElementById("road_permit").value = "No";
		}
		
		if (document.getElementById("delivery_detail").checked == true){
			document.getElementById("delivery_detail").value = "Yes";
		} else {
			document.getElementById("delivery_detail").value = "No";
		}
		
		if (document.getElementById("gate_entry").checked == true){
			document.getElementById("gate_entry").value = "Yes";
		} else {
			document.getElementById("gate_entry").value = "No";
		}	
	}
</script>
<script>
function check_all_pr(){
	
	if(document.getElementById("pr").checked == true){
		document.getElementById("create_pr").checked = true;
		document.getElementById("attach_drawing").checked = true;
		document.getElementById("lvl1_approval").checked = true;
		document.getElementById("lvl1_disapprove").checked = true;
		document.getElementById("lvl2_approval").checked = true;
		document.getElementById("lvl2_disapprove").checked = true;
		document.getElementById("erp_auth_pending").checked = true;
		document.getElementById("pr_po_nt_created").checked = true;
	} else {
		document.getElementById("create_pr").checked = false;
		document.getElementById("attach_drawing").checked = false;
		document.getElementById("lvl1_approval").checked = false;
		document.getElementById("lvl1_disapprove").checked = false;
		document.getElementById("lvl2_approval").checked = false;
		document.getElementById("lvl2_disapprove").checked = false;
		document.getElementById("erp_auth_pending").checked = false;
		document.getElementById("pr_po_nt_created").checked = false;
	}
	
}

function check_all_po(){
	
	if(document.getElementById("po").checked == true){
		
		document.getElementById("fresh_po").checked = true;
		document.getElementById("auth_po_l1").checked = true;
		document.getElementById("dis_po_l1").checked = true;
		document.getElementById("auth_po_l2").checked = true;
		document.getElementById("dis_po_l2").checked = true;
		document.getElementById("erp_auth_pend_po").checked = true;
		document.getElementById("erp_amend_po").checked = true;
		document.getElementById("supp_for_purchase").checked = true;
		document.getElementById("mc_planning").checked = true;
		document.getElementById("mc_purchase").checked = true;
		document.getElementById("acknowledgement").checked = true;
		document.getElementById("tc_upload").checked = true;
		document.getElementById("advance").checked = true;
		document.getElementById("pi_upload").checked = true;
		document.getElementById("pi_approval").checked = true;
		document.getElementById("pi_payment").checked = true;
		document.getElementById("pdc_creation").checked = true;
		document.getElementById("freight").checked = true;
		document.getElementById("road_permit").checked = true;
		document.getElementById("delivery_detail").checked = true;
		document.getElementById("gate_entry").checked = true;
		
	} else {
		
		document.getElementById("fresh_po").checked = false;
		document.getElementById("auth_po_l1").checked = false;
		document.getElementById("dis_po_l1").checked = false;
		document.getElementById("auth_po_l2").checked = false;
		document.getElementById("dis_po_l2").checked = false;
		document.getElementById("erp_auth_pend_po").checked = false;
		document.getElementById("erp_amend_po").checked = false;
		document.getElementById("supp_for_purchase").checked = false;
		document.getElementById("mc_planning").checked = false;
		document.getElementById("mc_purchase").checked = false;
		document.getElementById("acknowledgement").checked = false;
		document.getElementById("tc_upload").checked = false;
		document.getElementById("advance").checked = false;
		document.getElementById("pi_upload").checked = false;
		document.getElementById("pi_approval").checked = false;
		document.getElementById("pi_payment").checked = false;
		document.getElementById("pdc_creation").checked = false;
		document.getElementById("freight").checked = false;
		document.getElementById("road_permit").checked = false;
		document.getElementById("delivery_detail").checked = false;
		document.getElementById("gate_entry").checked = false;
	}
	
}
</script>
