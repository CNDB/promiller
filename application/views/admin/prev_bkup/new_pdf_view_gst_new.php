<?php
ob_start();

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//Set Fonts
$pdf->SetFont('dejavusans', '', 10);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// Add a page
$pdf->AddPage();

//PO STATUS PRINT
$x = 3;
$y = 11;
$w = 15;
$h = 15;

foreach($po_status->result() as $row){
	$pomas_podocstatus = $row->pomas_podocstatus;
	
	if($pomas_podocstatus == 'FR' || $pomas_podocstatus == 'DF' || $pomas_podocstatus == 'MD'){
		$pdf->Image('assets/admin/img/fresh.png', $x, $y, $w, $h, 'PNG', '', '', false, 0, '', false, false, 0, $fitbox, false, false);
	} else if($pomas_podocstatus == 'DE'){
		$pdf->Image('assets/admin/img/deleted.png', $x, $y, $w, $h, 'PNG', '', '', false, 0, '', false, false, 0, $fitbox, false, false);
	} else if($pomas_podocstatus == 'OP'){
		$pdf->Image('assets/admin/img/authorized.png', $x, $y, $w, $h, 'PNG', '', '', false, 0, '', false, false, 0, $fitbox, false, false);
	} else if($pomas_podocstatus == 'AM'){
		$pdf->Image('assets/admin/img/amended.png', $x, $y, $w, $h, 'PNG', '', '', false, 0, '', false, false, 0, $fitbox, false, false);
	} else if($pomas_podocstatus == 'CL' || $pomas_podocstatus == 'NT' || $pomas_podocstatus == 'RT' || $pomas_podocstatus == 'SC' ){
		$pdf->Image('assets/admin/img/closed.png', $x, $y, $w, $h, 'PNG', '', '', false, 0, '', false, false, 0, $fitbox, false, false);
	}
}

$html = '<div>
		   <h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px"><u>PURCHASE ORDER</u></h4>
		   <img src='.$wm_path.' widht="" height="">
	     </div>'; 
		 
$html .='<table width="100%" cellpadding="4px" cellspacing="0" style="font-size:6px;">
			<tr style="background-color:#CCC;">
				<td width="33.33%" style="border:solid 1px black;"><b>VENDOR DETAILS</b></td>
				<td width="33.33%" style="border:solid 1px black;"><b>PO DETAILS</b></td>
				<td width="33.33%" style="border:solid 1px black;"><b>BUYER DETAILS</b></td>
			</tr>
            <tr>
                <td width="33.33%" style="border:solid 1px black; padding: 2px;">';
				
                foreach ($view_po_pdf->result() as $row){
							$po_no = $row->pomas_pono;
							$supp_name = $row->supp_spmn_supname;
							$supp_add1 = $row->supp_addr_address1;
							$supp_add2 = $row->supp_addr_address2;
							$supp_add3 = $row->supp_addr_address3;
							$comp_add1 = $supp_add1."&nbsp;".$supp_add2."&nbsp;".$supp_add3;
							$comp_add = mb_convert_encoding($comp_add1, "ISO-8859-1", "UTF-8");
							$supp_addr_city = $row->supp_addr_city;
							$supp_addr_state = $row->supp_addr_statedesc;
							$supp_addr_zip = $row->supp_addr_zip;
							$supp_spmn_supcode = $row->supp_spmn_supcode;
							$po_supp_email = $row->supp_addr_email;
							$supp_addr_contperson = $row->supp_addr_contperson;
							$supplier_phone = $row->supp_addr_phone;
							
							//mail id is not avalable in erp
							if($po_supp_email == '' || is_null($po_supp_email) == TRUE ){
								$sql_supp_live_det = "select top 1 * from TIPLDB..insert_po where po_num = '$po_no'";
								$query_supp_live_det = $this->db->query($sql_supp_live_det);
								
								foreach ($query_supp_live_det->result() as $row) {
								  $po_supp_email = $row->po_supp_name;
								}
							}
							
							if($supp_addr_contperson == '' || is_null($supp_addr_contperson) == TRUE ){
								$sql_supp_live_det = "select top 1 * from TIPLDB..insert_po where po_num = '$po_no'";
								$query_supp_live_det = $this->db->query($sql_supp_live_det);
								
								foreach ($query_supp_live_det->result() as $row) {
								  $supp_addr_contperson = $row->contact_person;
								}
							}
							//mail id is not avalable in erp
							
							$sql_gst_no ="select * from scmdb..supp_tax_details where supp_tax_type = 'EXCISE' and supp_tax_supcode = '$supp_spmn_supcode'";
							$query_gst_no = $this->db->query($sql_gst_no);
							
							foreach ($query_gst_no->result() as $row) {
							  $vendor_gst_no = $row->supp_tax_regdno;
							}
							
 $html .= '          <table widht="100%" height="150px">
                        <tr height="10px">
                            <td width="50%"><b>VENDOR NAME:</b></td>
                            <td width="50%">'.$supp_name.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>ADDRESS:</b></td>
                            <td width="50%">'.$comp_add.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="100%" colspan="2"><b>CITY:</b>&nbsp;'.$supp_addr_city.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>STATE:&nbsp;</b>'.$supp_addr_state.'</td>
                            <td width="50%"><b>PIN:&nbsp;</b>'.$supp_addr_zip.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>VENDOR CODE:</b></td>
                            <td width="50%">'.$supp_spmn_supcode.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>EMAIL:</b></td>
                            <td width="50%">'.$po_supp_email.'</td>
                        </tr >
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>CONTACT PERSON:</b></td>
                            <td width="50%">'.$supp_addr_contperson.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
						<tr height="10px">
                            <td width="50%"><b>PHONE:</b></td>
                            <td width="50%">'.$supplier_phone.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>VENDOR GST NO:</b></td>
                            <td width="50%" style="text-transform:uppercase;">'.$vendor_gst_no.'</td>
                        </tr>
                    </table>';
                break;
				}
				
$html .= '      </td>';

$html .= '        <td width="33.33%" style="border:solid 1px black; padding: 2px;">';
				
					foreach ($view_po_pdf->result() as $row){ 
						    $po_num = $row->pomas_pono;
							$po_type = $row->pomas_potype;
							$po_amend_no = $row->pomas_poamendmentno;
							$po_date = $row->pomas_podate;
							$po_freight = $row->paytm_incoterm;
							
							if($po_freight == 'FORD'){
								$po_freight_new = $po_freight." ,TIPL Ajmer";
							} else if($po_freight == 'FOR'){
								$po_freight_new = $po_freight." ,Ajmer";
							} else if($po_freight == 'EXW'){
								$po_freight_new = $po_freight." (EX-WORKS)";
							} else {
								$po_freight_new = $po_freight;
							}
							
							$po_payterm = $row->paytm_payterm;
							$paytem_desc = $row->pt_description;
							//$po_created_by = $row->pomas_createdby;
				
$html .= '      	<table width="100%" height="150px" >
                        <tr height="10px">
                            <td width="50%"><b>PO TYPE:</b></td>
                            <td width="50%">'.$po_type.'</td>
                        </tr>
						<tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>PO NO:</b></td>
                            <td width="50%">'.$po_num.'</td>
                        </tr>
						<tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>PO REV/AMEND:</b></td>
                            <td width="50%">'.$po_amend_no.'</td>
                        </tr>
						<tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>DATE:</b></td>
                            <td width="50%">'.substr($po_date,0,11).'</td>
                        </tr>						 
						<tr height="5px">
                            <td colspan="2"><br><hr /><br></td>
                        </tr>
                        <tr rowspan="6" height="60px">
                            <td colspan="2" width="100%"><p style="text-align:justify;">We are pleased to place our firm order on the terms & conditions stipulated here under. Please send your order Acceptance immediately after receipt of this order. In case of any discrepancy about PO terms and conditions, refer to us within 7 days. Otherwise PO will be treated as accepted by you.</p>			
                            </td>
                        </tr>
                        <tr height="5px">
                            <td colspan="2"><br><hr /><br></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>TIPL GST NO.</b></td>
                            <td width="50%">08AAACT1582B1ZO</td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>';

$html .= '      <td width="33.33%" style="border:solid 1px black; padding: 2px;">';
				
                foreach ($view_po_pdf->result() as $row){ 
							$buyer_name_orginal = $row->pomas_createdby;
						    $buyer_name = str_replace("."," ",$row->pomas_createdby);
							
							$po_first_three = substr($po_num,0,3);
							
							$sql_mobile ="select * from TIPLDB..login where email like'%$buyer_name_orginal%'";
							$query_mobile = $this->db->query($sql_mobile);
							
							foreach ($query_mobile->result() as $row) {
							  $buyer_email_address = $row->email;
							  $buyer_mobile_no = $row->mobile_no;
							  //$buyer_name  = $row->name;
							}
							
							//Currency
							if($po_first_three == 'FPO'){
								$sql_currency ="select * from tipldb..po_master_table where po_num = '$po_num'";
								$query_currency = $this->db->query($sql_currency);
								
								foreach ($query_currency->result() as $row){
									$currency = $row->currency;
								}
							} else {	
								$currency = 'INR';	
							}
							
							//Need Date
							$sql_need_date ="select min(poitm_needdate) as min_need_date from scmdb..po_poitm_item_detail where poitm_pono = '$po_num'";
							$query_need_date = $this->db->query($sql_need_date);
							
							foreach ($query_need_date->result() as $row) {
							  $need_date = substr($row->min_need_date,0,11);
							  //$need_date = date("d-m-Y", strtotime($need_date1));
							}
							
$html .= '         <table widht="100%" height="150px" >
                        <tr height="10px">
                            <td width="40%"><b>BUYER NAME:</b></td>
                            <td width="60%" style=" text-transform:uppercase">'.strtoupper($buyer_name).'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>MOBILE NO:</b></td>
                            <td width="60%">'.$buyer_mobile_no.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>EMAIL:</b></td>
                            <td width="60%">'.$buyer_email_address.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
						<tr height="10px">
                            <td width="40%"><b>CURRENCY:</b></td>
                            <td width="60%">'.$currency.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>NEED DATE:</b></td>
                            <td width="60%">'.$need_date.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>
            </tr>
			</table>';
			
			//CGST Tax
			$sql_check_cgst = "select * from scmdb..tcal_tax_dtl where tran_no = '$po_num' and code_type = 'CGST'";
			
			$query_check_cgst = $this->db->query($sql_check_cgst);
			
			if ($query_check_cgst->num_rows() > 0){	
				$cgst_td = 1;
				$tot_cols = 13;
				$tot_cols1 = 14;
			}
			
			//IGST Tax
			$sql_check_igst = "select * from scmdb..tcal_tax_dtl where tran_no = '$po_num' and code_type = 'IGST'";
			
			$query_check_igst = $this->db->query($sql_check_igst);
			
			if ($query_check_igst->num_rows() > 0){	
				$igst_td = 1;	
				$tot_cols = 12;
				$tot_cols1 = 13;
			}
			
			//No Taxes
			if($query_check_cgst->num_rows() <= 0 && $query_check_igst->num_rows() <= 0){
				$cgst_td = 0;
				$igst_td = 0;
				$tot_cols = 11;
				$tot_cols1 = 12;
			}
			
$html .= '  <div>
		   		<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">ITEM DETAILS</h4>
	     	</div>';
            
$html .= ' <table width="100%" height="auto" cellpadding="1px" cellspacing="0" style="font-size:5px;">         
                        <tr style="background-color:#CCC">
                            <td style="border:solid 1px black; padding: 2px;"><b>SN</b></td>
                            <td style="border:solid 1px black; padding: 2px;" colspan="4"><b>ITEM CODE & DESCRIPTION</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>HSN</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>QTY</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>RATE</b></td>
                            <td style="border:solid 1px black; padding: 2px;"><b>UOM</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>VALUE BASIC</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>DISCOUNT %</b></td>';
							
							if($cgst_td == 1){
							
				  $html .= '<td style="border:solid 1px black; padding: 2px; text-align:center"><b>CGST %</b></td>
							<td style="border:solid 1px black; padding: 2px; text-align:center"><b>SGST/UTGST %</b></td>';
							
							}
							
							if($igst_td == 1){
							
				  $html .= '<td style="border:solid 1px black; padding: 2px; text-align:center"><b>IGST %</b></td>';
				  
							}
				  
				  $html .= '<td style="border:solid 1px black; padding: 2px;"><b>TOTAL (INCL. TAX)</b></td>
                        </tr>';
						
                        	$po_s_no = 1;
							$total_value1 = 0;
							foreach ($view_po_pdf_item->result() as $row){
								$po_num = $row->pomas_pono;
								$pomas_podate = $row->pomas_podate;
								$po_line_no = $row->poitm_polineno;
								$item_code =  $row->poitm_itemcode;
								$need_date1 = substr($row->poitm_needdate,0,11);
								$need_date = date("d-m-Y", strtotime($need_date1));
								$qty = $row->poitm_order_quantity;
								$uom = $row->poitm_puom;
								$unit_rate = $row->poitm_po_cost; 
								$item_value = $row->poitm_itemvalue;
								
								$charges=0;
								$discount=0;
								//Charges
								$sql_charges = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
								where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
								and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode != 'PDISCOUNTS'
								and poitm_poamendmentno = (select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono = '$po_num')
								and poitm_poamendmentno = potcd_poamendmentno";

								$query_charges = $this->db->query($sql_charges);
								
								$charges_tot = 0;
								foreach ($query_charges->result() as $row) {
								  	$charges = $row->potcd_tcdamount;
									$charges_tot = $charges_tot+$charges;
								}
								
								//Discounts
								$sql_discounts = "select * from scmdb..po_poitm_item_detail a, scmdb..po_itemtcd_vw b 
								where a.poitm_pono = b.potcd_pono and a.poitm_polineno = b.potcd_polineno 
								and a.poitm_polineno = '$po_line_no' and a.poitm_pono = '$po_num' and b.potcd_tcdcode = 'PDISCOUNTS'
								and poitm_poamendmentno = (select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono = '$po_num')
								and poitm_poamendmentno = potcd_poamendmentno";

								$query_discounts = $this->db->query($sql_discounts);
								
								$discounts_tot = 0;
								foreach ($query_discounts->result() as $row) {
								  	$discounts = $row->potcd_tcdamount;
									$discounts_tot = $discounts_tot+$discounts;
								}
								
								$discount_percent = ($discounts_tot*100)/$item_value;
								
								$sql_item_desc ="select loi_itemdesc,lov_matlspecification from scmdb..itm_loi_itemhdr a, 
								scmdb..itm_lov_varianthdr b 
								where a.loi_itemcode = b.lov_itemcode
								and a.loi_itemcode =  '$item_code'";
								$query_item_desc = $this->db->query($sql_item_desc);
								
								foreach ($query_item_desc->result() as $row) {
								  	$item_desc1 = $row->loi_itemdesc;
									$item_desc2 = $row->lov_matlspecification;
									
									$item_desc = $item_desc1."<br><br>".$item_desc2;
									
									$item_desc = str_replace("'","",$item_desc);
									$item_desc = mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");
									
									//$item_desc = strip_tags($item_desc);
								}
								
								$sql_hsnsac_code = "select * from scmdb..trd_tax_group_dtl 
								where item_code='$item_code' and tax_group_code like'%GST%' and effective_from_date < '$pomas_podate'";
								$query_hsnsac_code = $this->db->query($sql_hsnsac_code);
								
								if ($query_hsnsac_code->num_rows() > 0) {
									foreach ($query_hsnsac_code->result() as $row) {
										$item_hsnsac_code = $row->commoditycode;
									}
								} else {
									$item_hsnsac_code = "";
								}
								
								//CGST
								$sql_cgst = "select * from scmdb..po_poitm_item_detail a, scmdb..tcal_tax_dtl b 
								where a.poitm_pono =b.tran_no 
								and a.poitm_polineno = b.tran_line_no
								and a.poitm_poamendmentno = (select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono = a.poitm_pono)
								and a.poitm_polineno = '$po_line_no'
								and a.poitm_pono = '$po_num'
								and b.code_type = 'CGST'";
								$query_cgst = $this->db->query($sql_cgst);
								
								if ($query_cgst->num_rows() > 0) {
									foreach ($query_cgst->result() as $row) {
										$tax_rate_cgst = $row->tax_rate;
										$tax_amt_cgst = $row->corr_tax_amt;
									}
								} else {
									$tax_rate_cgst = 0.00;
									$tax_amt_cgst = 0.00;
								}
								
								//SGST
								$sql_sgst = "select * from scmdb..po_poitm_item_detail a, scmdb..tcal_tax_dtl b 
								where a.poitm_pono =b.tran_no 
								and a.poitm_polineno = b.tran_line_no
								and a.poitm_poamendmentno = (select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono = a.poitm_pono)
								and a.poitm_polineno = '$po_line_no'
								and a.poitm_pono = '$po_num'
								and b.code_type in('SGST','UTGST')";
								$query_sgst = $this->db->query($sql_sgst);
								
								if ($query_sgst->num_rows() > 0) {
									foreach ($query_sgst->result() as $row) {
										$tax_rate_sgst = $row->tax_rate;
										$tax_amt_sgst = $row->corr_tax_amt;
									}
								} else {
									$tax_rate_sgst = 0.00;
									$tax_amt_sgst = 0.00;
								}
								
								//IGST
								$sql_igst = "select * from scmdb..po_poitm_item_detail a, scmdb..tcal_tax_dtl b 
								where a.poitm_pono =b.tran_no 
								and a.poitm_polineno = b.tran_line_no
								and a.poitm_poamendmentno = (select max(poitm_poamendmentno) from scmdb..po_poitm_item_detail where poitm_pono = a.poitm_pono)
								and a.poitm_polineno = '$po_line_no'
								and a.poitm_pono = '$po_num'
								and b.code_type = 'IGST'";
								$query_igst = $this->db->query($sql_igst);
								
								if ($query_igst->num_rows() > 0) {
									foreach ($query_igst->result() as $row) {
										$tax_rate_igst = $row->tax_rate;
										//$tax_amt_igst = $row->comp_tax_amt;
										$tax_amt_igst = $row->corr_tax_amt;
									}
								} else {
									$tax_rate_igst = 0.00;
									$tax_amt_igst = 0.00;
								}
								
								$total_amount = $item_value+$charges-$discounts+$tax_amt_cgst+$tax_amt_sgst+$tax_amt_igst;
								
								$total_basic_value = $total_basic_value+$item_value;
								
								$total_charges = $total_charges+$charges_tot;
								
								$total_discount = $total_discount+$discounts_tot;
								
								$total_cgst_tax = $total_cgst_tax+$tax_amt_cgst;
								
								$total_sgst_tax = $total_sgst_tax+$tax_amt_sgst;
								
								$total_igst_tax = $total_igst_tax+$tax_amt_igst;
								
								$total_value_with_taxes = $total_basic_value+$total_charges+$total_cgst_tax+$total_sgst_tax+$total_igst_tax-$total_discount;
								
								$taxes_total = $total_cgst_tax+$total_sgst_tax+$total_igst_tax;
								
								//End Of Tax & Total Calculation
								
								if($po_first_three == 'FPO'){
									$sql_currency ="select * from tipldb..po_master_table where po_num = '$po_num'";
									$query_currency = $this->db->query($sql_currency);
									
									foreach ($query_currency->result() as $row){
										$currency = $row->currency;
									}
								} else {	
									$currency = 'INR';	
								}
						
$html .= '              <tr>
                            <td style="border:solid 1px black; padding: 2px;">'.$po_s_no.'</td>
                            <td style="border:solid 1px black; padding: 2px; font-size:6px;" colspan="4"><b>'.$item_code.'</b><br><br>'.$item_desc.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.$item_hsnsac_code.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($qty,2,'.','').'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($unit_rate,2,'.','').'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.$uom.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($item_value,2,'.','').'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.number_format($discount_percent,2,'.','').'</td>';
							
							if($cgst_td == 1){
							
							$html .= '<td style="border:solid 1px black; padding: 2px;">'.number_format($tax_rate_cgst,2,'.','').'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($tax_rate_sgst,2,'.','').'</td>';
							
							}
							if($igst_td == 1){
							
                            $html .= '<td style="border:solid 1px black; padding: 2px;">'.number_format($tax_rate_igst,2,'.','').'</td>';
							
							}
							
                            $html .= '<td style="border:solid 1px black; padding: 2px;">'.number_format($total_amount,2,'.','').'</td>
                        </tr>';
							$po_s_no++;}
							
$html .= '              <tr>
                            <td colspan="9" style="border:solid 1px black; padding:2px;"><b>TOTAL</b></td>
							<td style="border:solid 1px black; padding:2px;"><b>'.number_format($total_basic_value,2,'.','').'</b></td>
							<td style="border:solid 1px black; padding:2px;"><b></b></td>';
							
							if($cgst_td == 1){
							
							$html .= '<td style="border:solid 1px black; padding:2px; text-align:right">
								<b></b>
							</td>
							<td style="border:solid 1px black; padding:2px; text-align:right">
								<b></b>
							</td>';
							
							}
							if($igst_td == 1){
							
							$html .= '<td style="border:solid 1px black; padding:2px; text-align:right">
								<b></b>
							</td>';
							
							}
							
                            $html .= '<td colspan="1" style="border:solid 1px black; padding:2px;"><b>'.number_format($total_value_with_taxes,2,'.','').'</b></td>
                        </tr>';
						
$html .= '              <tr>
                            <td colspan="'.$tot_cols.'" style="border:solid 1px black; padding: 2px;">
								<b>BASIC VALUE + LINE LEVEL TCD + STATUTORY VALUE SUMMARY</b>
							</td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($total_value_with_taxes,2, '.', '').'</td>
                        </tr>';
						
						//Charges Document Level
						$tcd_amount_ch_tot = 0;
						foreach ($doc_lvl_charges->result() as $row){
							$tcd_amount_ch = $row->tcdamount;
							$tcd_desc_ch   = $row->tcdcodedesc;
							
							$tcd_amount_ch_tot = $tcd_amount_ch_tot+$tcd_amount_ch;
						
$html .= '              <tr>
                            <td colspan="'.$tot_cols.'" style="border:solid 1px black; padding: 2px;"><b>'.$tcd_desc_ch.'</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;"><b>'.number_format($tcd_amount_ch,2,'.','').'</b></td>
                        </tr>';
						}
						
						//Discounts Document Level
						$tcd_amount_dis_tot = 0;
						foreach ($doc_lvl_discount->result() as $row){
							$tcd_amount_dis = $row->tcdamount;
							$tcd_variant_dis = $row->tcdvariant;
							$tcd_desc_dis   = $row->tcdcodedesc;
							$tcd_amount_dis_tot = $tcd_amount_dis_tot+$tcd_amount_dis;
							
							if($tcd_variant_dis == ''){
								$tcd_variant_dis_new = '';
							} else {
								$tcd_variant_dis_new = '&nbsp;@'.$tcd_variant_dis."%";
							}
						
$html .= '              <tr>
                            <td colspan="'.$tot_cols.'" style="border:solid 1px black; padding: 2px;"><b>'.$tcd_desc_dis.$tcd_variant_dis_new.'</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;"><b>'.number_format($tcd_amount_dis_tot,2,'.','').'</b></td>
                        </tr>';
						}
						
$html .= '              <tr>
                            <td colspan="'.$tot_cols.'" style="border:solid 1px black; padding: 2px;"><b>TOTAL TAXES</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($taxes_total,2,'.','').'</td>
                        </tr>';
						
						

						//Calculation Of GRAND TOTAL
						$grand_total = ($total_value_with_taxes+$tcd_amount_ch_tot-$tcd_amount_dis_tot);
						$grand_total1 = round($grand_total);
						
$html .= '              <tr>
                            <td colspan="'.$tot_cols.'" style="border:solid 1px black; padding: 2px;"><b>GRAND TOTAL</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;"><b>'.number_format($grand_total1,2,'.','').'</b></td>
                        </tr>';
						
						//Coverting Grand Total Into Words
						   $number = $grand_total1;
						   $no = round($number,2);
						   $point = round($number - $no) * 100;
						   $hundred = null;
						   $digits_1 = strlen($no);
						   $i = 0;
						   $str = array();
						   $words = array(
							'0'  => '', 
							'1'  => 'One', 
							'2'  => 'Two',
							'3'  => 'Three', 
							'4'  => 'Four', 
							'5'  => 'Five', 
							'6'  => 'Six',
							'7'  => 'Seven', 
							'8'  => 'Eight', 
							'9'  => 'Nine',
							'10' => 'Ten', 
							'11' => 'Eleven', 
							'12' => 'Twelve',
							'13' => 'Thirteen', 
							'14' => 'Fourteen',
							'15' => 'Fifteen', 
							'16' => 'Sixteen', 
							'17' => 'Seventeen',
							'18' => 'Eighteen', 
							'19' => 'Nineteen', 
							'20' => 'Twenty',
							'30' => 'Thirty', 
							'40' => 'Forty', 
							'50' => 'Fifty',
							'60' => 'Sixty', 
							'70' => 'Seventy',
							'80' => 'Eighty', 
							'90' => 'Ninety');
						   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
						   while ($i < $digits_1) {
							 $divider = ($i == 2) ? 10 : 100;
							 $number = floor($no % $divider);
							 $no = floor($no / $divider);
							 $i += ($divider == 10) ? 1 : 2;
							 if ($number) {
								$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
								$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
								$str [] = ($number < 21) ? $words[$number] .
									" " . $digits[$counter] . $plural . " " . $hundred
									:
									$words[floor($number / 10) * 10]
									. " " . $words[$number % 10] . " "
									. $digits[$counter] . $plural . " " . $hundred;
							 } else $str[] = null;
						  }
						  $str = array_reverse($str);
						  $result = implode('', $str);
						  $points = ($point) ?
							"." . $words[$point / 10] . " " . 
								  $words[$point = $point % 10] : '';
						  $result . "Rupees  " . $points . " Paise";
						  
						//Coverting Grand Total Into Words
						
$html .= '              <tr>
                            <td colspan="'.$tot_cols1.'" style="border:solid 1px black; padding: 2px;">
								<b>AMOUNT IN WORDS : '.$result ." ".$points." Only".'</b>
							</td>
                        </tr>';

$html .= '			</table><br>';

$html .='			<div style="font-size:10px; width:100%; margin-left:10px">
						<b>For Toshniwal Industries Pvt. Ltd.</b>
					</div>';
					
					foreach ($doc_lvl_notes->result() as $row){
						$doc_notes = $row->doc_notes;
						
						$doc_notes = mb_convert_encoding($doc_notes, "ISO-8859-1", "UTF-8");
						
$html .= '   	   <div style="font-size:6px">
						<b>Document Level Notes: </b>
						<p>'.$doc_notes.'</p>
					</div>';
					}
$html .= '		   <br pagebreak="true"/>
   	                <div>
		   				<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">TERMS AND CONDITIONS</h4>
	     			</div>
					
					<table width="100%" height="auto" style="font-size:7px;" cellpadding="1px" cellspacing="1">
                        <tr>
                            <td width="5%">1.</td>
                            <td width="20%" colspan="2"><b>GST</b></td>
                            <td width="75%" colspan="9">As applicable</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">2.</td>
                            <td width="20%" colspan="2"><b>LD</b></td>
                            <td width="75%" colspan="9">In the event of delay in agreed contractual delivery as per the Purchase Order, penalty @ 0.50 % (half
percent) per week or part thereof but limited to a maximum of 10% (Ten Percent) of the value of undelivered
portion (basic material cost) will be applicable. Delivery will commence from the date of document
approval by TIPL or date of issue of manufacturing clearance, whichever is later.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">3.</td>
                            <td width="20%" colspan="2"><b>PACKING & FORWARDING</b></td>
                            <td width="75%" colspan="9">Inclusive</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">4.</td>
                            <td width="20%" colspan="2"><b>FREIGHT TERMS</b></td>
                            <td width="75%" colspan="9">'.$po_freight_new.'</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">5.</td>
                            <td width="20%" colspan="2"><b>AUTHORISED TRANSPORTER</b></td>
                            <td width="75%" colspan="9">If freight term is Ex-works or To Pay then please take our prior approval regarding transporter name before dispatch of material. In case if the same is not followed, the supplier has to pay the freight charges</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">6.</td>
                            <td width="20%" colspan="2"><b>DISPATCH INSTRUCTIONS & EWAY BILL</b></td>
                            <td width="75%" colspan="9">Dispatch Instructions or Eway Bill will be issued only after submission of Test Certificate and its
subsequent approval.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">7.</td>
                            <td width="20%" colspan="2"><b>PAYMENT TERMS</b></td>
                            <td width="10%">1.</td>
                            <td width="65%" colspan="8">'.$po_payterm.' ('.$paytem_desc.')</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%">2.</td>
                            <td width="65%" colspan="8">Without the below said documents MRN shall not be raised in our portal for payment.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%"></td>
                            <td width="5%">(i)</td>
                            <td width="25%" colspan="3">Consigee LR (Original + 1 Copy)</td>
                            <td width="5%">(ii)</td>
                            <td width="30%" colspan="3">Eway Bill (if applicable)</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%"></td>
                            <td width="5%">(iii)</td>
                            <td width="25%" colspan="3">Duplicate for Transport Invoice</td>
                            <td width="5%">(iv)</td>
                            <td width="30%" colspan="3">Original for Buyer Invoice Copy</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%"></td>
                            <td width="5%">(v)</td>
                            <td width="25%" colspan="3">Original Test Certificate</td>
                            <td width="5%"></td>
                            <td width="30%" colspan="3"></td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%">3.</td>
                            <td width="65%" colspan="8">In case, if any of the above documents are not received, your payment cycle will start only after / from the date of receiving of all the documents in our premises.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>';
						
						//Manufacturing Clearance Clause Added As per Neeraj Sir Mail On 05-11-18
						foreach ($mc_clause->result() as $row){
							$mc_count = $row->count1;
							
							if($mc_count > 0){
						
$html .= '			   <tr>
                            <td width="5%">8.</td>
                            <td width="20%" colspan="2" style="color:red"><b>MANUFACTURING CLEARANCE</b></td>
                            <td width="75%" colspan="9">Vendor/ Supplier has to furnish the required Document /Drawing for Manufacturing Clearance asked by the purchaser after which purchaser will give manufacturing clearance. Without Manufacturing Clearance, the purchaser will not be liable for any losses or change in the requirement of the material.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>';
						
							}
						}
						
						if($mc_count > 0){
						//Dispatch Instruction Clause Added As per Neeraj Sir Mail On 05-11-18
						foreach ($di_clause->result() as $row){
							$di_count = $row->count1;
							
							if($di_count > 0){
						
$html .= '			   <tr>
                            <td width="5%">9.</td>
                            <td width="20%" colspan="2"><b style="color:red">DISPATCH INSTRUCTION</b></td>
                            <td width="75%" colspan="9">Without Dispatch Instruction Vendor/ Supplier should not send the material to the Purchaser. If the Supplier /Vendor is sending any material to the purchaser without Dispatch Instruction then, the purchaser has full rights to reject the material or send back the material on To Pay basis. Purchaser will not be liable for any delay payments or any loss if material sent without Dispatch Instruction.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>';
						
							}
						}
						} else {
						
						//Dispatch Instruction Clause Added As per Neeraj Sir Mail On 05-11-18
						foreach ($di_clause->result() as $row){
							$di_count = $row->count1;
							
							if($di_count > 0){
							
$html .= '			   <tr>
                            <td width="5%">8.</td>
                            <td width="20%" colspan="2"><b style="color:red">DISPATCH INSTRUCTION</b></td>
                            <td width="75%" colspan="9">Without Dispatch Instruction Vendor/ Supplier should not send the material to the Purchaser. If the Supplier /Vendor is sending any material to the purchaser without Dispatch Instruction then, the purchaser has full rights to reject the material or send back the material on To Pay basis. Purchaser will not be liable for any delay payments or any loss if material sent without Dispatch Instruction.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>';
						
							}	
						}
						}
						
 $html .= '             <tr>
                            <td width="5%"></td>
                            <td width="95%" colspan="11"><b>All the general terms & conditions are as per Annexure attached.</b></td>
                        </tr>
        </table><br pagebreak="true"/>';
		
$html .='<div>
		   	<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">GENERAL TERMS AND CONDITIONS</h4>
			<p style="font-size:7px; text-align:justify; line-height:10px">
				<b>1. PRICE:</b><br>
i) Prices mentioned in this purchase order are firm and no deviation shall be considered under any circumstances. <br>
ii) Supplies in excess of ordered quantity shall be entirely at supplier`s risk and cost and all charges for handling excess supplies, including transport , repacking etc. will be to supplier account. <br><br>

<b>2. DELIVERY SCHEDULE:</b> <br>
(i)The delivery schedule as referred in our order shall be strictly adhered to without any deviation. However, we reserve the right to postpone/ cancel/ modify the delivery schedule due to any unforeseen circumstances due to the reasons beyond control. The communication in this regard shall be sent in advance before the date from whereof the delivery commences.<br>
(ii) <b>LIQUIDATED DAMAGES (LD)</b><br>
Timely delivery of material is the ESSENCE of this order. In the event of delay in agreed contractual delivery as per Purchase Order, penalty@0.5% (half percent) per week or part thereof but limited to a max of 10% (Ten Percent) value of undelivered portion (basic material cost)will be applicable. Delivery will commence from the date of document approval by TIPL or date of issue of manufacturing clearance, whichever is later.<br>
(iii) In addition TIPL shall be entitled to arrange supplies from alternate source at cost and risk of supplier and/or treat the order as complete and/or balance items as cancelled at its sole discretion; without need for any formal notice to supplier in this respect. This condition & discretion of TIPL shall not be litigated upon at any forum by the supplier.<br>
(iv)All the materials ordered including spares, if any, shall be dispatched in one consignment unless otherwise mentioned specifically in purchase order or the terms and condition of supply stipulated thereto. The title to the material will pass to the Buyer on receipt and acceptance after inspection at the buyer premises. <br><br>

<b>3. DOCUMENTATION:</b><br>
(i) You shall arrange dispatch of documents along with the inspection report and warranty certificate/Test Certificate wherever applicable. In the event the documents are sent through Bank as per the Terms agreed, you shall send copy of advance documents including invoice, Challan, Packing List, Photostat copy of GR / RR for our verification, as well making necessary arrangements for retiring the documents and clearance of consignment well within time.<br>
(ii) DELAY IN DOCUMENTS:<br>
In the event of delay in submission of complete set of documents (like drawings, bill of material, datasheets, catalogues, quality plan etc. Then the payment will delay accordingly till the very last document received and it will proceed for the payments only after/and the date of receiving the same.<br><br>

<b>4. PAYMENT:</b> <br>
(i) Bill should be raised in triplicate mentioning order numbers and challans. <br>
(ii) Bill will be passed in accordance with terms of this order. Any extra charges in the bill but not mentioned in the Purchase Order will be disallowed.<br>(iii) Payment of bill is normally in 45 days from the date of your invoice or from the date of receipt of complete material/documents in our stores whichever is later unless otherwise mentioned in order. <br>
(iv) Bill inclusive of taxes, duties must be accompanied with gate pass and authenticated documents. <br>
(v) All despatch documents must be mailed to us directly unless otherwise stipulated in this purchase order. In case of documents negotiated through Bank G.C. Note, L.R. etc. must accompany the documents. <br>
(vi) The payment terms as discussed and finalised and which has been mentioned in the aforesaid Purchase order pertains to only the baisc amount of the material. Payment against the GST portion will be released only after the credit for the same gets reflected in our GSTR 2A form on the basis of your filings for GSTR 1 for the respective month, or the due date of the PO as per the terms of payment, whichever is later.<br>
(vii) In addition to this our payment cycle will be followed for making due payment as per the terms of payment.<br><br>

<b>5. TRANSIT INSURANCE:</b> <br>
Only in case of Ex-works delivery terms, Transit Insurance between vendor’s works and site shall be arranged by TIPL through their underwriters, details of which shall be informed along with dispatch clearance intimation. However the vendor shall also inform the insurance agency suitably, while shipping the items. In case of FOR / FORD delivery terms, transit insurance / risk shall be in suppliers scope.<br><br>

<b>6. SHORTAGE/LOSS REJECTION/REPLACEMENT:</b> <br>
(i) You shall be liable to make good the losses, shortage if found in the consignment dispatched and received at our Plant i.e. Shipping Address. <br>
(ii) The rejection / discrepancy, if found, in the consignment supplied for the reasons whatsoever it may be, you shall make immediate necessary arrangements for rectification or replacement free of cost at our Sites, without any cost implications on us. <br><br>

<b>7. MODE OF DISPATCH:</b> <br>
(i) If the material is dispatched through wagons; the same shall be booked to TIPL siding.<br>
(ii) The RR/GR/LR must be consigned to TIPL only, not to SELF. Even if it is marked the same, responsibility will be of Seller for any Commercial Implications.<br>
(iii) When a consignment is sent in packages/bags with their weights/dimensions in metric units shall be specified in invoices(s) delivery note / challans.<br>
(iv)The material dispatched by road and booked on door delivery basis shall be delivered directly at our Plant site without any transhipment failing which you shall be liable to make good the loss including consequential losses. <br>
(v) The price if agreed upon as ex-works, you shall get our prior approval in respect of freight charges payable and the Transporter through whom the material shall be dispatched.<br>
(vi) Material should be duly packed in road worthy condition for safe delivery at our plant.<br><br>


<b>8. INSPECTION</b> <br>
The Buyer reserves the right to inspect the material at suppliers works before dispatch. The supplier shall inform the Buyer sufficiently in advance when the material is ready for dispatch to enable to Buyer to arrange inspection of the material and carry out any test, if so desired by the Buyer. <br><br>

<b>9. GUARANTEE/WARRANTY</b><br> 
The material offered shall be guaranteed for satisfactory performance for a period of 18 months from the date of dispatch of the last consignment as specified at other places in the PO. <br><br>

<b>10. JURISDICTION</b><br>
If at any dispute or differences shall arise in connection with the interpretation of any terms and conditions of this order, the provisions of the Indian Arbitration Act, 1996 will apply in the event of any dispute and subject to Ajmer Jurisdiction only.<br><br>

<b>11. GENERAL</b><br>
The terms and conditions expressly provided herein shall be final and supersede and prevail over the condition lay down or Stipulated elsewhere. You shall not be entitled to transfer your rights and obligation under this order to a third party without our written consent. On your signing the duplicate of this purchase order or accepting the Purchase order through email or otherwise, the parties hereto bind themselves to abide by the terms and conditions mentioned herein and this shall be constructed for  purposes as being a binding contract / agreement entered into at  Ajmer.<br><br>

<b>12. CANCELLATION</b><br>
(i)We reserve the right to cancel this order should the material not be delivered within the stipulated period, or they are not found to the make or brand or specification drawing mentioned in the order.<br>
(ii)For the cancellation thereof we do not hold ourselves responsible for Vendors loss of material capacity tooling or of business or for any other reasons.<br>
(iii)Above "General Terms & Conditions" shall be applicable unless stipulated otherwise, specifically, elsewhere in the purchase order.<br>

			</p>
		</div>';
//echo $html; 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('uploads/pdf/'.$po_no.'.pdf', 'FI');
?>