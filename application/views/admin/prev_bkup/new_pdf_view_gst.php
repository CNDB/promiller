<?php
ob_start();

$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
//Set Fonts
$pdf->SetFont('dejavusans', '', 10);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// Add a page
$pdf->AddPage();

$html = '<div>
		   <h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px"><u>PURCHASE ORDER</u></h4>
	     </div>'; 

$html .='<table width="100%" cellpadding="4" cellspacing="0" style="font-size:8px;">
			<tr style="background-color:#CCC;">
				<td width="33.33%" style="border:solid 1px black;"><b>VENDOR DETAILS</b></td>
				<td width="33.33%" style="border:solid 1px black;"><b>PO DETAILS</b></td>
				<td width="33.33%" style="border:solid 1px black;"><b>BUYER DETAILS</b></td>
			</tr>
            <tr>
                <td width="33.33%" style="border:solid 1px black; padding: 2px;">';

                foreach ($view_po_pdf->result() as $row){
							$po_no = $row->pomas_pono;
							$supp_name = $row->supp_spmn_supname;
							$supp_add1 = $row->supp_addr_address1;
							$supp_add2 = $row->supp_addr_address2;
							$supp_add3 = $row->supp_addr_address3;
							$comp_add1 = $supp_add1."&nbsp;".$supp_add2."&nbsp;".$supp_add3;
							$comp_add = mb_convert_encoding($comp_add1, "ISO-8859-1", "UTF-8");
							$supp_addr_city = $row->supp_addr_city;
							$supp_addr_state = $row->supp_addr_statedesc;
							$supp_addr_zip = $row->supp_addr_zip;
							$supp_spmn_supcode = $row->supp_spmn_supcode;
							$po_supp_email = $row->supp_addr_email;
							$supp_addr_contperson = $row->supp_addr_contperson;
							
							//mail id is not avalable in erp
							if($po_supp_email == '' || is_null($po_supp_email) == TRUE ){
								$sql_supp_live_det = "select top 1 * from TIPLDB..insert_po where po_num = '$po_no'";
								$query_supp_live_det = $this->db->query($sql_supp_live_det);
								
								foreach ($query_supp_live_det->result() as $row) {
								  $po_supp_email = $row->po_supp_name;
								}
							}
							
							if($supp_addr_contperson == '' || is_null($supp_addr_contperson) == TRUE ){
								$sql_supp_live_det = "select top 1 * from TIPLDB..insert_po where po_num = '$po_no'";
								$query_supp_live_det = $this->db->query($sql_supp_live_det);
								
								foreach ($query_supp_live_det->result() as $row) {
								  $supp_addr_contperson = $row->contact_person;
								}
							}
							//mail id is not avalable in erp
							
							$sql_gst_no ="select * from scmdb..supp_tax_details where supp_tax_type = 'EXCISE' and supp_tax_supcode = '$supp_spmn_supcode'";
							$query_gst_no = $this->db->query($sql_gst_no);
							
							foreach ($query_gst_no->result() as $row) {
							  $vendor_gst_no = $row->supp_tax_regdno;
							}
							
 $html .= '          <table widht="100%" height="150px">
                        <tr height="10px">
                            <td width="50%"><b>VENDOR NAME:</b></td>
                            <td width="50%">'.$supp_name.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>ADDRESS:</b></td>
                            <td width="50%">'.$comp_add.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="100%" colspan="2"><b>CITY:</b>&nbsp;'.$supp_addr_city.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>STATE:&nbsp;</b>'.$supp_addr_state.'</td>
                            <td width="50%"><b>PIN:&nbsp;</b>'.$supp_addr_zip.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>VENDOR CODE:</b></td>
                            <td width="50%">'.$supp_spmn_supcode.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>EMAIL:</b></td>
                            <td width="50%">'.$po_supp_email.'</td>
                        </tr >
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>CONTACT PERSON:</b></td>
                            <td width="50%">'.$supp_addr_contperson.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>VENDOR GST NO:</b></td>
                            <td width="50%" style="text-transform:uppercase;">'.$vendor_gst_no.'</td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>
                <td width="33.33%" style="border:solid 1px black; padding: 2px;">';
                
					foreach ($view_po_pdf->result() as $row){ 
						    $po_no = $row->pomas_pono;
							$po_type = $row->pomas_potype;
							$po_amend_no = $row->pomas_poamendmentno;
							$po_date = $row->pomas_podate;
							$po_freight = $row->paytm_incoterm;
							
							if($po_freight == 'FORD'){
								$po_freight_new = $po_freight." ,TIPL Ajmer";
							} else if($po_freight == 'FOR'){
								$po_freight_new = $po_freight." ,Ajmer";
							} else if($po_freight == 'EXW'){
								$po_freight_new = $po_freight." (EX-WORKS)";
							} else {
								$po_freight_new = $po_freight;
							}
							
							$po_payterm = $row->paytm_payterm;
							//$po_created_by = $row->pomas_createdby;
				
$html .= '      	<table width="100%" height="150px" >
                        <tr height="10px">
                            <td width="50%"><b>PO TYPE:</b></td>
                            <td width="50%">'.$po_type.'</td>
                        </tr>
						<tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>PO NO:</b></td>
                            <td width="50%">'.$po_no.'</td>
                        </tr>
						<tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>PO REV/AMEND:</b></td>
                            <td width="50%">'.$po_amend_no.'</td>
                        </tr>
						<tr height="10px">
                            <td width="50%"></td>
                            <td width="50%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>DATE:</b></td>
                            <td width="50%">'.substr($po_date,0,11).'</td>
                        </tr>						 
						<tr height="5px">
                            <td colspan="2"><br><hr /><br></td>
                        </tr>
                        <tr rowspan="6" height="60px">
                            <td colspan="2" width="100%"><p style="text-align:justify;">We are pleased to place our firm order on the terms & conditions stipulated here under. Please send your order Acceptance immediately after receipt of this order. In case of any discrepancy about PO terms and conditions, refer to us within 7 days. Otherwise PO will be treated as accepted by you.</p>			
                            </td>
                        </tr>
                        <tr height="5px">
                            <td colspan="2"><br><hr /><br></td>
                        </tr>
                        <tr height="10px">
                            <td width="50%"><b>TIPL GST NO.</b></td>
                            <td width="50%">08AAACT1582B1ZO</td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>
                <td width="33.33%" style="border:solid 1px black; padding: 2px;">';
                foreach ($view_po_pdf->result() as $row){ 
							$buyer_name_orginal = $row->pomas_createdby;
						    $buyer_name = str_replace("."," ",$row->pomas_createdby);
							
							
							$sql_mobile ="select * from TIPLDB..login where email like'%$buyer_name_orginal%'";
							$query_mobile = $this->db->query($sql_mobile);
							
							foreach ($query_mobile->result() as $row) {
							  $buyer_email_address = $row->email;
							  $buyer_mobile_no = $row->mobile_no;
							  //$buyer_name  = $row->name;
							}
							
$html .= '         <table widht="100%" height="150px" >
                        <tr height="10px">
                            <td width="40%"><b>BUYER NAME:</b></td>
                            <td width="60%" style=" text-transform:uppercase">'.strtoupper($buyer_name).'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>MOBILE NO</b></td>
                            <td width="60%">'.$buyer_mobile_no.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>EMAIL</b></td>
                            <td width="60%">'.$buyer_email_address.'</td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>RFQ. NO.</b></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"><b>QUOT. NO</b></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                        <tr height="10px">
                            <td width="40%"></td>
                            <td width="60%"></td>
                        </tr>
                    </table>';
                break;}
$html .= '      </td>
            </tr>
			</table>
			
            <div>
		   		<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">ITEM DETAILS</h4>
	     	</div>
            
			<table width="100%" height="auto" cellpadding="4" cellspacing="0" style="font-size:6px;">         
                        <tr style="background-color:#CCC">
                            <td style="border:solid 1px black; padding: 2px;"><b>SN</b></td>
                            <td style="border:solid 1px black; padding: 2px;"><b>ITEM CODE</b></td>
                            <td style="border:solid 1px black; padding: 2px;" colspan="3"><b>DESCRIPTION</b></td>
                            <td style="border:solid 1px black; padding: 2px;"><b>NEED DATE</b></td>
                            <td style="border:solid 1px black; padding: 2px;"><b>UOM</b></td>
                            <td style="border:solid 1px black; padding: 2px;"><b>CURR.</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>RATE</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>QTY</b></td>
							<td style="border:solid 1px black; padding: 2px;"><b>TOTAL PRICE</b></td>
                        </tr>';
                        	$po_s_no = 1;
							$total_value1 = 0;
							foreach ($view_po_pdf_item->result() as $row){
								$po_num = $row->pomas_pono;
								$po_line_no = $row->poitm_polineno;
								$item_code =  $row->poitm_itemcode;
								//$po_ipr_no = $row->po_ipr_no;
								$need_date1 = substr($row->poitm_needdate,0,11);
								$need_date = date("d-m-Y", strtotime($need_date1));
								$qty = $row->poitm_order_quantity;
								$uom = $row->poitm_puom;
								$unit_rate = $row->poitm_po_cost; 
								$item_value = $row->poitm_itemvalue;
								$total_value1 = $total_value1+$item_value;
								/*$item_desc1 = $row->po_itm_desc;
								$item_desc2 = '';
								$item_desc = $item_desc1.' '.$item_desc2;*/
								//$item_remarks = $row->item_remarks;
								/*$curr_symbol = "&#x20B9;";
								$currency = $row->currency;*/
								
								$sql_item_desc ="select * from scmdb..itm_loi_itemhdr where loi_itemcode = '$item_code'";
								$query_item_desc = $this->db->query($sql_item_desc);
								
								foreach ($query_item_desc->result() as $row) {
								  	$item_desc1 = $row->loi_itemdesc;
									$item_desc = iconv(mb_detect_encoding($item_desc1, mb_detect_order(), true), "UTF-8", $item_desc1);
								}
								
								/*$sql_currency ="select * from tipldb..po_master_table where po_num = '$po_num'";
								$query_currency = $this->db->query($sql_currency);
								
								foreach ($query_currency->result() as $row) {
								  	$currency = $row->currency;
								}*/
								
								if($po_first_three == 'FPO'){
									$sql_currency ="select * from tipldb..po_master_table where po_num = '$po_num'";
									$query_currency = $this->db->query($sql_currency);
									
									foreach ($query_currency->result() as $row){
										$currency = $row->currency;
									}
								} else {	
									$currency = 'INR';	
								}
						
$html .= '              <tr>
                            <td style="border:solid 1px black; padding: 2px;">'.$po_s_no.'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.$item_code.'</td>
                            <td style="border:solid 1px black; padding: 2px;" colspan="3">'.$item_desc.'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.$need_date1.'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.$uom.'</td>
                            <td style="border:solid 1px black; padding: 2px;">'.$currency.'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($unit_rate,2).'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($qty,2).'</td>
							<td style="border:solid 1px black; padding: 2px;">'.number_format($item_value,2).'</td>
                            
                        </tr>';
							$po_s_no++;}
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>TOTAL AMOUNT</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($total_value1,2).'</td>
                        </tr>';
						
						//Freight Charges
						
						$frt_count = 0;
						foreach ($freight_charges->result() as $row){
							$frt_amt = $row->podoc_tcdamount;
							//$frt_desc = $row->tcdcode;
							$frt_count++;
							
						}
						if($frt_count > 0 ){
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>FREIGHT CHARGES</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($frt_amt,2).'</td>
                        </tr>';
						}
						
						//Packing Charges
						$pack_count = 0;
						foreach ($packing_charges->result() as $row){
							$packing_amt = $row->podoc_tcdamount;
							//$frt_desc = $row->tcdcode;
							$pack_count++;
							
						}
						if($pack_count > 0 ){
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>PACKING CHARGES</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($packing_amt,2).'</td>
                        </tr>';
						}
						
						//Handling Charges
						$hand_count = 0;
						foreach ($handling_charges->result() as $row){
							$handling_amt = $row->podoc_tcdamount;
							//$frt_desc = $row->tcdcode;
							$hand_count++;
							
						}
						if($hand_count > 0 ){
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>HANDLING CHARGES</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($handling_amt,2).'</td>
                        </tr>';
						}
						
						//CGST TAX
						
						$cgst_count = 0;
						foreach ($view_po_pdf_cgst->result() as $row){
							$cgst_tax = $row->podoc_tcdamount;
							$cgst_desc = $row->tcdcode;
							$cgst_count++;
							
						}
						if($cgst_count > 0 ){
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>TOTAL CGST TAX</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($cgst_tax,2).'</td>
                        </tr>';
						}
						//SGST TAX
						
						$sgst_count = 0;
						foreach ($view_po_pdf_sgst->result() as $row){
							$sgst_tax = $row->podoc_tcdamount;
							$sgst_desc = $row->tcdcode;
							$sgst_count++;
						}
							
						if($sgst_count > 0 ){
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>TOTAL SGST TAX</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($sgst_tax,2).'</td>
                        </tr>';
						}
						//IGST TAX
						$igst_count = 0;
						foreach ($view_po_pdf_igst->result() as $row){
							$igst_tax = $row->podoc_tcdamount;
							$igst_desc = $row->tcdcode;
							$igst_count++;
						}
							
						if($igst_count > 0 ){
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>TOTAL IGST TAX</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($igst_tax,2).'</td>
                        </tr>';
						}
						//Calculation Of GRAND TOTAL
						$grand_total = ($total_value1+$frt_amt+$cgst_tax+$sgst_tax+$igst_tax+$packing_amt+$handling_amt);
						$grand_total1 = round($grand_total);
						
$html .= '              <tr>
                            <td colspan="10" style="border:solid 1px black; padding: 2px;"><b>GRAND TOTAL</b></td>
                            <td colspan="1" style="border:solid 1px black; padding: 2px;">'.number_format($grand_total1,2).'</td>
                        </tr>';
						
						//Coverting Grand Total Into Words
						   $number = $grand_total1;
						   $no = round($number);
						   $point = round($number - $no) * 100;
						   $hundred = null;
						   $digits_1 = strlen($no);
						   $i = 0;
						   $str = array();
						   $words = array(
							'0'  => '', 
							'1'  => 'One', 
							'2'  => 'Two',
							'3'  => 'Three', 
							'4'  => 'Four', 
							'5'  => 'Five', 
							'6'  => 'Six',
							'7'  => 'Seven', 
							'8'  => 'Eight', 
							'9'  => 'Nine',
							'10' => 'Ten', 
							'11' => 'Eleven', 
							'12' => 'Twelve',
							'13' => 'Thirteen', 
							'14' => 'Fourteen',
							'15' => 'Fifteen', 
							'16' => 'Sixteen', 
							'17' => 'Seventeen',
							'18' => 'Eighteen', 
							'19' => 'Nineteen', 
							'20' => 'Twenty',
							'30' => 'Thirty', 
							'40' => 'Forty', 
							'50' => 'Fifty',
							'60' => 'Sixty', 
							'70' => 'Seventy',
							'80' => 'Eighty', 
							'90' => 'Ninety');
						   $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
						   while ($i < $digits_1) {
							 $divider = ($i == 2) ? 10 : 100;
							 $number = floor($no % $divider);
							 $no = floor($no / $divider);
							 $i += ($divider == 10) ? 1 : 2;
							 if ($number) {
								$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
								$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
								$str [] = ($number < 21) ? $words[$number] .
									" " . $digits[$counter] . $plural . " " . $hundred
									:
									$words[floor($number / 10) * 10]
									. " " . $words[$number % 10] . " "
									. $digits[$counter] . $plural . " " . $hundred;
							 } else $str[] = null;
						  }
						  $str = array_reverse($str);
						  $result = implode('', $str);
						  $points = ($point) ?
							"." . $words[$point / 10] . " " . 
								  $words[$point = $point % 10] : '';
						  $result . "Rupees  " . $points . " Paise";
						  
						//Coverting Grand Total Into Words
						
$html .= '              <tr>
                            <td colspan="11" style="border:solid 1px black; padding: 2px;"><b>AMOUNT IN WORDS : '.$result ." ".$points." Only".'</b></td>
                        </tr>';
$html .= '              <tr>
                            <td colspan="11" style="border:solid 1px black; padding: 2px; text-align:right">
								<b>For Toshniwal Industries Pvt. Ltd.</b>
							</td>
                        </tr>
					</table><br>';
					foreach ($doc_lvl_notes->result() as $row){
						$doc_notes = $row->doc_notes;
$html .= '   	   <div style="font-size:6px">
						<b>Document Level Notes: </b>
						<p>'.$doc_notes.'</p>
					</div>';
					}
$html .= '		   <br pagebreak="true"/>
   	                <div>
		   				<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">TERMS AND CONDITIONS</h4>
	     			</div>
					
					<table width="100%" height="auto" style="font-size:8px;" cellpadding="1px" cellspacing="1">
                        <tr>
                            <td width="5%">1.</td>
                            <td width="20%" colspan="2"><b>GST</b></td>
                            <td width="75%" colspan="9">As applicable</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">2.</td>
                            <td width="20%" colspan="2"><b>LD</b></td>
                            <td width="75%" colspan="9">In the event of delay in agreed contractual delivery as per Purchase Order, penalty@0.5% (half percent) per week or part thereof but limited to a max of 10% (Ten Percent) value of undelivered portion (basic material cost)will be applicable. Delivery will commence from the date of document approval by TIPL or date of issue of manufacturing clearance,whichever is later.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">3.</td>
                            <td width="20%" colspan="2"><b>PACKING & FORWARDING</b></td>
                            <td width="75%" colspan="9">Inclusive</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">4.</td>
                            <td width="20%" colspan="2"><b>FREIGHT TERMS</b></td>
                            <td width="75%" colspan="9">'.$po_freight_new.'</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">5.</td>
                            <td width="20%" colspan="2"><b>AUTHORISED TRANSPORTER</b></td>
                            <td width="75%" colspan="9">If freight term is Ex-works or To Pay then please take our prior approval regarding transporter name before dispatch of material. In case if the same is not followed, the supplier has to pay the freight charges</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">6.</td>
                            <td width="20%" colspan="2"><b>DISPATCH INSTRUCTIONS & ROAD PERMIT</b></td>
                            <td width="75%" colspan="9">Dispatch Instructions or Road Permit will be issued only after submission of Test Certificate and its subsequent approval.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%">7.</td>
                            <td width="20%" colspan="2"><b>PAYMENT TERMS</b></td>
                            <td width="10%">1.</td>
                            <td width="65%" colspan="8">'.$po_payterm.'</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%">2.</td>
                            <td width="65%" colspan="8">Without the below said documents MRN shall not be raised in our portal for payment.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%"></td>
                            <td width="5%">(i)</td>
                            <td width="25%" colspan="3">Consigee LR (Original + 1 Copy)</td>
                            <td width="5%">(ii)</td>
                            <td width="30%" colspan="3">Road Permit (if applicable)</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%"></td>
                            <td width="5%">(iii)</td>
                            <td width="25%" colspan="3">Duplicate for Transport Invoice</td>
                            <td width="5%">(iv)</td>
                            <td width="30%" colspan="3">Original for Buyer Invoice Copy</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%"></td>
                            <td width="5%">(v)</td>
                            <td width="25%" colspan="3">Original Test Certificate</td>
                            <td width="5%"></td>
                            <td width="30%" colspan="3"></td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="20%" colspan="2"></td>
                            <td width="10%">3.</td>
                            <td width="65%" colspan="8">In case if any of the above documents is not received,your payment cycle will start only after/and from the date of  receiving of the that document in our premises.</td>
                        </tr>
                        <tr>
                            <td width="100%" colspan="12"></td>
                        </tr>
                        <tr>
                            <td width="5%"></td>
                            <td width="95%" colspan="11"><b>All the general terms & conditions are as per Annexure attached.</b></td>
                        </tr>
        </table><br pagebreak="true"/>';
		
$html .='<div>
		   	<h4 align="center" style="font-weight:bold; color:black; text-transform:uppercase; font-size:11px">GENERAL TERMS AND CONDITIONS</h4>
			<p style="font-size:7px; text-align:justify; line-height:10px">
				<b>1. PRICE:</b><br>
i) Prices mentioned in this purchase order are firm and no deviation shall be considered under any circumstances. <br>
ii) Supplies in excess of ordered quantity shall be entirely at supplier`s risk and cost and all charges for handling excess supplies, including transport , repacking etc. will be to supplier account. <br><br>

<b>2. DELIVERY SCHEDULE:</b> <br>
(i)The delivery schedule as referred in our order shall be strictly adhered to without any DEVIATION. HOWEVER, WE RESERVE THE RIGHT TO POSTPONE / CANCEL /MODIFY THE DELIVERY SCHEDULE DUE TO ANY UNFORESEEN CIRCUMSTANCES DUE TO THE REASONS BEYOND CONTROL. THE COMMUNICATION IN THIS REGARD SHALL BE SENT IN ADVANCE BEFORE THE DATE FROM WHEREOF THE DELIVERY COMMENCES.<br><br>
(ii) <b>LIQUIDATED DAMAGES (LD)</b><br>
Timely delivery of material is the ESSENCE of this order. In the event of delay in agreed contractual delivery as per Purchase Order, penalty@0.5% (half percent) per week or part thereof but limited to a max of 10% (Ten Percent) value of undelivered portion (basic material cost)will be applicable. Delivery will commence from the date of document approval by TIPL or date of issue of manufacturing clearance, whichever is later.<br>
(iii) In addition TIPL shall be entitled to arrange supplies from alternate source at cost and risk of supplier and/or treat the order as complete and/or balance items as cancelled at its sole discretion; without need for any formal notice to supplier in this respect. This condition & discretion of TIPL shall not be litigated upon at any forum by the supplier.<br>
(iv)All the materials ordered including spares, if any, shall be dispatched in one consignment unless otherwise mentioned specifically in purchase order or the terms and condition of supply stipulated thereto. The title to the material will pass to the Buyer on receipt and acceptance after inspection at the buyer premises. <br><br>

<b>3. DOCUMENTATION:</b><br>
(i) You shall arrange dispatch of documents along with the inspection report and warranty certificate/Test Certificate wherever applicable. In the event the documents are sent through Bank as per the Terms agreed, you shall send copy of advance documents including invoice, Challan, Packing List, Photostat copy of GR / RR for our verification, as well making necessary arrangements for retiring the documents and clearance of consignment well within time.<br>
(ii) DELAY IN DOCUMENTS:<br>
In the event of delay in submission of complete set of documents (like drawings, bill of material, datasheets, catalogues, quality plan etc. Then the payment will delay accordingly till the very last document received and it will proceed for the payments only after/and the date of receiving the same.<br><br>

<b>4. PAYMENT:</b> <br>
(i) Bill should be raised in triplicate mentioning order numbers and challans. <br>
(ii) Bill will be passed in accordance with terms of this order. Any extra charges in the bill but not mentioned in the Purchase Order will be disallowed.<br>(iii) Payment of bill is normally in 45 days from the date of your invoice or from the date of receipt of complete material/documents in our stores whichever is later unless otherwise mentioned in order. <br>
(iv) Bill inclusive of taxes, duties must be accompanied with gate pass and authenticated documents. <br>
(v) All despatch documents must be mailed to us directly unless otherwise stipulated in this purchase order. In case of documents negotiated through Bank G.C. Note, L.R. etc. must accompany the documents. <br>
(vi) The payment terms as discussed and finalised and which has been mentioned in the aforesaid Purchase order pertains to only the baisc amount of the material. Payment against the GST portion will be released only after the credit for the same gets reflected in our GSTR 2A form on the basis of your filings for GSTR 1 for the respective month, or the due date of the PO as per the terms of payment, whichever is later.<br>
(vii) In addition to this our payment cycle will be followed for making due payment as per the terms of payment.<br><br>

<b>5. TRANSIT INSURANCE:</b> <br>
Transit Insurance between vendor’s works and site shall be arranged by TIPL through their underwriters, details of which shall be informed along with dispatch clearance intimation. However the vendor shall also inform the insurance agency suitably, while shipping the items.<br><br>

<b>6. SHORTAGE/LOSS REJECTION/REPLACEMENT:</b> <br>
(i) You shall be liable to make good the losses, shortage if found in the consignment dispatched and received at our Plant i.e. Shipping Address. <br>
(ii) The rejection / discrepancy, if found, in the consignment supplied for the reasons whatsoever it may be, you shall make immediate necessary arrangements for rectification or replacement free of cost at our Sites, without any cost implications on us. <br><br>

<b>7. MODE OF DISPATCH:</b> <br>
(i) If the material is dispatched through wagons; the same shall be booked to TIPL siding.<br>
(ii) The RR/GR/LR must be consigned to TIPL only, not to SELF. Even if it is marked the same, responsibility will be of Seller for any Commercial Implications.<br>
(iii) When a consignment is sent in packages/bags with their weights/dimensions in metric units shall be specified in invoices(s) delivery note / challans.<br>
(iv)The material dispatched by road and booked on door delivery basis shall be delivered directly at our Plant site without any transhipment failing which you shall be liable to make good the loss including consequential losses. <br>
(v) The price if agreed upon as ex-works, you shall get our prior approval in respect of freight charges payable and the Transporter through whom the material shall be dispatched.<br>
(vi) Material should be duly packed in road worthy condition for safe delivery at our plant.<br><br>

<b>8. INSPECTION</b> <br>
The Buyer reserves the right to inspect the material at suppliers works before dispatch. The supplier shall inform the Buyer sufficiently in advance when the material is ready for dispatch to enable to Buyer to arrange inspection of the material and carry out any test, if so desired by the Buyer. <br><br>

<b>9. GUARANTEE/WARRANTY</b><br> 
The material offered shall be guaranteed for satisfactory performance for a period of 18 months from the date of dispatch of the last consignment as specified at other places in the PO. <br><br>

<b>10. JURISDICTION</b><br>
If at any dispute or differences shall arise in connection with the interpretation of any terms and conditions of this order, the provisions of the Indian Arbitration Act, 1996 will apply in the event of any dispute and subject to Ajmer Jurisdiction only.<br><br>

<b>11. GENERAL</b><br>
The terms and conditions expressly provided herein shall be final and supersede and prevail over the condition lay down or Stipulated elsewhere. You shall not be entitled to transfer your rights and obligation under this order to a third party without our written consent. On your signing the duplicate of this purchase order or accepting the Purchase order through email or otherwise, the parties hereto bind themselves to abide by the terms and conditions mentioned herein and this shall be constructed for  purposes as being a binding contract / agreement entered into at  Ajmer.<br><br>

<b>12. CANCELLATION</b><br>
(i)We reserve the right to cancel this order should the material not be delivered within the stipulated period, or they are not found to the make or brand or specification drawing mentioned in the order.<br>
(ii)For the cancellation thereof we do not hold ourselves responsible for Vendors loss of material capacity tooling or of business or for any other reasons.<br>
(iii)Above "General Terms & Conditions" shall be applicable unless stipulated otherwise, specifically, elsewhere in the purchase order.<br>

			</p>
		</div>';
//echo $html; 
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('uploads/pdf/'.$po_no.'.pdf', 'FI');
?>