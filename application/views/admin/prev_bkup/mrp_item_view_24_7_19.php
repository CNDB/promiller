<?php include'header.php'; ?>
<?php
$item_code = $this->uri->segment(3);

$user_name = $_SESSION['username'];

$item_code1 = rawurldecode($item_code);

if(strpos($item_code, 'chandra') !== false){
	
	$item_code1 = str_replace("chandra","%2F",$item_code);
}else {
	$item_code1 = $item_code;
}

$item_code2 = rawurldecode($item_code1);

$sql="select a.TotalWipIss+a.ReOrderLevel as required_qty, a.FreeStockQty+a.TotalPORcp+a.TotalPRRcp+a.TotalGRRcp as available_qty,* from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, scmdb..itm_ibu_itemvarhdr b, 
tipldb..erp_live_category c where a.itemcode = '$item_code2' and a.ItemCode = b.ibu_itemcode
and b.ibu_category = c.erp_cat_code";

$query = $this->db->query($sql);

foreach ($query->result() as $row) {
	
	$item_desc = $row->Descriptions; 
	$live_category = $row->live_category;
	$StockUom = $row->StockUom;
	$PurchaseUom = $row->PurchaseUom;
	$required_qty = $row->required_qty;
	$available_qty = $row->available_qty;
	$mrp_total_required = $required_qty-$available_qty;
	$reorder_level = $row->ReOrderLevel;
	
	$item_desc_new = $str = str_replace(array('\'', '"'), '', $item_desc);
}

//Checking previously requested items fresh request

$sql_prev_ent = "select * from tipldb..mrp_item_request_entry_table where item_code = '$item_code2' and status = 'Pending For PR Creation In ERP'";
$query_prev_ent = $this->db->query($sql_prev_ent);

if ($query_prev_ent->num_rows() > 0) {
  $prev_entry_tot = 0;
  foreach ($query_prev_ent->result() as $row) {
	  $prev_entry = $row->pr_req_qty;
	  $prev_entry_tot = $prev_entry_tot+$prev_entry;
	}
} else {
	  $prev_entry_tot = 0;
	  $prev_entry_tot = 0;
}

$mrp_total_required_net = $mrp_total_required;

//Net Required Quanities
?>
<section id="main-content">
  <section class="wrapper">
        <div class="row"  style="margin-top:-10px">
            <div class="col-lg-12" style="background-color:#333333; padding:2px">
                  <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">MRP ITEM REQUEST</h4>
            </div>
        </div><br />
        
        <form action="<?php echo base_url(); ?>index.php/mrp_reportc/mrp_item_entry" method="post" id="myform" onSubmit="return reqd()">
        
        <div class="row" style="font-size:14px;">   
            <div class="col-lg-2">
                <b>ITEM CODE</b><br />
                <?php echo $item_code2; ?>
                <input type="hidden" name="item_code" id="item_code" value="<?php echo $item_code2; ?>" />
            </div>
            <div class="col-lg-2">
                <b>ITEM DESCRIPTION</b><br />
                <?php echo $item_desc_new; ?>
                <input type="hidden" name="item_desc" id="item_desc" value="<?php echo $item_desc_new; ?>" />
            </div>
            <div class="col-lg-2">
                <b>CATEGORY</b><br />
                <?php echo $live_category; ?>
                <input type="hidden" name="live_category" id="live_category" value="<?php echo $live_category; ?>" />
            </div>
            <div class="col-lg-2">
                <b>STOCK UOM</b><br />
                <?php echo $StockUom; ?>
                <input type="hidden" name="StockUom" id="StockUom" value="<?php echo $StockUom; ?>" />
            </div>
            <div class="col-lg-2">
                <b>PURCHASE UOM</b><br />
                <?php echo $PurchaseUom; ?>
                <input type="hidden" name="PurchaseUom" id="PurchaseUom" value="<?php echo $PurchaseUom; ?>" />
            </div>
            <div class="col-lg-2">
                <b>MRP TOTAL REQUIRED</b><br />
                <?php echo number_format($mrp_total_required_net,2); ?>
                <input type="hidden" name="mrp_total_required" id="mrp_total_required" value="<?php echo $mrp_total_required_net; ?>" />
            </div>
        </div><br /><br />
        
        <div class="row" style="font-size:14px;">   
            <div class="col-lg-2">
                <b>PR TYPE</b><br />
                <?php if($reorder_level > 0){ echo $pr_type = "Reorder";} else{ echo $pr_type = "Project"; } ?>
                <input type="hidden" name="pr_type" id="pr_type" value="<?php echo $pr_type; ?>">
            </div>
            <div class="col-lg-2">
                <b>User Name</b><br />
                <?php echo $user_name; ?>
                <input type="hidden" name="user_name" id="user_name" value="<?php echo $user_name; ?>" />
            </div>
            <div class="col-lg-8">   
            </div>
        </div><br /><br />
        
        <div class="row" style="font-size:14px;">
            <div class="col-lg-2"><b>PR REQ. QUANTITY</b><b style="color:red">&nbsp;*</b></div>
            <div class="col-lg-2">
            <input type="text" name="pr_req_qty" id="pr_req_qty" class="form-control" value="" onkeypress='return validateQty(event);' required/>
            </div>
            <div class="col-lg-2"><b>REMARKS</b></div>
            <div class="col-lg-4"><input type="text" name="remarks" id="remarks" class="form-control" value="" required/></div>
            <div class="col-lg-2"></div>  
        </div><br /><br />
        
        <div class="row" style="font-size:14px;">
            <div class="col-lg-5"></div>
            <div class="col-lg-2"><input type="submit" name="submit_date" id="submit_data" class="form-control" value="SAVE" /></div>
            <div class="col-lg-5"></div>   
        </div>
    </div>
    <hr>
    <?php include('pendal_card.php'); ?>      		      
    
  </section>
</section>

<script>

function validateQty(event) {
    var key = window.event ? event.keyCode : event.which;

	if (event.keyCode == 8 || event.keyCode == 46 || event.keyCode == 37 || event.keyCode == 39  ) {
		return true;
	}
	else if ( key != 46 && key > 31 && (key < 48 || key > 57) ) {
		return false;
	}
	else return true;
}

function reqd(){
	var pr_qty = document.getElementById("pr_req_qty").value;
	var remarks = document.getElementById("remarks").value;
	var mrp_total_required = document.getElementById("mrp_total_required").value;
	
	if(pr_qty == ''){
		alert("PR Required Quantity Cannot Be Blank");
		document.getElementById("pr_req_qty").focus;
		return false;
	}
	
	/*if(Math.round(pr_qty*100) > Math.round(mrp_total_required*100)){
		alert("PR Quantity Cannot Be Greater Than MRP Required Qty");
		document.getElementById("pr_req_qty").focus;
		return false;
	}*/
	
	if(remarks == ''){
		alert("Please Enter Remarks");
		document.getElementById("remarks").focus;
		return false;
	}
}

</script>

<?php include('footer.php'); ?>