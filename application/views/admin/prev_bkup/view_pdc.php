<!--********* PO INFORMATION *********-->

<form action="<?php echo base_url(); ?>index.php/createpdcc/insert_po_sub" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?> 

<!--****** REMARKS & SUPPLIER QUOTES *******-->

<?php include('po_supplier_quotes.php'); ?>

<?php
	foreach ($view_po->result() as $row){ 
	$attached_pi = $row->attach_pi;
?>
<div class="row">
    <div class='col-lg-3'><b>Attached Performa Invoice : </b></div>
    <div class='col-lg-3'><a href="<?php echo base_url(); ?>uploads/<?php echo $attached_pi; ?>" target="_blank"><?php echo $attached_pi; ?></a></div>
    <div class="col-lg-3"></div>
    <div class="col-lg-3"></div>
</div><br />   

<?php 
	break;} 
?> 
<hr />
<?php //PDC PAYMENT START ?>
<div class="row">
	<div class="col-lg-4"></div>
    <div class="col-lg-4"><h3 style="text-align:center">PDC Payment Details</h3></div>
    <div class="col-lg-4"></div>
</div><br /><br />
<?php
		$sql9 ="select * from scmdb..sr_receipt_mst a, TIPLDB..po_master_table b where a.supplier_code = b.supplier_code and a.receipt_date > b.po_create_date
		and a.supplier_code = '$supplier_code' and b.po_num = '$po_num_new'";
		$query9 = $this->db->query($sql9);
						
		
		foreach ($query9->result() as $row){
			$receipt_no = $row->receipt_no;
			$receipt_mode = $row->receipt_mode;
			$receipt_date = $row->receipt_date;
			$receipt_status = $row->receipt_status;
			$bank_name = $row->bank_cash_code;
			$recipt_amount = $row->receipt_amount;
?>


<div class="row">
	<div class="col-lg-2"><b>Receipt No :</b></div>
    <div class="col-lg-2"><b><?php echo $receipt_no; ?></b></div>
    <div class="col-lg-2"><b>Receipt Mode :</b></div>
    <div class="col-lg-2"><b><?php echo $receipt_mode; ?></b></div>
    <div class="col-lg-2"><b>Receipt Date :</b></div>
    <div class="col-lg-2"><b><?php echo date("d-m-Y", strtotime($receipt_date)); ?></b></div>
</div><br />

<div class="row">
	<div class="col-lg-2"><b>Receipt Status :</b></div>
    <div class="col-lg-2"><b><?php echo $receipt_status; ?></b></div>
    <div class="col-lg-2"><b>Bank Name :</b></div>
    <div class="col-lg-2"><b><?php echo $bank_name; ?></b></div>
    <div class="col-lg-2"><b>Receipt Amount :</b></div>
    <div class="col-lg-2"><b><?php echo number_format($recipt_amount, 2); ?></b></div>
</div><br />
<?php } ?>
            
<div class="row">
    <div class="col-lg-4"></div>
    <div class="col-lg-4"> 
        <input type="submit" name="pdc_create_inst" value="PDC Payment To Supplier" class="form-control" style="font-weight:bold; background:#000000; 
        color:#FFFFFF; letter-spacing:2px" />     
    </div>
    <div class="col-lg-4"></div>
</div>         
</form>  
      
<?php //Chat History ?>
 
<?php include('po_chat_history.php'); ?>
 
<?php //Action Timing Report ?>

<?php include('po_action_timing.php'); ?>

<?php //Footer ?>
      		
<?php include('footer.php'); ?>