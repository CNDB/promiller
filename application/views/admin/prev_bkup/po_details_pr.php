<?php $this->load->helper('status'); ?>

<div class="row">
    <div class="col-lg-12">
        <table class="table table-bordered" border="1">
            <tr style="font-size:14px; font-weight:bold; background-color:rgb(204, 204, 204)">
                <td>PO NO</td>
                <td>PO CREATE DATE</td>
                <td>PO ERP STATUS</td>
                <td>PO LIVE STATUS</td>
                <td>GATE ENTRY NO</td>
                <td>GATE ENTRY DATE</td>
                <td>GRCPT NO</td>
                <td>GRCPT DATE</td>
                <td>GRCPT STATUS</td>
            </tr>
            <?php
                $sql_po_det = "select * from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b 
                where a.po_ipr_no = '".$pr_num."' and a.po_num = b.pomas_pono 
                and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
                and b.pomas_podocstatus not in('DE')";
                
                $qry_po_det = $this->db->query($sql_po_det);
                
                foreach($qry_po_det->result() as $row){
                    $po_num = $row->po_num;
                    $po_create_date = $row->pomas_createddate;
                    $po_erp_status = $row->pomas_podocstatus;
                    $po_live_status = $row->status;
                    
                    $sql_grcpt_det = "select * from scmdb..gr_hdr_grmain where gr_hdr_orderno = '".$po_num."' and gr_hdr_grstatus not in('DL')";
                    $qry_grcpt_det = $this->db->query($sql_grcpt_det);
                    
                    if($qry_grcpt_det->num_rows() > 0){
                        foreach($qry_grcpt_det->result() as $row){
                            $grcpt_no = $row->gr_hdr_grno;
                            $grcpt_date = $row->gr_hdr_grdate;
                            $grcpt_status = $row->gr_hdr_grstatus;
                            $gate_entry_no = $row->gr_hdr_gatepassno;
                            $gate_entry_date = $row->gr_hdr_gatepassdate;
                        }
                    } else {
                        $grcpt_no = "";
                        $grcpt_date = "";
                        $grcpt_status = "";
                        $gate_entry_no = "";
                        $gate_entry_date = "";
                    }
                    
            ?>
            <tr>
                <td><?php echo $po_num; ?></td>
                <td><?php echo substr($po_create_date,0,11); ?></td>
                <td><?php echo status($po_erp_status); ?></td>
                <td><?php echo $po_live_status; ?></td>
                <td><?php echo $gate_entry_no; ?></td>
                <td><?php echo substr($gate_entry_date,0,11); ?></td>
                <td><?php echo $grcpt_no; ?></td>
                <td><?php echo substr($grcpt_date,0,11); ?></td>
                <td><?php echo status($grcpt_status); ?></td>
            </tr>
            <?php } ?>
        </table>
    </div>
</div><br /><br />