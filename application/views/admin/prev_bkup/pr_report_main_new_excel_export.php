<?php
ini_set('max_execution_time', -1); 
ini_set('memory_limit','-1');
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=export.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>

<?php

$from_date = $_GET['from_date'];
$to_date = $_GET['to_date'];
$usage_type1 = $_GET['usage_type'];
$usage_type=rawurldecode($usage_type1);

?>
<table border="1">
    <tr style="background-color:#0CF; font-weight:bold;">
        <td>S.NO.</td>
        <td>PR NUMBER</td>
        <td>ITEM CODE</td>
        <td>ITEM DESCRIPTION</td>
        <td>USAGE TYPE</td>
        <td>CATEGORY</td>
        <td>PR ERP CREATE DATE (A)</td>
        <td>PR LIVE CREATE DATE (B)</td>
        <td>AGE (B-A)</td>
        <td>PR L1 APP DATE (C)</td>
        <td>AGE (C-B)</td>
        <td>PR L2 APP DATE (D)</td>
        <td>AGE (D-C)</td>
        <td>PR ERP AUTH DATE</td>
        <td>PR NEED DATE</td>
        <td>PR ERP STATUS</td>
        <td>PR PR LIVE STATUS</td>
        <td>PO NUMBER</td>
        <td>PO AMEND NO</td>
        <td>SUPPLIER NAME</td>
        <td>PO CATEGORY</td>
        <td>PAYTERM</td>
        <td>FREIGHT TERM</td>
        <td>PACKING CHARGES</td>
        <td>FREIGHT CHARGES</td>
        <td>PO QTY</td>
        <td>LAST PRICE</td>
        <td>CURRENT PRICE</td>
        <td>SAVING</td>
        <td>LOSS</td>
        <td>PO ERP STATUS</td>
        <td>PO ERP CREATE DATE (E)</td>
        <td>AGE (E-D)</td>
        <td>PO LIVE CREATE DATE (F)</td>
        <td>AGE (F-E)</td>
        <td>PO LIVE L1 APP DATE (G)</td>
        <td>AGE (G-F)</td>
        <td>PO LIVE L2 APP DATE (H)</td>
        <td>AGE (H-G)</td>
        <td>PO LIVE ERP AUTH DATE DATE (I)</td>
        <td>AGE (I-H)</td>
        <td>PO RELESED TO SUPP DATE (J)</td>
        <td>AGE (J-I)</td>
        <td>PO GATE ENTRY DATE (K)</td>
        <td>AGE (K-J)</td>
        <td>PO GRCPT CREATE DATE (L)</td>
        <td>AGE (L-K)</td>
        <td>GRCPT FREEZE RECIPT DATE (M)</td>
        <td>AGE (M-L)</td>
        <td>GRCPT FREEZE ACCEPTANCE DATE (N)</td>
        <td>AGE (N-M)</td>
        <td>GRCPT FREEZE MOVEMENT DATE (O)</td>
        <td>AGE (O-N)</td>
    </tr>
    <?php
		//Queries
		if($usage_type == 'Purchase Request'){
			
			$sql_main = "select * from TIPLDB..pr_po_history_report where convert(date,pr_erp_create_date) BETWEEN '$from_date' and '$to_date' order by      			            pr_erp_create_date desc";
			
			$query_main = $this->db->query($sql_main);
			
		} else if($usage_type == 'Purchase Order'){
			
			$sql_main = "select * from TIPLDB..pr_po_history_report where convert(date,po_erp_create_date) BETWEEN '$from_date' and '$to_date' order by    	         	po_erp_create_date desc";
			
			$query_main = $this->db->query($sql_main);
			
		}
		
		//Queries 
        $sno = 0;
        foreach ($query_main->result() as $row){
            $sno++;
            $pr_num                = $row->pr_num;
            $item_code             = $row->item_code;
            $item_code1            = urlencode($item_code);
            $item_desc             = $row->item_desc;
            $item_desc_new         = mb_convert_encoding($item_desc, "ISO-8859-1", "UTF-8");
            $usage_type            = $row->usage;
            $category              = $row->pr_category;
            $pr_date               = $row->pr_erp_create_date;
            $live_created_by       = $row->pr_live_create_by;
            $pr_status             = $row->pr_live_status;
			$pr_live_cd            = $row->pr_live_create_date;
            $lvl1_approval_by      = $row->pr_lvl1_app_by;
            $pr_approval_inst      = $row->pr_approval_inst_lvl1;
            $lvl1_approval_date    = $row->pr_lvl1_app_date;
            $pr_approval_by_lvl2   = $row->pr_lvl2_app_by;
            $pr_approval_inst_lvl2 = $row->pr_lvl2_inst;
            $pr_approval_date_lvl2 = $row->pr_lvl2_app_date;
			$pr_erp_auth_date      = $row->pr_erp_auth_date;
			$erp_status            = $row->pr_erp_status;
			$pr_erp_cd             = $row->pr_erp_create_date;
			$po_num                = $row->po_num;
			$po_live_create_date   = $row->po_live_create_date;
			$po_l1_app_date        = $row->po_lvl1_app_date;
			$po_l2_app_date        = $row->po_lvl2_approval_date;
			$po_send_to_supp_date  = $row->po_send_to_supplier_date;
			$po_category           = $row->po_category;
			$last_price            = $row->last_price;
			$current_price         = $row->current_price;
			$po_qty                = $row->po_qty;
			$po_erp_createdate     = $row->po_erp_create_date;
			$po_erp_status         = $row->po_erp_status;
			$grcpt_createdate      = $row->grcpt_fr_date;
			$gate_entry_no         = $row->gate_entry_no;
			$grcpt_fr_date         = $row->grcpt_fr_date;
			$grcpt_fa_date         = $row->grcpt_fa_date;
			$grcpt_fm_date         = $row->grcpt_fm_date;
			$gate_entry_date       = $row->po_gate_entry_date;
			$pr_need_date          = $row->pr_need_date;
			$supplier_name         = $row->supplier_name;
			$payterm               = $row->current_payterm;
			$freight               = $row->current_freight;
			$packing_chrg          = $row->packing_chrg;
			$freight_chrg          = $row->freight_chrg;
			$po_amendment_no       = $row->po_amendment_no;
			
			if(strpos($item_code1, '%2F') !== false){
                $item_code2 = str_replace("%2F","chandra",$item_code1);
            }else{
                $item_code2 = $item_code1;
            }
            
			//ERP Status
            if($erp_status == 'DR'){
                $erp_status1 = 'DRAFT';
            } else if($erp_status == 'HD'){
                $erp_status1 = 'HOLD';
            } else if($erp_status == 'FR'){
                $erp_status1 = 'FRESH';
            } else if($erp_status == 'CA'){
                $erp_status1 = 'CANCELLED';
            } else if($erp_status == 'DE'){
                $erp_status1 = 'DELETED';
            } else if($erp_status == 'AU'){
                $erp_status1 = 'AUTHORIZED';
            } else {
                $erp_status1 = $erp_status;
            }
			
			//Last Price Blank Check
			if($last_price==""){
				$last_price1 = 0;
			} else {
				$last_price1 = $last_price;
			}
			
			$difference = $last_price1-$current_price;
			
			//Loss Or Saving
			if($difference > 0 && $last_price != ""){
				$stat1 = number_format($difference,2,".","");
				$colm = "saving";
			} else if($difference < 0  && $last_price != ""){
				$stat1 = number_format($difference,2,".","");
				$colm = "loss";
			} else if($difference == 0  && $last_price != ""){
				$stat1 = number_format($difference,2,".","");
				$colm="no col";
			} else {
				$stat1 = "";
				$colm="no col";
			}
			
			//L1 Not Approved
			if($pr_approval_inst == 'DISAPPROVE'){
                $lvl1_approval_by = "";
                $lvl1_approval_date = "";
            }
			
			
			//L2 Not Approved
			if($pr_approval_inst_lvl2 == 'DISAPPROVE'){
                $pr_approval_by_lvl2 = "";
                $pr_approval_date_lvl2 = "";
            }
    ?>
    <tr>
        <td><?php echo $sno; ?></td>
        <td><?php echo $pr_num; ?></td>
        <td><?php echo $item_code; ?></td>
        <td><?php echo str_replace("'","",$item_desc_new); ?></td>
        <td><?php echo $usage_type; ?></td>
        <td><?php echo $category; ?></td>
        
        <td><?php echo $a=substr($pr_erp_cd,0,11); ?></td>
        <td><?php echo $b=substr($pr_live_cd,0,11); ?></td>
        
        <td>
		<?php 
			if($b!="" && $a!=""){
			$diff = abs(strtotime($b) - strtotime($a)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $c=substr($lvl1_approval_date,0,11); ?></td>
        
        <td>
		<?php 
			if($c!="" && $b!=""){
			$diff = abs(strtotime($c) - strtotime($b));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $d=substr($pr_approval_date_lvl2,0,11); ?></td>
        
        <td>
		<?php 
			if($d!="" && $c!=""){
			$diff = abs(strtotime($d) - strtotime($c));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
			printf("%d",$days);
			}
		?>
        </td>
        <td><?php if($erp_status1 != 'FRESH'){ echo substr($pr_erp_auth_date,0,11); } else { echo "";} ?></td>
        <td><?php echo substr($pr_need_date,0,11); ?></td>
        <td><?php echo $erp_status1; ?></td>
        <td><?php echo $pr_status; ?></td>
        <td><?php echo $po_num; ?></td>
        <td><?php echo $po_amendment_no; ?></td>
        <td><?php echo $supplier_name; ?></td>
        <td><?php echo $po_category; ?></td>
        <td><?php echo $payterm; ?></td>
        <td><?php echo $freight; ?></td>
        <td><?php echo number_format($packing_chrg,2,".",""); ?></td>
        <td><?php echo number_format($freight_chrg,2,".",""); ?></td>
        <td><?php echo number_format($po_qty,2,".",""); ?></td>
        <td><?php echo number_format($last_price,2,".",""); ?></td>
        <td><?php echo number_format($current_price,2,".",""); ?></td>
        <td><?php if($colm == 'saving'){ echo $stat1; }?></td>
        <td><?php if($colm == 'loss'){ echo $stat1; } ?></td>
        <td><?php echo $po_erp_status; ?></td>
        
        <td><?php echo $e=substr($po_erp_createdate,0,11); ?></td>
        
        <td>
		<?php 
			if($e!="" && $d!=""){
			$diff = abs(strtotime($e) - strtotime($d));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
			printf("%d",$days);
			}
		?>
        </td>
        
        <td><?php echo $f=substr($po_live_create_date,0,11); ?></td>
        
        <td>
		<?php 
			if($f!="" && $e!=""){
			$diff = abs(strtotime($f) - strtotime($e)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $g=$h=substr($po_l1_app_date,0,11); ?></td>
        
        <td>
		<?php 
			if($g!="" && $f!=""){
			$diff = abs(strtotime($g) - strtotime($f)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days);
			}
		?>
        </td>
        
        <td><?php echo $h=substr($po_l2_app_date,0,11); ?></td>
        
        <td>
		<?php
			if($h!="" && $g!=""){ 
			$diff = abs(strtotime($h) - strtotime($g));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $i=substr($po_l2_app_date,0,11); ?></td>
        
        <td>
		<?php
			if($i!="" && $h!=""){ 
			$diff = abs(strtotime($i) - strtotime($h)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days);
			}
		?>
        </td>
        
        <td><?php echo $j=substr($po_send_to_supp_date,0,11); ?></td>
        
        <td>
		<?php 
			if($j!="" && $i!=""){
			$diff = abs(strtotime($j) - strtotime($i));
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24)); 
			printf("%d",$days);
			}
		?>
        </td>
        
        <td><?php echo $k=substr($gate_entry_date,0,11); ?></td>
        
        <td>
		<?php 
			if($k!="" && $j!=""){
			$diff = abs(strtotime($k) - strtotime($j)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $l=substr($grcpt_createdate,0,11); ?></td>
        
        <td>
		<?php 
			if($l!="" && $k!=""){
			$diff = abs(strtotime($l) - strtotime($k)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $m=substr($grcpt_fr_date,0,11); ?></td>
        
        <td>
		<?php 
			if($m!="" && $l!=""){
			$diff = abs(strtotime($m) - strtotime($l)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days); 
			}
		?>
        </td>
        
        <td><?php echo $n=substr($grcpt_fa_date,0,11); ?></td>
        
        <td>
		<?php 
			if($n!="" && $m!=""){
			$diff = abs(strtotime($n) - strtotime($m)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days);
			}
		?>
        </td>
        
        <td><?php echo $o=substr($grcpt_fm_date,0,11); ?></td>
        
        <td>
		<?php 
			if($o!="" && $n!=""){
			$diff = abs(strtotime($o) - strtotime($n)); 
			$days = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));
			printf("%d",$days); 
			}
		?>
        </td>
    </tr>
    <?php
        }
    ?>
</table>