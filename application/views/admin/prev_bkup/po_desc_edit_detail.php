<?php

$po_num = $_REQUEST['po_number'];

//$desc_edit_url = $this->uri->segment(2);

$sql_check ="select * from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num'";

$query_check = $this->db->query($sql_check);

if ($query_check->num_rows() > 0) {

?>
<!---PO Details Start--->

<?php $po_first_three = substr($po_num,0,3); ?>

<?php 
if($po_first_three != 'FPO'){
	 echo '<h3 style="color:red"> You cannot edit description of this Purchase Order....</h3>'; 
	 die;
} 
?>

<br><br>

<form action="<?php echo base_url(); ?>index.php/createpodc/po_desc_edit_entry" method="post" onSubmit="reqd();">

<div class="row">
    <div class="col-lg-6">
        <h4>PO No. &nbsp;&nbsp;<?php echo $po_num;  ?></h4>
    </div>
    <div class="col-lg-6">
        <table border="1" align="center" class="table table-bordered" style="font-size:9px">
            <tr>
                <td><b>CONDITION</b></td>
                <td>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
                <td>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
                <td>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
                <td>FIRST TIME PURCHASE</td>
            </tr>
            <tr>
                <td><b>COLOR</b></td>
                <td style="background-color:red;"></td>
                <td style="background-color:green;"></td>
                <td style="background-color:blue;"></td>
                <td style="background-color:yellow;"></td>
            </tr>
        </table>
    </div>
</div><br />

<!-- Print PO -->

<div class="row">
    <div class="col-lg-2">
    <?php 
	$sql_fet_entry = "select * from tipldb..po_item_desc_edit_table where po_num = '$po_num'"; 
	$qry_fet_entry = $this->db->query($sql_fet_entry);
	
	if($qry_fet_entry->num_rows() > 0){
	
	?>
    <?php if($po_first_three == 'FPO' || $po_first_three == 'fpo'){ ?>
      <a href="<?php echo base_url(); ?>index.php/new_pdfc/view_po_gst_foreign_desc_edit/<?php echo $po_num; ?>"target="_blank" 
      style="font-size:16px; font-weight:bold;">
           <button type="button" class="form-control">PRINT PO</button>
      </a>     
    <?php } ?>
    <?php } ?>
    </div>
</div><br />


<?php foreach ($view_po->result() as $row){
	$po_number         = $row->poitm_pono;
	$supp_name         = $row->supp_spmn_supname;
	$address1          = $row->supp_addr_address1;
	$address2          = $row->supp_addr_address2;
	$address3          = $row->supp_addr_address3;
	$city              = $row->supp_addr_city;
	$state             = $row->supp_addr_state;
	$country           = $row->supp_addr_country;
	$zip               = $row->supp_addr_zip;
	$supplier_email    = $row->supp_addr_email;
	$supplier_phone_no = $row->supp_addr_phone;
	$contact_person    = $row->supp_addr_contperson;
	$comp_add          = $address1.$address2."<br>".$address3.$city." (".$state.") ".$country." ".$zip;
	$po_total_value    = $row->pomas_pobasicvalue;
	$cst               = $row->pomas_tcdtotalrate;
	$tax               = $row->pomas_tcal_total_amount;
	$grand_total_po    = $po_total_value+$cst+$tax;
	$carrier_name      = $row->paytm_carrier;
	$freight           = $row->paytm_incoterm;
	$ipr_age1          = $row->diff1;
	$po_erp_created_by = $row->pomas_createdby;
	$po_erp_created_date = $row->pomas_createddate;
	$po_amend_no       = $row->pomas_poamendmentno;
	$po_age            = $row->diff;
	$po_erp_stauts     = $row->pomas_podocstatus;
	
	if($ipr_age1 == '' || $ipr_age1 == NULL){
		
		$ipr_age = "No IPR age";
		
	} else {
		
		$ipr_age = $ipr_age1." Days";
		
	}
	$freight_place     = $row->paytm_incoplace; 
	$payterm           = $row->paytm_payterm;
	
	$insurance_liablity= $row->paytm_insuranceterm;
	$transport_mode    = $row->paytm_transmode;
	if($freight == "FOR")
	{
		$freight1 = $freight.",&nbsp;Ajmer";
	}
	else if($freight == "FORD")
	{
		$freight1 = $freight.",&nbsp;TIPL Ajmer";
	}
	else
	{
		$freight1 = $freight."&nbsp;";
	}
	
	$sql_live = "select * from tipldb..po_master_table a, tipldb..insert_po b where a.po_num = b.po_num and a.po_num = '".$po_num."'";
	$query_live = $this->db->query($sql_live);
	
	foreach ($query_live->result() as $row) {	
		$po_deli_type = $row->po_deli_type;
		$mat_rec_date = $row->po_lead_time;
		$currency = $row->currency;
		$freight_type = $row->freight_type;
		$approx_freight = $row->approx_freight;
		$ld_applicable = $row->ld_applicable;
		$po_status = $row->status;	
	}
	
?>
<!-- PO ERP Status -->
<div class="row">
	<div class="col-lg-6">
    	<h4 style="text-align:left">
			<?php
				$cha = "PO Live Status : ";
				echo $cha.$po_status;
			?>
        </h4>
    </div>
	<div class="col-lg-6">
    	<h4 style="text-align:right">
			<?php
				$cha = "PO ERP Status : ";
				if($po_erp_stauts == 'MD'){
					echo $cha."Made In Draft";
				} else if($po_erp_stauts == 'SC'){
					echo $cha."Short Closed";
				} else if($po_erp_stauts == 'FR'){
					echo $cha."Fresh";
				} else if($po_erp_stauts == 'DF'){
					echo $cha."Draft";
				} else if($po_erp_stauts == 'DE'){
					echo $cha."Deleted";
				} else if($po_erp_stauts == 'AM'){
					echo $cha."Amended";
				} else if($po_erp_stauts == 'CL'){
					echo $cha."Closed";
				} else if($po_erp_stauts == 'OP'){
					echo $cha."Open";
				} 
			?>
        </h4>
    </div>
</div><br />

<div class="row">
    <div class="col-lg-2">
        <b>Supplier Name</b><br /><?php echo $supp_name; ?>   	
    </div>
    <div class="col-lg-2">
        <b>Supplier Address</b><br /><?php echo $comp_add; ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Email Address</b><br /><?php echo $supplier_email; ?>
    </div>
    <div class="col-lg-2">
        <b>Supp Phone Number</b><br /><?php echo $supplier_phone_no; ?>
    </div>
    <div class="col-lg-2">
        <b>Contact Person</b><br /><?php echo $contact_person; ?>
    </div>
    <div class="col-lg-2" style="background-color:#FF0">
        <b>Order Value <br />(Including Taxes & TCD)</b><br /><?php echo number_format($grand_total_po,2); ?>
    </div>
</div><br /><br />
<div class="row">
	<div class="col-lg-2">
        <b>PO Age</b><br /><?php echo $po_age."&nbsp;Days"; ?>
    </div>
	<div class="col-lg-2">
        <b>IPR Age</b><br /><?php echo $ipr_age; ?>
    </div>
    <div class="col-lg-2" style="background:#FF0">
        <b>Payment Terms</b><br /><?php echo $payterm; ?>  	
    </div>
    <div class="col-lg-2" style="background:#FF0">
        <b>Freight Terms</b><br /><?php echo $freight1; ?>
    </div>
    <div class="col-lg-2" style="background:#FF0">
        <b>Freight Place</b><br /><?php echo $freight_place; ?>
    </div>
    <div class="col-lg-2">
        <b>Carrier Name</b><br /><?php echo $carrier_name; ?>
    </div>
</div><br /><br />
<div class="row">
	<div class="col-lg-2" style="background:#FF0">
        <b>Insurance Term</b><br />
        <?php echo $insurance_liablity; ?>
    </div>
	<div class="col-lg-2">
    	<b>Mode Of Transport</b><br /><?php echo $transport_mode; ?>
    </div>
    <div class="col-lg-2" style="background:#FF0">
    	<b>Delivery Type</b><br /><?php echo $po_deli_type; ?>
    </div>
    
    <div class="col-lg-2">
    	<b>Exp. Mat. Recipt Date</b><br /><?php echo $mat_rec_date; ?>
    </div>
    
    <div class="col-lg-2">
    	<b>Currency</b><br /><?php echo $currency; ?>
    </div> 
     
    <div class="col-lg-2" style="background:#FF0">
    	<b>Freight Type</b><br /><?php echo $freight_type; ?>
    </div>
    
</div><br />
<div class="row">
	<div class="col-lg-2">
    	<b>Approx Freight</b><br /><?php echo $approx_freight; ?>
    </div>  
    <div class="col-lg-2">
    	<b>LD Applicable</b><br /><?php echo $ld_applicable; ?>
    </div>
    <div class="col-lg-2">
    	<b>Special Inst. For Supplier</b><br /><?php echo $ld_applicable; ?>
    </div>  
    <div class="col-lg-2"></div>
    <div class="col-lg-2"></div>  
    <div class="col-lg-2"></div>
</div><br />
<?php break;} ?>

<!--******** ITEM INFORMATION ********-->
<div class="row">
    <div class="col-lg-12" style="text-align:center">
        <h3>ITEM DETAILS</h3>
    </div>
</div>

<div class="row" style=" overflow-x:auto;">
<table class="table table-bordered" id="dataTable" border="1">
    <thead>
      <tr>
        <th>IPR No.</th> 
        <th>Item Code, Description &amp UOM</th>
        <th>PR Qty</th>
		<th>Item Qty In PO</th>        
        <th>Costing</th>
        <th>Last Price</th>
        <th>Current Price</th> 
        <th>Total Item Value</th>
        <th>PR Need Date</th>
        <th>PO Item Need Date</th> 
        <th>PR Type &amp Special Remarks, Category</th> 
        <th>SO &amp ATAC Details</th>
        <th>PR Remarks <br />(Planning & Purchase)</th>
        <th>Supplier Remarks <br />(Planning &amp Purchase)</th>
        <th>MC &amp DI</th>
        <th>Warehouse Code</th>
        <th>Last Supplier Details</th>
      </tr>
    </thead>
    <tbody>
<?php
   $arr = array();
   foreach ($view_po->result() as $row)  
	{   
		$po_line_no        = $row->poprq_polineno;
		$item_desc1        = $row->ml_itemvardesc;
		$item_desc2        = $row->lov_matlspecification;
		$item_desc         = $item_desc1.' '.$item_desc2;
		$item_desc_new     = str_replace("'","",$item_desc);
		$supp_code         = $row->supp_spmn_supcode;
		$supp_name         = $row->supp_spmn_supname;
		$po_ipr_no         = $row->poprq_prno;
		$item_code         = $row->poitm_itemcode;
		$po_date           = $row->pomas_podate;
		
		$item_code1 = urlencode($item_code);
				
		if(strpos($item_code1, '%2F') !== false)
		{
			$item_code2 = str_replace("%2F","chandra",$item_code1);
		}
		else 
		{
			$item_code2 = $item_code1;
		}
		
		$carrier_name      = $row->paytm_carrier;
		$freight           = $row->paytm_incoterm; 
		$payterm           = $row->paytm_payterm;
		$po_quantity       = $row->poitm_order_quantity;
		$for_stk_qty       = $row->poitm_order_quantity;
		$for_stk_qty1      = number_format($for_stk_qty,2);
		$uom               = $row->poitm_puom;
		$current_price     = $row->poitm_po_cost;
		$item_value        = $row->poitm_itemvalue; 
		$po_total_value    = $row->pomas_pobasicvalue;
		$po_date           = $row->pomas_podate;
		$cost_pr_unt       = $row->poitm_costper;
		$need_date         = $row->poitm_needdate;
		$wh_code           = $row->poitm_warehousecode;
		$odr_qty           = $row->poitm_order_quantity;
		$totalcost         = $odr_qty * $cost_pr_unt;
		$address1          = $row->supp_addr_address1;
		$address2          = $row->supp_addr_address2;
		$address3          = $row->supp_addr_address3;
		$city              = $row->supp_addr_city;
		$state             = $row->supp_addr_state;
		$country           = $row->supp_addr_country;
		$zip               = $row->supp_addr_zip;
		$comp_add          = $address1.$address2."<br>".$address3.$city." (".$state.") ".$country." ".$zip;
		$supp_email        = $row->supp_addr_email;
		$currency          = $row->pomas_pocurrency;
		//new pdf elements
		$po_amend_no 	   = $row->pomas_poamendmentno;
		$po_type 		   = $row->pomas_potype;
		$supp_phone        = $row->supp_addr_phone;
		$contact_person    = $row->supp_addr_contperson;
		$pay_mode          = $row->paytm_paymode;
		$trans_mode        = $row->paytm_transmode;
		$partial_ship      = $row->paytm_shippartial;
		$po_item_need_date = $row->poitm_needdate;
			
		$sql1 ="exec tipldb..pendalcard '$item_code'";
		$sql2 ="select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$item_code'";
		$sql3 ="select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$item_code'";
		$sql4 ="select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$item_code'";
		//Last Rate
		$sql5 ="select top 1 poitm_po_cost,* from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b where poitm_itemcode = '$item_code'
and a.pomas_pono = b.poitm_pono and a.pomas_poou = b.poitm_poou and a.pomas_podocstatus NOT IN('DE')
and a.pomas_poamendmentno = b.poitm_poamendmentno 
and a.pomas_poamendmentno = (SELECT MAX(pomas_poamendmentno) FROM scmdb..po_pomas_pur_order_hdr WHERE pomas_pono = a.pomas_pono AND pomas_poou = a.pomas_poou)
and a.pomas_pono in (select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderdoc = 'PO')
order by pomas_poauthdate desc";

		$sql6 ="select * from tipldb..pr_submit_table where pr_num = '$po_ipr_no' and item_code = '$item_code'";
		$sql7 ="select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$item_code' and PendStatus = 'OPEN'";
		$sql8 ="select * from TIPLDB..road_permit_state a, scmdb..supp_addr_address b where a.state_code = b.supp_addr_state 
		and supp_addr_supcode = '$supp_code'";
		$sql_draw = "select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$item_code'";
		$sql_current_yr_con = "select RecptTotalQty from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$item_code'";
		$sql_last_supp_details = "select SuppName, SuppRate, SuppLeadTime from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
		$sql_reorder_detail = "select iou_reorderlevel, iou_reorderqty from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
		
		$query1 = $this->db->query($sql1);
		$this->db->close();
		$this->db->initialize();
		$query2 = $this->db->query($sql2);
		$query3 = $this->db->query($sql3);
		$query4 = $this->db->query($sql4);
		$query5 = $this->db->query($sql5);
		$query6 = $this->db->query($sql6);
		$query7 = $this->db->query($sql7);
		$query8 = $this->db->query($sql8);
		$query_draw = $this->db->query($sql_draw);
		$query_current_yr_con = $this->db->query($sql_current_yr_con);
		$query_last_supp_detail = $this->db->query($sql_last_supp_detail);
		$query_reorder_details = $this->db->query($sql_reorder_details);
			
		if ($query2->num_rows() > 0) {
		  foreach ($query2->result() as $row) {
			  $lastyr_cons = $row->ConsTotalQty;
			  $lastyr_cons1 = number_format($lastyr_cons,2);
			}
		} else {
			  $lastyr_cons = 0;
			  $lastyr_cons1 = 0.00;
		}
		
		if ($query3->num_rows() > 0) {
			  $current_stk = 0;
		  foreach ($query3->result() as $row) {
			  $current_stock = $row->ItemStkAccepted;
			  $current_stk = $current_stk + $current_stock;
			  $current_stk1 = number_format($current_stk,2);
			}
		} else {
			  $current_stk = 0;
			  $current_stk1 = 0.00;
		}
		
		if ($query4->num_rows() > 0) {
		  foreach ($query4->result() as $row) {
			  $reservation_qty = $row->AllocPendingTot;
			  $reservation_qty1 = number_format($reservation_qty,2);
			}
		} else {
			  $reservation_qty = 0;
			  $reservation_qty1 = 0.00;
		}
		
		if ($query5->num_rows() > 0) {
		  foreach ($query5->result() as $row) {
			  $last_price = $row->poitm_po_cost;
			  $last_price1 = number_format($last_price,2);
			}
		} else {
			  $last_price = "";
			  $last_price1 = "";
		}
		
		if ($query6->num_rows() > 0) {
		  foreach ($query6->result() as $row) {
				$ipr_type            = $row->usage;
				$sono                = $row->sono;
				$atac_no             = $row->atac_no;
				$atac_ld_date        = $row->atac_ld_date;
				$atac_need_date      = $row->atac_need_date;
				$atac_payment_terms  = $row->atac_payment_terms;
				$pm_group            = $row->pm_group;
				$category            = $row->category;
				$project_name        = $row->project_name;
				$costing             = $row->costing;
				$ipr_remarks         = $row->remarks;
				$attached_cost_sheet = $row->attch_cost_sheet;
				$ipr_need_date       = $row->need_date;
				$manufact_clrnce	 = $row->manufact_clearance;
				$dispatch_inst       = $row->dispatch_inst;
				$supp_item_remarks   = $row->pr_supp_remarks; 
				$pr_qty              = number_format($row->required_qty,2);
				
				$arr[] = $category;
			}
		} else {
				$ipr_type            = "";
				$sono                = "";
				$atac_no             = "";
				$pm_group            = "";
				$category            = "";
				$project_name        = "";
				$costing             = "";
				$ipr_remarks         = "";
				$ipr_need_date       = "";
				$atac_ld_date        = "";
				$atac_need_date      = "";
				$atac_payment_terms  = "";
				$attached_cost_sheet = "";

				$manufact_clrnce	 = "";
				$dispatch_inst       = "";
				$supp_item_remarks   = "";
				$pr_qty              = ""; 	  
		}
		
		if ($query7->num_rows() > 0) {
		  $incoming_qty_tot1 = 0;
		  foreach ($query7->result() as $row) {
			  $incoming_qty = $row->PendPoSchQty;
			  $incoming_qty_tot1 = $incoming_qty_tot1 + $incoming_qty;
			  $incoming_qty_tot  = number_format($incoming_qty_tot1,2);
			}
		} else {
			  $incoming_qty_tot = "";
		}
		
		if ($query8->num_rows() > 0) {
		  foreach ($query8->result() as $row) {
			  $supp_state = $row->state_code;
			  $road_permit_req = $row->road_permit;
			}
		} else {
			  $supp_state = "";
			  $road_permit_req = "";
		}
		
		if ($query_draw->num_rows() > 0) {
		  foreach ($query_draw->result() as $row) {
			  $drawing_no = $row->DrawingNo;
			  $purchase_uom  = $row->ItemPurcaseUom;
			  $manufact_uom = $row->ItemMnfgUom;
			}
		} else {
			  $drawing_no = "";
			  $purchase_uom  = "";
			  $manufact_uom = "";
		}
		
		if ($query_current_yr_con->num_rows() > 0) {
		  foreach ($query_current_yr_con->result() as $row) {
			  $current_yr_con = $row->RecptTotalQty;
			}
		} else {
			  $current_yr_con = "";
		}
		
		if ($query_last_supp_detail->num_rows() > 0) {
		  foreach ($query_last_supplier_details->result() as $row) {
			  $SuppName = $row->SuppName; 
			  $SuppRate = $row->SuppRate; 
			  $SuppLeadTime = $row->SuppLeadTime;
			}
		} else {
			  $SuppName = ""; 
			  $SuppRate = ""; 
			  $SuppLeadTime = "";
		}
		
		if ($query_reorder_details->num_rows() > 0) {
		  foreach ($query_reorder_details->result() as $row) {
			  $iou_reorderlevel = $row->iou_reorderlevel; 
			  $iou_reorderqty = $row->iou_reorderqty;
			}
		} else {
			  $iou_reorderlevel = ""; 
			  $iou_reorderqty = "";
		}
		
		$sql = "select top 1 * from scmdb..itm_ucon_conversion where ucon_fromuom = '$manufact_uom' and ucon_touom = '$purchase_uom' and ucon_itemcode='$item_code'";

		$query = $this->db->query($sql);
		
		if ($query->num_rows() > 0) {
		  foreach ($query->result() as $row) {
			  $ucon_confact_ntr = $row->ucon_confact_ntr;
			  $ucon_confact_dtr = $row->ucon_confact_dtr;
			  $conversion_factor = $ucon_confact_ntr / $ucon_confact_dtr;
		  }
		} else {
			   $conversion_factor = "";  
		}
		/* */ 
		//Supplier Average Lead Time
					
		$sql_supplier = "select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$item_code'";
		$query_supplier = $this->db->query($sql_supplier);
		
		if ($query_supplier->num_rows() > 0) {
			$supplier_lead_time_tot = 0;
			$counter = 0;
		  foreach ($query_supplier->result() as $row) {
			  $counter++;
			  $supplier_lead_time = $row->SuppLeadTime;
			  $supplier_lead_time_tot = $supplier_lead_time_tot+$supplier_lead_time; 
		  }
		  $supplier_avg_lead_time = $supplier_lead_time_tot/$counter;
		} else {
			   $counter = 0;
			   $supplier_lead_time = "";
			   $supplier_lead_time_tot = ""; 
			   $supplier_avg_lead_time = 0;  
		}
        /* */
		
		$sql_supplier = "exec TIPLDB..supplier_details  '1' , '$item_code'  , '$po_ipr_no'";
		$query_supplier = $this->db->query($sql_supplier);
		
		$this->db->close();
		$this->db->initialize();
		
		$sql_supplier_dtl = "select * from TIPLDB..supplier_dtl where user_id=1";
		$query_supplier_dtl = $this->db->query($sql_supplier_dtl);
		
		 foreach ($query_supplier_dtl->result() as $row) {
			$diffdays    = $row->diffDays;
		 }
		 
		//echo "Cha==".$diffdays;
		
		if($diffdays != NULL){
			if($diffdays > 0){
				$color = "red";
			} else if($diffdays < 0){
				$color = "green";
			} else if($diffdays = 0){
				$color = "blue";
			}
		} else {
			$color = "yellow";
		}
		
		
?>         
      <tr> 
      	<input type="hidden" name="po_num" id="po_num" value="<?php echo $po_num; ?>">           
        <td>
            <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($po_ipr_no); ?>" target="_blank">
            <?php echo $po_ipr_no; ?></a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" 
            target="_blank"><?php echo $item_code; ?></a><br /><br />
            <input type="hidden" name="item_code[]" id="item_code" value="<?php echo $item_code; ?>" />
            <textarea id="edit_desc" class="form-control" name="edit_desc[]"><?php echo $item_desc; ?></textarea><br /><br />
            
            <?php echo "<b>UOM - </b>".$uom; ?>
        </td>
        <td><?php echo number_format($pr_qty,2); ?></td>
        <td><?php echo number_format($po_quantity,2); ?></td>
        <td>
            <a href='<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>' target='_blank'>
            	<?php echo $costing; ?>
            </a>
        </td>
        <td><?php echo $last_price1; ?></td>
        <td><?php echo number_format($current_price,2); ?></td>
        <td><?php echo number_format($item_value,2); ?></td>
        <td style="background-color:<?php echo $color; ?>"><?php echo substr($ipr_need_date,0,11); ?></td>
        <td style="background-color:<?php echo $color; ?>"><?php echo substr($po_item_need_date,0,11); ?></td>
        <td>
			<?php if($ipr_type != '' || $ipr_type != NULL ){ echo "<b>PR TYPE - </b>".$ipr_type; } ?><br /><br />
            <?php if($category != '' || $category != NULL ){ echo "<b>CATEGORY - </b>".$category; } ?>
        </td>
        <td>
			<?php if($sono != '' || $sono != NULL ){ echo "<b>SONO - </b>".$sono; } ?><br /><br />
            <a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?php echo $atac_no; ?>">
                <?php if($atac_no != '' || $atac_no != NULL ){ echo "<b>ATAC NO - </b>".$atac_no; } ?>
            </a><br /><br />
            <?php if($atac_ld_date != '' || $atac_ld_date != NULL ){ echo "<b>ATAC LD DATE - </b>".$atac_ld_date; } ?><br /><br />
            <?php if($atac_need_date != '' || $atac_need_date != NULL ){ echo "<b>ATAC NEED DATE - </b>".$atac_need_date; } ?><br /><br />
            <?php if($atac_payment_terms != '' || $atac_payment_terms != NULL ){ echo "<b>ATAC PAYMENT TERMS - </b>".$atac_payment_terms; } ?><br /><br />
            <?php if($pm_group != '' || $pm_group != NULL ){ echo "<b>PM GROUP - </b>".$pm_group; } ?><br /><br />
            <?php if($project_name != '' || $project_name != NULL ){ echo "<b>PROJECT NAME - </b>".$project_name; } ?>
        </td>
        <td>
			<?php if($ipr_remarks != '' || $ipr_remarks != NULL ){ echo "<b>PLANNING - </b>".$ipr_remarks; } ?>
        </td>
        <td><?php echo $supp_item_remarks; ?></td>
        <td>
			<?php if($manufact_clrnce != '' || $manufact_clrnce != NULL ){ echo "<b>MC -</b>".$manufact_clrnce; } ?><br /><br />
            <?php if($dispatch_inst != '' || $dispatch_inst != NULL ){ echo "<b>DI -</b>".$dispatch_inst; } ?>
        </td>
        <td><?php echo $wh_code; ?></td>
        <td>
			<?php if($SuppName != '' || $SuppName != NULL ){ echo "<b>LAST SUPP NAME -</b>".$SuppName; }?><br /><br />
            <?php if($SuppRate != '' || $SuppRate != NULL ){ echo "<b>LAST SUPP RATE -</b>".number_format($SuppRate,2); } ?><br /><br />
            <?php if($SuppLeadTime != '' || $SuppLeadTime != NULL ){ echo "<b>LAST SUPP LEAD TIME -</b>".number_format($SuppLeadTime,2); } ?><br /><br />
        </td>
      </tr>
    <?php }?> 
 </tbody>
</table>
</div><br /><br />

<div class="row">
	<div class="col-lg-6">
    	<b>Attached Supplier Quotes</b><br><br>
        <table class="table table-borderd">
        	<tr style="background-color:#0CF">
            	<td><b>SNO</b></td>
                <td><b>FILE NAME</b></td>
                <td><b>DATETIME</b></td>
            </tr>
            
            <?php 	
			$sql_supp_quotes ="select * from TIPLDB..po_supplier_quotes where po_num = '$po_num'";
			$query_supp_quotes = $this->db->query($sql_supp_quotes);
							
			
		    $cnt = 0;
		    foreach ($query_supp_quotes->result() as $row) {
				$cnt++;
			    $supp_quotes = $row->attached_supp_quotes;
				$date_time = $row->date_time;
			?>
            
            <tr>
            	<td><?php echo $cnt; ?></td>
                <td><a href='<?php echo base_url(); ?>uploads/<?php echo $supp_quotes; ?>' target='_blank'><?php echo $supp_quotes; ?></a><br /></td>
                <td><?php echo substr($date_time,0,11); ?></td>
            </tr>
            <?php } ?>  
        </table>
    </div>
    <div class="col-lg-6">
    	<b>Remarks</b>
    </div>
</div><br />

<div class="row">
	<div class="col-lg-5"></div>
    <div class="col-lg-2">
    	<input type="submit" name="submit" id="submit" value="SUBMIT" class="form-control" />
    </div>
    <div class="col-lg-5"></div>
</div>

<?php //chat history ?>
<div class="row">
    <div class="col-lg-12">
        <h4>Chat History</h4>	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
                <tr>
                    <th>LEVEL</th>
                    <th>NAME</th>
                    <th>COMMENT</th>
                    <th>INSTRUCTION</th>
                    <th>DATE TIME</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            $sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
            $query1 = $this->db->query($sql1);
            if ($query1->num_rows() > 0) {
            foreach ($query1->result() as $row) {
            $level = $row->level;
            $name  = $row->comment_by;
            $comment = $row->comment;
            $instruction = $row->instruction;
            $datentime = $row->datentime;
            
            ?>
                <tr>
                    <td><?php echo $level; ?></td>
                    <td><?php echo $name; ?></td>
                    <td><?php echo $comment; ?></td>
                    <td><?php echo $instruction; ?></td>
                    <td><?php echo $datentime; ?></td>                            
                </tr>
            <?php 
            } } 
            ?>
            </tbody>
        </table>    
    </div>
</div><br />
<?php //chat history ?>

<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12">
    	<h4>Action Timing</h4>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<table align="center" class="table table-bordered">
        	<thead>
            	<th>ERP CREATED</th>
                <th>ERP CREATED DATE</th>
                <th>ERP LAST MODIFIED</th>
                <th>ERP LAST MODIFIED DATE</th>
            </thead>
         	<?php foreach($view_po->result() as $row){ ?>
            <tbody>
            	<td><?php echo $row->pomas_createdby; ?></td>
                <td><?php echo $row->pomas_createddate; ?></td>
                <td><?php echo $row->pomas_lastmodifiedby; ?></td>
                <td><?php echo $row->pomas_lastmodifieddate; ?></td>
            </tbody>
            <?php break; } ?>
        </table>    
    </div>
</div>

<?php //Action Timing Report ?>

<!---PO Details Ends--->
<?php	
	
} else {
	  echo "<br><br><h4 style='text-align:center; color:red;'>Enter Valid PO Number...</h4>";
}

?>