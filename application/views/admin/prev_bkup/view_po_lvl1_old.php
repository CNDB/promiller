<!--main content start-->
<!--- PO Type ----->
<?php

	foreach ($view_po->result() as $row){
		$po_from_screen = $row->po_from_screen;
	}
		
	if($po_from_screen == 'fresh') {	
		$po_stat = "Fresh PO";	
	} else if($po_from_screen == 'disapprove_lvl1'){
		$po_stat = "Disapprove PO LVL1";
	} else if($po_from_screen == 'disapprove_lvl2'){
		$po_stat = "Disapprove PO LVL2";
	} else if($po_from_screen == 'amend_po'){
		$po_stat = "Amended PO";
	}

?>

<div class="row">
    <div class="col-lg-12">
           <h3><?php echo $po_stat; ?></h3>
     </div>
</div><br />

<form action="<?php echo base_url(); ?>index.php/createpodc/insert_po_sub_lvl1" method="post" enctype="multipart/form-data" onsubmit="return reqd()">

<?php include('po_details_div.php'); ?>

<!--****** REMARKS *******--> 
<?php
  foreach ($view_po->result() as $row)  {
	  $po_num_new = $row->po_num; 
  }
?>                      
<div class="row">
    <div class='col-lg-3'> 
    <b>Attach Quotes From Supplier</b>
    </div>
    <div class='col-lg-3'> 
    <?php 	
		$sql8 ="select * from tipldb..po_supplier_quotes where po_num = '$po_num_new' order by date_time desc";
		$query8 = $this->db->query($sql8);
						
		if ($query8->num_rows() > 0) {
		  foreach ($query8->result() as $row) {
			  $supp_quotes = $row->attached_supp_quotes;
	?>
			  <a href="<?php echo base_url(); ?>uploads/<?php echo $supp_quotes; ?>" target="_blank"><?php echo $supp_quotes; ?></a><br />
    <?
			}
		} else {
			  $supp_quotes = "";
		}	
    ?>
    </div>
    <div class="col-lg-1">
     <b> Remarks:</b>
    </div>
    <div class="col-lg-5">
    <?php
    	foreach ($view_po->result() as $row){ 
    ?>
      <b><?php echo $row->po_rmks; ?></b>
    <?php 
		break;} 
	?>
    </div>
</div><br /> 

<?php //lock for po approval by abhinav sir ?>
<?php 
	$username = strtolower($_SESSION['username']);
	
	$sql_po_approval = "select * from tipldb..po_master_table where po_num = '$po_num'";

	$query_approval_sql = $this->db->query($sql_po_approval);
	
	foreach ($query_approval_sql->result() as $row) {
	  $level1_mail_to = $row->level1_mail_to;
	}
		
if(($username == 'admin' || $username == 'abhinav.toshniwal' || $username == $level1_mail_to)||(($category == 'SECURITY') && $username == 'shubham.soni' ) ){
?>
            
<div class="row">
    <div class="col-lg-2">
    <b>Approve Disapprove Comments:</b>  
    </div>
    <div class="col-lg-4"> 
    <input type="text" name="app_disapp_comnts_lvl1" id="app_disapp_comnts_lvl1" class="form-control"  value="" />  
    </div>
    <div class="col-lg-3"> 
    <input type="submit" name="po_approval_lvl1" value="Approve" class="form-control" id="po_approval_lvl1"
    style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px; display:block;" onclick="hide('po_approval_lvl1')" />     
    </div>
    <div class="col-lg-3"> 
    <input type="submit" name="po_approval_lvl1" value="Disapprove" class="form-control" id="po_approval_lvl1"
    style="font-weight:bold; background:#000000; color:#FFFFFF; letter-spacing:2px; display:block;" onclick="hide('po_approval_lvl1')"/>    
    </div>
</div><br /><br />

<?php
} else {
	echo "<h3 style='color:red; text-align:center'>You are not authorized to approve/disapprove this PO</h3>";
}
?>
<?php //lock for po approval by abhinav sir ?>
          
</form>

 <?php //chat history ?>
 <div class="row">
  <div class="col-lg-12">
    <h3>Chat History</h3>	
        <table class="table table-bordered" id="dataTable" border="1">
            <thead>
              <tr>
                <th>LEVEL</th>
                <th>NAME</th>
                <th>COMMENT</th>
                <th>INSTRUCTION</th>
                <th>DATE TIME</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                $sql1 ="select * from tipldb..insert_po_comment where po_num = '$po_num' order by datentime asc";
                $query1 = $this->db->query($sql1);
                if ($query1->num_rows() > 0) {
				foreach ($query1->result() as $row) {
				  $level = $row->level;
				  $name  = $row->comment_by;
				  $comment = $row->comment;
				  $instruction = $row->instruction;
				  $datentime = $row->datentime;
                                
               ?>
              <tr>
                <td><?php echo $level; ?></td>
                <td><?php echo $name; ?></td>
                <td><?php echo $comment; ?></td>
                <td><?php echo $instruction; ?></td>
                <td><?php echo $datentime; ?></td>                            
              </tr>
              <?php 
                } } 
              ?>
            </tbody>
       </table>    
    </div>
 </div>
 <?php //chat history ?>
 
<?php //Action Timing Report ?>

<div class="row">
    <div class="col-lg-12">
    	<h3>Action Timing</h3>    
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
    	<table align="center" class="table table-bordered">
        	<thead>
            	<th>ERP CREATED</th>
                <th>ERP CREATED DATE</th>
                <th>ERP LAST MODIFIED</th>
                <th>ERP LAST MODIFIED DATE</th>
                <th>LIVE CREATED</th>
                <th>LIVE CREATED DATE</th>
            </thead>
         	<?php foreach($view_po->result() as $row){ ?>
            <tbody>
            	<td><?php echo $row->pomas_createdby; ?></td>
                <td><?php echo $row->pomas_createddate; ?></td>
                <td><?php echo $row->pomas_lastmodifiedby; ?></td>
                <td><?php echo $row->pomas_lastmodifieddate; ?></td>
                <td style="text-transform:uppercase;"><?php echo $row->po_approvedby_lvl0; ?></td>
                <td><?php echo $row->po_approveddate_lvl0; ?></td>
            </tbody>
            <?php break; } ?>
        </table>    
    </div>
</div>

<?php //Action Timing Report ?>
      		
<!--main content end-->
  
<!-- container section end -->
      
<?php include('footer.php'); ?>