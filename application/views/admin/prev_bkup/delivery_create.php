<?php
	include'header.php';
	$nik = $this->uri->segment(3);                      
?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PENDING FOR DELIVERY DETAILS</h4>
        </div>
    </div><br />
    
    <?php include('po_header.php'); ?>
    
  </section>
</section>
      
<script type="text/javascript">
	function check(str,whre_nik)
	{
	 $("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/loading.gif" width="317px" height="58px" style="margin-left:150px" />');
	 
	 
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  	xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200 && whre_nik == 'from_pr')
		{
			document.getElementById('detail').innerHTML=xmlhttp.responseText;
			$( "#datepicker1" ).datepicker();		   
		} 
	  }
	
	 var queryString="?q="+str;
	 if(whre_nik == 'from_pr') {		 	
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/deliveryc/view_po_lvl2" + queryString,true); 
	 }	    
	 xmlhttp.send();	
	}

</script>
<script type="text/javascript" >
function reqd()
  {
	  var docket_detail = document.getElementById("docket_detail").value;
	  var transporter = document.getElementById("transporter").value;
	  var datepicker1 = document.getElementById("datepicker1").value;
	  
	  if(docket_detail == ""){
		  alert("Please enter docket detail !!");
		  document.getElementById("docket_detail").focus();
		  return false;
	  }
	  
	  if(transporter == ""){
		  alert("Please enter transporter name !!");
		  document.getElementById("transporter").focus();
		  return false;
	  }
	  
	  if(datepicker1 == ""){
		  alert("Please select delivery date !!");
		  document.getElementById("datepicker1").focus();
		  return false;
	  }
  }
</script>
 
<script type="text/javascript" >
	$(document).ready(function(){	
	
		check('<? echo $nik; ?>','from_pr');
	
	});
</script>
 

<?php include('footer.php'); ?>