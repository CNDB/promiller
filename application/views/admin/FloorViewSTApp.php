<?php include'header.php'; echo "Hello ! ".$username = $_SESSION['username']; ?>
<?php $vhcl_reg_no = $_REQUEST['vhcl_reg_no']; ?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Add Wheat Vehicles</li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row">
        <div class="col-lg-2"><a href="<?php echo base_url(); ?>index.php/floorc/floorRpt">Reports</a></div>
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-12">
        	<h2>Pending For Sample Test Approval</h2>
        </div>
    </div><br /><br />
    
    <?php 
	$sql = "select * from vehicle_entry where vhcl_reg_no = '$vhcl_reg_no'";
	$qry =  $this->db->query($sql);
	foreach($qry->result() as $row){
		$vhcl_reg_no = $row->vhcl_reg_no;
		$vhcl_type = $row->vhcl_type;
		$vehicle_arr_tm = $row->vehicle_arr_tm;
		$driver_nm = $row->driver_nm;
		$doc_by_driver = $row->doc_by_driver;
		$vehicle_arr_plc = $row->vehicle_arr_plc;
		$vehicle_wht_scale = $row->vehicle_wht_scale;
		$vehicle_wht_bill = $row->vehicle_wht_bill;
		
		$moist_perc = $row->moist_perc;
		$hdhlw_perc = $row->hdhlw_perc;
		$murgi_perc = $row->murgi_perc;
		$stone_perc = $row->stone_perc;
		$fg_perc = $row->fg_perc;
		$dantal_perc = $row->dantal_perc;
		$chilka_perc = $row->chilka_perc;
		$stat_wheat = $row->stat_wheat;
		$driver_mob_no = $row->driver_mob_no;
	}
	
	?>
    
    <form action="<?php echo base_url(); ?>index.php/floorc/VhclSTApp" method="post">
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Arrival Time</b></div>
        <div class="col-lg-3"><input type="text" id="vehicle_arr_tm" name="vehicle_arr_tm" value="<?php echo $vehicle_arr_tm; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Type</b></div>
        <div class="col-lg-3">
        <select id="vhcl_type" name="vhcl_type" class="form-control" required>
        	<option value="<?php echo $vehicle_arr_tm; ?>"><?php echo $vhcl_type; ?></option>
        	<option value="">--Select--</option>
            <option value="Truck">Truck</option>
            <option value="Pickup">Pickup</option>
            <option value="Tempo">Tempo</option>
            <option value="Other">Other</option>
        </select>
        </div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Registration Number</b></div>
        <div class="col-lg-3"><input type="text" id="vhcl_reg_no" name="vhcl_reg_no" value="<?php echo $vhcl_reg_no; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Driver's Name</b></div>
        <div class="col-lg-3"><input type="text" id="driver_nm" name="driver_nm" value="<?php echo $driver_nm; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Driver's Mobile Number</b></div>
        <div class="col-lg-3"><input type="text" id="driver_mob_no" name="driver_mob_no" value="<?php echo $driver_mob_no; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Documents Given By Driver</b></div>
        <div class="col-lg-3"><input type="text" id="doc_by_driver" name="doc_by_driver" value="<?php echo $doc_by_driver; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Vehicle Arrived From Place</b></div>
        <div class="col-lg-3"><input type="text" id="vehicle_arr_plc" name="vehicle_arr_plc" value="<?php echo $vehicle_arr_plc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Weight Of Wheat in the vehicle as per Scale</b></div>
        <div class="col-lg-3"><input type="text" id="vehicle_wht_scale" name="vehicle_wht_scale" value="<?php echo $vehicle_wht_scale; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Weight of Wheat mentioned in the Bill</b></div>
        <div class="col-lg-3"><input type="text" id="vehicle_wht_bill" name="vehicle_wht_bill" value="<?php echo $vehicle_wht_bill; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Moisture Percent</b></div>
        <div class="col-lg-3"><input type="text" id="moist_perc" name="moist_perc" value="<?php echo $moist_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>H.D.H.L.W Percentage</b></div>
        <div class="col-lg-3"><input type="text" id="hdhlw_perc" name="hdhlw_perc" value="<?php echo $hdhlw_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Murgidana Percentage</b></div>
        <div class="col-lg-3"><input type="text" id="murgi_perc" name="murgi_perc" value="<?php echo $murgi_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Stone Percentage</b></div>
        <div class="col-lg-3"><input type="text" id="stone_perc" name="stone_perc" value="<?php echo $stone_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Foreign Grain Percentage</b></div>
        <div class="col-lg-3"><input type="text" id="fg_perc" name="fg_perc" value="<?php echo $fg_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Dantel Percentage</b></div>
        <div class="col-lg-3"><input type="text" id="dantal_perc" name="dantal_perc" value="<?php echo $dantal_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Chilka Percentage</b></div>
        <div class="col-lg-3"><input type="text" id="chilka_perc" name="chilka_perc" value="<?php echo $chilka_perc; ?>" class="form-control" /></div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-3"></div>
        <div class="col-lg-3"><b>Status of Wheat</b></div>
        <div class="col-lg-3">
        	<select id="stat_wheat" name="stat_wheat" class="form-control">
            	<option value="<?php echo $stat_wheat; ?>"><?php echo $stat_wheat; ?></option>
            	<option value="">--select--</option>
                <option value="Ok">Ok</option>
                <option value="Not Ok">Not Ok</option>
            </select>
        </div>
        <div class="col-lg-3"></div>
    </div><br />
    
    <div class="row" style="text-align:left">
    	<div class="col-lg-4"></div>
        <div class="col-lg-2"><input type="submit" id="submit_btn_app" name="submit_btn_app" value="Approve" class="form-control" /></div>
        <div class="col-lg-2"><input type="submit" id="submit_btn_disapp" name="submit_btn_disapp" value="Disapprove" class="form-control" /></div>
        <div class="col-lg-4"></div>
    </div><br />
    </form>
  </section>
</section>
<?php include('footer.php'); ?>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script>
$('#vehicle_arr_tm').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '24',
    maxTime: '11:55pm',
    defaultTime: '11',
    startTime: '24:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});

$('#vehicle_dep_tm').timepicker({
    timeFormat: 'h:mm p',
    interval: 5,
    minTime: '24',
    maxTime: '11:55pm',
    defaultTime: '11',
    startTime: '24:00',
    dynamic: false,
    dropdown: true,
    scrollbar: true
});
</script>