<table class="table table-bordered" id="all_fresh_po" border="1">
  <tr style="background-color:#CCC">
    <td>PO NO.</td>
    <td>SUPPLIER NAME</td>
    <td>ORDER VALUE</td>
    <td>PO AGE</td>
    <td>PR AGE</td>
    <td>PENDING ON</td>
  </tr>
  <?php 
      foreach ($amend_po_new->result() as $row) { 
      $pono = $row->pomas_pono;
      $po_basic_value = $row->pomas_pobasicvalue;
      $po_cst_value = $row->pomas_tcdtotalrate; 
      $po_tax_value = $row->pomas_tcal_total_amount;
      $po_total_value = $po_basic_value + $po_cst_value + $po_tax_value;
      $po_age = $row->diff;
	  $pending_on = $row->pomas_createdby;
	  $supp_name = $row->po_supp_name;
      
      $sql1 = "select *, datediff(DAY,c.preqm_prdate,getdate()) as diff1 from scmdb..po_pomas_pur_order_hdr a, 
		scmdb..po_poprq_poprcovg_detail b, scmdb..prq_preqm_pur_reqst_hdr c,
		scmdb..supp_spmn_suplmain d where a.pomas_pono = b.poprq_pono and b.poprq_prno = c.preqm_prno 
		and a.pomas_suppliercode = d.supp_spmn_supcode and a.pomas_pono = '$pono' and a.pomas_poamendmentno = b.poprq_poamendmentno 
		and a.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono')";
      $query1 = $this->db->query($sql1);
	  if($query1->num_rows() > 0){
		  foreach ($query1->result() as $row) {
			$pr_age = $row->diff1;
		  }
	  } else {
		  $pr_age = "";
	  }
  ?>
  <tr>
    <td><a href="<?php echo base_url(); ?>index.php/createpodc/view_po_new/<?php echo $pono; ?>/amend_po">
        <?php echo $pono; ?></a>
    </td><!--PO Number-->
    <td><?php echo $supp_name; ?></td><!--Supplier Name-->
    <td>
        <?php
            echo number_format($po_total_value,2);
        ?>
    </td><!--Order Value-->
    <td><?php echo $po_age;  ?></td><!--PO Age (In Days)-->
    <td><?php echo $pr_age;  ?></td><!--PR Age (In Days)-->
    <td><?php echo $pending_on;  ?></td><!--Pending On-->
  </tr>
  <?php } ?>
</table>