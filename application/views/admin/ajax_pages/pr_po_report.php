<?php //ERP AUTHORIZED PR WHOSE PO NOT CREATED ?>
<?php 
	$this->load->helper('flow');
	$all_pr = "FPR','IPR','LPR','CGP";
?>

<label>Sort:</label>
<select id="sort_pr" name="sort_pr" onchange="show_content_pr_po_report(this.value);">
	<option value="ALL">ALL</option>
    <option value="FPR">FPR</option>
    <option value="IPR">IPR</option>
    <option value="LPR">LPR</option>
</select>

<!-- ALL -->

<table class="table table-bordered" id="all_pr" border="1" style="font-size:10px;">
  <?php echo det_pr_po_pend($all_pr); ?>
</table>

<!-- FPR -->

<table class="table table-bordered" id="all_fpr" border="1" style="display:none; font-size:10px;">
  <?php echo det_pr_po_pend('FPR'); ?>
</table>

<!-- IPR -->

<table class="table table-bordered" id="all_ipr" border="1"  style="display:none; font-size:10px;">
  <?php echo det_pr_po_pend('IPR'); ?>
</table>

<!-- LPR -->

<table class="table table-bordered" id="all_lpr" border="1"  style="display:none; font-size:10px;">
  <?php echo det_pr_po_pend('LPR'); ?>
</table>