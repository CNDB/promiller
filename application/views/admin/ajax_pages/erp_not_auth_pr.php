<?php //Authorize Purchase Request Purchase ?>
<?php 
	$this->load->helper('flow');
	$all_pr = "FPR','IPR','LPR','CGP";
?>

<label>Sort:</label>
<select id="sort_pr" name="sort_pr" onchange="show_content_erp_not_auth_pr(this.value);">
	<option value="ALL">ALL</option>
    <option value="FPR">FPR</option>
    <option value="IPR">IPR</option>
    <option value="LPR">LPR</option>
</select>

<!-- ALL -->
<table class="table table-bordered" id="all_pr" border="1">
  <?php echo det_erp_not_auth_pr($all_pr); ?>
</table>

<!-- FPR -->
<table class="table table-bordered" id="all_fpr" border="1" style="display:none;">
  <?php echo det_erp_not_auth_pr('FPR'); ?>
</table>

<!-- IPR -->
<table class="table table-bordered" id="all_ipr" border="1" style="display:none;">
  <?php echo det_erp_not_auth_pr('IPR'); ?>
</table>

<!-- LPR -->
<table class="table table-bordered" id="all_lpr" border="1" style="display:none;">
  <?php echo det_erp_not_auth_pr('LPR'); ?>
</table>