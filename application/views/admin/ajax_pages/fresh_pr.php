<?php $this->load->helper('flow'); ?>

<label>Sort:</label>
<select id="sort_pr" name="sort_pr" onchange="show_content_fresh_pr(this.value);">
	<option value="ALL">ALL</option>
    <option value="FPR">FPR</option>
    <option value="IPR">IPR</option>
    <option value="LPR">LPR</option>
</select>

<?php $all_pr = "FPR','IPR','LPR','CGP"; ?>

<!-- ALL -->
<table class="table table-bordered" id="all_pr" border="1">
  <?php echo fresh_pr($all_pr); ?>
</table>

<!-- FPR -->
<table class="table table-bordered" id="all_fpr" border="1" style="display:none;">
  <?php echo fresh_pr('FPR'); ?>
</table>

<!-- IPR -->
<table class="table table-bordered" id="all_ipr" border="1" style="display:none;">
  <?php echo fresh_pr('IPR'); ?>
</table>

<!-- LPR -->
<table class="table table-bordered" id="all_lpr" border="1" style="display:none;">
  <?php echo fresh_pr('LPR'); ?>
</table>