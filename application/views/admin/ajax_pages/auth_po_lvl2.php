<label>Sort:</label>
<select id="sort_fresh_po" name="sort_fresh_po" onchange="show_content_auth_po_lvl2(this.value);">
	<option value="ALL">ALL</option>
    <option value="FPO">FPO</option>
    <option value="IPO">IPO</option>
    <option value="LPO">LPO</option>
</select>

<!--ALL PO-->

<table class="table table-bordered" id="all_fresh_po_lvl2" border="1">
  <tr style="background-color:#CCC">
    <td>PO NO.</td>
    <td>SUPPLIER NAME</td>
    <td>ORDER VALUE</td>
    <td>PO AGE</td>
    <td>PR AGE</td>
    <td>PENDING ON</td>
  </tr>
  <?php 
    $counter = 0; 
    foreach ($m_new->result() as $row) 
    {  
    $pono = $row->po_num;
    $supp_name = $row->po_supp_name;
    $po_age  = $row->diff;
	$pending_on = $row->level2_mail_to;
     
             
    $sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
    $query2 = $this->db->query($sql2);
    foreach ($query2->result() as $row) {
        $amend_no = $row->amend_no;
    }
    
    $sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
    scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
             
    $sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
             where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
             
    $query3 = $this->db->query($sql3);
    foreach ($query3->result() as $row) {
        $po_basic_value = $row->pomas_pobasicvalue;
        $po_cst_value   = $row->pomas_tcdtotalrate; 
        $po_tax_value   = $row->pomas_tcal_total_amount;
        $po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
    }
    
    $query4 = $this->db->query($sql4);
    foreach ($query4->result() as $row) {
        $pr_age = $row->diff1;
    }
  ?>
  <tr>
    <td>
        <a href="<?php echo base_url(); ?>index.php/createpodc/create_po_lvl2/<?php echo $pono; ?>">
            <?php echo $pono; ?>
        </a>
    </td><!--PO Number-->
    <td><?php echo mb_convert_encoding($supp_name, "ISO-8859-1", "UTF-8"); ?></td><!--SUPP. NAME-->
    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
    <td><?php echo $po_age; ?></td><!--PO AGE-->
    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
    <td><?php echo $pending_on; ?></td><!--Pending On-->
  </tr>
  <?php 
   }
  ?>
</table>

<!--ALL FPO-->

<table class="table table-bordered" id="all_fresh_fpo_lvl2" border="1" style="display:none">
  <tr style="background-color:#CCC">
    <td>PO NO.</td>
    <td>SUPPLIER NAME</td>
    <td>ORDER VALUE</td>
    <td>PO AGE</td>
    <td>PR AGE</td>
    <td>PENDING ON</td>
  </tr>
  <?php 
    $counter = 0; 
    foreach ($m_new_fpo->result() as $row) 
    {  
    $pono = $row->po_num;
    $supp_name = $row->po_supp_name;
    $po_age  = $row->diff;
    $pending_on = $row->level2_mail_to;
             
    $sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
    $query2 = $this->db->query($sql2);
    foreach ($query2->result() as $row) {
        $amend_no = $row->amend_no;
    }
    
    $sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
    scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
             
    $sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
             where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
             
    $query3 = $this->db->query($sql3);
    foreach ($query3->result() as $row) {
        $po_basic_value = $row->pomas_pobasicvalue;
        $po_cst_value   = $row->pomas_tcdtotalrate; 
        $po_tax_value   = $row->pomas_tcal_total_amount;
        $po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
    }
    
    $query4 = $this->db->query($sql4);
    foreach ($query4->result() as $row) {
        $pr_age = $row->diff1;
    }
  ?>
  <tr>
    <td>
        <a href="<?php echo base_url(); ?>index.php/createpodc/create_po_lvl2/<?php echo $pono; ?>">
            <?php echo $pono; ?>
        </a>
    </td><!--PO Number-->
    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
    <td><?php echo $po_age; ?></td><!--PO AGE-->
    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
    <td><?php echo $pending_on; ?></td><!--Pending On-->
  </tr>
  <?php 
   }
  ?>
</table>

<!--ALL IPO-->

<table class="table table-bordered" id="all_fresh_ipo_lvl2" border="1" style="display:none">
  <tr style="background-color:#CCC">
    <td>PO NO.</td>
    <td>SUPPLIER NAME</td>
    <td>ORDER VALUE</td>
    <td>PO AGE</td>
    <td>PR AGE</td>
    <td>PENDING ON</td>
  </tr>
  <?php 
    $counter = 0; 
    foreach ($m_new_ipo->result() as $row) 
    {  
    $pono = $row->po_num;
    $supp_name = $row->po_supp_name;
    $po_age  = $row->diff;
	$pending_on = $row->level2_mail_to;
              
    $sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
    $query2 = $this->db->query($sql2);
    foreach ($query2->result() as $row) {
        $amend_no = $row->amend_no;
    }
    
    $sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
    scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
             
    $sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
             where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
             
    $query3 = $this->db->query($sql3);
    foreach ($query3->result() as $row) {
        $po_basic_value = $row->pomas_pobasicvalue;
        $po_cst_value   = $row->pomas_tcdtotalrate; 
        $po_tax_value   = $row->pomas_tcal_total_amount;
        $po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
    }
    
    $query4 = $this->db->query($sql4);
    foreach ($query4->result() as $row) {
        $pr_age = $row->diff1;
    }
  ?>
  <tr>
    <td>
        <a href="<?php echo base_url(); ?>index.php/createpodc/create_po_lvl2/<?php echo $pono; ?>">
            <?php echo $pono; ?>
        </a>
    </td><!--PO Number-->
    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
    <td><?php echo $po_age; ?></td><!--PO AGE-->
    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
    <td><?php echo $pending_on; ?></td><!--Pending On-->
  </tr>
  <?php 
   }
  ?>
</table>

<!--ALL LPO-->

<table class="table table-bordered" id="all_fresh_lpo_lvl2" border="1" style="display:none">
  <tr style="background-color:#CCC">
    <td>PO NO.</td>
    <td>SUPPLIER NAME</td>
    <td>ORDER VALUE</td>
    <td>PO AGE</td>
    <td>PR AGE</td>
    <td>PENDING ON</td>
  </tr>
  <?php 
    $counter = 0; 
    foreach ($m_new_lpo->result() as $row) 
    {  
    $pono = $row->po_num;
    $supp_name = $row->po_supp_name;
    $po_age  = $row->diff;
	$pending_on = $row->level2_mail_to;
             
    $sql2 = "select MAX(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono'";
    $query2 = $this->db->query($sql2);
    foreach ($query2->result() as $row) {
        $amend_no = $row->amend_no;
    }
    
    $sql3 = "select pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount from 
    scmdb..po_pomas_pur_order_hdr where pomas_pono = '$pono' and pomas_poamendmentno = '$amend_no'";
             
    $sql4 = "select *,datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
             where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
             
    $query3 = $this->db->query($sql3);
    foreach ($query3->result() as $row) {
        $po_basic_value = $row->pomas_pobasicvalue;
        $po_cst_value   = $row->pomas_tcdtotalrate; 
        $po_tax_value   = $row->pomas_tcal_total_amount;
        $po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
    }
    
    $query4 = $this->db->query($sql4);
    foreach ($query4->result() as $row) {
        $pr_age = $row->diff1;
    }
  ?>
  <tr>
    <td>
        <a href="<?php echo base_url(); ?>index.php/createpodc/create_po_lvl2/<?php echo $pono; ?>">
            <?php echo $pono; ?>
        </a>
    </td><!--PO Number-->
    <td><?php echo $supp_name; ?></td><!--SUPP. NAME-->
    <td><?php echo number_format($po_total_value,2); ?></td><!--PO TOTAL VALUE-->
    <td><?php echo $po_age; ?></td><!--PO AGE-->
    <td><?php echo $pr_age; ?></td><!--PR Age (In Days)-->
    <td><?php echo $pending_on; ?></td><!--Pending On-->
  </tr>
  <?php 
   }
  ?>
</table>