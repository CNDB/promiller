<?php //Authorize Purchase Request Purchase ?>
<table class="table table-bordered" id="dataTable" border="1">
  <tr style="background-color:#CCC">
    <td>CATEGORY</td>
    <td>PROJECT NAME</td>
    <td>PR NO.</td>
    <td>ITEM CODE</td>
    <!--<td>ITEM DESC.</td>-->
    <td>PR AGE</td>
  </tr>
  <?php 
    foreach($auth_pr_pur_new->result() as $row)
        {  
          $category = $row->category;
          $project_name = $row->project_name;
          $prnum = $row->pr_num;
          $item_code = $row->item_code;
          $item_desc = $row->itm_desc;
          $age = $row->diff;
  ?>
  <tr>
    <td><?php echo $category; ?></td><!--Category-->
    <td><?php echo $project_name; ?></td><!--Project Name-->
    <td><a href="<?php echo base_url(); ?>index.php/createpoc/view_pr_auth_pur/<?php echo $prnum; ?>">
    <?php echo $prnum; ?></a></td><!--PR Number-->
    <td><?php echo $item_code; ?></td><!--Item Code-->
    <?php /*?><td><?php echo $item_desc; ?></td><?php */?>
    <td><?php echo $age;  ?></td><!--Age (In Days)-->
  </tr>
  <?php } ?>
</table>