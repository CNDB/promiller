<table class="table table-bordered" id="dataTable" border="1">
  <tr style="background-color:#CCC">
    <td>PO NO.</td>
    <td>SUPPLIER NAME</td>
    <td>ORDER VALUE</td>
    <td>PO AGE</td>
    <td>PR AGE</td>
    <td>DISAPPROVED BY</td>
    <td>PENDING ON</td>
  </tr>
  <?php foreach ($disaprve_lvl1_new->result() as $row) { 
    $pono           = $row->po_num;
	$supp_name      = $row->po_supp_name;
    $po_total_value = $row->po_total_value;
   	$po_age         = $row->diff;
	$created_by = $row->created_by;
	
	$sql="select distinct po_approvedby_lvl1 from tipldb..insert_po where po_num = '$pono'";
	$qry = $this->db->query($sql);
	
	foreach($qry->result() as $row){
		$disapproved_by = $row->po_approvedby_lvl1;
	}
	
    $sql1 = "select datediff(DAY,b.preqm_prdate,getdate()) as diff1 from TIPLDB..insert_po a, scmdb..prq_preqm_pur_reqst_hdr b 
where a.po_ipr_no = b.preqm_prno and a.po_num = '$pono'";
    $query1 = $this->db->query($sql1);
    foreach ($query1->result() as $row) {
        $pr_age = $row->diff1;
    }
  ?>
  <tr>
    <td><a href="<?php echo base_url(); ?>index.php/createpodc/view_po_new/<?php echo $pono; ?>/disapprove_L1"><?php echo $pono; ?></a></td>
    <td><?php echo $supp_name; ?></td><!--Purchase Order Date-->
    <td><?php echo number_format($po_total_value,2); ?></td><!--ORDER VALUE-->
    <td><?php echo $po_age; ?></td><!--PO AGE-->
    <td><?php echo $pr_age;  ?></td><!--PR Age (In Months)-->
    <td><?php echo $disapproved_by;  ?></td><!--Disapproved By-->
    <td><?php echo $created_by; ?></td><!--Pending On-->
  </tr>
  <?php } ?>
</table>