<?php include'header.php'; echo "Hello ! ".$username = $_SESSION['username']; ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i> Inventory Management</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Inventory Management</li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-12">
        	<h2>Inventory Management</h2>
        </div>
    </div><br /><br />
    
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/inventory/categories.png" width="50%"/><br /><br />
            Categories
        </div>
        
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/inventory/products.png" width="50%"/><br /><br />
            Products
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/inventory/stock.png" width="50%"/><br /><br />
            Stock
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/inventory/godown.png" width="50%"/><br /><br />
            Godowns
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/inventory/warehouse.png" width="50%"/><br /><br />
            Warehouse & Stores
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/inventory/fg_warehouse.png" width="50%"/><br /><br />
            FG Warehouse
        </div>
    </div><br /><br />

  </section>
</section>
<?php include('footer.php'); ?>