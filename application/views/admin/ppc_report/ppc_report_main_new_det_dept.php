<?php 
//include'header.php'; 
$username = $_SESSION['username'];

//Fetching User Other Details
$user_email = $username."@tipl.com";

$sql_user_det = "select id,user_type,name,* from tipldb..login where email like '$user_email%' and  emp_active = 'Yes'";
$qry_user_det = $this->db->query($sql_user_det);

foreach($qry_user_det->result() as $row){
	$user_id =  $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
} 
?>

<?php $this->load->helper('ppc_report_helper'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC DETAILS</h4>
        </div>
    </div><br />
    
<?php
$category = $_REQUEST['category'];
$department = $_REQUEST['department'];
?>
    
    <div class="row">
        <div class="col-lg-12">
              <table align="center" class="table table-striped table-bordered" style="font-size:10px" id="example">
              <thead style="background-color:#0CF;">
              		<tr>
                        <th>SNO</th>
                        <th>ATAC NO.</th>
                        <th>SO NO. & STATUS</th>
                        <th>CUSTOMER NAME</th>
                        <th>PROJECT NAME</th>
                        <th>CUSTOMER NEED DATE</th>
                        <th>LD DATE</th>
                        <th>PR NEED DATE</th>
                        <th>PO ERP & LIVE STATUS</th>
                        <th>PR NO. & PR CD & PO RD</th>
                        <th>PO NUMBER</th>
                        <th>ITEM CODE</th>
                        <th>ITEM DESC</th>
                        <th>PR TYPE</th>
                        <th>AVERAGE LEAD TIME</th>
                        <th>HISTORY PREDICTED DATE</th>
                        <th>PURCHASE PREDICTED FM DATE</th>
                        <th>PREDICTION NO</th>
                        <th>LAST REMARKS</th>
                        <th>CATEGORY</th>
                        <th>SUPPLIER DETAILS</th>
                    </tr>
              </thead>
              <tbody>
                    <?php
					
					if($category == 'All'){
						$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing','IT";
					}
						
					$sql = "select * from tipldb..ppc_stage_master a, tipldb..ppc_report_details_test b 
					where a.stage = b.flag and b.pr_category in('$category') and a.department in('$department')";	 
					
					$query = $this->db->query($sql);
		
					$sno=0;
					foreach ($query->result() as $row) {
						  $sno++;
						  $pr_num        = $row->pr_num;
						  $item_code     = $row->item_code;
						  $itm_desc1     = $row->item_desc;
						  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
						  $itm_desc      = strip_tags($itm_desc);
						  $category      = $row->pr_category;
						  $created_by    = $row->erp_created_by;
						  $create_date   = substr($row->erp_create_date,0,11);
						  $need_date     = substr($row->pr_need_date,0,11);
						  $ld_date       = substr($row->ld_date,0,11);
						  $atac_no       = $row->atac_no;
						  $project_name  = $row->project_name;
						  $po_num        = $row->po_number;
						  $po_erp_stat   = $row->po_erp_status;
						  $last_fm_date  = substr($row->last_fm_date,0,11);
						  $last_remarks  = $row->last_remarks;
						  $item_code1    = urlencode($item_code);
						  $reorder_level = $row->reorder_level;
						  $current_stock = $row->current_stock;
						  $pending_allocation = $row->pending_allocation;
						  $customer_name = $row->customer_name;
						  //New Columns Added
						  $so_number = $row->so_number;
						  $so_need_date = substr($row->so_need_date,0,11);
						  $po_auth_date = substr($row->po_auth_date,0,11);
						  $pr_type = $row->pr_type;
						  $hist_lead_time = $row->hist_lead_time;
						  $exp_prob_date = substr($row->exp_prob_date,0,11);
						  $revisions = $row->revisions;
						  $so_status = $row->so_status;
						  //Supplier Details
						  $po_supp_name = $row->po_supp_name;
						  $po_supp_email = $row->po_supp_email;
						  $po_supp_phone = $row->po_supp_phone;
						  $po_contact_person = $row->po_contact_person;
						  $po_live_status = $row->po_live_status;
							
						  if(strpos($item_code1, '%2F') !== false){ 
							$item_code2 = str_replace("%2F","chandra",$item_code1); 
						  }else{ 
							$item_code2 = $item_code1; 
						  }
						  
						  if($reorder_level > 0){
							  $reorder_level = 'Yes';
						  } else {
							  $reorder_level = 'No';
						  }
					?>
                    <tr>
                        <td><?php echo $sno; ?></td>
                        <td><a href="http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=<?=$atac_no; ?>&user_id=<?=$user_id; ?>&user_type=<?= $user_type; ?>&name=<?=$name; ?>" target="_blank"><?=$atac_no; ?></a></td>
                        <td><?=$so_number; ?><br /><?=$so_status; ?></td>
                        <td><?=$customer_name; ?></td>
                        <td><?=$project_name; ?></td>
                        <td><?=$so_need_date; ?></td>
                        <td><?=$ld_date; ?></td>
                        <td><?=$need_date; ?></td>
                        <td><?=$po_erp_stat; ?><br><?=$po_live_status; ?></td>
                        <td>
                            <a href="<?=base_url(); ?>index.php/ppcc/ppc_pr_entry/<?=$pr_num; ?>" target="_blank"><?=$pr_num; ?></a>
                            <br><?=$create_date; ?>
                            <br><?=$po_auth_date; ?>
                        </td>
                        <td><a href="<?=base_url(); ?>index.php/ppcc/ppc_po_entry/<?=$po_num; ?>"  target="_blank"><?=$po_num; ?></a></td>
                        <td><a href="<?=base_url(); ?>index.php/createpoc/pendal_view/<?=$item_code2; ?>" target="_blank"><?=$item_code; ; ?></a></td>
                        <td><?=$itm_desc; ?></td>
                        <td><?=$pr_type; ?></td>
                        <td><?=$hist_lead_time; ?></td>
                        <td><?=$exp_prob_date; ?></td>
                        <td style='background-color:#FF0'><?=$last_fm_date; ?></td>
                        <td><?=$revisions; ?></td>
                        <td><?=$last_remarks; ?></td>
                        <td><?=$category; ?></td>
                        <td>
                            <? if($po_supp_name != ''){ ?><b>NAME:</b> <?=$po_supp_name; ?><br><? } ?>
                            <? if($po_supp_name != ''){ ?><b>EMAIL:</b> <?=$po_supp_email; ?><br><? } ?>
                            <? if($po_supp_name != ''){ ?><b>PHONE:</b> <?=$po_supp_phone; ?><br><? } ?>
                            <? if($po_supp_name != ''){ ?><b>CONTACT PERSON:</b> <?=$po_contact_person; ?><br><? } ?>
                        </td>
                  	</tr>
                  <?php } ?>
              </tbody>
              </table>
        </div>
    </div><br />
  </section>
</section>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>