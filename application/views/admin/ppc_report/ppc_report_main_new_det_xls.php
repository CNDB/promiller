<?php
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=export.xls");
header("Pragma: no-cache");
header("Expires: 0");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Excel</title>
</head>

<body>
<?php 
//include'header.php'; 
$username = $_SESSION['username'];

//Fetching User Other Details
$user_email = $username."@tipl.com";

$sql_user_det = "select id,user_type,name,* from tipldb..login where email like '$user_email%' and  emp_active = 'Yes'";
$qry_user_det = $this->db->query($sql_user_det);

foreach($qry_user_det->result() as $row){
	$user_id =  $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
} 
?>

<?php $this->load->helper('ppc_report_helper'); ?>
    
<?php

$pr_type = $_REQUEST['pr_type'];
$category = $_REQUEST['category']; 
$department = $_REQUEST['department'];
$usage_type = $_REQUEST['usage_type'];
$stage = $_REQUEST['stage'];
$pending_on = $_REQUEST['pending_on'];

$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT') and user_id = '$user_id'";

//PR Type
if($pr_type == ''){
	$where_str .= "";
} else if($pr_type == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pr_series in(".$pr_type.")";	
}

//PR Category
if($category == ''){
	$where_str .= "";
} else if($category == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pr_category in('".$category."')";	
}

//Department
if($department == ''){
	$where_str .= "";
} else if($department == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and department in(".$department.")";	
}

//Usage Type
if($usage_type == ''){
	$where_str .= "";
} else if($usage_type == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pr_type in(".$usage_type.")";	
}

//Usage Type
if($stage == ''){
	$where_str .= "";
} else if($stage == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and flag in('".$stage."')";	
}

//Pending ON
if($pending_on == ''){
	$where_str .= "";
} else if($pending_on == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pending_on in('".$pending_on."')";	
}

$where_str .= " order by case when so_need_date is not null then so_need_date else pr_need_date end ";	

?>
	
<table>
    <thead style="background-color:#0CF;">
        <tr>
            <th>SNO</th>
            <th>ATAC NO.</th>
            <th>SO NO.</th>
            <th>SO STATUS</th>
            <th>CUSTOMER NAME</th>
            <th>PROJECT NAME</th>
            <th>CUSTOMER NEED DATE</th>
            <th>LD DATE</th>
            <th>PR NEED DATE</th>
            <th>PO ERP</th>
            <th>PO LIVE STATUS</th>
            <th>PO GE STATUS</th>
            <th>PR NO.</th>
            <th>PR CD</th>
            <th>PO RD</th>
            <th>PO NUMBER</th>
            <th>ITEM CODE</th>
            <th>ITEM DESC</th>
            <th>PR TYPE</th>
            <th>AVERAGE LEAD TIME</th>
            <th>HISTORY PREDICTED DATE</th>
            <th>PURCHASE PREDICTED FM DATE</th>
            <th>PREDICTION NO</th>
            <th>LAST PREDICTION REMARKS</th>
            <th>CATEGORY</th>
            <th>SUPPLIER NAME</th>
            <th>SUPPLIER EMAIL</th>
            <th>SUPPLIER PHONE</th>
            <th>CONTACT PERSON</th>
            <th>PR AGE</th>
            <th>PO AGE</th>
            <th>PENDING ON</th>
            <th>PENDING ON AGE</th>
        </tr>
    </thead>
    <tbody>
        <?php
            
        $sql = "select * from tipldb..ppc_report_details_test_new where $where_str";
        
        //echo $sql; die;
        
        $query = $this->db->query($sql);
    
        $sno=0;
        foreach ($query->result() as $row) {
              $sno++;
              $pr_num        = $row->pr_num;
              $item_code     = $row->item_code;
              $itm_desc1     = $row->item_desc;
              $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
              $itm_desc      = strip_tags($itm_desc);
              $category      = $row->pr_category;
              $created_by    = $row->erp_created_by;
              $create_date   = substr($row->erp_create_date,0,11);
              $need_date     = substr($row->pr_need_date,0,11);
              $ld_date       = substr($row->ld_date,0,11);
              $atac_no       = $row->atac_no;
              $project_name  = $row->project_name;
              $po_num        = $row->po_number;
              $po_erp_stat   = $row->po_erp_status;
              $last_fm_date  = substr($row->last_fm_date,0,11);
              $last_remarks  = $row->last_remarks;
              $item_code1    = urlencode($item_code);
              $reorder_level = $row->reorder_level;
              $current_stock = $row->current_stock;
              $pending_allocation = $row->pending_allocation;
              $customer_name = $row->customer_name;
              //New Columns Added
              $so_number = $row->so_number;
              $so_need_date = substr($row->so_need_date,0,11);
              $po_auth_date = substr($row->po_auth_date,0,11);
              $pr_type = $row->pr_type;
              $hist_lead_time = $row->hist_lead_time;
              $exp_prob_date = substr($row->exp_prob_date,0,11);
              $revisions = $row->revisions;
              $so_status = $row->so_status;
              //Supplier Details
              $po_supp_name = $row->po_supp_name;
              $po_supp_email = $row->po_supp_email;
              $po_supp_phone = $row->po_supp_phone;
              $po_contact_person = $row->po_contact_person;
              $po_live_status = $row->po_live_status;
              $pr_age = $row->pr_age;
              $po_age = $row->po_age;
              $pending_on = $row->pending_on;
              $pending_on_age = $row->pending_on_age;
              $flag = $row->flag;
              $entry_no = $row->entry_no;
              $ge_status = $row->ge_status;
              
              //Manupulating PO Status
              $sql_po_stat_nw = "select * from tipldb..ppc_stage_master where live_status is not null and live_status = '$po_live_status'";
                
              $query_po_stat_nw = $this->db->query($sql_po_stat_nw);
                
              foreach($query_po_stat_nw->result() as $row){
                $po_stat_nw = $row->stage;
              }
                
              if($po_stat_nw != ''){
                $po_live_status1 = $po_stat_nw;
              }
                
                
              if(strpos($item_code1, '%2F') !== false){ 
                $item_code2 = str_replace("%2F","chandra",$item_code1); 
              }else{ 
                $item_code2 = $item_code1; 
              }
              
              if($reorder_level > 0){
                  $reorder_level = 'Yes';
              } else {
                  $reorder_level = 'No';
              }
        ?>
        <tr>
            <td><?= $sno; ?></td>
            <td><?=$atac_no; ?></td>
            <td><?=$so_number; ?></td>
            <td><?=$so_status; ?></td>
            <td><?=$customer_name; ?></td>
            <td><?=$project_name; ?></td>
            <td><?=$so_need_date; ?></td>
            <td><?=$ld_date; ?></td>
            <td><?=$need_date; ?></td>
            <td><?=$po_erp_stat; ?></td>
            <td><?=$po_live_status1; ?></td>
            <td><?=$ge_status; ?></td>
            <td><?=$pr_num; ?></td>
            <td><?=$create_date; ?></td>
            <td><?=$po_auth_date; ?></td>
            <td><?=$po_num; ?></td>
            <td><?=$item_code; ?></td>
            <td><?=$itm_desc; ?></td>
            <td><?=$pr_type; ?></td>
            <td><?=$hist_lead_time; ?></td>
            <td><?=$exp_prob_date; ?></td>
            <td><?=$last_fm_date; ?></td>
            <td><?=$revisions; ?></td>
            <td><?=$last_remarks; ?></td>
            <td><?=$category; ?></td>
            <td><?=$po_supp_name; ?></td>
            <td><?=$po_supp_email; ?></td>
            <td><?=$po_supp_phone; ?></td>
            <td><?=$po_contact_person; ?></td>
            <td><?=$pr_age; ?></td>
            <td><?=$po_age; ?></td>
            <td><?=$pending_on; ?></td>
            <td><?=$pending_on_age; ?></td>
        </tr>
      <?php } ?>
      </tbody>
</table>
</body>
</html>
        