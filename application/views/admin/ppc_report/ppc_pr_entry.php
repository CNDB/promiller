<?php //include'header.php'; ?>

<?php $pr_num = $this->uri->segment(3); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC UPDATE LAST FREEZE MOVEMENT DATE</h4>
        </div>
    </div><br />
    
     <div class="row">
        <div class="col-lg-12">
              <h4>ENTER DETAILS</h4>
        </div>
    </div><br />
    
    <form action="<?php echo base_url(); ?>index.php/ppcc/ppc_pr_entry_submit" method="post" onSubmit="return validate()">
    
    <div class="row">
        <div class="col-lg-2">
        	<b>PR Number</b><br>
            <a href="<?php echo base_url(); ?>index.php/iprc/view_ipr/<?php echo urlencode($pr_num); ?>" target="_blank">
            	<?php echo $pr_num; ?>
            </a>
            <input type="hidden" id="pr_num" name="pr_num" value="<?php echo $pr_num; ?>">
        </div>
        <?php 
			$sql = "select * from tipldb..pr_submit_table where pr_num = '$pr_num'"; 
			$qry = $this->db->query($sql);
			foreach($qry->result() as $row){
				$item_code = $row->item_code;
				$itm_desc = $row->itm_desc;
				$category = $row->category;
				$created_by = $row->created_by;
				$create_date = $row->create_date;
				$need_date = $row->need_date;
				$atac_no = $row->atac_no;
				$project_name = $row->project_name;
				
				$item_code1 = urlencode($item_code);
				
				if(strpos($item_code1, '%2F') !== false)
				{
					$item_code2 = str_replace("%2F","chandra",$item_code1);
				}
				else 
				{
					$item_code2 = $item_code1;
				}
				
				$sql1 = "select acc_name from tipldb..master_atac where atac_no = '$atac_no'";
				$qry1 = $this->db->query($sql1);
				
				foreach($qry1->result() as $row){
					$customer_name = $row->acc_name;
				}
			}
		?>
        <div class="col-lg-2">
        	<b>Item Code</b><br>
            <a href="<?php echo base_url(); ?>index.php/createpoc/pendal_view/<?php echo $item_code2; ?>" target="_blank">
            	<?php echo $item_code; ?>
            </a>
            <input type="hidden" id="item_code" name="item_code" value="<?php echo $item_code; ?>">
        </div>
        <div class="col-lg-2">
        	<b>Item Desc.</b><br>
            <?php echo $itm_desc; ?>
            <input type="hidden" id="item_desc" name="item_desc" value="<?php echo $itm_desc; ?>">
        </div>
        <div class="col-lg-2">
        	<b>Category</b><br>
            <?php echo $category; ?>
            <input type="hidden" id="category" name="category" value="<?php echo $category; ?>">
        </div>
        <div class="col-lg-2">
        	<b>ERP Created By</b><br>
            <?php echo $created_by; ?>
            <input type="hidden" id="pr_created_by" name="pr_created_by" value="<?php echo $created_by; ?>">
        </div>
        <div class="col-lg-2">
        	<b>ERP Created Date</b><br>
            <?php echo substr($create_date,0,11); ?>
            <input type="hidden" id="pr_create_date" name="pr_create_date" value="<?php echo $create_date; ?>">
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-2">
        	<b>ERP Need Date</b><br>
            <?php echo $need_date; ?>
            <input type="hidden" id="need_date" name="need_date" value="<?php echo $need_date; ?>">
        </div>
        <div class="col-lg-2">
        	<b>ATAC No.</b><br>
            <?php echo $atac_no; ?>
            <input type="hidden" id="atac_no" name="atac_no" value="<?php echo $atac_no; ?>">
        </div>
        <div class="col-lg-2">
        	<b>Project Name</b><br>
            <?php echo $project_name; ?>
            <input type="hidden" id="project_name" name="project_name" value="<?php echo $project_name; ?>">
        </div>
        <div class="col-lg-2">
        	<b>Customer Name</b><br>
            <?php echo $customer_name; ?>
            <input type="hidden" id="customer_name" name="customer_name" value="<?php echo $customer_name; ?>">
        </div>
        <div class="col-lg-2"></div>
    </div><br>
    
    <div class="row">
    	<div class="col-lg-2"><b>Select:</b></div>
        <div class="col-lg-2">
            <select id="comm_quest" name="comm_ques" class="form-control" onChange="load_res(this.value);">
            	<option value="">Select</option>
                <option value="ask_question">Ask Question/Answer Question</option>
                <option value="update_date">Update Date</option>
            </select>
        </div>
        <div class="col-lg-8"></div>
    </div><br>
    
    <!--- Ask Question Or Update Freeze Movement Date Starts--->
    
    <div id="detail">
    </div>
    
    <?php
	
	$sql_ques_ans = "select * from tipldb..ppc_entry_table_quesans where pr_num = '$pr_num'";
	$qry_ques_ans = $this->db->query($sql_ques_ans);
	
	$count = 0;
	foreach($qry_ques_ans->result() as $row){
		$count++;
	}
	
	if($count > 0){
	
	?>
    <div class="row">
    	<div class="col-lg-8"><h4>Questionaire</h4></div>
    </div>
    
    <div class="row">
    	<div class="col-lg-8">
        	<table align="center" width="100%" height="auto" cellpadding="0px" cellspacing="0px" class="table table-striped">
            	<thead>
            	<tr style="background-color:#0CF">
                	<th><b>SNO.</b></th>
                    <th><b>PR NUMBER</b></th>
                    <th><b>QUES. OR ANS</b></th>
                    <th><b>CREATED BY</b></th>
                    <th><b>CREATED DATE</b></th>
                </tr>
                </thead>
                <tbody>
                <?php
					$sno1=0;
					foreach($qry_ques_ans->result() as $row){
						$sno1++;
						$pr_num1 = $row->pr_num;
						$ques_ans = $row->remarks;
						$ques_ans_by = $row->created_by;
						$ques_ans_date = $row->created_date;
				?>
                <tr>
                	<td><?php echo $sno1; ?></td>
                    <td><?php echo $pr_num1; ?></td>
                    <td><?php echo $ques_ans; ?></td>
                    <td><?php echo $ques_ans_by; ?></td>
                    <td><?php echo $ques_ans_date; ?></td>
                </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <?php } ?>
    
    <!--- Ask Question Or Update Freeze Movement Date Ends--->
    
    <?php 
		$sql_main = "select * from tipldb..ppc_entry_table where pr_num = '$pr_num'";
		$qry_main = $this->db->query($sql_main);
		$count = 0;
		foreach($qry_main->result() as $row){
			$count++;
		}
		
		if($count > 0){
	?>
    
    <div class="row">
    	<div class="col-lg-4"><h4>Previous Entries</h4></div>
        <div class="col-lg-2"></div>
        <div class="col-lg-4"></div>
    </div>
        
     <div class="row">
    	<div class="col-lg-8">
        	<table align="center" width="100%" height="auto" cellpadding="0px" cellspacing="0px" class="table table-striped">
            	<thead>
                	<tr style="background-color:#0CF">
                    	<th>SNO.</th>
                        <th>UPDATED FM DATE</th>
                        <th>REMARKS</th>
                        <th>ENTERED BY</th>
                        <th>ENTRY DATE TIME</th>
                    </tr>
                </thead>
                <tbody>
                <?php
					$sno=0;
					foreach($qry_main->result() as $row){
						$sno++;
						$pend_alloc_date = $row->pend_alloc_date;
						$remarks = $row->remarks;
						$created_by = $row->created_by;
						$created_date = $row->created_date;
				?>
                	<tr>
                    	<td><?php echo $sno; ?></td>
                        <td><?php echo $pend_alloc_date; ?></td>
                        <td><?php echo $remarks; ?></td>
                        <td><?php echo $created_by; ?></td>
                        <td><?php echo $created_date; ?></td>
                    </tr>
                <?php
					}
				?>
                <?php
					//History Table Records
					$sql_hist = "select * from tipldb..ppc_entry_table_history where pr_num = '$pr_num'";
					
					$qry_hist = $this->db->query($sql_hist);
					
					foreach($qry_hist->result() as $row){
						$sno++;
						$pend_alloc_date = $row->pend_alloc_date;
						$remarks = $row->remarks;
						$created_by = $row->created_by;
						$created_date = $row->created_date;
					
				?>
                	<tr>
                    	<td><?php echo $sno; ?></td>
                        <td><?php echo $pend_alloc_date; ?></td>
                        <td><?php echo $remarks; ?></td>
                        <td><?php echo $created_by; ?></td>
                        <td><?php echo $created_date; ?></td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
        <div class="col-lg-4"></div>
    </div>
    <?php } ?>
    
    <hr/>
    
    <!-------- PR DETAILS START ---------------------->
<div class="row">
	<div class="col-lg-12" align="center"><h3><u>PR DETAILS</u></h3></div>
</div><br>
    
<?php include('pr_item_details.php'); ?>

<? //REASON FOR APPROVAL ?>
<?php
	foreach ($v_auth->result() as $row){
		
		$error = $row->error;
		
		if($error != '' || $error != NULL){
?>

<div class="row">
	<div class="col-lg-12">
    	<h4 style="text-align:center; color:red">Reason For Approval : <?php echo $error; ?></h4>
    </div>
</div>

<?php
		} }
?>

<? //live fetched Details ?>
<table class="table table-bordered">
<?php
	foreach ($v_auth->result() as $row){
		$created_by = $row->preqm_createdby;
		$created_date = $row->preqm_createddate;
		$modified_by = $row->preqm_lastmodifiedby;
		$modified_date = $row->preqm_lastmodifieddate;
		$live_created_by = $row->created_by;
		$live_created_date = $row->create_date;
?>
    <tr>
        <td><b>ERP Created By : <?php echo $created_by; ?><?php echo "<input type='hidden' name ='created_by' value='$created_by' />"; ?></b></td>
        <td><b>ERP Created Date : <?php echo $created_date; ?></b></td>
        <td><b>ERP Modified BY : <?php echo $created_by; ?></b></td>
        <td><b>ERP Modified Date : <?php echo $modified_date; ?></b></td>
        <td><b>LIVE Created BY : <?php echo $live_created_by; ?></b></td>
        <td><b>LIVE Created Date : <?php echo $live_created_date; ?></b></td>
    </tr>
<?php
	break; }
?>

<?php  /*reorder_level*/  ?>
<?php   
$sql10 = "select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$item_code'";
$query10 = $this->db->query($sql10);

if ($query10->num_rows() > 0) {
  foreach ($query10->result() as $row) {
	  $reorder_level = $row->iou_reorderlevel;
	  $reorder_qty =  $row->iou_reorderqty;
	  echo "<input type='hidden' name='reorder_level' value='$reorder_level' >";
	  echo "<input type='hidden' name='reorder_qty' value='$reorder_qty' >";
	}
} else {
	  $reorder_level = 0.00;
	  $reorder_qty =  0.00;
	  echo "<input type='hidden' name='reorder_level' value='$reorder_level' >";
	  echo "<input type='hidden' name='reorder_qty' value='$reorder_qty' >";
}
?>

<?php 
foreach ($supplier1->result() as $row){
	$supplier_name = $row->SuppName;
	$supplier_rate = number_format($row->SuppRate,2); 
	$supplier_lead_time = $row->SuppLeadTime;
	
	echo "<input type='hidden' name='supplier_name' value='$supplier_name' >";
	echo "<input type='hidden' name='supplier_rate' value='$supplier_rate' >";
	echo "<input type='hidden' name='supplier_lead_time' value='$supplier_lead_time' >";
break; } 
?>

<?php 
  	foreach ($v_auth->result() as $row){
		
		$usage = $row->usage;
		$category = $row->category;
		$workorder_no = $row->work_odr_no;
		$so_no = $row->sono;
		$customer_name = $row->customer_name;
		$drawing_no = $row->drawing_no;
		$pm_group = $row->pm_group;
		$project_name = $row->project_name;
		$atac_no = $row->atac_no;
		$atac_ld_date = $row->atac_ld_date;
		$atac_need_date = $row->atac_need_date;
		$atac_payment_terms = $row->atac_payment_terms;
		$pr_remarks = $row->remarks;
		$special_inst_supp = $row->pr_supp_remarks;
		$project_target_date = $row->project_target_date;
		$test_cert_req = $row->test_cert;
		$manufact_clearance = $row->manufact_clearance;
		$dispatch_inst = $row->dispatch_inst;
		$attached_cost_sheet = $row->attch_cost_sheet;
		$attached_drawing = $row->attach_drawing;
		$other_attachment = $row->other_attachment;
		$why_spcl_rmks = $row->why_spcl_rmks;
		
	}
?>

    <tr>
        <td><b>Usage Type:</b><?php echo $usage; ?><?php echo "<input type='hidden' name='usage' value='$usage'>"; ?></td>
        <td><b>Category:</b><?php echo $category; ?><?php echo "<input type='hidden' name='category' value='$category'>"; ?></td>
        <td><b>WO Number:</b><?php echo $workorder_no; ?></td>
        <td><b>SO Number:</b><?php echo $so_no; ?></td>
        <td><b>Customer Name:</b><?php echo $customer_name; ?></td>
        <td><b>Drawing Number:</b><?php echo $drawing_no; ?></td>
    </tr>
    <tr>
        <td><b>PM Group:</b><?php echo $pm_group; ?><?php echo "<input type='hidden' name='pm_group' value='$pm_group'>"; ?></td>
        <td><b>Project Name:</b><?php echo $project_name; ?><?php echo "<input type='hidden' name='pro_ordr_name' value='$project_name'>"; ?></td>
        <td><b>ATAC Number:</b><?php echo $atac_no; ?></td>
        <td><b>ATAC LD Date:</b><?php echo $atac_ld_date; ?></td>
        <td><b>ATAC Need Date:</b><?php echo $atac_need_date; ?></td>
        <td><b>ATAC Payment Terms:</b><?php echo $atac_payment_terms; ?></td>
    </tr>
    <tr>
        <td><b>PR Remarks:</b><?php echo $pr_remarks; ?></td>
        <td><b>Supplier Spcl Remarks:</b><?php echo $special_inst_supp; ?></td>
        <td><b>Project Target Date:</b>
			<?php
			if($project_target_date != '' && $project_target_date != '1900-01-01') {
				echo $project_target_date; 
			}
			?>
        </td>
        <td><b>Test Cert Req:</b><?php echo $test_cert_req; ?></td>
        <td><b>Manufact Clearance:</b><?php echo $manufact_clearance; ?></td>
        <td><b>Dispatch Inst:</b><?php echo $dispatch_inst; ?> </td>
    </tr>
    <tr>
        <td><b>Attached Cost Sheet:</b><?php //echo $attached_cost_sheet;?> 
		<a href="<?php echo base_url(); ?>uploads/<?php echo $attached_cost_sheet; ?>" target='_blank'><?php echo $attached_cost_sheet; ?></a></td>
        <td><b>Attached Drawing:</b><?php echo $attached_drawing; ?></td>
        <td>
            <b>Other Attachement:</b>
            <a href="<?php echo $other_attachment; ?>uploads/<?php echo $other_attachment; ?>" target='_blank'>
            	<?php echo $other_attachment; ?>
            </a>
        </td>
        <td></td>
        <td colspan="2"></td>
    </tr>
</table>

<?php //include('pr_footer.php'); ?>
    
  </section>
</section>

<?php //include('footer.php'); ?>

<script type="text/javascript">
	$("#pend_alloc_date").datepicker({ 
		dateFormat: 'yy-mm-dd',
		minDate: 0,
	});
</script>
<script type="text/javascript">
function validate(){
	var pend_alloc_date = document.getElementById('pend_alloc_date').value;
	var remarks = document.getElementById('remarks').value;
	
	if(pend_alloc_date == ''){
		alert('Enter Expected Freeze Movement Date...');
		return false;
	}
	
	if(remarks == ''){
		alert('Please Enter Reason...');
		return false;
	}
}
</script>

<script>

function load_res(a){
	//alert(a);
	
	$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/wait.gif" width="200px" height="75px" style="margin-left:150px" />');
	
	//Ajax Function
	
	if (window.XMLHttpRequest)
	{// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}
	else
	{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
			
			xmlhttp.onreadystatechange=function(){
			
				if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
				$("#pend_alloc_date").datepicker({ dateFormat: 'yy-mm-dd',minDate: 0,});
			}
	}
	
	//var queryString="?category="+encodeURIComponent(category);
	
	if(a=="ask_question"){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/ppcc/ppc_ask_question",true);
		xmlhttp.send();
	} else {	
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/ppcc/ppc_update_fm_date",true);
		xmlhttp.send();	
	}
}

function hide(a){
	document.getElementById(a).style.display='none';
}

</script>

