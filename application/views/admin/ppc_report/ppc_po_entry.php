<?php include'header.php'; ?>

<?php $po_num = $this->uri->segment(3); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC UPDATE LAST FREEZE MOVEMENT DATE PO</h4>
        </div>
    </div><br />
    
    <?php include('po_detail_div_ppc.php'); ?>
    
    
    
    </section>
</section>

<?php include('footer.php'); ?>

<script type="text/javascript">
	$("#pend_alloc_date").datepicker({ 
		dateFormat: 'yy-mm-dd',
		minDate: 0,
	});
</script>
<script type="text/javascript">
function validate(){
	var pend_alloc_date = document.getElementById('pend_alloc_date').value;
	var remarks = document.getElementById('remarks').value;
	
	if(pend_alloc_date == ''){
		alert('Enter Expected Freeze Movement Date...');
		return false;
	}
	
	if(remarks == ''){
		alert('Please Enter Reason...');
		return false;
	}
}
</script>