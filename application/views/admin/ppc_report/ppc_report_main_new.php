<?php $this->load->helper('ppc_report_helper'); ?>

<?php
$username = $_SESSION['username'];
//Fetching User Other Details
$user_email = $username."@tipl.com";
$sql_user_det = "select id,user_type,name,* from tipldb..login where email like '$user_email%' and  emp_active = 'Yes'";
$qry_user_det = $this->db->query($sql_user_det);

foreach($qry_user_det->result() as $row){
	$user_id =  $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
}
?>

<?php
$sql_proc = "exec tipldb..ppc_report_proc_test_new '$user_id'";
$qry_proc = $this->db->query($sql_proc);
$this->db->close();
$this->db->initialize();
?>
<section id="main-content">
<section class="wrapper">
    <div class="row" style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC REPORT</h4>
        </div>
    </div><br>
    
    <input type="hidden" name="user_id" id="user_id" value="<?= $user_id; ?>" />
    <div class="row">
    	<!-- PR TYPE -->
        <div class="col-lg-1">
        	  <b style="font-size:14px">PR Type</b><br />
             <select id="pr_type" name="pr_type[]" class="form-control pr_type_ary" multiple="multiple">
                <option value="All">All</option>
                <?php
					$sql1 = "select distinct pr_series from tipldb..ppc_report_details_test_new (NOLOCK)
					where pr_series is not null and user_id = '$user_id' order by pr_series";
					 
					$qry1 = $this->db->query($sql1);
					
					foreach($qry1->result() as $row){
						$pr_series = $row->pr_series;
				?>
                <option value="<?=$pr_series; ?>"><?=$pr_series; ?></option>
                <?php } ?>
             </select>
        </div>
        <!-- CATEGORY -->
        <div class="col-lg-2">
        	 <b style="font-size:14px">Category</b>
             <select id="category" name="category[]" class="form-control category_ary" multiple="multiple">
                <option value="All">All</option>
                <?php
					$sql2 = "select distinct pr_category from tipldb..ppc_report_details_test_new  (NOLOCK)
					where pr_category is not null and  pr_category != 'NOT DECIDED' and user_id = '$user_id'
					order by pr_category";
					 
					$qry2 = $this->db->query($sql2);
					
					foreach($qry2->result() as $row){
						$pr_category = $row->pr_category;
				?>
                <option value="<?=$pr_category; ?>"><?=$pr_category; ?></option>
                <?php } ?>
             </select>
        </div>
        <!-- DEPARTMENT -->
        <div class="col-lg-2">
        	 <b style="font-size:14px">Department</b><br />
             <select id="department" name="department[]" class="form-control department_ary" multiple="multiple">
                <option value="All">All</option>
                <?php
					$sql3 = "select distinct department from tipldb..ppc_report_details_test_new  (NOLOCK) 
					where department is not null and department != 'All' and user_id = '$user_id' order by department"; 
					$qry3 = $this->db->query($sql3);
					
					foreach($qry3->result() as $row){
						$department = $row->department;
				?>
                <option value="<?=$department; ?>"><?=$department; ?></option>
                <?php } ?>
             </select>
        </div>
        <!-- USAGE TYPE -->
        <div class="col-lg-2">
        	 <b style="font-size:14px">Usage Type</b><br />
             <select id="usage_type" name="usage_type[]" class="form-control usage_type_ary" multiple="multiple">
                <option value="All">All</option>
                <?php
					$sql4 = "select distinct pr_type from tipldb..ppc_report_details_test_new (NOLOCK)
					where pr_type is not null and pr_type != 'NOT DECIDED' and user_id = '$user_id' order by pr_type"; 
					
					$qry4 = $this->db->query($sql4);
					
					foreach($qry4->result() as $row){
						$pr_type = $row->pr_type;
				?>
                <option value="<?=$pr_type; ?>"><?=$pr_type; ?></option>
                <?php } ?>
             </select>
        </div>
        <!-- PENDING ON -->
        <div class="col-lg-2">
        	 <b style="font-size:14px">Pending On</b><br />
             <select id="pending_on" name="pending_on" class="form-control" style="text-transform:uppercase">
             	<?php
					if($username == "admin" || $username == "abhinav.toshniwal" || $username == "neeraj.pandey" || $username == "priyanka.vijay" || 
					$username == "manisha.agarwal"){
				?>
                <option value="All">All</option>
                <?php
					$sql5 = "select distinct pending_on from tipldb..ppc_report_details_test_new (NOLOCK) where pending_on is not null 
					and user_id = '$user_id' order by pending_on";
					 
					$qry5 = $this->db->query($sql5);
					
					foreach($qry5->result() as $row){
						$pending_on = $row->pending_on;
				?>
                <option value="<?=$pending_on; ?>"><?=strtoupper($pending_on); ?></option>
                <?php } ?>
                <?php 
				} else {
					$sql5 = "select dep_role,department from tipldb..login where email like '$username%' and emp_active = 'yes'";					 
					$qry5 = $this->db->query($sql5)->row();
					$dep_role = $qry5->dep_role;
					$dep = $qry5->department;
					
					if($dep == 'Accounts and Finance'){
						echo "<option value='ACCOUNTS'>ACCOUNTS</option>";
					} else if($dep == 'Quality Assurance'){
						echo "<option value='INSPECTION'>INSPECTION</option>";
					} else if($dep == 'Stores'){
						echo "<option value='STORE'>STORE</option>";
					} else {
						echo "<option value='".$username."'>".strtoupper($username)."</option>";
					}
				} 
			 	?>
             </select>
        </div>
        <div class="col-lg-2">
        	 <b style="font-size:14px">Display Type</b><br />
             <select id="display_type" name="display_type" class="form-control" style="text-transform:uppercase">
                <option value="PR">PR</option>
                <option value="PO">PO</option>
             </select>
        </div>
        <div class="col-lg-1">
             <input type="button" name="filter" id="filter" value="Filter" class="form-control" onClick="filter()">
        </div>
    </div><br><hr />
    <div class="row">
    	<div class="col-lg-12" id="detail">
        </div>
    </div><br>
</section>
</section>

<script type="text/javascript">
function filter(){
	var pr_type = document.getElementById("pr_type").value;
	var category = document.getElementById("category").value;
	var department = document.getElementById("department").value;
	var usage_type = document.getElementById("usage_type").value;
	var pending_on = document.getElementById("pending_on").value;
	var user_id = document.getElementById("user_id").value;
	var display_type = document.getElementById("display_type").value;
	//Null Checking
	if(pr_type == ""){
		alert("Select All the Colums...");
		return false;
	}
	
	if(category == ""){
		alert("Select All the Colums...");
		return false;
	}
	
	if(department == ""){
		alert("Select All the Colums...");
		return false;
	}
	
	if(usage_type == ""){
		alert("Select All the Colums...");
		return false;
	}
	
	if(pending_on == ""){
		alert("Select All the Colums...");
		return false;
	}
	
	if(display_type == ""){
		alert("Select All the Colums...");
		return false;
	}
	
	//Selecting Multiple Values
	//PR Type
	var type_pr = [];
	$.each($(".pr_type_ary option:selected"), function(){            
		type_pr.push($(this).val());
	});
		
	var pr_type1 = JSON.stringify(type_pr);
	
	//Category
	var type_cat = [];
	$.each($(".category_ary option:selected"), function(){            
		type_cat.push($(this).val());
	});
		
	var category1 = JSON.stringify(type_cat);
	
	//Department
	var type_dept = [];
	$.each($(".department_ary option:selected"), function(){            
		type_dept.push($(this).val());
	});
		
	var department1 = JSON.stringify(type_dept);
	
	
	//usage_type
	var type_usage = [];
	$.each($(".usage_type_ary option:selected"), function(){            
		type_usage.push($(this).val());
	});
		
	var usage_type1 = JSON.stringify(type_usage);
	
	
	
	$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/wait.gif" width="200px" height="75px" style="margin-left:150px" />');
	
	//Ajax Function
	
	if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp=new XMLHttpRequest();
	}else{// code for IE6, IE5
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}
		  
		xmlhttp.onreadystatechange=function()
		{
		
			if(xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById('detail').innerHTML=xmlhttp.responseText;
		}
	}
	
	var queryString="?pr_type="+pr_type1+"&category="+category1+"&department="+department1+"&usage_type="+usage_type1+"&pending_on="+pending_on+"&user_id="+user_id;
	
	if(display_type == 'PR'){
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/ppcc/ppc_report_main_new_ajax_pr" + queryString,true);
	} else {
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/ppcc/ppc_report_main_new_ajax_po" + queryString,true);
	}
	
	xmlhttp.send();
	
	
}
</script>
<script>
$("#pending_on").select2();
</script>
