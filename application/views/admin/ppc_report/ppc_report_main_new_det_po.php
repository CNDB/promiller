<?php 
//include'header.php'; 
$username = $_SESSION['username'];

//Fetching User Other Details
$user_email = $username."@tipl.com";

$sql_user_det = "select id,user_type,name,* from tipldb..login where email like '$user_email%' and  emp_active = 'Yes'";
$qry_user_det = $this->db->query($sql_user_det);

foreach($qry_user_det->result() as $row){
	$user_id =  $row->id;
	$user_type = $row->user_type;
	$name = $row->name;
} 
?>

<?php $this->load->helper('ppc_report_helper'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC DETAILS</h4>
        </div>
    </div><br />
	
	
    
<?php

$pr_type = $_REQUEST['pr_type'];
$category = $_REQUEST['category']; 
$department = $_REQUEST['department'];
$usage_type = $_REQUEST['usage_type'];
$stage = $_REQUEST['stage'];
$pending_on = $_REQUEST['pending_on'];

$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT','ERP AUTHORIZED PR PENDING FOR ERP PO CREATION','DISAPPROVED PURCHASE REQUEST BY PURCHASE DEPARTMENT')  and user_id = '$user_id' and po_number is not null and po_number != ''";

//Purchaser Checking
$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";

$qry_pur=$this->db->query($sql_pur);

foreach($qry_pur->result() as $row){
	$count1 = $row->count1;
}

//Planner Checking
$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";

$qry_plan=$this->db->query($sql_plan);

foreach($qry_plan->result() as $row){
	$count2 = $row->count2;
}

//PR Type
if($pr_type == ''){
	$where_str .= "";
} else if($pr_type == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pr_series in(".$pr_type.")";	
}

//PR Category
if($category == ''){
	$where_str .= "";
} else if($category == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pr_category in('".$category."')";	
}

//Department
if($department == ''){
	$where_str .= "";
} else if($department == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and department in(".$department.")";	
}

//Usage Type
if($usage_type == ''){
	$where_str .= "";
} else if($usage_type == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and pr_type in(".$usage_type.")";	
}

//Usage Type
if($stage == ''){
	$where_str .= "";
} else if($stage == 'All'){	
	$where_str .= "";	
} else {	
	$where_str .= " and flag in('".$stage."')";	
}

//Pending ON
if($pending_on == ''){
	$where_str .= "";
} else if($pending_on == 'All'){	
	$where_str .= "";	
} else {	
	//$where_str .= " and pending_on in('".$pending_on."')";
	
	//New Condition
	if($count2 > 0){
		$where_str .= " and erp_created_by in('".$pending_on."')";
    } else if($count1 > 0){
		$where_str .= " and po_created_by in('".$pending_on."')";
	} else {
		$where_str .= " and pending_on in('".$pending_on."')";
	}	
}

$where_str .= " order by so_need_date desc";	

?>
	<div class="row">
    	<div class="col-lg-2">
        	<a href="<?= base_url(); ?>index.php/ppcc/ppc_report_nw_det_xls?pr_type=<?=$pr_type;?>&category=<?=$category;?>&stage=<?=$stage;?>&usage_type=<?=$usage_type;?>&pending_on=<?=$pending_on;?>&department=<?=$department;?>" target="_blank">
            	<input type="button" name="export_data" value="Export Data" class="form-control">
            </a>
        </div>
    </div><br />
    
    <div class="row">
        <div class="col-lg-12">
              <table align="center" class="table table-striped table-bordered" style="font-size:10px" id="example">
              <thead style="background-color:#0CF;">
              		<tr>
                        <th>SNO</th>
                        <th>SO NO. & STATUS</th>
                        <th>CUSTOMER NAME</th>
                        <th>CUSTOMER NEED DATE</th>
                        <th>LD DATE</th>
                        <th>PO ERP & LIVE STATUS & GE STATUS</th>
                        <th>PO RD</th>
                        <th>PO NUMBER</th>
                        <th>CATEGORY</th>
                        <th>SUPPLIER DETAILS</th>
                        <th>PO AGE</th>
                        <th>PENDING ON</th>
                        <th>PENDING ON AGE</th>
                        <th>PENDING FROM DATE</th>
                        <th>LINK</th>
                    </tr>
              </thead>
              <tbody>
                    <?php						
					$sql = "select distinct po_number,po_erp_status,customer_name,so_number,so_need_date,po_auth_date,so_status,po_supp_name,
					po_supp_email,po_supp_phone,po_contact_person,po_live_status,po_age,pending_on,pending_on_age,flag,entry_no,
					ge_status,pend_from_date from tipldb..ppc_report_details_test_new where $where_str";	
					
					//echo $sql; die;
									
					$query = $this->db->query($sql);
		
					$sno=0;
					foreach ($query->result() as $row) {
						  $sno++;
						  $po_num        = $row->po_number;
						  $po_erp_stat   = $row->po_erp_status;
						  $customer_name = $row->customer_name;
						  //New Columns Added
						  $so_number = $row->so_number;
						  $so_need_date = substr($row->so_need_date,0,11);
						  $po_auth_date = substr($row->po_auth_date,0,11);
						  $so_status = $row->so_status;
						  //Supplier Details
						  $po_supp_name = $row->po_supp_name;
						  $po_supp_email = $row->po_supp_email;
						  $po_supp_phone = $row->po_supp_phone;
						  $po_contact_person = $row->po_contact_person;
						  $po_live_status = $row->po_live_status;
						  $po_age = $row->po_age;
						  $pending_on = $row->pending_on;
						  $pending_on_age = $row->pending_on_age;
						  $flag = $row->flag;
						  $entry_no = $row->entry_no;
						  $ge_status = $row->ge_status;
						  $pend_from_date = $row->pend_from_date;
						  
						  //Manupulating PO Status
						  $sql_po_stat_nw = "select * from tipldb..ppc_stage_master where live_status is not null and live_status = '$po_live_status'";							
						  $query_po_stat_nw = $this->db->query($sql_po_stat_nw);
							
						  foreach($query_po_stat_nw->result() as $row){
						  	$po_stat_nw = $row->stage;
						  }
							
						  if($po_stat_nw != ''){
							$po_live_status1 = $po_stat_nw;
						  }
						  
						  if($flag != 'SERVICE CASES'){
							  $sql_link = "select link,param1,concate,param2,user_det_req from tipldb..ppc_stage_master 
							  where link_req = 'Yes' and stage = '$flag'";
						  } else {
							  if($po_live_status == '' && $po_erp_stat == 'FR'){
								  $sql_link = "select link,param1,concate,param2,user_det_req from tipldb..ppc_stage_master 
								  where link_req = 'Yes' and live_status = 'ERP FRESH PO PENDING FOR LIVE PO CREATION'";
							  } else if($po_erp_stat == 'AM'){
								  $sql_link = "select link,param1,concate,param2,user_det_req from tipldb..ppc_stage_master 
								  where link_req = 'Yes' and stage = 'ERP AMEND PO'";
							  } else {
								  $sql_link = "select link,param1,concate,param2,user_det_req from tipldb..ppc_stage_master 
								  where link_req = 'Yes' and live_status = '$po_live_status'";
							  }
							    
						  }
						  
						  //echo $sql_link; die;
						  
						  $query_link = $this->db->query($sql_link);
						  
						  $url = "";
						  foreach($query_link->result() as $row){
							  $link = $row->link;
							  $param1 = $row->param1;
							  $concate = $row->concate;
							  $param2 = $row->param2;
							  $user_det_req = $row->user_det_req;
							  
							  if($param2 != '' && $user_det_req == 'No'){
								  
								  if($param1 == '$po_number'){
									  $param1_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param1_new = $entry_no;
								  }
								  
								  if($param2 == '$po_number'){
									  $param2_new = $po_num;
								  } else if($param2 == '$entry_no'){
									  $param2_new = $entry_no;
								  }
								  
								  $url = $link.$param1_new.$concate.$param2_new;
								  
							  } else if($param2 == '' && $user_det_req == 'No'){
								  
								  if($param1 == '$po_number'){
									  $param1_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param1_new = $entry_no;
								  }
								  
								  if($param2 == '$po_number'){
									  $param2_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param2_new = $entry_no;
								  }
								  
								  $url = $link.$param1_new;
								  
							  } else if($param2 != '' && $user_det_req == 'Yes'){
								  
								 if($param1 == '$po_number'){
									  $param1_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param1_new = $entry_no;
								  }
								  
								  if($param2 == '$po_number'){
									  $param2_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param2_new = $entry_no;
								  }
								  
								  $url = $link.$param1_new.$concate.$param2_new."&user_id=".$user_id."&user_type=".$user_type."&name=".$name;
								  
							  }else if($param2 == '' && $user_det_req == 'Yes'){
								  
								  if($param1 == '$po_number'){
									  $param1_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param1_new = $entry_no;
								  }
								  
								  if($param2 == '$po_number'){
									  $param2_new = $po_num;
								  } else if($param1 == '$entry_no'){
									  $param2_new = $entry_no;
								  }
								  
								  $url = $link.$param1_new."&user_id=".$user_id."&user_type=".$user_type."&name=".$name;
							  }
						  }
						  
						  //echo $url; die;
						  
					?>
                    <tr>
                        <td><?php echo $sno; ?></td>
                        <td><?=$so_number; ?><br /><?=$so_status; ?></td>
                        <td><?=$customer_name; ?></td>
                        <td><?=$so_need_date; ?></td>
                        <td><?=$ld_date; ?></td>
                        <td>
                            <? if($po_erp_stat != ''){ ?>PO ERP Status: <?=$po_erp_stat; ?> <? } ?><br><br>
							<? if($po_live_status1 != ''){ ?>PO Live Status : <?=$po_live_status1; ?> <? } ?><br><br>
                            <? if($ge_status != ''){ ?>GE Status : <?=$ge_status; ?> <? } ?>
                        </td>
                        <td><?=$po_auth_date; ?></td>
                        <td><a href="<?=base_url(); ?>index.php/ppcc/ppc_po_entry/<?=$po_num; ?>"  target="_blank"><?=$po_num; ?></a></td>
                        <td><?=$category; ?></td>
                        <td>
                            <? if($po_supp_name != ''){ ?><b>NAME:</b> <?=$po_supp_name; ?><br><? } ?>
                            <? if($po_supp_name != ''){ ?><b>EMAIL:</b> <?=$po_supp_email; ?><br><? } ?>
                            <? if($po_supp_name != ''){ ?><b>PHONE:</b> <?=$po_supp_phone; ?><br><? } ?>
                            <? if($po_supp_name != ''){ ?><b>CONTACT PERSON:</b> <?=$po_contact_person; ?><br><? } ?>
                        </td>
                        <td><?=$po_age; ?></td>
                        <td><?=$pending_on; ?></td>
                        <td><?=$pending_on_age; ?></td>
                        <td><?=$pend_from_date; ?></td>
                        <td>
                        <?php if($url != ''){ ?>
                        <a href="<?php echo $url; ?>" target="_blank">
                        	<img src="<?php echo base_url('assets/admin/img/');?>edit_icon.png" width="25px" height="25px"  />
                        </a>
                        <?php } ?>
                        </td>
                  	</tr>
                  <?php } ?>
                  </tbody>
              </table>
        </div>
    </div><br />
  </section>
</section>

<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
		
		"scrollY":        '65vh',
		"scrollX": 		true,
		"scrollCollapse": true,
		"paging":         false,
		
		// "dom": '<flip<t>>',
		// 'dom': 'Rlfrtip',
		'dom': 'iRlfrtp',
		language: {
			searchPlaceholder: "Search anything.."
		},
		stateSave: true
		
	});
} );
</script>