<?php //include'header.php'; ?>

<?php $this->load->helper('ppc_report_helper'); ?>

<section id="main-content">
  <section class="wrapper">
    <div class="row"  style="margin-top:-10px">
        <div class="col-lg-12" style="background-color:#333333; padding:2px">
              <h4  align="center" style="font-weight:bold; color:#FFFFFF; text-transform:uppercase;">PPC REPORT</h4>
        </div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-1"></div>
        <div class="col-lg-10">
             <h4>PR To Freeze Movement Prediction</h4>
        </div>
        <div class="col-lg-1"></div>
    </div><br />
    
    <div class="row">
    	<div class="col-lg-1"></div>
        <div class="col-lg-1">
             <b style="font-size:14px">Category</b>
        </div>
        <div class="col-lg-2">
             <select id="category" name="category" class="form-control">
             	<option value="">--select--</option>
                <option value="All">ALL</option>
                <option value="Avazonic">Avazonic</option>
                <option value="Capital Goods">Capital Goods</option>
                <option value="Instrumentation">Instrumentation</option>
                <option value="Non Production Consumables">Non Production Consumables</option>
                <option value="Production Consumables">Production Consumables</option>
                <option value="Security">Security</option>
                <option value="Service">Service</option>
                <option value="Sensors">Sensors</option>
                <option value="Tools">Tools</option>
                <option value="Packing">Packing</option>
                <!--<option value="All">All</option>-->
             </select>
        </div>
        <div class="col-lg-1">
             <b style="font-size:14px">PR Type</b>
        </div>
        <div class="col-lg-2">
             <select id="pr_type" name="pr_type" class="form-control">
             	<option value="">--select--</option>
                <option value="CGP">CGP</option>
                <option value="FPR">FPR</option>
                <option value="IPR">IPR</option>
             </select>
        </div>
        <div class="col-lg-2">
             <input type="button" name="filter" id="filter" value="Filter" class="form-control" onClick="filter()">
        </div>
        <div class="col-lg-2">
            <input type="button" name="reset" id="reset" value="Reset" class="form-control" onClick="reset_fun()">
        </div>
        <div class="col-lg-1"></div>
    </div><br />
    
<?php include('ppc_report_main_include.php'); ?>

<?php //include('footer.php'); ?>

<script type="text/javascript">

function filter(){
	var category = document.getElementById("category").value;
	var pr_type = document.getElementById("pr_type").value;
	
	if(category == "" && pr_type == ""){
		alert("Please Select Category And PO Type...");
	} else {
		//alert(category);
		$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/wait.gif" width="200px" height="75px" style="margin-left:150px" />');
		
		//Ajax Function
		
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
			  
			xmlhttp.onreadystatechange=function()
			{
			
				if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
			}
		}
		
		var queryString="?category="+encodeURIComponent(category)+"&pr_type="+encodeURIComponent(pr_type);
		
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/ppcc/ppc_report_main_ajax" + queryString,true);
		xmlhttp.send();
	
	}
}

function reset_fun(){
	
	var category = document.getElementById("category").value;
	var pr_type = document.getElementById("pr_type").value;
	
	if(category == "" && pr_type == ""){
		alert("Please Select Category And PO Type...");
	} else {
	
		$("#detail").empty().html('<img src="<?php echo base_url(); ?>assets/images/wait.gif" width="200px" height="75px" style="margin-left:150px" />');
		
		//Ajax Function
		
		if (window.XMLHttpRequest)
		{// code for IE7+, Firefox, Chrome, Opera, Safari
			xmlhttp=new XMLHttpRequest();
		}
		else
		{// code for IE6, IE5
			xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
		}
			  
			xmlhttp.onreadystatechange=function()
			{
			
				if(xmlhttp.readyState==4 && xmlhttp.status==200){
				document.getElementById('detail').innerHTML=xmlhttp.responseText;
			}
		}
		
		xmlhttp.open("GET","<?php echo base_url(); ?>index.php/ppcc/ppc_report_main_ajax_reset",true);
		xmlhttp.send();
	}
}

</script>
