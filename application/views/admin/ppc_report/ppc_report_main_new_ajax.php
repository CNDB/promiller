<?php $this->load->helper('ppc_report_helper'); ?>

<?php 
	$pr_type = $_REQUEST['pr_type'];
	$category = $_REQUEST['category']; 
	$department = $_REQUEST['department'];
	$usage_type = $_REQUEST['usage_type'];
	$pending_on = $_REQUEST['pending_on'];
	$user_id = $_REQUEST['user_id'];
	
	
	$pr_type =json_decode($pr_type, TRUE);
	$category =json_decode($category, TRUE);
	$department =json_decode($department, TRUE);
	$usage_type =json_decode($usage_type, TRUE);
	
	//Pr Type
	if( $pr_type[0] == 'All' ){
		$pr_type1 = "All";
	}else{
		 
		$str = '';
		foreach($pr_type as $var){
			if($str == ''){
				$str = "'".$var."'";
			}else{
				$str = $str.","."'".$var."'";
			}
		}
		
		$pr_type1 = $str;
	}
	
	//echo $pr_type1;
	
	//Category
	if( $category[0] == 'All' ){
		$category1 = "All";
	}else{
		 
		$str = '';
		foreach($category as $var){
		
			if($str == ''){
				$str = "'".$var."'";
			}else{
				$str = $str.","."'".$var."'";
			}
		}
		
		$category1 = $str;
	}
	
	//echo $category1;
	//die;
	
	//Department
	if( $department[0] == 'All' ){
		$department1 = "All";
	}else{
		 
		$str = '';
		foreach($department as $var){
		
			if($str == ''){
				$str = "'".$var."'";
			}else{
				$str = $str.","."'".$var."'";
			}
		}
		
		$department1 = $str;
	}
	
	//echo $department1;
	
	//Usage Type
	if( $usage_type[0] == 'All' ){
		$usage_type1 = "All";
	}else{
		 
		$str = '';
		foreach($usage_type as $var){
		
			if($str == ''){
				$str = "'".$var."'";
			}else{
				$str = $str.","."'".$var."'";
			}
		}
		
		$usage_type1 = $str;
	}
	
	//echo $usage_type1;
	
	//$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT','ERP FRESH PR PENDING FOR LIVE PR CREATION')";

	$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT','SERVICE CASES') and user_id = '$user_id'";
	
	//PR Type
	if($pr_type1 == ''){
		$where_str .= "";
	} else if($pr_type1 == 'All'){	
		$where_str .= "";	
	} else {	
		$where_str .= " and pr_series in(".$pr_type1.")";	
	}
	
	//PR Category
	if($category1 == ''){
		$where_str .= "";
	} else if($category1 == 'All'){	
		$where_str .= "";	
	} else {	
		$where_str .= " and pr_category in(".$category1.")";	
	}
	
	//Department
	if($department1 == ''){
		$where_str .= "";
	} else if($department1 == 'All'){	
		$where_str .= "";	
	} else {	
		$where_str .= " and department in(".$department1.")";	
	}
	
	//Usage Type
	if($usage_type1 == ''){
		$where_str .= "";
	} else if($usage_type1 == 'All'){	
		$where_str .= "";	
	} else {	
		$where_str .= " and pr_type in(".$usage_type1.")";	
	}
	
	//Usage Type
	if($stage == ''){
		$where_str .= "";
	} else if($stage == 'All'){	
		$where_str .= "";	
	} else {	
		$where_str .= " and flag in('".$stage."')";	
	}
	
	//Pending ON
	if($pending_on == ''){
		$where_str .= "";
	} else if($pending_on == 'All'){	
		$where_str .= "";	
	} else {	
		$where_str .= " and pending_on in('".$pending_on."')";	
	}
	
	$where_str .= " order by priority";
?>

<table class="table table-bordered">
    <tr style="background-color:#0CF;">
    	<td><b>Category</b></td>
    	<?php
			if($category1 == 'All'){				
		?>
        <td><b>Avazonic</b></td>
        <td><b>Capital Goods</b></td>
        <td><b>Instrumentation</b></td>
        <td><b>IT</b></td>
        <td><b>Non Production Consumables</b></td>
        <td><b>Production Consumables</b></td>
        <td><b>Security</b></td>
        <td><b>Service</b></td>
        <td><b>Sensors</b></td>
        <td><b>Tools</b></td>
        <td><b>Packing</b></td>
        <td><b>Total</b></td>
        <?php } else { ?>
        	<?php foreach($category as $value){ ?>
        		<td><b><?php echo $value; ?></b></td>
        	<?php } ?>
        <?php } ?>
    </tr>
    <?php
		$sql_stages = "
		select distinct flag, priority, stage_bg_color from tipldb..ppc_report_details_test_new 
		where $where_str";
		
        $qry_stages = $this->db->query($sql_stages);
        
        foreach($qry_stages->result() as $row){
    ?>
    <tr style="background-color:<?php echo $stage_bg_color = $row->stage_bg_color; ?>">
        <td style="background-color:#0CF;"><b><?php echo $stage = $row->flag; ?></b></td>
        <?php if($category1 == 'All'){ ?>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Avazonic"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Avazonic',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Capital Goods"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Capital Goods',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Instrumentation"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Instrumentation',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "IT"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'IT',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Non Production Consumables"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Non Production Consumables',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Production Consumables"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Production Consumables',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Security"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Security',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Service"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Service',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Sensors"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Sensors',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Tools"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Tools',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Packing"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,'Packing',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "All"; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_tot($pr_type1,'All',$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <?php } else { ?>
        	<?php foreach($category as $value){ ?>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo $value; ?>&stage=<?php echo $stage; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt($pr_type1,$value,$stage,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        	<?php } ?>
        <?php } ?>
    </tr>
    <?php } ?>
    <tr>
        <td style="background-color:#0CF;"><b>TOTAL</b></td>
        <?php
			if($category1 == 'All'){
		?>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Avazonic"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Avazonic',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Capital Goods"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Capital Goods',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Instrumentation"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Instrumentation',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "IT"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'IT',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Non Production Consumables"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Non Production Consumables',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Production Consumables"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Production Consumables',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Security"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Security',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Service"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Service',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Sensors"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Sensors',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Tools"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Tools',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "Packing"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'Packing',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo "All"; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_dept($pr_type1,'All',$department1,$usage_type1,$pending_on,$user_id); ?>
            </a>
        </td>
        <?php } else { ?>
        	<?php foreach($category as $value){ ?>
            <td>
                <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?pr_type=<?php echo $pr_type1; ?>&category=<?php echo $value; ?>&department=<?php echo $department1; ?>&usage_type=<?php echo $usage_type1; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                    <?php ppc_new_rpt_cnt_dept($pr_type1,$value,$department1,$usage_type1,$pending_on,$user_id); ?>
                </a>
            </td>
        	<?php } ?>
        <?php } ?>
    </tr>
    <?php if($department1 == 'All'){ ?>
    <!-- Service Cases Display A/C Department -->
    <tr>
        <td style="background-color:#0CF;"><b>SERVICE CASES</b></td>
        <?php if($category1 == 'All'){ ?>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Avazonic"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Avazonic','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Capital Goods"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Capital Goods','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Instrumentation"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Instrumentation','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "IT"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','IT','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Non Production Consumables"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Non Production Consumables','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Production Consumables"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Production Consumables','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Security"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Security','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Service"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Service','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Sensors"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Sensors','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Tools"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Tools','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Packing"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('','Packing','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "All"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt_tot('','All','SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        <?php } else { ?>
        	<?php foreach($category as $value){ ?>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo $value; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                <?php ppc_new_rpt_cnt('',$value,'SERVICE CASES','',$pending_on,$user_id); ?>
            </a>
        </td>
        	<?php } ?>
        <?php } ?>
    </tr>
    <?php } else { ?>
    	<?php foreach($department as $value1){ ?>
        	<?php if($value1 == 'Purchase' || $value1 == 'PO Approval'){ ?>
            
            <tr>
                <td style="background-color:#0CF;"><b>SERVICE CASES</b></td>
                <?php if($category1 == 'All'){ ?>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Avazonic"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Avazonic','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Capital Goods"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Capital Goods','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Instrumentation"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Instrumentation','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "IT"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','IT','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Non Production Consumables"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Non Production Consumables','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Production Consumables"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Production Consumables','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Security"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Security','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Service"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Service','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Sensors"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Sensors','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Tools"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Tools','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Packing"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('','Packing','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "All"; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt_tot('','All','SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                <?php } else { ?>
                    <?php foreach($category as $value){ ?>
                <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo $value; ?>&stage=<?php echo "SERVICE CASES"; ?>&pending_on=<?php echo $pending_on; ?>" target="_blank">
                        <?php ppc_new_rpt_cnt('',$value,'SERVICE CASES','',$pending_on,$user_id); ?>
                    </a>
                </td>
                    <?php } ?>
                <?php } ?>
            </tr>
            <!-- Service Cases Display A/C Department -->
            <?php } ?>
        <?php } ?>
    <?php } ?>
</table>