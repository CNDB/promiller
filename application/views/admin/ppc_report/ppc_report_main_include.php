<?php

if($condition != ''){
	
	$pr_type = $condition;
	
} else {

	$pr_type="CGP','IPR','FPR";

}

//echo $pr_type;
//die;

$current_date = date("Y-m-d");

$sql1="select DATEADD(dd, -(DATEPART(dw, '$current_date')-1), '$current_date') as current_week_start_date";

$qry1 = $this->db->query($sql1);

foreach($qry1->result() as $row){
	$cwsd=substr($row->current_week_start_date,0,11);
}

$sql2="select DATEADD(dd, 7-(DATEPART(dw, '$current_date')), '$current_date') as current_week_end_date";

$qry2 = $this->db->query($sql2);

foreach($qry2->result() as $row){
	$cwed=substr($row->current_week_end_date,0,11);
}

//First Week Start & End Date

$fwd = date('Y-m-d', strtotime("+7 days"));

$sql3="select DATEADD(dd, -(DATEPART(dw, '$current_date')-8), '$current_date') as first_week_start_date";

$qry3 = $this->db->query($sql3);

foreach($qry3->result() as $row){
	$fwsd=substr($row->first_week_start_date,0,11);
}

$sql4="select DATEADD(dd, 14-(DATEPART(dw, '$current_date')), '$current_date') as first_week_end_date"; 

$qry4 = $this->db->query($sql4);

foreach($qry4->result() as $row){
	$fwed=substr($row->first_week_end_date,0,11);
}

//Second Week Start & End Date

$swd=date('Y-m-d', strtotime("+14 days"));

$sql3="select DATEADD(dd, -(DATEPART(dw, '$current_date')-15), '$current_date') as second_week_start_date";

$qry3 = $this->db->query($sql3);

foreach($qry3->result() as $row){
	$swsd=substr($row->second_week_start_date,0,11);
}

$sql4="select DATEADD(dd, 21-(DATEPART(dw, '$current_date')), '$current_date') as second_week_end_date"; 

$qry4 = $this->db->query($sql4);

foreach($qry4->result() as $row){
	$swed=substr($row->second_week_end_date,0,11);
}

//Third Week Start & End Date

$twd=date('Y-m-d', strtotime("+21 days"));

$sql5="select DATEADD(dd, -(DATEPART(dw, '$current_date')-22), '$current_date') as third_week_start_date";

$qry5 = $this->db->query($sql5);

foreach($qry5->result() as $row){
	$twsd=substr($row->third_week_start_date,0,11);
}

$sql6="select DATEADD(dd, 28-(DATEPART(dw, '$current_date')), '$current_date') as third_week_end_date";

$qry6 = $this->db->query($sql6);

foreach($qry6->result() as $row){
	$twed=substr($row->third_week_end_date,0,11);
}

//Fourth Week Start & End Date

$ftwd=date('Y-m-d', strtotime("+28 days"));

$sql7="select DATEADD(dd, -(DATEPART(dw, '$current_date')-29), '$current_date') as fourth_week_start_date";

$qry7 = $this->db->query($sql7);

foreach($qry7->result() as $row){
	$ftwsd=substr($row->fourth_week_start_date,0,11);
}

$sql8="select DATEADD(dd, 35-(DATEPART(dw, '$current_date')), '$current_date') as fourth_week_end_date"; 

$qry8 = $this->db->query($sql8);

foreach($qry8->result() as $row){
	$ftwed=substr($row->fourth_week_end_date,0,11);
}

//die;
?>

<?php 
//All Categories 
$all_category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing"; 
?>
    
    <div class="row">
    	<div class="col-lg-1"></div>
        <div class="col-lg-10" id="detail">
             <table align="center" width="100%" height="auto" cellpadding="0" cellspacing="0" class="table table-bordered">
             	<!--*********************Question And Answer*****************-->
                <?php /*?><tr>
                	<td style="background-color:#0CF;"><b>Requested Inputs by Planning(CT for Response 4hrs)</b></td>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                  	<a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_queans">
						<?php count_total_queans($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr><?php */?>
             	<tr style="background-color:#0CF;">
                	<td><b>Category</b></td>
                    <td></td>
                    <td><b>Avazonic</b></td>
                    <td><b>Capital Goods</b></td>
                    <td><b>Instrumentation</b></td>
                    <td><b>Non Production Consumables</b></td>
                    <td><b>Production Consumables</b></td>
                    <td><b>Security</b></td>
                    <td><b>Service</b></td>
                    <td><b>Sensors</b></td>
                    <td><b>Tools</b></td>
                    <td><b>Packing</b></td>
                    <td><b>Total</b></td>
                </tr>
                <!--**************************NOT Authorized PR'S************************-->
                
                <tr>
                	<td style="background-color:#0CF;"><b>Non Authorised PR'S</b></td>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prnotauth">
						<?php count_total_prnotauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <!--************************Pending Target Allocation For Freeze Movement (No) (Pending For Date Update)******************-->
                <tr>
                	<td style="background-color:#0CF;"><b>Pending Target Allocation for Freeze Movement (No)</b></td>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prauth">
					<?php count_total_prauth($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <!--************************Disapproved Purchase Requests By Purchase******************-->
                <tr>
                	<td style="background-color:#0CF;"><b>Disapproved PR By Purchase</b></td>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Avazonic',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Capital Goods',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Instrumentation',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Non Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Production Consumables',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Security',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Service',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Sensors',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Tools',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis('Packing',$pr_type); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_prdis">
					<?php count_total_prdis($all_category,$pr_type); ?>
                    </a>
                    </td>
                </tr>
                <!---------- AUTHORIZED PR GRAND TOTAL ----------->
                <tr>
                	<td rowspan="4" style="background-color:#0CF;"><b>GRAND TOTAL FROM PR AUTHORIZED</b></td>
                    <td style="background-color:#F90;"><b>LD</b></td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
					<?php count_ld_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td style="background-color:#F90;">
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ld_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ld_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Delay</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_delay_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_delay_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>OT</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_ontime_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_ontime_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
                <tr>
                    <td style="background-color:#0CF;"><b>Total</b></td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "avazonic"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Avazonic',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Capital Goods"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Capital Goods',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Instrumentation"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Instrumentation',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Non Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Non Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Production Consumables"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Production Consumables',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Security"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Security',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Service"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Service',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Sensors"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Sensors',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Tools"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Tools',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo "Packing"; ?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total('Packing',$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                    <td>
                    <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_details?category=<?php echo $all_category;?>&&pr_type=<?php echo $pr_type; ?>&&fun=det_total_grand_total&&start_date=<?php echo $cwsd; ?>&&end_date=<?php echo $ftwed; ?>">
						<?php count_total_grand_total($all_category,$pr_type,$cwsd,$ftwed); ?>
                    </a>
                    </td>
                </tr>
             </table>
        </div>
        <div class="col-lg-1"></div>
    </div><br />
    
  </section>
</section>