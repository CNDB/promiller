<?php $this->load->helper('ppc_report_helper'); ?>

<table class="table table-bordered">
    <tr style="background-color:#0CF;">
        <td><b>Category</b></td>
        <td><b>Avazonic</b></td>
        <td><b>Capital Goods</b></td>
        <td><b>Instrumentation</b></td>
        <td><b>IT</b></td>
        <td><b>Non Production Consumables</b></td>
        <td><b>Production Consumables</b></td>
        <td><b>Security</b></td>
        <td><b>Service</b></td>
        <td><b>Sensors</b></td>
        <td><b>Tools</b></td>
        <td><b>Packing</b></td>
        <td><b>Total</b></td>
    </tr>
    <?php
        $sql_stages = "select * from tipldb..ppc_stage_master order by priority"; 
        $qry_stages = $this->db->query($sql_stages);
        
        foreach($qry_stages->result() as $row){
    ?>
    <tr style="background-color:<?php echo $stage_bg_color = $row->stage_bg_color; ?>">
        <td style="background-color:#0CF;"><b><?php echo $stage = $row->stage; ?></b></td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Avazonic"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Avazonic',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Capital Goods"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Capital Goods',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Instrumentation"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Instrumentation',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "IT"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('IT',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Non Production Consumables"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Non Production Consumables',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Production Consumables"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Production Consumables',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Security"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Security',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Service"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Service',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Sensors"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Sensors',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Tools"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Tools',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "Packing"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt('Packing',$stage); ?>
            </a>
        </td>
        <td>
            <a href="<?php echo base_url(); ?>index.php/ppcc/ppc_report_nw_det?category=<?php echo "All"; ?>&stage=<?php echo $stage; ?>">
                <?php ppc_new_rpt_cnt_tot('All',$stage); ?>
            </a>
        </td>
    </tr>
    <?php } ?>
</table>