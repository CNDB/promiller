<?php include'header.php'; ?>

<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i> Master Dashboard</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Dashboard </li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2" style="font-size: 13px;">
        	<a href="<?php echo base_url(); ?>index.php/floorc" target="_blank">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Flour Mill
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/inventoryc" target="_blank">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/inventory_management.png" width="50%"/><br /><br />
                Inventory
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/logisticksc" target="_blank">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/logistics_management.png" width="50%"/><br /><br />
                Logistics
            </a>
        </div>

        <div class="col-lg-2">
            <a href="<?php echo base_url(); ?>index.php/purchasec" target="_blank">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/purchase_management.png" width="50%"/><br /><br />
                Purchase
            </a>
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/sales_management.png" width="50%"/><br /><br />
            Sales
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/order_management.png" width="50%"/><br /><br />
            Order
        </div>
    </div><br /><br />
    
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/hrms.png" width="50%"/><br /><br />
            HRM
        </div>

        <div class="col-lg-2">	
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/crm.png" width="50%"/><br /><br />
            CRM
        </div>

        <div class="col-lg-2">	
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/distributor_management.png" width="50%"/><br /><br />
            Distributor
        </div>

        <div class="col-lg-2">	
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/visitors.png" width="50%"/><br /><br />
            Visitors
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/contacts.png" width="50%"/><br /><br />
        	  Contacts
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/updated/fleet_hub.png" width="50%"/><br /><br />
        	  Fleet Hub
        </div>
    </div><br /><br />
    
  </section>
</section>
 
  
  <script>

      //knob
      $(function() {
        $(".knob").knob({
          'draw' : function () { 
            $(this.i).val(this.cv + '%')
          }
        })
      });

      //carousel
      $(document).ready(function() {
          $("#owl-slider").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });
	  
	  /* ---------- Map ---------- */
	$(function(){
	  $('#map').vectorMap({
	    map: 'world_mill_en',
	    series: {
	      regions: [{
	        values: gdpData,
	        scale: ['#000', '#000'],
	        normalizeFunction: 'polynomial'
	      }]
	    },
		backgroundColor: '#eef3f7',
	    onLabelShow: function(e, el, code){
	      el.html(el.html()+' (GDP - '+gdpData[code]+')');
	    }
	  });
	});



  </script>

<?php include('footer.php'); ?>