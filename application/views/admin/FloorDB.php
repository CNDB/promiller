<?php include'header.php'; echo "Hello ! ".$username = $_SESSION['username']; ?>
<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Flour Mill</h3>
            <ol class="breadcrumb">
                <li><i class="fa fa-home"></i><a href="<?php echo base_url(); ?>index.php/welcome/dashboard">Home</a></li>
                <li><i class="fa fa-laptop"></i>Flour Mill</li>						  	
            </ol>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/floorc" target="_blank">
                <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/mill_active_control.png" width="50%"/><br /><br />
                Mill Active Controls
            </a>
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/material_intake.png" width="50%"/><br /><br />
            Material Intake At Plant
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/pre_cleaning.png" width="50%"/><br /><br />
            Pre Cleaning
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/cleaning.png" width="50%"/><br /><br />
            Cleaning
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/refraction_silos.png" width="50%"/><br /><br />
            Refraction Silos
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/conditioning.png" width="50%"/><br /><br />
            Conditioning
        </div>
    </div><br /><br />
    
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/milling.png" width="50%"/><br /><br />
            Milling
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/atta_silos.png" width="50%"/><br /><br />
            Atta Silos
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/packing_silos.png" width="50%"/><br /><br />
        	Packing Silos
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/pg_lab_test.png" width="50%"/><br /><br />
            FG Lab Test
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/packing.png" width="50%"/><br /><br />
            Packing
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/plant_maintenance.png" width="50%"/><br /><br />
            Plant
        </div>
    </div><br /><br />
    
    <div class="row" style="text-align:center">
    	<div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/manpower_mgmt.png" width="50%"/><br /><br />
            Manpower
        </div>

        <div class="col-lg-2">
            <img src="<?php echo base_url(); ?>assets/admin/db/floor_mill/energy_monitor.png" width="50%"/><br /><br />
            Energy
        </div>

        <div class="col-lg-8">
        </div>
    </div><br /><br />
  </section>
</section>
<?php include('footer.php'); ?>