<?php include'header.php' ?>
<style>
	.land_text_cat{
		font-size:15px;
	}

	.land_text_head{
		font-size:20px;
		font-weight:600;
	}
</style>
<br><br><br>
<div class="row">
	<div class="col-lg-12">
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner">
				<div class="item active">
					<img src="<?php echo base_url(); ?>assets/images/slider/Promiller-Carousel-001.jpg" width="100%">
				</div>

				<div class="item">
					<img src="<?php echo base_url(); ?>assets/images/slider/MIllersoft_ban2.jpg" width="100%">
				</div>
			</div>
			<!-- Left and right controls -->
			<!--
			<a class="left carousel-control" href="#myCarousel" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="false"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="right carousel-control" href="#myCarousel" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="false"></span>
				<span class="sr-only">Next</span>
			</a>
			-->
		</div>
	</div>
</div><br><br>

<!-- Product Box -->
<div class="row">
	<div class="col-lg-3">
		<img src="<?php echo base_url(); ?>assets/images/front_cat/promiller.jpg" width="100%"><br>
		<h4 class="land_text_head">Promiller</h4>
		<p class="land_text">Analytics on the go.</p>
		<h5><a href="#">Expore Now  ></a></h5>
	</div>
	<div class="col-lg-3">
		<img src="<?php echo base_url(); ?>assets/images/front_cat/ai-2.jpg" width="100%"><br>
		<h4 class="land_text_head">Just Say "AUM"</h4>
		<p class="land_text">Experience the power of AI with a smart assistant.</p>
		<h5><a href="#">Expore Now  ></a></h5>
	</div>
	<div class="col-lg-3">
		<img src="<?php echo base_url(); ?>assets/images/front_cat/miller_soft.jpg" width="100%"><br>
		<h4 class="land_text_head">Miller Soft</h4>
		<p class="land_text">Get control of all activities of your Milling Plant.</p>
		<h5><a href="#">Expore Now  ></a></h5>
	</div>
	<div class="col-lg-3">
		<img src="<?php echo base_url(); ?>assets/images/front_cat/upgrade_digi-2.jpg" width="100%"><br>
		<h4 class="land_text_head">Machine Learning</h4>
		<p class="land_text">Technology to monitor plant activities digitally.</p>
		<h5><a href="#">Expore Now  ></a></h5>
	</div>
</div>

	
<?php include'footer.php' ?>