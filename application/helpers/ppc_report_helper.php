<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('ppc_report'))
{
	
	//Requested Inputs by Planning(CT for Response 4hrs) Count
	function  count_ld_queans($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR Delay Cases
	function  count_delay_queans($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR On Time Cases
	function count_ontime_queans($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR Total Cases
	function count_total_queans($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	/************** Not Authorized Purchase Requests *************/
	
	//Count Not Authorized PR LD Cases
	function  count_ld_prnotauth($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Delay Not Authorized PR Cases
	function count_delay_prnotauth($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count On Time Not Authorized PR Cases
	function count_ontime_prnotauth($category,$pr_type)
	{
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Total Not Authorized PR Cases
	function count_total_prnotauth($category,$pr_type)
	{
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	/********* Authorized Purchase Requests **********/
	
	//Count Authorized PR LD Cases
	function  count_ld_prauth($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR Delay Cases
	function  count_delay_prauth($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR On Time Cases
	function count_ontime_prauth($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR Total Cases
	function count_total_prauth($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	/********* Disapproved Purchase Requests by Purchase Department **********/
	
	//Count Disapproved PR LD Cases
	function  count_ld_prdis($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Disapproved PR Delay Cases
	function  count_delay_prdis($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Disapproved PR On Time Cases
	function count_ontime_prdis($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Disapproved PR Total Cases
	function count_total_prdis($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	/********* Authorized Purchase Request Wrong Commitment **********/
	
	//Count Authorized PR LD Cases
	function  count_ld_currdate($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR Delay Cases
	function  count_delay_currdate($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR On Time Cases
	function count_ontime_currdate($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	//Count Authorized PR Total Cases
	function count_total_currdate($category,$pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	/********** Authorized Purchase Request Whose LD Date/Need date in Given Range **********/
	
	function  count_ld_prauth_weekwise($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		if($start_date == "no_date"){
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases greater than fourth week' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')";
		
		} else {
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases between first start date and last week end date' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')
		and convert(date,last_fm_date) between '$start_date' and '$end_date'";
			
		}
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	function  count_delay_prauth_weekwise($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		if($start_date == "no_date"){
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases greater than fourth week'
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')";
		
		} else {
			
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases between first start date and last week end date' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')
		and convert(date,last_fm_date) between '$start_date' and '$end_date'";
			
		}
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	function  count_ontime_prauth_weekwise($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		if($start_date == "no_date"){
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases greater than fourth week' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')";
		
		} else {
			
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases between first start date and last week end date' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')
		and convert(date,last_fm_date) between '$start_date' and '$end_date'";
		
		}
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	function  count_total_prauth_weekwise($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		if($start_date == "no_date"){
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases greater than fourth week' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')";
		
		} else {
		
		$sql = "select count(*) as count1 from tipldb..ppc_report_details 
		where flag='Cases between first start date and last week end date' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') 
		and pr_category in('$category')
		and convert(date,last_fm_date) between '$start_date' and '$end_date'";	
			
		}
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	
	
	/********** GRAND TOTAL OF ALL THE AUTHORIZED CASES **********/
	
	function  count_ld_grand_total($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql="select count(*) as count1 from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	function  count_delay_grand_total($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
			
		$sql="select count(*) as count1 from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	function  count_ontime_grand_total($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
			
		$sql="select count(*) as count1 from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	
	function  count_total_grand_total($category, $pr_type, $start_date, $end_date){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$sql="select count(*) as count1 from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";	
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
	}
	

/********************************************************** DETAILS *******************************************************************/

//Requested Inputs by Planning(CT for Response 4hrs) Details
	function  det_ld_queans($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>PR NO.</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>REORDER</b></td>
				<td><b>CURRENT STOCK</b></td>
				<td><b>PENDING ALLOCATION</b></td>
				<td><b>ERP CREATED BY</b></td>
				<td><b>ERP CREATED DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>LAST FM DATE</b></td>
				<td><b>LAST REMARKS</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,customer_name
		from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$category</td>
				<td>$reorder_level</td>
				<td>$current_stock</td>
				<td>$pending_allocation</td>
				<td>$created_by</td>
				<td>$create_date</td>
				<td style='background-color:#FF0'>$need_date</td>
				<td>$ld_date</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$project_name</td>
				<td>$customer_name</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_erp_stat</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$last_remarks</td>
			  </tr>";
		}
		
		echo $html;
	}
	
	//Details Authorized PR Delay Cases
	function  det_delay_queans($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>PR NO.</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>REORDER</b></td>
				<td><b>CURRENT STOCK</b></td>
				<td><b>PENDING ALLOCATION</b></td>
				<td><b>ERP CREATED BY</b></td>
				<td><b>ERP CREATED DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>LAST FM DATE</b></td>
				<td><b>LAST REMARKS</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,customer_name
		from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$category</td>
				<td>$reorder_level</td>
				<td>$current_stock</td>
				<td>$pending_allocation</td>
				<td>$created_by</td>
				<td>$create_date</td>
				<td style='background-color:#FF0'>$need_date</td>
				<td>$ld_date</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$project_name</td>
				<td>$customer_name</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_erp_stat</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$last_remarks</td>
			  </tr>";
		}
		
		echo $html;
	}
	
	//Details Authorized PR On Time Cases
	function det_ontime_queans($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>PR NO.</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>REORDER</b></td>
				<td><b>CURRENT STOCK</b></td>
				<td><b>PENDING ALLOCATION</b></td>
				<td><b>ERP CREATED BY</b></td>
				<td><b>ERP CREATED DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>LAST FM DATE</b></td>
				<td><b>LAST REMARKS</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,customer_name
		from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$category</td>
				<td>$reorder_level</td>
				<td>$current_stock</td>
				<td>$pending_allocation</td>
				<td>$created_by</td>
				<td>$create_date</td>
				<td style='background-color:#FF0'>$need_date</td>
				<td>$ld_date</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$project_name</td>
				<td>$customer_name</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_erp_stat</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$last_remarks</td>
			  </tr>";
		}
		
		echo $html;
	}
	
	//Details Authorized PR Total Cases
	function det_total_queans($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>PR NO.</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>REORDER</b></td>
				<td><b>CURRENT STOCK</b></td>
				<td><b>PENDING ALLOCATION</b></td>
				<td><b>ERP CREATED BY</b></td>
				<td><b>ERP CREATED DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>LAST FM DATE</b></td>
				<td><b>LAST REMARKS</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,customer_name
		from tipldb..ppc_report_details 
		where flag='Requested Inputs by Planning(CT for Response 4hrs)' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$category</td>
				<td>$reorder_level</td>
				<td>$current_stock</td>
				<td>$pending_allocation</td>
				<td>$created_by</td>
				<td>$create_date</td>
				<td style='background-color:#FF0'>$need_date</td>
				<td>$ld_date</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$project_name</td>
				<td>$customer_name</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_erp_stat</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$last_remarks</td>
			  </tr>";
		}
		
		echo $html;
	}

/***********Fresh PR Cases***********/

//Details LD Not Authorized PR Cases
	function det_ld_prnotauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

//Details Delay Not Authorized PR Cases
	function det_delay_prnotauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

//Details Ontime Not Authorized PR Cases
	function det_ontime_prnotauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

//Details Total Not Authorized PR Cases
	function det_total_prnotauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag='Non Authorised PR' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	/******************Authorized PR Cases Date Whose FM Not Updated*********************/

	//Details Delay Authorized PR Cases
	function det_ld_prauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Delay Authorized PR Cases
	function det_delay_prauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details On Time Authorized PR Cases
	function det_ontime_prauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Total Authorized PR Cases
	function det_total_prauth($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Pending Target Allocation for Freeze Movement (No)' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}
	
	/******************Disapproved PR Cases Date Whose FM Not Updated*********************/

	//Details Delay Disapproved PR Cases
	function det_ld_prdis($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Delay Disapproved PR Cases
	function det_delay_prdis($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details On Time Disapproved PR Cases
	function det_ontime_prdis($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Total Disapproved PR Cases
	function det_total_prdis($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Disapproved PR By Purchase' 
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}
	
	/******************Authorized PR Cases Date Whose FM Date Updated and FM Date Less than current date*********************/

	//Details Delay Authorized PR Cases whose FM Date Updated
	function det_ld_currdate($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Delay Authorized PR Cases whose FM Date Updated
	function det_delay_currdate($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = number_format($row->reorder_level,2,".","");
			  $current_stock = number_format($row->current_stock,2,".","");
			  $pending_allocation = number_format($row->pending_allocation,2,".","");
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details On Time Authorized PR Cases whose FM Date Updated
	function det_ontime_currdate($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Total Authorized PR Cases whose FM Date Updated
	function det_total_currdate($category,$pr_type,$user_id,$user_type,$name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		echo $category;
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
		
		$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag='Cases Less Than Current Date' 
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') and pr_category in('$category')";
	
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

/*********************************** Weekwise Details *********************************/

	//Details LD Authorized PR Weekwise
	function det_ld_prauth_weekwise($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
			if($start_date == "no_date"){
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
				from tipldb..ppc_report_details 
				where flag='Cases greater than fourth week' 
				and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')";
			
			} else {
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
				from tipldb..ppc_report_details 
				where flag='Cases between first start date and last week end date' 
				and case_type in('LD') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')
				and convert(date,last_fm_date) between '$start_date' and '$end_date'";
			
			}
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Delay Authorized PR Cases Weekwise
	function det_delay_prauth_weekwise($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
			if($start_date == "no_date"){
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
				from tipldb..ppc_report_details 
				where flag='Cases greater than fourth week' 
				and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')";
			
			} else {
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
				from tipldb..ppc_report_details 
				where flag='Cases between first start date and last week end date' 
				and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')
				and convert(date,last_fm_date) between '$start_date' and '$end_date'";
			
			}
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details On Time Authorized PR Cases Weekwise
	function det_ontime_prauth_weekwise($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
			if($start_date == "no_date"){
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
				from tipldb..ppc_report_details 
				where flag='Cases greater than fourth week' 
				and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')";
			
			} else {
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
				from tipldb..ppc_report_details 
				where flag='Cases between first start date and last week end date' 
				and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')
				and convert(date,last_fm_date) between '$start_date' and '$end_date'";
			
			}
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Total Authorized PR Cases Weekwise
	function det_total_prauth_weekwise($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		//echo $category;
		//die;
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
			if($start_date == "no_date"){
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
				from tipldb..ppc_report_details 
				where flag='Cases greater than fourth week' 
				and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')";
			
			} else {
			
				$sql = "select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
				atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
				customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
				from tipldb..ppc_report_details 
				where flag='Cases between first start date and last week end date' 
				and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type') 
				and pr_category in('$category')
				and convert(date,last_fm_date) between '$start_date' and '$end_date'";
			
			}
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}
	
	
	
	/*********************************** GRAND TOTAL OF AUTHORIZED CASES *********************************/

	//Details LD Authorized PR Grand Total
	function det_ld_grand_total($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
		$sql="select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('LD') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Delay Authorized PR Cases Grand Total
	function det_delay_grand_total($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
		$sql="select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('DELAY') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details On Time Authorized PR Cases Grand Total
	function det_ontime_grand_total($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
		$sql="select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions 
		from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('ONTIME') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}

	//Details Total Authorized PR Cases Grand Total
	function det_total_grand_total($category, $pr_type, $start_date, $end_date, $user_id, $user_type, $name){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == "All"){
			$category = "Avazonic','Capital Goods','Instrumentation','Non Production Consumables','Production Consumables','Security','Service','Sensors','Tools','Packing";
		}
		
		
		
		$html="
			  <h3>Question And Answer LD</h3><br>
			  <b style='color:red'>* Click On PR Number To Enter Details</b>	
			  <tr style='background-color:#0CF;'>
				<td><b>SNO</b></td>
				<td><b>ATAC NO.</b></td>
				<td><b>SO NO.</b></td>
				<td><b>CUSTOMER NAME</b></td>
				<td><b>PROJECT NAME</b></td>
				<td><b>CUSTOMER NEED DATE</b></td>
				<td><b>LD DATE</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>PR NO.</b></td>
				<td><b>PR CREATE DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO RELEASE DATE</b></td>
				<td><b>PR NEED DATE</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC</b></td>
				<td><b>PR TYPE</b></td>
				<td><b>AVERAGE LEAD TIME</b></td>
				<td><b>HISTORY PREDICTED DATE</b></td>
				<td><b>PURCHASE PREDICTED FM DATE</b></td>
				<td><b>PREDICTION NO</b></td>
				<td><b>LAST REMARKS</b></td>
				<td><b>CATEGORY</b></td>
			  </tr>";
			  
		$sql="select pr_num,item_code,item_desc,pr_category,erp_created_by,erp_create_date,pr_need_date,ld_date,
		atac_no,project_name,po_number,po_erp_status,last_fm_date,last_remarks,reorder_level,current_stock,pending_allocation,
		customer_name,so_number,so_need_date,po_auth_date,pr_type,hist_lead_time,exp_prob_date,revisions
		from tipldb..ppc_report_details 
		where flag in('Pending Target Allocation for Freeze Movement (No)','Disapproved PR By Purchase'
		,'Cases between first start date and last week end date','Cases greater than fourth week','Cases Less Than Current Date')
		and case_type in('DELAY','ONTIME') and substring(pr_num,1,3) in('$pr_type')
		and pr_category in('$category')";
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
			  $sno++;
			  $pr_num        = $row->pr_num;
			  $item_code     = $row->item_code;
			  $itm_desc1     = $row->item_desc;
			  $itm_desc      = mb_convert_encoding($itm_desc1, "ISO-8859-1", "UTF-8");
			  $itm_desc      = strip_tags($itm_desc);
			  $category      = $row->pr_category;
			  $created_by    = $row->erp_created_by;
			  $create_date   = substr($row->erp_create_date,0,11);
			  $need_date     = substr($row->pr_need_date,0,11);
			  $ld_date       = substr($row->ld_date,0,11);
			  $atac_no       = $row->atac_no;
			  $project_name  = $row->project_name;
			  $po_num        = $row->po_number;
			  $po_erp_stat   = $row->po_erp_status;
			  $last_fm_date  = substr($row->last_fm_date,0,11);
			  $last_remarks  = $row->last_remarks;
			  $item_code1    = urlencode($item_code);
			  $reorder_level = $row->reorder_level;
			  $current_stock = $row->current_stock;
			  $pending_allocation = $row->pending_allocation;
			  $customer_name = $row->customer_name;
			  //New Columns Added
			  $so_number = $row->so_number;
			  $so_need_date = substr($row->so_need_date,0,11);
			  $po_auth_date = substr($row->po_auth_date,0,11);
			  $pr_type = $row->pr_type;
			  $hist_lead_time = $row->hist_lead_time;
			  $exp_prob_date = substr($row->exp_prob_date,0,11);
			  $revisions = $row->revisions;
			  	
			  if(strpos($item_code1, '%2F') !== false){ 
			  	$item_code2 = str_replace("%2F","chandra",$item_code1); 
			  }else{ 
			  	$item_code2 = $item_code1; 
			  }
			  
			  if($reorder_level > 0){
				  $reorder_level = 'Yes';
			  } else {
				  $reorder_level = 'No';
			  }
			  
			  $html.="
			  <tr>
				<td>$sno</td>
				<td><a href='http://live.tipl.com/tipl_project1/atac_new/view_atac_details1.php?atacno=".$atac_no."&user_id=".$user_id."&user_type=".$user_type."&name=".$name."' target='_blank'>$atac_no</a></td>
				<td>$so_number</td>
				<td>$customer_name</td>
				<td>$project_name</td>
				<td>$so_need_date</td>
				<td>$ld_date</td>
				<td>$po_erp_stat</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_pr_entry/".$pr_num."' target='_blank'>$pr_num</a></td>
				<td>$create_date</td>
				<td><a href='".base_url()."index.php/ppcc/ppc_po_entry/".$po_num."'  target='_blank'>$po_num</a></td>
				<td>$po_auth_date</td>
				<td>$need_date</td>
				<td><a href='".base_url()."index.php/createpoc/pendal_view/".$item_code2."' target='_blank'>$item_code</a></td>
				<td>$itm_desc</td>
				<td>$pr_type</td>
				<td>$hist_lead_time</td>
				<td>$exp_prob_date</td>
				<td style='background-color:#FF0'>$last_fm_date</td>
				<td>$revisions</td>
				<td>$last_remarks</td>
				<td>$category</td>
			  </tr>";
		}
		
		echo $html;
	}
	

/********** PPC Report New Format ****************/

	//PR WISE DATA STARTS
	function ppc_new_rpt_cnt($pr_type, $category, $stage, $usage_type, $pending_on, $user_id){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT') and user_id = '$user_id'";
		
		//Purchaser Check
		$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
		where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";
		
		$qry_pur=$ci->db->query($sql_pur);
		
		foreach($qry_pur->result() as $row){
			$count1 = $row->count1;
		}
		
		//Planner Check
		$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
		where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
		and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";
		
		$qry_plan=$ci->db->query($sql_plan);
		
		foreach($qry_plan->result() as $row){
			$count2 = $row->count2;
		}
	
		//PR Type
		if($pr_type == ''){
			$where_str .= "";
		} else if($pr_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_series in(".$pr_type.")";	
		}
		
		//PR Category
		if($category == ''){
			$where_str .= "";
		} else if($category == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_category in('".$category."')";	
		}
		
		//Department
		if($department == ''){
			$where_str .= "";
		} else if($department == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and department in(".$department.")";	
		}
		
		//Usage Type
		if($usage_type == ''){
			$where_str .= "";
		} else if($usage_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_type in(".$usage_type.")";	
		}
		
		//Usage Type
		if($stage == ''){
			$where_str .= "";
		} else if($stage == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and flag in('".$stage."')";	
		}
		
		//Pending ON
		if($pending_on == ''){
			$where_str .= "";
		} else if($pending_on == 'All'){	
			$where_str .= "";	
		} else {	
			//$where_str .= " and pending_on in('".$pending_on."')";
			
			//New Condition
			if($count2 > 0){
				$where_str .= " and erp_created_by in('".$pending_on."')";
			} else if($count1 > 0){
				$where_str .= " and po_created_by in('".$pending_on."')";
			} else {
				$where_str .= " and pending_on in('".$pending_on."')";
			}	
		}
		
		//$where_str .= " order by priority";
			  
		$sql="select count(*) as count1 from tipldb..ppc_report_details_test_new where $where_str";
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		//echo $sql;
	}
	
	function ppc_new_rpt_cnt_tot($pr_type, $category, $stage, $usage_type, $pending_on, $user_id){
		
		$ci =& get_instance();
		$ci->load->database();

		$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT') and user_id = '$user_id'";
		
		//Purchaser Check
		$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
		where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";
		
		$qry_pur=$ci->db->query($sql_pur);
		
		foreach($qry_pur->result() as $row){
			$count1 = $row->count1;
		}
		
		//Planner Check
		$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
		where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
		and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";
		
		$qry_plan=$ci->db->query($sql_plan);
		
		foreach($qry_plan->result() as $row){
			$count2 = $row->count2;
		}
	
		//PR Type
		if($pr_type == ''){
			$where_str .= "";
		} else if($pr_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_series in(".$pr_type.")";	
		}
		
		//PR Category
		if($category == ''){
			$where_str .= "";
		} else if($category == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_category in('".$category."')";	
		}
		
		//Department
		if($department == ''){
			$where_str .= "";
		} else if($department == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and department in(".$department.")";	
		}
		
		//Usage Type
		if($usage_type == ''){
			$where_str .= "";
		} else if($usage_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_type in(".$usage_type.")";	
		}
		
		//Usage Type
		if($stage == ''){
			$where_str .= "";
		} else if($stage == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and flag in('".$stage."')";	
		}
		
		//Pending ON
		if($pending_on == ''){
			$where_str .= "";
		} else if($pending_on == 'All'){	
			$where_str .= "";	
		} else {	
			//$where_str .= " and pending_on in('".$pending_on."')";
			
			//New Condition
			if($count2 > 0){
				$where_str .= " and erp_created_by in('".$pending_on."')";
			} else if($count1 > 0){
				$where_str .= " and po_created_by in('".$pending_on."')";
			} else {
				$where_str .= " and pending_on in('".$pending_on."')";
			}		
		}
			  
		$sql="select count(*) as count1 from tipldb..ppc_report_details_test_new where $where_str";
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		//echo $sql;
	}
	
	//Ajax Pages total cases
	function ppc_new_rpt_cnt_dept($pr_type, $category, $department, $usage_type, $pending_on, $user_id){
		
		$ci =& get_instance();
		$ci->load->database();

		$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT') and user_id = '$user_id'";
		
		//Purchaser Check
		$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
		where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";
		
		$qry_pur=$ci->db->query($sql_pur);
		
		foreach($qry_pur->result() as $row){
			$count1 = $row->count1;
		}
		
		//Planner Check
		$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
		where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
		and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";
		
		$qry_plan=$ci->db->query($sql_plan);
		
		foreach($qry_plan->result() as $row){
			$count2 = $row->count2;
		}
	
		//PR Type
		if($pr_type == ''){
			$where_str .= "";
		} else if($pr_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_series in(".$pr_type.")";	
		}
		
		//PR Category
		if($category == ''){
			$where_str .= "";
		} else if($category == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_category in('".$category."')";	
		}
		
		//Department
		if($department == ''){
			$where_str .= "";
		} else if($department == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and department in(".$department.")";	
		}
		
		//Usage Type
		if($usage_type == ''){
			$where_str .= "";
		} else if($usage_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_type in(".$usage_type.")";	
		}
		
		//Usage Type
		if($stage == ''){
			$where_str .= "";
		} else if($stage == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and flag in('".$stage."')";	
		}
		
		//Pending ON
		if($pending_on == ''){
			$where_str .= "";
		} else if($pending_on == 'All'){	
			$where_str .= "";	
		} else {	
			//$where_str .= " and pending_on in('".$pending_on."')";
			
			//New Condition
			if($count2 > 0){
				$where_str .= " and erp_created_by in('".$pending_on."')";
			} else if($count1 > 0){
				$where_str .= " and po_created_by in('".$pending_on."')";
			} else {
				$where_str .= " and pending_on in('".$pending_on."')";
			}		
		}
			  
		$sql="select count(*) as count1 from tipldb..ppc_report_details_test_new where $where_str";
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		//echo $sql;
	}
	
	//PO WISE DATA STARTS
	function ppc_new_rpt_cnt_po($pr_type, $category, $stage, $usage_type, $pending_on, $user_id){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT','ERP AUTHORIZED PR PENDING FOR ERP PO CREATION','DISAPPROVED PURCHASE REQUEST BY PURCHASE DEPARTMENT') and user_id = '$user_id' and po_number is not null and po_number != ''";
		
		//Purchaser Check
		$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
		where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";
		
		$qry_pur=$ci->db->query($sql_pur);
		
		foreach($qry_pur->result() as $row){
			$count1 = $row->count1;
		}
		
		//Planner Check
		$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
		where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
		and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";
		
		$qry_plan=$ci->db->query($sql_plan);
		
		foreach($qry_plan->result() as $row){
			$count2 = $row->count2;
		}
	
		//PR Type
		if($pr_type == ''){
			$where_str .= "";
		} else if($pr_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_series in(".$pr_type.")";	
		}
		
		//PR Category
		if($category == ''){
			$where_str .= "";
		} else if($category == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_category in('".$category."')";	
		}
		
		//Department
		if($department == ''){
			$where_str .= "";
		} else if($department == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and department in(".$department.")";	
		}
		
		//Usage Type
		if($usage_type == ''){
			$where_str .= "";
		} else if($usage_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_type in(".$usage_type.")";	
		}
		
		//Usage Type
		if($stage == ''){
			$where_str .= "";
		} else if($stage == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and flag in('".$stage."')";	
		}
		
		//Pending On
		if($pending_on == ''){
			$where_str .= "";
		} else if($pending_on == 'All'){	
			$where_str .= "";	
		} else {	
			//$where_str .= " and pending_on in('".$pending_on."')";
			
			//New Condition
			if($count2 > 0){
				$where_str .= " and erp_created_by in('".$pending_on."')";
			} else if($count1 > 0){
				$where_str .= " and po_created_by in('".$pending_on."')";
			} else {
				$where_str .= " and pending_on in('".$pending_on."')";
			}		
		}
		
		//$where_str .= " order by priority";
			  
		$sql="select count(*) as count1 from(
			select distinct po_number, entry_no from tipldb..ppc_report_details_test_new where $where_str
		) as abc";
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
	}
	
	function ppc_new_rpt_cnt_tot_po($pr_type, $category, $stage, $usage_type, $pending_on, $user_id){
		
		$ci =& get_instance();
		$ci->load->database();

		$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT','ERP AUTHORIZED PR PENDING FOR ERP PO CREATION','DISAPPROVED PURCHASE REQUEST BY PURCHASE DEPARTMENT') and user_id = '$user_id' and po_number is not null and po_number != ''";
		
		//Purchaser Check
		$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
		where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";
		
		$qry_pur=$ci->db->query($sql_pur);
		
		foreach($qry_pur->result() as $row){
			$count1 = $row->count1;
		}
		
		//Planner Check
		$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
		where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
		and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";
		
		$qry_plan=$ci->db->query($sql_plan);
		
		foreach($qry_plan->result() as $row){
			$count2 = $row->count2;
		}
	
		//PR Type
		if($pr_type == ''){
			$where_str .= "";
		} else if($pr_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_series in(".$pr_type.")";	
		}
		
		//PR Category
		if($category == ''){
			$where_str .= "";
		} else if($category == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_category in('".$category."')";	
		}
		
		//Department
		if($department == ''){
			$where_str .= "";
		} else if($department == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and department in(".$department.")";	
		}
		
		//Usage Type
		if($usage_type == ''){
			$where_str .= "";
		} else if($usage_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_type in(".$usage_type.")";	
		}
		
		//Usage Type
		if($stage == ''){
			$where_str .= "";
		} else if($stage == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and flag in('".$stage."')";	
		}
		
		//Pending On
		if($pending_on == ''){
			$where_str .= "";
		} else if($pending_on == 'All'){	
			$where_str .= "";	
		} else {	
			//$where_str .= " and pending_on in('".$pending_on."')";
			
			//New Condition
			if($count2 > 0){
				$where_str .= " and erp_created_by in('".$pending_on."')";
			} else if($count1 > 0){
				$where_str .= " and po_created_by in('".$pending_on."')";
			} else {
				$where_str .= " and pending_on in('".$pending_on."')";
			}		
		}
			  
		$sql="select count(*) as count1 from(
			select distinct po_number, entry_no from tipldb..ppc_report_details_test_new where $where_str
		) as abc";
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
	}
	
	//Ajax Pages total cases
	function ppc_new_rpt_cnt_dept_po($pr_type, $category, $department, $usage_type, $pending_on, $user_id){
		
		$ci =& get_instance();
		$ci->load->database();

		$where_str = "flag not in('PENDING FOR TARGET ALLOCATION FOR FREEZE MOVEMENT','ERP AUTHORIZED PR PENDING FOR ERP PO CREATION','DISAPPROVED PURCHASE REQUEST BY PURCHASE DEPARTMENT') and user_id = '$user_id' and po_number is not null and po_number != ''";
		
		//Purchaser Check
		$sql_pur="select count(*) as count1 from tipldb..ppc_report_details_test_new 
		where po_created_by is not null and po_created_by != '' and po_created_by = '$pending_on' and user_id = '$user_id'";
		
		$qry_pur=$ci->db->query($sql_pur);
		
		foreach($qry_pur->result() as $row){
			$count1 = $row->count1;
		}
		
		//Planner Check
		$sql_plan="select count(*) as count2 from tipldb..ppc_report_details_test_new 
		where erp_created_by is not null and erp_created_by != '' and erp_created_by = '$pending_on' and user_id = '$user_id' 
		and erp_created_by not in('RAJESH.KUMAR','POOJA.YADAV')";
		
		$qry_plan=$ci->db->query($sql_plan);
		
		foreach($qry_plan->result() as $row){
			$count2 = $row->count2;
		}
	
		//PR Type
		if($pr_type == ''){
			$where_str .= "";
		} else if($pr_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_series in(".$pr_type.")";	
		}
		
		//PR Category
		if($category == ''){
			$where_str .= "";
		} else if($category == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_category in('".$category."')";	
		}
		
		//Department
		if($department == ''){
			$where_str .= "";
		} else if($department == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and department in(".$department.")";	
		}
		
		//Usage Type
		if($usage_type == ''){
			$where_str .= "";
		} else if($usage_type == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and pr_type in(".$usage_type.")";	
		}
		
		//Usage Type
		if($stage == ''){
			$where_str .= "";
		} else if($stage == 'All'){	
			$where_str .= "";	
		} else {	
			$where_str .= " and flag in('".$stage."')";	
		}
		
		//Pending On
		if($pending_on == ''){
			$where_str .= "";
		} else if($pending_on == 'All'){	
			$where_str .= "";	
		} else {	
			//$where_str .= " and pending_on in('".$pending_on."')";
			
			//New Condition
			if($count2 > 0){
				$where_str .= " and erp_created_by in('".$pending_on."')";
			} else if($count1 > 0){
				$where_str .= " and po_created_by in('".$pending_on."')";
			} else {
				$where_str .= " and pending_on in('".$pending_on."')";
			}		
		}
			  
		$sql="select count(*) as count1 from(
			select distinct po_number, entry_no from tipldb..ppc_report_details_test_new where $where_str
		) as abc";
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
	}

}