<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('flow'))
{
	//Fresh PR count
	function count_fresh_pr(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from scmdb..prq_preqm_pur_reqst_hdr
		where substring(preqm_prno, 1, 3) in ('FPR','IPR','LPR','CGP') and preqm_status = 'FR'
		and preqm_prno not in (select pr_num from tipldb..pr_submit_table)";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
		
	//Fresh PR Details
	function fresh_pr($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>ITEM DESC.</td>
			<td>PR AGE</td>
			<td>ERP CREATED BY</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, preqm_prdate, getdate()) as diff from scmdb..prq_preqm_pur_reqst_hdr
		where substring(preqm_prno, 1, 3) in ('$pr_type') and preqm_status = 'FR'
		and preqm_prno not in (select pr_num from tipldb..pr_submit_table) ORDER BY  preqm_prno ASC";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$prnum        = $row->preqm_prno;
			$age          = $row->diff;
			$erp_created_by = $row->preqm_createdby;
			
			$sql1 = "select * from scmdb..prq_prqit_item_detail a, scmdb..itm_lov_varianthdr b where a.prqit_itemcode = b.lov_itemcode
					and a.prqit_prno = '$prnum'";
			$query1 = $ci->db->query($sql1);
			foreach ($query1->result() as $row) {
				$item_code    = $row->prqit_itemcode;
				$item_desc    = $row->lov_itmvardesc;
			}
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/createpoc/create_pr/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.substr($item_desc,0,20).'...</td>
				<td>'.$age.'&nbsp;Days</td>
				<td>'.$erp_created_by.'</td>
		  	</tr>';	
		}
		
		echo $html;
		
    }
	
	//Drawing count
	function count_drawing(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table where SUBSTRING(pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') 
		and drawing_no != '' and pr_status = 'Pending to upload drawing'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Drawing Details
	function drawing_det($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>ITEM DESC</td>
			<td>DRAWING NO.</td>
			<td>PR AGE</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, pr_date, getdate()) as diff from TIPLDB..pr_submit_table 
		 where SUBSTRING(pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and drawing_no != '' and pr_status = 'Pending to upload drawing'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$prnum = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->itm_desc;
			$drawing_no = $row->drawing_no;
			$diff = $row->diff;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/drawingc/create_pr/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
    			<td>'.$item_desc.'</td>
    			<td>'.$drawing_no.'</td>
    			<td>'.$diff.'</td>
		  	</tr>';	
		}	
		echo $html;	
    }
	
	//Authorize PR Level1 Count
	function count_pr_l1(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and a.pr_status = 'Pending For PR Authorize' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Authorize PR Level1 Details
	function det_pr_l1($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>CATEGORY</td>
			<td>PROJECT NAME</td>
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>PR AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, pr_date, getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('$pr_type') and a.pr_status = 'Pending For PR Authorize' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$category = $row->category;
			$project_name = $row->project_name;
			$prnum = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->itm_desc;
			$age = $row->diff;
			$pending_on = $row->level1_approval_mailto;
			
			$html.='
			<tr>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td><a href="'.base_url().'index.php/createpoc/view_pr_auth/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.$age.'&nbsp;Days</td>
				<td>'.$pending_on.'</td>
		  	</tr>';	
		}	
		echo $html;	
    }
	
	//Disapprove PR Level1 Count
	function count_dis_pr_l1(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and a.pr_status = 'PR Disapproved At Planning Level1' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Disapprove PR Level1 Details
	function det_dis_pr_l1($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>CATEGORY</td>
			<td>PROJECT NAME</td>
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>PR AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, pr_date, getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('$pr_type') and a.pr_status = 'PR Disapproved At Planning Level1' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$category = $row->category;
			$project_name = $row->project_name;
			$prnum = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->itm_desc;
			$age = $row->diff;
			$pending_on = $row->created_by;
			
			$html.='
			<tr>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td><a href="'.base_url().'index.php/createpoc/create_pr_disapproved/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.$age.'&nbsp;Days</td>
				<td>'.$pending_on.'</td>
		  	</tr>';	
		}	
		echo $html;	
    }
	
	//Authorize PR Level2 Count
	function count_pr_l2(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and a.pr_status = 'PR Approved At Planning Level1' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Authorize PR Level2 Details
	function det_pr_l2($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>CATEGORY</td>
			<td>PROJECT NAME</td>
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>PR AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, pr_date, getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('$pr_type') and a.pr_status = 'PR Approved At Planning Level1' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$category = $row->category;
			$project_name = $row->project_name;
			$prnum = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->itm_desc;
			$age = $row->diff;
			$pending_on = $row->level2_approval_mailto;
			
			$html.='
			<tr>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td><a href="'.base_url().'index.php/createpoc/view_pr_auth_lvl2/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.$age.'&nbsp;Days</td>
				<td>'.$pending_on.'</td>
		  	</tr>';	
		}	
		echo $html;	
    }
	
	//Disapprove PR Level2 Count
	function count_dis_pr_l2(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and a.pr_status = 'PR Disapproved At Planning Level2' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Disapprove PR Level2 Details
	function det_dis_pr_l2($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>CATEGORY</td>
			<td>PROJECT NAME</td>
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>PR AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, pr_date, getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('$pr_type') and a.pr_status = 'PR Disapproved At Planning Level2' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$category = $row->category;
			$project_name = $row->project_name;
			$prnum = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->itm_desc;
			$age = $row->diff;
			$pending_on = $row->created_by;
			
			$html.='
			<tr>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td><a href="'.base_url().'index.php/createpoc/create_pr/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.$age.'&nbsp;Days</td>
				<td>'.$pending_on.'</td>
		  	</tr>';	
		}	
		echo $html;	
    }
	
	//Live Authorized ERP Not Auth PR Count
	function count_erp_not_auth_pr(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and a.pr_status = 'Pending For PR Authorize In ERP' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Disapprove PR Level2 Details
	function det_erp_not_auth_pr($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>CATEGORY</td>
			<td>PROJECT NAME</td>
			<td>PR NO.</td>
			<td>ITEM CODE</td>
			<td>PR AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, pr_date, getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('$pr_type') and a.pr_status = 'Pending For PR Authorize In ERP' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'FR'";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$category = $row->category;
			$project_name = $row->project_name;
			$prnum = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->itm_desc;
			$age = $row->diff;
			$pending_on = $row->created_by;
			
			$html.='
			<tr>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td><a href="'.base_url().'index.php/createpoc/erp_not_auth_pr/'.$prnum.'">'.$prnum.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.$age.'&nbsp;Days</td>
				<td>'.$pending_on.'</td>
		  	</tr>';	
		}	
		echo $html;	
    }
	
	//Count PR Pending For PO Creation
	function count_pr_po_pend(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..pr_po_report_table where balance_qty > 0";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Details PR Pending For PO Creation
	function det_pr_po_pend($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>PR NO</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>COSTING</b></td>
			<td><b>NEED DATE</b></td>
			<td><b>PR AGE</b></td>
	  	</tr>';
			
		$sql="select * from tipldb..pr_po_report_table where balance_qty > 0 and substring(pr_num,1,3) in('$pr_type')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pr_num = $row->pr_num;
			$item_code = wordwrap($row->item_code, 10, "<br />\n");
			$item_desc = $row->item_desc;
			$category = $row->category;
			$project_name = $row->project_name;
			$costing = $row->costing;
			$pr_need_date = $row->pr_need_date;
			$pr_age = $row->pr_age;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/createpoc/view_pr_auth_pur/'.$pr_num.'">'.$pr_num.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.substr($item_desc,0,20).'...</td>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.substr($pr_need_date,0,11).'</td>
				<td>'.$pr_age.'&nbsp;Days</td>
		  	</tr>';
		}
		
		echo $html;
		
    }
	
	//Count Disapproved Purchase Request Purchase
	function count_dis_pr_pur(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		/*$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGPR') and a.pr_status = 'PR Disapproved At Purchase Level' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'AU'";*/
		
		$sql="select count(*) as count1 from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGP') and a.pr_status = 'PR Disapproved At Purchase Level' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'AU' 
		and a.pr_num not in(
			select distinct po_ipr_no from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
			and pomas_poamendmentno in(select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
			and pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT') and a.po_ipr_no != '' and a.po_ipr_no is not null
		)";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Count Disapproved Purchase Request Purchase
	function det_dis_pr_pur(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>PR NO</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>NEED DATE</b></td>
			<td><b>PR AGE</b></td>
	  	</tr>';
			
		/*$sql="select *,datediff(DAY,preqm_authdate,getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGPR') and a.pr_status = 'PR Disapproved At Purchase Level' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'AU'";*/
		
		$sql="select *,datediff(DAY,preqm_authdate,getdate()) as diff from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
		where SUBSTRING(a.pr_num, 1, 3) in ('IPR','LPR','FPR','CGPR') and a.pr_status = 'PR Disapproved At Purchase Level' 
		and a.pr_num = b.preqm_prno and b.preqm_status = 'AU' 
		and a.pr_num not in(
			select distinct po_ipr_no from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
			and pomas_poamendmentno in(select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
			and pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT') and a.po_ipr_no != '' and a.po_ipr_no is not null
		)";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pr_num = $row->pr_num;
			$item_code = wordwrap($row->item_code, 10, "<br />\n");
			$item_desc = $row->itm_desc;
			$category = $row->category;
			$project_name = $row->project_name;
			$pr_need_date = $row->need_date;
			$pr_age = $row->diff;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/createpoc/create_pr_disapproved/'.$pr_num.'">'.$pr_num.'</a></td>
				<td>'.$item_code.'</td>
				<td>'.substr($item_desc,0,20).'...</td>
				<td>'.$category.'</td>
				<td>'.$project_name.'</td>
				<td>'.substr($pr_need_date,0,11).'</td>
				<td>'.$pr_age.'&nbsp;Days</td>
		  	</tr>';
		}
		
		echo $html;
		
    }
	
	//Count Fresh PO
	function count_fresh_po(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from ( 
			select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
			SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV') and a.pomas_podocstatus in ('FR')
			and a.pomas_suppliercode = b.supp_spmn_supcode
			and a.pomas_pono not in(select po_num from tipldb..insert_po)
			union
			select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
			SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV') and a.pomas_podocstatus in ('FR')
			and a.pomas_suppliercode = b.supp_spmn_supcode
			and a.pomas_pono in(select po_num from tipldb..insert_po where status = 'Draft')
		) as def";
		
		$query = $ci->db->query($sql);
		
		$count_tot = 0;
		foreach ($query->result() as $row) {
			$count = $row->count1;
			$count_tot = $count_tot+$count;
		}
		
		echo $count_tot;
		
    }
	
	//Count Authorize PO Level1
	function count_auth_po_l1(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
		and b.pomas_podocstatus  in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV')
		and status in('PO Send For Level 1 Authorization')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Count Disapprove PO Level1
	function count_dis_po_l1(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from SCMDB..po_pomas_pur_order_hdr a, TIPLDB..po_master_table b where a.pomas_pono = b.po_num 
		and b.status = 'PO Disapproved At Level 1' and a.pomas_podocstatus in('FR','AM')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count Authorize PO Level2
	function count_auth_po_l2(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
		and b.pomas_podocstatus  in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') 
		and status in('PO Send For Level 2 Authorization')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Count Disapprove PO Level2
	function count_dis_po_l2(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from SCMDB..po_pomas_pur_order_hdr a, TIPLDB..po_master_table b where a.pomas_pono = b.po_num 
		and b.status = 'PO Disapproved At Level 2' and pomas_podocstatus in('FR','AM')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count Authorize PO Level3
	function count_auth_po_l3(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from 
		TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
		and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') and status in('PO Send For Level 3 Authorization')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;
		
    }
	
	//Count Disapprove PO Level3
	function count_dis_po_l3(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1
		from SCMDB..po_pomas_pur_order_hdr a, TIPLDB..po_master_table b where a.pomas_pono = b.po_num 
		and b.status = 'PO Disapproved At Level 3' and pomas_podocstatus in('FR','AM')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count Not Authorized PO
	function count_not_auth_po(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and a.status = 'Authorized PO Send to Supplier' and b.pomas_podocstatus  in('OP') 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr 
where pomas_pono = a.po_num)
and a.po_num not in('IPO-00460-17','IPO-00462-17','IPO-00463-17','IPO-00464-17')";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count PO's Under Amendment
	function count_amend_po(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from scmdb..po_pomas_pur_order_hdr a, tipldb..po_master_table b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV') and a.pomas_podocstatus in ('AM') and a.pomas_pono = b.po_num
		and a.pomas_poamendmentno > (select MAX(po_amend_no) from TIPLDB..po_master_table where po_num = a.pomas_pono)";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count PO pending for send to supplier for purchase old
	function count_po_supp_old(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and a.status = 'Authorized PO Send to Supplier' and b.pomas_podocstatus in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr 
where pomas_pono = a.po_num)";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count Lead Time Update By Purchase
	function count_lead_time_update(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
		and a.status = 'Pending For Lead Time Update' and b.pomas_podocstatus in('OP') 
		and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') 
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr 
		where pomas_pono = a.po_num)";
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Details Lead Time Update By Purchase
	function det_lead_time_update(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$html='
		<tr style="background-color:#CCC">
			<td>PO NO.</td>
			<td>SUPPLIER NAME</td>
			<td>ORDER VALUE</td>
			<td>PO AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, po_create_date, getdate()) as diff
		from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
		and a.status = 'Pending For Lead Time Update' and b.pomas_podocstatus in('OP') 
		and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') 
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr 
		where pomas_pono = a.po_num) order by po_num";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pono = $row->po_num;
			$supp_name = $row->po_supp_name;
			$po_basic_value = $row->pomas_pobasicvalue;
			$po_cst_value   = $row->pomas_tcdtotalrate; 
			$po_tax_value   = $row->pomas_tcal_total_amount;
			$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
			$po_age  = $row->diff;
			$pending_no = $row->created_by;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/lead_time_updatec/index/'.$pono.'">'.$pono.'</a></td>
				<td>'.$supp_name.'</td>
				<td>'.number_format($po_total_value,2,".","").'</td>
				<td>'.$po_age.'&nbsp;Days</td>
				<td>'.$pending_no.'</td>
		  	</tr>';
		}
		
		echo $html;	
    }
	
	//Count MC Planning
	function count_mc_planning(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b
		where a.status in('PO Send To Issuance Of Manufacturing Clearanace By Planning') 
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Det MC Planning
	function det_mc_planning(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PO NO.</td>
			<td>SUPPLIER NAME</td>
			<td>ORDER VALUE</td>
			<td>PO AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, po_create_date, getdate()) as diff from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b
		where a.status in('PO Send To Issuance Of Manufacturing Clearanace By Planning') 
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pono           = $row->po_num;
			$supp_name      = $row->po_supp_name;
			$po_basic_value = $row->pomas_pobasicvalue;
			$po_cst_value   = $row->pomas_tcdtotalrate; 
			$po_tax_value   = $row->pomas_tcal_total_amount;
			$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
			$category       = $row->po_category;
			$po_age         = $row->diff;
			
			$sql2 = "select * from tipldb..pm_approval_master_bkup
			where pm_group = (select distinct pm_group from tipldb..insert_po 
			where po_num = '$pono' and pm_group is not null and pm_group != '' and pm_group != 'No PM Group')
			and category = (select distinct ipr_category from tipldb..insert_po 
			where po_num = '$pono' and ipr_category is not null and ipr_category != '')";
			
			$query2 = $ci->db->query($sql2);
			
			if($query2->num_rows() > 0){
				foreach ($query2->result() as $row) {
					$pending_no = $row->pi_app_by;
				}
			} else {
				
				$sql3 = "select * from tipldb..pm_approval_master_bkup
				where pm_group = (select distinct pm_group from tipldb..insert_po 
				where po_num = '$pono' and pm_group is not null and pm_group != '')
				and category = (select distinct ipr_category from tipldb..insert_po 
				where po_num = '$pono' and ipr_category is not null and ipr_category != '')";
				
				$query3 = $ci->db->query($sql3);
				
				if($query3->num_rows() > 0){
					foreach ($query3->result() as $row) {
						$pending_no = $row->pi_app_by;
					}
				} else {
					$pending_no = "";
				}
				
			}
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/mc_planningc/create_po_lvl2/'.$pono.'">'.$pono.'</a></td>
				<td>'.$supp_name.'</td>
				<td>'.number_format($po_total_value,2,".","").'</td>
				<td>'.$po_age.'&nbsp;Days</td>
				<td>'.$pending_no.'</td>
		  	</tr>';
		}
		
		echo $html;
    }
	
	//Count MC Purchase
	function count_mc_purchase(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b
		where a.status = 'Manufacturing Clearnace Given By Planning'
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Det MC Purchase
	function det_mc_purchase(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PO NO.</td>
			<td>SUPPLIER NAME</td>
			<td>ORDER VALUE</td>
			<td>PO AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, po_create_date, getdate()) as diff from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b
		where a.status = 'Manufacturing Clearnace Given By Planning'
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pono = $row->po_num;
			$supp_name = $row->po_supp_name;
			$po_basic_value = $row->pomas_pobasicvalue;
			$po_cst_value   = $row->pomas_tcdtotalrate; 
			$po_tax_value   = $row->pomas_tcal_total_amount;
			$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
			$po_age  = $row->diff;
			$pending_no = $row->created_by;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/manufact_clerancec/create_po_lvl2/'.$pono.'">'.$pono.'</a></td>
				<td>'.$supp_name.'</td>
				<td>'.number_format($po_total_value,2,".","").'</td>
				<td>'.$po_age.'&nbsp;Days</td>
				<td>'.$pending_no.'</td>
		  	</tr>';
		}
		
		echo $html;	
    }
	
	//Count Acknowledgement Supplier
	function count_ack_supp(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b
		where a.status in('Manufacturing Clearance Given By Purchase', 'Pending For Acknowledgement From Supplier') 
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count TC Attach
	function count_tc_attch(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status in('Acknowledgement Updated')
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		and a.po_num in(select distinct po_num from tipldb..insert_po a, tipldb..pr_submit_table b where a.po_ipr_no = b.pr_num and b.test_cert = 'Yes')";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		return $count;	
    }
	
	//Count DI Planning
	function count_di_planning(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status in('PO Send To Issuance Of DI From Planning','Test Certificate Uploaded')
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		and a.po_num in(
			select distinct po_num from tipldb..insert_po a, tipldb..pr_submit_table b 
			where a.po_ipr_no = b.pr_num and b.dispatch_inst = 'Yes'
		)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details DI Planning
	function det_di_planning(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PO NO.</td>
			<td>SUPPLIER NAME</td>
			<td>ORDER VALUE</td>
			<td>PO AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, po_create_date, getdate()) as diff from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status in('PO Send To Issuance Of DI From Planning','Test Certificate Uploaded')
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		and a.po_num in(
			select distinct po_num from tipldb..insert_po a, tipldb..pr_submit_table b 
			where a.po_ipr_no = b.pr_num and b.dispatch_inst = 'Yes'
		)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pono           = $row->po_num;
			$supp_name      = $row->po_supp_name;
			$po_basic_value = $row->pomas_pobasicvalue;
			$po_cst_value   = $row->pomas_tcdtotalrate; 
			$po_tax_value   = $row->pomas_tcal_total_amount;
			$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
			$po_age         = $row->diff;
			$category       = $row->po_category;
			
			$sql2 = "select * from tipldb..pm_approval_master_bkup
			where pm_group = (select distinct pm_group from tipldb..insert_po 
			where po_num = '$pono' and pm_group is not null and pm_group != '' and pm_group != 'No PM Group')
			and category = (select distinct ipr_category from tipldb..insert_po 
			where po_num = '$pono' and ipr_category is not null and ipr_category != '')";
			
			$query2 = $ci->db->query($sql2);
			
			if($query2->num_rows() > 0){
				foreach ($query2->result() as $row) {
					$pending_no = $row->pi_app_by;
				}
			} else {
				$sql3 = "select * from tipldb..pm_approval_master_bkup
				where pm_group = (select distinct pm_group from tipldb..insert_po 
				where po_num = '$pono' and pm_group is not null and pm_group != '')
				and category = (select distinct ipr_category from tipldb..insert_po 
				where po_num = '$pono' and ipr_category is not null and ipr_category != '')";
				
				$query3 = $ci->db->query($sql3);
				
				if($query3->num_rows() > 0){
					foreach ($query3->result() as $row) {
						$pending_no = $row->pi_app_by;
					}
				} else {
					$pending_no = "";
				}
			}
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/di_planningc/index/'.$pono.'">'.$pono.'</a></td>
				<td>'.$supp_name.'</td>
				<td>'.$po_total_value.'</td>
				<td>'.$po_age.'&nbsp;Days</td>
				<td>'.$pending_no.'</td>
		  	</tr>';
		}
		
		echo $html;	
    }
	
	//Count DI Purchase
	function count_di_purchase(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status in('Dispatch Instruction Given by Planning')
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		and a.po_num in(
			select distinct po_num from tipldb..insert_po a, tipldb..pr_submit_table b 
			where a.po_ipr_no = b.pr_num and b.dispatch_inst = 'Yes'
		)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details DI Purchase
	function det_di_purchase(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PO NO.</td>
			<td>SUPPLIER NAME</td>
			<td>ORDER VALUE</td>
			<td>PO AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select *, datediff(DAY, po_create_date, getdate()) as diff from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status in('Dispatch Instruction Given by Planning')
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		and a.po_num in(
			select distinct po_num from tipldb..insert_po a, tipldb..pr_submit_table b 
			where a.po_ipr_no = b.pr_num and b.dispatch_inst = 'Yes'
		)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pono = $row->po_num;
			$supp_name = $row->po_supp_name;
			$po_basic_value = $row->pomas_pobasicvalue;
			$po_cst_value   = $row->pomas_tcdtotalrate; 
			$po_tax_value   = $row->pomas_tcal_total_amount;
			$po_total_value = ($po_basic_value + $po_cst_value + $po_tax_value);
			$po_age  = $row->diff;
			$pending_no = $row->created_by;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/di_purchasec/index/'.$pono.'">'.$pono.'</a></td>
				<td>'.$supp_name.'</td>
				<td>'.$po_total_value.'</td>
				<td>'.$po_age.'&nbsp;Days</td>
				<td>'.$pending_no.'</td>
		  	</tr>';
		}
		
		echo $html;	
    }
	
	//Count dispatch
	function count_dispatch(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where status in('PO Awaited For Road Permit') 
		and a.road_permit_req = 'Yes'
		and a.po_num = b.pomas_pono
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Count Delivery Details
	function count_delivery_det(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b  
		where a.status in('Dispatch Instruction Updated')
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Count Gate Entry
	function count_gate_entry(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status = 'Submit Delivery'
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Count HSN & Quantity Check
	function count_hsn_check(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) AS count1 from
		TIPLDB..po_master_table a, TIPLDB..gateentry_item_rec_master b
		where a.po_num = b.po_num
		and b.status in('Pending For HSN Check By Purchaser')
		and a.po_num in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details HSN & Quantity Check
	function det_hsn_check(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td>PO NO.</td>
			<td>ENTRY NO</td>
			<td>SUPPLIER NAME</td>
			<td>ORDER VALUE</td>
			<td>PO AGE</td>
			<td>PENDING ON</td>
	  	</tr>';
			
		$sql="select a.po_num, b.entry_no, a.po_supp_name, po_total_value, datediff(DAY, a.po_create_date, getdate()) as diff, a.created_by 
		from TIPLDB..po_master_table a, TIPLDB..gateentry_item_rec_master b
		where a.po_num = b.po_num
		and b.status in('Pending For HSN Check By Purchaser')
		and a.po_num in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		order by a.po_num asc";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$pono = $row->po_num;
			$entry_no = $row->entry_no;
			$supp_name = $row->po_supp_name;
			$po_total_value = $row->po_total_value;
			$po_age  = $row->diff;
			$pending_no = $row->created_by;
			
			$html.='
			<tr>
				<td><a href="'.base_url().'index.php/hsn_checkc/index/'.$pono.'/'.$entry_no.'">'.$pono.'</a></td>
				<td>'.$entry_no.'</td>
				<td>'.$supp_name.'</td>
				<td>'.$po_total_value.'</td>
				<td>'.$po_age.'&nbsp;Days</td>
				<td>'.$pending_no.'</td>
		  	</tr>';
		}
		
		echo $html;	
    }
	
	//Count without MC & DI pending for approval form RA
	function count_mcdi_ra(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For MC & DI Approval From RA'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details without MC & DI pending for approval form RA
	function det_mcdi_ra(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>STATUS</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
						
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For MC & DI Approval From RA'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$gr_hdr_grno = $row->gr_hdr_grno;
			$gr_hdr_grdate = $row->gr_hdr_grdate;
			$diff = $row->diff;
			$created_by = $row->created_by;
			$pending_on = "sandeep.tak";
			
			/*$sql2="select emp_suprv from tipldb..employee where emp_email like'%$created_by%'";
			$query2 = $ci->db->query($sql2);
			
			if($query2->num_rows() > 0){
				foreach ($query2->result() as $row){
					$emp_suprv = $row->emp_suprv;
					
					$sql3="select emp_email from tipldb..employee where emp_name like'%$emp_suprv%'";
					$query3 = $ci->db->query($sql3);
					
					if($query3->num_rows() > 0){
						foreach ($query3->result() as $row){
							$emp_email = $row->emp_email;
							$pending_on = explode("@",$emp_email);
						}
					}
				}
			}*/
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_ra/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$status.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;	
    }
	
	//Count Disapproved At RA
	function count_dis_ra(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Reporting Authority Level'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details Disapproved form RA
	function det_dis_ra(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
						
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Reporting Authority Level'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$diff = $row->diff;
			$pending_on = $row->created_by;
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_disapproved/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;	
    }
	
	//Count without MC & DI pending for approval form Project Incharge
	function count_mcdi_pi(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For Approval From Project Incharge'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details without MC & DI pending for approval form Project Incharge
	function det_mcdi_pi(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
		
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For Approval From Project Incharge'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$diff = $row->diff;
			//$pending_on = $row->created_by;
			
			$sql2 = "select * from tipldb..pm_approval_master_bkup
			where pm_group = (select distinct pm_group from tipldb..insert_po 
			where po_num = '$po_num' and pm_group is not null and pm_group != '')
			and category = (select distinct ipr_category from tipldb..insert_po 
			where po_num = '$po_num' and ipr_category is not null and ipr_category != '')";
			
			//$sql2 = "select * from tipldb..pm_approval_master where category = '$po_category'";
			
			//echo $sql2;
			
			$query2 = $ci->db->query($sql2);
			
			if($query2->num_rows() > 0){
				foreach ($query2->result() as $row) {
					//$pending_on = $row->pi_app_by;
					$pending_on = $row->pm_app_by;
				}
			} else {
				$pending_on = "";
			}
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_pi/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;	
    }
	
	//Count Disapproved form Project Incharge
	function count_dis_pi(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Project Incharge Level'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details Disapproved form Project Incharge
	function det_dis_pi(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
		
		$sql="select a.entry_no, a.entry_date, b.po_num, b.po_category, a.supp_nm, b.status, 
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Project Incharge Level'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;			
			$diff = $row->diff;
			$pending_on = $row->created_by;
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_disapproved/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;	
    }
	
	//Count without MC & DI pending for approval form Cash Flow Incharge L1
	function count_mcdi_cf1(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For Approval From Cash Flow Incharge LVL1'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details without MC & DI pending for approval form Cash Flow Incharge L1
	function det_mcdi_cf1(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
		
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For Approval From Cash Flow Incharge LVL1'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$diff = $row->diff;
			
			$sql2="select cf1_app_by from tipldb..pm_approval_master where category = '$po_category'";
			$query2 = $ci->db->query($sql2);
			
			if($query2->num_rows() > 0){
				foreach ($query2->result() as $row){
					$pending_on = $row->cf1_app_by;
				}
			}
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_cf1/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;		
    }
	
	//Count Disapproved form Cash Flow Incharge L1
	function count_dis_cf1(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Cash Flow Incharge LVL1'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details Disapproved form Cash Flow Incharge L1
	function det_dis_cf1(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
		
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Cash Flow Incharge LVL1'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$pending_on = $row->created_by;
			$diff = $row->diff;
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_disapproved/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;	
    }
	
	//Count without MC & DI pending for approval form Cash Flow Incharge L2
	function count_mcdi_cf2(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For Approval From Cash Flow Incharge LVL2'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details without MC & DI pending for approval form Cash Flow Incharge L2
	function det_mcdi_cf2(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
		
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'Pending For Approval From Cash Flow Incharge LVL2'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$diff = $row->diff;
			
			$sql2="select cf2_app_by from tipldb..pm_approval_master where category = '$po_category'";
			$query2 = $ci->db->query($sql2);
			
			if($query2->num_rows() > 0){
				foreach ($query2->result() as $row){
					$pending_on = $row->cf2_app_by;
				}
			}
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_cf2/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;		
    }
	
	//Count Disapproved form Cash Flow Incharge L2
	function count_dis_cf2(){
		
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select count(*) as count1
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Cash Flow Incharge LVL2'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Details Disapproved form Cash Flow Incharge L2
	function det_dis_cf2(){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#CCC">
			<td><b>ENTRY NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO CATEGORY</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>PO AGE (IN DAYS)</b></td>
		</tr>';
		
		$sql="select a.entry_no, a.entry_date, a.po_num, a.po_category, a.supp_nm, a.status,
		datediff(DAY, b.po_create_date, getdate()) as diff, b.created_by 
		from tipldb..gateentry_item_rec_master a, tipldb..po_master_table b, scmdb..po_pomas_pur_order_hdr c 
		where a.po_num = b.po_num and b.po_num = c.pomas_pono 
		and c.pomas_podocstatus not in('DE','DF','SC','CL')
		and a.status = 'PO Disapproved At Cash Flow Incharge LVL2'
		and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.po_num)";
				
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
			$entry_no = $row->entry_no;
			$entry_date = $row->entry_date;
			$po_num = $row->po_num;
			$po_category = $row->po_category;
			$supp_nm = $row->supp_nm;
			$status = $row->status;
			$pending_on = $row->created_by;
			$diff = $row->diff;
			
			$html.='
			<tr>
				<td>'.$entry_no.'</td>
				<td><a href="'.base_url().'index.php/mcdi_approvalc/mcdi_disapproved/'.$po_num.'/'.$entry_no.'">'.$po_num.'</a></td>
				<td>'.$po_category.'</td>
				<td>'.$supp_nm.'</td>
				<td>'.$pending_on.'</td>
				<td>'.$diff.'</td>
			</tr>';	
		
		}
		
		echo $html;		
    }
			
}