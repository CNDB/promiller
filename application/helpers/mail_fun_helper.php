<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('mail_fun')){	

	function mail_id_active()
    {
		$ci =& get_instance();
		$ci->load->database();
		
				
		$count_query1="select a.mail_id,a.pwd from tipldb..mail_master_tbl a,tipldb..mail_master_tbl_final b
		where convert(time,getdate()) between b.from_time and b.to_time and b.active_email = a.id";
		
		$row_count = $ci->db->query($count_query1);
		
		foreach ($row_count->result() as $row) {
		  	$mail_id = $row->mail_id;
			$pwd = $row->pwd;
		}
		
		$config = Array(
			 'protocol' => 'smtp',
			 'smtp_host' => 'smtp.gmail.com',
			 'smtp_port' => 587,
			 'smtp_user' => $mail_id, // change it to yours
			 'smtp_pass' => $pwd, // change it to yours
			 'mailtype' => 'html',
			 'charset' => 'UTF-8',
			 'wordwrap' => TRUE
		 );
		
		return $config;
    }
	
}