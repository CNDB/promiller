<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('mrp')){	
	function category_count($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){
				
			$count_query1="select count(*) as count1 from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns";
		} else {
			
			$count_query1="select count(*) as count1 from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, 
			scmdb..itm_ibu_itemvarhdr b, tipldb..erp_live_category c
			where a.ItemCode = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and c.live_category in('$category')
			and c.erp_cat_code in('$erp_cat')";
		}
		
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	//Total Project Items
	function category_count_project($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){
				
			$count_query1="select count(*) as count1 from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, 
			scmdb..itm_iou_itemvarhdr b
			where a.ItemCode = b.iou_itemcode
			and (b.iou_reorderlevel <= 0 or b.iou_reorderlevel is NULL)";
		} else {
			
			$count_query1="select count(*) as count1 from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, 
			scmdb..itm_ibu_itemvarhdr b, tipldb..erp_live_category c, scmdb..itm_iou_itemvarhdr d
			where a.ItemCode = b.ibu_itemcode 
			and b.ibu_category = c.erp_cat_code
			and b.ibu_itemcode = d.iou_itemcode  
			and c.live_category in('$category')
			and c.erp_cat_code in('$erp_cat')
			and (d.iou_reorderlevel <= 0 or d.iou_reorderlevel is NULL)";	
		}
		
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	//Reorder Items
	function category_count_reorder($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){
				
			$count_query1="select count(*) as count1 from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, 
			scmdb..itm_iou_itemvarhdr b
			where a.ItemCode = b.iou_itemcode
			and b.iou_reorderlevel > 0";
		} else {
			
			$count_query1="select count(*) as count1 from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns a, 
			scmdb..itm_ibu_itemvarhdr b, tipldb..erp_live_category c, scmdb..itm_iou_itemvarhdr d
			where a.ItemCode = b.ibu_itemcode 
			and b.ibu_category = c.erp_cat_code
			and b.ibu_itemcode = d.iou_itemcode  
			and c.live_category in('$category')
			and c.erp_cat_code in('$erp_cat')
			and d.iou_reorderlevel > 0";
		}
		
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	//Age Greater than One Day
	function category_age_greater($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){
				
			$count_query1="select COUNT(*) as count1 from TIPLDB..mrp_report_history where last_visible_date is NULL 
			and DATEDIFF(day,create_date, getdate()) > 1";
		} else {
			
			$count_query1="select count(*) as count1 from SCMDB..tipl_ReordercontrolNewtest_live_tbl_cns a, 
	SCMDB..itm_ibu_itemvarhdr b, TIPLDB..erp_live_category c, TIPLDB..mrp_report_history d
	where a.ItemCode = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and a.ItemCode = d.item_code 
	and d.last_visible_date is null 
	and c.live_category in('$category') and DATEDIFF(day,d.create_date, getdate()) > 1
	and c.erp_cat_code in('$erp_cat')";	
		}
		
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	//Age Less Than Equal To One Day
	function category_age_less($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){	
		
			$count_query1="select COUNT(*) as count1 from TIPLDB..mrp_report_history where last_visible_date is NULL 
and DATEDIFF(day,create_date, getdate()) <= 1";
		} else {
			
			$count_query1="select count(*) as count1 from SCMDB..tipl_ReordercontrolNewtest_live_tbl_cns a, 
	SCMDB..itm_ibu_itemvarhdr b, TIPLDB..erp_live_category c, TIPLDB..mrp_report_history d
	where a.ItemCode = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and a.ItemCode = d.item_code 
	and d.last_visible_date is null and c.live_category in('$category') and DATEDIFF(day,d.create_date, getdate()) <= 1
	and c.erp_cat_code in('$erp_cat')";	
		}
		
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	//Last 3 months and current month MRP to pr Conversion AVERAGE AGE (IN DAYS) Category Wise
	function avg_monthwise($erp_cat,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select DATEDIFF(day,a.create_date, a.last_visible_date) as days,* from tipldb..mrp_report_history a, SCMDB..itm_ibu_itemvarhdr b,
		TIPLDB..erp_live_category c 
		where a.last_visible_date is not NULL
		and a.item_code = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and c.live_category in('$category')
		and convert(date,create_date) between '$from_date' and '$to_date'
		and c.erp_cat_code in('$erp_cat') order by DATEDIFF(day,a.create_date, a.last_visible_date) desc";

		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
		}
		
		$avg = $days_sum/$counter;
		echo number_format($avg,2);
		
    }
	
	function avg_monthwise_det($erp_cat,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select DATEDIFF(day,a.create_date, a.last_visible_date) as days,* from tipldb..mrp_report_history a, SCMDB..itm_ibu_itemvarhdr b,
		TIPLDB..erp_live_category c 
		where a.last_visible_date is not NULL
		and a.item_code = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and c.live_category in('$category')
		and convert(date,create_date) between '$from_date' and '$to_date'
		and c.erp_cat_code in('$erp_cat') order by DATEDIFF(day,a.create_date, a.last_visible_date) desc";
	
		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
			$item_code = $row->item_code;
			$item_code1 = urlencode($item_code); 
			if(strpos($item_code1, '%2F') !== false){ 
			  $item_code2 = str_replace("%2F","chandra",$item_code1);
			} else {
			  $item_code2 = $item_code1;
			}
			$item_desc = $row->item_desc; 
			$ibu_category = $row->ibu_category;
			$live_category = $row->live_category;
			$create_date = $row->create_date;
			$last_visible_date = $row->last_visible_date;
			
			$html .= '
			<tr>
				<td>'.$counter.'</td>
				<td><a href='.base_url().'index.php/createpoc/pendal_view/'.$item_code2.' target="_blank">'.$item_code.'</td>
				<td><p>'.$item_desc.'</p></td>
				<td>'.$ibu_category.'</td>
				<td>'.$live_category.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td>'.substr($last_visible_date,0,11).'</td>
				<td>'.$days.'</td>
			</tr>
			';
		}
		
		$avg = $days_sum/$counter;
		
		$html.= '
		<tr>
			<td colspan="7"><b>AVERAGE AGE (IN DAYS)</b></td>
			<td>'.number_format($avg,2).'</td>
		</tr>
		';
		echo $html;
    }
	
	//Average Of Open Items
	
	function avg_open_items($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
			
		$sql="select DATEDIFF(day,a.create_date, getdate()) as days,* from tipldb..mrp_report_history a, SCMDB..itm_ibu_itemvarhdr b,
		TIPLDB..erp_live_category c where a.last_visible_date is NULL
		and a.item_code = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and c.live_category in('$category')
		and c.erp_cat_code in('$erp_cat') order by DATEDIFF(day,a.create_date, getdate()) desc";

		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
		}
		
		$avg = $days_sum/$counter;
		echo number_format($avg,2);
    }
	
	function avg_open_items_det($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		$sql="select DATEDIFF(day,a.create_date, getdate()) as days,* from tipldb..mrp_report_history a, SCMDB..itm_ibu_itemvarhdr b,
		TIPLDB..erp_live_category c where a.last_visible_date is NULL
		and a.item_code = b.ibu_itemcode and b.ibu_category = c.erp_cat_code and c.live_category in('$category')
		and c.erp_cat_code in('$erp_cat') order by DATEDIFF(day,a.create_date, getdate()) desc";
	
		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
			$item_code = $row->item_code;
			$item_code1 = urlencode($item_code); 
			if(strpos($item_code1, '%2F') !== false){ 
			  $item_code2 = str_replace("%2F","chandra",$item_code1);
			} else {
			  $item_code2 = $item_code1;
			}
			$item_desc = $row->item_desc; 
			$ibu_category = $row->ibu_category;
			$live_category = $row->live_category;
			$create_date = $row->create_date;
			
			//Getting Reorder Status
			$sql_reorder_stat = "select status from tipldb..reorder_update_entry  where item_code = '$item_code'";
			$qry_reorder_stat = $ci->db->query($sql_reorder_stat);
			
			if($qry_reorder_stat->num_rows() > 0){
				foreach($qry_reorder_stat->result() as $row){
					$status = $row->status;
				}
			} else {
				$status = "Project Item";
			}
			
			$html .= '
			<tr>
				<td>'.$counter.'</td>
				<td><a href='.base_url().'index.php/createpoc/pendal_view/'.$item_code2.' target="_blank">'.$item_code.'</td>
				<td><p>'.$item_desc.'</p></td>
				<td>'.$ibu_category.'</td>
				<td>'.$live_category.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td></td>
				<td>'.$days.'</td>
				<td>'.$status.'</td>
			</tr>
			';
		}
		$avg = $days_sum/$counter;
		
		$html.= '
		<tr>
			<td colspan="7" style="background-color:#CCC"><b>AVERAGE AGE (IN DAYS)</b></td>
			<td>'.number_format($avg,2).'</td>
			<td style="background-color:#CCC"></td>
		</tr>
		';
		echo $html;
    }
	
	//Converting Month Into Alphbets
	function month_year($date){
		//GETTING MONTH
		$month = substr($date,5,2);
		$year  = substr($date,2,2);
		
		if($month == '01'){
			$month_word = "JAN";
		} else if($month == '02'){
			$month_word = "FEB";
		} else if($month == '03'){
			$month_word = "MAR";
		} else if($month == '04'){
			$month_word = "APR";
		} else if($month == '05'){
			$month_word = "MAY";
		} else if($month == '06'){
			$month_word = "JUNE";
		} else if($month == '07'){
			$month_word = "JULY";
		} else if($month == '08'){
			$month_word = "AUG";
		} else if($month == '09'){
			$month_word = "SEP";
		} else if($month == '10'){
			$month_word = "OCT";
		} else if($month == '11'){
			$month_word = "NOV";
		} else if($month == '12'){
			$month_word = "DEC";
		}
		$month_year = $month_word."-".$year;
		echo $month_year;
	}
	
	function category_owner($category){
		$ci =& get_instance();
		$ci->load->database();
		
		$sql="select * from tipldb..mrp_category_owner_master where category='$category'";
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$category_owner = $row->category_owner;
		}
		
		echo $category_owner;
	}	
	
	function erp_category(){
		$ci =& get_instance();
		$ci->load->database();
		
		$sql="select * from tipldb..mrp_category_owner_master_bkup order by erp_cat";
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$erp_cat = $row->erp_cat;
			
			$html.="<option value=".$erp_cat.">".$erp_cat."</option>";
		}
		
		echo $html;
	}
	
	//PICS & Length Mismatched Items Whose Stock Is Available
	function cp_pics_na($erp_cat,$category,$cp_stat){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){	
			$count_query1="select count(*) as count1 from ( 
				select distinct ItemCode from TIPLDB..pendingissuetbl a, SCMDB..itm_ibu_itemvarhdr b, TIPLDB..erp_live_category c
				where a.ItemCode = b.ibu_itemcode 
				and b.ibu_category = c.erp_cat_code 
				and a.cp_status = '$cp_stat'
			)x";
		} else {
			$count_query1="select count(*) as count1 from ( 
				select distinct ItemCode from TIPLDB..pendingissuetbl a, SCMDB..itm_ibu_itemvarhdr b, TIPLDB..erp_live_category c
				where a.ItemCode = b.ibu_itemcode 
				and b.ibu_category = c.erp_cat_code 
				and a.cp_status = '$cp_stat'
				and c.live_category in('$category')
				and c.erp_cat_code in('$erp_cat')
			)x";		
		}
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;	
    }
	
	//Disapproved Purchase Request
	function disapp_pr($erp_cat,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'All'){	
		
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table c, scmdb..prq_preqm_pur_reqst_hdr d, 
			SCMDB..itm_ibu_itemvarhdr e, TIPLDB..erp_live_category f
			where c.pr_status = 'PR Disapproved At Purchase Level'
			and c.pr_num = d.preqm_prno
			and d.preqm_status  = 'AU'
			and c.item_code = e.ibu_itemcode
			and e.ibu_category = f.erp_cat_code
			and c.pr_num not in(
				select distinct poprq_prno from scmdb..po_poprq_poprcovg_detail a, scmdb..po_pomas_pur_order_hdr b 
				where a.poprq_poamendmentno = b.pomas_poamendmentno
				and a.poprq_pono = b.pomas_pono
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and b.pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT')
				and poprq_pocovqty = c.required_qty
			)";
		} else {
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table c, scmdb..prq_preqm_pur_reqst_hdr d, 
			SCMDB..itm_ibu_itemvarhdr e, TIPLDB..erp_live_category f
			where c.pr_status = 'PR Disapproved At Purchase Level'
			and c.pr_num = d.preqm_prno
			and d.preqm_status  = 'AU'
			and c.item_code = e.ibu_itemcode
			and e.ibu_category = f.erp_cat_code
			and f.erp_cat_code in('$erp_cat')
			and f.live_category in('$category')
			and c.pr_num not in(
				select distinct poprq_prno from scmdb..po_poprq_poprcovg_detail a, scmdb..po_pomas_pur_order_hdr b 
				where a.poprq_poamendmentno = b.pomas_poamendmentno
				and a.poprq_pono = b.pomas_pono
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and b.pomas_podocstatus in('FR','DF','OP','AM','CL','NT','RT')
				and poprq_pocovqty = c.required_qty
			)";		
		}
		
		$row_count = $ci->db->query($count_query1);
			
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;	
    }
	
}