<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('not_live_po'))
{
    
	// PR Not Created In Live
	
	$date = '2017-07-01';
	
	function prnotcreatedlive($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$sql = "select count(*) as count1 from scmdb..prq_preqm_pur_reqst_hdr a where 
a.preqm_prno not in(select pr_num from tipldb..pr_submit_table) 
and convert(date,preqm_createddate) > '2017-12-01'
and preqm_status = 'AU' 
and substring(preqm_prno,1,3) != 'RPR'
and substring(preqm_prno,1,3) in($pr_type)";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	function prnotcreatedlive_view($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$sql = "select datediff(DAY, preqm_prdate, getdate()) as diff1,* from scmdb..prq_preqm_pur_reqst_hdr a, 
scmdb..prq_prqit_item_detail b, scmdb..itm_loi_itemhdr c where 
a.preqm_prno not in(select pr_num from tipldb..pr_submit_table) 
and convert(date,a.preqm_createddate) > '2017-12-01'
and a.preqm_prno = b.prqit_prno
and b.prqit_itemcode = c.loi_itemcode
and a.preqm_status = 'AU' 
and substring(a.preqm_prno,1,3) != 'RPR'
and substring(a.preqm_prno,1,3) in($pr_type)";

		$query = $ci->db->query($sql);
		$sno = 0;
		foreach ($query->result() as $row) {
		 	$sno++;
			$pr_num = $row->preqm_prno;
			$item_code = $row->prqit_itemcode;
			$item_desc = $row->loi_itemdesc;
			$uom = $row->prqit_puom;
			$qty = number_format($row->prqit_authqty,2);
			$pr_date = substr($row->preqm_prdate,0,11);
			$pr_need_date = substr($row->prqit_needdate,0,11);
			$age = $row->diff1; 
			
			$html .= "   
                <tr>
                	<td>$sno</td>
                    <td>$pr_num</td>
                    <td>$item_code</td>
                    <td>$item_desc</td>
                    <td></td>
                    <td></td>
                    <td>$qty</td>
                    <td>$uom</td>
                    <td></td>
                    <td>$pr_date</td>
                    <td>$pr_need_date</td>
                    <td>$age</td>
                    <td></td>
                    <td></td>
                </tr>
			";
		}
		
		echo $html;
    }
	//PR Created In Live But Not Authorized In Live
	
	function prnotauthlive($pr_type,$category){
		
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select count(*) as count1 from scmdb..prq_preqm_pur_reqst_hdr a where 
		a.preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status not in('Authorized','PR Disapproved At Purchase Level','Pending For PR Authorize In ERP') and category in('$category')) 
		and convert(date,preqm_createddate) > '2017-12-01'
		and preqm_status = 'AU' 
		and substring(preqm_prno,1,3) != 'RPR'
		and substring(preqm_prno,1,3) in($pr_type)";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	function prnotauthlive_view($pr_type,$category){
		
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select datediff(DAY, preqm_prdate, getdate()) as diff1,* from scmdb..prq_preqm_pur_reqst_hdr a, scmdb..prq_prqit_item_detail b, scmdb..itm_loi_itemhdr c, tipldb..pr_submit_table d where 
a.preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status not in('Authorized','PR Disapproved At Purchase Level','Pending For PR Authorize In ERP') and category in('$category'))
and a.preqm_prno = b.prqit_prno
and b.prqit_itemcode = c.loi_itemcode
and a.preqm_prno =  d.pr_num
and convert(date,a.preqm_createddate) > '2017-12-01'
and a.preqm_status = 'AU' 
and substring(a.preqm_prno,1,3) != 'RPR'
and substring(a.preqm_prno,1,3) in($pr_type)";

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
		 	$sno++;
			$pr_num = $row->preqm_prno;
			$item_code = $row->prqit_itemcode;
			$item_desc = $row->loi_itemdesc;
			$category = $row->category;
			$project_name = $row->project_name;
			$uom = $row->prqit_puom;
			$costing = $row->costing;
			$qty = number_format($row->prqit_authqty,2);
			$pr_date = substr($row->preqm_prdate,0,11);
			$pr_need_date = substr($row->prqit_needdate,0,11);
			$age = $row->diff1;
			$live_created_by = $row->created_by;
			$live_status = $row->pr_status;
			$html .= "   
                <tr>
                	<td>$sno</td>
                    <td>$pr_num</td>
                    <td>$item_code</td>
                    <td>$item_desc</td>
                    <td>$category</td>
                    <td>$project_name</td>
                    <td>$qty</td>
                    <td>$uom</td>
                    <td>$costing</td>
                    <td>$pr_date</td>
                    <td>$pr_need_date</td>
                    <td>$age</td>
                    <td>$live_created_by</td>
                    <td>$live_status</td>
                </tr>
			";
		}
		
		echo $html;
    }
	
	//PO Not Created In Live and ERP Open, Amended, Closed, Short Closed 
	
	function ponotcreatedlive($po_type){
		
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select count(*) as count1 from scmdb..po_pomas_pur_order_hdr a where 
a.pomas_pono not in(select po_num from TIPLDB..po_master_table)
and a.pomas_createddate > '2017-12-01'
and a.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono)
and a.pomas_podocstatus in('OP','SC','CL','AM')
and SUBSTRING(pomas_pono,1,3) in($po_type)";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	function ponotcreatedlive_view($po_type){
		
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select datediff(DAY, pomas_podate, getdate()) as diff1,* from scmdb..po_pomas_pur_order_hdr a where 
a.pomas_pono not in(select po_num from TIPLDB..po_master_table)
and a.pomas_createddate > '2017-12-01'
and a.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono)
and a.pomas_podocstatus in('OP','SC','CL','AM')
and SUBSTRING(pomas_pono,1,3) in($po_type) order by a.pomas_pono asc";

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
		 	$sno++;
			$po_num = $row->pomas_pono;
		    $po_date = substr($row->pomas_podate,0,11);
		    $po_value = number_format($row->pomas_pobasicvalue,2);
		    $created_by = $row->pomas_createdby;
		    $po_age = $row->diff1;
			$po_erp_status = $row->pomas_podocstatus;
			$html .= "   
                <tr>
                	<td>$sno</td>
                    <td>$po_num</td>
                    <td></td>
                    <td></td>
                    <td>$po_date</td>
                    <td>$po_value</td>
					<td></td>
					<td>$po_erp_status</td>
                    <td>$po_age</td>
					<td>$created_by</td>
                </tr>
			";
		}
		
		echo $html;
		
		echo $count;
    }
	
	//PO Created In Live and But Not Authorized In Live 
	
	function ponotauthlive($po_type,$category){
		
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select count(*) as count1 from scmdb..po_pomas_pur_order_hdr a where 
a.pomas_pono in(select po_num from TIPLDB..po_master_table where 
status in('PO Disapproved At Level 1','PO Disapproved At Level 2','PO Send For Level 1 Authorization','PO Send For Level 2 Authorization') 
AND po_category in('$category'))
and a.pomas_createddate > '2017-12-01'
and a.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono)
and a.pomas_podocstatus in('OP','SC','CL')
and SUBSTRING(pomas_pono,1,3) in($po_type)";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo $count;
    }
	
	function ponotauthlive_view($po_type,$category){
		
		$ci =& get_instance();
		$ci->load->database();

		$sql = "select datediff(DAY, pomas_podate, getdate()) as diff1,* from scmdb..po_pomas_pur_order_hdr a,  TIPLDB..po_master_table b where
a.pomas_pono = b.po_num
and a.pomas_pono in(select po_num from TIPLDB..po_master_table where 
status in('PO Disapproved At Level 1','PO Disapproved At Level 2','PO Send For Level 1 Authorization','PO Send For Level 2 Authorization') 
AND po_category in('$category'))
and a.pomas_createddate > '2017-12-01'
and a.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono)
and a.pomas_podocstatus in('OP','SC','CL')
and SUBSTRING(a.pomas_pono,1,3) in($po_type) order by a.pomas_pono asc";

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
		 	$sno++;
			$po_num = $row->pomas_pono;
			$supp_name = $row->po_supp_name;
			$po_category =  $row->po_category;
		    $po_date = substr($row->pomas_podate,0,11);
		    $po_value = number_format($row->pomas_pobasicvalue,2);
			$live_status = $row->status;
		    $po_age = $row->diff1;
			$created_by = $row->pomas_createdby;
			$po_erp_status = $row->pomas_podocstatus;
			$html .= "   
                <tr>
                	<td>$sno</td>
                    <td>$po_num</td>
                    <td>$supp_name</td>
                    <td>$po_category</td>
                    <td>$po_date</td>
                    <td>$po_value</td>
					<td>$live_status</td>
					<td>$po_erp_status</td>
                    <td>$po_age</td>
					<td>$created_by</td>
                </tr>
			";
		}
		
		echo $html;
    }
	
	
	/********************************* New Function PR Count ***************************/
	//Fresh PR 
	function erp_fresh_pr($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		//echo $pr_type;

		$sql = "select count(*) as count1 from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
		and SUBSTRING(preqm_prno, 1, 3) in('$pr_type')
		and preqm_prno not in(select pr_num from tipldb..pr_submit_table)";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pending_poc/pending_pr_list?fun=erp_fresh_pr_det&pr_type='.$pr_type.'" target="_blank">'.$count.'</a>';
		
	}
	
	//Level 1 Pending PR
	function live_pending_pr($pr_type, $category, $live_status){
		
		$ci =& get_instance();
		$ci->load->database();
		
		//echo $pr_type;

		$sql = "select count(*) as count1 from scmdb..prq_preqm_pur_reqst_hdr a, tipldb..pr_submit_table b 
where a.preqm_prno = b.pr_num 
and a.preqm_status = 'FR' 
and SUBSTRING(a.preqm_prno, 1, 3) in('$pr_type')
and b.category in('$category')
and b.pr_status in('$live_status')";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pending_poc/pending_pr_list?fun=live_pending_pr_det&pr_type='.$pr_type.'&category='.$category.'
		&live_status='.$live_status.'" target="_blank">'.$count.'</a>';
		
		/*echo $sql;*/
		
	}
	
	//PR PO Not Created
	function pr_po_not_created($pr_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$sql = "select count(*) as count1 from tipldb..pr_po_report 
		where substring(preqm_prno, 1, 3) in('$pr_type')
		and category in('$category')";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pending_poc/pending_pr_list?fun=pr_po_not_created_det&pr_type='.$pr_type.'&category='.$category.'
		" target="_blank">'.$count.'</a>';
		
	}
	
	//Total Cases
	function total_pending_pr($pr_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$sql = "select COUNT(*) as count1 from (
		select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
		a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category in('$category')
		and substring(a.pr_num, 1, 3) in('$pr_type')
		union
		select preqm_prno from tipldb..pr_po_report where category in('$category')
		and substring(preqm_prno, 1, 3) in('$pr_type')
		)x";

		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pending_poc/pending_pr_list?fun=total_pending_pr_det&pr_type='.$pr_type.'&category='.$category.'" 
		target="_blank">'.$count.'</a>';
		
	}
	
	/********************************* New Function PR Details ***************************/
	//Fresh PR 
	function erp_fresh_pr_det($pr_type){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html="	
		<tr style='background-color:#0CF;'>
			<td><b>S.NO.</b></td>
			<td><b>PR NUM</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>PR QTY</b></td>
			<td><b>UOM</b></td>
			<td><b>NEED DATE</b></td>
			<td><b>CREATED BY</b></td>
			<td><b>CREATED DATE</b></td>
			<td><b>ERP STATUS</b></td>
			<td><b>AGE</b></td>
		</tr>";
		
		$sql = "select preqm_prno,prqit_itemcode,loi_itemdesc,prqit_reqdqty,prqit_puom,prqit_needdate,preqm_createdby,
		preqm_createddate,preqm_status,datediff(DAY, preqm_createddate, getdate()) as age 
		from scmdb..prq_preqm_pur_reqst_hdr a, scmdb..prq_prqit_item_detail b, scmdb..itm_loi_itemhdr c where a.preqm_status = 'FR' 
		and a.preqm_prno = b.prqit_prno
		and b.prqit_itemcode = c.loi_itemcode
		and SUBSTRING(a.preqm_prno, 1, 3) in('$pr_type')
		and a.preqm_prno not in(select pr_num from tipldb..pr_submit_table)";

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
			$sno++;
			$pr_num = $row->preqm_prno;
			$item_code = $row->prqit_itemcode;
			$loi_itemdesc = $row->loi_itemdesc;
			$prqit_reqdqty = $row->prqit_reqdqty;
			$prqit_puom = $row->prqit_puom;
			$prqit_needdate = $row->prqit_needdate;
			$preqm_createdby = $row->preqm_createdby;
			$preqm_createddate = $row->preqm_createddate;
			$preqm_status = $row->preqm_status;
			$age = $row->age;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false){
				$item_code2 = str_replace("%2F","chandra",$item_code1);
			} else {
				$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			$html.="	
			<tr>
				<td>".$sno."</td>
				<td><a href=".base_url()."index.php/iprc/view_ipr/".$pr_num." target='_blank'>".$pr_num."</a></td>
				<td><a href=".base_url()."index.php/createpoc/pendal_view/".$item_code2." target='_blank'>".$item_code."</a></td>
				<td>".$loi_itemdesc."</td>
				<td>".number_format($prqit_reqdqty,2,".","")."</td>
				<td>".$prqit_puom."</td>
				<td>".substr($prqit_needdate,0,11)."</td>
				<td>".$preqm_createdby."</td>
				<td>".substr($preqm_createddate,0,11)."</td>
				<td>".$preqm_status."</td>
				<td>".$age."</td>
			</tr>";
		
		}
		
		echo $html;
		
	}
	
	//Live Not authorized PR
	function live_pending_pr_det($pr_type, $category, $live_status){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html="	
		<tr style='background-color:#0CF;'>
			<td><b>S.NO.</b></td>
			<td><b>PR NUM</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>PR QTY</b></td>
			<td><b>UOM</b></td>
			<td><b>COSTING</b></td>
			<td><b>COST CALCULATION REMARKS</b></td>
			<td><b>COST NOT AVAILABLE REMARKS</b></td>
			<td><b>PLANNING SPECIAL REMARKS FOR PURCHASE</b></td>
			<td><b>CREATED BY</b></td>
			<td><b>CREATED DATE</b></td>
			<td><b>NEED DATE</b></td>
			<td><b>AGE</b></td>
			<td><b>ERP STATUS</b></td>
			<td><b>LIVE STATUS</b></td>
		</tr>";

		$sql = "select preqm_prno,prqit_itemcode,itm_desc,category,project_name,
		prqit_reqdqty,prqit_puom,costing,cost_calculation_remarks,
		cost_not_available_remarks,pr_supp_remarks,preqm_createdby,preqm_createddate,prqit_needdate,
		datediff(DAY, preqm_createddate, getdate()) as age,preqm_status,pr_status 
		from scmdb..prq_preqm_pur_reqst_hdr a, 
		scmdb..prq_prqit_item_detail b, tipldb..pr_submit_table c 
		where a.preqm_prno = b.prqit_prno
		and a.preqm_prno = c.pr_num 
		and a.preqm_status in('FR') 
		and SUBSTRING(a.preqm_prno, 1, 3) in('$pr_type')
		and c.category in('$category')
		and c.pr_status in('$live_status')";

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
			$sno++;
			$pr_num = $row->preqm_prno;
			$item_code = $row->prqit_itemcode;
			$item_desc = strip_tags($row->itm_desc);
			$category = $row->category;
			$project_name = $row->project_name;
			$prqit_reqdqty = $row->prqit_reqdqty;
			$prqit_puom = $row->prqit_puom;
			$costing = $row->costing;
			$cost_calculation_remarks = $row->cost_calculation_remarks;
			$cost_not_available_remarks = $row->cost_not_available_remarks;
			$pr_supp_remarks = $row->pr_supp_remarks;
			$preqm_createdby = $row->preqm_createdby;
			$preqm_createddate = $row->preqm_createddate;
			$prqit_needdate = $row->prqit_needdate;
			$age = $row->age;
			$preqm_status = $row->preqm_status;
			$live_status = $row->pr_status;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false){
				$item_code2 = str_replace("%2F","chandra",$item_code1);
			} else {
				$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			if($preqm_status == 'DR'){
				$preqm_status = 'DRAFT';
			} else if($preqm_status == 'HD'){
				$preqm_status = 'HD';
			} else if($preqm_status == 'FR'){
				$preqm_status = 'FRESH';
			} else if($preqm_status == 'CA'){
				$preqm_status = 'CANCELLED';
			} else if($preqm_status == 'DE'){
				$preqm_status = 'DELETED';
			} else if($preqm_status == 'RT'){
				$preqm_status = 'RT';
			}
			
			$html.="	
			<tr>
				<td>".$sno."</td>
				<td><a href=".base_url()."index.php/iprc/view_ipr/".$pr_num." target='_blank'>".$pr_num."</a></td>
				<td><a href=".base_url()."index.php/createpoc/pendal_view/".$item_code2." target='_blank'>".$item_code."</a></td>
				<td>".$item_desc."</td>
				<td>".$category."</td>
				<td>".$project_name."</td>
				<td>".number_format($prqit_reqdqty,2,".","")."</td>
				<td>".$prqit_puom."</td>
				<td>".$costing."</td>
				<td>".$cost_calculation_remarks."</td>
				<td>".$cost_not_available_remarks."</td>
				<td>".$pr_supp_remarks."</td>
				<td>".$preqm_createdby."</td>
				<td>".substr($preqm_createddate,0,11)."</td>
				<td>".substr($prqit_needdate,0,11)."</td>
				<td>".$age."</td>
				<td>".$preqm_status."</td>
				<td>".$live_status."</td>
				
			</tr>";
		
		}
		
		echo $html;
		
	}
	
	//Live authorized PR Pending for po
	function pr_po_not_created_det($pr_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html="	
		<tr style='background-color:#0CF;'>
			<td><b>S.NO.</b></td>
			<td><b>PR NUM</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>PR TYPE</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>ATAC NO</b></td>
			<td><b>CUSTOMER NAME</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>PR QTY</b></td>
			<td><b>UOM</b></td>
			<td><b>COSTING</b></td>
			<td><b>COST CALCULATION REMARKS</b></td>
			<td><b>COST NOT AVAILABLE REMARKS</b></td>
			<td><b>PLANNING SPECIAL REMARKS FOR PURCHASE</b></td>
			<td><b>CREATED BY</b></td>
			<td><b>CREATED DATE</b></td>
			<td><b>AUTHORIZED DATE</b></td>
			<td><b>NEED DATE</b></td>
			<td><b>AGE</b></td>
			<td><b>ERP STATUS</b></td>
			<td><b>LIVE STATUS</b></td>
		</tr>";
		
		$sql = "select preqm_prno,prqit_itemcode,itm_desc,category,project_name,
		prqit_reqdqty,prqit_puom,costing,cost_calculation_remarks,
		cost_not_available_remarks,pr_supp_remarks,preqm_createdby,preqm_createddate,preqm_authdate,prqit_needdate,
		datediff(DAY, preqm_authdate, getdate()) as age,preqm_status,pr_status,atac_no,customer_name,usage 
		from scmdb..prq_preqm_pur_reqst_hdr a, 
		scmdb..prq_prqit_item_detail b, tipldb..pr_submit_table c 
		where a.preqm_prno = b.prqit_prno
		and a.preqm_prno = c.pr_num 
		and a.preqm_status in('AU') 
		and SUBSTRING(a.preqm_prno, 1, 3) in('$pr_type')
		and c.category in('$category')
		and c.pr_status not in('PR Disapproved At Purchase Level')
		and a.preqm_prno in(select preqm_prno from tipldb..pr_po_report)";

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
			$sno++;
			$pr_num = $row->preqm_prno;
			$item_code = $row->prqit_itemcode;
			$item_desc = strip_tags($row->itm_desc);
			$category = $row->category;
			$project_name = $row->project_name;
			$prqit_reqdqty = $row->prqit_reqdqty;
			$prqit_puom = $row->prqit_puom;
			$costing = $row->costing;
			$cost_calculation_remarks = $row->cost_calculation_remarks;
			$cost_not_available_remarks = $row->cost_not_available_remarks;
			$pr_supp_remarks = $row->pr_supp_remarks;
			$preqm_createdby = $row->preqm_createdby;
			$preqm_createddate = $row->preqm_createddate;
			$preqm_authdate = $row->preqm_authdate;
			$prqit_needdate = $row->prqit_needdate;
			$age = $row->age;
			$preqm_status = $row->preqm_status;
			$live_status = $row->pr_status;
			
			$usage = $row->usage;
			$atac_no = $row->atac_no;
			$customer_name = $row->customer_name;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false){
				$item_code2 = str_replace("%2F","chandra",$item_code1);
			} else {
				$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			if($preqm_status == 'DR'){
				$preqm_status = 'DRAFT';
			} else if($preqm_status == 'HD'){
				$preqm_status = 'HD';
			} else if($preqm_status == 'FR'){
				$preqm_status = 'FRESH';
			} else if($preqm_status == 'CA'){
				$preqm_status = 'CANCELLED';
			} else if($preqm_status == 'DE'){
				$preqm_status = 'DELETED';
			} else if($preqm_status == 'RT'){
				$preqm_status = 'RT';
			}
			
			$html.="	
			<tr>
				<td>".$sno."</td>
				<td><a href=".base_url()."index.php/iprc/view_ipr/".$pr_num." target='_blank'>".$pr_num."</a></td>
				<td><a href=".base_url()."index.php/createpoc/pendal_view/".$item_code2." target='_blank'>".$item_code."</a></td>
				<td>".$item_desc."</td>
				<td>".$usage."</td>
				<td>".$category."</td>
				<td>".$atac_no."</td>
				<td>".$customer_name."</td>
				<td>".$project_name."</td>
				<td>".number_format($prqit_reqdqty,2,".","")."</td>
				<td>".$prqit_puom."</td>
				<td>".$costing."</td>
				<td>".$cost_calculation_remarks."</td>
				<td>".$cost_not_available_remarks."</td>
				<td>".$pr_supp_remarks."</td>
				<td>".$preqm_createdby."</td>
				<td>".substr($preqm_createddate,0,11)."</td>
				<td>".substr($preqm_authdate,0,11)."</td>
				<td>".substr($prqit_needdate,0,11)."</td>
				<td>".$age."</td>
				<td>".$preqm_status."</td>
				<td>".$live_status."</td>
				
			</tr>";
		
		}
		
		echo $html;
		
	}
	
	//Total Cases
	function total_pending_pr_det($pr_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html="	
		<tr style='background-color:#0CF;'>
			<td><b>S.NO.</b></td>
			<td><b>PR NUM</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>PR TYPE</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>ATAC NO</b></td>
			<td><b>CUSTOMER NAME</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>PR QTY</b></td>
			<td><b>UOM</b></td>
			<td><b>COSTING</b></td>
			<td><b>COST CALCULATION REMARKS</b></td>
			<td><b>COST NOT AVAILABLE REMARKS</b></td>
			<td><b>PLANNING SPECIAL REMARKS FOR PURCHASE</b></td>
			<td><b>CREATED BY</b></td>
			<td><b>CREATED DATE</b></td>
			<td><b>AUTHORIZED DATE</b></td>
			<td><b>NEED DATE</b></td>
			<td><b>AGE</b></td>
			<td><b>ERP STATUS</b></td>
			<td><b>LIVE STATUS</b></td>
		</tr>";
		
		$sql = "select preqm_prno,prqit_itemcode,itm_desc,category,project_name,
		prqit_reqdqty,prqit_puom,costing,cost_calculation_remarks,
		cost_not_available_remarks,pr_supp_remarks,preqm_createdby,preqm_createddate,preqm_authdate,prqit_needdate,
		datediff(DAY, preqm_createddate, getdate()) as age,preqm_status,pr_status,atac_no,customer_name,usage 
		from scmdb..prq_preqm_pur_reqst_hdr a, 
		scmdb..prq_prqit_item_detail b, tipldb..pr_submit_table c 
		where a.preqm_prno = b.prqit_prno
		and a.preqm_prno = c.pr_num 
		and a.preqm_status in('FR') 
		and SUBSTRING(a.preqm_prno, 1, 3) in('$pr_type')
		and c.category in('$category')
		and c.pr_status in('PR Disapproved At Purchase Level','PR Disapproved At Planning Level1',
		'Pending For PR Authorize In ERP','AUTHORIZED','PR Disapproved At Planning Level',
		'PR Disapproved At Planning Level2','Pending For PR Authorize','PR Approved At Planning Level1')
		and c.level1_approval_req = 'Yes'
		UNION
		select preqm_prno,prqit_itemcode,itm_desc,category,project_name,
		prqit_reqdqty,prqit_puom,costing,cost_calculation_remarks,
		cost_not_available_remarks,pr_supp_remarks,preqm_createdby,preqm_createddate,preqm_authdate,prqit_needdate,
		datediff(DAY, preqm_authdate, getdate()) as age,preqm_status,pr_status,atac_no,customer_name,usage 
		from scmdb..prq_preqm_pur_reqst_hdr a, 
		scmdb..prq_prqit_item_detail b, tipldb..pr_submit_table c 
		where a.preqm_prno = b.prqit_prno
		and a.preqm_prno = c.pr_num 
		and a.preqm_status in('AU') 
		and SUBSTRING(a.preqm_prno, 1, 3) in('$pr_type')
		and c.category in('$category')
		and c.pr_status not in('PR Disapproved At Purchase Level')
		and a.preqm_prno in(select preqm_prno from tipldb..pr_po_report)";
		
		//echo $sql;

		$query = $ci->db->query($sql);
		
		$sno = 0;
		foreach ($query->result() as $row) {
			$sno++;
			$pr_num = $row->preqm_prno;
			$item_code = $row->prqit_itemcode;
			$item_desc = strip_tags($row->itm_desc);
			$category = $row->category;
			$project_name = $row->project_name;
			$prqit_reqdqty = $row->prqit_reqdqty;
			$prqit_puom = $row->prqit_puom;
			$costing = $row->costing;
			$cost_calculation_remarks = $row->cost_calculation_remarks;
			$cost_not_available_remarks = $row->cost_not_available_remarks;
			$pr_supp_remarks = $row->pr_supp_remarks;
			$preqm_createdby = $row->preqm_createdby;
			$preqm_createddate = $row->preqm_createddate;
			$preqm_authdate = $row->preqm_authdate;
			$prqit_needdate = $row->prqit_needdate;
			$age = $row->age;
			$preqm_status = $row->preqm_status;
			$live_status = $row->pr_status;
			
			$atac_no = $row->atac_no;
			$customer_name = $row->customer_name;
			$usage = $row->usage;
			
			/*if($live_status != 'AUTHORIZED' || $live_status != 'Pending For PR Authorize In ERP'){
				$preqm_authdate = '';
			}*/
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false){
				$item_code2 = str_replace("%2F","chandra",$item_code1);
			} else {
				$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			if($preqm_status == 'DR'){
				$preqm_status = 'DRAFT';
			} else if($preqm_status == 'HD'){
				$preqm_status = 'HD';
			} else if($preqm_status == 'FR'){
				$preqm_status = 'FRESH';
			} else if($preqm_status == 'CA'){
				$preqm_status = 'CANCELLED';
			} else if($preqm_status == 'DE'){
				$preqm_status = 'DELETED';
			} else if($preqm_status == 'RT'){
				$preqm_status = 'RT';
			}
			
			$html.="	
			<tr>
				<td>".$sno."</td>
				<td><a href=".base_url()."index.php/iprc/view_ipr/".$pr_num." target='_blank'>".$pr_num."</a></td>
				<td><a href=".base_url()."index.php/createpoc/pendal_view/".$item_code2." target='_blank'>".$item_code."</a></td>
				<td>".$item_desc."</td>
				<td>".$usage."</td>
				<td>".$category."</td>
				<td>".$atac_no."</td>
				<td>".$customer_name."</td>
				<td>".$project_name."</td>
				<td>".number_format($prqit_reqdqty,2,".","")."</td>
				<td>".$prqit_puom."</td>
				<td>".$costing."</td>
				<td>".$cost_calculation_remarks."</td>
				<td>".$cost_not_available_remarks."</td>
				<td>".$pr_supp_remarks."</td>
				<td>".$preqm_createdby."</td>
				<td>".substr($preqm_createddate,0,11)."</td>
				<td>".substr($preqm_authdate,0,11)."</td>
				<td>".substr($prqit_needdate,0,11)."</td>
				<td>".$age."</td>
				<td>".$preqm_status."</td>
				<td>".$live_status."</td>
				
			</tr>";
		
		}
		
		echo $html;
		
	}
	
	//***************//
	//Purchase Orders
	//***************//
	
	/***************** Count PO Functions *************/
	
	function pending_po($po_stat, $po_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
		
		if($category != 'ALL'){
		
			if($po_type != 'SRV'){
			
				$sql = "select count(*) as count1 from scmdb..po_pomas_pur_order_hdr z where pomas_podocstatus IN('$po_stat')
				and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
				where a.poprq_prno = b.pr_num and b.category in('$category'))
				and substring(pomas_pono,1,3) in('$po_type')
				and pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = z.pomas_pono)";
				
			} else {
				
				$sql = "select count(*) as count1 from scmdb..po_pomas_pur_order_hdr 
				where pomas_podocstatus in('$po_stat')
				and substring(pomas_pono,1,3) in('$po_type')";
				
			}
			
		} else {
			
				$category1 = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES'
				,'SERVICE','PACKING','AVAZONIC','NON PRODUCTION CONSUMABLES','IT";
				
				$sql = "select count(*) as count1 from (
				select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr z where pomas_podocstatus IN('$po_stat')
				and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
				where a.poprq_prno = b.pr_num and b.category in('$category1'))
				and substring(pomas_pono,1,3) in('$po_type')
				and pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = z.pomas_pono)
				
				UNION
				
				select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('$po_stat')
				and substring(pomas_pono,1,3) in('SRV')
				)x";
				
		}
		
		
		$query = $ci->db->query($sql);
		
		foreach ($query->result() as $row) {
		  $count = $row->count1;
		}
		
		//$url= base_url().""
		
		echo '<a href="'.base_url().'index.php/pending_poc/pending_po_list?fun=pending_po_det&po_stat='.$po_stat.'&po_type='.$po_type.'
		&category='.$category.'" target="_blank">'.$count.'</a>';
		
	}
	
	
	/***************** Details PO Functions *************/
	
	function pending_po_det($po_stat, $po_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
		
		$html="	
		  <tr style='background-color:#0CF;'>
			<td><b>S.NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO AMENDMENT NO.</b></td>
			<td><b>SUPP NAME</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>PO CREATE DATE</b></td>
			<td><b>PO VALUE</b></td>
			<td><b>PO LIVE STATUS</b></td>
			<td><b>PO ERP STATUS</b></td>
			<td><b>AGE</b></td>
			<td><b>CREATED BY</b></td>
			<td><b>ATAC NO</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>CUSTOMER NAME</b></td>
		  </tr>";
		
		if($category != 'ALL'){
		
			if($po_type != 'SRV'){
			
				$sql = "select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno  
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y where z.pomas_podocstatus IN('$po_stat')
				and z.pomas_pono in (
					select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
					where a.poprq_prno = b.pr_num and b.category in('$category')
				)
				and z.pomas_suppliercode = y.supp_spmn_supcode
				and substring(z.pomas_pono,1,3) in('$po_type')
				and z.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = z.pomas_pono)
				";
				
			} else {
				
				$sql = "select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y
				where z.pomas_podocstatus in('$po_stat')
				and z.pomas_suppliercode = y.supp_spmn_supcode 
				and substring(z.pomas_pono,1,3) in('$po_type')";
				
			}
			
		} else {
			
				$category1 = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES'
				,'SERVICE','PACKING','AVAZONIC','NON PRODUCTION CONSUMABLES','IT";
				
				$sql = "select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno 
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y where z.pomas_podocstatus IN('$po_stat')
				and z.pomas_pono in (
					select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
					where a.poprq_prno = b.pr_num and b.category in('$category1')
				)
				and z.pomas_suppliercode = y.supp_spmn_supcode
				and substring(z.pomas_pono,1,3) in('$po_type')
				and z.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = z.pomas_pono)
				
				UNION
				
				select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno 
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y
				where z.pomas_podocstatus in('$po_stat')
				and z.pomas_suppliercode = y.supp_spmn_supcode 
				and substring(z.pomas_pono,1,3) in('SRV')";
				
		}
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
		  $sno++;
		  $pomas_pono = $row->pomas_pono;
		  $supp_spmn_supname = $row->supp_spmn_supname;
		  $pomas_createddate = $row->pomas_createddate;
		  $pomas_pobasicvalue = $row->pomas_pobasicvalue;
		  $pomas_podocstatus = $row->pomas_podocstatus;
		  $age = $row->age;
		  $pomas_createdby = $row->pomas_createdby;
		  $pomas_poamendmentno = $row->pomas_poamendmentno;
		  
			if($pomas_podocstatus == 'MD'){
				$pomas_podocstatus = "Made In Draft";
			} else if($pomas_podocstatus == 'SC'){
				$pomas_podocstatus = "Short Closed";
			} else if($pomas_podocstatus == 'FR'){
				$pomas_podocstatus = "Fresh";
			} else if($pomas_podocstatus == 'DF'){
				$pomas_podocstatus = "Draft";
			} else if($pomas_podocstatus == 'DE'){
				$pomas_podocstatus = "Deleted";
			} else if($pomas_podocstatus == 'AM'){
				$pomas_podocstatus = "Under Amendment";
			} else if($pomas_podocstatus == 'CL'){
				$pomas_podocstatus = "Closed";
			} else if($pomas_podocstatus == 'OP'){
				$pomas_podocstatus = "Open";
			}
			
			$sql_cat = "select po_category,status from tipldb..po_master_table where po_num = '$pomas_pono'";
			$query_cat = $ci->db->query($sql_cat);
			
			if($query_cat->num_rows() > 0){
				foreach($query_cat->result() as $row){
					$category = $row->po_category;
					$status = $row->status;
				}
			} else {
				$category = "";
				$status = $row->status;
			}
			
			$sql_proj_det = "select TOP 1 atac_no,project_name,customer_name,* from TIPLDB..insert_po where po_num = '$pomas_pono'";
			
			$qry_proj_det = $ci->db->query($sql_proj_det);
			
			if($qry_proj_det->num_rows() > 0){
				foreach($qry_proj_det->result() as $row){
					$atac_no = $row->atac_no;
					$project_name = $row->project_name;
					$customer_name = $row->customer_name;
				}
			} else {
				$atac_no ="";
				$project_name = "";
				$customer_name = "";
			}
			
		  $html.="
		  <tr>
			<td>".$sno."</td>
			<td><a href=".base_url()."index.php/po_reportc/po_details/".$pomas_pono." target='_blank'>".$pomas_pono."</a></td>
			<td>".$pomas_poamendmentno."</td>
			<td>".$supp_spmn_supname."</td>
			<td>".$category."</td>
			<td>".substr($pomas_createddate,0,11)."</td>
			<td>".number_format($pomas_pobasicvalue,2,".","")."</td>
			<td>".$status."</td>
			<td>".$pomas_podocstatus."</td>
			<td>".$age."</td>
			<td>".$pomas_createdby."</td>
			<td>".$atac_no."</td>
			<td><p>".$project_name."</p></td>
			<td><p>".$customer_name."</p></td>
		  </tr>";
		}
		
		echo $html;
		
	}
	
	//Details with other feilds not in main report
	function pending_po_det_new($po_stat, $po_type, $category){
		
		$ci =& get_instance();
		$ci->load->database();
	
		$html="	
		  <tr style='background-color:#0CF;'>
			<td><b>S.NO.</b></td>
			<td><b>PO NUM.</b></td>
			<td><b>PO AMENDMENT NO.</b></td>
			<td><b>SUPP NAME</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>PO CREATE DATE</b></td>
			<td><b>PO VALUE</b></td>
			<td><b>PO LIVE STATUS</b></td>
			<td><b>PO ERP STATUS</b></td>
			<td><b>AGE</b></td>
			<td><b>CREATED BY</b></td>
			<td><b>PO LINE NO</b></td>
			<td><b>PR NO</b></td>
			<td><b>ITEM CODE</b></td>
			<td><b>ITEM DESC</b></td>
			<td><b>PO QTY</b></td>
			<td><b>PROJECT NAME</b></td>
			<td><b>ATAC NO</b></td>
			<td><b>CUSTOMER NAME</b></td>
		  </tr>";
		
		if($category != 'ALL'){
		
			if($po_type != 'SRV'){
			
				$sql = "select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno
				,po_line_no,po_ipr_no,po_item_code,po_itm_desc,for_stk_quantity,project_name,atac_no,customer_name  
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y, tipldb..po_master_table x, tipldb..insert_po w
				where z.pomas_podocstatus IN('$po_stat')
				and z.pomas_pono in (
					select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
					where a.poprq_prno = b.pr_num and b.category in('$category')
				)
				and z.pomas_suppliercode = y.supp_spmn_supcode
				and substring(z.pomas_pono,1,3) in('$po_type')
				and z.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = z.pomas_pono)
				and x.po_num = z.pomas_pono 
				and x.po_num = w.po_num
				";
				
			} else {
				
				$sql = "select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y
				where z.pomas_podocstatus in('$po_stat')
				and z.pomas_suppliercode = y.supp_spmn_supcode 
				and substring(z.pomas_pono,1,3) in('$po_type')";
				
			}
			
		} else {
			
				$category1 = "CAPITAL GOODS','SENSORS','SECURITY','INSTRUMENTATION','TOOLS','PRODUCTION CONSUMABLES'
				,'SERVICE','PACKING','AVAZONIC','NON PRODUCTION CONSUMABLES','IT";
				
				$sql = "select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno 
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y where z.pomas_podocstatus IN('$po_stat')
				and z.pomas_pono in (
					select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
					where a.poprq_prno = b.pr_num and b.category in('$category1')
				)
				and z.pomas_suppliercode = y.supp_spmn_supcode
				and substring(z.pomas_pono,1,3) in('$po_type')
				and z.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = z.pomas_pono)
				
				UNION
				
				select pomas_pono,supp_spmn_supname,pomas_createddate,pomas_pobasicvalue,pomas_podocstatus,
				datediff(DAY, pomas_createddate, getdate()) as age,pomas_createdby,pomas_poamendmentno 
				from scmdb..po_pomas_pur_order_hdr z, scmdb..supp_spmn_suplmain y
				where z.pomas_podocstatus in('$po_stat')
				and z.pomas_suppliercode = y.supp_spmn_supcode 
				and substring(z.pomas_pono,1,3) in('SRV')";
				
		}
		
		
		$query = $ci->db->query($sql);
		
		$sno=0;
		foreach ($query->result() as $row) {
		$sno++;
		$pomas_pono = $row->pomas_pono;
		$supp_spmn_supname = $row->supp_spmn_supname;
		$pomas_createddate = $row->pomas_createddate;
		$pomas_pobasicvalue = $row->pomas_pobasicvalue;
		$pomas_podocstatus = $row->pomas_podocstatus;
		$age = $row->age;
		$pomas_createdby = $row->pomas_createdby;
		$pomas_poamendmentno = $row->pomas_poamendmentno;
		
		$po_line_no = $row->po_line_no;
		$po_ipr_no = $row->po_ipr_no;
		$po_item_code = $row->po_item_code;
		$po_itm_desc = $row->po_itm_desc;
		$for_stk_quantity = $row->for_stk_quantity;
		$project_name = $row->project_name;
		$atac_no = $row->atac_no;
		$customer_name = $row->customer_name;		
		
		
		$sql_cat = "select po_category,status from tipldb..po_master_table where po_num = '$pomas_pono'";
		$query_cat = $ci->db->query($sql_cat);
		
		if($query_cat->num_rows() > 0){
			foreach($query_cat->result() as $row){
				$category = $row->po_category;
				$status = $row->status;
			}
		} else {
			$category = "";
			$status = $row->status;
		}
			
		$html.="
			<tr>
				<td>".$sno."</td>
				<td><a href=".base_url()."index.php/po_reportc/po_details/".$pomas_pono." target='_blank'>".$pomas_pono."</a></td>
				<td>".$pomas_poamendmentno."</td>
				<td>".$supp_spmn_supname."</td>
				<td>".$category."</td>
				<td>".substr($pomas_createddate,0,11)."</td>
				<td>".number_format($pomas_pobasicvalue,2,".","")."</td>
				<td>".$status."</td>
				<td>".$pomas_podocstatus."</td>
				<td>".$age."</td>
				<td>".$pomas_createdby."</td>
				<td>".$po_line_no."</td>
				<td>".$po_ipr_no."</td>
				<td>".$po_item_code."</td>
				<td>".$po_itm_desc."</td>
				<td>".$for_stk_quantity."</td>
				<td>".$project_name."</td>
				<td>".$atac_no."</td>
				<td>".$customer_name."</td>
			</tr>";
		}
		
		echo $html;	
	}
	
}