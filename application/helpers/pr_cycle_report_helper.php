<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('pr_cycle_report'))
{	
	//Converting Month Into Alphbets
	function month_year($date){
		//GETTING MONTH
		$month = substr($date,5,2);
		$year  = substr($date,2,2);
		
		if($month == '01'){
			$month_word = "JAN";
		} else if($month == '02'){
			$month_word = "FEB";
		} else if($month == '03'){
			$month_word = "MAR";
		} else if($month == '04'){
			$month_word = "APR";
		} else if($month == '05'){
			$month_word = "MAY";
		} else if($month == '06'){
			$month_word = "JUNE";
		} else if($month == '07'){
			$month_word = "JULY";
		} else if($month == '08'){
			$month_word = "AUG";
		} else if($month == '09'){
			$month_word = "SEP";
		} else if($month == '10'){
			$month_word = "OCT";
		} else if($month == '11'){
			$month_word = "NOV";
		} else if($month == '12'){
			$month_word = "DEC";
		}
		
		$month_year = $month_word."-".$year;
		echo $month_year;
	}
	
	//Total Fresh PR
	function pr_count($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type == 'PR'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'";
		
		} else if($type == 'PO'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		} else if($type == 'GRCPT'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.preqm_prno 
			and a.pr_num = c.po_ipr_no 
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and b.preqm_status = 'AU'
			and d.pomas_podocstatus in('OP')
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and substring(a.pr_num,1,3) != 'LPR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		}
		
		$row_count = $ci->db->query($count_query1);
		
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det?fun=pr_count_det&type='.$type.'&owner_name='.$owner_name.'
		&category='.$category.'" target="_blank">'.$count.'</a>';
    }
	
	//Age Greater than One Day
	function pr_age_greater($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type == 'PR'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'
			and DATEDIFF(day,a.create_date, getdate()) > 1";
		
		} else if($type == 'PO'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) > 1";
		
		} else if($type == 'GRCPT'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.preqm_prno 
			and a.pr_num = c.po_ipr_no 
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and b.preqm_status = 'AU'
			and d.pomas_podocstatus in('OP')
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and substring(a.pr_num,1,3) != 'LPR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) > 1";
		
		}
		
		$row_count = $ci->db->query($count_query1);
		
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det?fun=pr_age_greater_det&type='.$type.'&owner_name='.$owner_name.'&category='.$category.'" target="_blank">'.$count.'</a>';
				
    }
	
	//Age Less Than Equal To One Day
	function pr_age_less($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type == 'PR'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'
			and DATEDIFF(day,a.create_date, getdate()) <= 1";
		
		} else if($type == 'PO'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) <= 1";
			
		} else if($type == 'GRCPT'){
			
			$count_query1="select count(*) as count1 from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.preqm_prno 
			and a.pr_num = c.po_ipr_no 
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and b.preqm_status = 'AU'
			and d.pomas_podocstatus in('OP')
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and substring(a.pr_num,1,3) != 'LPR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) <= 1";
			
		}
		
		$row_count = $ci->db->query($count_query1);
		
		foreach ($row_count->result() as $row) {
		  $count = $row->count1;
		}
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det?fun=pr_age_less_det&type='.$type.'&owner_name='.$owner_name.'
		&category='.$category.'" target="_blank">'.$count.'</a>';
    }
	
	//Last 3 months and current month PR Creation to PR Authorization Conversion AVERAGE AGE (IN DAYS)
	function avg_monthwise($type,$owner_name,$category,$from_date,$to_date)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type == 'PR'){
			
			$sql="select DATEDIFF(day,b.preqm_createddate, b.preqm_authdate) as days,* from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'AU'
			and convert(date,b.preqm_createddate) between '$from_date' and '$to_date'
			order by b.preqm_createddate desc";
		
		} else if($type == 'PO'){
			
			$sql="select DATEDIFF(day,b.preqm_authdate, c.supp_for_pur_date) as days,* from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus not in('FR')
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and convert(date,b.preqm_authdate) between '$from_date' and '$to_date'";
			
		} else if($type == 'GRCPT'){
			
			$sql="select DATEDIFF(day,b.preqm_authdate, e.gr_hdr_grdate) as days,*
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d, scmdb..gr_hdr_grmain e 
			where a.pr_num = b.preqm_prno
			and a.pr_num = c.po_ipr_no
			and c.po_num = d.pomas_pono
			and d.pomas_pono = e.gr_hdr_orderno
			and d.pomas_podocstatus in('OP','CL','SC','RT','NT')
			and d.pomas_poamendmentno = e.gr_hdr_orderamendno
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_pono in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and e.gr_hdr_grdate = (select max(gr_hdr_grdate) from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and e.gr_hdr_grstatus in('FZ','FA','FM')
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and b.preqm_authdate between '$from_date' and '$to_date'";
			
		}

		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
		}
		
		$avg = $days_sum/$counter;
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det?fun=avg_monthwise_det&type='.$type.'&owner_name='.$owner_name.'&category='.$category.'&from_date='.$from_date.'&to_date='.$to_date.'" target="_blank">'.number_format($avg,2).'</a>';
		
    }
	
	//Average Of Open Items
	function avg_open_items($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type == 'PR'){
			
			$sql="select DATEDIFF(day,b.preqm_createddate, getdate()) as days,* from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'";

		} else if($type == 'PO'){
			
			$sql="select DATEDIFF(day,b.preqm_authdate, getdate()) as days from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		} else if($type == 'GRCPT'){
			
			$sql="select DATEDIFF(day,b.preqm_authdate, getdate()) as days from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'OP'
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		}

		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
		}
		
		$avg = $days_sum/$counter;
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det?fun=avg_open_items_det&type='.$type.'&owner_name='.$owner_name.'
		&category='.$category.'" target="_blank">'.number_format($avg,2).'</a>';
		
    }
	
	/******************** DETAILS *************************/
	
	//Total Fresh PR
	function pr_count_det($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type=='PR'){
		
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
			</tr>
		    ';
		
		} else if($type=='PO'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
			</tr>
		';
			
		} else if($type=='GRCPT'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>PO AUTHORIZE DATE</b></td>
			</tr>
		    ';
		}
		
		
		if($type == 'PR'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date 
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'
			order by a.pr_num desc";
		
		} else if($type == 'PO'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate 
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		} else if($type == 'GRCPT'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,d.pomas_poauthdate 
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.preqm_prno 
			and a.pr_num = c.po_ipr_no 
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and b.preqm_status = 'AU'
			and d.pomas_podocstatus in('OP')
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and substring(a.pr_num,1,3) != 'LPR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		}
		
		$row_count = $ci->db->query($count_query1);
		
		$sno = 0;
		foreach ($row_count->result() as $row) {
		  $sno++;
		  $pr_num = $row->pr_num;
		  $item_code = $row->item_code;
		  $itm_desc = $row->itm_desc;
		  $category = $row->category;
		  $usage = $row->usage;
		  $required_qty = $row->required_qty;
		  $trans_uom = $row->trans_uom;
		  $created_by = $row->created_by;
		  $create_date = $row->create_date;
		  
		  //Encoding Item Code
		  $item_code1 = urlencode($item_code);
				
		  if(strpos($item_code1, '%2F') !== false)
		  {
			$item_code2 = str_replace("%2F","chandra",$item_code1);
		  }
		  else 
		  {
			$item_code2 = $item_code1;
		  }
		  //Encoding Item Code Ends
		  
		  if($type == 'PO'){
			  
			 $po_num = $row->pomas_pono;
			 $pomas_poamendmentno = $row->pomas_poamendmentno;
			 $pomas_podocstatus = $row->pomas_podocstatus;
			 $po_supp_name = $row->po_supp_name;
			 $ipr_category = $row->ipr_category; 
			 $pomas_podate = $row->pomas_podate; 
			 
		  } else if($type == 'GRCPT'){
			  
			 $po_num = $row->pomas_pono;
			 $pomas_poamendmentno = $row->pomas_poamendmentno;
			 $pomas_podocstatus = $row->pomas_podocstatus;
			 $po_supp_name = $row->po_supp_name;
			 $ipr_category = $row->ipr_category; 
			 $pomas_podate = $row->pomas_podate;
			 $pomas_poauthdate = $row->pomas_poauthdate;
			 
		  }
		  
		  $htm.='
		  <tr>
		  	<td>'.$sno.'</td>
			<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
			<td>'.$itm_desc.'</td>
			<td>'.$category.'</td>
			<td>'.$usage.'</td>
			<td>'.number_format($required_qty,2).'</td>
			<td>'.$trans_uom.'</td>
			<td>'.$created_by.'</td>
			<td>'.substr($create_date,0,11).'</td>';
		if($type == 'PO'){
			
			$htm.='
			<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
			<td>'.$pomas_poamendmentno.'</td>
			<td>'.$pomas_podocstatus.'</td>
			<td>'.$po_supp_name.'</td>
			<td>'.$ipr_category.'</td>
			<td>'.substr($pomas_podate,0,11).'</td>
		  ';
			
		} else if($type == 'GRCPT'){
			
			$htm.='
			<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
			<td>'.$pomas_poamendmentno.'</td>
			<td>'.$pomas_podocstatus.'</td>
			<td>'.$po_supp_name.'</td>
			<td>'.$ipr_category.'</td>
			<td>'.substr($pomas_podate,0,11).'</td>
			<td>'.substr($pomas_poauthdate,0,11).'</td>
		  ';
			
		}
			
		  $htm.='</tr>
		  ';
		}
		
		echo $htm;
    }
	//Age Greater than One Day
	function pr_age_greater_det($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type=='PR'){
		
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
			</tr>
		    ';
		
		} else if($type=='PO'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
			</tr>
		';
			
		} else if($type=='GRCPT'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>PO AUTHORIZE DATE</b></td>
			</tr>
		    ';
		}
		
		if($type == 'PR'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date  
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'
			and DATEDIFF(day,a.create_date, getdate()) > 1
			order by a.pr_num desc";
		
		} else if($type == 'PO'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate 
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) > 1";
		
		} else if($type == 'GRCPT'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,d.pomas_poauthdate 
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.preqm_prno 
			and a.pr_num = c.po_ipr_no 
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and b.preqm_status = 'AU'
			and d.pomas_podocstatus in('OP')
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and substring(a.pr_num,1,3) != 'LPR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) > 1";
		
		}
		
		$row_count = $ci->db->query($count_query1);
		
		$sno = 0;
		foreach ($row_count->result() as $row) {
		  $sno++;
		  $pr_num = $row->pr_num;
		  $item_code = $row->item_code;
		  $itm_desc = $row->itm_desc;
		  $category = $row->category;
		  $usage = $row->usage;
		  $required_qty = $row->required_qty;
		  $trans_uom = $row->trans_uom;
		  $created_by = $row->created_by;
		  $create_date = $row->create_date;
		  
		  //Encoding Item Code
		  $item_code1 = urlencode($item_code);
				
		  if(strpos($item_code1, '%2F') !== false)
		  {
			$item_code2 = str_replace("%2F","chandra",$item_code1);
		  }
		  else 
		  {
			$item_code2 = $item_code1;
		  }
		  //Encoding Item Code Ends
		  
		  if($type == 'PO'){
			  
			 $po_num = $row->pomas_pono;
			 $pomas_poamendmentno = $row->pomas_poamendmentno;
			 $pomas_podocstatus = $row->pomas_podocstatus;
			 $po_supp_name = $row->po_supp_name;
			 $ipr_category = $row->ipr_category; 
			 $pomas_podate = $row->pomas_podate; 
			 
		  } else if($type == 'GRCPT'){
			  
			 $po_num = $row->pomas_pono;
			 $pomas_poamendmentno = $row->pomas_poamendmentno;
			 $pomas_podocstatus = $row->pomas_podocstatus;
			 $po_supp_name = $row->po_supp_name;
			 $ipr_category = $row->ipr_category; 
			 $pomas_podate = $row->pomas_podate;
			 $pomas_poauthdate = $row->pomas_poauthdate;
			 
		  }
		  
		  $htm.='
		  <tr>
		  	<td>'.$sno.'</td>
			<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
			<td>'.$itm_desc.'</td>
			<td>'.$category.'</td>
			<td>'.$usage.'</td>
			<td>'.number_format($required_qty,2).'</td>
			<td>'.$trans_uom.'</td>
			<td>'.$created_by.'</td>
			<td>'.substr($create_date,0,11).'</td>';
		if($type == 'PO'){
			
			$htm.='
			<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
			<td>'.$pomas_poamendmentno.'</td>
			<td>'.$pomas_podocstatus.'</td>
			<td>'.$po_supp_name.'</td>
			<td>'.$ipr_category.'</td>
			<td>'.substr($pomas_podate,0,11).'</td>
		  ';
			
		} else if($type == 'GRCPT'){
			
			$htm.='
			<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
			<td>'.$pomas_poamendmentno.'</td>
			<td>'.$pomas_podocstatus.'</td>
			<td>'.$po_supp_name.'</td>
			<td>'.$ipr_category.'</td>
			<td>'.substr($pomas_podate,0,11).'</td>
			<td>'.substr($pomas_poauthdate,0,11).'</td>
		  ';
			
		}
			
		  $htm.='</tr>
		  ';
		}
		
		echo $htm;	
    }
	
	//Age Less Than Equal To One Day
	function pr_age_less_det($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type=='PR'){
		
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
			</tr>
		    ';
		
		} else if($type=='PO'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
			</tr>
		';
			
		} else if($type=='GRCPT'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>PO AUTHORIZE DATE</b></td>
			</tr>
		    ';
		}
		
		if($type == 'PR'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date 
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'
			and DATEDIFF(day,a.create_date, getdate()) <= 1
			order by a.pr_num desc";
		
		} else if($type == 'PO'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) <= 1";
			
		} else if($type == 'GRCPT'){
			
			$count_query1="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,d.pomas_poauthdate  
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.preqm_prno 
			and a.pr_num = c.po_ipr_no 
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and b.preqm_status = 'AU'
			and d.pomas_podocstatus in('OP')
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and substring(a.pr_num,1,3) != 'LPR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and datediff(day,b.preqm_authdate,getdate()) <= 1";
			
		}
		
		$row_count = $ci->db->query($count_query1);
		
		$sno = 0;
		foreach ($row_count->result() as $row) {
		  $sno++;
		  $pr_num = $row->pr_num;
		  $item_code = $row->item_code;
		  $itm_desc = $row->itm_desc;
		  $category = $row->category;
		  $usage = $row->usage;
		  $required_qty = $row->required_qty;
		  $trans_uom = $row->trans_uom;
		  $created_by = $row->created_by;
		  $create_date = $row->create_date;
		  
		  //Encoding Item Code
		  $item_code1 = urlencode($item_code);
				
		  if(strpos($item_code1, '%2F') !== false)
		  {
			$item_code2 = str_replace("%2F","chandra",$item_code1);
		  }
		  else 
		  {
			$item_code2 = $item_code1;
		  }
		  //Encoding Item Code Ends
		  
		  if($type == 'PO'){
			  
			 $po_num = $row->pomas_pono;
			 $pomas_poamendmentno = $row->pomas_poamendmentno;
			 $pomas_podocstatus = $row->pomas_podocstatus;
			 $po_supp_name = $row->po_supp_name;
			 $ipr_category = $row->ipr_category; 
			 $pomas_podate = $row->pomas_podate; 
			 
		  } else if($type == 'GRCPT'){
			  
			 $po_num = $row->pomas_pono;
			 $pomas_poamendmentno = $row->pomas_poamendmentno;
			 $pomas_podocstatus = $row->pomas_podocstatus;
			 $po_supp_name = $row->po_supp_name;
			 $ipr_category = $row->ipr_category; 
			 $pomas_podate = $row->pomas_podate;
			 $pomas_poauthdate = $row->pomas_poauthdate;
			 
		  }
		  
		  $htm.='
		  <tr>
		  	<td>'.$sno.'</td>
			<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
			<td>'.$itm_desc.'</td>
			<td>'.$category.'</td>
			<td>'.$usage.'</td>
			<td>'.number_format($required_qty,2).'</td>
			<td>'.$trans_uom.'</td>
			<td>'.$created_by.'</td>
			<td>'.substr($create_date,0,11).'</td>';
		if($type == 'PO'){
			
			$htm.='
			<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
			<td>'.$pomas_poamendmentno.'</td>
			<td>'.$pomas_podocstatus.'</td>
			<td>'.$po_supp_name.'</td>
			<td>'.$ipr_category.'</td>
			<td>'.substr($pomas_podate,0,11).'</td>
		  ';
			
		} else if($type == 'GRCPT'){
			
			$htm.='
			<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
			<td>'.$pomas_poamendmentno.'</td>
			<td>'.$pomas_podocstatus.'</td>
			<td>'.$po_supp_name.'</td>
			<td>'.$ipr_category.'</td>
			<td>'.substr($pomas_podate,0,11).'</td>
			<td>'.substr($pomas_poauthdate,0,11).'</td>
		  ';
			
		}
			
		  $htm.='</tr>
		  ';
		}
		
		echo $htm;
    }
	
	//Last 3 months and current month PR Creation to PR Authorization Conversion AVERAGE AGE (IN DAYS)
	function avg_monthwise_det($type,$owner_name,$category,$from_date,$to_date)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type=='PR'){
		
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>AGE IN DAYS</b></td>
			</tr>
		    ';
		
		} else if($type=='PO'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>AGE IN DAYS</b></td>
			</tr>
		';
			
		} else if($type=='GRCPT'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>PO AUTHORIZE DATE</b></td>
				
				<td><b>GATE ENTRY NO</b></td>
				<td><b>GATE ENTRY DATE</b></td>
				<td><b>GRCPT NO</b></td>
				<td><b>GRCPT CREATE DATE</b></td>
				<td><b>GRCPT STATUS</b></td>
				<td><b>GRCPT FR DATE</b></td>
				<td><b>GRCPT FA DATE</b></td>
				<td><b>GRCPT FM DATE</b></td>
				<td><b>AGE IN DAYS</b></td>
			</tr>
		    ';
		}
		
		if($type == 'PR'){
			
			$sql="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			DATEDIFF(day,b.preqm_createddate, b.preqm_authdate) as days,* from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'AU'
			and convert(date,b.preqm_createddate) between '$from_date' and '$to_date'
			order by a.pr_num desc";
		
		} else if($type == 'PO'){
			
			$sql="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,
			DATEDIFF(day,b.preqm_authdate, c.supp_for_pur_date) as days,* from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus not in('FR')
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and convert(date,b.preqm_authdate) between '$from_date' and '$to_date'";
			
		} else if($type == 'GRCPT'){
			
			$sql="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,d.pomas_poauthdate,
			DATEDIFF(day,b.preqm_authdate, e.gr_hdr_grdate) as days,
			e.gr_hdr_gatepassno,e.gr_hdr_gatepassdate,e.gr_hdr_grno,e.gr_hdr_grdate,e.gr_hdr_grstatus,e.gr_hdr_frdate,e.gr_hdr_fadate,e.gr_hdr_fmdate,*
			from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d, scmdb..gr_hdr_grmain e 
			where a.pr_num = b.preqm_prno
			and a.pr_num = c.po_ipr_no
			and c.po_num = d.pomas_pono
			and d.pomas_pono = e.gr_hdr_orderno
			and d.pomas_podocstatus in('OP','CL','SC','RT','NT')
			and d.pomas_poamendmentno = e.gr_hdr_orderamendno
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_pono in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and e.gr_hdr_grdate = (select max(gr_hdr_grdate) from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and e.gr_hdr_grstatus in('FZ','FA','FM')
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')
			and b.preqm_authdate between '$from_date' and '$to_date'";
			
		}

		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		$sno = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
			
		    $sno++;
		    $pr_num = $row->pr_num;
		    $item_code = $row->item_code;
		    $itm_desc = $row->itm_desc;
		    $category = $row->category;
		    $usage = $row->usage;
		    $required_qty = $row->required_qty;
		    $trans_uom = $row->trans_uom;
		    $created_by = $row->created_by;
		    $create_date = $row->create_date;
			
			  //Encoding Item Code
			  $item_code1 = urlencode($item_code);
					
			  if(strpos($item_code1, '%2F') !== false)
			  {
				$item_code2 = str_replace("%2F","chandra",$item_code1);
			  }
			  else 
			  {
				$item_code2 = $item_code1;
			  }
			  //Encoding Item Code Ends
			
			  if($type == 'PO'){
			  
				 $po_num = $row->pomas_pono;
				 $pomas_poamendmentno = $row->pomas_poamendmentno;
				 $pomas_podocstatus = $row->pomas_podocstatus;
				 $po_supp_name = $row->po_supp_name;
				 $ipr_category = $row->ipr_category; 
				 $pomas_podate = $row->pomas_podate; 
				 
			  } else if($type == 'GRCPT'){
				  
				 $po_num = $row->pomas_pono;
				 $pomas_poamendmentno = $row->pomas_poamendmentno;
				 $pomas_podocstatus = $row->pomas_podocstatus;
				 $po_supp_name = $row->po_supp_name;
				 $ipr_category = $row->ipr_category; 
				 $pomas_podate = $row->pomas_podate;
				 $pomas_poauthdate = $row->pomas_poauthdate;
				 
				 $gr_hdr_gatepassno = $row->gr_hdr_gatepassno;
				 $gr_hdr_gatepassdate = $row->gr_hdr_gatepassdate;
				 $gr_hdr_grno = $row->gr_hdr_grno;
				 $gr_hdr_grdate = $row->gr_hdr_grdate;
				 $gr_hdr_grstatus = $row->gr_hdr_grstatus;
				 $gr_hdr_frdate = $row->gr_hdr_frdate;
				 $gr_hdr_fadate = $row->gr_hdr_fadate;
				 $gr_hdr_fmdate = $row->gr_hdr_fmdate;
			  }
			
			
		
			$htm.='
			  <tr>';
			  if($type == 'PR'){
				  
			  	$htm.='
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			    <td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$itm_desc.'</td>
				<td>'.$category.'</td>
				<td>'.$usage.'</td>
				<td>'.number_format($required_qty,2).'</td>
				<td>'.$trans_uom.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td>'.$days.'</td>';
				
		      } else if($type == 'PO'){
				
				$htm.='
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			    <td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$itm_desc.'</td>
				<td>'.$category.'</td>
				<td>'.$usage.'</td>
				<td>'.number_format($required_qty,2).'</td>
				<td>'.$trans_uom.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
				<td>'.$pomas_poamendmentno.'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$po_supp_name.'</td>
				<td>'.$ipr_category.'</td>
				<td>'.substr($pomas_podate,0,11).'</td>
				<td>'.$days.'</td>
			  ';
				
			 } else if($type == 'GRCPT'){
				
				$htm.='
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			    <td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$itm_desc.'</td>
				<td>'.$category.'</td>
				<td>'.$usage.'</td>
				<td>'.number_format($required_qty,2).'</td>
				<td>'.$trans_uom.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
				<td>'.$pomas_poamendmentno.'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$po_supp_name.'</td>
				<td>'.$ipr_category.'</td>
				<td>'.substr($pomas_podate,0,11).'</td>
				<td>'.substr($pomas_poauthdate,0,11).'</td>
				
				<td>'.$gr_hdr_gatepassno.'</td>
				<td>'.substr($gr_hdr_gatepassdate,0,11).'</td>
				<td>'.$gr_hdr_grno.'</td>
				<td>'.substr($gr_hdr_grdate,0,11).'</td>
				<td>'.$gr_hdr_grstatus.'</td>
				<td>'.substr($gr_hdr_frdate,0,11).'</td>
				<td>'.substr($gr_hdr_fadate,0,11).'</td>
				<td>'.substr($gr_hdr_fmdate,0,11).'</td>
				<td>'.$days.'</td>
			  ';
				
			}
			
		  $htm.='</tr>
		  ';
		}
		
		$avg = $days_sum/$counter;
		
		if($type == 'PR'){
			$cols = '10';
		} else if($type == 'PO'){
			$cols = '16';
		} else if($type == 'GRCPT'){
			$cols = '25';
		}
		
		$htm.='
			  <tr>
				<td colspan="'.$cols.'" style="text-align:right; background-color:#0CF"><b>AVERAGE AGE (IN DAYS)</b></td>
				<td>'.number_format($avg,2).'</td>
			  </tr>
		  ';
		  
		echo $htm;
		
    }
	
	//Average Of Open Items
	
	function avg_open_items_det($type,$owner_name,$category)
    {
		$ci =& get_instance();
		$ci->load->database();
		
		if($type=='PR'){
		
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>AGE IN DAYS</b></td>
			</tr>
		    ';
		
		} else if($type=='PO'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>AGE IN DAYS</b></td>
			</tr>
		';
			
		} else if($type=='GRCPT'){
			
			$htm = '
			<tr style="background-color:#0CF">
				<td><b>SNO.</b></td>
				<td><b>PR NUMBER</b></td>
				<td><b>ITEM CODE</b></td>
				<td><b>ITEM DESC.</b></td>
				<td><b>CATEGORY</b></td>
				<td><b>USAGE TYPE</b></td>
				<td><b>QUANTITY</b></td>
				<td><b>UOM</b></td>
				<td><b>PR CREATED BY</b></td>
				<td><b>PR CREATED DATE</b></td>
				<td><b>PO NUMBER</b></td>
				<td><b>PO AMEND NO</b></td>
				<td><b>PO STATUS</b></td>
				<td><b>SUPP. NAME</b></td>
				<td><b>PO CATEGORY</b></td>
				<td><b>PO CREATE DATE</b></td>
				<td><b>PO AUTHORIZE DATE</b></td>
				<td><b>AGE IN DAYS</b></td>
			</tr>
		    ';
		}
		
		if($type == 'PR'){
			
			$sql="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			DATEDIFF(day,b.preqm_createddate, getdate()) as days,* from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b 
			where a.pr_num = b.preqm_prno 
			and a.created_by in('$owner_name')
			and a.category in('$category')
			and b.preqm_status = 'FR'
			order by a.pr_num desc";

		} else if($type == 'PO'){
			
			$sql="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,
			DATEDIFF(day,b.preqm_authdate, getdate()) as days from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'FR'
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		} else if($type == 'GRCPT'){
			
			$sql="select a.pr_num,a.item_code,a.itm_desc,a.category,a.usage,a.required_qty,a.trans_uom,a.created_by,a.create_date,
			d.pomas_pono,d.pomas_poamendmentno,d.pomas_podocstatus,c.po_supp_name,c.ipr_category,d.pomas_podate,d.pomas_poauthdate,
			DATEDIFF(day,b.preqm_authdate, getdate()) as days from tipldb..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b, 
			tipldb..insert_po c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.preqm_prno and a.pr_num = c.po_ipr_no and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus not in('DF','DE') 
			and b.preqm_status = 'AU'
			and a.pr_status != 'PR Disapproved At Purchase Level'
			and d.pomas_podocstatus = 'OP'
			and d.pomas_pono not in(select gr_hdr_orderno from scmdb..gr_hdr_grmain where gr_hdr_orderno = d.pomas_pono)
			and c.po_approvedby_lvl0 in('$owner_name')
			and c.ipr_category in('$category')";
			
		}

		$qry = $ci->db->query($sql);
		
		$counter = 0;
		$days_sum = 0;
		$sno = 0;
		foreach($qry->result() as $row){
			$counter++;
			$days = $row->days;
			$days_sum = $days_sum+$days;
			
		    $sno++;
		    $pr_num = $row->pr_num;
		    $item_code = $row->item_code;
		    $itm_desc = $row->itm_desc;
		    $category = $row->category;
		    $usage = $row->usage;
		    $required_qty = $row->required_qty;
		    $trans_uom = $row->trans_uom;
		    $created_by = $row->created_by;
		    $create_date = $row->create_date;
			
			  //Encoding Item Code
			  $item_code1 = urlencode($item_code);
					
			  if(strpos($item_code1, '%2F') !== false)
			  {
				$item_code2 = str_replace("%2F","chandra",$item_code1);
			  }
			  else 
			  {
				$item_code2 = $item_code1;
			  }
			  //Encoding Item Code Ends
			
			if($type == 'PO'){
			  
				 $po_num = $row->pomas_pono;
				 $pomas_poamendmentno = $row->pomas_poamendmentno;
				 $pomas_podocstatus = $row->pomas_podocstatus;
				 $po_supp_name = $row->po_supp_name;
				 $ipr_category = $row->ipr_category; 
				 $pomas_podate = $row->pomas_podate; 
				 
			  } else if($type == 'GRCPT'){
				  
				 $po_num = $row->pomas_pono;
				 $pomas_poamendmentno = $row->pomas_poamendmentno;
				 $pomas_podocstatus = $row->pomas_podocstatus;
				 $po_supp_name = $row->po_supp_name;
				 $ipr_category = $row->ipr_category; 
				 $pomas_podate = $row->pomas_podate;
				 $pomas_poauthdate = $row->pomas_poauthdate;
				 
			  }
			  
			
			$htm.='
			  <tr>';
			  if($type == 'PR'){
				  
			  	$htm.='
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			    <td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$itm_desc.'</td>
				<td>'.$category.'</td>
				<td>'.$usage.'</td>
				<td>'.number_format($required_qty,2).'</td>
				<td>'.$trans_uom.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td>'.$days.'</td>';
				
		      } else if($type == 'PO'){
				
				$htm.='
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			    <td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$itm_desc.'</td>
				<td>'.$category.'</td>
				<td>'.$usage.'</td>
				<td>'.number_format($required_qty,2).'</td>
				<td>'.$trans_uom.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
				<td>'.$pomas_poamendmentno.'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$po_supp_name.'</td>
				<td>'.$ipr_category.'</td>
				<td>'.substr($pomas_podate,0,11).'</td>
				<td>'.$days.'</td>
			  ';
				
			 } else if($type == 'GRCPT'){
				
				$htm.='
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
			    <td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$itm_desc.'</td>
				<td>'.$category.'</td>
				<td>'.$usage.'</td>
				<td>'.number_format($required_qty,2).'</td>
				<td>'.$trans_uom.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($create_date,0,11).'</td>
				<td><a href="'.base_url().'index.php/po_reportc/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
				<td>'.$pomas_poamendmentno.'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$po_supp_name.'</td>
				<td>'.$ipr_category.'</td>
				<td>'.substr($pomas_podate,0,11).'</td>
				<td>'.substr($pomas_poauthdate,0,11).'</td>
				<td>'.$days.'</td>
			  ';
				
			}
			
		  $htm.='</tr>
		  ';
		}
		
		$avg = $days_sum/$counter;
		
		if($type == 'PR'){
			$cols = '10';
		} else if($type == 'PO'){
			$cols = '16';
		} else if($type == 'GRCPT'){
			$cols = '17';
		}
		
		$htm.='
			  <tr>
				<td colspan="'.$cols.'"><b>AVERAGE AGE (IN DAYS)</b></td>
				<td>'.number_format($avg,2).'</td>
			  </tr>
		  ';
		  
		echo $htm;
    }
	
	/************************ 
		Number Of PO's 
	*************************/
	
	function count_no_po($cat_owner,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($cat_owner != 'total'){
		
			$sql="select count(*) as count1 from (
				select distinct a.po_num from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 1' and c.comment_by = '$cat_owner'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION 
				select distinct a.po_num from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 2' and c.comment_by = '$cat_owner'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION
				select distinct a.po_num from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 3' and c.comment_by = '$cat_owner'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
			) as test";
			
		
		} else {
			
			$sql="select count(*) as count1 from (
				select distinct a.po_num from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 1'
				and c.comment_by not in('mmrathore','saket.ranjan','bharat.sharma','ashwani.giri')
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION ALL
				select distinct a.po_num from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 2'
				and c.comment_by not in('raviraj.mehra','sunil.tandon')
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION ALL
				select distinct a.po_num from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 3'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
			) as test";
			
		}
		
		$qry = $ci->db->query($sql);
		
		$count_tot = 0;
		foreach($qry->result() as $row){
			$count = $row->count1;
			$count_tot = $count_tot+$count;
		}
		
		return $count_tot;
	}
	
	//AVG Approval Time
	function avg_app_time_no_po($cat_owner,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($cat_owner != 'total'){
		
			$sql="select sum(app_time) as app_time from (
					select datediff(hour, c.pre_action_date, c.datentime) as app_time from tipldb..po_master_table a, 
					scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
					where a.po_num = b.pomas_pono and a.po_num = c.po_num
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and c.level = 'LEVEL 1' and c.comment_by = '$cat_owner'
					and convert(date,a.po_create_date) between '$from_date' and '$to_date'
					UNION ALL
					select datediff(hour, c.pre_action_date, c.datentime) as app_time from tipldb..po_master_table a, 
					scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
					where a.po_num = b.pomas_pono and a.po_num = c.po_num
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and c.level = 'LEVEL 2' and c.comment_by = '$cat_owner'
					and convert(date,a.po_create_date) between '$from_date' and '$to_date'
					UNION ALL
					select datediff(hour, c.pre_action_date, c.datentime) as app_time from tipldb..po_master_table a, 
					scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
					where a.po_num = b.pomas_pono and a.po_num = c.po_num
					and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
					and c.level = 'LEVEL 3' and c.comment_by = '$cat_owner'
					and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				) as test";
		
		} else {
			
			$sql="select sum(app_time) as app_time from (
				select datediff(hour, c.pre_action_date, c.datentime) as app_time from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 1'
				and c.comment_by not in('mmrathore','saket.ranjan','bharat.sharma','ashwani.giri')
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION ALL 
				select datediff(hour, c.pre_action_date, c.datentime) as app_time from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 2'
				and c.comment_by not in('raviraj.mehra','sunil.tandon')
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION ALL
				select datediff(hour, c.pre_action_date, c.datentime) as app_time from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 3'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
			) as test";
			
		}
		
		$qry = $ci->db->query($sql);
		
		$count=0;
		$tot_app_time = 0;
		foreach($qry->result() as $row){
			$count++;
			$app_time = $row->app_time;	
		}
		
		return round($app_time,2);
	}
	
	/*****************************************
	          DETAILS NO OF PO REPORT
	*****************************************/
	
	//Age less than one
	function avg_app_time_no_po_det($cat_owner,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NUMBER</td>
			<td>SUPP NAME</td>
			<td>PO VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>APPROVAL AUTHORITY NAME</td>
			<td>APPROVAL AUTHORITY APPROVAL TIME (HOURS)</td>
		</tr>';
		
		if($cat_owner != 'total'){
		
			$sql="select a.po_num,po_supp_name,pomas_pobasicvalue,po_create_date,pomas_podocstatus,created_by,level1_approval_by as app_name,
				(select sum(datediff(HOUR, pre_action_date,datentime)) from tipldb..insert_po_comment where po_num = a.po_num 
				and level = 'LEVEL 1' and comment_by = '$cat_owner' ) as diff1 
				from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 1' and c.comment_by = '$cat_owner'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION 
				select a.po_num,po_supp_name,pomas_pobasicvalue,po_create_date,pomas_podocstatus,created_by,level2_approval_by as app_name,
				(select sum(datediff(HOUR, pre_action_date,datentime)) from tipldb..insert_po_comment where po_num = a.po_num 
				and level = 'LEVEL 2' and comment_by = '$cat_owner' ) as diff1  
				from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 2' and c.comment_by = '$cat_owner'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION
				select a.po_num,po_supp_name,pomas_pobasicvalue,po_create_date,pomas_podocstatus,created_by,level3_approval_by as app_name,
				(select sum(datediff(HOUR, pre_action_date,datentime)) from tipldb..insert_po_comment where po_num = a.po_num 
				and level = 'LEVEL 3' and comment_by = '$cat_owner' ) as diff1 
				from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 3' and c.comment_by = '$cat_owner'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'";
		
		} else {
			
			$sql="select a.po_num,po_supp_name,pomas_pobasicvalue,po_create_date,pomas_podocstatus,created_by,level1_approval_by as app_name,
				(select sum(datediff(HOUR, pre_action_date,datentime)) from tipldb..insert_po_comment where po_num = a.po_num 
				and level = 'LEVEL 1' and comment_by not in('mmrathore','saket.ranjan','bharat.sharma','ashwani.giri')) as diff1 
				from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 1' and c.comment_by not in('mmrathore','saket.ranjan','bharat.sharma','ashwani.giri')
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION 
				select a.po_num,po_supp_name,pomas_pobasicvalue,po_create_date,pomas_podocstatus,created_by,level2_approval_by as app_name,
				(select sum(datediff(HOUR, pre_action_date,datentime)) from tipldb..insert_po_comment where po_num = a.po_num 
				and level = 'LEVEL 2' and comment_by not in('raviraj.mehra','sunil.tandon')) as diff1   
				from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 2' and c.comment_by not in('raviraj.mehra','sunil.tandon')
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'
				UNION
				select a.po_num,po_supp_name,pomas_pobasicvalue,po_create_date,pomas_podocstatus,created_by,level3_approval_by as app_name,
				(select sum(datediff(HOUR, pre_action_date,datentime)) from tipldb..insert_po_comment where po_num = a.po_num 
				and level = 'LEVEL 3') as diff1    
				from tipldb..po_master_table a, 
				scmdb..po_pomas_pur_order_hdr b, tipldb..insert_po_comment c
				where a.po_num = b.pomas_pono and a.po_num = c.po_num
				and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = b.pomas_pono)
				and c.level = 'LEVEL 3'
				and convert(date,a.po_create_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno = 0;
		$po_total = 0;
		$diff_tot = 0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_total_value = $row->pomas_pobasicvalue;
			$po_create_date = $row->po_create_date;
			$created_by = $row->created_by;
			$app_name = $row->app_name;
			$pomas_podocstatus = $row->pomas_podocstatus;
			$diff1 = $row->diff1;
			
			//$diff_days = $diff1/24;
			
			$diff_tot = $diff_tot+$diff1;
			$po_total = $po_total+$po_total_value;
			
			$html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'" target="_blank">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.number_format($po_total_value, 2, '.', '').'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_create_date,0,11).'</td>
				<td>'.$app_name.'</td>
				<td>'.number_format($diff1,2,'.','').'</td>
			</tr>
			';
		}
		
		$html.='
		<tr>
			<td colspan="3" style="text-align:right; font-weight:bold; background-color:#0CF">TOTAL</td>
			<td>'.$po_total.'</td>
			<td colspan="4" style="text-align:right; font-weight:bold; background-color:#0CF"><b>TOTAL APPROVAL TIME (HOURS)</b></td>
			<td>'.number_format($diff_tot,2,'.','').'</td>
		</tr>
		';
		
		
		//AVERAGE
		$avg = $diff_tot/$sno;
		
		$html.='
		<tr>
			<td colspan="8" style="text-align:right; font-weight:bold; background-color:#0CF"><b>AVERGAGE APPROVAL TIME (HOURS)</b></td>
			<td>'.number_format($avg,2,'.','').'</td>
		</tr>
		'; 
		
		echo $html;
	}
	
	/*******************************************
		        COST SAVING REPORT
	*******************************************/
	//Count No Of PO's
	function count_no_po_cat($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
			$sql="select count(*) as count1 from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC') 
			and category in('$category')
			and substring(po_num,1,3) in('$po_type') 
			and category not in('OTHERS','RAW MATERIALS')
			and convert(date,po_date) between '$from_date' and '$to_date'";
		
		} else {
			
			$sql="select count(*) as count1 from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')  
			and category not in('OTHERS','RAW MATERIALS')
			and substring(po_num,1,3) in('$po_type')
			and convert(date,po_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$count = $row->count1;
		}
		
		return $count;	
	}

	
	//Sum No of PO with value
	function sum_no_po_cat($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
			$sql="select sum(po_basic_value) as tot_val from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC') 
			and category in('$category') 
			and substring(po_num,1,3) in('$po_type')
			and category not in('OTHERS','RAW MATERIALS')
			and convert(date,po_date) between '$from_date' and '$to_date'";
		
		} else {
			
			$sql="select sum(po_basic_value) as tot_val from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')  
			and category not in('OTHERS','RAW MATERIALS')
			and substring(po_num,1,3) in('$po_type')
			and convert(date,po_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$tot_val = $row->tot_val;
			$tot_val_lakh = $tot_val/100000;
		}
		
		$link = "&&category=".$category."&&from_date=".$from_date."&&to_date=".$to_date;
		
		return number_format($tot_val_lakh, 2, '.', '');	
	}
	
	//Without Basis
	function without_basis_count($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
		$sql="select count(*) as count1 from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category in('$category')
		and substring(po_num,1,3) in('$po_type') 
		and category not in('OTHERS','RAW MATERIALS')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and last_price_sum is null and costing_sum is null";
		
		} else {
			
		$sql="select count(*) as count1 from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC') 
		and category not in('OTHERS','RAW MATERIALS')
		and substring(po_num,1,3) in('$po_type')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and last_price_sum is null and costing_sum is null";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$count = $row->count1;
		}
		
		return $count;
		
		
	}
	
	//Saving Last Price - Current Price
	function saving_lp_cp($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
		$sql="select sum(last_price_sum-current_price_sum2) as saving_lp_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category in('$category') 
		and substring(po_num,1,3) in('$po_type')
		and category not in('OTHERS','RAW MATERIALS')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and last_price_sum > current_price_sum2";
		
		} else {
			
		$sql="select sum(last_price_sum-current_price_sum2) as saving_lp_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC') 
		and category not in('OTHERS','RAW MATERIALS')
		and substring(po_num,1,3) in('$po_type')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and last_price_sum > current_price_sum2";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$saving = $row->saving_lp_cp;
		}
		
		//$saving = $last_price_sum-$current_price_sum;
		
		$saving_lakhs = $saving/100000;
		
		return number_format($saving_lakhs, 2, '.', '');
		
		
	}
	
	//Saving (Costing - Current Price)
	function saving_costing_cp($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
		$sql="select sum(costing_sum-current_price_sum1) as saving_costing_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category in('$category') 
		and substring(po_num,1,3) in('$po_type')
		and category not in('OTHERS','RAW MATERIALS')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and costing_sum > current_price_sum1";
		
		} else {
			
		$sql="select sum(costing_sum-current_price_sum1) as saving_costing_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category in('$category') 
		and substring(po_num,1,3) in('$po_type')
		and category not in('OTHERS','RAW MATERIALS')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and costing_sum > current_price_sum1";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$saving = $row->saving_costing_cp;
		}
		
		//$saving = $costing_sum-$current_price_sum;
		
		$saving_lakhs = $saving/100000;
		
		return number_format($saving_lakhs, 2, '.', '');
		
		
	}
	
	//Cost Over Run (Last Price - Current Price)
	function loss_lp_cp($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
		$sql="select sum(current_price_sum2-last_price_sum) as loss_lp_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category in('$category') 
		and substring(po_num,1,3) in('$po_type')
		and category not in('OTHERS','RAW MATERIALS')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and current_price_sum2 > last_price_sum";
		
		} else {
			
		$sql="select sum(current_price_sum2-last_price_sum) as loss_lp_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC') 
		and category not in('OTHERS','RAW MATERIALS')
		and substring(po_num,1,3) in('$po_type')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and current_price_sum2 > last_price_sum";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$loss = $row->loss_lp_cp;
		}
		
		//$loss = $current_price_sum-$last_price_sum;
		
		$loss_lakhs = $loss/100000;
		
		return number_format($loss_lakhs, 2, '.', '');
	}
	
	//Cost Over Run (Current Price-Costing)
	function loss_costing_cp($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		if($category != 'total'){
		
		$sql="select sum(current_price_sum1-costing_sum) as loss_costing_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category in('$category')
		and substring(po_num,1,3) in('$po_type') 
		and category not in('OTHERS','RAW MATERIALS')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and current_price_sum1 > costing_sum";
		
		} else {
			
		$sql="select sum(current_price_sum1-costing_sum) as loss_costing_cp from tipldb..cost_saving_report where po_erp_status in('OP','CL','SC')
		and category not in('OTHERS','RAW MATERIALS')
		and substring(po_num,1,3) in('$po_type')
		and convert(date,po_date) between '$from_date' and '$to_date'
		and current_price_sum1 > costing_sum";
			
		}
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$loss = $row->loss_costing_cp;
		}
		
		//$saving = $current_price_sum-$costing_sum;
		
		$loss_lakhs = $loss/100000;
		
		return number_format($loss_lakhs, 2, '.', '');
		
		
	}
	
	
	/*******************************************
		        COST SAVING REPORT DETAILS
	*******************************************/
	//NO of purchase orders
	function count_no_po_cat_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category in('$category')
		and substring(a.po_num,1,3) in('$po_type') 
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		order by a.po_num ";
		
		} else {
			
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC') 
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;	
	}
	
	//Sum No Of PO's
	function sum_no_po_cat_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC') 
		and a.category in('$category') 
		and substring(a.po_num,1,3) in('$po_type')
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		order by a.po_num ";
		
		} else {
			
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC') 
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and a.convert(date,po_date) between '$from_date' and '$to_date'
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;	
	}
	
	//Without Basis Details
	function without_basis_count_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category in('$category')
		and substring(a.po_num,1,3) in('$po_type') 
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.last_price_sum is null 
		and a.costing_sum is null
		order by a.po_num ";
		
		} else {
			
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.last_price_sum is null 
		and a.costing_sum is null
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;
	}
	
	
	//Saving (Last Price - Current Price)
	function saving_lp_cp_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category in('$category') 
		and substring(a.po_num,1,3) in('$po_type')
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.last_price_sum > a.current_price_sum2
		order by a.po_num ";
		
		} else {
			
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC') 
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.last_price_sum > a.current_price_sum2
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;
		
	}
	
	//Saving (Costing - Current Price)
	function saving_costing_cp_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category in('$category') 
		and substring(a.po_num,1,3) in('$po_type')
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.costing_sum > a.current_price_sum1
		order by a.po_num ";
		
		} else {
			
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.costing_sum > a.current_price_sum1
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;
	}
	
	//Loss (Last Price - Current Price)
	function loss_lp_cp_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category in('$category') 
		and substring(a.po_num,1,3) in('$po_type')
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.current_price_sum2 > a.last_price_sum
		order by a.po_num ";
		
		} else {
			
		$sql="select select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.current_price_sum2 > a.last_price_sum
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;
	}
	
	//Loss (Current Price-Costing)
	function loss_costing_cp_det($po_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($po_type == 'ALL'){
			$po_type = "CGP','FPO','IPO','LPO','SRV";	
		} else {
			$po_type = str_replace(",","','",$po_type);
		}
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NO.</td>
			<td>PO SUPP NAME</td>
			<td>CATEGORY</td>
			<td>PO TOTAL VALUE</td>
			<td>PO ERP STATUS</td>
			<td>PO CREATED BY</td>
			<td>PO CREATE DATE</td>
			<td>PR NUMBER</td>
			<td>PO QTY</td>
			<td>COSTING</td>
			<td>LAST PRICE</td>
			<td>CURRENT PRICE</td>
			<td>ITEM VALUE</td>
		</tr>';
		
		if($category != 'total'){
		
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category in('$category')
		and substring(a.po_num,1,3) in('$po_type') 
		and a.category not in('OTHERS','RAW MATERIALS')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.current_price_sum1 > a.costing_sum
		order by a.po_num ";
		
		} else {
			
		$sql="select a.po_num, a.supplier_name, a.category, a.po_basic_value, a.po_erp_status, a.created_by, a.po_date, 
		b.po_ipr_no, b.for_stk_quantity, b.costing, b.last_price, b.current_price, b.total_item_value 
		from tipldb..cost_saving_report a, tipldb..insert_po b 
		where a.po_num = b.po_num 
		and a.po_erp_status in('OP','CL','SC')
		and a.category not in('OTHERS','RAW MATERIALS')
		and substring(a.po_num,1,3) in('$po_type')
		and convert(date,a.po_date) between '$from_date' and '$to_date'
		and a.current_price_sum1 > a.costing_sum
		order by a.po_num ";
			
		}
		
		$qry = $ci->db->query($sql);
		$sno=0;
		$net_total=0;
		$costing_tot=0;
		$last_price_tot=0;
		$current_price_tot=0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$po_basic_value = $row->po_basic_value;
			$po_erp_status = $row->po_erp_status;
			$created_by = $row->created_by;
			$po_date = $row->po_date;
			//PR DETAILS
			$po_ipr_no = $row->po_ipr_no;
			$for_stk_quantity = $row->for_stk_quantity;
			$costing = $row->costing;
			$last_price = $row->last_price;
			$current_price = $row->current_price;
			$total_item_value = $row->total_item_value;
			//totals
			$costing_tot = $costing_tot+$costing;
			$last_price_tot = $last_price_tot+$last_price;
			$current_price_tot = $current_price_tot+$current_price;
			
			$net_total = $net_total+$po_basic_value;
			
		    $html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$supplier_name.'</td>
				<td>'.$category.'</td>
				<td>'.number_format($po_basic_value, 2, '.', '').'</td>
				<td>'.$po_erp_status.'</td>
				<td>'.$created_by.'</td>
				<td>'.substr($po_date, 0,11).'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$po_ipr_no.'" target="_blank">'.$po_ipr_no.'</a></td>
				<td>'.number_format($for_stk_quantity,2,".","").'</td>
				<td>'.number_format($costing,2,".","").'</td>
				<td>'.number_format($last_price,2,".","").'</td>
				<td>'.number_format($current_price,2,".","").'</td>
				<td>'.number_format($total_item_value,2,".","").'</td>
			</tr>';
		}
		
		$html.='
		<tr>
			<td colspan="10" style="background-color:#0CF; font-weight:bold; text-align:right">TOTAL</td>
			<td>'.number_format($costing_tot,2,'.','').'</td>
			<td>'.number_format($last_price_tot,2,'.','').'</td>
			<td>'.number_format($current_price_tot,2,'.','').'</td>
			<td style="background-color:#0CF; font-weight:bold"></td>
		</tr>';
		
		echo $html;
		
	}
	
	
	function saving_no_po_cat($category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category != 'total'){
		
			$sql="select sum(convert(decimal(16,2),b.last_price)) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and b.last_price is not null and a.costing is NULL
			and c.po_category = '$category'
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and convert(decimal(16,2),b.last_price) > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'
			
			UNION
			
			select sum(a.costing_new) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and a.costing_new is not null
			and c.po_category = '$category'
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and a.costing_new > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'";
		
		} else {
			
			$sql="select sum(convert(decimal(16,2),b.last_price)) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and b.last_price is not null and a.costing is NULL
			and c.po_category not in('OTHERS','RAW MATERIALS')
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and convert(decimal(16,2),b.last_price) > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'
			
			UNION
			
			select sum(a.costing_new) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and a.costing_new is not null
			and c.po_category not in('OTHERS','RAW MATERIALS')
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and a.costing_new > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		
		$last_price_sum_tot = 0;
		$current_price_sum_tot = 0;
		foreach($qry->result() as $row){
			$last_price_sum = $row->last_price_sum;
			$current_price_sum = $row->current_price_sum;
			
			$last_price_sum_tot=$last_price_sum_tot+$last_price_sum;
			$current_price_sum_tot=$current_price_sum_tot+$current_price_sum;
		}
		
		$saving = $last_price_sum_tot-$current_price_sum_tot;
		
		$saving_lacs = $saving/100000;
		
		$link = "&&category=".$category."&&from_date=".$from_date."&&to_date=".$to_date;
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det/?fun=saving_no_po_cat_det'.$link.'" target="_blank">'
		.number_format($saving_lacs, 2, '.', '').'</a>';	
	}
	
	function loss_no_po_cat($category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category != 'total'){
		
			$sql="select sum(convert(decimal(16,2),b.last_price)) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and b.last_price is not null and a.costing is NULL
			and c.po_category = '$category'
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and convert(decimal(16,2),b.last_price) < convert(decimal(16,2),b.current_price)
			and po_create_date between '$from_date' and '$to_date'
			
			UNION
			
			select sum(a.costing_new) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and a.costing_new is not null
			and c.po_category = '$category'
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and a.costing_new < convert(decimal(16,2),b.current_price)
			and convert(date,c.create_date) between '$from_date' and '$to_date'";
		
		} else {
			
			$sql="select sum(convert(decimal(16,2),b.last_price)) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and b.last_price is not null and a.costing is NULL
			and c.po_category not in('OTHERS','RAW MATERIALS')
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and convert(decimal(16,2),b.last_price) < convert(decimal(16,2),b.current_price)
			and po_create_date between '$from_date' and '$to_date'
			
			UNION
			
			select sum(a.costing_new) as last_price_sum, sum(convert(decimal(16,2),b.current_price)) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num
			and a.costing_new is not null
			and c.po_category not in('OTHERS','RAW MATERIALS')
			and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and a.costing_new < convert(decimal(16,2),b.current_price)
			and convert(date,c.create_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		
		$last_price_sum_tot = 0;
		$current_price_sum_tot = 0;
		foreach($qry->result() as $row){
			$last_price_sum = $row->last_price_sum;
			$current_price_sum = $row->current_price_sum;
			
			$last_price_sum_tot=$last_price_sum_tot+$last_price_sum;
			$current_price_sum_tot=$current_price_sum_tot+$current_price_sum;
		}
		
		$loss = $current_price_sum_tot-$last_price_sum_tot;
		
		$loss_lakhs = $loss/100000;
		
		$link = "&&category=".$category."&&from_date=".$from_date."&&to_date=".$to_date;
		
		echo '<a href="'.base_url().'index.php/pr_cycletimec/pr_report_det/?fun=loss_no_po_cat_det'.$link.'" target="_blank">'
		.number_format($loss_lakhs, 2, '.', '').'</a>';
				
	}
	
	/*******************************************
	******NO OF PO WITH COSTING DETAILS*********
	*******************************************/
	
	function saving_no_po_cat_det($category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NUMBER</td>
			<td>SUPP NAME</td>
			<td>PO VALUE</td>
			<td>PO CREATE DATE</td>
			<td>PO CREATED BY</td>
			<td>LVL1 APPROVAL BY</td>
			<td>LVL2 APPROVAL BY</td>
			<td>LVL3 APPROVAL BY</td>
			<td>PO ERP STATUS</td>
			<td>COSTING SUM</td>
			<td>LAST PRICE SUM</td>
			<td>CURRENT PRICE SUM</td>
		</tr>
		';
		
		if($category != 'TOTAL'){
		
			$sql="select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no 
			and b.po_num = c.po_num
			and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC')
			and b.last_price is not null and a.costing is NULL
			and c.po_category = '$category'
			and convert(decimal(16,2),b.last_price) > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'
			
			UNION
			
			select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no 
			and b.po_num = c.po_num
			and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC')
			and a.costing_new is not null
			and c.po_category = '$category'
			and a.costing_new > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'";
		
		} else {
			
			$sql="select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no 
			and b.po_num = c.po_num
			and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC')
			and b.last_price is not null and a.costing is NULL
			and c.po_category not in('OTHERS','RAW MATERIALS')
			and convert(decimal(16,2),b.last_price) > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'
			
			UNION
			
			select select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum 
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d
			where a.pr_num = b.po_ipr_no 
			and b.po_num = c.po_num
			and c.po_num = d.pomas_pono
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC')
			and a.costing_new is not null
			and c.po_category not in('OTHERS','RAW MATERIALS')
			and a.costing_new > convert(decimal(16,2),b.current_price)
			and convert(date,c.po_create_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		
		$sno = 0;
		$po_total = 0;
		$costing_tot = 0;
		$last_price_tot =0;
		$current_price_tot = 0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_total_value = $row->pomas_pobasicvalue;
			$po_create_date = $row->po_create_date;
			$created_by = $row->created_by;
			$level1_approval_by = $row->level1_approval_by;
			$level2_approval_by = $row->level2_approval_by;
			$level3_approval_by = $row->level3_approval_by;
			$pomas_podocstatus = $row->pomas_podocstatus;
			$costing_sum = $row->costing_sum;
			$last_price_sum = $row->last_price_sum;
			$current_price_sum = $row->current_price_sum;
			
			$po_total = $po_total+$po_total_value;
			
			$costing_tot = $costing_tot+$costing_sum;
			$last_price_tot = $last_price_tot+$last_price_sum;
			$current_price_tot = $current_price_tot+$current_price_sum;
			
			$html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.number_format($po_total_value, 2, '.', '').'</td>
				<td>'.substr($po_create_date,0,11).'</td>
				<td>'.$created_by.'</td>
				<td>'.$level1_approval_by.'</td>
				<td>'.$level2_approval_by.'</td>
				<td>'.$level3_approval_by.'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$costing_sum.'</td>
				<td>'.$last_price_sum.'</td>
				<td>'.$current_price_sum.'</td>
			</tr>
			';
		}
		
		$html.='
		<tr>
			<td colspan="3" style="text-align:right; font-weight:bold; background-color:#0CF">TOTAL</td>
			<td>'.$po_total.'</td>
			<td colspan="6" style="background-color:#0CF"></td>
			<td>'.$costing_tot.'</td>
			<td>'.$last_price_tot.'</td>
			<td>'.$current_price_tot.'</td>
		</tr>
		';
		
		echo $html;	
	}
	
	function loss_no_po_cat_det($category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		$html='
		<tr style="background-color:#0CF; font-weight:bold">
			<td>SNO</td>
			<td>PO NUMBER</td>
			<td>SUPP NAME</td>
			<td>PO VALUE</td>
			<td>PO CREATE DATE</td>
			<td>PO CREATED BY</td>
			<td>LVL1 APPROVAL BY</td>
			<td>LVL2 APPROVAL BY</td>
			<td>LVL3 APPROVAL BY</td>
			<td>PO ERP STATUS</td>
			<td>COSTING SUM</td>
			<td>LAST PRICE SUM</td>
			<td>CURRENT PRICE SUM</td>
		</tr>
		';
		
		if($category != 'TOTAL'){
		
			$sql="select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and b.last_price is not null and a.costing is NULL and c.po_category = '$category' 
			and convert(decimal(16,2),b.last_price) < convert(decimal(16,2),b.current_price) 
			and po_create_date between '$from_date ' and '$to_date' 
			UNION 
			select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and a.costing_new is not null and c.po_category = '$category' and a.costing_new < convert(decimal(16,2),b.current_price) 
			and convert(date,c.create_date) between '$from_date' and '$to_date' ";
		
		} else {
			
			$sql="select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono)
			and d.pomas_podocstatus in('OP','CL','SC') 
			and b.last_price is not null and a.costing is NULL  
			and convert(decimal(16,2),b.last_price) < convert(decimal(16,2),b.current_price) 
			and po_create_date between '$from_date ' and '$to_date' 
			UNION 
			select c.po_num,c.po_supp_name,d.pomas_pobasicvalue,c.po_create_date,d.pomas_podocstatus,c.created_by,c.level1_approval_by, 
			c.level2_approval_by,c.level3_approval_by,
			(select sum(convert(decimal(16,2),costing_new)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as costing_sum,
			(select sum(convert(decimal(16,2),last_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as last_price_sum,
			(select sum(convert(decimal(16,2),current_price)) from tipldb..pr_submit_table e, tipldb..insert_po f where e.pr_num = f.po_ipr_no 
			and f.po_num = c.po_num) as current_price_sum
			from tipldb..pr_submit_table a, tipldb..insert_po b, tipldb..po_master_table c, scmdb..po_pomas_pur_order_hdr d 
			where a.pr_num = b.po_ipr_no and b.po_num = c.po_num and c.po_num = d.pomas_pono 
			and d.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = d.pomas_pono) 
			and d.pomas_podocstatus in('OP','CL','SC')
			and a.costing_new is not null and a.costing_new < convert(decimal(16,2),b.current_price) 
			and convert(date,c.create_date) between '$from_date' and '$to_date'";
			
		}
		
		$qry = $ci->db->query($sql);
		
		$sno = 0;
		$po_total = 0;
		$costing_tot = 0;
		$last_price_tot =0;
		$current_price_tot = 0;
		foreach($qry->result() as $row){
			$sno++;
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_total_value = $row->pomas_pobasicvalue;
			$po_create_date = $row->po_create_date;
			$created_by = $row->created_by;
			$level1_approval_by = $row->level1_approval_by;
			$level2_approval_by = $row->level2_approval_by;
			$level3_approval_by = $row->level3_approval_by;
			$pomas_podocstatus = $row->pomas_podocstatus;
			$costing_sum = $row->costing_sum;
			$last_price_sum = $row->last_price_sum;
			$current_price_sum = $row->current_price_sum;
			
			$po_total = $po_total+$po_total_value;
			
			$costing_tot = $costing_tot+$costing_sum;
			$last_price_tot = $last_price_tot+$last_price_sum;
			$current_price_tot = $current_price_tot+$current_price_sum;
			
			$html.='
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.number_format($po_total_value, 2, '.', '').'</td>
				<td>'.substr($po_create_date,0,11).'</td>
				<td>'.$created_by.'</td>
				<td>'.$level1_approval_by.'</td>
				<td>'.$level2_approval_by.'</td>
				<td>'.$level3_approval_by.'</td>
				<td>'.$pomas_podocstatus.'</td>
				<td>'.$costing_sum.'</td>
				<td>'.$last_price_sum.'</td>
				<td>'.$current_price_sum.'</td>
			</tr>
			';
		}
		
		$html.='
		<tr>
			<td colspan="3" style="text-align:right; font-weight:bold; background-color:#0CF">TOTAL</td>
			<td>'.$po_total.'</td>
			<td colspan="6" style="background-color:#0CF"></td>
			<td>'.$costing_tot.'</td>
			<td>'.$last_price_tot.'</td>
			<td>'.$current_price_tot.'</td>
		</tr>
		';
		
		echo $html;
		//echo $sql;
				
	}
	
	/***************************************************************************
					PR Nee Date Vs Actual Report 
	***************************************************************************/
	
	function count_cases($case_type,$pr_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
			
		if($case_type == 'TOT'){
			$case_type = "BT','D','OT";
		}
	
		$sql = "select count(*) as count1 from tipldb..need_date_vs_actual_report where category in('$category')
		and substring(pr_num,1,3) in('$pr_type') and convert(date,need_date) between '$from_date' and '$to_date'
		and in_column in('$case_type')";
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$count = $row->count1;
		}
		
		return $count;
		
	}
	
	//COUNT OPEN CASES
	function count_cases_open($case_type,$pr_type,$category){
		$ci =& get_instance();
		$ci->load->database();
			
		if($case_type == 'TOT'){
			$case_type = "D','OT";
		}
	
		$sql = "select count(*) as count1 from tipldb..need_date_vs_actual_report_open where category in('$category')
		and substring(pr_num,1,3) in('$pr_type') and in_column in('$case_type')";
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$count = $row->count1;
		}
		
		return $count;
		
	}
	
	//Total
	function count_cases_tot($case_type,$pr_type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'GT'){
			$category = "AVAZONIC','CAPITAL GOODS','INSTRUMENTATION','IT','NON PRODUCTION CONSUMABLES','PACKING','PRODUCTION CONSUMABLES','SECURITY','SENSORS','TOOLS";
		}
		
		if($pr_type == 'ALL'){
			$pr_type ="CGP','FPR','IPR";
		}
		
		if($case_type == 'TOT'){
			$case_type = "BT','D','OT";
		}
		
		$sql = "select count(*) as count1 from tipldb..need_date_vs_actual_report where category in('$category')
		and substring(pr_num,1,3) in('$pr_type') and convert(date,need_date) between '$from_date' and '$to_date'
		and in_column in('$case_type')";
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$count = $row->count1;
		}
		
		return  $count;
		
	}
	
	//TOTAL OPEN CASES
	function count_cases_tot_open($case_type,$pr_type,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'GT'){
			$category = "AVAZONIC','CAPITAL GOODS','INSTRUMENTATION','IT','NON PRODUCTION CONSUMABLES','PACKING','PRODUCTION CONSUMABLES','SECURITY','SENSORS','TOOLS";
		}
		
		if($pr_type == 'ALL'){
			$pr_type ="CGP','FPR','IPR";
		}
		
		if($case_type == 'TOT'){
			$case_type = "D','OT";
		}
		
		$sql = "select count(*) as count1 from tipldb..need_date_vs_actual_report_open where category in('$category')
		and substring(pr_num,1,3) in('$pr_type') and in_column in('$case_type')";
		
		$qry = $ci->db->query($sql);
		
		foreach($qry->result() as $row){
			$count = $row->count1;
		}
		
		return  $count;
		
	}
	//Details
	function count_cases_det($case_type,$type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
			
		if($case_type == 'TOT'){
			$case_type = "BT','D','OT";
		}
		
		$html = '
			<tr style="background-color:#0CF; font-weight:bold">
				<td>SNO</td>
				<td>PR NUM</td>
				<td>ITEM CODE</td>
				<td>ITEM DESC</td>
				<td>PR NEED DATE</td>
				<td>PR QTY</td>
				<td>CATEGORY</td>
				<td>PR CREATED BY</td>
				<td>PR CREATE DATE</td>
				<td>PO NUM</td>
				<td>SUPPLIER NAME</td>
				<td>PO CREATED BY</td>
				<td>PO CREATE DATE</td>
				<td>GATE ENTRY NO</td>
				<td>GATE ENTRY DATE</td>
				<td>CARRIER NAME</td>
				<td>GRCPT NO</td>
				<td>GRCPT STATUS</td>
				<td>GRCPT CREATE DATE</td>
				<td>GRCPT FR DATE</td>
				<td>GRCPT FA DATE</td>
				<td>GRCPT FM DATE</td>
				<td>PR NEED DATE - GRCPT FR DATE</td>
			</tr>
		';
		
	
		$sql = "select * from tipldb..need_date_vs_actual_report where category in('$category')
		and substring(pr_num,1,3) in('$type') and convert(date,need_date) between '$from_date' and '$to_date'
		and in_column in('$case_type') order by pr_num";
		
		$qry = $ci->db->query($sql);
		
		$sno = 0;
		foreach($qry->result() as $row){
			$sno++;
			$pr_num = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->item_desc;
			$need_date = substr($row->need_date,0,11);
			$pr_qty = $row->pr_qty;
			$category = $row->category;
			$pr_created_by = $row->pr_created_by;
			$pr_create_date = substr($row->pr_create_date,0,11);
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_need_date = substr($row->po_need_date,0,11);
			$po_created_by = $row->po_created_by;
			$po_create_date = substr($row->po_create_date,0,11);
			$grcpt_no = $row->grcpt_no;
			$grcpt_status = $row->grcpt_status;
			$gate_entry_no = $row->gate_entry_no;
			$gate_entry_date = substr($row->gate_entry_date,0,11);
			$carrier_name = $row->carrier_name;
			$grcpt_create_date = substr($row->grcpt_create_date,0,11);
			$grcpt_fr_date = substr($row->grcpt_fr_date,0,11);
			$grcpt_fa_date = substr($row->grcpt_fa_date,0,11);
			$grcpt_fm_date = substr($row->grcpt_fm_date,0,11);
			$diff_days = $row->diff_days;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false)
			{
			$item_code2 = str_replace("%2F","chandra",$item_code1);
			}
			else 
			{
			$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			$html .= '
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
				<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$item_desc.'</td>
				<td>'.$need_date.'</td>
				<td>'.$pr_qty.'</td>
				<td>'.$category.'</td>
				<td>'.$pr_created_by.'</td>
				<td>'.$pr_create_date.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.$po_created_by.'</td>
				<td>'.$po_create_date.'</td>
				<td>'.$gate_entry_no.'</td>
				<td>'.$gate_entry_date.'</td>
				<td>'.$carrier_name.'</td>
				<td>'.$grcpt_no.'</td>
				<td>'.$grcpt_status.'</td>
				<td>'.$grcpt_create_date.'</td>
				<td>'.$grcpt_fr_date.'</td>
				<td>'.$grcpt_fa_date.'</td>
				<td>'.$grcpt_fm_date.'</td>
				<td>'.$diff_days.'</td>
			</tr>';
		
		}
		
		echo $html;
		
	}
	
	//Details OPEN CASES
	function count_cases_det_open($case_type,$type,$category){
		$ci =& get_instance();
		$ci->load->database();
			
		if($case_type == 'TOT'){
			$case_type = "D','OT";
		}
		
		$html = '
			<tr style="background-color:#0CF; font-weight:bold">
				<td>SNO</td>
				<td>PR NUM</td>
				<td>ITEM CODE</td>
				<td>ITEM DESC</td>
				<td>PR NEED DATE</td>
				<td>PR QTY</td>
				<td>CATEGORY</td>
				<td>PR CREATED BY</td>
				<td>PR CREATE DATE</td>
				<td>PO NUM</td>
				<td>SUPPLIER NAME</td>
				<td>PO CREATED BY</td>
				<td>PO CREATE DATE</td>
				<td>PR NEED DATE - CURRENT DATE</td>
			</tr>
		';
		
	
		$sql = "select * from tipldb..need_date_vs_actual_report_open where category in('$category')
		and substring(pr_num,1,3) in('$type') and in_column in('$case_type') order by pr_num";
		
		$qry = $ci->db->query($sql);
		
		$sno = 0;
		foreach($qry->result() as $row){
			$sno++;
			$pr_num = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->item_desc;
			$need_date = substr($row->need_date,0,11);
			$pr_qty = $row->pr_qty;
			$category = $row->category;
			$pr_created_by = $row->pr_created_by;
			$pr_create_date = substr($row->pr_create_date,0,11);
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_need_date = substr($row->po_need_date,0,11);
			$po_created_by = $row->po_created_by;
			$po_create_date = substr($row->po_create_date,0,11);
			$diff_days = $row->diff_days;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false)
			{
			$item_code2 = str_replace("%2F","chandra",$item_code1);
			}
			else 
			{
			$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			$html .= '
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
				<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$item_desc.'</td>
				<td>'.$need_date.'</td>
				<td>'.$pr_qty.'</td>
				<td>'.$category.'</td>
				<td>'.$pr_created_by.'</td>
				<td>'.$pr_create_date.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.$po_created_by.'</td>
				<td>'.$po_create_date.'</td>
				<td>'.$diff_days.'</td>
			</tr>';
		
		}
		
		echo $html;
		
	}
	
	//Total
	function count_cases_tot_det($case_type,$type,$category,$from_date,$to_date){
		$ci =& get_instance();
		$ci->load->database();
		
		$html = '
			<tr style="background-color:#0CF; font-weight:bold">
				<td>SNO</td>
				<td>PR NUM</td>
				<td>ITEM CODE</td>
				<td>ITEM DESC</td>
				<td>PR NEED DATE</td>
				<td>PR QTY</td>
				<td>CATEGORY</td>
				<td>PR CREATED BY</td>
				<td>PR CREATE DATE</td>
				<td>PO NUM</td>
				<td>SUPPLIER NAME</td>
				<td>PO CREATED BY</td>
				<td>PO CREATE DATE</td>
				<td>GATE ENTRY NO</td>
				<td>GATE ENTRY DATE</td>
				<td>CARRIER NAME</td>
				<td>GRCPT NO</td>
				<td>GRCPT STATUS</td>
				<td>GRCPT CREATE DATE</td>
				<td>GRCPT FR DATE</td>
				<td>GRCPT FA DATE</td>
				<td>GRCPT FM DATE</td>
				<td>PR NEED DATE - GRCPT FR DATE</td>
			</tr>
		';
		
		if($category == 'GT'){
			$category = "AVAZONIC','CAPITAL GOODS','INSTRUMENTATION','IT','NON PRODUCTION CONSUMABLES','PACKING','PRODUCTION CONSUMABLES','SECURITY','SENSORS','TOOLS";
		}
		
		if($type == 'ALL'){
			$type ="CGP','FPR','IPR";
		}
		
		if($case_type == 'TOT'){
			$case_type = "B','D','OT";
		}
		
		$sql = "select * from tipldb..need_date_vs_actual_report where category in('$category')
		and substring(pr_num,1,3) in('$type') and convert(date,need_date) between '$from_date' and '$to_date'
		and in_column in('$case_type') order by pr_num";
		
		$qry = $ci->db->query($sql);
		
		$sno = 0;
		foreach($qry->result() as $row){
			$sno++;
			$pr_num = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->item_desc;
			$need_date = substr($row->need_date,0,11);
			$pr_qty = $row->pr_qty;
			$category = $row->category;
			$pr_created_by = $row->pr_created_by;
			$pr_create_date = substr($row->pr_create_date,0,11);
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_need_date = substr($row->po_need_date,0,11);
			$po_created_by = $row->po_created_by;
			$po_create_date = substr($row->po_create_date,0,11);
			$grcpt_no = $row->grcpt_no;
			$grcpt_status = $row->grcpt_status;
			$gate_entry_no = $row->gate_entry_no;
			$gate_entry_date = substr($row->gate_entry_date,0,11);
			$carrier_name = $row->carrier_name;
			$grcpt_create_date = substr($row->grcpt_create_date,0,11);
			$grcpt_fr_date = substr($row->grcpt_fr_date,0,11);
			$grcpt_fa_date = substr($row->grcpt_fa_date,0,11);
			$grcpt_fm_date = substr($row->grcpt_fm_date,0,11);
			$diff_days = $row->diff_days;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false)
			{
			$item_code2 = str_replace("%2F","chandra",$item_code1);
			}
			else 
			{
			$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			$html .= '
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
				<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$item_desc.'</td>
				<td>'.$need_date.'</td>
				<td>'.$pr_qty.'</td>
				<td>'.$category.'</td>
				<td>'.$pr_created_by.'</td>
				<td>'.$pr_create_date.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.$po_created_by.'</td>
				<td>'.$po_create_date.'</td>
				<td>'.$gate_entry_no.'</td>
				<td>'.$gate_entry_date.'</td>
				<td>'.$carrier_name.'</td>
				<td>'.$grcpt_no.'</td>
				<td>'.$grcpt_status.'</td>
				<td>'.$grcpt_create_date.'</td>
				<td>'.$grcpt_fr_date.'</td>
				<td>'.$grcpt_fa_date.'</td>
				<td>'.$grcpt_fm_date.'</td>
				<td>'.$diff_days.'</td>
			</tr>';
		
		}
		
		echo $html;
		
	}
	
	//Total OPEN CASES
	function count_cases_tot_det_open($case_type,$type,$category){
		$ci =& get_instance();
		$ci->load->database();
		
		$html = '
			<tr style="background-color:#0CF; font-weight:bold">
				<td>SNO</td>
				<td>PR NUM</td>
				<td>ITEM CODE</td>
				<td>ITEM DESC</td>
				<td>PR NEED DATE</td>
				<td>PR QTY</td>
				<td>CATEGORY</td>
				<td>PR CREATED BY</td>
				<td>PR CREATE DATE</td>
				<td>PO NUM</td>
				<td>SUPPLIER NAME</td>
				<td>PO CREATED BY</td>
				<td>PO CREATE DATE</td>
				<td>PR NEED DATE - CURRENT DATE</td>
			</tr>
		';
		
		if($category == 'GT'){
			$category = "AVAZONIC','CAPITAL GOODS','INSTRUMENTATION','IT','NON PRODUCTION CONSUMABLES','PACKING','PRODUCTION CONSUMABLES','SECURITY','SENSORS','TOOLS";
		}
		
		if($type == 'ALL'){
			$type ="CGP','FPR','IPR";
		}
		
		if($case_type == 'TOT'){
			$case_type = "D','OT";
		}
		
		$sql = "select * from tipldb..need_date_vs_actual_report_open where category in('$category')
		and substring(pr_num,1,3) in('$type') and in_column in('$case_type') order by pr_num";
		
		$qry = $ci->db->query($sql);
		
		$sno = 0;
		foreach($qry->result() as $row){
			$sno++;
			$pr_num = $row->pr_num;
			$item_code = $row->item_code;
			$item_desc = $row->item_desc;
			$need_date = substr($row->need_date,0,11);
			$pr_qty = $row->pr_qty;
			$category = $row->category;
			$pr_created_by = $row->pr_created_by;
			$pr_create_date = substr($row->pr_create_date,0,11);
			$po_num = $row->po_num;
			$po_supp_name = $row->po_supp_name;
			$po_need_date = substr($row->po_need_date,0,11);
			$po_created_by = $row->po_created_by;
			$po_create_date = substr($row->po_create_date,0,11);
			$diff_days = $row->diff_days;
			
			//Encoding Item Code
			$item_code1 = urlencode($item_code);
				
			if(strpos($item_code1, '%2F') !== false)
			{
			$item_code2 = str_replace("%2F","chandra",$item_code1);
			}
			else 
			{
			$item_code2 = $item_code1;
			}
			//Encoding Item Code Ends
			
			$html .= '
			<tr>
				<td>'.$sno.'</td>
				<td><a href="'.base_url().'index.php/iprc/view_ipr/'.$pr_num.'" target="_blank">'.$pr_num.'</a></td>
				<td><a href="'.base_url().'index.php/createpoc/pendal_view/'.$item_code2.'" target="_blank">'.$item_code.'</a></td>
				<td>'.$item_desc.'</td>
				<td>'.$need_date.'</td>
				<td>'.$pr_qty.'</td>
				<td>'.$category.'</td>
				<td>'.$pr_created_by.'</td>
				<td>'.$pr_create_date.'</td>
				<td><a href="'.base_url().'index.php/po_history_itemwisec/po_details/'.$po_num.'">'.$po_num.'</a></td>
				<td>'.$po_supp_name.'</td>
				<td>'.$po_created_by.'</td>
				<td>'.$po_create_date.'</td>
				<td>'.$diff_days.'</td>
			</tr>';
		
		}
		
		echo $html;
		
	}
	
	/**********************************************************/
	/**********************************************************/
	/*************Goods Receipt Cycletime Report***************/
	/**********************************************************/
	/**********************************************************/
	
	function gr_age_monthwise($category,$po_type,$start_date,$end_date,$first_col,$second_col){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		$sql_age_sum = "select avg(convert(decimal(8,2),datediff(hour,$first_col,$second_col))) as avg_age 
		from tipldb..ge_to_grcptfm_cycletime 
		where category in('".$category."') 
		and substring(order_no,1,3) in('".$po_type."') 
		and $first_col is not null 
		and $second_col is not null
		and convert(date,ge_create_date) between '".$start_date."' and '".$end_date."'";
		
		
		$qry_age_sum = $ci->db->query($sql_age_sum);
		
		foreach ($qry_age_sum->result() as $row) {
		  $avg_age = $row->avg_age;
		}
		
		return number_format($avg_age,2,".","");
	}
	
	//Age less than 4 hours
	function gr_open_age_less($category,$po_type,$first_col,$second_col){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		//Fresh
		if($second_col == 'ge_rec_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null 
			and ge_status = 'Fresh'
			and datediff(hour,$first_col,getdate()) <= 4";
		
		//Received	
		} else if($second_col == 'ge_send_to_pur_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null 
			--and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Received')
			and datediff(hour,$first_col,getdate()) <= 4";
		
		//Send To Accounts	
		} else if($second_col == 'ge_send_to_acc_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			--and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is NULL
			and ge_status in('Pending For HSN Check By Purchaser')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For GRCPT	
		} else if($second_col == 'grcpt_create_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is NULL
			and ge_status in('Pending For GRCPT')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For Freeze Receipt	
		} else if($second_col == 'grcpt_fr_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is not NULL
			and grcpt_status = 'FR'";
		
		//Pending For Inspection Live	
		} else if($second_col == 'grcpt_live_insp_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'
			and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')";
		
		//Pending For Inspection ERP	
		} else if($second_col == 'grcpt_erp_insp_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'";
		
		//Pending For Movement ERP	
		} else if($second_col == 'grcpt_erp_mov_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is not NULL
			and grcpt_status = 'FA'";
			
		} else if($second_col == 'live_rej_check_pur_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and grcpt_no is not NULL
			and grcpt_status = 'FM'
			and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
			and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
			and ge_status in('Inspection Done')";
			
		} else if($second_col == 'erp_gretn_create_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)";
			
		} else if($second_col == 'live_gretn_create_date') {
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
			and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))";
			
		} else {
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) <= 4";
			
		}
		
		$qry_count = $ci->db->query($sql_count);
		
		foreach ($qry_count->result() as $row) {
		  $count = $row->count1;
		}
		
		return $count;
	}
	
	//Age Greater Than 4 Hours
	function gr_open_age_greater($category,$po_type,$first_col,$second_col){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		//Fresh
		if($second_col == 'ge_rec_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null
			and ge_status = 'Fresh'
			and datediff(hour,$first_col,getdate()) > 4";
		
		//Received	
		} else if($second_col == 'ge_send_to_pur_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null 
			--and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Received')
			and datediff(hour,$first_col,getdate()) > 4";
		
		//Send to Accounts	
		} else if($second_col == 'ge_send_to_acc_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			--and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is NULL
			and ge_status in('Pending For HSN Check By Purchaser')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For GRCPT	
		} else if($second_col == 'grcpt_create_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is NULL
			and ge_status in('Pending For GRCPT')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For Freeze Receipt	
		} else if($second_col == 'grcpt_fr_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is not NULL
			and grcpt_status = 'FR'";
		
		//Pending For Inspection Live	
		} else if($second_col == 'grcpt_live_insp_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'
			and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')";
		
		//Pending For Inspection ERP	
		} else if($second_col == 'grcpt_erp_insp_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'";
		
		//Pending For Movement ERP	
		} else if($second_col == 'grcpt_erp_mov_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is not NULL
			and grcpt_status = 'FA'";
			
		} else if($second_col == 'live_rej_check_pur_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and grcpt_no is not NULL
			and grcpt_status = 'FM'
			and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
			and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
			and ge_status in('Inspection Done')";
			
		} else if($second_col == 'erp_gretn_create_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)";
			
		} else if($second_col == 'live_gretn_create_date') {
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
			and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))";
			
		} else {
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and datediff(hour,$first_col,getdate()) > 4";
			
		}
		
		
		$qry_count = $ci->db->query($sql_count);
		
		foreach ($qry_count->result() as $row) {
		  $count = $row->count1;
		}
		
		return $count;
	}
	
	//Total Cases 
	function gr_open_age_total($category,$po_type,$first_col,$second_col){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		//Fresh
		if($second_col == 'ge_rec_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null
			and ge_status = 'Fresh'";
			
		//Received	
		} else if($second_col == 'ge_send_to_pur_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null 
			--and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Received')";
			
		//Send To Accounts	
		} else if($second_col == 'ge_send_to_acc_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			--and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Pending For HSN Check By Purchaser')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For GRCPT	
		} else if($second_col == 'grcpt_create_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Pending For GRCPT') 
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For Freeze Receipt	
		} else if($second_col == 'grcpt_fr_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FR'";
		
		//Pending For Inspection Live	
		} else if($second_col == 'grcpt_live_insp_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'
			and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')";
		
		//Pending For Inspection ERP	
		} else if($second_col == 'grcpt_erp_insp_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'";
		
		//Pending For Movement ERP	
		} else if($second_col == 'grcpt_erp_mov_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FA'";
			
		} else if($second_col == 'live_rej_check_pur_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FM'
			and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
			and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
			and ge_status in('Inspection Done')";
			
		} else if($second_col == 'erp_gretn_create_date'){
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)";
			
		} else if($second_col == 'live_gretn_create_date') {
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
			and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))";
			
		} else {
			
			$sql_count = "select count(*) as count1 from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null";
			
		}
		
		
		$qry_count = $ci->db->query($sql_count);
		
		foreach ($qry_count->result() as $row) {
		  $count = $row->count1;
		}
		
		return $count;
		//echo $sql_count;
	}
	
	//Total Cases Average
	function gr_open_age_total_avg($category,$po_type,$first_col,$second_col){
		$ci =& get_instance();
		$ci->load->database();
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		//Fresh
		if($second_col == 'ge_rec_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null 
			and ge_status = 'Fresh'";
		
		//Received	
		} else if($second_col == 'ge_send_to_pur_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where $first_col is not null 
			--and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Received')";
			
		//Send To Accounts	
		} else if($second_col == 'ge_send_to_acc_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			--and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Pending For HSN Check By Purchaser')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For GRCPT	
		} else if($second_col == 'grcpt_create_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is NULL
			and ge_status in('Pending For GRCPT')
			and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))";
		
		//Pending For Freeze Receipt	
		} else if($second_col == 'grcpt_fr_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FR'";
		
		//Pending For Inspection Live	
		} else if($second_col == 'grcpt_live_insp_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'
			and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')";
		
		//Pending For Inspection ERP	
		} else if($second_col == 'grcpt_erp_insp_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FZ'";
		
		//Pending For Movement ERP	
		} else if($second_col == 'grcpt_erp_mov_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FA'";
			
		} else if($second_col == 'live_rej_check_pur_date'){
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and grcpt_no is not NULL
			and grcpt_status = 'FM'
			and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
			and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
			and ge_status in('Inspection Done')";
			
		} else if($second_col == 'erp_gretn_create_date'){ 
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
			and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)";
			
		} else if($second_col == 'live_gretn_create_date') {
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null
			and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
			and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))";
			
		} else {
			
			$sql_avg_age = "select avg(convert(decimal(8,2),datediff(hour,$first_col,getdate()))) as avg_age from tipldb..ge_to_grcptfm_cycletime 
			where category in('$category') 
			and substring(order_no,1,3) in('$po_type') 
			and $first_col is not null 
			and $second_col is null";
			
		}
		
		
		$qry_avg_age = $ci->db->query($sql_avg_age);
		
		foreach ($qry_avg_age->result() as $row) {
		  $avg_age = $row->avg_age;
		}
		
		if($avg_age == 0){
			$avg_age1 = 0.00;
		} else {
			$avg_age1 = $avg_age;
		}
		
		return number_format($avg_age1,2,".","");
		//echo $sql_avg_age;
		//echo $second_col;
	}
	
	function gr_age_monthwise_tot_det($category,$po_type,$start_date,$end_date){
		$ci =& get_instance();
		$ci->load->database();
		
		$html="
		<tr style='background-color:#CCC'>
			<td><b>SNO</b></td>
			<td><b>GATE ENTRY NO</b></td>
			<td><b>PO NUMBER</b></td>
			<td><b>PO STATUS</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>GRCPT NO</b></td>
			<td><b>GRCPT STATUS</b></td>
			<td><b>GE STATUS</b></td>
			<td><b>GRETN NO</b></td>
		</tr>
		";
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		$sql_age_sum = "select * 
		from tipldb..ge_to_grcptfm_cycletime 
		where category in('".$category."') 
		and substring(order_no,1,3) in('".$po_type."')
		and convert(date,ge_create_date) between '".$start_date."' and '".$end_date."' order by ge_no desc";
		
		
		$qry_age_sum = $ci->db->query($sql_age_sum);
		
		$sno=0;
		foreach ($qry_age_sum->result() as $row) {
			$sno++;
			$ge_no = $row->ge_no;
			$order_no = $row->order_no;
			$order_curr_stat = $row->order_curr_stat;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$grcpt_no = $row->grcpt_no;
			$grcpt_status = $row->grcpt_status;
			$ge_status = $row->ge_status;
			$gretn_no = $row->gretn_no;
			
			$html.="
			<tr>
				<td>".$sno."</td>
				<td>".$ge_no."</td>
				<td>".$order_no."</td>
				<td>".$order_curr_stat."</td>
				<td>".$supplier_name."</td>
				<td>".$category."</td>
				<td>".$grcpt_no."</td>
				<td>".$grcpt_status."</td>
				<td>".$ge_status."</td>
				<td>".$gretn_no."</td>
			</tr>
			";
			
		}
		
		echo $html;
	}
	
	//Open Age Greater Than 4 hours Cases
	function gr_open_cases_greater_det($category,$po_type){
		$ci =& get_instance();
		$ci->load->database();
		
		$html="
		<tr style='background-color:#CCC'>
			<td><b>SNO</b></td>
			<td><b>GATE ENTRY NO</b></td>
			<td><b>GATE ENTRY CREATE DATE</b></td>
			<td><b>PO NUMBER</b></td>
			<td><b>PO STATUS</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>GRCPT NO</b></td>
			<td><b>GRCPT STATUS</b></td>
			<td><b>GE STATUS</b></td>
			<td><b>GRETN NO</b></td>
			<td><b>PENDING ON DEPT.</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>AGE FROM GATE ENTRY(HOURS)</b></td>
		</tr>
		";
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		$sql_age_sum = "select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where ge_create_date is not null
		and datediff(hour,ge_create_date,getdate()) > 4 
		and grcpt_no is NULL
		and ge_status = 'Fresh'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime 
		where ge_rec_date is not null 
		--and ge_send_to_pur_date is null
		and datediff(hour,ge_rec_date,getdate()) > 4 
		and grcpt_no is NULL
		and ge_status in('Received')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and ge_send_to_pur_date is not null 
		--and ge_send_to_acc_date is null
		and datediff(hour,ge_send_to_pur_date,getdate()) > 4 
		and grcpt_no is NULL 
		and ge_status in('Pending For HSN Check By Purchaser')
		and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and ge_send_to_acc_date is not null and grcpt_create_date is null
		and datediff(hour,ge_send_to_acc_date,getdate()) > 4
		and grcpt_no is NULL 
		and ge_status in('Pending For GRCPT')
		and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_create_date is not null and grcpt_fr_date is null
		and datediff(hour,grcpt_create_date,getdate()) > 4 
		and grcpt_no is not NULL and grcpt_status = 'FR'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_fr_date is not null and grcpt_live_insp_date is null
		and datediff(hour,grcpt_fr_date,getdate()) > 4
		and grcpt_no is not NULL and grcpt_status = 'FZ'
		and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_live_insp_date is not null and grcpt_erp_insp_date is null 
		and datediff(hour,grcpt_live_insp_date,getdate()) > 4
		and grcpt_no is not NULL and grcpt_status = 'FZ'
		and ge_status in('Inspection Done')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_erp_insp_date is not null and grcpt_erp_mov_date is null
		and datediff(hour,grcpt_erp_insp_date,getdate()) > 4
		and grcpt_no is not NULL and grcpt_status = 'FA'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_erp_mov_date is not null and live_rej_check_pur_date is null 
		and datediff(hour,grcpt_erp_mov_date,getdate()) > 4
		and grcpt_no is not NULL and grcpt_status = 'FM' 
		and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
					and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
		and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
		and ge_status in('Inspection Done')
		 
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and live_rej_check_pur_date is not null and erp_gretn_create_date is null
		and datediff(hour,live_rej_check_pur_date,getdate()) > 4
		and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
		and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and erp_gretn_create_date is not null and live_gretn_create_date is null
		and datediff(hour,erp_gretn_create_date,getdate()) > 4
		and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
		and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and live_gretn_create_date is not null and gretn_pur_update_date is null
		and datediff(hour,live_gretn_create_date,getdate()) > 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_pur_update_date is not null and greten_storehead_app_date is null
		and datediff(hour,gretn_pur_update_date,getdate()) > 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and greten_storehead_app_date is not null and gretn_store_upload_doc_date is null
		and datediff(hour,greten_storehead_app_date,getdate()) > 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_store_upload_doc_date is not null and gretn_packing_date is null
		and datediff(hour,gretn_store_upload_doc_date,getdate()) > 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_packing_date is not null and gretn_gate_exit_date is null
		and datediff(hour,gretn_packing_date,getdate()) > 4 order by ge_no desc";
		
		
		$qry_age_sum = $ci->db->query($sql_age_sum);
		
		$sno=0;
		foreach ($qry_age_sum->result() as $row) {
			$sno++;
			$ge_no = $row->ge_no;
			$ge_create_date = $row->ge_create_date;
			$order_no = $row->order_no;
			$order_curr_stat = $row->order_curr_stat;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$grcpt_no = $row->grcpt_no;
			$grcpt_status = $row->grcpt_status;
			$ge_status = $row->ge_status;
			$gretn_no = $row->gretn_no;
			$pend_dept = $row->pend_dept;
			$pend_on = $row->pend_on;
			$age = number_format($row->diff,2,".","");
			$age_ge = number_format($row->diff1,2,".","");
			$for_status = $row->for_status;
			
			$html.="
			<tr>
				<td>".$sno."</td>
				<td>".$ge_no."</td>
				<td>".$ge_create_date."</td>
				<td>".$order_no."</td>
				<td>".$order_curr_stat."</td>
				<td>".$supplier_name."</td>
				<td>".$category."</td>
				<td>".$grcpt_no."</td>
				<td>".$grcpt_status."</td>
				<td>".$for_status."</td>
				<td>".$gretn_no."</td>
				<td>".$pend_dept."</td>
				<td style='text-transform:uppercase'>".$pend_on."</td>
				<td>".$age."</td>
			</tr>
			";
			
		}
		
		echo $html;
	}
	
	//Open Age Greater Than 4 hours Cases
	function gr_open_cases_less_det($category,$po_type){
		$ci =& get_instance();
		$ci->load->database();
		
		$html="
		<tr style='background-color:#CCC'>
			<td><b>SNO</b></td>
			<td><b>GATE ENTRY NO</b></td>
			<td><b>GATE ENTRY CREATE DATE</b></td>
			<td><b>PO NUMBER</b></td>
			<td><b>PO STATUS</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>GRCPT NO</b></td>
			<td><b>GRCPT STATUS</b></td>
			<td><b>GE STATUS</b></td>
			<td><b>GRETN NO</b></td>
			<td><b>PENDING ON DEPT.</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>AGE FROM GATE ENTRY(HOURS)</b></td>
		</tr>
		";
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		$sql_age_sum = "
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where ge_create_date is not null
		and datediff(hour,ge_create_date,getdate()) <= 4 
		and grcpt_no is NULL
		and ge_status = 'Fresh'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime 
		where ge_rec_date is not null 
		--and ge_send_to_pur_date is null
		and datediff(hour,ge_rec_date,getdate()) <= 4 
		and grcpt_no is NULL
		and ge_status in('Received')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and ge_send_to_pur_date is not null 
		--and ge_send_to_acc_date is null
		and datediff(hour,ge_send_to_pur_date,getdate()) <= 4 
		and grcpt_no is NULL 
		and ge_status in('Pending For HSN Check By Purchaser')
		and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and ge_send_to_acc_date is not null and grcpt_create_date is null
		and datediff(hour,ge_send_to_acc_date,getdate()) <= 4
		and grcpt_no is NULL 
		and ge_status in('Pending For GRCPT')
		and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_create_date is not null and grcpt_fr_date is null
		and datediff(hour,grcpt_create_date,getdate()) <= 4 
		and grcpt_no is not NULL and grcpt_status = 'FR'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_fr_date is not null and grcpt_live_insp_date is null
		and datediff(hour,grcpt_fr_date,getdate()) <= 4
		and grcpt_no is not NULL and grcpt_status = 'FZ'
		and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_live_insp_date is not null and grcpt_erp_insp_date is null 
		and datediff(hour,grcpt_live_insp_date,getdate()) <= 4
		and grcpt_no is not NULL and grcpt_status = 'FZ'
		and ge_status in('Inspection Done')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_erp_insp_date is not null and grcpt_erp_mov_date is null
		and datediff(hour,grcpt_erp_insp_date,getdate()) <= 4
		and grcpt_no is not NULL and grcpt_status = 'FA'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_erp_mov_date is not null and live_rej_check_pur_date is null 
		and datediff(hour,grcpt_erp_mov_date,getdate()) <= 4
		and grcpt_no is not NULL and grcpt_status = 'FM' 
		and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
					and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
		and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
		and ge_status in('Inspection Done')
		 
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and live_rej_check_pur_date is not null and erp_gretn_create_date is null
		and datediff(hour,live_rej_check_pur_date,getdate()) <= 4
		and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
		and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and erp_gretn_create_date is not null and live_gretn_create_date is null
		and datediff(hour,erp_gretn_create_date,getdate()) <= 4
		and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
		and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and live_gretn_create_date is not null and gretn_pur_update_date is null
		and datediff(hour,live_gretn_create_date,getdate()) <= 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_pur_update_date is not null and greten_storehead_app_date is null
		and datediff(hour,gretn_pur_update_date,getdate()) <= 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and greten_storehead_app_date is not null and gretn_store_upload_doc_date is null
		and datediff(hour,greten_storehead_app_date,getdate()) <= 4
		
		union
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_store_upload_doc_date is not null and gretn_packing_date is null
		and datediff(hour,gretn_store_upload_doc_date,getdate()) <= 4
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_packing_date is not null and gretn_gate_exit_date is null
		and datediff(hour,gretn_packing_date,getdate()) <= 4 order by ge_no desc";
		
		$qry_age_sum = $ci->db->query($sql_age_sum);
		
		$sno=0;
		foreach ($qry_age_sum->result() as $row) {
			$sno++;
			$ge_no = $row->ge_no;
			$ge_create_date = $row->ge_create_date;
			$order_no = $row->order_no;
			$order_curr_stat = $row->order_curr_stat;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$grcpt_no = $row->grcpt_no;
			$grcpt_status = $row->grcpt_status;
			$ge_status = $row->ge_status;
			$gretn_no = $row->gretn_no;
			$pend_dept = $row->pend_dept;
			$pend_on = $row->pend_on;
			$age = number_format($row->diff,2,".","");
			$age_ge = number_format($row->diff1,2,".","");
			$for_status = $row->for_status;
			
			$html.="
			<tr>
				<td>".$sno."</td>
				<td>".$ge_no."</td>
				<td>".$ge_create_date."</td>
				<td>".$order_no."</td>
				<td>".$order_curr_stat."</td>
				<td>".$supplier_name."</td>
				<td>".$category."</td>
				<td>".$grcpt_no."</td>
				<td>".$grcpt_status."</td>
				<td>".$for_status."</td>
				<td>".$gretn_no."</td>
				<td>".$pend_dept."</td>
				<td style='text-transform:uppercase'>".$pend_on."</td>
				<td>".$age."</td>
			</tr>
			";
			
		}
		
		echo $html;
	}
	
	//All Open Cases
	function gr_open_cases_all_det($category,$po_type){
		$ci =& get_instance();
		$ci->load->database();
		
		$html="
		<tr style='background-color:#CCC'>
			<td><b>SNO</b></td>
			<td><b>GATE ENTRY NO</b></td>
			<td><b>GATE ENTRY CREATE DATE</b></td>
			<td><b>PO NUMBER</b></td>
			<td><b>PO STATUS</b></td>
			<td><b>SUPPLIER NAME</b></td>
			<td><b>CATEGORY</b></td>
			<td><b>GRCPT NO</b></td>
			<td><b>GRCPT STATUS</b></td>
			<td><b>GE STATUS</b></td>
			<td><b>GRETN NO</b></td>
			<td><b>PENDING ON DEPT.</b></td>
			<td><b>PENDING ON</b></td>
			<td><b>AGE FROM GATE ENTRY(HOURS)</b></td>
		</tr>
		";
		
		if($category == 'ALL_CATEGORY'){
			$category = "PACKING','SECURITY','PRODUCTION CONSUMABLES','SENSORS','CAPITAL GOODS','IT','TOOLS','INSTRUMENTATION','NON PRODUCTION CONSUMABLES','AVAZONIC";
		}
		
		if($po_type == 'ALL_PO'){
			$po_type = "FPO','IPO','LPO','CGP";
		}
		
		$sql_age_sum = "select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where ge_create_date is not null 
		and grcpt_no is NULL
		and ge_status = 'Fresh'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime 
		where ge_rec_date is not null 
		--and ge_send_to_pur_date is null 
		and grcpt_no is NULL
		and ge_status in('Received')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and ge_send_to_pur_date is not null 
		--and ge_send_to_acc_date is null 
		and grcpt_no is NULL 
		and ge_status in('Pending For HSN Check By Purchaser')
		and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and ge_send_to_acc_date is not null and grcpt_create_date is null
		and grcpt_no is NULL 
		and ge_status in('Pending For GRCPT')
		and order_no in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_create_date is not null and grcpt_fr_date is null 
		and grcpt_no is not NULL and grcpt_status = 'FR'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_fr_date is not null and grcpt_live_insp_date is null
		and grcpt_no is not NULL and grcpt_status = 'FZ'
		and ge_status in('GRCPT_Created','GRCPT Receipt Pending For Inspection')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_live_insp_date is not null and grcpt_erp_insp_date is null 
		and grcpt_no is not NULL and grcpt_status = 'FZ'
		and ge_status in('Inspection Done')
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status  
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_erp_insp_date is not null and grcpt_erp_mov_date is null
		and grcpt_no is not NULL and grcpt_status = 'FA'
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and grcpt_erp_mov_date is not null and live_rej_check_pur_date is null 
		and grcpt_no is not NULL and grcpt_status = 'FM' 
		and ge_no in(select entry_no from tipldb..gateentry_item_rec_table where entry_no = tipldb..ge_to_grcptfm_cycletime.ge_no
					and po_num = tipldb..ge_to_grcptfm_cycletime.order_no and insp_rej_qty > 0) 
		and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
		and ge_status in('Inspection Done')
		 
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status  
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and live_rej_check_pur_date is not null and erp_gretn_create_date is null
		and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts') 
		and grcpt_no not in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain)
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and erp_gretn_create_date is not null and live_gretn_create_date is null
		and ge_status in('Pending For Warehouse Rejection By Accounts','Pending For Re-GRCPT By Accounts')
		and grcpt_no in(select distinct grn_hdr_outpassno from scmdb..grn_hdr_grnmain where grn_hdr_grnstatus in('FR'))
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and live_gretn_create_date is not null and gretn_pur_update_date is null
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_pur_update_date is not null and greten_storehead_app_date is null
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and greten_storehead_app_date is not null and gretn_store_upload_doc_date is null
		
		union
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status 
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_store_upload_doc_date is not null and gretn_packing_date is null
		
		union
		
		select ge_no,order_no,order_curr_stat,supplier_name,category,grcpt_no,grcpt_status,gretn_no,pend_dept,pend_on
		,datediff(HOUR,ge_create_date,getdate()) as diff,ge_status,ge_create_date,datediff(HOUR,ge_create_date,getdate()) as diff1
		,for_status
		from tipldb..ge_to_grcptfm_cycletime where category in('".$category."') and substring(order_no,1,3) in('".$po_type."') 
		and gretn_packing_date is not null and gretn_gate_exit_date is null order by ge_no desc";
		
		
		$qry_age_sum = $ci->db->query($sql_age_sum);
		
		$sno=0;
		foreach ($qry_age_sum->result() as $row) {
			$sno++;
			$ge_no = $row->ge_no;
			$ge_create_date = $row->ge_create_date;
			$order_no = $row->order_no;
			$order_curr_stat = $row->order_curr_stat;
			$supplier_name = $row->supplier_name;
			$category = $row->category;
			$grcpt_no = $row->grcpt_no;
			$grcpt_status = $row->grcpt_status;
			$ge_status = $row->ge_status;
			$gretn_no = $row->gretn_no;
			$pend_dept = $row->pend_dept;
			$pend_on = $row->pend_on;
			$age = number_format($row->diff,2,".","");
			$age_ge = number_format($row->diff1,2,".","");
			$for_status = $row->for_status;
			
			$html.="
			<tr>
				<td>".$sno."</td>
				<td>".$ge_no."</td>
				<td>".$ge_create_date."</td>
				<td>".$order_no."</td>
				<td>".$order_curr_stat."</td>
				<td>".$supplier_name."</td>
				<td>".$category."</td>
				<td>".$grcpt_no."</td>
				<td>".$grcpt_status."</td>
				<td>".$for_status."</td>
				<td>".$gretn_no."</td>
				<td>".$pend_dept."</td>
				<td style='text-transform:uppercase'>".$pend_on."</td>
				<td>".$age."</td>
			</tr>
			";
			
		}
		
		echo $html;
	}
			
}