<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dbuserc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('dbuserm');
	}
	
	//Reports
	public function index(){ 
		$this->load->view('admin/Dbuserview'); 
	}

	public function RegisterUser(){
		$data = array();
		$data['UserReg'] = $this->dbuserm->UserReg($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}

	public function ViewList(){
		$data['ViewList'] = $this->dbuserm->ListView($data);
		$data['ViewHead'] = $this->dbuserm->ListHead($data);
		
		$this->load->view('admin/ListView', $data);
	}
	
}
