<?php
class Pr_reportc extends CI_Controller{  
   
	function __construct(){
		parent::__construct(); 
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('pr_reportm');
		user_act();
	}
	
	public function index(){
		$data['all_pr']=$this->pr_reportm->all_pr();
		$this->load->view('admin/pr_report_main', $data); 
	}
	
	public function pr_ajax(){
		$data['all_pr']=$this->pr_reportm->all_pr();
		$this->load->view('admin/pr_history_ajax', $data); 
	}
	
	//Age Report
	
	public function pr_report_main_new(){
		$data['pr_report_proc']=$this->pr_reportm->pr_report_proc();
		$this->load->view('admin/pr_report_main_new', $data); 
	}
	
	public function pr_ajax_new(){
		$data['all_pr']=$this->pr_reportm->all_pr(); 
		$this->load->view('admin/pr_history_ajax_new', $data); 
	}
	
	public function pr_report_main_new_excel_export(){
		$this->load->view('admin/pr_report_main_new_excel_export'); 
	}

}

