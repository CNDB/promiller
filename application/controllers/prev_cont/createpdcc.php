<?php
class Createpdcc extends CI_Controller{ 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('createpdcm');
		$this->load->helper(array('form'));
		user_act();
	}
	
	public function index(){     
		$data['h']=$this->createpdcm->select_prqit_prno();
		$this->load->view('admin/create_pdc', $data); 
	} 
	
	public function create_po(){  
		$data['h']=$this->createpdcm->select_prqit_prno();
		$this->load->view('admin/create_pdc', $data);  
	}
	
	public function view_po(){
		$selectpr = $_REQUEST['q'];
		$data['max_amend_no'] = $this->createpdcm->max_amend_no($selectpr);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		$data['view_po']=$this->createpdcm->view_poqit_pono($selectpr, $amend_no);
		$this->load->view('admin/view_pdc',$data);
	} 
	
	public function insert_po_sub(){
		$config['upload_path']   = './uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|';
		$config['max_size']      = 100000000; 
		$config['max_width']     = 102400; 
		$config['max_height']    = 768000; 
		$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
		$filename = strtolower($_FILES["uploaded_check_img"]["name"]);
		$fileExe  = substr($filename, strrpos($filename,'.'));
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		$file = basename($filename, ".".$ext);
		$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
		$NewFileName2 = $NewFileName.'_'.$RandNumber.$fileExe;
		$config['file_name'] = $NewFileName2;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('uploaded_check_img')){
			$error = array('error' => $this->upload->display_errors());
		}else { 
			$data2 = array('upload_data2' => $this->upload->data());
		}
		
		$data = array();
		$this->createpdcm->insert_po_sub($data, $NewFileName2);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_po_sub');
	}
}