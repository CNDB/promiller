<?php
class Createpoc_lvl1 extends CI_Controller{   
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('createpom_lvl1');
		user_act();
	}
	
	public function index(){    
		$data['h']=$this->createpom_lvl1->select_prqit_prno();
		$this->load->view('admin/create_pr_lvl1', $data); 
	} 
	
	/******* Create Purchase Order Controller *******/
	public function create_pr(){  
		$data['h']=$this->createpom_lvl1->select_prqit_prno();
		$this->load->view('admin/create_pr_lvl1', $data);  
	}
	
	/********** View Purchase Order Controller ************/
	public function view_pr(){
		$selectpr = $_REQUEST['q'];
		$data['v']=$this->createpom_lvl1->view_prqit_prno($selectpr);
		foreach($data['v']->result_array() AS $row){
			$sup_code = $row['prqit_itemcode'];
		}
		
		$sup_code1 = urldecode($sup_code);
		$data['p']=$this->createpom_lvl1->procesdure_run($sup_code);
		$this->db->close();
		$this->db->initialize();
		$data['r']=$this->createpom_lvl1->pendal_info($sup_code);
		$data['month']= $this->createpom_lvl1->pendal_info_monthwise($sup_code);
		$data['last5'] = $this->createpom_lvl1->pendal_info_last5_trans($sup_code);
		$data['reorder'] = $this->createpom_lvl1->pendal_info_reorder_lvl_qty($sup_code);
		$data['monthofinvtry'] = $this->createpom_lvl1->monthofinvtry($sup_code);
		$data['orderinadvnce'] = $this->createpom_lvl1->orderinadvnce($sup_code);
		$data['wrkordr_ser']=$this->createpom_lvl1->wrkordr_ser($sup_code);
		$data['lstyrcumnrec1'] = $this->createpom_lvl1->lstyrcumnrec1($sup_code);
		$data['lastfivetrans1'] = $this->createpom_lvl1->lastfivetrans1($sup_code);
		$data['itemdescmaster'] = $this->createpom_lvl1->itemdescmaster($sup_code1);
		$data['mnthwsecurryrcons1'] = $this->createpom_lvl1->pendal_master_mnthwsecurryrcons($sup_code1);
		$data['mnthwsecurryrrcpt1'] = $this->createpom_lvl1->pendal_master_mnthwsecurryrrcpt($sup_code1);
		$data['supplier1'] = $this->createpom_lvl1->pendal_master_supplier($sup_code1);
		$data['bomdetail1'] = $this->createpom_lvl1->pendal_master_bomdetail($sup_code1);
		$data['prerplive'] = $this->createpom_lvl1->prerplive($selectpr);
		$this->load->view('admin/view_pr_lvl1',$data);
	}	
	
	public function cost_sheet(){
		$data['cost_sheet']=$this->createpom_lvl1->cost_sheet();
		$this->load->view('admin/cost_sheet', $data);
	}  
	  
}
