<?php
class Po_history_itemwisec extends CI_Controller{  
   
   	function __construct(){
		
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('po_history_itemwisem');
		 
		 user_act();
	 }
	 
     public function index(){
		     
		 $data['all_po']=$this->po_history_itemwisem->all_po();
		  
         $this->load->view('admin/po_history_itemwise_main', $data); 
      }
	  
	 public function po_details(){
		   
		 $po_num = $this->uri->segment(3);
		 
		 $data['view_po']=$this->po_history_itemwisem->view_po($po_num);  
		  
         $this->load->view('admin/po_details', $data); 
     } 
	  
	  
}

