<?php
class Rightc extends CI_Controller{  

	function __construct(){
		parent::__construct(); 
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('rightm');
		user_act();
	}
	
	public function index(){  
		$data['emp_list']=$this->rightm->emp_list();
		$this->load->view('admin/right_view', $data); 
	}
	
	public function right_view_details(){   
		$this->load->view('admin/right_view_details', $data); 
	}
	
	public function right_submit(){
		$data = array();
		$this->rightm->right_submit($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/right_submit'); 
	}
	
}

