<?php
class Mrp_reportc extends CI_Controller{  
   
   	 function __construct(){
		 parent::__construct();  
		 $this->load->helper('url');	  
		 $this->load->database(); 	  
		 $this->load->model('mrp_reportm');
		 user_act();
	 }
	 
	 // MRP REPORT PAGE
     public function index(){ 
		 $data['h']=$this->mrp_reportm->mrp_procedure();
		 $this->db->close();
		 $this->db->initialize();
		 $create_date = date('Y-m-d H:i:s');
		 $data['mrp_records_entry']=$this->mrp_reportm->mrp_records_entry($create_date); 
         $this->load->view('admin/mrp_report_main', $data); 
     }
	 
	 //MRP AJAX PAGE
	 public function mrp_report_ajax(){   
         $this->load->view('admin/mrp_report_ajax'); 
     }
	 
	 //MRP ITEM VIEW PAGE
	 public function mrp_item_view(){
	 	    $itemcode1 = $this->uri->segment(3);
			//Pendal Card Details Starts
			if(strpos($itemcode1, 'chandra') !== false){
				$item_code2 = str_replace("chandra","%2F",$itemcode1);
			}else{
				$item_code2 = $itemcode1;
			}
			
			$itemcode = rawurldecode($item_code2);
			$data['p']=$this->mrp_reportm->procesdure_run($itemcode);
			$this->db->close();
			$this->db->initialize();
			
			$data['pendal'] = $this->mrp_reportm->pendal_master($itemcode);
			$data['pendalstkblnc'] = $this->mrp_reportm->pendal_master_whstkblnc($itemcode);
			$data['pndposopr'] = $this->mrp_reportm->pendal_master_pndposopr($itemcode);
			$data['pndposopr_new'] = $this->mrp_reportm->pendal_master_pndposopr_new($itemcode);
			$data['allocat'] = $this->mrp_reportm->pendal_master_allocat($itemcode);
			$data['allocat_tot'] = $this->mrp_reportm->pendal_master_allocat_tot($itemcode);
			$data['wipdet'] = $this->mrp_reportm->pendal_master_wipdet($itemcode);
			$data['lastfivetrans'] = $this->mrp_reportm->pendal_master_lastfivetrans($itemcode);
			$data['lstyrcumnrec'] = $this->mrp_reportm->pendal_master_lstyrcumnrec($itemcode);
			$data['supplier'] = $this->mrp_reportm->pendal_master_supplier($itemcode);
			$data['mnthwsecurryrcons'] = $this->mrp_reportm->pendal_master_mnthwsecurryrcons($itemcode);
			$data['mnthwsecurryrrcpt'] = $this->mrp_reportm->pendal_master_mnthwsecurryrrcpt($itemcode);
			$data['bomdetail'] = $this->mrp_reportm->pendal_master_bomdetail($itemcode);
			//Pendal Card Details
            $this->load->view('admin/mrp_item_view',$data); 
     }
	 
	 //MRP ITEM ENTRY PAGE
	 public function mrp_item_entry(){
         $data = array();
		 $this->mrp_reportm->mrp_item_entry();
		 $data['message'] = 'Data Inserted Successfully';
		 $this->load->view('admin/mrp_item_entry'); 
     }
	 
	 //SPECIAL ITEM PAGES STARTS
	 
	 //MRP ITEM VIEW PAGE
	 public function mrp_spcl_item_view(){    
         $this->load->view('admin/mrp_spcl_item_view'); 
     }
	 
	 //ITEM DETAILS SEARCH PAGE
	 public function search(){   
         $this->load->view('admin/search'); 
     }
	 
	 //ATAC DETAILS SEARCH PAGE AJAX
	 public function atac_details_mrp(){   
         $this->load->view('admin/mrp_atac_details'); 
     }
	 
	 //ITEM DETAILS SEARCH PAGE AJAX
	 public function mrp_item_details_ajax(){  
         $this->load->view('admin/mrp_item_details_ajax'); 
     }
	 
	 //MRP ITEM ENTRY PAGE
	 public function mrp_spcl_item_entry(){   
         $data = array();
		 $this->mrp_reportm->mrp_spcl_item_entry();
		 $data['message'] = 'Data Inserted Successfully';
		 $this->load->view('admin/mrp_item_entry'); 
     }
	 //SPECIAL ITEM PAGES ENDS
	 
	 //MRP DETAILS ENTRY REPORT FRESH
	 public function mrp_item_report(){   
	 	 $data['mrp_item_report'] = $this->mrp_reportm->mrp_item_report();
		 $this->load->view('admin/mrp_item_report',$data); 
     }
	 
	 //DELETE ENTRY
	 public function delete_item_view(){   
	 	 $sno = $_REQUEST['id'];
		 $data['delete_item_view'] = $this->mrp_reportm->delete_item_view($sno);
		 $this->load->view('admin/mrp_delete_item_view',$data); 
     }
	 
	 public function delete_item_entry(){   
	 	 $sno = $_REQUEST['sno'];
		 $data = array();
		 $this->mrp_reportm->delete_item_entry($sno);
		 $this->load->view('admin/mrp_delete_item_entry'); 
     }
	 
	 //MRP View Page Average Items
	 public function mrp_view_avg_itm(){
		 $this->load->view('admin/mrp_view_avg_itm'); 	 
	 }
	 
	 public function mrp_live_category(){
		 $this->load->view('admin/mrp_live_category');	 
	 }
	 
	 public function mrp_reorder_update(){
		$this->load->view('admin/mrp_reorder_update'); 	 
	 }
	 
	 public function itm_stk_na(){ 
		$this->load->view('admin/mrp_item_stock_na'); 	 
	 }
	 
	 public function itm_stk_na_det(){ 
		$this->load->view('admin/mrp_item_stk_na_det'); 	 
	 }
	 
	 public function mrp_disapp_pr(){ 
		$this->load->view('admin/mrp_disapp_pr'); 	 
	 }	 
}

