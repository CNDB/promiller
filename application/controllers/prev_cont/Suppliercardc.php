<?php
class Suppliercardc extends CI_Controller{  

	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('suppliercardm');
		user_act();
	}
	
	public function index(){ 
		$this->load->view('admin/supplier_card_main'); 
	} 
	
	public function suppliercard_det(){
		$supp_code = $_REQUEST['supp_code'];
		$supp_code = trim($supp_code);
		
		//supplier card procedure
		$data['p']=$this->suppliercardm->procesdure_run($supp_code);
		$this->db->close();
		$this->db->initialize();
		//die; 
		 
		$this->load->view('admin/supplier_card_detail',$data); 
	} 
	
}