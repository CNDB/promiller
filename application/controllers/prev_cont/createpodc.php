<?php
   class Createpodc extends CI_Controller{  
   
   	 function __construct(){
	    parent::__construct();
	    $this->load->helper('url');
	    $this->load->database();
	    $this->load->model('createpodm');
		user_act();
	 }
	 
     public function index(){   
		$data['h']=$this->createpodm->select_prqit_prno();
        $this->load->view('admin/create_po', $data); 
      } 
	   
	  /******* Create Purchase Request Controller *******/
	  
	  public function create_po(){
		 $data['h']=$this->createpodm->select_prqit_prno();
		 $this->load->view('admin/create_po', $data);  
      }
	  
	  /******* View Purchase Request Controller *******/
	  
	  public function view_po(){
	    $selectpr = $_REQUEST['q'];
		$data['max_amend_no'] = $this->createpodm->max_amend_no($selectpr);
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
	    }
	    $data['v']=$this->createpodm->view_poqit_pono($selectpr, $amend_no);
		$data['act_timing'] = $this->createpodm->act_timing($selectpr);
	    $sup_code = array();
		
	    foreach($data['v']->result_array() AS $row){
			 $sup_code[] = $row['poitm_itemcode'];
			 $pr_num     = $row['poprq_prno'];	 
	    }
		
		$arrlength = count($sup_code);
		for($i=0; $i<$arrlength; $i++){
			$sup_code1 = urldecode($sup_code[$i]);
			$data['p']=$this->createpodm->procesdure_run($sup_code[$i]);
			$this->db->close();
			$this->db->initialize();
		}
	  	$this->load->view('admin/view_po',$data);
	  }
	  
	  /****** PO View Page New ******/
	  public function view_po_new(){
		$selectpr = $this->uri->segment(3);
		$data['max_amend_no'] = $this->createpodm->max_amend_no($selectpr);
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
	    }
		
	    $data['view_po']=$this->createpodm->view_poqit_pono($selectpr, $amend_no);
		$data['act_timing'] = $this->createpodm->act_timing($selectpr);
	    $sup_code = array();
	  	$this->load->view('admin/view_po_new',$data);
	  }
	  
	  /************ PO Query Submit Table ***********/
	  public function insert_po_sub(){
		$this->load->helper(array('form'));
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$user_name = $_SESSION['username'];
		$from_email = $_SESSION['username']."@tipl.com";
		$po_num = $this->input->post('po_num');
		
		if($po_num != ''){
			$pm_group = $this->input->post("pm_group");
			$category = $this->input->post("category");
			$category1 = $this->input->post("category1");
			$base_url  = base_url();
			$current_date = date("Y-m-d H:i:s");
			$po_type = substr($po_num,0,3);
			
			//Service PO Condition
			if($po_type == 'SRV'){
			 $category1 = $this->input->post("spo_category");
			}
			
			//Without Category PO
			if($category1 == ''){
			 echo "Puchase Order Without Category Cannot Be Created. Contact With IT";
			 die;
			}
			
			$data['master_data']=$this->createpodm->master_data($category1, $po_type);
			foreach($data['master_data']->result_array() AS $row){
				$lvl1_approval_by = $row['lvl1_approval_by'];
				$lvl2_approval_by = $row['lvl2_approval_by'];
				$lvl1_email = $row['lvl1_email'];
				$lvl2_email = $row['lvl2_email'];
				$lvl1_app_req = $row['lvl1_app_req'];
				$lvl2_app_req = $row['lvl2_app_req'];
				$mail_header_line = $row['mail_header_line'];
				$this->email->to($lvl1_email);
			}
			
			$created_by = $_SESSION['username'];
			
			$data['creator_email']=$this->createpodm->creator_email($created_by);
			foreach($data['creator_email']->result_array() AS $row) {
				$creator_email = $row['emp_email'];	
			}
					  
			$cc_email = "purchase@tipl.com,".$creator_email;
			$po_link = $base_url."index.php/createpodc/create_po_lvl1/";	  
			$this->email->from($from_email, $user_name);
			$this->email->cc($cc_email);
			$this->email->bcc('it.software@tipl.com');	 
			$this->email->subject($po_num.'- PO Created In Live');	 
			
			$po_ipr_no = $this->input->post('po_ipr_no');
			$array_count = count($po_ipr_no);
			$po_date = $this->input->post('po_date');
			$supp_name = $this->input->post('po_supp_name');
			$supp_name = str_replace("'","",$supp_name);
			$item_code = $this->input->post('po_item_code');
			$item_desc = $this->input->post('item_desc');
			$carrier_name = $this->input->post('carrier_name');
			$freight_terms = $this->input->post('freight');
			$payment_terms = $this->input->post('payterm');
			$last_yr_cons = $this->input->post('lastyr_cons');
			$current_stk = $this->input->post('current_stk');
			$reservation_qty = $this->input->post('reservation_qty');
			$for_stk_qty = $this->input->post('for_stk_quantity');
			$uom = $this->input->post('po_uom');
			$last_price = $this->input->post('last_price');
			$current_price = $this->input->post('current_price');
			$po_total_value = $this->input->post('po_total_value');
			$po_remarks = $this->input->post('spcl_inst_supp');
			$ipr_need_date = $this->input->post('ipr_need_date');
			$supplier_avg_lead_time = $this->input->post('supplier_avg_lead_time');
			$current_yr_con = $this->input->post('current_yr_con');
			$reorder_level = $this->input->post('iou_reorderlevel');
			$reorder_qty = $this->input->post('iou_reorderqty');
			$last_supplier = $this->input->post('SuppName');
			$last_supplier = str_replace("'","",$last_supplier);
			$last_supplier_lead_time = $this->input->post('SuppLeadTime');
			$last_supplier_rate = $this->input->post('SuppRate');
			$type_of_pr = $this->input->post('ipr_type');
			$project_name = $this->input->post('project_name');
			$atac_no = $this->input->post('atac_no');
			$color = $this->input->post('color');
			$costing = $this->input->post('costing');
			$costing_currency = $this->input->post('costing_currency');
			$why_spcl_rmks = $this->input->post('why_spcl_rmks');
			$today_date = date("Y-m-d H:i:s");
			$current_year = substr($today_date,0,4);
			$next_year = $current_year+1;
			$last_year = $current_year-1;
			$current_fin_year = $current_year."-".substr($next_year,2,2);
			$last_fin_year = $last_year."-".substr($current_year,2,2);
			 
			//page fetched values
			$purchase_order = "<b>PO No. - ".$po_num."</b><br />";
			
			$po_table = " <table border='1' cellpadding='2' cellspacing='0'>
							<tr>
								<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
							</tr>
						</table><br/><br/>
						<table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:11px; padding:10px; border-radius:5px;'>
							<tr style='background-color:#CFF'>
								<td><b>Supplier Name</b></td>
								<td><b>Need Date</b></td>
								<td><b>Lead Time (AVG)</b></td>
								<td><b>Item Code</b></td>
								<td><b>Item Description</b></td>
								<td><b>Carrier Name</b></td>
								<td><b>Freight Terms</b></td>
								<td><b>Payment Terms</b></td>
								<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
								<td><b>Current Stock</b></td>
								<td><b>Reservation Qty</b></td>
								<td><b>For Stk Qty</b></td>
								<td><b>UOM</b></td>
								<td><b>Last Price</b></td>
								<td><b>Current Price</b></td>
								<td><b>Costing</b></td>
								<td><b>Costing Currency</b></td>
								<td><b>Category</b></td>
								<td><b>PM-Group</b></td>
								<td><b>Current Year Cons</b></td>
								<td><b>Reorder Level</b></td>
								<td><b>Reorder Qty</b></td>
								<td><b>Last Supplier</b></td>
								<td><b>Last Supplier Lead Time</b></td>
								<td><b>Last Supplier Rate</b></td>
								<td><b>Type Of PR</b></td>
								<td><b>Why Special Remarks</b></td>
								<td><b>Project Name</b></td>
								<td><b>ATAC No</b></td>
								<td><b>PO Value</b></td>
								<td><b>Remarks</b></td>
							</tr>";
						
						for($i=0; $i<$array_count; $i++){
						
				$po_table .="<tr>
								<td>".$supp_name."</td>
								<td style='background-color:".$color[$i]."'>".$ipr_need_date[$i]."</td>
								<td>".number_format($supplier_avg_lead_time[$i],2)."</td>
								<td>".$item_code[$i]."</td>
								<td>".$item_desc[$i]."</td>
								<td>".$carrier_name."</td>
								<td>".$freight_terms."</td>
								<td>".$payment_terms."</td>
								<td>".number_format($last_yr_cons[$i],2)."</td>
								<td>".number_format($current_stk[$i],2)."</td>
								<td>".number_format($reservation_qty[$i],2)."</td>
								<td>".number_format($for_stk_qty[$i],2)."</td>
								<td>".$uom[$i]."</td>
								<td>".number_format($last_price[$i],2)."</td>
								<td>".number_format($current_price[$i],2)."</td>
								<td>".number_format($costing[$i],2)."</td>
								<td>".$costing_currency[$i]."</td>
								<td>".$category[$i]."</td>
								<td>".$pm_group[$i]."</td>
								<td>".number_format($current_yr_con[$i],2)."</td>
								<td>".number_format($reorder_level[$i],2)."</td>
								<td>".number_format($reorder_qty[$i],2)."</td>
								<td>".$last_supplier[$i]."</td>
								<td>".number_format($last_supplier_lead_time[$i],2)."</td>
								<td>".number_format($last_supplier_rate[$i],2)."</td>
								<td>".$type_of_pr[$i]."</td>
								<td>".$why_spcl_rmks[$i]."</td>
								<td>".$project_name[$i]."</td>
								<td>".$atac_no[$i]."</td>							
								<td>".number_format($po_total_value,2)."</td>
								<td>".$po_remarks."</td>
							</tr>";
						
						}
						
			$po_table .="</table>";
			
			$purchase_order = "<b>PO No. - ".$po_num."</b><br /><br /><b style='text-transform:uppercase;'>Live Created By - ".$created_by."</b><br />";
			  
			$mes = "<p>".$mail_header_line."</p><br/>".$po_table."<a href='".$po_link.$po_num."' target='_blank'>View PO</a><br/><b>Thanks & Regards, </b><br><b style='text-transform:uppercase;'>".$user_name."</b>";
			
			$message = $purchase_order.$mes;
			$this->email->message($message);
			if(!$this->email->send()){
				echo $this->email->print_debugger();
				die;
			}
			
			$number_of_files = sizeof($_FILES['po_quote_frmsupp']['tmp_name']);
			$files = $_FILES['po_quote_frmsupp'];
			$this->load->library('upload');
			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|bmp';
			
			for($i = 0; $i < $number_of_files; $i++){
				$_FILES['po_quote_frmsupp']['name'] = $files['name'][$i];
				$_FILES['po_quote_frmsupp']['type'] = $files['type'][$i];
				$_FILES['po_quote_frmsupp']['tmp_name'] = $files['tmp_name'][$i];
				$_FILES['po_quote_frmsupp']['error'] = $files['error'][$i];
				$_FILES['po_quote_frmsupp']['size'] = $files['size'][$i];
				$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
				$filename = $_FILES['po_quote_frmsupp']['name'];
				$fileExe  = substr($filename, strrpos($filename,'.'));
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$file = basename($filename, ".".$ext);
				$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
				$NewFileName2 = $NewFileName.'_'.$RandNumber.".".$ext;
				$config['file_name'] = $NewFileName2;
				$config['remove_spaces'] = true;
				
				//now we initialize the upload library
				$this->upload->initialize($config);
				
				if ($this->upload->do_upload('po_quote_frmsupp')){
					//$this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$file_name = $data['upload_data']['file_name'];
					rename("./uploads/$file_name","./uploads/$NewFileName2");
				} 
				$pr_nik = $this->input->post('po_num');
				$this->createpodm->insert_attachment($pr_nik, $NewFileName2);
			}
			
			//File Upload Code End
			$data = array();
			$this->createpodm->insert_po_sub($data, $lvl1_approval_by, $lvl2_approval_by, $lvl1_email, $lvl2_email, $lvl1_app_req, $lvl2_app_req);
			$data['message'] = 'Data Inserted Successfully';
			$this->load->view('admin/insert_po_sub');	  
		}
	  }
	  
	  /****** Level 1 Approval ******/
		 
	  public function create_po_lvl1(){  
		 $data['m']=$this->createpodm->select_prqit_prno_lvl1();
		 $this->load->view('admin/create_po_lvl1', $data);  
	  }
	  
	  /****** View Purchase Request Controller *******/
	  public function view_po_lvl1(){
		 $selectpr = $_REQUEST['q'];
		 //Procedure for fetching L3 approval details
		 $data['p']=$this->createpodm->fetch_lvl3_app_req();
		 $this->db->close();	
		 $this->db->initialize();
		  
		 $data['max_amend_no'] = $this->createpodm->max_amend_no($selectpr);
	     foreach($data['max_amend_no']->result_array() AS $row) {
		 	$amend_no = $row['amend_no']; 	 
	     }
		 
		 $data['view_po']=$this->createpodm->po_view_lvl1($selectpr,$amend_no);	   		
		 $this->load->view('admin/view_po_lvl1',$data);
	  } 
	  
	  public function insert_po_sub_lvl1(){ 
		$this->load->helper(array('form'));
		//mail function start
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$po_num = $this->input->post('po_num');
		if($po_num != ''){
			//new values added
			$supplier_avg_lead_time = $this->input->post('supplier_avg_lead_time');
			//financial year
			$today_date = date("Y-m-d H:i:s");
			$current_year = substr($today_date,0,4);
			$next_year = $current_year+1;
			$last_year = $current_year-1;
			$current_fin_year = $current_year."-".substr($next_year,2,2);
			$last_fin_year = $last_year."-".substr($current_year,2,2);
			$po_date = $this->input->post('po_date');
			$live_created_by = $this->input->post('live_created_by');
			//Creator email
			$data['creator_email']=$this->createpodm->creator_email($live_created_by);
			foreach($data['creator_email']->result_array() AS $row) {
			$creator_email = $row['emp_email'];	
			}
			
			$to_email = "planning@tipl.com, purchase@tipl.com,".$creator_email;
			
			//Creator email
			
			$user_name = $_SESSION['username'];
			
			$from_email = $_SESSION['username']."@tipl.com";
			
			$data['mail_table']= $this->createpodm->mail_table($po_num);
			 
			$msg_table = "<table border='1' cellpadding='2' cellspacing='0'>
							<tr>
								<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
							</tr>
					   </table><br/><br/>
					   <table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:12px; padding:10px; border-radius:5px;'>
						<tr style='background-color:#CFF'>
							<td><b>Supplier Name</b></td>
							<td><b>Need Date</b></td>
							<td><b>Lead Time (AVG)</b></td>
							<td><b>Item Code</b></td>
							<td><b>Item Description</b></td>
							<td><b>Carrier Name</b></td>
							<td><b>Freight Terms</b></td>
							<td><b>Payment Terms</b></td>
							<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
							<td><b>Current Stock</b></td>
							<td><b>Reservation Qty</b></td>
							<td><b>For Stk Qty</b></td>
							<td><b>UOM</b></td>
							<td><b>Last Price</b></td>
							<td><b>Current Price</b></td>
							<td><b>Costing</b></td>
							<td><b>Costing Currency</b></td>
							<td><b>Category</b></td>
							<td><b>PM-Group</b></td>
							<td><b>Current Year Cons</b></td>
							<td><b>Reorder Level</b></td>
							<td><b>Reorder Qty</b></td>
							<td><b>Last Supplier</b></td>
							<td><b>Last Supplier Lead Time</b></td>
							<td><b>Last Supplier Rate</b></td>
							<td><b>Type Of PR</b></td>
							<td><b>Why Special Remarks</b></td>
							<td><b>Project Name</b></td>
							<td><b>ATAC No</b></td>
							<td><b>Item Total Value</b></td>
							<td><b>PO Value</b></td>
							<td><b>Remarks</b></td>
						</tr>";
			
			foreach($data['mail_table']->result_array() AS $row) {
				 $po_supp_name = $row['po_supp_name'];
				 $ipr_need_date1 = $row['po_need_date'];
				 $ipr_need_date = substr($ipr_need_date1,0,11);
				 $po_item_code = $row['po_item_code'];
				 $po_itm_desc = $row['po_itm_desc'];
				 $carrier_name = $row['carrier_name'];
				 $freight = $row['freight'];
				 $payterm = $row['payterm'];
				 $lastyr_cons = $row['lastyr_cons'];
				 $current_stk = $row['current_stk'];
				 $reservation_qty = $row['reservation_qty'];
				 $for_stk_quantity = $row['for_stk_quantity'];
				 $po_uom = $row['po_uom'];
				 $last_price = $row['last_price'];
				 $current_price = $row['current_price'];
				 $total_item_value = $row['total_item_value'];
				 $po_total_value = $row['po_total_value'];
				 $po_spcl_inst_frm_supp = $row['po_spcl_inst_frm_supp'];
				 $category = $row['ipr_category'];
				 $pm_group = $row['ipr_pm_group'];
				 
				 //new feilds
				 $current_yr_con = $row['current_yr_con'];
				 $color = $row['color'];
				 $reorder_level = $row['reorder_level'];
				 $reorder_qty = $row['reorder_qty'];
				 $last_supplier = $row['last_supplier'];
				 $last_supplier_lead_time = $row['last_supplier_lead_time'];
				 $last_supplier_rate = $row['last_supplier_rate'];
				 $type_of_pr = $row['type_of_pr'];
				 $project_name = $row['project_name'];
				 $atac_no = $row['atac_no'];
				 $costing = $row['costing'];
				 $costing_currency = $row['costing_currency'];
				 $why_spcl_rmks = $row['why_spcl_rmks'];
				 
			$msg_table .= "<tr>
							<td>".$po_supp_name."</td>
							<td style='background-color:".$color."'>".$ipr_need_date."</td>
							<td>".number_format($supplier_avg_lead_time,2)."</td>
							<td>".$po_item_code."</td>
							<td>".$po_itm_desc."</td>
							<td>".$carrier_name."</td>
							<td>".$freight."</td>
							<td>".$payterm."</td>
							<td>".number_format($lastyr_cons,2)."</td>
							<td>".number_format($current_stk,2)."</td>
							<td>".number_format($reservation_qty,2)."</td>
							<td>".number_format($for_stk_quantity,2)."</td>
							<td>".$po_uom."</td>
							<td>".number_format($last_price,2)."</td>
							<td>".number_format($current_price,2)."</td>
							<td>".$costing."</td>
							<td>".$costing_currency."</td>
							<td>".$category."</td>
							<td>".$pm_group."</td>
							<td>".number_format($current_yr_con,2)."</td>
							<td>".number_format($reorder_level,2)."</td>
							<td>".number_format($reorder_qty,2)."</td>
							<td>".$last_supplier."</td>
							<td>".$last_supplier_lead_time."</td>
							<td>".number_format($last_supplier_rate,2)."</td>
							<td>".$type_of_pr."</td>
							<td>".$why_spcl_rmks."</td>
							<td>".$project_name."</td>
							<td>".$atac_no."</td>
							<td>".number_format($total_item_value,2)."</td>
							<td>".number_format($po_total_value,2)."</td>
							<td>".$po_spcl_inst_frm_supp."</td>
					   </tr>";
			}
			
			$msg_table .="</table>";
				  
			$this->email->to($to_email);
			$this->email->from($from_email, $user_name);
			$this->email->cc($from_email);
			$this->email->bcc('it.software@tipl.com');
			$po_subject = $po_num.' - PO Authorization At Level 1';
			$this->email->subject($po_subject);
			$app_disapp_comnts_lvl1 = $this->input->post('app_disapp_comnts_lvl1');
			$po_approval_lvl1 = $this->input->post('po_approval_lvl1');
			$lvl1_app_req  = $this->input->post('lvl1_app_req');
			$lvl2_app_req  = $this->input->post('lvl2_app_req');
			$lvl3_app_req  = $this->input->post('lvl3_app_req');
			
			if($po_approval_lvl1 == 'Approve' && $lvl2_app_req == 'Yes'){
			 $po_status = "Status - PO Approved At Level 1 Pending For L2 Approval<br/><br/>"; 
			}else if($po_approval_lvl1 == 'Approve' && $lvl2_app_req == 'No' && $lvl3_app_req == 'Yes'){
			 $po_status = "Status - PO Approved At Level 1 Pending For L3 Approval.<br/><br/>";
			}else if($po_approval_lvl1 == 'Approve' && $lvl2_app_req == 'No' && $lvl3_app_req == 'No'){
			 $po_status = "Status - PO Approved At Level 1 Pending For ERP Authorization.<br/><br/>"; 
			} else {
			 $po_status = "Status - PO Disapproved At Level 1<br/><br/>";
			}
			  
			$message = "<b>PO No. - ".$po_num."</b><br /><br />"."<b style='text-transform:uppercase;'>PO Created By. - ".$live_created_by."</b><br /><br />".$msg_table."<p>PO Remarks - ".$app_disapp_comnts_lvl1."</p>".$po_status."<b>Thanks & Regards, </b><br/><br /><b style='text-transform:uppercase;'>".$user_name."</b>";
			
			$this->email->message($message);
			
			if(!$this->email->send()){
			echo $this->email->print_debugger();
			die;
			}
			//mail function ends
			
			$data = array();
			$this->createpodm->insert_po_sub_lvl1($data);
			$data['message'] = 'Data Inserted Successfully';
			$this->load->view('admin/insert_po_sub',$data);
		}
	  }
	  
	  /****** Level 2 Approval ******/
		 
	  public function create_po_lvl2(){  
		 $data['m']=$this->createpodm->select_prqit_prno_lvl2();
		 $this->load->view('admin/create_po_lvl2', $data);  
	  }
	  
	  /****** View Purchase Request Controller *******/
	  public function view_po_lvl2(){
		  $selectpr = $_REQUEST['q'];
		  $data['max_amend_no'] = $this->createpodm->max_amend_no($selectpr);
	
		  foreach($data['max_amend_no']->result_array() AS $row) {
			 $amend_no = $row['amend_no']; 	 
		  }
		  
		  $data['view_po']= $this->createpodm->po_view_lvl2($selectpr, $amend_no);
		  $data['supplier_quotes'] = $this->createpodm->supplier_quotes($selectpr);  
		  $this->load->view('admin/view_po_lvl2',$data);
	  } 
	  
	  public function insert_po_sub_lvl2(){
		 $this->load->helper(array('form'));
		 $this->load->library('email');
		 //New mail function start
		 $po_num = $this->input->post('po_num');
		 if($po_num != ''){
		 $supplier_avg_lead_time = $this->input->post('supplier_avg_lead_time');
		 $today_date = date("Y-m-d H:i:s");
		 $current_year = substr($today_date,0,4);
		 $next_year = $current_year+1;
		 $last_year = $current_year-1;
		 $current_fin_year = $current_year."-".substr($next_year,2,2);
		 $last_fin_year = $last_year."-".substr($current_year,2,2);
		 $po_date = $this->input->post('po_date');
		 $user_name = $_SESSION['username'];
		 $from_email = $_SESSION['username']."@tipl.com";
		 $data['mail_table']= $this->createpodm->mail_table($po_num);
		 $msg_table = "<table border='1' cellpadding='2' cellspacing='0'>
							<tr>
								<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
							</tr>
					   </table><br/><br/>
		 			   <table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:12px; padding:10px; border-radius:5px;'>
						<tr style='background-color:#CFF'>
							<td><b>Supplier Name</b></td>
							<td><b>Need Date</b></td>
							<td><b>Lead Time (AVG)</b></td>
							<td><b>Item Code</b></td>
							<td><b>Item Description</b></td>
							<td><b>Carrier Name</b></td>
							<td><b>Freight Terms</b></td>
							<td><b>Payment Terms</b></td>
							<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
							<td><b>Current Stock</b></td>
							<td><b>Reservation Qty</b></td>
							<td><b>For Stk Qty</b></td>
							<td><b>UOM</b></td>
							<td><b>Last Price</b></td>
							<td><b>Current Price</b></td>
							<td><b>Costing</b></td>
							<td><b>Costing Currency</b></td>
							<td><b>Category</b></td>
							<td><b>PM-Group</b></td>
							<td><b>Current Year Cons</b></td>
							<td><b>Reorder Level</b></td>
							<td><b>Reorder Qty</b></td>
							<td><b>Last Supplier</b></td>
							<td><b>Last Supplier Lead Time</b></td>
							<td><b>Last Supplier Rate</b></td>
							<td><b>Type Of PR</b></td>
							<td><b>Why Special Remarks</b></td>
							<td><b>Project Name</b></td>
							<td><b>ATAC No</b></td>
							<td><b>Item Total Value</b></td>
							<td><b>PO Value</b></td>
							<td><b>Remarks</b></td>
						</tr>";
		  
		  foreach($data['mail_table']->result_array() AS $row) {
				 $po_supp_name = $row['po_supp_name'];
				 $ipr_need_date1 = $row['po_need_date'];
				 $ipr_need_date = substr($ipr_need_date1,0,11);
				 $po_item_code = $row['po_item_code'];
				 $po_itm_desc = $row['po_itm_desc'];
				 $carrier_name = $row['carrier_name'];
				 $freight = $row['freight'];
				 $payterm = $row['payterm'];
				 $lastyr_cons = $row['lastyr_cons'];
				 $current_stk = $row['current_stk'];
				 $reservation_qty = $row['reservation_qty'];
				 $for_stk_quantity = $row['for_stk_quantity'];
				 $po_uom = $row['po_uom'];
				 $last_price = $row['last_price'];
				 $current_price = $row['current_price'];
				 $total_item_value = $row['total_item_value'];
				 $po_total_value = $row['po_total_value'];
				 $po_spcl_inst_frm_supp = $row['po_spcl_inst_frm_supp'];
				 $category = $row['ipr_category'];
				 $pm_group = $row['ipr_pm_group'];
				 
				 //new feilds
				 $current_yr_con = $row['current_yr_con'];
				 $color = $row['color'];
				 $reorder_level = $row['reorder_level'];
				 $reorder_qty = $row['reorder_qty'];
				 $last_supplier = $row['last_supplier'];
				 $last_supplier_lead_time = $row['last_supplier_lead_time'];
				 $last_supplier_rate = $row['last_supplier_rate'];
				 $type_of_pr = $row['type_of_pr'];
				 $project_name = $row['project_name'];
				 $atac_no = $row['atac_no'];
				 $costing = $row['costing'];
				 $costing_currency = $row['costing_currency'];
				 $why_spcl_rmks = $row['why_spcl_rmks'];
				 
		 $msg_table .= "<tr>
							<td>".$po_supp_name."</td>
							<td style='background-color:".$color."'>".$ipr_need_date."</td>
							<td>".number_format($supplier_avg_lead_time,2)."</td>
							<td>".$po_item_code."</td>
							<td>".$po_itm_desc."</td>
							<td>".$carrier_name."</td>
							<td>".$freight."</td>
							<td>".$payterm."</td>
							<td>".number_format($lastyr_cons,2)."</td>
							<td>".number_format($current_stk,2)."</td>
							<td>".number_format($reservation_qty,2)."</td>
							<td>".number_format($for_stk_quantity,2)."</td>
							<td>".$po_uom."</td>
							<td>".number_format($last_price,2)."</td>
							<td>".number_format($current_price,2)."</td>
							<td>".$costing."</td>
							<td>".$costing_currency."</td>
							<td>".$category."</td>
							<td>".$pm_group."</td>
							<td>".number_format($current_yr_con,2)."</td>
							<td>".number_format($reorder_level,2)."</td>
							<td>".number_format($reorder_qty,2)."</td>
							<td>".$last_supplier."</td>
							<td>".$last_supplier_lead_time."</td>
							<td>".number_format($last_supplier_rate,2)."</td>
							<td>".$type_of_pr."</td>
							<td>".$why_spcl_rmks."</td>
							<td>".$project_name."</td>
							<td>".$atac_no."</td>
							<td>".number_format($total_item_value,2)."</td>
							<td>".number_format($po_total_value,2)."</td>
							<td>".$po_spcl_inst_frm_supp."</td>
					   </tr>";
		  }
		  $msg_table .="</table>";
		  
		  $live_created_by = $this->input->post('live_created_by');
		  $data['creator_email']=$this->createpodm->creator_email($live_created_by);
		  foreach($data['creator_email']->result_array() AS $row) {
				$creator_email = $row['emp_email'];	
		  }
		  
		 $to_email = "planning@tipl.com, purchase@tipl.com,".$creator_email;	  
		 $this->email->to($to_email);
		 $this->email->from($from_email, $user_name);
		 $cc_email = "abhinav.toshniwal@tipl.com,".$from_email;
		 $this->email->cc($cc_email);
		 $this->email->bcc('it.software@tipl.com');
		 $po_subject = $po_num.'PO Authorization At Level 2';
		 $this->email->subject($po_subject);
		 $app_disapp_comnts_lvl2 = $this->input->post('app_disapp_comnts_lvl2');
		 $po_approval_lvl2 = $this->input->post('po_approval_lvl2');
		 $lvl3_app_req  = $this->input->post('lvl3_app_req');
		 
		 if($po_approval_lvl2 == 'Approve' && $lvl3_app_req == 'Yes'){
			 $po_status = "Status - PO Approved At Level 2 Pending For Level 3 Authorization<br/><br/>"; 
		 }else if($po_approval_lvl2 == 'Approve' && $lvl3_app_req == 'No'){
			 $po_status = "Status - PO Approved At Level 2 Pending For ERP Authorzation<br/><br/>"; 
		 } else {
			 $po_status = "Status - PO Disapproved At Level 2<br/><br/>";
		 }
		 
		 $message ="<b>PO No. - ".$po_num."</b><br /><br />"."<b style='text-transform:uppercase;'>PO Created By. - ".$live_created_by."</b><br /><br />".$msg_table."<p>PO Remarks - ".$app_disapp_comnts_lvl2."</p>".$po_status."<b>Thanks & Regards,</b><br><br><b style='text-transform:uppercase;'>".$user_name."</b>";
			
		$this->email->message( $message);
			
		if(!$this->email->send()){  
			echo $this->email->print_debugger();
			die;
		}
		
		//mail ends
		//Amended PO Comparision Mail to Abhinav Sir
		$data['amend_comparision'] = $this->createpodm->amend_comparision($po_num);
		foreach($data['amend_comparision']->result_array() AS $row) {
			$count = $row['count']; 	 
	    }
		
		$data['mail_table_history']= $this->createpodm->mail_table_history($po_num);
		if($count > 0 && $po_approval_lvl2 == 'Approve'){
			$this->email->to('abhinav.toshniwal@tipl.com');	
			$this->email->from($from_email, $user_name);
			$this->email->bcc('it.software@tipl.com');
			$po_subject = $po_num.'- Amend PO Comparision With Previous PO';
			$this->email->subject($po_subject);
			
			$msg_table1 = "
			<table border='1' cellpadding='2' cellspacing='0'>
				<tr>
					<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
				</tr>
			</table><br/><br/>";

			$msg_table1 .= "
			<h3>PO Before Amendment</h3><br>
			<table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:12px; padding:10px; border-radius:5px;'>
				<tr style='background-color:#CFF'>
					<td><b>Supplier Name</b></td>
					<td><b>Need Date</b></td>
					<td><b>Lead Time (AVG)</b></td>
					<td><b>Item Code</b></td>
					<td><b>Item Description</b></td>
					<td><b>Carrier Name</b></td>
					<td><b>Freight Terms</b></td>
					<td><b>Payment Terms</b></td>
					<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
					<td><b>Current Stock</b></td>
					<td><b>Reservation Qty</b></td>
					<td><b>For Stk Qty</b></td>
					<td><b>UOM</b></td>
					<td><b>Last Price</b></td>
					<td><b>Current Price</b></td>
					<td><b>Costing</b></td>
					<td><b>Costing Currency</b></td>
					<td><b>Category</b></td>
					<td><b>PM-Group</b></td>
					<td><b>Current Year Cons</b></td>
					<td><b>Reorder Level</b></td>
					<td><b>Reorder Qty</b></td>
					<td><b>Last Supplier</b></td>
					<td><b>Last Supplier Lead Time</b></td>
					<td><b>Last Supplier Rate</b></td>
					<td><b>Type Of PR</b></td>
					<td><b>Why Special Remarks</b></td>
					<td><b>Project Name</b></td>
					<td><b>ATAC No</b></td>
					<td><b>Item Total Value</b></td>
					<td><b>PO Value</b></td>
					<td><b>Remarks</b></td>
				</tr>";

				foreach($data['mail_table_history']->result_array() AS $row) {
				$po_supp_name = $row['po_supp_name'];
				$ipr_need_date1 = $row['po_need_date'];
				$ipr_need_date = substr($ipr_need_date1,0,11);
				$po_item_code = $row['po_item_code'];
				$po_itm_desc = $row['po_itm_desc'];
				$carrier_name = $row['carrier_name'];
				$freight = $row['freight'];
				$payterm = $row['payterm'];
				$lastyr_cons = $row['lastyr_cons'];
				$current_stk = $row['current_stk'];
				$reservation_qty = $row['reservation_qty'];
				$for_stk_quantity = $row['for_stk_quantity'];
				$po_uom = $row['po_uom'];
				$last_price = $row['last_price'];
				$current_price = $row['current_price'];
				$total_item_value = $row['total_item_value'];
				$po_total_value = $row['po_total_value'];
				$po_spcl_inst_frm_supp = $row['po_spcl_inst_frm_supp'];
				$category = $row['ipr_category'];
				$pm_group = $row['ipr_pm_group'];
				$current_yr_con = $row['current_yr_con'];
				$color = $row['color'];
				$reorder_level = $row['reorder_level'];
				$reorder_qty = $row['reorder_qty'];
				$last_supplier = $row['last_supplier'];
				$last_supplier_lead_time = $row['last_supplier_lead_time'];
				$last_supplier_rate = $row['last_supplier_rate'];
				$type_of_pr = $row['type_of_pr'];
				$project_name = $row['project_name'];
				$atac_no = $row['atac_no'];
				$costing = $row['costing'];
				$costing_currency = $row['costing_currency'];
				$why_spcl_rmks = $row['why_spcl_rmks'];

				$msg_table1 .= "
				<tr>
					<td>".$po_supp_name."</td>
					<td style='background-color:".$color."'>".$ipr_need_date."</td>
					<td>".number_format($supplier_avg_lead_time,2)."</td>
					<td>".$po_item_code."</td>
					<td>".$po_itm_desc."</td>
					<td>".$carrier_name."</td>
					<td>".$freight."</td>
					<td>".$payterm."</td>
					<td>".number_format($lastyr_cons,2)."</td>
					<td>".number_format($current_stk,2)."</td>
					<td>".number_format($reservation_qty,2)."</td>
					<td>".number_format($for_stk_quantity,2)."</td>
					<td>".$po_uom."</td>
					<td>".number_format($last_price,2)."</td>
					<td>".number_format($current_price,2)."</td>
					<td>".$costing."</td>
					<td>".$costing_currency."</td>
					<td>".$category."</td>
					<td>".$pm_group."</td>
					<td>".number_format($current_yr_con,2)."</td>
					<td>".number_format($reorder_level,2)."</td>
					<td>".number_format($reorder_qty,2)."</td>
					<td>".$last_supplier."</td>
					<td>".$last_supplier_lead_time."</td>
					<td>".number_format($last_supplier_rate,2)."</td>
					<td>".$type_of_pr."</td>
					<td>".$why_spcl_rmks."</td>
					<td>".$project_name."</td>
					<td>".$atac_no."</td>
					<td>".number_format($total_item_value,2)."</td>
					<td>".number_format($po_total_value,2)."</td>
					<td>".$po_spcl_inst_frm_supp."</td>
				</tr>";
				}

				$msg_table1 .="</table>";

				$msg_table1 .= "
				<h3>PO After Amendment</h3>
				<table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:12px; padding:10px; border-radius:5px;'>
					<tr style='background-color:#CFF'>
						<td><b>Supplier Name</b></td>
						<td><b>Need Date</b></td>
						<td><b>Lead Time (AVG)</b></td>
						<td><b>Item Code</b></td>
						<td><b>Item Description</b></td>
						<td><b>Carrier Name</b></td>
						<td><b>Freight Terms</b></td>
						<td><b>Payment Terms</b></td>
						<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
						<td><b>Current Stock</b></td>
						<td><b>Reservation Qty</b></td>
						<td><b>For Stk Qty</b></td>
						<td><b>UOM</b></td>
						<td><b>Last Price</b></td>
						<td><b>Current Price</b></td>
						<td><b>Costing</b></td>
						<td><b>Costing Currency</b></td>
						<td><b>Category</b></td>
						<td><b>PM-Group</b></td>
						<td><b>Current Year Cons</b></td>
						<td><b>Reorder Level</b></td>
						<td><b>Reorder Qty</b></td>
						<td><b>Last Supplier</b></td>
						<td><b>Last Supplier Lead Time</b></td>
						<td><b>Last Supplier Rate</b></td>
						<td><b>Type Of PR</b></td>
						<td><b>Why Special Remarks</b></td>
						<td><b>Project Name</b></td>
						<td><b>ATAC No</b></td>
						<td><b>Item Total Value</b></td>
						<td><b>PO Value</b></td>
						<td><b>Remarks</b></td>
					</tr>";
					foreach($data['mail_table']->result_array() AS $row) {
						$po_supp_name = $row['po_supp_name'];
						$ipr_need_date1 = $row['po_need_date'];
						$ipr_need_date = substr($ipr_need_date1,0,11);
						$po_item_code = $row['po_item_code'];
						$po_itm_desc = $row['po_itm_desc'];
						$carrier_name = $row['carrier_name'];
						$freight = $row['freight'];
						$payterm = $row['payterm'];
						$lastyr_cons = $row['lastyr_cons'];
						$current_stk = $row['current_stk'];
						$reservation_qty = $row['reservation_qty'];
						$for_stk_quantity = $row['for_stk_quantity'];
						$po_uom = $row['po_uom'];
						$last_price = $row['last_price'];
						$current_price = $row['current_price'];
						$total_item_value = $row['total_item_value'];
						$po_total_value = $row['po_total_value'];
						$po_spcl_inst_frm_supp = $row['po_spcl_inst_frm_supp'];
						$category = $row['ipr_category'];
						$pm_group = $row['ipr_pm_group'];
						$current_yr_con = $row['current_yr_con'];
						$color = $row['color'];
						$reorder_level = $row['reorder_level'];
						$reorder_qty = $row['reorder_qty'];
						$last_supplier = $row['last_supplier'];
						$last_supplier_lead_time = $row['last_supplier_lead_time'];
						$last_supplier_rate = $row['last_supplier_rate'];
						$type_of_pr = $row['type_of_pr'];
						$project_name = $row['project_name'];
						$atac_no = $row['atac_no'];
						$costing = $row['costing'];
						$costing_currency = $row['costing_currency'];
						$why_spcl_rmks = $row['why_spcl_rmks'];
						
					$msg_table1 .= "
					<tr>
						<td>".$po_supp_name."</td>
						<td style='background-color:".$color."'>".$ipr_need_date."</td>
						<td>".number_format($supplier_avg_lead_time,2)."</td>
						<td>".$po_item_code."</td>
						<td>".$po_itm_desc."</td>
						<td>".$carrier_name."</td>
						<td>".$freight."</td>
						<td>".$payterm."</td>
						<td>".number_format($lastyr_cons,2)."</td>
						<td>".number_format($current_stk,2)."</td>
						<td>".number_format($reservation_qty,2)."</td>
						<td>".number_format($for_stk_quantity,2)."</td>
						<td>".$po_uom."</td>
						<td>".number_format($last_price,2)."</td>
						<td>".number_format($current_price,2)."</td>
						<td>".$costing."</td>
						<td>".$costing_currency."</td>
						<td>".$category."</td>
						<td>".$pm_group."</td>
						<td>".number_format($current_yr_con,2)."</td>
						<td>".number_format($reorder_level,2)."</td>
						<td>".number_format($reorder_qty,2)."</td>
						<td>".$last_supplier."</td>
						<td>".$last_supplier_lead_time."</td>
						<td>".number_format($last_supplier_rate,2)."</td>
						<td>".$type_of_pr."</td>
						<td>".$why_spcl_rmks."</td>
						<td>".$project_name."</td>
						<td>".$atac_no."</td>
						<td>".number_format($total_item_value,2)."</td>
						<td>".number_format($po_total_value,2)."</td>
						<td>".$po_spcl_inst_frm_supp."</td>
					</tr>";
					}
				
					$msg_table1 .="</table>";
			
					//Message Table
					$message = "<b>PO No. - ".$po_num."</b><br />".$msg_table1."<p>PO Remarks - ".$app_disapp_comnts_lvl2."</p>".$po_status."<b>Thanks & Regards,</b><br><br><b style='text-transform:uppercase;'>".$user_name."</b>";
		
					$this->email->message( $message);
					if(!$this->email->send()){
						echo $this->email->print_debugger();
						die;
					}		

				}
				//Amended PO Comparision Mai 
				$data = array();
				$this->createpodm->insert_po_sub_lvl2($data);
				$data['message'] = 'Data Inserted Successfully';
				$this->load->view('admin/insert_po_sub',$data);
			}
	  }
	  
	  /****** Level 3 Approval ******/ 
	  public function create_po_lvl3(){  
		 $data['m']=$this->createpodm->select_prqit_prno_lvl2();
		 $this->load->view('admin/create_po_lvl3', $data);  
	  }
	  
	  public function view_po_lvl3(){
		  $selectpr = $_REQUEST['q'];
		  $data['max_amend_no'] = $this->createpodm->max_amend_no($selectpr);
		  foreach($data['max_amend_no']->result_array() AS $row) {
			 $amend_no = $row['amend_no']; 	 
		  }
		  
		  $data['view_po']= $this->createpodm->po_view_lvl2($selectpr, $amend_no);
		  $data['supplier_quotes'] = $this->createpodm->supplier_quotes($selectpr);	  
		  $this->load->view('admin/view_po_lvl3',$data);
	  } 
	  
	  public function insert_po_sub_lvl3(){
		$this->load->helper(array('form'));
		$this->load->library('email');
		//mail start
		$po_num = $this->input->post('po_num');
		
		if($po_num != ''){
			$supplier_avg_lead_time = $this->input->post('supplier_avg_lead_time');
			$today_date = date("Y-m-d H:i:s");
			$current_year = substr($today_date,0,4);
			$next_year = $current_year+1;
			$last_year = $current_year-1;
			$current_fin_year = $current_year."-".substr($next_year,2,2);
			$last_fin_year = $last_year."-".substr($current_year,2,2);
			$po_date = $this->input->post('po_date');
			$user_name = $_SESSION['username'];
			$from_email = $_SESSION['username']."@tipl.com";
			$data['mail_table']= $this->createpodm->mail_table($po_num);
			$msg_table = "
			<table border='1' cellpadding='2' cellspacing='0'>
				<tr>
					<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
				</tr>
			</table><br/><br/>
			<table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:12px; padding:10px; border-radius:5px;'>
				<tr style='background-color:#CFF'>
					<td><b>Supplier Name</b></td>
					<td><b>Need Date</b></td>
					<td><b>Lead Time (AVG)</b></td>
					<td><b>Item Code</b></td>
					<td><b>Item Description</b></td>
					<td><b>Carrier Name</b></td>
					<td><b>Freight Terms</b></td>
					<td><b>Payment Terms</b></td>
					<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
					<td><b>Current Stock</b></td>
					<td><b>Reservation Qty</b></td>
					<td><b>For Stk Qty</b></td>
					<td><b>UOM</b></td>
					<td><b>Last Price</b></td>
					<td><b>Current Price</b></td>
					<td><b>Costing</b></td>
					<td><b>Costing Currency</b></td>
					<td><b>Category</b></td>
					<td><b>PM-Group</b></td>
					<td><b>Current Year Cons</b></td>
					<td><b>Reorder Level</b></td>
					<td><b>Reorder Qty</b></td>
					<td><b>Last Supplier</b></td>
					<td><b>Last Supplier Lead Time</b></td>
					<td><b>Last Supplier Rate</b></td>
					<td><b>Type Of PR</b></td>
					<td><b>Why Special Remarks</b></td>
					<td><b>Project Name</b></td>
					<td><b>ATAC No</b></td>
					<td><b>Item Total Value</b></td>
					<td><b>PO Value</b></td>
					<td><b>Remarks</b></td>
				</tr>";
			  
				foreach($data['mail_table']->result_array() AS $row) {
					 $po_supp_name = $row['po_supp_name'];
					 $ipr_need_date1 = $row['po_need_date'];
					 $ipr_need_date = substr($ipr_need_date1,0,11);
					 $po_item_code = $row['po_item_code'];
					 $po_itm_desc = $row['po_itm_desc'];
					 $carrier_name = $row['carrier_name'];
					 $freight = $row['freight'];
					 $payterm = $row['payterm'];
					 $lastyr_cons = $row['lastyr_cons'];
					 $current_stk = $row['current_stk'];
					 $reservation_qty = $row['reservation_qty'];
					 $for_stk_quantity = $row['for_stk_quantity'];
					 $po_uom = $row['po_uom'];
					 $last_price = $row['last_price'];
					 $current_price = $row['current_price'];
					 $total_item_value = $row['total_item_value'];
					 $po_total_value = $row['po_total_value'];
					 $po_spcl_inst_frm_supp = $row['po_spcl_inst_frm_supp'];
					 $category = $row['ipr_category'];
					 $pm_group = $row['ipr_pm_group'];
					 $current_yr_con = $row['current_yr_con'];
					 $color = $row['color'];
					 $reorder_level = $row['reorder_level'];
					 $reorder_qty = $row['reorder_qty'];
					 $last_supplier = $row['last_supplier'];
					 $last_supplier_lead_time = $row['last_supplier_lead_time'];
					 $last_supplier_rate = $row['last_supplier_rate'];
					 $type_of_pr = $row['type_of_pr'];
					 $project_name = $row['project_name'];
					 $atac_no = $row['atac_no'];
					 $costing = $row['costing'];
					 $costing_currency = $row['costing_currency'];
					 $why_spcl_rmks = $row['why_spcl_rmks'];
					 
					$msg_table .= "
					<tr>
						<td>".$po_supp_name."</td>
						<td style='background-color:".$color."'>".$ipr_need_date."</td>
						<td>".number_format($supplier_avg_lead_time,2)."</td>
						<td>".$po_item_code."</td>
						<td>".$po_itm_desc."</td>
						<td>".$carrier_name."</td>
						<td>".$freight."</td>
						<td>".$payterm."</td>
						<td>".number_format($lastyr_cons,2)."</td>
						<td>".number_format($current_stk,2)."</td>
						<td>".number_format($reservation_qty,2)."</td>
						<td>".number_format($for_stk_quantity,2)."</td>
						<td>".$po_uom."</td>
						<td>".number_format($last_price,2)."</td>
						<td>".number_format($current_price,2)."</td>
						<td>".$costing."</td>
						<td>".$costing_currency."</td>
						<td>".$category."</td>
						<td>".$pm_group."</td>
						<td>".number_format($current_yr_con,2)."</td>
						<td>".number_format($reorder_level,2)."</td>
						<td>".number_format($reorder_qty,2)."</td>
						<td>".$last_supplier."</td>
						<td>".$last_supplier_lead_time."</td>
						<td>".number_format($last_supplier_rate,2)."</td>
						<td>".$type_of_pr."</td>
						<td>".$why_spcl_rmks."</td>
						<td>".$project_name."</td>
						<td>".$atac_no."</td>
						<td>".number_format($total_item_value,2)."</td>
						<td>".number_format($po_total_value,2)."</td>
						<td>".$po_spcl_inst_frm_supp."</td>
					</tr>";
				}
			  
				$msg_table .="</table>";
				
				$live_created_by = $this->input->post('live_created_by');
				//Creator email
				$data['creator_email']=$this->createpodm->creator_email($live_created_by);
				foreach($data['creator_email']->result_array() AS $row) {
					$creator_email = $row['emp_email'];	
				}
				
				$to_email = "planning@tipl.com, purchase@tipl.com,".$creator_email;
				
				//Creator email	  
				$this->email->to($to_email);
				$this->email->from($from_email, $user_name);
			 
				if($user_name == 'abhinav.toshniwal'){
					$cc_email = "abhinav.toshniwal@tipl.com";
				} else {
					$cc_email = "abhinav.toshniwal@tipl.com,".$from_email;
				}
			 
				$this->email->cc($cc_email);
				$this->email->bcc('it.software@tipl.com');
				$po_subject = $po_num.'PO Authorization At Level 3';
				$this->email->subject($po_subject);
				$app_disapp_comnts_lvl3 = $this->input->post('app_disapp_comnts_lvl3');
				$po_approval_lvl3 = $this->input->post('po_approval_lvl3');
				
				if($po_approval_lvl3 == 'Approve'){
					$po_status = "Status - PO Approved At Level 3<br/><br/>";
				} else {
					$po_status = "Status - PO Disapproved At Level 3<br/><br/>";
				}
			 
				$message ="<b>PO No. - ".$po_num."</b><br /><br />"."<b style='text-transform:uppercase;'>PO Created By. - ".$live_created_by."</b><br /><br />".$msg_table."<p>PO Remarks - ".$app_disapp_comnts_lvl3."</p>".$po_status."<b>Thanks & Regards,</b><br><br><b style='text-transform:uppercase;'>".$user_name."</b>";
				
				$this->email->message( $message);
				
				if(!$this->email->send()){
					echo $this->email->print_debugger();
					die;
				}
				//mail ends
				$data = array();
				$this->createpodm->insert_po_sub_lvl3($data);
				$data['message'] = 'Data Inserted Successfully';
				$this->load->view('admin/insert_po_sub',$data);
			}
		
	  }
	  
	public function atac_details(){
		$this->load->view('admin/atac_details');
	}
	
	//PO Description Edit Foreign Purchase
	public function po_desc_edit_main(){ 
		$this->load->view('admin/po_desc_edit_main'); 
	}
	
	public function po_desc_edit_detail(){
		$po_num = $_REQUEST['po_number'];
		$data['max_amend_no'] = $this->createpodm->max_amend_no($po_num);
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
	
		$data['view_po']=$this->createpodm->view_poqit_pono($po_num, $amend_no);
		$data['act_timing'] = $this->createpodm->act_timing($po_num);
		$sup_code = array();
		$this->load->view('admin/po_desc_edit_detail',$data); 
	}
	
	public function po_desc_edit_entry(){
		$this->load->helper(array('form'));
		$po_num = $this->input->post('po_num');
		
		if($po_num != ''){
			$data = array(); 
			$this->createpodm->po_desc_edit_entry($data);
			$data['message'] = 'Data Inserted Successfully';
			$this->load->view('admin/insert_po_sub_des_edit',$data);
		}
	}
}