<?php
class Drawingc extends CI_Controller{  
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('drawingm');
		user_act();
	}
	
	public function index(){  
		$data['h']=$this->drawingm->select_prqit_prno();
		$this->load->view('admin/drawing_create', $data); 
	} 
	
	public function create_pr(){  
		$data['h']=$this->drawingm->select_prqit_prno();
		$this->load->view('admin/drawing_create', $data);  
	}
	
	public function view_pr(){
		$selectpr = $_REQUEST['q'];
		$data['v']=$this->drawingm->view_prqit_prno($selectpr);
		
		foreach($data['v']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		}
		
		$sup_code1 = urldecode($sup_code);
		$data['p']=$this->drawingm->procesdure_run($sup_code);
		$this->db->close();
		$this->db->initialize();
		
		$data['r']=$this->drawingm->pendal_info($sup_code);
		$data['month']= $this->drawingm->pendal_info_monthwise($sup_code);
		$data['last5'] = $this->drawingm->pendal_info_last5_trans($sup_code);
		$data['reorder'] = $this->drawingm->pendal_info_reorder_lvl_qty($sup_code);
		$data['monthofinvtry'] = $this->drawingm->monthofinvtry($sup_code);
		$data['orderinadvnce'] = $this->drawingm->orderinadvnce($sup_code);
		$data['wrkordr_ser']=$this->drawingm->wrkordr_ser($sup_code);
		$data['lstyrcumnrec1'] = $this->drawingm->lstyrcumnrec1($sup_code);
		$data['lastfivetrans1'] = $this->drawingm->lastfivetrans1($sup_code);
		$data['itemdescmaster'] = $this->drawingm->itemdescmaster($sup_code1);
		$data['mnthwsecurryrcons1'] = $this->drawingm->pendal_master_mnthwsecurryrcons($sup_code1);
		$data['mnthwsecurryrrcpt1'] = $this->drawingm->pendal_master_mnthwsecurryrrcpt($sup_code1);
		$data['supplier1'] = $this->drawingm->pendal_master_supplier($sup_code1);
		$data['bomdetail1'] = $this->drawingm->pendal_master_bomdetail($sup_code1);
		$this->load->view('admin/drawing_view',$data);
	} 
	
	public function pendal_view($sup_code1){
		$sup_code = urldecode($sup_code1);
		$data['pendal'] = $this->drawingm->pendal_master($sup_code);
		$data['pendalstkblnc'] = $this->drawingm->pendal_master_whstkblnc($sup_code);
		$data['pndposopr'] = $this->drawingm->pendal_master_pndposopr($sup_code);
		$data['allocat'] = $this->drawingm->pendal_master_allocat($sup_code);
		$data['allocat_tot'] = $this->drawingm->pendal_master_allocat_tot($sup_code);
		$data['wipdet'] = $this->drawingm->pendal_master_wipdet($sup_code);
		$data['lastfivetrans'] = $this->drawingm->pendal_master_lastfivetrans($sup_code);
		$data['lstyrcumnrec'] = $this->drawingm->pendal_master_lstyrcumnrec($sup_code);
		$data['supplier'] = $this->drawingm->pendal_master_supplier($sup_code);
		$data['mnthwsecurryrcons'] = $this->drawingm->pendal_master_mnthwsecurryrcons($sup_code);
		$data['mnthwsecurryrrcpt'] = $this->drawingm->pendal_master_mnthwsecurryrrcpt($sup_code);
		$data['bomdetail'] = $this->drawingm->pendal_master_bomdetail($sup_code);
		$this->load->view('admin/pendal_view',$data);
	} 
	
	public function wrkordr_ser_res(){
		$selectWo = $_REQUEST['q'];
		$data['wrkordr_ser_res']=$this->drawingm->wrkordr_ser_res($selectWo);
		$this->load->view('admin/workorder_search_drawing', $data);
	}
	
	public function insert_pr_sub(){
		$this->load->helper(array('form'));
		$config['upload_path']   = './uploads/'; 
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|'; 
		$config['max_size']      = 100000000; 
		$config['max_width']     = 102400; 
		$config['max_height']    = 768000; 
		$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
		$first_filename = strtolower($_FILES["attach_drawing"]["name"]);
		$first_fileExe  = substr($first_filename, strrpos($first_filename,'.'));
		$ext = pathinfo($first_filename, PATHINFO_EXTENSION);
		$file = basename($first_filename, ".".$ext);
		$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
		$uploaded_drawing = $NewFileName.'_'.$RandNumber.$first_fileExe;
		$config['file_name'] = $uploaded_drawing;
		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload('attach_drawing')) {
			$error = array('error' => $this->upload->display_errors());
		}else { 
			$data1 = array('upload_data' => $this->upload->data()); 
		} 
		
		$data = array();
		$this->drawingm->insert_pr_sub($data, $uploaded_drawing);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_pr_sub');
	}	  

}
