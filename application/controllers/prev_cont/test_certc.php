<?php
class Test_certc extends CI_Controller  
{  
   
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url'); 
		 
		$this->load->database();
		   
		$this->load->model('test_certm');
		
		$this->load->helper(array('form'));
		
		user_act();
	}
	 
	public function create_po_lvl2()  
	{
		$data['m']=$this->test_certm->select_prqit_prno_lvl2();
		
		$this->load->view('admin/test_certificate_create', $data);  
	}
	  
	  /******** View Purchase Request Controller ********/
	  
	public function view_po_lvl2()
	{
		$selectpr = $_REQUEST['q'];
		
		$data['max_amend_no'] = $this->test_certm->max_amend_no($selectpr);
	
	    foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
	    }
		
		$data['view_po']=$this->test_certm->po_view_lvl2($selectpr, $amend_no);
		
		$this->load->view('admin/test_certificate_view',$data);
	} 
	  
	public function insert_po_sub_lvl2(){
		
		$config['upload_path']   = './uploads/'; 
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|'; 
		$config['max_size']      = 100000000; 
		$config['max_width']     = 102400; 
		$config['max_height']    = 768000; 
		
		$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
		
		$first_filename = strtolower($_FILES["test_cert"]["name"]);
		
		$first_fileExe  = substr($first_filename, strrpos($first_filename,'.'));
		
		$ext = pathinfo($first_filename, PATHINFO_EXTENSION);
		
		$file = basename($first_filename, ".".$ext);
		
		$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
		
		$testcert = $NewFileName.'_'.$RandNumber.$first_fileExe;
		
		$config['file_name'] = $testcert;
		
		$this->load->library('upload', $config);	
		
		if ( ! $this->upload->do_upload('test_cert')) {
			$error = array('error' => $this->upload->display_errors());
		}else { 
			$data1 = array('upload_data' => $this->upload->data()); 
		}
		
		$data = array();
		
		$this->test_certm->insert_po_sub_lvl2($data, $testcert);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub_lvl2',$data);
	}
}