<?php
   class Scr_poc extends CI_Controller{  
   
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('scr_pom');
		user_act();
	}
	 
	public function index(){
		$data['h']=$this->scr_pom->select_prqit_prno();
		$this->load->view('admin/scr_create_po', $data); 
	} 
	   
	/******* Create Purchase Request Controller *******/
	
	public function create_po(){  
		$data['h']=$this->scr_pom->select_prqit_prno();
		$this->load->view('admin/scr_create_po', $data);  
	}
	  
	/******* View Purchase Request Controller *******/
	public function view_po(){
		$selectpr = $_REQUEST['q'];
		$data['v']=$this->scr_pom->view_poqit_pono($selectpr);
		$this->load->view('admin/scr_view_po',$data);
	} 	  
}