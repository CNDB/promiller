<?php
class Erpnotauthpoc extends CI_Controller{  
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();  
		$this->load->model('erpnotauthpom');
		$this->load->library('email');
	    $this->load->helper(array('form'));
		user_act();
	}
	  
	public function view_po_lvl2(){
		$selectpr = $this->uri->segment(3);
		$data['view_po']=$this->erpnotauthpom->po_view_lvl2($selectpr);
		$this->load->view('admin/erpnotauthpo_view',$data);
	} 
	  
	public function insert_po_sub_lvl2(){ 
		$this->email->set_mailtype("html");
		$list_email = $this->input->post('to_mail');
		$subject = $this->input->post('subject');
		$this->email->from('sandeep.tak@tipl.com');
		$this->email->to($list_email);
		$this->email->cc('sandeep.tak@tipl.com, purchase@tipl.com, planning@tipl.com');
		$this->email->bcc('it.software@tipl.com');
		$this->email->subject($subject);
		$po_num = $this->input->post('po_num');
		$po_date = $this->input->post('po_date');
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		$purchase_order_date = "<b>Our PO Date - ".$po_date."</b><br />";
		
		$message_text = "
		<p style='text-align:left;'>Sir/Madam,<br /><br />
		Please Find the attached Purchase Order.<br /><br />
		Thank You<br /><br />
		Regards,<br /><br />
		<b>Sandeep Tak</b><br />
		<b>Toshniwal Industries Private Limited, Ajmer</b><br />
		<b>Mo: 09352559002</b></p><br />
		";
		$message = $purchase_order.$purchase_order_date.$message_text;
		$base_url = base_url();
		$attch =  $base_url."/uploads/pdf/".$po_num."_po.pdf";
		$attch1 = $base_url."/uploads/pdf/foreign/".$po_num."_po.pdf";
		$this->email->attach($attch, $attch1);
		$this->email->message($message);
		$this->email->send();
		//email function1 ends
		$data = array();
		$this->erpnotauthpom->insert_po_sub_lvl2($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_po_sub_lvl2',$data);
	}
}