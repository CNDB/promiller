<?php
class Po_reportc extends CI_Controller{  
   
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('po_reportm');
		user_act();
	}
	
	public function index(){
		$data['all_po']=$this->po_reportm->all_po();
		$this->load->view('admin/po_report_main', $data); 
	}
	
	public function po_details(){
		$po_num = $this->uri->segment(3);
		$data['view_po']=$this->po_reportm->view_po($po_num);
		$this->load->view('admin/po_details', $data); 
	} 

}

