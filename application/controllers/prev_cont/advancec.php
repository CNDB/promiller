<?php
class Advancec extends CI_Controller  
{   
	function __construct()
	{
		parent::__construct();
		user_act();
	}
	
	public function index()  
	{  
		$this->load->helper('url');
		
		$this->load->view('admin/advance_view'); 
	}
	
	public function advance_view()  
	{  
		$this->load->helper('url');
		
		$this->load->database();
		
		$this->load->model('advancem');
		
		$po_num = $this->uri->segment(3);
		
		$data['view_po']= $this->advancem->po_details($po_num);
		
		$this->load->view('admin/advance_view',$data); 
	}
	
	public function insert_advance()  
	{  
		$this->load->helper('url');
		
		$this->load->database();
		
		$this->load->model('advancem');
		
		$data = array();
		
		$this->advancem->insert_advance($data);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub',$data); 
	}
}
