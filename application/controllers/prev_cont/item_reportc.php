<?php
class Item_reportc extends CI_Controller{  
   
   	function __construct()
	 {
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('item_reportm');
		 
		 user_act();
	 }
	 
	public function index()  
	{    
		$data['item_details']=$this->item_reportm->item_details();
		
		$this->load->view('admin/item_report_view', $data); 
	}
}

