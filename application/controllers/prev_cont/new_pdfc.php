<?php
class New_pdfc extends CI_Controller {  
	function __construct(){
	  parent::__construct();
	  $this->load->helper('url');
	  $this->load->database();
	  $this->load->model('new_pdfm');
	  user_act(); 
	}
	 
	public function index(){  
	  $this->load->view('admin/new_pdf_view'); 
	}
	
	public function view_po_gst(){
	  $this->load->library('mypdf');
	  $po_num = $this->uri->segment(3);
	  $pdf_name = $po_num.".pdf";
	  $data['view_po_pdf']=$this->new_pdfm->view_po_pdf($po_num);
	  $data['view_po_pdf_payterm']=$this->new_pdfm->view_po_pdf_payterm($po_num);
	  $data['view_po_pdf_item']=$this->new_pdfm->view_po_pdf_item($po_num);
	  //CGST TAX
	  $data['view_po_pdf_cgst']=$this->new_pdfm->view_po_pdf_cgst($po_num);
	  //SGST TAX
	  $data['view_po_pdf_sgst']=$this->new_pdfm->view_po_pdf_sgst($po_num);
	  //IGST TAX
	  $data['view_po_pdf_igst']=$this->new_pdfm->view_po_pdf_igst($po_num);
	  $data['doc_lvl_notes']=$this->new_pdfm->doc_lvl_notes($po_num);
	  $data['view_po_pdf_cst']=$this->new_pdfm->view_po_pdf_cst($po_num);
	  $data['freight_charges']=$this->new_pdfm->freight_charges($po_num);
	  $data['handling_charges']=$this->new_pdfm->handling_charges($po_num);
	  $data['packing_charges']=$this->new_pdfm->packing_charges($po_num);
	  $data['view_po_pdf_excise']=$this->new_pdfm->view_po_pdf_excise($po_num);
	  $data['view_po_pdf_ser']=$this->new_pdfm->view_po_pdf_ser($po_num);
	  $data['view_po_pdf_vat']=$this->new_pdfm->view_po_pdf_vat($po_num);
	  $data['doc_lvl_charges']=$this->new_pdfm->doc_lvl_charges($po_num);
	  $data['doc_lvl_discount']=$this->new_pdfm->doc_lvl_discount($po_num);
	  //MC
	  $data['mc_clause']=$this->new_pdfm->mc_clause($po_num);
	  //DI
	  $data['di_clause']=$this->new_pdfm->di_clause($po_num);
	  //PO Status
	  $data['po_status']=$this->new_pdfm->po_status($po_num);
	  
	  $data['insert_po_pdf_name'] = $this->new_pdfm->insert_po_pdf_name($po_num, $pdf_name);
	  
	  $this->load->view('admin/new_pdf_view_gst_new',$data);
	}
	
	//Foreign PO
	public function view_po_gst_foreign(){
	  $this->load->library('mypdf');
	  $po_num = $this->uri->segment(3);
	  $pdf_name = $po_num.".pdf";
	  $data['view_po_pdf']=$this->new_pdfm->view_po_pdf($po_num);
	  $data['view_po_pdf_payterm']=$this->new_pdfm->view_po_pdf_payterm($po_num);
	  $data['view_po_pdf_item']=$this->new_pdfm->view_po_pdf_item($po_num);
	  //CGST TAX
	  $data['view_po_pdf_cgst']=$this->new_pdfm->view_po_pdf_cgst($po_num);
	  //SGST TAX
	  $data['view_po_pdf_sgst']=$this->new_pdfm->view_po_pdf_sgst($po_num);
	  //IGST TAX
	  $data['view_po_pdf_igst']=$this->new_pdfm->view_po_pdf_igst($po_num);
	  $data['doc_lvl_notes']=$this->new_pdfm->doc_lvl_notes($po_num);
	  $data['view_po_pdf_cst']=$this->new_pdfm->view_po_pdf_cst($po_num);
	  $data['freight_charges']=$this->new_pdfm->freight_charges($po_num);
	  $data['handling_charges']=$this->new_pdfm->handling_charges($po_num);
	  $data['packing_charges']=$this->new_pdfm->packing_charges($po_num);
	  $data['view_po_pdf_excise']=$this->new_pdfm->view_po_pdf_excise($po_num);
	  $data['view_po_pdf_ser']=$this->new_pdfm->view_po_pdf_ser($po_num);
	  $data['view_po_pdf_vat']=$this->new_pdfm->view_po_pdf_vat($po_num);
	  $data['doc_lvl_charges']=$this->new_pdfm->doc_lvl_charges($po_num);
	  $data['doc_lvl_discount']=$this->new_pdfm->doc_lvl_discount($po_num);
	  //MC
	  $data['mc_clause']=$this->new_pdfm->mc_clause($po_num);
	  //DI
	  $data['di_clause']=$this->new_pdfm->di_clause($po_num);
	  //PO Status
	  $data['po_status']=$this->new_pdfm->po_status($po_num);
	  
	  $data['insert_po_pdf_name'] = $this->new_pdfm->insert_po_pdf_name($po_num, $pdf_name);
	  
	  $this->load->view('admin/new_pdf_view_gst_new_foriegn',$data);
	}
	
	public function view_po_gst_foreign_desc_edit(){
	  $this->load->library('mypdf');
	  $po_num = $this->uri->segment(3);
	  $pdf_name = $po_num.".pdf";
	  $data['view_po_pdf']=$this->new_pdfm->view_po_pdf($po_num);
	  $data['view_po_pdf_payterm']=$this->new_pdfm->view_po_pdf_payterm($po_num);
	  $data['view_po_pdf_item']=$this->new_pdfm->view_po_pdf_item($po_num);
	  //CGST TAX
	  $data['view_po_pdf_cgst']=$this->new_pdfm->view_po_pdf_cgst($po_num);
	  //SGST TAX
	  $data['view_po_pdf_sgst']=$this->new_pdfm->view_po_pdf_sgst($po_num);
	  //IGST TAX
	  $data['view_po_pdf_igst']=$this->new_pdfm->view_po_pdf_igst($po_num);
	  $data['doc_lvl_notes']=$this->new_pdfm->doc_lvl_notes($po_num);
	  $data['view_po_pdf_cst']=$this->new_pdfm->view_po_pdf_cst($po_num);
	  $data['freight_charges']=$this->new_pdfm->freight_charges($po_num);
	  $data['handling_charges']=$this->new_pdfm->handling_charges($po_num);
	  $data['packing_charges']=$this->new_pdfm->packing_charges($po_num);
	  $data['view_po_pdf_excise']=$this->new_pdfm->view_po_pdf_excise($po_num);
	  $data['view_po_pdf_ser']=$this->new_pdfm->view_po_pdf_ser($po_num);
	  $data['view_po_pdf_vat']=$this->new_pdfm->view_po_pdf_vat($po_num);
	  $data['doc_lvl_charges']=$this->new_pdfm->doc_lvl_charges($po_num);
	  $data['doc_lvl_discount']=$this->new_pdfm->doc_lvl_discount($po_num);
	  //MC
	  $data['mc_clause']=$this->new_pdfm->mc_clause($po_num);
	  //DI
	  $data['di_clause']=$this->new_pdfm->di_clause($po_num);
	  //PO Status
	  $data['po_status']=$this->new_pdfm->po_status($po_num);
	  
	  //$data['insert_po_pdf_name'] = $this->new_pdfm->insert_po_pdf_name($po_num, $pdf_name);
	  $this->load->view('admin/new_pdf_view_gst_new_foriegn_desc_edit',$data);
	}	 
}