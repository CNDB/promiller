<?php
class Mcdi_approvalc extends CI_Controller{  
   
	function __construct(){
		
		parent::__construct();
		
		$this->load->helper('url');
		  
		$this->load->database(); 
		  
		$this->load->model('mcdi_approvalm');
		
		$this->load->helper(array('form'));
		
		$this->load->library('email');
		 
		$this->email->set_mailtype("html");
		
		user_act();
	}
	
	//MC & DI Approval From Reporting Authority 
	
	public function mcdi_ra(){
		
		$po_num = $this->uri->segment(3);
		
		$data['max_amend_no'] = $this->mcdi_approvalm->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->mcdi_approvalm->view_po_details($po_num, $amend_no);
		
		$this->load->view('admin/mcdi_ra_view', $data); 
	} 
	
	public function mcdi_ra_submit(){
		
		$po_num = $this->input->post('po_num');
		
		$approval = $this->input->post('approval');
		
		$ra_rmks = $this->input->post('ra_rmks');
		 
		$user_name = $_SESSION['username'];
		
		$from_email = $_SESSION['username']."@tipl.com";
		
		$this->email->to('purchase@tipl.com, planning@tipl.com, it.software@tipl.com');
		
		$this->email->from($from_email, $user_name);
		
		$this->email->cc($from_email);
		
		$subject = $po_num." - Without MC & DI Approval From Reporting Authority";
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$remarks = "<b>Approval / Disapproval Remarks - ".$ra_rmks."</b><br />";
		
		if($approval == 'APPROVE'){
		
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Approved by Reporting Authority. Pending For  Approval From Project Incharge.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
		
		} else if($approval == 'DISAPPROVE'){
			
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Disapproved by Reporting Authority. Please Resend it for approval.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
			
		}
		
		$message = $purchase_order.$remarks.$message_text;
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$this->email->send();
		
		$data = array();
		
		$this->mcdi_approvalm->mcdi_ra_submit($data);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
		
	}
	
	//MC & DI Approval From Project Incharge
	public function mcdi_pi(){
		
		$po_num = $this->uri->segment(3);
		
		$data['max_amend_no'] = $this->mcdi_approvalm->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->mcdi_approvalm->view_po_details($po_num, $amend_no);
		
		$this->load->view('admin/mcdi_pi_view', $data); 
	} 
	
	public function mcdi_pi_submit(){
		
		$po_num = $this->input->post('po_num');
		
		$approval = $this->input->post('approval');
		
		$pi_rmks = $this->input->post('pi_rmks');
		 
		$user_name = $_SESSION['username'];
		
		$from_email = $_SESSION['username']."@tipl.com";
		
		$this->email->to('neeraj.pandey@tipl.com, purchase@tipl.com, planning@tipl.com, it.software@tipl.com');
		
		$this->email->from($from_email, $user_name);
		
		$this->email->cc($from_email);
		
		$subject = $po_num." - Without MC & DI Approval From Project Incharge";
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$remarks = "<b>Approval / Disapproval Remarks - ".$pi_rmks."</b><br />";
		
		if($approval == 'APPROVE'){
		
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Approved by Project Incharge. Pending For  Approval From Cash Flow Incharge LVL1.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
		
		} else if($approval == 'DISAPPROVE'){
			
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Disapproved by Project Incharge. Please Resend it for approval.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
			
		}
		
		$message = $purchase_order.$remarks.$message_text;
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$this->email->send();
		
		$data = array();
		
		$this->mcdi_approvalm->mcdi_pi_submit($data);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
		
	}
	
	//MC & DI Approval From Cash Flow Incharge LVL 1
	public function mcdi_cf1(){
		
		$po_num = $this->uri->segment(3);
		
		$data['max_amend_no'] = $this->mcdi_approvalm->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->mcdi_approvalm->view_po_details($po_num, $amend_no);
		
		$this->load->view('admin/mcdi_cf1_view', $data); 
	} 
	
	public function mcdi_cf1_submit(){
		
		$po_num = $this->input->post('po_num');
		
		$approval = $this->input->post('approval');
		
		$cf1_rmks = $this->input->post('cf1_rmks');
		 
		$user_name = $_SESSION['username'];
		
		$from_email = $_SESSION['username']."@tipl.com";
		
		$this->email->to('abhinav.toshniwal@tipl.com, purchase@tipl.com, planning@tipl.com, it.software@tipl.com');
		
		$this->email->from($from_email, $user_name);
		
		$this->email->cc($from_email);
		
		$subject = $po_num." - Without MC & DI Approval From Cash Flow Incharge LVL1";
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$remarks = "<b>Approval / Disapproval Remarks - ".$cf1_rmks."</b><br />";
		
		if($approval == 'APPROVE'){
		
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Approved by Cash Flow Incharge LVL1. Pending For  Approval From Cash Flow Incharge LVL2.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
		
		} else if($approval == 'DISAPPROVE'){
			
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Disapproved by Cash Flow Incharge LVL1. Please Resend it for approval.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
			
		}
		
		$message = $purchase_order.$remarks.$message_text;
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$this->email->send();
		
		$data = array();
		
		$this->mcdi_approvalm->mcdi_cf1_submit($data);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
		
	}
	
	//MC & DI Approval From Cash Flow Incharge LVL 2
	public function mcdi_cf2(){
		
		$po_num = $this->uri->segment(3);
		
		$data['max_amend_no'] = $this->mcdi_approvalm->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->mcdi_approvalm->view_po_details($po_num, $amend_no);
		
		$this->load->view('admin/mcdi_cf2_view', $data); 
	} 
	
	public function mcdi_cf2_submit(){
		
		$po_num = $this->input->post('po_num');
		
		$approval = $this->input->post('approval');
		
		$cf2_rmks = $this->input->post('cf2_rmks');
		 
		$user_name = $_SESSION['username'];
		
		$from_email = $_SESSION['username']."@tipl.com";
		
		$this->email->to('purchase@tipl.com, planning@tipl.com, it.software@tipl.com');
		
		$this->email->from($from_email, $user_name);
		
		$this->email->cc($from_email);
		
		$subject = $po_num." - Without MC & DI Approval From Cash Flow Incharge LVL2";
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$remarks = "<b>Approval / Disapproval Remarks - ".$cf2_rmks."</b><br />";
		
		if($approval == 'APPROVE'){
		
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Approved by Cash Flow Incharge LVL2. Pending For  GRCPT Creation In ERP.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
		
		} else if($approval == 'DISAPPROVE'){
			
			$message_text = "
				<p style='text-align:left;'>Sir/Madam,<br><br>
				Purchase Order Disapproved by Cash Flow Incharge LVL2. Please Resend it for approval.<br><br>
				<b>Regards,<br>TIPL Administrator</b><br>
				<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
			";
			
		}
		
		$message = $purchase_order.$remarks.$message_text;
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$this->email->send();
		
		$data = array();
		
		$this->mcdi_approvalm->mcdi_cf2_submit($data);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
		
	}	
	
	//Disapproved MC & DI Cases
	public function mcdi_disapproved(){
		
		$po_num = $this->uri->segment(3);
		
		$data['max_amend_no'] = $this->mcdi_approvalm->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->mcdi_approvalm->view_po_details($po_num, $amend_no);
		
		$this->load->view('admin/mcdi_disapproved_view', $data); 
	}
	
	public function mcdi_disapproved_submit(){
		
		$po_num = $this->input->post('po_num');
		
		$approval = $this->input->post('approval');
		
		$dis_rmks = $this->input->post('dis_rmks');
		 
		$user_name = $_SESSION['username'];
		
		$from_email = $_SESSION['username']."@tipl.com";
		
		$this->email->to('purchase@tipl.com, planning@tipl.com, it.software@tipl.com');
		
		$this->email->from($from_email, $user_name);
		
		$this->email->cc($from_email);
		
		$subject = $po_num." - Without MC & DI Send For Approval To Reporting Authority";
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$remarks = "<b>Approval / Disapproval Remarks - ".$dis_rmks."</b><br />";
		
		$message_text = "
			<p style='text-align:left;'>Sir/Madam,<br><br>
			Purchase Order is send for approval to Reporting Authority.<br><br>
			<b>Regards,<br>TIPL Administrator</b><br>
			<b>Toshniwal Industries Private Limited, Ajmer</b></p><br>
		";
		
		$message = $purchase_order.$remarks.$message_text;
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$this->email->send();
		
		$data = array();
		
		$this->mcdi_approvalm->mcdi_disapproved_submit($data);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
		
	}   
}

