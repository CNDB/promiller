<?php
class Pending_poc extends CI_Controller  
{   
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('pending_pom');
		
		user_act();
	}
	
	public function index(){
		$data['j'] = $this->pending_pom->select_prqit_prno2();
		$this->load->view('admin/pending_po_main',$data);
	}
	
	//LIVE PURCHASE ORDERS
	
	public function pending_po_list(){ 
		$this->load->view('admin/pending_po_list',$data);
	}
	
	public function pending_po_list_ajax(){
		$this->load->view('admin/pending_po_list_ajax',$data);
	}
	
	//Detailed Report
	public function pending_po_list_new(){ 
		$this->load->view('admin/pending_po_list_new',$data);
	}
	
	//PR Detail Page
	
	public function pending_pr_list(){
		$this->load->view('admin/pending_pr_list');
	}
	
	public function pending_pr_list_ajax(){
		$this->load->view('admin/pending_pr_list_ajax');
	}
	
	/*************** NOT IN LIVE PO ***************/
	
	public function pending_po_list_notlive(){
		$url_param = $this->uri->segment(3);
		
		if($url_param == 'capitalgoods_notlive_draft_new'){ //Draft PO List
		
			//$data['get_data']= $this->pending_pom->capitalgoods_notlive_draft_new();
		
		} else if($url_param == 'sensors_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->sensors_notlive_draft_new();
		
		} else if($url_param == 'security_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->security_notlive_draft_new();
		
		} else if($url_param == 'instrumentation_notlive_draft_new') { 
			
			$data['get_data']= $this->pending_pom->instrumentation_notlive_draft_new();
		
		} else if($url_param == 'tools_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->tools_notlive_draft_new();
		
		} else if($url_param == 'prodcon_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->prodcon_notlive_draft_new();
		
		} else if($url_param == 'service_notlive_draft_new') {
			
			//$data['get_data']= $this->pending_pom->service_notlive_draft_new();
		
		} else if($url_param == 'packing_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->packing_notlive_draft_new();
		
		}  else if($url_param == 'avazonic_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->avazonic_notlive_draft_new();
		
		} else if($url_param == 'nonprodcon_notlive_draft_new') {
			
			$data['get_data']= $this->pending_pom->nonprodcon_notlive_draft_new();
		
		} else if($url_param == 'all_notlive_draft_new') { 
			
			$data['get_data']= $this->pending_pom->all_notlive_draft_new();
		
		}  else if($url_param == 'capitalgoods_notlive_fresh_new') {  //Fresh PO
			
			//$data['get_data']= $this->pending_pom->capitalgoods_notlive_fresh_new();
		
		} else if($url_param == 'sensors_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->sensors_notlive_fresh_new();
		
		} else if($url_param == 'security_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->security_notlive_fresh_new();
		
		} else if($url_param == 'instrumentation_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->instrumentation_notlive_fresh_new();
		
		} else if($url_param == 'tools_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->tools_notlive_fresh_new();
		
		} else if($url_param == 'prodcon_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->prodcon_notlive_fresh_new();
		
		} else if($url_param == 'service_notlive_fresh_new') {
			
			//$data['get_data']= $this->pending_pom->service_notlive_fresh_new();
		
		} else if($url_param == 'packing_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->packing_notlive_fresh_new();
		
		} else if($url_param == 'avazonic_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->avazonic_notlive_fresh_new();
		
		} else if($url_param == 'nonprodcon_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->nonprodcon_notlive_fresh_new();
		
		} else if($url_param == 'all_notlive_fresh_new') {
			
			$data['get_data']= $this->pending_pom->all_notlive_fresh_new();
		
		} else if($url_param == 'capitalgoods_notlive_open_new') {  // Open PO
			
			//$data['get_data']= $this->pending_pom->capitalgoods_notlive_open_new();
		
		} else if($url_param == 'sensors_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->sensors_notlive_open_new();
		
		} else if($url_param == 'security_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->security_notlive_open_new();
		
		} else if($url_param == 'instrumentation_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->instrumentation_notlive_open_new();
		
		} else if($url_param == 'tools_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->tools_notlive_open_new();
		
		} else if($url_param == 'prodcon_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->prodcon_notlive_open_new();
		
		} else if($url_param == 'service_notlive_open_new') {
			
			//$data['get_data']= $this->pending_pom->service_notlive_open_new();
		
		} else if($url_param == 'packing_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->packing_notlive_open_new();
		
		} else if($url_param == 'avazonic_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->avazonic_notlive_open_new();
		
		} else if($url_param == 'nonprodcon_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->nonprodcon_notlive_open_new();
		
		} else if($url_param == 'all_notlive_open_new') {
			
			$data['get_data']= $this->pending_pom->all_notlive_open_new();
		
		}  else if($url_param == 'capitalgoods_notlive_amend_new') {  // Amendment PO
			
			//$data['get_data']= $this->pending_pom->capitalgoods_notlive_amend_new();
		
		} else if($url_param == 'sensors_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->sensors_notlive_amend_new();
		
		} else if($url_param == 'security_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->security_notlive_amend_new();
		
		} else if($url_param == 'instrumentation_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->instrumentation_notlive_amend_new();
		
		} else if($url_param == 'tools_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->tools_notlive_amend_new();
		
		} else if($url_param == 'prodcon_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->prodcon_notlive_amend_new();
		
		}  else if($url_param == 'service_notlive_amend_new') {
			
			//$data['get_data']= $this->pending_pom->service_notlive_amend_new();
		
		} else if($url_param == 'packing_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->packing_notlive_amend_new();
		
		}  else if($url_param == 'avazonic_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->avazonic_notlive_amend_new();
		
		} else if($url_param == 'nonprodcon_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->nonprodcon_notlive_amend_new();
		
		} else if($url_param == 'all_notlive_amend_new') {
			
			$data['get_data']= $this->pending_pom->all_notlive_amend_new();
		
		} else if($url_param == 'all_capitalgoods_notlive_new') { //Total PO Count 
			
			//$data['get_data']= $this->pending_pom->all_capitalgoods_notlive_new();
		
		} else if($url_param == 'all_sensors_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_sensors_notlive_new();
		
		} else if($url_param == 'all_security_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_security_notlive_new();
		
		} else if($url_param == 'all_instrument_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_instrument_notlive_new();
		
		} else if($url_param == 'all_tools_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_tools_notlive_new();
		
		} else if($url_param == 'all_prodcon_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_prodcon_notlive_new();
		
		} else if($url_param == 'all_service_notlive_new') {
			
			//$data['get_data']= $this->pending_pom->all_service_notlive_new();
		
		} else if($url_param == 'all_packing_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_packing_notlive_new();
		
		} else if($url_param == 'all_avazonic_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_avazonic_notlive_new();
		
		} else if($url_param == 'all_nonprodcon_notlive_new') {
			
			$data['get_data']= $this->pending_pom->all_nonprodcon_notlive_new();
		
		} else if($url_param == 'all_total_po_new') {
			
			$data['get_data']= $this->pending_pom->all_total_po_new();
		
		}    
		
		$this->load->view('admin/pending_po_list_notlive',$data);
	}
	
	public function not_live_pr(){
		$this->load->view('admin/not_live_pr_new');
	}
	
	public function not_live_po(){
		$this->load->view('admin/not_live_po_view');
	}
	
	/*************** NOT IN LIVE PO ***************/	  
}
