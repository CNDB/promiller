<?php
class Lead_time_updatec extends CI_Controller{  
   
	function __construct(){
		 parent::__construct();  
		 $this->load->helper('url');
		 $this->load->database(); 
		 $this->load->model('lead_time_updatem');
		 $this->load->helper(array('form'));
		 $this->load->library('email');
		 $this->email->set_mailtype("html");
		 
		 user_act();
	 }
	 
	public function index(){
		$po_num = $this->uri->segment(3);
		
		$data['max_amend_no'] = $this->lead_time_updatem->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->lead_time_updatem->view_po_details($po_num, $amend_no);
		
		$this->load->view('admin/lead_time_update_view', $data); 
	} 
	
	public function insert_lead_time_det(){
		 $po_num = $this->input->post('po_num');
		
		 $supp_lead_time = $this->input->post('supp_lead_time');
		
		 $remarks = $this->input->post('remarks');
		
		 $status = "Lead Time Updated";
		
		 $created_by = $_SESSION['username'];
		
		 $created_date = date("Y-m-d H:i:s");
		
		 $data = array();
		
		 $this->lead_time_updatem->lead_time_update($data);	  
		  
		 $data['message'] = 'Data Inserted Successfully';
		
		 //Mail Code Starts
		 
		 $user_name = $_SESSION['username'];
		 
		 $from_email = $_SESSION['username']."@tipl.com";
		 
		 $to_email = "planning@tipl.com, purchase@tipl.com";
		 
		 $data['mail_table']= $this->lead_time_updatem->mail_table($po_num);
		 
		 $msg_table = "<table border='1' cellpadding='2' cellspacing='0'>
							<tr>
								<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
								<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
							</tr>
					   </table><br/><br/>
		 			   <table cellpadding='1' cellspacing='1' border='1' style='border:solid 1px black; font-size:12px; padding:10px; border-radius:5px;'>
						<tr style='background-color:#CFF'>
							<td><b>Supplier Name</b></td>
							<td><b>Need Date</b></td>
							<td><b>Lead Time (AVG)</b></td>
							<td><b>Item Code</b></td>
							<td><b>Item Description</b></td>
							<td><b>Carrier Name</b></td>
							<td><b>Freight Terms</b></td>
							<td><b>Payment Terms</b></td>
							<td><b>Last 1 Yr Con. FY-".$last_fin_year."</b></td>
							<td><b>Current Stock</b></td>
							<td><b>Reservation Qty</b></td>
							<td><b>For Stk Qty</b></td>
							<td><b>UOM</b></td>
							<td><b>Last Price</b></td>
							<td><b>Current Price</b></td>
							<td><b>Costing</b></td>
							<td><b>Costing Currency</b></td>
							<td><b>Category</b></td>
							<td><b>PM-Group</b></td>
							<td><b>Current Year Cons</b></td>
							<td><b>Reorder Level</b></td>
							<td><b>Reorder Qty</b></td>
							<td><b>Last Supplier</b></td>
							<td><b>Last Supplier Lead Time</b></td>
							<td><b>Last Supplier Rate</b></td>
							<td><b>Type Of PR</b></td>
							<td><b>Why Special Remarks</b></td>
							<td><b>Project Name</b></td>
							<td><b>ATAC No</b></td>
							<td><b>Item Total Value</b></td>
							<td><b>PO Value</b></td>
							<td><b>Remarks</b></td>
						</tr>";
		  
		  foreach($data['mail_table']->result_array() AS $row){
			 $po_supp_name = $row['po_supp_name'];
			 $ipr_need_date1 = $row['po_need_date'];
			 $ipr_need_date = substr($ipr_need_date1,0,11);
			 $po_item_code = $row['po_item_code'];
			 $po_itm_desc = $row['po_itm_desc'];
			 $carrier_name = $row['carrier_name'];
			 $freight = $row['freight'];
			 $payterm = $row['payterm'];
			 $lastyr_cons = $row['lastyr_cons'];
			 $current_stk = $row['current_stk'];
			 $reservation_qty = $row['reservation_qty'];
			 $for_stk_quantity = $row['for_stk_quantity'];
			 $po_uom = $row['po_uom'];
			 $last_price = $row['last_price'];
			 $current_price = $row['current_price'];
			 $total_item_value = $row['total_item_value'];
			 $po_total_value = $row['po_total_value'];
			 $po_spcl_inst_frm_supp = $row['po_spcl_inst_frm_supp'];
			 $category = $row['ipr_category'];
			 $pm_group = $row['ipr_pm_group'];
			 
			 //new feilds
			 $current_yr_con = $row['current_yr_con'];
			 $color = $row['color'];
			 $reorder_level = $row['reorder_level'];
			 $reorder_qty = $row['reorder_qty'];
			 $last_supplier = $row['last_supplier'];
			 $last_supplier_lead_time = $row['last_supplier_lead_time'];
			 $last_supplier_rate = $row['last_supplier_rate'];
			 $type_of_pr = $row['type_of_pr'];
			 $project_name = $row['project_name'];
			 $atac_no = $row['atac_no'];
			 $costing = $row['costing'];
			 $costing_currency = $row['costing_currency'];
			 $why_spcl_rmks = $row['why_spcl_rmks'];
				 
		 $msg_table .= "<tr>
							<td>".$po_supp_name."</td>
							<td style='background-color:".$color."'>".$ipr_need_date."</td>
							<td>".number_format($supplier_avg_lead_time,2)."</td>
							<td>".$po_item_code."</td>
							<td>".$po_itm_desc."</td>
							<td>".$carrier_name."</td>
							<td>".$freight."</td>
							<td>".$payterm."</td>
							<td>".number_format($lastyr_cons,2)."</td>
							<td>".number_format($current_stk,2)."</td>
							<td>".number_format($reservation_qty,2)."</td>
							<td>".number_format($for_stk_quantity,2)."</td>
							<td>".$po_uom."</td>
							<td>".number_format($last_price,2)."</td>
							<td>".number_format($current_price,2)."</td>
							<td>".$costing."</td>
							<td>".$costing_currency."</td>
							<td>".$category."</td>
							<td>".$pm_group."</td>
							<td>".number_format($current_yr_con,2)."</td>
							<td>".number_format($reorder_level,2)."</td>
							<td>".number_format($reorder_qty,2)."</td>
							<td>".$last_supplier."</td>
							<td>".$last_supplier_lead_time."</td>
							<td>".number_format($last_supplier_rate,2)."</td>
							<td>".$type_of_pr."</td>
							<td>".$why_spcl_rmks."</td>
							<td>".$project_name."</td>
							<td>".$atac_no."</td>
							<td>".number_format($total_item_value,2)."</td>
							<td>".number_format($po_total_value,2)."</td>
							<td>".$po_spcl_inst_frm_supp."</td>
					   </tr>";
		  }
		  
		 $msg_table .="</table>";
			  
		 $this->email->to($to_email);
		 
		 $this->email->from($from_email, $user_name);
		 
		 $this->email->cc($from_email);
		 
		 $this->email->bcc('it.software@tipl.com');
		 
		 $po_subject = $po_num.' - Lead Time Updated By Purchase';
		 
		 $this->email->subject($po_subject);
		 	  
		 $message = "<b>PO No. - ".$po_num."</b><br /><br />".$msg_table."<br><b>Thanks & Regards, </b><br/><br /><b style='text-transform:uppercase;'>".$user_name."</b>";
			
		 $this->email->message($message);
			
		 if(!$this->email->send()){
		 	echo $this->email->print_debugger();
		 }
		 
		 //Mail Code Ends
		
		 $this->load->view('admin/insert_po_sub');
	} 
  
}

