<?php
class Deliveryc extends CI_Controller{  
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();  
		$this->load->model('deliverym');
		user_act(); 
	}
	
	public function create_po_lvl2(){  
		$data['m']=$this->deliverym->select_prqit_prno_lvl2();
		$this->load->view('admin/delivery_create', $data);  
	}
	
	public function view_po_lvl2(){
		$selectpr = $_REQUEST['q'];
		$data['max_amend_no'] = $this->deliverym->max_amend_no($selectpr);
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']=$this->deliverym->po_view_lvl2($selectpr, $amend_no);
		$this->load->view('admin/delivery_view',$data);
	} 
	
	public function insert_po_sub_lvl2(){	
		$data = array();
		$this->deliverym->insert_po_sub_lvl2($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_po_sub_lvl2',$data);
	}
}