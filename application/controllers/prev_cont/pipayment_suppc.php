<?php
class Pipayment_suppc extends CI_Controller{  
   
	function __construct(){
		parent::__construct();
		
		$this->load->helper('url');
		
		$this->load->database(); 
		 
		$this->load->model('pipayment_suppm');
		
		user_act();
	}
	 
	public function index(){  
		$data['h']=$this->pipayment_suppm->select_prqit_prno(); 
		$this->load->view('admin/pipayment_supp_create', $data); 
	} 
	   
	/**************** Create Purchase Request Controller ******/
	  
	public function create_po(){  
		$data['h']=$this->pipayment_suppm->select_prqit_prno();
		$this->load->view('admin/pipayment_supp_create', $data);  
	}
	  
	/********** View Purchase Request Controller ***************/
		  
	public function view_po(){
		$selectpr = $_REQUEST['q'];
		$data['max_amend_no'] = $this->pipayment_suppm->max_amend_no($selectpr);
	
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
			
		$data['view_po']=$this->pipayment_suppm->view_poqit_pono($selectpr, $amend_no);
		$data['view_payment_details'] = $this->pipayment_suppm->view_payment_details($selectpr);	  
		$this->load->view('admin/pipayment_supp_view',$data);
	}
	
	/******* Insert Or Update values in Database ********/
	
	public function insert_po_sub(){
		$this->load->helper(array('form'));
		
		$config['upload_path']   = './uploads/'; 
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|'; 
		$config['max_size']      = 100000000; 
		$config['max_width']     = 102400; 
		$config['max_height']    = 768000;
		$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
		$filename = strtolower($_FILES["pi_uploaded_cheque_img"]["name"]);
		$fileExe  = substr($filename, strrpos($filename,'.'));
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		$file = basename($filename, ".".$ext);
		$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
		$NewFileName2 = $NewFileName.'_'.$RandNumber.$fileExe;
		$config['file_name'] = $NewFileName2;
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('pi_uploaded_cheque_img')) {
			$error = array('error' => $this->upload->display_errors());
		}else { 
			$data2 = array('upload_data2' => $this->upload->data()); 
		}
		
		$data = array();
		
		$this->pipayment_suppm->insert_po_sub($data, $NewFileName2);
		
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
	
	}
   
}