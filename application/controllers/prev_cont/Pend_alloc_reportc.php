<?php
class Pend_alloc_reportc extends CI_Controller{  
   
	function __construct(){
	
	 parent::__construct();
	   
	 $this->load->helper('url');
		  
	 $this->load->database(); 
		  
	 $this->load->model('pend_alloc_reportm');
	 
	 user_act();
	}
	
	public function index(){
		
		$data['pend_alloc']=$this->pend_alloc_reportm->pend_alloc();
		
		$this->load->view('admin/header',$data);	
		$this->load->view('admin/pend_alloc_report_view',$data);
		$this->load->view('admin/footer',$data);
	}
	
	public function pend_alloc_export(){
		
		$data['pend_alloc']=$this->pend_alloc_reportm->pend_alloc();	
		$this->load->view('admin/pend_alloc_report_view_export',$data);
	}
}

