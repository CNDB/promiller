<?php
class Itemc_update extends CI_Controller{  
   
   	function __construct()
	 {
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('itemm_update');
		 
		 user_act();
	 }
	 
     public function index()  
      {    
		 $data['h']=$this->itemm_update->item_details();
		  
         $this->load->view('admin/item_view_update',$data); 
      }
}

