<?php
class Di_purchasec extends CI_Controller{ 
	function __construct(){
		 parent::__construct(); 
		 $this->load->helper('url');  
		 $this->load->database(); 	  
		 $this->load->model('di_purchasem');
		 $this->load->helper(array('form'));
		 $this->load->library('email');
		 $this->email->set_mailtype("html");
		 user_act();
	 }
	 
	public function index(){
		$po_num = $this->uri->segment(3);
		$data['max_amend_no'] = $this->di_purchasem->max_amend_no($po_num);
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
		}
		$data['view_po']=$this->di_purchasem->view_po_details($po_num, $amend_no);
		$this->load->view('admin/di_purchase_view', $data); 
	} 
	
	public function insert_di_pur(){
		$po_num = $this->input->post('po_num');
		$remarks = $this->input->post('remarks');
		$remarks = str_replace("'","",$remarks);
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d H:i:s");
		//Mail Starts
		$to_email = $this->input->post('to_mail');
		$subject = $this->input->post('subject');
		$username_table = $_SESSION['username'];
		$po_num = $this->input->post('po_num');
		$message_txt = $this->input->post('message_text');
		//Fetching Purchasor Details
		$data['pur_details']=$this->di_purchasem->pur_details($username_table);
		foreach($data['pur_details']->result_array() AS $row) {
			$pur_name = $row['emp_name'];
			$pur_mobile_no = $row['mobile_no'];
			$pur_email = $row['emp_email'];
		}
		
		//Fetching Purchasor Details
		$this->email->from($pur_email, $pur_name);
		$this->email->to($to_email);
		$cc_email = "purchase@tipl.com,".$pur_email;
		$this->email->cc($cc_email);
		$this->email->bcc('it.software@tipl.com');
		$this->email->subject($subject);
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		$message = "<b>Hello Sir/Madam,</b><br><br>".nl2br($message_txt)."<br><b>Thanks & Regards,</b><br /><br />
		<b style='text-transform:uppercase;'>".$pur_name." | PURCHASE</b><br /><br />
		<b>EMAIL - ".$pur_email."</b><br /><br />
		<b>CONTACT NO - ".$pur_mobile_no."</b><br /><br />
		<b style='text-transform:uppercase;'>Toshniwal Industries Private Limited, Ajmer</b></p><br />";
		
		$base_url = base_url();
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		$this->email->attach($attch);
		$this->email->message($message);
		$data = array();
		$this->di_purchasem->insert_di_pur($data);
		$data['message'] = 'Data Inserted Successfully';
		
		if(!$this->email->send()){
			echo $this->email->print_debugger();
			die;	
		}
		//Mail Ends
		$this->load->view('admin/insert_po_sub');
	}  
}

