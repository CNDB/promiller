<?php
class Not_live_prc extends CI_Controller  
{   
	function __construct()
	{
		parent::__construct();
		user_act();
	}
	
	public function index()  
	{  
		$this->load->helper('url');
		
		$this->load->database();
		
		$this->load->model('not_live_prm');
		
		$data['pr_data']= $this->not_live_prm->pr_data();
		
		$this->load->view('admin/not_live_pr_view',$data);
	}
}
