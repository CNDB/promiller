<?php
class Auth_poc extends CI_Controller  
{  
   
	function __construct(){
		parent::__construct();
		user_act();
	}
	
	public function index(){  
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('auth_pom');
		
		$data['all']=$this->auth_pom->view_all_po();
		
		foreach($data['all']->result_array() AS $row) {
			$sup_code = $row['po_item_code'];
		}
		
		if(empty($sup_code)){
			
		} else {
			$sup_code1 = urldecode($sup_code);
			$data['p']=$this->auth_pom->procesdure_run($sup_code);
			$this->db->close();
			$this->db->initialize();
			$data['lastfivetrans1_lvl2'] = $this->auth_pom->lastfivetrans1_lvl2($sup_code);  
		}
		$this->load->view('admin/auth_po', $data); 
	}
	
	public function insert_po_sub_lvl2(){
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('auth_pom');
		$this->load->helper(array('form'));
		
		$data = array(
			'po_num' => $this ->input ->post('po_num'),
			'po_approval_lvl2' => $this->input->post('po_approval_lvl2'),
			'remarks_level2' => $this->input->post('remarks_level2')
		);
		
		$this->auth_pom->insert_po_sub_lvl2($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_po_sub_at',$data);
	} 	 
}