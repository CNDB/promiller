<?php
class Po_fy_reportc extends CI_Controller{  
   
   	function __construct(){
		 parent::__construct();
		 $this->load->helper('url');
		 $this->load->database(); 
		 $this->load->model('po_fy_reportm');
		 user_act();
	 }
	 
      public function index(){    
		 $data['all_po_2016_17']=$this->po_fy_reportm->item_details_2016_17();
		 $data['all_po_2015_16']=$this->po_fy_reportm->item_details_2015_16();
		  
         $this->load->view('admin/po_fy_report_view', $data); 
      }
	  
	  public function export(){    
		 $data['excel']=$this->po_fy_reportm->item_details();
         $this->load->view('admin/po_fy_report_export_excel', $data); 
      }
	  
	  public function pr_po_pending_report(){    
		 $data['pr_po_report']=$this->po_fy_reportm->pr_po_report(); 
         $this->load->view('admin/pr_po_pending_report', $data); 
      }
	  
	  public function pr_po_pending_report_export(){    
		 $data['pr_po_report']=$this->po_fy_reportm->pr_po_report();
         $this->load->view('admin/pr_po_pending_report_excel', $data); 
      }
}