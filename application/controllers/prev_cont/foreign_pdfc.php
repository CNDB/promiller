<?php
class Foreign_pdfc extends CI_Controller  
{  
   
	function __construct()
	{
	  parent::__construct();
	  $this->load->helper('url');
	  $this->load->database();
	  $this->load->model('foreign_pdfm');
	  user_act(); 
	}
	 
	public function index()  
	{  
	  $this->load->view('admin/foreign_pdf_view'); 
	}
		  
	public function view_po()
	{
	  $this->load->library('mypdf');
	  $po_num = $this->uri->segment(3);
	  $pdf_name = $po_num."_pdf";
	  $data['view_po_pdf']=$this->foreign_pdfm->view_po_pdf($po_num);
	  $data['view_po_pdf_payterm']=$this->foreign_pdfm->view_po_pdf_payterm($po_num);
	  $data['view_po_pdf_item']=$this->foreign_pdfm->view_po_pdf_item($po_num);
	  $data['view_po_pdf_cst']=$this->foreign_pdfm->view_po_pdf_cst($po_num);
	  $data['view_po_pdf_excise']=$this->foreign_pdfm->view_po_pdf_excise($po_num);
	  $data['view_po_pdf_ser']=$this->foreign_pdfm->view_po_pdf_ser($po_num);
	  $data['view_po_pdf_vat']=$this->foreign_pdfm->view_po_pdf_vat($po_num);
	  $data['insert_po_pdf_name'] = $this->foreign_pdfm->insert_po_pdf_name($po_num, $pdf_name);
	  $this->load->view('admin/foreign_pdf_view',$data);
	}
	 
}