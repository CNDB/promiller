<?php
class Supplier_reportc extends CI_Controller{  
   
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('supplier_reportm');
		user_act();
	}
	
	public function index(){
		 
	 $data['supp_procedure']=$this->supplier_reportm->supp_procedure();
	 
	 $this->db->close();
			
	 $this->db->initialize();
	 
	 $data['category']=$this->supplier_reportm->category();
	 
	 $data['all_supplier_data']=$this->supplier_reportm->all_supplier_data();
	  
	 $this->load->view('admin/supplier_report_main', $data);
	}
	
	public function supp_ajax(){
		
	 $category = $_REQUEST['category'];	
	 
	 $data['all_supplier_data_ajax']=$this->supplier_reportm->all_supplier_data_ajax($category);
	  
	 $this->load->view('admin/supplier_report_ajax', $data);
	 
	}

}

