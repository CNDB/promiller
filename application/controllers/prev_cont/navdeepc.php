<?php
class Navdeepc extends CI_Controller  
{   
	function __construct()
	{
		parent::__construct();
		user_act();
	}
	
	public function index()  
	{  
		$this->load->helper('url');
		
		$this->load->database();
		
		$this->load->model('navdeepm');
		
		$data['r']=$this->navdeepm->fresh_req_not_auth_erp();
		
		$this->load->view('admin/navdeep_view',$data); 
	}
		  
}
