<?php
class Pendalc extends CI_Controller{  
   
	function __construct(){
	
		parent::__construct();
		
		$this->load->helper('url');
		
		$this->load->database(); 
		
		$this->load->model('pendalm');
		
		user_act();
	}
	
	public function index(){
		$this->load->view('admin/pendal_card_search'); 
	}
	
	public function pendal_details(){
		$itemcode = $_REQUEST['item_code'];
		
		//Pendal Card Details Starts
		$data['p']=$this->pendalm->procesdure_run($itemcode);
		
		$this->db->close();
		
		$this->db->initialize();
		
		$username = $_SESSION['username'];
		
		$data['user_det'] = $this->pendalm->user_det($username);
		
		foreach($data['user_det']->result_array() AS $row){
			 $id = $row['id'];	 
	    }
		
		//Reorder Details Procedure
		$data['reorder_proc']=$this->pendalm->reorder_proc($itemcode,$id);
		
		$this->db->close();
		
		$this->db->initialize();
		
		
		//Queries
		$data['pendal'] = $this->pendalm->pendal_master($itemcode);
		
		$data['pendalstkblnc'] = $this->pendalm->pendal_master_whstkblnc($itemcode);
		
		$data['pndposopr'] = $this->pendalm->pendal_master_pndposopr($itemcode);
		
		$data['pndposopr_new'] = $this->pendalm->pendal_master_pndposopr_new($itemcode);
		
		$data['allocat'] = $this->pendalm->pendal_master_allocat($itemcode);
		
		$data['allocat_tot'] = $this->pendalm->pendal_master_allocat_tot($itemcode);
		
		$data['wipdet'] = $this->pendalm->pendal_master_wipdet($itemcode);
		
		$data['lastfivetrans'] = $this->pendalm->pendal_master_lastfivetrans($itemcode);
		
		$data['lstyrcumnrec'] = $this->pendalm->pendal_master_lstyrcumnrec($itemcode);
		
		$data['supplier'] = $this->pendalm->pendal_master_supplier($itemcode);
		
		$data['mnthwsecurryrcons'] = $this->pendalm->pendal_master_mnthwsecurryrcons($itemcode);
		
		$data['mnthwsecurryrrcpt'] = $this->pendalm->pendal_master_mnthwsecurryrrcpt($itemcode);
		
		$data['bomdetail'] = $this->pendalm->pendal_master_bomdetail($itemcode);
		
		$data['pics_entry'] = $this->pendalm->pics_entry($itemcode);
		
		$data['cut_peice_det'] = $this->pendalm->cut_peice_det($itemcode);
		
		$data['item_creation_hist'] = $this->pendalm->item_creation_hist($itemcode);
		
		$data['item_drawing'] = $this->pendalm->item_drawing($itemcode);
		
		$data['item_drawing_det'] = $this->pendalm->item_drawing_det($itemcode);
		
		$data['cons_quantity'] = $this->pendalm->cons_quantity($itemcode,$id);
		
		$data['lead_time_qry'] = $this->pendalm->lead_time_qry($itemcode,$id);
		
		//Pendal Card Details Ends 
		 
		$this->load->view('admin/pendal_card',$data);
	}	  
}