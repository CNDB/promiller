<?php
class View_poc extends CI_Controller{  
   
   	function __construct(){
		
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('view_pom');
		 
		 user_act();
	 }
	 
     public function index(){
		  
         $this->load->view('admin/view_po_search'); 
      }
	  
	 public function po_details(){
		 
		$po_num = $_REQUEST['po_number'];
		
		$data['max_amend_no'] = $this->view_pom->max_amend_no($po_num);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			$amend_no = $row['amend_no']; 	 
	    }
		
	    $data['view_po']=$this->view_pom->view_poqit_pono($po_num, $amend_no);
		
		$data['act_timing'] = $this->view_pom->act_timing($po_num);
	  
	    $sup_code = array();
		
		$this->load->view('admin/view_po_details',$data); 
     }	  
   }