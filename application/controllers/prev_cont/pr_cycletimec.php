<?php
class Pr_cycletimec extends CI_Controller{  
   
	function __construct(){
		parent::__construct();
		$this->load->helper('url');  
		$this->load->database(); 
		$this->load->model('pr_cycletimem');
		user_act();
	}
	
	public function index(){
		$data['h']=$this->pr_cycletimem->po_procedure();
	    $this->db->close();
		$this->db->initialize();
		
		$this->load->view('admin/header',$data);	
		$this->load->view('admin/pr_report/pr_cycletime_report_main',$data);
		$this->load->view('admin/footer',$data);
	}
		
	public function pr_report(){	
		$this->load->view('admin/pr_report/pr_report');
	}
	
	public function po_report(){	
		$this->load->view('admin/pr_report/po_report');
	}
	
	public function grcpt_report(){	
		$this->load->view('admin/pr_report/grcpt_report');
	}
	
	public function pr_report_det(){
	
		$this->load->view('admin/header');	
		$this->load->view('admin/pr_report/pr_cycletime_report_det');
		$this->load->view('admin/footer');
	
	}
	
	public function no_of_po(){
		$this->load->view('admin/pr_report/no_of_po');
	}
	
	public function cost_saving_report(){
		$this->load->view('admin/pr_report/cost_saving_report');
	}
	
	public function no_po_cost_saving_merge(){
		$this->load->view('admin/pr_report/no_po_cost_saving_merge');
	}
	
	public function approval_cycletime(){
		$this->load->view('admin/pr_report/approval_cycletime');
	}
	
	public function cost_saving_det(){
		$this->load->view('admin/pr_report/cost_saving_det');
	}
	
	public function cost_saving_report_monthwise(){
		$this->load->view('admin/pr_report/cost_saving_report_monthwise');
	}
	
	public function need_data_vs_actual(){
		$data['ndva_proc']=$this->pr_cycletimem->ndva_proc();
		 
	    $this->db->close();	
		$this->db->initialize();
		
		$this->load->view('admin/pr_report/need_data_vs_actual', $data);
	}
	
	public function fetch_months_ndva(){
		$this->load->view('admin/pr_report/fetch_months_ndva');
	}
	
	public function fetch_months_ndva_report_main(){
		$this->load->view('admin/pr_report/fetch_months_ndva_report_main');
	}
	
	//GRCPT Cycletime Report
	public function goods_receipt_cycletime_main(){
		$this->load->view('admin/pr_report/goods_receipt_cycletime_main');
	}
	
	public function po_type(){
		$this->load->view('admin/pr_report/po_type');
	}
	
	public function base_month(){
		$this->load->view('admin/pr_report/base_month');
	}
	
	public function good_receipt_main_rpt(){
		$this->load->view('admin/pr_report/good_receipt_main_rpt');
	}
	
	public function good_receipt_main_rpt_view(){
		$this->load->view('admin/header',$data);
		$this->load->view('admin/pr_report/good_receipt_main_rpt_view');
		$this->load->view('admin/footer',$data);
	}
	
	
}

