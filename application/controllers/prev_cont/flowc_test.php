<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flowc_test extends CI_Controller {

	public function index()
	{
		$this->load->helper('url');
		$this->load->model('flowm_test');
		
		$data['h']             = $this->flowm_test->select_prqit_prno();
		$data['draw']          = $this->flowm_test->drawing();
		$data['j']             = $this->flowm_test->select_prqit_prno2();
		$data['j_new']         = $this->flowm_test->select_prqit_prno_new();
		$data['auth_pr']         = $this->flowm_test->authorize_level_pr();
		$data['scrpo']         = $this->flowm_test->select_scrit_scono();
		$data['i']             = $this->flowm_test->select_prqit_pono();
		$data['m_lvl1']        = $this->flowm_test->select_prqit_prno_lvl1();
		$data['disaprve_lvl1'] = $this->flowm_test->disapproved_po_lvl1();
		$data['m']             = $this->flowm_test->select_prqit_prno_lvl2();
		$data['disaprve']      = $this->flowm_test->disapproved_po_lvl2();
		$data['n']             = $this->flowm_test->send_supplier();
		$data['mc_planning']   = $this->flowm_test->mc_planning();
		$data['tc']            = $this->flowm_test->test_cert();
		$data['pi']            = $this->flowm_test->per_inv();
		$data['pi2']           = $this->flowm_test->per_inv_lvl2();
		$data['pips']          = $this->flowm_test->pi_payment_to_supplier();
		$data['pdc']           = $this->flowm_test->post_dated_check();
		$data['freight']       = $this->flowm_test->freight();
		$data['o']             = $this->flowm_test->manufact_clearance();
		$data['r']             = $this->flowm_test->ack_supp();
		$data['t']             = $this->flowm_test->dispatch();
		$data['s']             = $this->flowm_test->delivery();
		$data['z']             = $this->flowm_test->grcpt();
		
		$this->load->view('admin/flow_test', $data);
	}
	
 }