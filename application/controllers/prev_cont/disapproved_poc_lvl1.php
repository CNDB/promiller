<?php
class Disapproved_poc_lvl1 extends CI_Controller{  
   
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('disapproved_pom_lvl1');
		user_act();
	}
	 
	public function index()  
	{   
		$data['h']=$this->disapproved_pom_lvl1->select_prqit_prno();
		$this->load->view('admin/disapproved_po_create_lvl1', $data); 
	} 
	   
	/********** Create Purchase Request Controller **********/
	  
	public function create_po()  
	{  
		$data['h']=$this->disapproved_pom_lvl1->select_prqit_prno();
		$this->load->view('admin/disapproved_po_create_lvl1', $data);  
	}
	  
	/*********** View Purchase Request Controller **********/
	  
	public function view_po()
	{
		echo $selectpr = $_REQUEST['q'];
		
		$data['cal_amend_no'] = $this->disapproved_pom_lvl1->cal_amend_no($selectpr);
		
		foreach($data['cal_amend_no']->result_array() AS $row){
			 $amend_no = $row['amend_no'];
		}
			
		$data['v']=$this->disapproved_pom_lvl1->view_poqit_pono($selectpr, $amend_no);
		
		foreach($data['v']->result_array() AS $row){
			 $sup_code = $row['poitm_itemcode'];
		}
		
		$sup_code1 = urldecode($sup_code);
		
		$data['p']=$this->disapproved_pom_lvl1->procesdure_run($sup_code);
		
		$this->db->close();
		
		$this->db->initialize();
		
		$data['chat_history'] = $this->disapproved_pom_lvl1->chat_history($selectpr);
						  
		$this->load->view('admin/disapproved_po_view_lvl1',$data);
	} 
	  
	/********* Mail *******/
	  
	public function mail()
	{
		$this->load->view('admin/mail');
	}
	  
	public function insert_po_sub()
	{
		//New Mail function starts
		 
		 $this->load->helper(array('form'));
		 
		 $this->load->library('email');
		 
		 $this->email->set_mailtype("html");
		 
		 $user_name = $_SESSION['username'];
		 
		 $from_email = $_SESSION['username']."@tipl.com";
		 
		 $po_num = $this->input->post('po_num');
		 
		 $pm_group = $this->input->post("pm_group");
		 
		 $category = $this->input->post("category");
		 
		 $base_url  = base_url();
		 
		 $this->email->to('gaurav.mangal@tipl.com');
		 
		 //$this->email->to('it.support@tipl.com');
		 
		 $po_link = $base_url."index.php/createpodc/create_po_lvl1/";
		 	  
		 $this->email->from($from_email, $user_name);
		
		 $this->email->cc('purchase@tipl.com');
		 
		 $this->email->bcc('it.software@tipl.com');
			 
		 $this->email->subject($po_num.'- Disapproved PO Again Send For Approval At Level 1');
			  
		 //page fetched values
	     $po_ipr_no = $this->input->post('po_ipr_no');
		 $array_count = count($po_ipr_no);
		 $po_date = $this->input->post('po_date');
		 $supp_name = $this->input->post('po_supp_name');
		 $item_code = $this->input->post('po_item_code');
		 $item_desc = $this->input->post('item_desc');
		 $carrier_name = $this->input->post('carrier_name');
		 $freight_terms = $this->input->post('freight');
		 $payment_terms = $this->input->post('payterm');
		 $last_yr_cons = $this->input->post('lastyr_cons');
		 $current_stk = $this->input->post('current_stk');
		 $reservation_qty = $this->input->post('reservation_qty');
		 $for_stk_qty = $this->input->post('for_stk_quantity');
		 $uom = $this->input->post('po_uom');
		 $last_price = $this->input->post('last_price');
		 $current_price = $this->input->post('current_price');
		 $po_total_value = $this->input->post('po_total_value');
		 $po_remarks = $this->input->post('spcl_inst_supp');
		 //page fetched values
		 
		 $purchase_order = "<b>PO No. - ".$po_num."</b><br />";
		 
		 $po_table = "<table cellpadding='1' cellspacing='1' style='border:solid 1px black; font-size:11px; padding:10px; border-radius:5px;'>
						<tr style='background-color:#CFF'>
							<td><b>Supplier Name</b><td>
							<td><b>Item Code</b><td>
							<td><b>Item Description</b><td>
							<td><b>Carrier Name</b><td>
							<td><b>Freight Terms</b><td>
							<td><b>Payment Terms</b><td>
							<td><b>Last 1 Yr Con.</b><td>
							<td><b>Current Stk</b><td>
							<td><b>Reservation Qty</b><td>
							<td><b>For Stk Qty</b><td>
							<td><b>UOM</b><td>
							<td><b>Last Price</b><td>
							<td><b>Current Price</b><td>
							<td><b>PO Value</b><td>
							<td><b>Remarks</b><td>
						</tr>";
						
						for($i=0; $i<$array_count; $i++){
						
			$po_table .="<tr>
							<td>".$supp_name."<td>
							<td>".$item_code[$i]."<td>
							<td>".$item_desc[$i]."<td>
							<td>".$carrier_name."<td>
							<td>".$freight_terms."<td>
							<td>".$payment_terms."<td>
							<td>".number_format($last_yr_cons[$i],2)."<td>
							<td>".number_format($current_stk[$i],2)."<td>
							<td>".number_format($reservation_qty[$i],2)."<td>
							<td>".number_format($for_stk_qty[$i],2)."<td>
							<td>".$uom[$i]."<td>
							<td>".number_format($last_price[$i],2)."<td>
							<td>".number_format($current_price[$i],2)."<td>
							<td>".number_format($po_total_value,2)."<td>
							<td>".$po_remarks."<td>
						</tr>";
						
						}
						
			$po_table .="</table>";
		 
		 $purchase_order = "<b>PO No. - ".$po_num."</b><br />";
			  
		 $mes = "<p>New Purchase Order is created in live, Please check and authorize this purchase order in live.</p><br/>".$po_table."
		 <a href='".$po_link.$po_num."' target='_blank'>View PO</a><br/>
		 <b>Thank You, </b><br /><b>Regards, </b><br><b>".$user_name."</b>";
		 
		 $message = $purchase_order.$mes;
			
		 $this->email->message( $message);
			
		 $this->email->send();
		 
		 //New Mail function ends
		
		$config['upload_path']   = './uploads/'; 
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|'; 
		$config['max_size']      = 100000000; 
		$config['max_width']     = 102400; 
		$config['max_height']    = 768000;	
			
		$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
		$filename = strtolower($_FILES["po_quote_frmsupp"]["name"]);
		$fileExe  = substr($filename, strrpos($filename,'.'));
		
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		$file = basename($filename, ".".$ext);
			
		$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
		$NewFileName2 = $NewFileName.'_'.$RandNumber.$fileExe;
		$config['file_name'] = $NewFileName2;
		
		$this->load->library('upload', $config);			
			
		
		if ( ! $this->upload->do_upload('po_quote_frmsupp')){
		$error = array('error' => $this->upload->display_errors());
		}else { 
		$data2 = array('upload_data' => $this->upload->data()); 
		} 
		
		$data = array();
		
		$this->disapproved_pom_lvl1->insert_po_sub($data, $NewFileName2);
		$data['message'] = 'Data Inserted Successfully';
		
		$this->load->view('admin/insert_po_sub');
	}
	  
	public function view_ipr()
	{
		$selectpr = $_REQUEST['q'];
			
		$data['v']=$this->disapproved_pom_lvl1->view_poqit_pono($selectpr);
		
		foreach($data['v']->result_array() AS $row){
			 $sup_code = $row['poitm_itemcode'];
		}
					
		$this->load->view('admin/view_ipr',$data);
	}  

}