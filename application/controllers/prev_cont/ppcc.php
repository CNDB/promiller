<?php
class Ppcc extends CI_Controller{  
   
   	function __construct(){
		 parent::__construct();
		 $this->load->helper('url');
		 $this->load->database(); 
		 $this->load->model('ppcm');
		 user_act();
	 }
	 
     public function index(){
		 $data['last_run_date']=$this->ppcm->last_run_date();
		 
		 foreach($data['last_run_date']->result_array() AS $row) {
			$last_run_date = substr($row['run_date'],0,11); 	 
	     }
		 
		 $current_date = date('Y-m-d');
		 
		 //Condition To Run Report in a Day
		 if($current_date != $last_run_date){
			 $data['ppc_proc']=$this->ppcm->ppc_proc();
			 $this->db->close();
			 $this->db->initialize();
		 }
		 
		 $this->load->view('admin/header',$data); 
         $this->load->view('admin/ppc_report/ppc_report_main',$data);
		 $this->load->view('admin/footer',$data); 
     }
	 
	 public function ppc_report_details(){
		  $this->load->view('admin/header'); 
          $this->load->view('admin/ppc_report/ppc_report_details'); 
		  $this->load->view('admin/footer'); 	 
     }
	 
	 public function ppc_pr_entry(){
		  $selectpr = $this->uri->segment(3);
		  $user_id = 1;
		  $data['v_auth']=$this->ppcm->view_prqit_prno_auth($selectpr);
		  
		  foreach($data['v_auth']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		  }
			
		  $sup_code1 = urldecode($sup_code);	
		  $data['p']=$this->ppcm->procesdure_run($sup_code1);
		  $this->db->close();
		  $this->db->initialize();
		  
		  $data['p_new']=$this->ppcm->procesdure_run_supplier($user_id, $selectpr, $sup_code);	
		  $this->db->close();
		  $this->db->initialize();
		  
		  //Chat History Query
		  $data['chat_history'] = $this->ppcm->chat_history_table($selectpr);
		  //Chat History Query
		  $data['r']=$this->ppcm->pendal_info($sup_code);
		  $data['month']= $this->ppcm->pendal_info_monthwise($sup_code1);
		  $data['last5'] = $this->ppcm->pendal_info_last5_trans($sup_code);
		  $data['reorder'] = $this->ppcm->pendal_info_reorder_lvl_qty($sup_code);
		  $data['monthofinvtry'] = $this->ppcm->monthofinvtry($sup_code);
		  $data['orderinadvnce'] = $this->ppcm->orderinadvnce($sup_code);
		  $data['wrkordr_ser']=$this->ppcm->wrkordr_ser($sup_code);
		  $data['lstyrcumnrec1'] = $this->ppcm->lstyrcumnrec1($sup_code);
		  $data['lastfivetrans1'] = $this->ppcm->lastfivetrans1($sup_code);
		  $data['itemdescmaster'] = $this->ppcm->itemdescmaster($sup_code);
		  $data['mnthwsecurryrcons1'] = $this->ppcm->pendal_master_mnthwsecurryrcons($sup_code);
		  $data['mnthwsecurryrrcpt1'] = $this->ppcm->pendal_master_mnthwsecurryrrcpt($sup_code);
		  $data['supplier1'] = $this->ppcm->pendal_master_supplier($sup_code1);
		  $data['bomdetail1'] = $this->ppcm->pendal_master_bomdetail($sup_code1);
		  //Action Timing Report Queries
		  $data['prerplive'] = $this->ppcm->prerplive($selectpr);
		  //Action Timing Report Queries
          $this->load->view('admin/ppc_pr_entry', $data);
     }
	 
	 public function ppc_pr_entry_submit(){
		  $this->load->helper(array('form'));
		  $this->load->library('email');
		  $this->email->set_mailtype("html");
		  //Data Starts
		  $user_name = $_SESSION['username'];
		  $from_email = $_SESSION['username']."@tipl.com";
		  $pr_num = $this->input->post('pr_num');
		  $item_code = $this->input->post('item_code');
		  $item_desc = $this->input->post('item_desc');
		  $category = $this->input->post('category');
		  $pr_created_by = $this->input->post('pr_created_by');
		  $pr_create_date = $this->input->post('pr_create_date');
		  $need_date = $this->input->post('need_date');
		  $pend_alloc_date = $this->input->post('pend_alloc_date');
		  $atac_no = $this->input->post('atac_no');
		  $project_name = $this->input->post('project_name');
		  $customer_name = $this->input->post('customer_name');
		  $remarks = $this->input->post('remarks');
		  $created_by = $_SESSION['username'];
		  $created_date = date('Y-m-d H:i:s');
		  $comm_ques = $this->input->post('comm_ques');
		  $ques_ans  = $this->input->post('ques_ans');
		  
		  //Mail Body Starts
		  $to_mail = "planning@tipl.com,purchase@tipl.com";
		  $cc_mail = "mis@tipl.com,".$from_email;
		  $this->email->to($to_mail);
		  $this->email->from($from_email, $user_name);
		  $this->email->cc($cc_email);
		  $this->email->bcc("it.software@tipl.com");
		  $subject = "Change / Update In Target Freeze Movement Date Of PR";
		  $this->email->subject($subject);
		  
		  $mail_body="
			<p style='font-weight:bold'>
				Dear All,<br><br>
				Freeze Movement Date Of Below Purchase Request is Changed / Updated In Live.
				<br>Kindly Note That and Proceed Accordingly.
			</p>
			<br>
			<table cellpadding='2px' cellspacing='2px' border='1'>
				<tr style='background-color:#0CF'>
					<td><b>PR Number</b></td>
					<td><b>Item Code</b></td>
					<td><b>Item Desc.</b></td>
					<td><b>Category</b></td>
					<td><b>Atac No.</b></td>
					<td><b>Project Name</b></td>
					<td><b>Customer Name</b></td>
					<td><b>ERP Created By</b></td>
					<td><b>ERP Created Date</b></td>
					<td><b>PR Need Date</b></td>
					<td><b>Target Freeze Movement Date</b></td>
					<td><b>Reason For Change</b></td>
				</tr>
				<tr>
					<td>".$pr_num."</td>
					<td>".$item_code."</td>
					<td>".$item_desc."</td>
					<td>".$category."</td>
					<td>".$atac_no."</td>
					<td>".$project_name."</td>
					<td>".$customer_name."</td>
					<td>".$pr_created_by."</td>
					<td>".$pr_create_date."</td>
					<td>".$need_date."</td>
					<td>".$pend_alloc_date."</td>
					<td>".$remarks."</td>
				</tr>
			</table><br><br>
			<b>Remarks - ".$ques_ans."</b><br><b>Thanks & Regards,</b><br><b style='text-transform:uppercase'>".$created_by."</b>";

		  	$this->email->message($mail_body);
			if(!$this->email->send()){
				echo $this->email->print_debugger();
				die;
			}
			
			$data = array();
		  	$data['message'] = 'Data Inserted Successfully';
			$this->ppcm->insert_ppc_entry($data);
            $this->load->view('admin/ppc_pr_entry_submit'); 
     }
	 
	 //PO Entry
	 
	 public function ppc_po_entry(){
		 $po_num = $this->uri->segment(3);
		 $data['max_amend_no'] = $this->ppcm->max_amend_no($po_num);
		 
	     foreach($data['max_amend_no']->result_array() AS $row) {
		 	$amend_no = $row['amend_no']; 	 
	     }
		  
		 $data['view_po']=$this->ppcm->po_view_lvl1($po_num,$amend_no);
		 $this->load->view('admin/ppc_po_entry', $data);
	 }
	 
	  public function ppc_po_entry_submit(){
			$this->load->helper(array('form'));
			$this->load->library('email');
			$this->email->set_mailtype("html");
			$select_pr = $this->input->post('select_pr');
			$pend_alloc_date = $this->input->post('pend_alloc_date');
			$remarks = $this->input->post('remarks');
			$created_by = $_SESSION['username'];
			$pr_count = count($select_pr);
			
			if($pr_count > 0){
			$user_name = $_SESSION['username'];
			$from_email = $_SESSION['username']."@tipl.com";
			$created_by = $_SESSION['username'];
			$created_date = date('Y-m-d H:i:s');
			
			//Mail Body Starts
			$to_mail = "planning@tipl.com,purchase@tipl.com";
			$cc_mail = "mis@tipl.com,".$from_email;
			$this->email->to($to_mail);
			$this->email->from($from_email, $user_name);
			$this->email->cc($cc_email);
			$this->email->bcc("it.software@tipl.com");
			$subject = "Change / Update In Target Freeze Movement Date Of PO";
			$this->email->subject($subject);
		  
			$mail_body = "
			<p style='font-weight:bold'>
				Dear All,<br><br>
				Freeze Movement Date Of Below Purchase Requests is Changed / Updated In Live.
				<br>Kindly Note That and Proceed Accordingly.
			</p><br>";
			
			$mail_body .= "
			<table width='100%' border='1' cellpadding='2' cellspacing='2'>
				<tr style='background-color:#0CF'>
					<td><b>PR Num</b></td>
					<td><b>Item Code</b></td>
					<td><b>Item Desc</b></td>
					<td><b>Category</b></td>
					<td><b>ATAC No</b></td>
					<td><b>Project Name</b></td>
					<td><b>Customer Name</b></td>
					<td><b>PR Created By</b></td>
					<td><b>PR Create Date</b></td>
					<td><b>PR Need Date</b></td>
					<td><b>Expected FM Date</b></td>
					<td><b>Remarks</b></td>
				</tr>
			";
			  
			for($i=0;$i<$pr_count;$i++){
				$data['pr_det'] = $this->ppcm->pr_details($select_pr[$i]);
				foreach($data['pr_det']->result_array() AS $row){
					$mail_body .="
					<tr>
						<td>".$select_pr[$i]."</td>
						<td>".$item_code[$i] = $row['item_code']."</td>
						<td>".$item_desc[$i] = $row['itm_desc']."</td>
						<td>".$category[$i] = $row['category']."</td>
						<td>".$atac_no[$i] = $row['atac_no']."</td>
						<td>".$project_name[$i] = $row['project_name']."</td>
						<td>".$customer_name[$i] = $row['customer_name']."</td>
						<td>".$pr_created_by[$i] = $row['created_by']."</td>
						<td>".$pr_create_date[$i] = $row['create_date']."</td>
						<td>".$need_date[$i] = $row['need_date']."</td>
						<td>".$pend_alloc_date."</td>
						<td>".$remarks."</td>
					</tr>
					";   	 
				}
			
			}
			  
			$mail_body .="</table><br>";
			$mail_body.="<b>Thanks & Regards,</b><br><b style='text-transform:uppercase'>".$created_by."</b>";
		  	$this->email->message($mail_body);
			$data = array();
		  
			if($this->email->send()){
				$this->ppcm->insert_ppc_po_entry($data);
			} else {
				echo $this->email->print_debugger();
			}
			
			//Mail Body Ends
			$data['message'] = 'Data Inserted Successfully';
			$this->load->view('admin/ppc_pr_entry_submit');
		} else {
			echo $error = "No PR is Selected....";
			$this->load->view('admin/ppc_pr_entry_submit');
		}	 
     }
	 
	public function ppc_report_main_ajax(){
		$this->load->view('admin/ppc_report/ppc_report_main_ajax'); 
	}
	 
	public function ppc_report_main_ajax_reset(){
		$this->load->view('admin/ppc_report/ppc_report_main_ajax_reset');
	}
	 
	//Ask Question On PR
	public function ppc_ask_question(){
		$this->load->view('admin/ppc_report/ppc_ask_question');
	}
	 
	//Update Freeze Movement Date
	public function ppc_update_fm_date(){
		$this->load->view('admin/ppc_report/ppc_update_fm_date'); 
	}
	 
	 public function ppc_report_nw(){
		 //Procedure for fetching L3 approval details
		 $data['p']=$this->ppcm->fetch_lvl3_app_req();
		 $this->db->close();
		 $this->db->initialize();
		 
		 $this->load->view('admin/header'); 
         $this->load->view('admin/ppc_report/ppc_report_main_new');
		 $this->load->view('admin/footer',$data);
     }
	 
	 public function ppc_report_main_new_ajax_pr(){
		 $this->load->view('admin/ppc_report/ppc_report_main_new_ajax_pr');
	 }
	 
	 public function ppc_report_main_new_ajax_po(){
		 $this->load->view('admin/ppc_report/ppc_report_main_new_ajax_po');
	 }
	 
	 public function ppc_report_main_new_ajax_reset(){
		 $this->load->view('admin/ppc_report/ppc_report_main_new_ajax_reset');
	 }
	 
	 /*public function ppc_report_nw_det_dept(){ 
		 $this->load->view('admin/header'); 
         $this->load->view('admin/ppc_report/ppc_report_main_new_det_dept');
		 $this->load->view('admin/footer'); 
     }*/
	 
	 
	  public function ppc_report_nw_det(){ 
		 $this->load->view('admin/header'); 
         $this->load->view('admin/ppc_report/ppc_report_main_new_det');
		 $this->load->view('admin/footer'); 
     }
	 
	 //PO Wise
	 public function ppc_report_nw_det_po(){ 
		 $this->load->view('admin/header'); 
         $this->load->view('admin/ppc_report/ppc_report_main_new_det_po');
		 $this->load->view('admin/footer'); 
     }
	 
	 //Excel Export
	 public function ppc_report_nw_det_xls(){ 
         $this->load->view('admin/ppc_report/ppc_report_main_new_det_xls'); 
     }
}

