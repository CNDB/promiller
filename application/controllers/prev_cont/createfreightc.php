<?php
class Createfreightc extends CI_Controller{  
	function __construct(){
		parent::__construct();
		$this->load->helper('url'); 
		$this->load->database();
		$this->load->model('createfreightm'); 
		$this->load->helper(array('form'));
		user_act();
	}
	
	public function create_po_lvl2(){
		$data['m']=$this->createfreightm->select_prqit_prno_lvl2();
		$this->load->view('admin/create_freight', $data);  
	}
	
	public function view_po_lvl2(){
		$selectpr = $_REQUEST['q'];
		$data['max_amend_no'] = $this->createfreightm->max_amend_no($selectpr);
		foreach($data['max_amend_no']->result_array() AS $row) {
			 $amend_no = $row['amend_no']; 	 
		}
		
		$data['view_po']= $this->createfreightm->po_view_lvl2($selectpr, $amend_no);
		$this->load->view('admin/view_freight',$data);
	} 

	public function insert_po_sub_lvl2(){
		$this->load->helper(array('form'));
		$config['upload_path']   = './uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|'; 
		$config['max_size']      = 100000000; 
		$config['max_width']     = 102400; 
		$config['max_height']    = 768000;
		$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
		$filename = strtolower($_FILES["freight_uploaded_cheque_img"]["name"]);
		$fileExe  = substr($filename, strrpos($filename,'.'));
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		$file = basename($filename, ".".$ext);
		$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
		$NewFileName2 = $NewFileName.'_'.$RandNumber.$fileExe;
		$config['file_name'] = $NewFileName2;
		
		$this->load->library('upload', $config);
		
		if ( ! $this->upload->do_upload('freight_uploaded_cheque_img')) {
			$error = array('error' => $this->upload->display_errors());
		}else { 
			$data2 = array('upload_data2' => $this->upload->data()); 
		}
		
		$data = array();
		$this->createfreightm->insert_po_sub_lvl2($data, $NewFileName2);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_po_sub',$data);
	}
}