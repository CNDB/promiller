<?php
class Mc_planningc extends CI_Controller
{   
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url'); 
		
		$this->load->database();
		  
		$this->load->model('mc_planningm');
		
		$this->load->library('email');
		
		$this->load->helper(array('form'));
		
		user_act();
	}
	 
	public function create_po_lvl2()  
	{  
		$data['m']=$this->mc_planningm->select_prqit_prno_lvl2();
		
		$this->load->view('admin/mc_planning_create', $data);  
	}

	/********* View Purchase Request Controller ************/
	  
	public function view_po_lvl2()
	{
		$selectpr = $_REQUEST['q'];
		
		$data['max_amend_no'] = $this->mc_planningm->max_amend_no($selectpr);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			 $amend_no = $row['amend_no']; 	 
		  }
		
		$data['view_po']=$this->mc_planningm->po_view_lvl2($selectpr, $amend_no);
		  
		$this->load->view('admin/mc_planning_view',$data);
	} 
	  
	public function insert_po_sub_lvl2()
	{ 
		//email function
		
		$this->email->set_mailtype("html");
		
		$po_num = $this->input->post('po_num');
		
		$po_date = $this->input->post('po_date');
		
		$user_name = $_SESSION['username'];
		
		$from_email = $_SESSION['username']."@tipl.com";
		
		$this->email->to('purchase@tipl.com, planning@tipl.com');
		
		$this->email->from($from_email, $user_name);
		
		$this->email->cc($from_email);
		
		$this->email->bcc('it.software@tipl.com');
		
		$subject = $po_num." - Manufacturing Clearnance Planning";
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$purchase_order_date = "<b>Our PO Date -".$po_date."</b><br />";
		
		$message_text = "
		<p style='text-align:left;'>Sir/Madam,<br />
		Please Find the attachment PO and proceed for manufacturing for mentioned items in attachment.<br />
		Regards,<br />
		<b>Planning Team</b><br />
		<b>Toshniwal Industries Private Limited, Ajmer</b></p><br />
		";
		
		$message = $purchase_order.$purchase_order_date.$message_text;
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$data = array();
		$this->mc_planningm->insert_po_sub_lvl2($data);
		$data['message'] = 'Data Inserted Successfully';
		
		if(!$this->email->send()){
			echo $this->email->print_debugger();
			die;
		}
		
		//email function ends
		
		$this->load->view('admin/insert_po_sub'); 
	}

}