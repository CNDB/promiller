<?php
class Itemc extends CI_Controller{  
   
   	function __construct()
	 {
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('itemm');
		 
		 user_act();
	 }
	 
     public function index()  
      {    
		 $data['h']=$this->itemm->item_details();
		  
         $this->load->view('admin/item_view', $data); 
      }
}

