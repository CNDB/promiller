<?php
class Currencyc extends CI_Controller{ 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('currencym');
		user_act();
	}
	
	public function index(){    
		$data['h']=$this->currencym->select_currency();
		$this->load->view('admin/currency_view', $data); 
	} 
	
	public function currency_submit(){    
		$data = array();
		$data['i']=$this->currencym->insert_currency($data);
		$this->load->view('admin/currency_view', $data); 
	} 
}
