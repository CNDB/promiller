<?php
class Manufact_clerancec extends CI_Controller  
{  
   
	function __construct()
	{
		parent::__construct();
		
		$this->load->helper('url');
		 
		$this->load->database();
		   
		$this->load->model('manufact_clerancem');
		
		$this->load->library('email');
		
		$this->load->helper(array('form'));
		
		user_act();
	}
	 
	public function create_po_lvl2()  
	{  
		$data['m']=$this->manufact_clerancem->select_prqit_prno_lvl2();
		
		$this->load->view('admin/manufact_clerance_supplier', $data);  
	}
	  
	/******** View Purchase Request Controller ********/
	  
	public function view_po_lvl2()
	{
		$selectpr = $_REQUEST['q'];
		
		$data['max_amend_no'] = $this->manufact_clerancem->max_amend_no($selectpr);
		
		foreach($data['max_amend_no']->result_array() AS $row) {
			 $amend_no = $row['amend_no']; 	 
		  }
		
		$data['view_po']=$this->manufact_clerancem->po_view_lvl2($selectpr, $amend_no);
		
		$this->load->view('admin/manufact_clerance_supplier_view',$data);
	} 
	  
	public function insert_po_sub_lvl2(){
		
		$po_num = $this->input->post('po_num');
		
		$created_by = $_SESSION['username'];
		
		$created_date = date("Y-m-d H:i:s");
		
		//Mail Function Starts
		
		$to_email = $this->input->post('to_mail');
		
		$subject = $this->input->post('subject');
		
		$username_table = $_SESSION['username'];
		
		$po_num = $this->input->post('po_num');
		
		$message_txt = $this->input->post('message_text');
		$message_txt = str_replace("'","",$message_txt);
		
		//Fetching Purchasor Details
		$data['pur_details']=$this->manufact_clerancem->pur_details($username_table);
		
		foreach($data['pur_details']->result_array() AS $row) {
			$pur_name = $row['emp_name'];
			$pur_mobile_no = $row['mobile_no'];
			$pur_email = $row['emp_email'];
		}
		
		
		//Fetching Purchasor Details
		
		$this->email->from($pur_email, $pur_name);
		
		$this->email->to($to_email);
		
		$cc_email = "purchase@tipl.com,".$pur_email;
		
		$this->email->cc($cc_email);
		
		$this->email->bcc('it.software@tipl.com');
		
		$this->email->subject($subject);
		
		$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
		
		$message = "<b>Hello Sir/Madam,</b><br><br>".nl2br($message_txt)."<br><b>Thanks & Regards,</b><br /><br />
		<b style='text-transform:uppercase;'>".$pur_name." | PURCHASE</b><br /><br />
		<b>EMAIL - ".$pur_email."</b><br /><br />
		<b>CONTACT NO - ".$pur_mobile_no."</b><br /><br />
		<b style='text-transform:uppercase;'>Toshniwal Industries Private Limited, Ajmer</b></p><br />";
		
		$base_url = base_url();
		
		$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
		
		$this->email->attach($attch);
		
		$this->email->message($message);
		
		$data = array();
		$this->manufact_clerancem->insert_po_sub_lvl2($data);
		$data['message'] = 'Data Inserted Successfully';
		
		if(!$this->email->send()){
			echo $this->email->print_debugger();
			die;	
		}
		//Mail Function Ends
		
		$this->load->view('admin/insert_po_sub');
	}
}