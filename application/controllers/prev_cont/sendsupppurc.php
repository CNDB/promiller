<?php
class Sendsupppurc extends CI_Controller{  
   
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();  
		$this->load->model('sendsupppurm');
		$this->load->library('email');
	    $this->load->helper(array('form'));
		
		user_act();
	}
	 
	public function create_po_lvl2(){  
		$data['m']=$this->sendsupppurm->select_prqit_prno_lvl2();
		$this->load->view('admin/send_supplier_purchase', $data);  
	}
	  
	/***** View Purchase Request Controller ****/  
	public function view_po_lvl2(){
		$selectpr = $_REQUEST['q'];
		$data['view_po']=$this->sendsupppurm->po_view_lvl2($selectpr);
		$this->load->view('admin/send_supplier_purchase_view',$data);
	} 
	  
	public function insert_po_sub_lvl2(){ 
		$this->email->set_mailtype("html");
		$to_email = $this->input->post('to_mail');
		$cc_email = $this->input->post('cc_mail');
		$subject = $this->input->post('subject');
		$username_table = $_SESSION['username'];
		$po_num = $this->input->post('po_num');
		$message_txt = $this->input->post('message_text');
		
		//Fetching Purchasor Details
		$data['pur_details']=$this->sendsupppurm->pur_details($username_table);
		foreach($data['pur_details']->result_array() AS $row) {
			$pur_name = $row['emp_name'];
			$pur_mobile_no = $row['mobile_no'];
			$pur_email = $row['emp_email'];
		}
		
		//File Upload Code Starts
		$number_of_files = sizeof($_FILES['other_att']['tmp_name']);
		$files = $_FILES['other_att'];
		$this->load->library('upload');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|bmp';
		//echo $number_of_files; die;
		 
		if($number_of_files > 0){
			for($i = 0; $i < $number_of_files; $i++){
				$_FILES['other_att']['name'] = $files['name'][$i];
				$_FILES['other_att']['type'] = $files['type'][$i];
				$_FILES['other_att']['tmp_name'] = $files['tmp_name'][$i];
				$_FILES['other_att']['error'] = $files['error'][$i];
				$_FILES['other_att']['size'] = $files['size'][$i];
				$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
				$filename = $_FILES['other_att']['name'];
				$fileExe  = substr($filename, strrpos($filename,'.'));
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				$file = basename($filename, ".".$ext);
				$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
				$NewFileName2 = $NewFileName.'_'.$RandNumber.".".$ext;
				$config['file_name'] = $NewFileName2;
				$config['remove_spaces'] = true;
				
				//now we initialize the upload library
				$this->upload->initialize($config);
				
				if ($this->upload->do_upload('other_att')){
					$data = array('upload_data' => $this->upload->data());
					$file_name = $data['upload_data']['file_name'];
					rename("./uploads/$file_name","./uploads/$NewFileName2");
				} 
				$pr_nik = $this->input->post('po_num');
				$this->sendsupppurm->insert_attachment($pr_nik, $NewFileName2);
			}
		}
		//File Upload Code End
		  
		//Mail Code Starts
		if($po_num != ''){
			$this->email->from($pur_email, $pur_name);
			$this->email->to($to_email);
			$cc_email1 = $cc_email.",".$pur_email;
			$this->email->cc($cc_email1);
			$this->email->bcc('it.software@tipl.com');
			$this->email->subject($subject);
			
			$purchase_order = "<b>Our PO No. - ".$po_num."</b><br />";
			$message = "<b>Hello Sir/Madam,</b><br><br>".nl2br($message_txt)."<br><b>Thanks & Regards,</b><br /><br />
			<b style='text-transform:uppercase;'>".$pur_name." | PURCHASE</b><br /><br />
			<b>EMAIL - ".$pur_email."</b><br /><br />
			<b>CONTACT NO - ".$pur_mobile_no."</b><br /><br />
			<b style='text-transform:uppercase;'>Toshniwal Industries Private Limited, Ajmer</b></p><br />";
			
			$base_url = base_url();
			$attch =  $base_url."/uploads/pdf/".$po_num.".pdf";
			$this->email->attach($attch);
			
			$data['get_attachment']=$this->sendsupppurm->get_attachment($po_num);
			
			foreach($data['get_attachment']->result_array() AS $row) {
				$attached_supp_quotes = $row['attached_supp_quotes'];
				$other_att =  $base_url."/uploads/".$attached_supp_quotes;
				$this->email->attach($other_att);
			}
			
			$this->email->message($message);
			$data = array();
			
			if($this->email->send()){
				$this->sendsupppurm->insert_po_sub_lvl2($data);	
			} else {
				echo $this->email->print_debugger();	
			}
					
			$data['message'] = 'Data Inserted Successfully';
			$this->load->view('admin/insert_po_sub_lvl2',$data);
		}
	}
}