<?php
class View_prc extends CI_Controller{  
   
   	function __construct(){
		
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('view_prm');
		 
		 user_act();
	 }
	 
     public function index(){
		  
         $this->load->view('admin/view_pr_search'); 
      }
	  
	 public function pr_details(){
		 
		$pr_num = $_REQUEST['pr_number'];
		
		//PR Details Page 
		
		$data['view_pr']=$this->view_prm->view_prqit_prno($pr_num);
		
		foreach($data['view_pr']->result_array() AS $row){
			$sup_code = $row['prqit_itemcode'];
		}
			
		$sup_code1 = urldecode($sup_code);
			
		$data['p']=$this->view_prm->procesdure_run($sup_code);
			
		$this->db->close();
			
		$this->db->initialize();
		
		/*$data['p_new']=$this->view_prm->procesdure_run_supplier($user_id, $pr_num, $sup_code);
		
		$this->db->close();
		
		$this->db->initialize();*/ 
		
		$data['view_pr_live']=$this->view_prm->view_pr_live($pr_num);
		
		$data['chat_history'] = $this->view_prm->chat_history_table($pr_num);
		
		$data['prerplive'] = $this->view_prm->prerplive($pr_num);
		
		$data['mnthwsecurryrcons1'] = $this->view_prm->pendal_master_mnthwsecurryrcons($sup_code1);
		
		$data['mnthwsecurryrrcpt1'] = $this->view_prm->pendal_master_mnthwsecurryrrcpt($sup_code1);
		
		$data['lstyrcumnrec1'] = $this->view_prm->lstyrcumnrec1($sup_code);
		
		$data['reorder'] = $this->view_prm->pendal_info_reorder_lvl_qty($sup_code);
		
		$data['lastfivetrans1'] = $this->view_prm->lastfivetrans1($sup_code);
		
		$data['supplier1'] = $this->view_prm->pendal_master_supplier($sup_code);
		
		$data['bomdetail1'] = $this->view_prm->pendal_master_bomdetail($sup_code1);
		
		
		//PR Details Page Ends
		
		$this->load->view('admin/view_pr_details',$data); 
     }	  
   }