<?php
class Iprc extends CI_Controller  
{   
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('iprm');
		user_act();
	}
	
	public function index()  
	{  
		$this->load->helper('url');
		
		$this->load->view('admin/view_ipr'); 
	}
	
	/******** View Purchase Request Controller ********/
	
	public function view_ipr(){
		$ipr_no = $this->uri->segment(3);
				
		$data['v_auth']= $this->iprm->view_ipr($ipr_no);	
		
		$data['suppcode'] = $this->iprm->view_suppcode($ipr_no);
		
		foreach($data['v_auth']->result_array() AS $row){
		 $sup_code = $row['prqit_itemcode'];
		} 
		
		$sup_code1 = urldecode($sup_code);
		
		$data['p']=$this->iprm->procesdure_run($sup_code);
		
		$this->db->close();
		
		$this->db->initialize();
		
		//Chat History Query
		$data['chat_history'] = $this->iprm->chat_history_table($ipr_no);
		//Chat History Query
		
		//Action Timing Report Queries
	    $data['prerplive'] = $this->iprm->prerplive($ipr_no);
	    //Action Timing Report Queries
		
		$data['r']=$this->iprm->pendal_info($sup_code);
		
		$data['month']= $this->iprm->pendal_info_monthwise($sup_code);
		
		$data['last5'] = $this->iprm->pendal_info_last5_trans($sup_code);
		
		$data['reorder'] = $this->iprm->pendal_info_reorder_lvl_qty($sup_code);
		
		$data['monthofinvtry'] = $this->iprm->monthofinvtry($sup_code);
		
		$data['orderinadvnce'] = $this->iprm->orderinadvnce($sup_code);
		
		$data['lstyrcumnrec1'] = $this->iprm->lstyrcumnrec1($sup_code);
		
		$data['lastfivetrans1'] = $this->iprm->lastfivetrans1($sup_code);
		
		$data['itemdescmaster'] = $this->iprm->itemdescmaster($sup_code1);
		
		$data['mnthwsecurryrcons1'] = $this->iprm->pendal_master_mnthwsecurryrcons($sup_code1);
		
		$data['mnthwsecurryrrcpt1'] = $this->iprm->pendal_master_mnthwsecurryrrcpt($sup_code1);
		
		$data['supplier1'] = $this->iprm->pendal_master_supplier($sup_code1);
		
		$data['bomdetail1'] = $this->iprm->pendal_master_bomdetail($sup_code1);
		
		$this->load->view('admin/view_ipr',$data);
	}	  
}
