<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flowc extends CI_Controller {
	
	function __construct(){	
	    parent::__construct();
	   	$this->load->helper('url');
		$this->load->model('flowm');
		user_act();
	}

	public function index(){
		$this->load->view('admin/flow');
	}
	
	//Fresh Purchase Requests
	public function fresh_pr(){
		$this->load->view('admin/ajax_pages/fresh_pr');
	}
	
	//Attach Drawing
	public function attach_drawing(){
		$this->load->view('admin/ajax_pages/attach_drawing');
	}
	
	//Authorize Purchase Request planning
	public function auth_pr(){
		$this->load->view('admin/ajax_pages/auth_pr');
	}
	
	//Disapproved Purchase Request Planning
	public function dis_pr_plan(){		
		$this->load->view('admin/ajax_pages/dis_pr_plan');
	}
	
	//Authorize Purchase Request planning Lvl2
	public function auth_pr_lvl2(){
		$this->load->view('admin/ajax_pages/auth_pr_lvl2');
	}
	
	//Disapproved Purchase Request Planning Level2
	public function dis_pr_plan_lvl2(){
		$this->load->view('admin/ajax_pages/dis_pr_plan_lvl2');
	
	}
	
	//Live Authorize Purchase Request Pending For ERP Authorization
	public function erp_not_auth_pr(){		
		$this->load->view('admin/ajax_pages/erp_not_auth_pr');
	
	}
	
	//Authorized Purchase Requests whose po is not created in erp
	public function pr_po_report(){	
		$this->load->view('admin/ajax_pages/pr_po_report');
	}
	
	//Disapproved Purchase Request Purchase
	public function dis_pr_pur(){
		
		$this->load->view('admin/ajax_pages/dis_pr_pur');
	
	}
	
	//Fresh ERP Purchase Orders pending for live po creation	
	public function fresh_po(){
	
		$data['i_new'] = $this->flowm->select_prqit_pono();
		$data['i_new_fpo'] = $this->flowm->select_prqit_pono_fpo();
		$data['i_new_ipo'] = $this->flowm->select_prqit_pono_ipo();
		$data['i_new_lpo'] = $this->flowm->select_prqit_pono_lpo();
		
		$this->load->view('admin/ajax_pages/fresh_po', $data);
	}
	
	//PO authorization level 1	
	public function auth_po_lvl1(){
	
		$data['m_lvl1_new'] = $this->flowm->select_prqit_prno_lvl1();
		$data['m_lvl1_new_fpo'] = $this->flowm->select_prqit_prno_lvl1_fpo();
		$data['m_lvl1_new_ipo'] = $this->flowm->select_prqit_prno_lvl1_ipo();
		$data['m_lvl1_new_lpo'] = $this->flowm->select_prqit_prno_lvl1_lpo();
		
		$this->load->view('admin/ajax_pages/auth_po_lvl1', $data);
	
	}
	
	//PO disapproval level1	
	public function dis_po_lvl1(){
	
		$data['disaprve_lvl1_new'] = $this->flowm->disapproved_po_lvl1();
		
		$this->load->view('admin/ajax_pages/dis_po_lvl1', $data);
	
	}
	
	//PO authorization level 2
	public function auth_po_lvl2(){
	
		$data['m_new'] = $this->flowm->select_prqit_prno_lvl2();
		$data['m_new_fpo'] = $this->flowm->select_prqit_prno_lvl2_fpo();
		$data['m_new_ipo'] = $this->flowm->select_prqit_prno_lvl2_ipo();
		$data['m_new_lpo'] = $this->flowm->select_prqit_prno_lvl2_lpo();
		
		$this->load->view('admin/ajax_pages/auth_po_lvl2', $data);
	
	}
	
	//PO disapproval level2
	public function dis_po_lvl2(){
	
		$data['disaprve_new'] = $this->flowm->disapproved_po_lvl2();
		
		$this->load->view('admin/ajax_pages/dis_po_lvl2', $data);
	
	}
	
	//PO authorization level 3
	public function auth_po_lvl3(){
	
		$data['m_new_lvl3'] = $this->flowm->select_prqit_prno_lvl3();
		$data['m_new_fpo_lvl3'] = $this->flowm->select_prqit_prno_lvl3_fpo();
		$data['m_new_ipo_lvl3'] = $this->flowm->select_prqit_prno_lvl3_ipo();
		$data['m_new_lpo_lvl3'] = $this->flowm->select_prqit_prno_lvl3_lpo();
		
		$this->load->view('admin/ajax_pages/auth_po_lvl3', $data);
	
	}
	
	//PO disapproval level3
	public function dis_po_lvl3(){
	
		$data['disaprve_new_lvl3'] = $this->flowm->disapproved_po_lvl3();
		
		$this->load->view('admin/ajax_pages/dis_po_lvl3', $data);
	
	}
	
	//PO Pending For Authorize In ERP
	public function not_auth_po(){
	
		$data['not_auth_po_new'] = $this->flowm->not_auth_po();
		
		$this->load->view('admin/ajax_pages/not_auth_po', $data);
		
	}
	
	//ERP Amended PO'S
	public function amend_po(){
	
		$data['amend_po_new'] = $this->flowm->amend_po();
		
		$this->load->view('admin/ajax_pages/amend_po', $data);
		
	}
	
	//Purchase order send to supplier for purchase	
	public function supp_for_pur(){
	
		$data['n_new'] = $this->flowm->send_supplier();
		$data['n_new_fpo'] = $this->flowm->send_supplier_fpo();
		$data['n_new_ipo'] = $this->flowm->send_supplier_ipo();
		$data['n_new_lpo'] = $this->flowm->send_supplier_lpo();
		
		$this->load->view('admin/ajax_pages/supp_for_pur', $data);
	
	}
	
	//PO pending for lead time update by Purchase
	public function lead_time_update(){
		$this->load->view('admin/ajax_pages/lead_time_update');
	}
	
	//PO pending for manufacturing clearance planning
	public function mc_planning(){
	
		$data['mc_planning_new'] = $this->flowm->mc_planning();
		
		$this->load->view('admin/ajax_pages/mc_planning', $data);
	
	}
	
	//PO pending for manufacturing clearance purchase	
	public function mc_purchase(){
	
		$data['o_new'] = $this->flowm->manufact_clearance();
		
		$this->load->view('admin/ajax_pages/mc_purchase', $data);
	
	}
	
	//PO pending for acknowledgement from supplier	
	public function ack_frm_supp(){
	
		$data['r_new'] = $this->flowm->ack_supp();
		
		$this->load->view('admin/ajax_pages/ack_frm_supp', $data);
	
	}
	
	//PO pending for test certificate uploadion
	public function tc_upload(){
	
		$data['tc_new'] = $this->flowm->test_cert();
		
		$this->load->view('admin/ajax_pages/tc_upload', $data);
	
	}
	
	//PO pending for dispatch instruction planning
	public function di_planning(){
		$this->load->view('admin/ajax_pages/di_planning');
	}
	
	//PO pending for dispatch instruction purchase	
	public function di_purchase(){
		$this->load->view('admin/ajax_pages/di_purchase');
	}
	
	//PO pending for dispatch instruction
	public function dispatch_inst(){
	
		$data['t_new'] = $this->flowm->dispatch();
		
		$this->load->view('admin/ajax_pages/dispatch_inst', $data);
	
	}
	
	//PO Pending for delivery details
	public function deli_details(){
	
		$data['s_new'] = $this->flowm->delivery();
		
		$this->load->view('admin/ajax_pages/deli_details', $data);
	
	}
	
	//PO Pending for gate entry	
	public function gate_entry(){
	
		$data['z_new'] = $this->flowm->grcpt();
		
		$this->load->view('admin/ajax_pages/gate_entry', $data);
	
	}
	
	//PO Pending for gate entry	
	public function hsn_check(){
		
		$this->load->view('admin/ajax_pages/hsn_check');
	
	}
	
	//WITHOUT MC AND DI PENDING FOR APPROVAL FROM REPORTING AUTHORITY
	public function mcdi_ra(){
		
		$this->load->view('admin/ajax_pages/mcdi_ra');
	
	}
	
	//DISAPPROVED FROM REPORTING AUTHORITY
	public function dis_ra(){
		
		$this->load->view('admin/ajax_pages/dis_ra');
	
	}
	
	//WITHOUT MC AND DI PENDING FOR APPROVAL FROM PROJECT INCHARGE
	public function mcdi_pi(){
		
		$this->load->view('admin/ajax_pages/mcdi_pi');
	
	}
	
	//DISAPPROVED FROM PROJECT INCHARGE
	public function dis_pi(){
		
		$this->load->view('admin/ajax_pages/dis_pi');
	
	}
	
	//WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L1
	public function mcdi_cf1(){
		
		$this->load->view('admin/ajax_pages/mcdi_cf1');
	
	}
	
	//DISAPPROVED FROM CASHFLOW INCHARGE L1
	public function dis_cf1(){
		
		$this->load->view('admin/ajax_pages/dis_cf1');
	
	}
	
	//WITHOUT MC AND DI PENDING FOR APPROVAL FROM CASHFLOW INCHARGE L2	
	public function mcdi_cf2(){
		
		$this->load->view('admin/ajax_pages/mcdi_cf2');
	
	}
	
	//DISAPPROVED FROM CASHFLOW INCHARGE L2	
	public function dis_cf2(){
		
		$this->load->view('admin/ajax_pages/dis_cf2');
	
	}
	
}