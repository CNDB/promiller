<?php
class Hsn_checkc extends CI_Controller{  
   
   	function __construct(){
		 parent::__construct();  
		 $this->load->helper('url');
		 $this->load->database(); 
		 $this->load->model('hsn_checkm');
		 user_act();
	 }
	 
	 //HSN Check Page
     public function index(){
		 $po_num = $this->uri->segment(3);
		 $data['max_amend_no'] = $this->hsn_checkm->max_amend_no($po_num);
	
	     foreach($data['max_amend_no']->result_array() AS $row) {
		 	$amend_no = $row['amend_no']; 	 
	     }
		  
		 $data['view_po']=$this->hsn_checkm->po_view($po_num,$amend_no);
		 $this->load->view('admin/header',$data);
		 $this->load->view('admin/gate_entry/hsn_check_header',$data);
		 $this->load->view('admin/po_details_div_cns',$data);
		 $this->load->view('admin/gate_entry/hsn_check_footer',$data);
		 $this->load->view('admin/footer',$data);
      }
	  
	  public function grcpt_det_update(){
		$this->load->helper(array('form'));
		$po_num = $this->input->post("po_num");
		$action_by_pur = $this->input->post("proceed_to_grcpt");
		
		//Procedure for fetching L3 approval details
		 $data['p']=$this->hsn_checkm->fetch_lvl3_app_req();
		 $this->db->close();
		 $this->db->initialize();
		
		//MC Condition Check
		$data['mc_cond'] = $this->hsn_checkm->mc_cond($po_num);
		foreach($data['mc_cond']->result_array() AS $row) {
			$count_mc = $row['count_mc']; 	 
		}
		//DI Condition Check
		$data['di_cond'] = $this->hsn_checkm->di_cond($po_num);
		foreach($data['di_cond']->result_array() AS $row) {
			$count_di = $row['count_di']; 	 
		}
		
		//Prev App Condition Check
		$data['prev_app_cond'] = $this->hsn_checkm->prev_app_cond($po_num);
		foreach($data['prev_app_cond']->result_array() AS $row) {
			$count_prev = $row['count_prev']; 	 
		}
		
		if($action_by_pur == 'Proceed To GRCPT'){
			//MC & DI Condition Check
			if($count_mc > 0 && $count_prev > 0){	
				$status = 'Pending For MC & DI Approval From RA';	
			} else if($count_di > 0 && $count_prev > 0){	
				$status = 'Pending For MC & DI Approval From RA';	
			} else {	
				$status = 'Pending For GRCPT';
			}
		} else {
		  $status = 'Received';
		}
		
		//Mail Function Starts
		$this->load->library('email');
		$this->email->set_mailtype("html");
		$user_name = $_SESSION['username'];
		$from_email = $_SESSION['username']."@tipl.com";
		$entry_no = $this->input->post("entry_no");
		$remarks = $this->input->post("remarks");
		$to_mail = "purchase@tipl.com, accounts@tipl.com, it.software@tipl.com, store@tipl.com";
		$from_mail = $from_email;
		$cc_mail = $from_mail;
		$this->email->to($to_mail);
		$this->email->from($from_email, $user_name);
		$this->email->cc($from_email);
		$subject = $entry_no."- Gate Entry & ".$po_num."- PO No. Is Checked By Purchaser"; 
		$this->email->subject($subject);
		
		$message_table = "
		<b>".$subject."</b><br><br>
		<b>Remarks - ".$remarks."</b><br><br>
		<b>Action By Purchase - ".$action_by_pur."</b><br><br>
		<b>Status - ".$status."</b><br><br>
		<table cellpadding='1px' cellspacing='1px' border='1'>
			<tr style='background-color:#CFF'>
				<td><b>Entry No</b></td>
				<td><b>Entry Date</b></td>
				<td><b>PO Number</b></td>
				<td><b>Supplier Name</b></td>
				<td><b>PO Category</b></td>
				<td><b>PO Created By</b></td>
				<td><b>PO Created Date</b></td>
			</tr>
		";
		
		$data['gate_entry_det']=$this->hsn_checkm->gate_entry_det($entry_no, $po_num);
		
		foreach($data['gate_entry_det']->result_array() AS $row) {
			$entry_no = $row['entry_no'];
			$entry_date = $row['entry_date'];
			$po_num = $row['po_num'];
			$supp_nm = $row['supp_nm'];
			$po_category = $row['po_category'];
			$pomas_createdby = $row['pomas_createdby'];
			$pomas_createddate = $row['pomas_createddate'];	
		
			 $message_table.="
			 <tr>
				<td>".$entry_no."</td>
				<td>".$entry_date."</td>
				<td>".$po_num."</td>
				<td>".$supp_nm."</td>
				<td>".$po_category."</td>
				<td>".$pomas_createdby."</td>
				<td>".$pomas_createddate."</td>
			 </tr>
			 ";		
		
		}
		
		$message_table.="</table><br><br><b>Thanks & Regards<br>TIPL Administrator</b>";
		
		$this->email->message($message_table);
		if(!$this->email->send()){
			echo $this->email->print_debugger();
		}
		//Mail Function Ends
		  
		//File Upload Code Starts
		$number_of_files = sizeof($_FILES['po_ge_attchment']['tmp_name']);
		$files = $_FILES['po_ge_attchment'];
		$doc_type = $this->input->post('doc_type');
		$this->load->library('upload');
		$config['upload_path'] = './uploads/ge_attachment/';
		$config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|bmp';
		
		if($number_of_files > 0){
			for($i = 0; $i < $number_of_files; $i++){
				$_FILES['po_ge_attchment']['name'] = $files['name'][$i];
				$_FILES['po_ge_attchment']['type'] = $files['type'][$i];
				$_FILES['po_ge_attchment']['tmp_name'] = $files['tmp_name'][$i];
				$_FILES['po_ge_attchment']['error'] = $files['error'][$i];
				$_FILES['po_ge_attchment']['size'] = $files['size'][$i];
				
				$RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
				$filename = $_FILES['po_ge_attchment']['name'];
				$fileExe  = substr($filename, strrpos($filename,'.'));
				$ext = pathinfo($filename, PATHINFO_EXTENSION);
				
				$file = basename($filename, ".".$ext);
				$NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
				
				$NewFileName2 = $NewFileName.'_'.$RandNumber.".".$ext;
				$config['file_name'] = $NewFileName2;
				$config['remove_spaces'] = true;
				
				//now we initialize the upload library
				$this->upload->initialize($config);
				
				if ($this->upload->do_upload('po_ge_attchment')){
					//$this->upload->data();
					$data = array('upload_data' => $this->upload->data());
					$file_name = $data['upload_data']['file_name'];
					rename("./uploads/$file_name","./uploads/$NewFileName2");
				}
				
				$docmt_type = $doc_type[$i];
				 
				$pr_nik = $this->input->post('po_num');
				$entry_no = $this->input->post('entry_no');
				
				//If condition added on 22-06-2019
				if($filename != ''){
					$this->hsn_checkm->insert_attachment($pr_nik, $entry_no, $docmt_type, $NewFileName2);
				}
			}
		}
		//File Upload Code End
		$data = array();
		$this->hsn_checkm->grcpt_det_update($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/insert_po_sub');
		  
	  }	  
}