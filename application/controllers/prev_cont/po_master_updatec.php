<?php
class Po_master_updatec extends CI_Controller{  
   
   	 function __construct(){
		
		 parent::__construct();
		   
		 $this->load->helper('url');
			  
		 $this->load->database(); 
			  
		 $this->load->model('po_master_updatem');
		 
		 user_act();
	 }
	 
      public function index(){
		      
		 $data['po_app_mstr_view']=$this->po_master_updatem->po_app_mstr_view();
		  
         $this->load->view('admin/po_master_update_view', $data);  
      }
	  
	  public function po_master_update(){
		  
		 $data = array();
		
		 $this->po_master_updatem->update_po_app_mstr($data);
		 
		 $data['message'] = 'Data Inserted Successfully';
		
		 $this->load->view('admin/po_master_update_entry');
	  }
	  
	  
   }

