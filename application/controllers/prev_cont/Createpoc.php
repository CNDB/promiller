<?php
class Createpoc extends CI_Controller{  
   
   	 function __construct(){
		 parent::__construct();
		 $this->load->helper('url');
		 $this->load->database(); 
		 $this->load->model('createpom');
		 user_act();
	 }
	 
      public function index(){
		 $data['h']=$this->createpom->select_prqit_prno(); 
         $this->load->view('admin/create_pr', $data); 
      } 
	   
	  /******* Create Purchase Request Controller *******/
	  
	  public function create_pr(){    
		 $data['h']=$this->createpom->select_prqit_prno();
		 $this->load->view('admin/create_pr', $data);  
      }
	  
	  /********** View Purchase Request Controller ************/
	  
	  public function view_pr(){
		  $selectpr = $_REQUEST['q'];
		  $user_id = 1;
		  $data['v']=$this->createpom->view_prqit_prno($selectpr);
		  foreach($data['v']->result_array() AS $row){
		  	$sup_code = $row['prqit_itemcode'];
		  }
		  $sup_code1 = urldecode($sup_code);
		  $data['p']=$this->createpom->procesdure_run($sup_code);
		  $this->db->close();
		  $this->db->initialize();
		  $data['p_new']=$this->createpom->procesdure_run_supplier($user_id, $selectpr, $sup_code);
		  $this->db->close();
		  $this->db->initialize();
		  //Chat History Query
		  $data['chat_history'] = $this->createpom->chat_history_table($selectpr);
		  //Action Timing Report Queries
		  $data['prerp'] = $this->createpom->prerp($selectpr);
		  //Action Timing Report Queries
		  $data['r']=$this->createpom->pendal_info($sup_code);
		  $data['month']= $this->createpom->pendal_info_monthwise($sup_code1);
		  $data['last5'] = $this->createpom->pendal_info_last5_trans($sup_code);
		  $data['reorder'] = $this->createpom->pendal_info_reorder_lvl_qty($sup_code);
		  $data['monthofinvtry'] = $this->createpom->monthofinvtry($sup_code);
		  $data['orderinadvnce'] = $this->createpom->orderinadvnce($sup_code);
		  $data['wrkordr_ser']=$this->createpom->wrkordr_ser($sup_code);
		  $data['lstyrcumnrec1'] = $this->createpom->lstyrcumnrec1($sup_code);
		  $data['lastfivetrans1'] = $this->createpom->lastfivetrans1($sup_code);
		  $data['itemdescmaster'] = $this->createpom->itemdescmaster($sup_code1);
		  $data['mnthwsecurryrcons1'] = $this->createpom->pendal_master_mnthwsecurryrcons($sup_code1);
		  $data['mnthwsecurryrrcpt1'] = $this->createpom->pendal_master_mnthwsecurryrrcpt($sup_code1);
		  $data['supplier1'] = $this->createpom->pendal_master_supplier($sup_code1);
		  $data['bomdetail1'] = $this->createpom->pendal_master_bomdetail($sup_code1);
		  $this->load->view('admin/view_pr',$data);
	  } 
	  
	  /********** Pendal Card Controller *********/
	  
	  public function pendal_view($sup_code1){
		  $nik = $this->uri->segment(3);
		  $this->load->view('admin/pendal_view');
	  } 
	  
	  /***** Mail *****/
	  public function wrkordr_ser_res(){
		  $selectWo = $_REQUEST['q'];
		  $data['wrkordr_ser_res']=$this->createpom->wrkordr_ser_res($selectWo);
		  $this->load->view('admin/workorder_search', $data);
	  }
	  
	  public function wrkordr_ser_res_new(){
		  $selectWo = $_REQUEST['q'];
		  $pr_num = $_REQUEST['pr_num'];
		  $data['wrkordr_ser_res_new']=$this->createpom->wrkordr_ser_res_new($selectWo);
		  $this->load->view('admin/workorder_search_new', $data);
	  }
	  
	  public function wrkordr_ser_res_new1(){
		  $selectWo = $_REQUEST['q'];
		  $data['wrkordr_ser_res_new']=$this->createpom->wrkordr_ser_res_new($selectWo);
		  $this->load->view('admin/workorder_search_new1', $data);
	  }
	  
	  public function mail(){
		  $this->load->view('admin/mail');
	  }
	  
	  public function insert_pr_sub(){
		  $this->load->library('email');
		  $this->email->set_mailtype("html");
		  $this->load->helper(array('form'));
		  $pr_num = $this->input->post('pr_num');
		  
		  if($pr_num != ''){
			  $drawing_no = $this->input->post('drawing_no');
			  $item_code = $this->input->post('item_code');
			  $item_desc = $this->input->post('itm_desc');
			  $item_desc = str_replace("'","",$item_desc);
			  $item_desc = str_replace('"','',$item_desc);
			  $uom = $this->input->post('trans_uom');
			  $quantity = $this->input->post('required_qty');
			  $need_date = $this->input->post('need_date'); 
			  $username = $_SESSION['username'];
			  $from_email = $_SESSION['username']."@tipl.com";
			  //new elements in mail
			  $current_stk = $this->input->post('current_stk');
			  $reservation_qty = $this->input->post('reservation_qty');
			  $last_supplier = $this->input->post('supplier_name');
			  $last_supplier_lead_time = $this->input->post('supplier_lead_time');
			  $last_purchase_rate = $this->input->post('supplier_rate');
			  $category = $this->input->post('category');
			  $project_name = $this->input->post('pro_ordr_name');
			  $pm_group = $this->input->post('pm_group');
			  $last_yr_cons = $this->input->post('lastyr_cons');
			  $usage_type = $this->input->post('usage');
			  $atac_selection = $this->input->post('manual_atac');
			  $atac_no_select = $this->input->post('atac_no_select');
			  $workorder = $this->input->post('workorder');	
			  $pr_qty = $this->input->post('required_qty');
			  $color = $this->input->post('color');
			  $cost_available = $this->input->post('cost_available');
			  $cost = $this->input->post('costing');
			  $cost_cal_rmks = $this->input->post('cost_calculation_remarks');
			  $cost_na_rmks = $this->input->post('cost_not_available_remarks');
			  $currency = $this->input->post('currency');
			  $created_by = $this->input->post('created_by');
			  $why_spcl_rmks = $this->input->post('why_spcl_rmks');
			  
			  //Creator email
			  $data['creator_email']=$this->createpom->creator_email($created_by);
			  foreach($data['creator_email']->result_array() AS $row) {
					$creator_email = $row['emp_email'];	
			  }
			  
			  //Creator email
			  $reorder_level = $this->input->post('reorder_level');
			  if($reorder_level == '' || $reorder_level == NULL ){
				  $reorder_level = 0;
			  }
			  
			  $reorder_qty = $this->input->post('reorder_qty');
			  if($reorder_qty == '' || $reorder_qty == NULL){
				  $reorder_qty = 0;
			  }
			  
			  $wo_qty = $this->input->post('wo_qty');
			  if($wo_qty == '' || $wo_qty == NULL){
				  $wo_qty = 0;
			  }
			  
			  //Total required Quantity
			  $tot_required_qty = $reorder_qty+$reservation_qty;
			  
			  $accepted_stock = $this->input->post('accepted_stock');
			  if($accepted_stock == '' || $accepted_stock == NULL){
				  $accepted_stock = 0;
			  }
			  
			  $po_pr_quantity = $this->input->post('po_pr_quantity');
			  if($po_pr_quantity == '' || $po_pr_quantity == NULL){
				  $po_pr_quantity = 0;
			  }
			  
			  $grcpt_recipt_qty = $this->input->post('grcpt_recipt_qty');
			  if($grcpt_recipt_qty == '' || $grcpt_recipt_qty == NULL){
				  $grcpt_recipt_qty = 0;
			  }
			  
			  $grcpt_accepted_qty = $this->input->post('grcpt_accepted_qty');
			  if($grcpt_accepted_qty == '' || $grcpt_accepted_qty == NULL){
				  $grcpt_accepted_qty = 0;
			  }
			  
			  $tot_available_qty = $accepted_stock+$po_pr_quantity+$grcpt_recipt_qty+$grcpt_accepted_qty;
			  $balance_qty = $tot_required_qty - $tot_available_qty;
			  
			  //Projcet Preorder Requirement
			  $pre_order_req = $quantity+$reservation_qty;
				if($category == ''){ 
					//Getting Columns Throught Query
					$data['project_category']=$this->createpom->project_category($pm_group);
					foreach($data['project_category']->result_array() AS $row) {
						$cat_live = $row['product_type'];
					}
					
					if($cat_live == 'Process_Instrumentation'){
						$category = "INSTRUMENTATION";
					} else if($cat_live == 'Security'){
						$category = "SECURITY";
					} else if($cat_live == 'Temperature_Sensors'){
						$category = "SENSORS";
					} else if($cat_live == 'Avazonic'){
						$category = "AVAZONIC";
					} else if($cat_live == 'Service'){
						$category = "SERVICE";
					}
				
				}
			  
			  //New Mail Code
			  $this->email->from($from_email, $username);
			  if($atac_no_select == ''){
				$atac_selection = 'No';
			  }else{
				$atac_selection = 'Yes';
			  }
			  
			  if($workorder == ''){
				$wo = 'No';
			  }else{
				$wo = 'Yes';
			  }
			  
			  $pr_num_first_three = substr($pr_num,0,3);
			  
		  //CGPO Condition
		  if($pr_num_first_three == 'CGP'){
			  echo $level1_approval_req = "Yes";
			  echo $level2_approval_req = "No";
			  echo $level1_approval_mailto = "vipin.patni";
			  echo $level1_email = "vipin.patni@tipl.com";
			  $email_to = $level1_email.",".$creator_email;
			  $this->email->to($email_to);
			  echo $level2_approval_mailto = NULL;
			  echo $error = "Capital Goods Purchase Request";
			  echo $mail_header_line = "Purchase Request is created in live, Pending for Level 1 Authorization.";
			  $category = "CAPITAL GOODS";
		  } else {
			  if($pr_qty > $balance_qty && $uom == 'NO'){
				  if($usage_type == 'Project'){
					  $data['master_data']=$this->createpom->master_data($usage_type,$atac_selection,$wo,$category);
					  foreach($data['master_data']->result_array() AS $row){
						$level1_approval_req = $row['level1_approval_req'];
						$level2_approval_req = $row['level2_approval_req'];
						$level1_approval_mailto = $row['level1_approval_mail_to'];
						$level1_approval_mailto_mail = $level1_approval_mailto."@tipl.com";
						$level1_email = $row['level1_email'];
						$level2_approval_mailto = $row['level2_approval_mail_to'];
						$error = $row['error'];
						$mail_header_line = $row['mail_header_line'];
						$this->email->to($level1_email);
					  } 
					  
				  } else {
					  $data['master_data']=$this->createpom->master_data($usage_type,$atac_selection,$wo,$category);
					  foreach($data['master_data']->result_array() AS $row) {
						$level1_approval_req = $row['level1_approval_req'];
						$level2_approval_req = $row['level2_approval_req'];
						$level1_approval_mailto = $row['level1_approval_mail_to'];
						$level1_approval_mailto_mail = $level1_approval_mailto."@tipl.com";
						$level1_email = $row['level1_email'];
						$level2_approval_mailto = $row['level2_approval_mail_to'];
						$error = $row['error'];
						$mail_header_line = $row['mail_header_line'];
						$this->email->to($level1_email);
					  } 
					  
				  }
			  
			  } else {
				  $this->email->to('planning@tipl.com');
				  $level1_approval_mailto = NULL;
				  $level2_approval_mailto = NULL;
				  $level1_approval_req = 'No';
				  $level2_approval_req = 'No';
				  $error = NULL;
				  $mail_header_line = 'Purchase Request is created in live, pending for ERP Authorization.'; 
			  }
	
		  }
		  
		  //Without Category Lock PR
		  if($category == ''){
			echo "PR Cannot be created without category.";
			die;
		  }
		  
		  //CGPR Condition Ends
		  //ERP Status
		  $data['erp_stat']=$this->createpom->erp_stat($pr_num);
		  
		  foreach($data['erp_stat']->result_array() AS $row) {
				$erp_status = $row['preqm_status'];	
		  }
		  
		  if($erp_status == 'AU'){
			  $this->email->to('purchase@tipl.com');
			  $mail_header_line = "Purchase Request is Pending For PO creation.";
			  $error = "Purchase Request is Pending For PO creation.";
		  }
		  
			  
		  $cc_email = "planning@tipl.com,".$creator_email;
		  $this->email->cc($cc_email);
		  $this->email->bcc('it.software@tipl.com');
		  $subject = $pr_num .'- PR Created In Live';
		  $this->email->subject($subject);
		  $message_table = "
			<table border='1' cellpadding='2' cellspacing='0'>
				<tr>
					<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
					<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
				</tr>
			</table><br/><br/>
			<table cellpadding='1' cellspacing='1' width='500px' border='1' style='border:solid 1px black; font-size:11px; padding:10px; border-radius:5px;'>
				  <tr style='background-color:#CFF'>
					  <td><b>IPR NO.</b></td>
					  <td><b>ITEM CODE</b></td>
					  <td><b>ITEM DESC.</b></td>
					  <td><b>UOM</b></td>
					  <td><b>QUANTITY</b></td>
					  <td><b>NEED DATE</b></td>
					  <td><b>CURRENT STK</b></td>
					  <td><b>RESERVATION QTY</b></td>
					  <td><b>REORDER LEVEL</b></td>
					  <td><b>REORDER QTY</b></td>
					  <td><b>LAST SUPPLIER</b></td>
					  <td><b>LAST SUPPLIER LEAD TIME</b></td>
					  <td><b>LAST PURCHASE RATE</b></td>
					  <td><b>TYPE OF PR</b></td>
					  <td><b>WHY SPCL REMARKS</b></td>
					  <td><b>CATEGORY</b></td>
					  <td><b>PROJECT NAME</b></td>
					  <td><b>PM GROUP</b></td>
					  <td><b>LAST YR CONS.</b></td>
					  <td><b>COST AVAIL.</b></td>
					  <td><b>COSTING</b></td>
					  <td><b>CURRENCY</b></td>
					  <td><b>COST CAL. RMKS</b></td>
					  <td><b>COST NOT AVAIL. RMKS</b></td>
				  </tr>
				  <tr>
					  <td>".$pr_num."</td>
					  <td>".$item_code."</td>
					  <td>".$item_desc."</td>
					  <td>".$uom."</td>
					  <td>".number_format($quantity,2)."</td>
					  <td style='background-color:".$color."'>".$need_date."</td>
					  <td>".number_format($current_stk,2)."</td>
					  <td>".number_format($reservation_qty,2)."</td>
					  <td>".number_format($reorder_level,2)."</td>
					  <td>".number_format($reorder_qty,2)."</td>
					  <td>".$last_supplier."</td>
					  <td>".$last_supplier_lead_time."</td>
					  <td>".$last_purchase_rate."</td>
					  <td>".$usage_type."</td>
					  <td>".$why_spcl_rmks."</td>
					  <td>".$category."</td>
					  <td>".$project_name."</td>
					  <td>".$pm_group."</td>
					  <td>".number_format($last_yr_cons,2)."</td>
					  <td>".$cost_available."</td>
					  <td>".$cost."</td>
					  <td>".$currency."</td>
					  <td>".$cost_cal_rmks."</td>
					  <td>".$cost_na_rmks."</td>
				  </tr>
			</table>";
				  
			$message = "<b>PR Number - ".$pr_num."</b><br/><br/>"."<p>".$mail_header_line."</p><br/><p style='color:red; font-weight:bold'>Reason For Approval Req -> ".$error."</p><br/><p>ERP Created By : ".$created_by."</p><br/>".$message_table."<b>Regards,</b><br><b style='text-transform:uppercase;'>".$username."</b>";
				  
			 $this->email->message($message);
			 
			 if(!$this->email->send()){
				echo $this->email->print_debugger();
				die;
			 }
			
			 //Attachment Code Starts
			 $filename = strtolower($_FILES["cost_sheet"]["name"]);
			 $filename2 = strtolower($_FILES["other_attachment"]["name"]);
			 
			 //Cost Sheet Attachment
			 if( $filename !== ""){
				 $config['upload_path']   = './uploads/'; 
				 $config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|';		
				 $RandNumber = rand(0, 9999999999); //Random number to make each filename unique.
				 $fileExe  = substr($filename, strrpos($filename,'.'));
				 $ext = pathinfo($filename, PATHINFO_EXTENSION);
				 $file = basename($filename, ".".$ext);		
				 $NewFileName = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file));
				 $NewFileName2 = $NewFileName.'_'.$RandNumber.".".$ext;
				 $config['file_name'] = $NewFileName2;
				 $config['log_threshold'] = 1;
				 $config['overwrite'] = false;
				 $config['remove_spaces'] = true;
				 
				 $this->load->library('upload', $config);			
				
				 if (!$this->upload->do_upload('cost_sheet')) {
					$error = array('error' => $this->upload->display_errors());
				 }else { 
					$data = array('upload_data' => $this->upload->data());
					$file_name = $data['upload_data']['file_name'];
					rename("./uploads/$file_name","./uploads/$NewFileName2");
				 }
			 }
			 
			 //Other Attachment
			 if( $filename2 !== ""){
				 echo "<br><br>";
				 $config = array();
				 $config['upload_path']   = './uploads/'; 
				 $config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|xls|xlsx|ppt|pptx|csv|ods|odt|odp|pdf|txt|';
				 $RandNumber2 = rand(0, 9999999999); //Random number to make each filename unique.
				 $fileExe2  = substr($filename2, strrpos($filename2,'.'));
				 $ext2 = pathinfo($filename2, PATHINFO_EXTENSION);
				 $file2 = basename($filename2, ".".$ext2);		
				 $NewFileName4 = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), strtolower($file2));
				 $NewFileName6 = $NewFileName4.'_'.$RandNumber2.".".$ext2;
				 $config['file_name'] = $NewFileName6;
				 $config['overwrite'] = false;
				 $config['remove_spaces'] = true;
				 $this->load->library('upload', $config);			
				
				 if (!$this->upload->do_upload('other_attachment')) {
					$error = array('error' => $this->upload->display_errors());
				 } else { 
					$data = array('upload_data' => $this->upload->data());
					$file_name = $data['upload_data']['file_name'];
					rename("./uploads/$file_name","./uploads/$NewFileName6");
				 }	 
			 }
			 //Attachment Code Ends
			 $data = array();
			
			 $this->createpom->insert_pr_sub($data, $NewFileName2, $NewFileName6, $level1_approval_mailto, $level2_approval_mailto, $level1_approval_req, $level2_approval_req, $error);
			 
			 $data['message'] = 'Data Inserted Successfully';
		 }
		 $this->load->view('admin/insert_pr_sub');
	  }
	  /*********** Authorise Purchase Request Planning Start ***************/
	  
	   public function view_pr_auth(){ 	  
		  $selectpr = $this->uri->segment(3);
		  $user_id = 1;
		  $data['v_auth']=$this->createpom->view_prqit_prno_auth($selectpr);
		  foreach($data['v_auth']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		  }
			
		  $sup_code1 = urldecode($sup_code);
		  $data['p']=$this->createpom->procesdure_run($sup_code1);
		  $this->db->close();
		  $this->db->initialize();
		  $data['p_new']=$this->createpom->procesdure_run_supplier($user_id, $selectpr, $sup_code);
		  $this->db->close();
		  $this->db->initialize();
		  
		  $data['chat_history'] = $this->createpom->chat_history_table($selectpr);
		  $data['r']=$this->createpom->pendal_info($sup_code);
		  $data['month']= $this->createpom->pendal_info_monthwise($sup_code1);
		  $data['last5'] = $this->createpom->pendal_info_last5_trans($sup_code);
		  $data['reorder'] = $this->createpom->pendal_info_reorder_lvl_qty($sup_code);
		  $data['monthofinvtry'] = $this->createpom->monthofinvtry($sup_code);
		  $data['orderinadvnce'] = $this->createpom->orderinadvnce($sup_code);
		  $data['wrkordr_ser']=$this->createpom->wrkordr_ser($sup_code);
		  $data['lstyrcumnrec1'] = $this->createpom->lstyrcumnrec1($sup_code);
		  $data['lastfivetrans1'] = $this->createpom->lastfivetrans1($sup_code);
		  $data['itemdescmaster'] = $this->createpom->itemdescmaster($sup_code);
		  $data['mnthwsecurryrcons1'] = $this->createpom->pendal_master_mnthwsecurryrcons($sup_code);
		  $data['mnthwsecurryrrcpt1'] = $this->createpom->pendal_master_mnthwsecurryrrcpt($sup_code);
		  $data['supplier1'] = $this->createpom->pendal_master_supplier($sup_code1);
		  $data['bomdetail1'] = $this->createpom->pendal_master_bomdetail($sup_code1);
		  $data['prerplive'] = $this->createpom->prerplive($selectpr);
		  
		  $this->load->view('admin/authorise_pr',$data);
	  }
	  
	  public function insert_pr_sub_auth($selectpr){
		  $this->load->library('email');
		  $this->email->set_mailtype("html");
		  $this->load->helper(array('form'));
		  $pr_num = $this->input->post('pr_num');
		  
		  if($pr_num != ''){
		  $drawing_no = $this->input->post('drawing_no');
		  $item_code = $this->input->post('item_code');
		  $item_desc1 = $this->input->post('itm_desc');
		  $item_desc = str_replace("'","",$item_desc1);
		  $uom = $this->input->post('trans_uom');
		  $quantity = $this->input->post('required_qty');
		  $need_date = $this->input->post('need_date'); 
		  $username = $_SESSION['username'];
		  $from_email = $_SESSION['username']."@tipl.com";
		  $pr_approval_remarks = $this->input->post('remarks_auth');
		  $approve_button = $this->input->post('APPROVE');
		  $current_stk = $this->input->post('current_stk');
		  $reservation_qty = $this->input->post('reservation_qty');
		  $reorder_level = $this->input->post('reorder_level');
		  $reorder_qty = $this->input->post('reorder_qty');
		  $last_supplier = $this->input->post('supplier_name');
		  $last_supplier_lead_time = $this->input->post('supplier_lead_time');
		  $last_purchase_rate = $this->input->post('supplier_rate');
		  $type_of_pr = $this->input->post('usage');
		  $category = $this->input->post('category');
		  $project_name = $this->input->post('pro_ordr_name');
		  $pm_group = $this->input->post('pm_group');
		  $last_yr_consumption = $this->input->post('lastyr_cons');
		  $level2_approval_req = $this->input->post("level2_approval_req");
		  $level2_approval_mailto = $this->input->post("level2_approval_mailto");
		  $color = $this->input->post('color');
		  $cost_available = $this->input->post('cost_available');
		  $cost = $this->input->post('costing');
		  $currency = $this->input->post('currency');
		  $cost_cal_rmks = $this->input->post('cost_calculation_remarks');
		  $cost_na_rmks = $this->input->post('cost_not_available_remarks');
		  $created_by = $this->input->post('created_by');
		  $workorder = $this->input->post('workorder');
		  $workorder = $this->input->post('workorder');
		  
		  if($atac_no_select == ''){
			$atac_selection = 'No';
		  }else{
			$atac_selection = 'Yes';
		  }
		  
		  if($workorder == ''){
			$wo = 'No';
		  }else{
			$wo = 'Yes';
		  }
		  
		  //Creator email
		  $data['creator_email']=$this->createpom->creator_email($created_by);
		  
		  foreach($data['creator_email']->result_array() AS $row) {
				$creator_email = $row['emp_email'];	
		  }
		  //Creator email
		  
		  //New Email code starts
		  if($approve_button == 'APPROVE' && $level2_approval_req == 'Yes'){
			  $status = "Approved";
			  $status1 = "Pending For Level 2 authorization.";
			  $level2_appr_full_mail = $level2_approval_mailto."";
			  
			  /*if($category == 'INSTRUMENTATION'){
				 $this->email->to('deen.mundra@tipl.com'); 
			  } else if($category == 'IT'){
				  $this->email->to('abhinav.toshniwal@tipl.com');
			  } else if($category == 'OTHERS'){
				  $this->email->to('abhinav.toshniwal@tipl.com');
			  }*/
			  
				$data['master_data']=$this->createpom->master_data($type_of_pr,$atac_selection,$wo,$category);
				foreach($data['master_data']->result_array() AS $row){
					$level1_approval_req = $row['level1_approval_req'];
					$level2_approval_req = $row['level2_approval_req'];
					$level1_approval_mailto = $row['level1_approval_mail_to'];
					$level1_approval_mailto_mail = $level1_approval_mailto."@tipl.com";
					$level1_email = $row['level1_email'];
					$level2_email = $row['level2_email'];
					$level2_approval_mailto = $row['level2_approval_mail_to'];
					$error = $row['error'];
					$mail_header_line = $row['mail_header_line'];
					//To Mail Address
					$this->email->to($level2_email);
				}
			  
		  } else if($approve_button == 'APPROVE' && ($level2_approval_req == 'No' || $level2_approval_req == '')) {
			  $status = "Approved";
			  $status1 = "Now you have to authorize this purchase request in ERP.";
			  $this->email->to('planning@tipl.com');
		  } else {
			  $status = "Disapproved";
			  $status1 = "You can't authorize this purchase request in ERP, untill it is not approved by Planning Manager.";
			  $this->email->to('planning@tipl.com');
		  }
			  
		  $this->email->from($from_email, $username);
		  $cc_email = "planning@tipl.com,".$creator_email;
		  $this->email->cc($cc_email);
		  $this->email->bcc('it.software@tipl.com');
		  $pr_num = $this->input->post('pr_num');
		  $subject = $pr_num."- PR Authorization Planning LVL1";
		  $this->email->subject($subject);
		  $message_table = "<table border='1' cellpadding='2' cellspacing='0'>
								<tr>
									<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
									<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
									<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
									<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
								</tr>
						  </table><br/><br/>
						  <table cellpadding='1' cellspacing='1' border='1' width='500px' 
						  style='border:solid 1px black; font-size:11px; padding:10px; border-radius:5px;'>
							  <tr style='background-color:#CFF'>
								  <td><b>IPR NO.</b></td>
								  <td><b>ITEM CODE</b></td>
								  <td><b>ITEM DESC.</b></td>
								  <td><b>UOM</b></td>
								  <td><b>QUANTITY</b></td>
								  <td><b>NEED DATE</b></td>
								  <td><b>CURRENT STK</b></td>
								  <td><b>RESERVATION QTY</b></td>
								  <td><b>REORDER LEVEL</b></td>
								  <td><b>REORDER QTY</b></td>
								  <td><b>LAST SUPPLIER</b></td>
								  <td><b>LAST SUPPLIER LEAD TIME</b></td>
								  <td><b>LAST PURCHASE RATE</b></td>
								  <td><b>TYPE OF PR</b></td>
								  <td><b>CATEGORY</b></td>
								  <td><b>PROJECT NAME</b></td>
								  <td><b>PM GROUP</b></td>
								  <td><b>LAST YR CONS.</b></td>
								  <td><b>COST AVAIL.</b></td>
								  <td><b>COSTING</b></td>
								  <td><b>CURRENCY</b></td>
								  <td><b>COST CAL. RMKS</b></td>
								  <td><b>COST NOT AVAIL. RMKS</b></td>
							  </tr>
							  <tr>
								  <td>".$pr_num."</td>
								  <td>".$item_code."</td>
								  <td>".$item_desc."</td>
								  <td>".$uom."</td>
								  <td>".number_format($quantity,2)."</td>
								  <td style='background-color:".$color."'>".$need_date."</td>
								  <td>".number_format($current_stk,2)."</td>
								  <td>".number_format($reservation_qty,2)."</td>
								  <td>".number_format($reorder_level,2)."</td>
								  <td>".number_format($reorder_qty,2)."</td>
								  <td>".$last_supplier."</td>
								  <td>".$last_supplier_lead_time."</td>
								  <td>".$last_purchase_rate."</td>
								  <td>".$type_of_pr."</td>
								  <td>".$category."</td>
								  <td>".$project_name."</td>
								  <td>".$pm_group."</td>
								  <td>".number_format($last_yr_cons,2)."</td>
								  <td>".$cost_available."</td>
								  <td>".$cost."</td>
								  <td>".$currency."</td>
								  <td>".$cost_cal_rmks."</td>
								  <td>".$cost_na_rmks."</td>
							  </tr>
						</table>";
		  
		   $message = "<b>PR Number - ".$pr_num."</b><br/><br/><p>Purchase Request is ".$status." in live.".$status1."</p><br/>
		   <p>Remarks : ".$pr_approval_remarks."</p><br/><p>ERP Created By : ".$created_by."</p><br/>".$message_table."<br/><b>Regards,</b>
		   <br><b style='text-transform:uppercase;'>".$username."</b>";
		  
		  $this->email->message($message);
		  if(!$this->email->send()){
			echo $this->email->print_debugger();
			die;
		  }
		  $data = array();
		  $this->createpom->insert_pr_sub_auth($data);
		  $data['message'] = 'Data Inserted Successfully';
		  $this->load->view('admin/insert_pr_sub');
		}
	  }
	  
	  /*********** Authorise Purchase Request Planning lvl2 Start ***************/
	   public function view_pr_auth_lvl2(){
		  $selectpr = $this->uri->segment(3);
		  $user_id = 1;
		  $data['v_auth']=$this->createpom->view_prqit_prno_auth($selectpr);
		  foreach($data['v_auth']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		  }
		  $sup_code1 = urldecode($sup_code);
		  $data['p']=$this->createpom->procesdure_run($sup_code);
		  $this->db->close();
		  $this->db->initialize();
		  $data['p_new']=$this->createpom->procesdure_run_supplier($user_id, $selectpr, $sup_code);
		  $this->db->close();
		  $this->db->initialize();
		  
		  //Chat History Query
		  $data['chat_history'] = $this->createpom->chat_history_table($selectpr);
		  
		  //Chat History Query
		  $data['r']=$this->createpom->pendal_info($sup_code);
		  $data['month']= $this->createpom->pendal_info_monthwise($sup_code1);			
		  $data['last5'] = $this->createpom->pendal_info_last5_trans($sup_code);			
		  $data['reorder'] = $this->createpom->pendal_info_reorder_lvl_qty($sup_code);			
		  $data['monthofinvtry'] = $this->createpom->monthofinvtry($sup_code);			
		  $data['orderinadvnce'] = $this->createpom->orderinadvnce($sup_code);			
		  $data['wrkordr_ser']=$this->createpom->wrkordr_ser($sup_code);			
		  $data['lstyrcumnrec1'] = $this->createpom->lstyrcumnrec1($sup_code);			
		  $data['lastfivetrans1'] = $this->createpom->lastfivetrans1($sup_code);			
		  $data['itemdescmaster'] = $this->createpom->itemdescmaster($sup_code1);		  
		  $data['mnthwsecurryrcons1'] = $this->createpom->pendal_master_mnthwsecurryrcons($sup_code1);		  
		  $data['mnthwsecurryrrcpt1'] = $this->createpom->pendal_master_mnthwsecurryrrcpt($sup_code1);		  
		  $data['supplier1'] = $this->createpom->pendal_master_supplier($sup_code1);		  
		  $data['bomdetail1'] = $this->createpom->pendal_master_bomdetail($sup_code1);		  
		  //Action Timing Report Queries		  
		  $data['prerplive'] = $this->createpom->prerplive($selectpr);
		  //Action Timing Report Queries				  
		  $this->load->view('admin/authorise_pr_lvl2',$data);	  
	  }
	  
	  public function insert_pr_sub_auth_lvl2($selectpr){
		  $this->load->library('email');
		  $this->email->set_mailtype("html");
		  $this->load->helper(array('form'));
		  $pr_num = $this->input->post('pr_num');
		  
		  if($pr_num != ''){	  
		  $drawing_no = $this->input->post('drawing_no');
		  $item_code = $this->input->post('item_code');
		  $item_desc1 = $this->input->post('itm_desc');
		  $item_desc = str_replace("'","",$item_desc1);
		  $uom = $this->input->post('trans_uom');
		  $quantity = $this->input->post('required_qty');
		  $need_date = $this->input->post('need_date'); 
		  $username = $_SESSION['username'];
		  $from_email = $_SESSION['username']."@tipl.com";
		  $pr_approval_remarks = $this->input->post('remarks_auth');
		  $approve_button = $this->input->post('APPROVE');
		  $current_stk = $this->input->post('current_stk');
		  $reservation_qty = $this->input->post('reservation_qty');
		  $reorder_level = $this->input->post('reorder_level');
		  $reorder_qty = $this->input->post('reorder_qty');
		  $last_supplier = $this->input->post('supplier_name');
		  $last_supplier_lead_time = $this->input->post('supplier_lead_time');
		  $last_purchase_rate = $this->input->post('supplier_rate');
		  $type_of_pr = $this->input->post('usage');
		  $category = $this->input->post('category');
		  $project_name = $this->input->post('pro_ordr_name');
		  $pm_group = $this->input->post('pm_group');
		  $last_yr_consumption = $this->input->post('lastyr_cons');
		  $color = $this->input->post('color');
		  $cost_available = $this->input->post('cost_available');
		  $cost = $this->input->post('costing');
		  $currency = $this->input->post('currency');
		  $cost_cal_rmks = $this->input->post('cost_calculation_remarks');
		  $cost_na_rmks = $this->input->post('cost_not_available_remarks');
		  $created_by = $this->input->post('created_by');
		  
		  $data['creator_email']=$this->createpom->creator_email($created_by);		  
		  foreach($data['creator_email']->result_array() AS $row) {
				$creator_email = $row['emp_email'];	
		  }
		  
		  if($approve_button == 'APPROVE'){			  
			  $status = "Approved";
			  $status1 = "Now you have to authorize this purchase request in ERP.";			  
		  } else {			  
			  $status = "Disapproved";
			  $status1 = "You can't authorize this purchase request in ERP, untill it is not approved by Planning Manager.";			  
		  }
		  
		  $this->email->to('planning@tipl.com');			  
		  $this->email->from($from_email, $username);		  
		  $cc_email = "planning@tipl.com,".$creator_email;		  
		  $this->email->cc($cc_email);		  
		  $this->email->bcc('it.software@tipl.com');		 
		  $pr_num = $this->input->post('pr_num');		  
		  $subject = $pr_num."- PR Authorization Planning LVL2";		 
		  $this->email->subject($subject);
		  
		  $message_table = "<table border='1' cellpadding='2' cellspacing='0'>
									<tr>
										<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
										<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
										<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
										<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
									</tr>
							  </table><br/><br/>
		  					  <table cellpadding='1' cellspacing='1' border='1' width='500px' style='border:solid 1px black; font-size:11px; padding:10px; border-radius:5px;'>
								  <tr style='background-color:#CFF'>
									  <td><b>IPR NO.</b></td>
									  <td><b>ITEM CODE</b></td>
									  <td><b>ITEM DESC.</b></td>
									  <td><b>UOM</b></td>
									  <td><b>QUANTITY</b></td>
									  <td><b>NEED DATE</b></td>
									  <td><b>CURRENT STK</b></td>
									  <td><b>RESERVATION QTY</b></td>
									  <td><b>REORDER LEVEL</b></td>
									  <td><b>REORDER QTY</b></td>
									  <td><b>LAST SUPPLIER</b></td>
									  <td><b>LAST SUPPLIER LEAD TIME</b></td>
									  <td><b>LAST PURCHASE RATE</b></td>
									  <td><b>TYPE OF PR</b></td>
									  <td><b>CATEGORY</b></td>
									  <td><b>PROJECT NAME</b></td>
									  <td><b>PM GROUP</b></td>
									  <td><b>LAST YR CONS.</b></td>
									  <td><b>COST AVAIL.</b></td>
									  <td><b>COSTING</b></td>
									  <td><b>CURRENCY</b></td>
									  <td><b>COST CAL. RMKS</b></td>
									  <td><b>COST NOT AVAIL. RMKS</b></td>
								  </tr>
								  <tr>
									  <td>".$pr_num."</td>
									  <td>".$item_code."</td>
									  <td>".$item_desc."</td>
									  <td>".$uom."</td>
									  <td>".number_format($quantity,2)."</td>
									  <td style='background-color:".$color."'>".$need_date."</td>
									  <td>".number_format($current_stk,2)."</td>
									  <td>".number_format($reservation_qty,2)."</td>
									  <td>".number_format($reorder_level,2)."</td>
									  <td>".number_format($reorder_qty,2)."</td>
									  <td>".$last_supplier."</td>
									  <td>".$last_supplier_lead_time."</td>
									  <td>".$last_purchase_rate."</td>
									  <td>".$type_of_pr."</td>
									  <td>".$category."</td>
									  <td>".$project_name."</td>
									  <td>".$pm_group."</td>
									  <td>".number_format($last_yr_cons,2)."</td>
									  <td>".$cost_available."</td>
									  <td>".$cost."</td>
									  <td>".$currency."</td>
									  <td>".$cost_cal_rmks."</td>
									  <td>".$cost_na_rmks."</td>
								  </tr>
							</table>";
		  
		   $message = "<b>PR Number - ".$pr_num."</b><br/><br/><p>Purchase Request is ".$status." in live.".$status1."</p><br/>
		   <p>Remarks : ".$pr_approval_remarks."</p><br/><p>ERP Created By : ".$created_by."</p><br/>".$message_table."<br/><b>Regards,</b>
		   <br><b style='text-transform:uppercase;'>".$username."</b>";
		  
		  $this->email->message($message);		  
		  if(!$this->email->send()){
			echo $this->email->print_debugger();
			die;
		  }

		  $data = array();		
		  $this->createpom->insert_pr_sub_auth_lvl2($data);		 
		  $data['message'] = 'Data Inserted Successfully';		
		  $this->load->view('admin/insert_pr_sub');		
		}
	  }
	  
	  /*********** Authorise Purchase Request Purchase Start ***************/	  
	   public function view_pr_auth_pur(){		   	  
		  $selectpr = $this->uri->segment(3);		  
		  $user_id = 1;				
		  $data['v_auth']=$this->createpom->view_prqit_prno_auth_pur($selectpr);		  
		  foreach($data['v_auth']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		  }			
		  $sup_code1 = urldecode($sup_code);			
		  $data['p']=$this->createpom->procesdure_run($sup_code);			
		  $this->db->close();			
		  $this->db->initialize();		  
		  $data['p_new']=$this->createpom->procesdure_run_supplier($user_id, $selectpr, $sup_code);			
		  $this->db->close();			
		  $this->db->initialize();	
		  		  
		  $data['chat_history'] = $this->createpom->chat_history_table($selectpr);			
		  $data['r']=$this->createpom->pendal_info($sup_code);			
		  $data['month']= $this->createpom->pendal_info_monthwise($sup_code1);			
		  $data['last5'] = $this->createpom->pendal_info_last5_trans($sup_code);			
		  $data['reorder'] = $this->createpom->pendal_info_reorder_lvl_qty($sup_code);			
		  $data['monthofinvtry'] = $this->createpom->monthofinvtry($sup_code);			
		  $data['orderinadvnce'] = $this->createpom->orderinadvnce($sup_code);			
		  $data['wrkordr_ser']=$this->createpom->wrkordr_ser($sup_code);			
		  $data['lstyrcumnrec1'] = $this->createpom->lstyrcumnrec1($sup_code);			
		  $data['lastfivetrans1'] = $this->createpom->lastfivetrans1($sup_code);			
		  $data['itemdescmaster'] = $this->createpom->itemdescmaster($sup_code1);		  
		  $data['mnthwsecurryrcons1'] = $this->createpom->pendal_master_mnthwsecurryrcons($sup_code1);		  
		  $data['mnthwsecurryrrcpt1'] = $this->createpom->pendal_master_mnthwsecurryrrcpt($sup_code1);		  
		  $data['supplier1'] = $this->createpom->pendal_master_supplier($sup_code1);		  
		  $data['bomdetail1'] = $this->createpom->pendal_master_bomdetail($sup_code1);		  
		  $data['prerplive'] = $this->createpom->prerplive($selectpr);				  
		  $this->load->view('admin/authorise_pr_pur',$data);	  
	  }
	  
	  public function insert_pr_sub_auth_pur($selectpr){		  
		  $this->load->library('email');
		  $this->email->set_mailtype("html");
		  $this->load->helper(array('form'));		  
		  $pr_num = $this->input->post('pr_num');
		  
		  if($pr_num != ''){		  
			  $drawing_no = $this->input->post('drawing_no');
			  $item_code = $this->input->post('item_code');
			  $item_desc1 = $this->input->post('itm_desc');
			  $item_desc = str_replace("'","",$item_desc1);
			  $uom = $this->input->post('trans_uom');
			  $quantity = $this->input->post('required_qty');
			  $need_date1 = $this->input->post('need_date'); 
			  $need_date = date("d-m-Y", strtotime($need_date1));
			  $username = $_SESSION['username'];
			  $from_email = $_SESSION['username']."@tipl.com";
			  $pr_approval_remarks = $this->input->post('remarks_auth');
			  $approve_button = $this->input->post('APPROVE');	
			  $current_stk = $this->input->post('current_stk');
			  $reservation_qty = $this->input->post('reservation_qty');
			  $reorder_level = $this->input->post('reorder_level');
			  $reorder_qty = $this->input->post('reorder_qty');
			  $last_supplier = $this->input->post('supplier_name');
			  $last_supplier_lead_time = $this->input->post('supplier_lead_time');
			  $last_purchase_rate = $this->input->post('supplier_rate');
			  $type_of_pr = $this->input->post('usage');
			  $category = $this->input->post('category');
			  $project_name = $this->input->post('pro_ordr_name');
			  $project_group = $this->input->post('pm_group');
			  $last_yr_consumption = $this->input->post('lastyr_cons');
			  $cost_available = $this->input->post('cost_available');
			  $cost = $this->input->post('costing');
			  $currency = $this->input->post('currency');
			  $cost_cal_rmks = $this->input->post('cost_calculation_remarks');
			  $cost_na_rmks = $this->input->post('cost_not_available_remarks');
			  $created_by = $this->input->post('created_by');		  
			  $data['creator_email']=$this->createpom->creator_email($created_by);		  
			  foreach($data['creator_email']->result_array() AS $row) {
					$created_by_email = $row['emp_email'];
			  }
					  
			  if($approve_button == 'DISAPPROVE'){			  
				  $status = "Disapproved By Purchase";
				  $status1 = "You have to short close this Purchase Request and create new Purchase Request";  
			  }
			   
			  
			  $this->email->to($created_by_email);		  
			  $cc_email = "planning@tipl.com,abhinav.toshiwal@tipl.com,".$from_email;			  
			  $this->email->from($from_email, $username);		  
			  $this->email->cc($cc_email);		  
			  $this->email->bcc('it.software@tipl.com');		 
			  $pr_num = $this->input->post('pr_num');		  
			  $subject = $pr_num."- PR Disapproved By Purchase";		 
			  $this->email->subject($subject);		  
			  $message_table = "
			  <table border='1' cellpadding='2' cellspacing='0'>
					<tr>
						<td style='background-color:red'>NEED DATE < (CURRENT DATE + LEAD TIME)</td>
						<td style='background-color:green'>NEED DATE > (CURRENT DATE + LEAD TIME)</td>
						<td style='background-color:blue'>NEED DATE = (CURRENT DATE + LEAD TIME)</td>
						<td style='background-color:yellow'>FIRST TIME PURCHASE</td>
					</tr>
			  </table><br/><br/>
			  <table cellpadding='1' cellspacing='1' border='1' width='500px' style='border:solid 1px black; font-size:11px; padding:10px; border-radius:5px;'>
				  <tr style='background-color:#CFF'>
					  <td><b>IPR NO.</b></td>
					  <td><b>ITEM CODE</b></td>
					  <td><b>ITEM DESC.</b></td>
					  <td><b>UOM</b></td>
					  <td><b>QUANTITY</b></td>
					  <td><b>NEED DATE</b></td>
					  <td><b>CURRENT STK</b></td>
					  <td><b>RESERVATION QTY</b></td>
					  <td><b>REORDER LEVEL</b></td>
					  <td><b>REORDER QTY</b></td>
					  <td><b>LAST SUPPLIER</b></td>
					  <td><b>LAST SUPPLIER LEAD TIME</b></td>
					  <td><b>LAST PURCHASE RATE</b></td>
					  <td><b>TYPE OF PR</b></td>
					  <td><b>CATEGORY</b></td>
					  <td><b>PROJECT NAME</b></td>
					  <td><b>PM GROUP</b></td>
					  <td><b>LAST YR CONS.</b></td>
					  <td><b>COST AVAIL.</b></td>
					  <td><b>COSTING</b></td>
					  <td><b>CURRENCY</b></td>
					  <td><b>COST CAL. RMKS</b></td>
					  <td><b>COST NOT AVAIL. RMKS</b></td>
				  </tr>
				  <tr>
					  <td>".$pr_num."</td>
					  <td>".$item_code."</td>
					  <td>".$item_desc."</td>
					  <td>".$uom."</td>
					  <td>".number_format($quantity,2)."</td>
					  <td style='background-color:".$color."'>".$need_date."</td>
					  <td>".number_format($current_stk,2)."</td>
					  <td>".number_format($reservation_qty,2)."</td>
					  <td>".number_format($reorder_level,2)."</td>
					  <td>".number_format($reorder_qty,2)."</td>
					  <td>".$last_supplier."</td>
					  <td>".$last_supplier_lead_time."</td>
					  <td>".$last_purchase_rate."</td>
					  <td>".$type_of_pr."</td>
					  <td>".$category."</td>
					  <td>".$project_name."</td>
					  <td>".$pm_group."</td>
					  <td>".number_format($last_yr_cons,2)."</td>
					  <td>".$cost_available."</td>
					  <td>".$cost."</td>
					  <td>".$currency."</td>
					  <td>".$cost_cal_rmks."</td>
					  <td>".$cost_na_rmks."</td>
				  </tr>
			</table>";
			  
			$message = "<b>PR Number - ".$pr_num."</b><br/><br/><p>Purchase Request is ".$status." in live.".$status1."</p><br/><p>Remarks : ".$pr_approval_remarks."</p><br/><p>ERP Created By : ".$created_by."</p><br>".$message_table."<br/><b>Regards,</b><br><b style='text-transform:uppercase;'>".$username."</b>";		  
			  $this->email->message($message);
			  if(!$this->email->send()){
				echo $this->email->print_debugger();
				die;
			  }
					  
			  $data = array();		
			  $this->createpom->insert_pr_sub_auth_pur($data);		 
			  $data['message'] = 'Data Inserted Successfully';		
			  $this->load->view('admin/insert_pr_sub');		 
		 }
	  }
	  /*********** Live Authorise Purchase Request Not Authorized in ERP ***************/	  
	  public function erp_not_auth_pr(){		  	  
		  $selectpr = $this->uri->segment(3);		  
		  $user_id = 1;				
		  $data['v_auth']=$this->createpom->view_prqit_prno_auth_pur($selectpr);		  
		  foreach($data['v_auth']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		  }
			
		  $sup_code1 = urldecode($sup_code);			
		  $data['p']=$this->createpom->procesdure_run($sup_code);			
		  $this->db->close();			
		  $this->db->initialize();		  
		  $data['p_new']=$this->createpom->procesdure_run_supplier($user_id, $selectpr, $sup_code);			
		  $this->db->close();			
		  $this->db->initialize();
		  
		  $data['chat_history'] = $this->createpom->chat_history_table($selectpr);
		  $data['r']=$this->createpom->pendal_info($sup_code);
		  $data['month']= $this->createpom->pendal_info_monthwise($sup_code1);
		  $data['last5'] = $this->createpom->pendal_info_last5_trans($sup_code);
		  $data['reorder'] = $this->createpom->pendal_info_reorder_lvl_qty($sup_code);
		  $data['monthofinvtry'] = $this->createpom->monthofinvtry($sup_code);
		  $data['orderinadvnce'] = $this->createpom->orderinadvnce($sup_code);
		  $data['wrkordr_ser']=$this->createpom->wrkordr_ser($sup_code);
		  $data['lstyrcumnrec1'] = $this->createpom->lstyrcumnrec1($sup_code);
		  $data['lastfivetrans1'] = $this->createpom->lastfivetrans1($sup_code);
		  $data['itemdescmaster'] = $this->createpom->itemdescmaster($sup_code1);
		  $data['mnthwsecurryrcons1'] = $this->createpom->pendal_master_mnthwsecurryrcons($sup_code1);
		  $data['mnthwsecurryrrcpt1'] = $this->createpom->pendal_master_mnthwsecurryrrcpt($sup_code1);
		  $data['supplier1'] = $this->createpom->pendal_master_supplier($sup_code1);
		  $data['bomdetail1'] = $this->createpom->pendal_master_bomdetail($sup_code1);
		  $data['prerplive'] = $this->createpom->prerplive($selectpr);
		  $this->load->view('admin/erpnotauthpr_view',$data);
	  }
	  
	  /*********** Live Authorise Purchase Request Not Authorized in ERP End ***************/
	  public function atac_project(){
		 $this->load->view('admin/atac_project');
	  }
	  
	  public function atac_special(){
		 $this->load->view('admin/atac_special');
	  }
	  
	  public function atac_project_preorder(){
		 $this->load->view('admin/atac_project_preorder');
	  }
	  
	  //Special Cases PM Group Selection
	  public function pr_spcl_pm_sel(){
		 $this->load->view('admin/pr_spcl_pm_sel');
	  }
	  
	  /********************** PR DISAPPROVAL PAGE STARTS **********************/
	  public function create_pr_disapproved(){   
		 $data['h']=$this->createpom->select_prqit_prno();
		 $this->load->view('admin/create_pr_disapproved', $data);
      }
	  
	  public function view_pr_disapproved(){
		  $selectpr = $_REQUEST['q'];
		  $user_id = 1;
		  $data['v']=$this->createpom->view_prqit_prno($selectpr);		  
		  foreach($data['v']->result_array() AS $row) {
			$sup_code = $row['prqit_itemcode'];
		  }
			
		  $sup_code1 = urldecode($sup_code);			
		  $data['p']=$this->createpom->procesdure_run($sup_code);			
		  $this->db->close();			
		  $this->db->initialize();		  
		  $data['p_new']=$this->createpom->procesdure_run_supplier($user_id, $selectpr, $sup_code);			
		  $this->db->close();			
		  $this->db->initialize();		  
		  $data['chat_history'] = $this->createpom->chat_history_table($selectpr);		  
		  $data['prerp'] = $this->createpom->prerp($selectpr);			
		  $data['r']=$this->createpom->pendal_info($sup_code);			
		  $data['month']= $this->createpom->pendal_info_monthwise($sup_code1);			
		  $data['last5'] = $this->createpom->pendal_info_last5_trans($sup_code);			
		  $data['reorder'] = $this->createpom->pendal_info_reorder_lvl_qty($sup_code);			
		  $data['monthofinvtry'] = $this->createpom->monthofinvtry($sup_code);			
		  $data['orderinadvnce'] = $this->createpom->orderinadvnce($sup_code);			
		  $data['wrkordr_ser']=$this->createpom->wrkordr_ser($sup_code);			
		  $data['lstyrcumnrec1'] = $this->createpom->lstyrcumnrec1($sup_code);			
		  $data['lastfivetrans1'] = $this->createpom->lastfivetrans1($sup_code);			
		  $data['itemdescmaster'] = $this->createpom->itemdescmaster($sup_code1);		  
		  $data['mnthwsecurryrcons1'] = $this->createpom->pendal_master_mnthwsecurryrcons($sup_code1);		  
		  $data['mnthwsecurryrrcpt1'] = $this->createpom->pendal_master_mnthwsecurryrrcpt($sup_code1);		  
		  $data['supplier1'] = $this->createpom->pendal_master_supplier($sup_code1);		  
		  $data['bomdetail1'] = $this->createpom->pendal_master_bomdetail($sup_code1);	  
		  $this->load->view('admin/view_pr_disapproved',$data);
	  } 
	  /********************** PR DISAPPROVAL PAGE ENDS **********************/
   }

