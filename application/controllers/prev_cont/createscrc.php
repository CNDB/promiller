<?php
class Createscrc extends CI_Controller{  
	function __construct(){
		parent::__construct();
		user_act();
	}
	
	public function index(){  
		$this->load->helper('url');
		$this->load->database(); 
		$this->load->model('createscrm');
		$data['h']=$this->createscrm->select_scrit_prno();
		$this->load->view('admin/create_scr', $data); 
	} 
	
	public function create_scr(){  
		$this->load->helper('url');
		$this->load->database();  
		$this->load->model('createscrm');
		$data['h']=$this->createscrm->select_scrit_scrno();
		$this->load->view('admin/create_scr', $data);  
	}
	
	public function view_scr(){
		$this->load->helper('url');
		$this->load->database();
		$this->load->model('createscrm');
		$selectpr = $_REQUEST['q'];
		$data['v']=$this->createscrm->view_prqit_prno($selectpr);
		foreach($data['v']->result_array() AS $row) {
			$sup_code = $row['scrit_itemcode'];
		}
		$sup_code1 = urldecode($sup_code);
		$data['p']=$this->createscrm->procesdure_run($sup_code);
		$this->db->close();
		$this->db->initialize();
		$data['r']=$this->createscrm->pendal_info($sup_code);
		$data['month']= $this->createscrm->pendal_info_monthwise($sup_code);
		$data['last5'] = $this->createscrm->pendal_info_last5_trans($sup_code);
		$data['reorder'] = $this->createscrm->pendal_info_reorder_lvl_qty($sup_code);
		$data['monthofinvtry'] = $this->createscrm->monthofinvtry($sup_code);
		$data['orderinadvnce'] = $this->createscrm->orderinadvnce($sup_code);
		$data['wrkordr_ser']=$this->createscrm->wrkordr_ser($sup_code);
		$data['lstyrcumnrec1'] = $this->createscrm->lstyrcumnrec1($sup_code);
		$data['lastfivetrans1'] = $this->createscrm->lastfivetrans1($sup_code);
		$data['itemdescmaster'] = $this->createscrm->itemdescmaster($sup_code1);
		$this->load->view('admin/view_scr',$data);
	}
}
