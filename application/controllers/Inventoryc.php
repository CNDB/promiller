<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Inventoryc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('inventorym');
	}
	
	//Reports
	public function index(){ 
		$this->load->view('admin/inventorydb'); 
	}
}
