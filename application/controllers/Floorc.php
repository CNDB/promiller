<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Floorc extends CI_Controller {
 
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->database(); 
		 $this->load->model('floorm');
	}
	
	//Reports
	public function index(){ 
		$this->load->view('admin/FloorDB'); 
	}

	public function FloorViewRpt(){ 
		$this->load->view('admin/FloorViewRpt'); 
	}
	
	//Vehicle Entry
	public function VhclEntryView(){ 
		$this->load->view('admin/FloorView'); 
	}
	
	public function VhclEntry(){
		
		$data = array();
		$data['VhclEntry'] = $this->floorm->VhclEntry($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}
	
	//Weighing
	public function weighing(){
		$this->load->view('admin/FloorViewWeighing');
	}
	
	public function VhclWeighing(){
		
		$data = array();
		$data['VhclEntry'] = $this->floorm->VhclEntryWeighing($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}
	
	//Sample Test
	public function SampleTest(){
		$this->load->view('admin/FloorViewST');
	}
	
	public function VhclST(){
		
		$data = array();
		$data['VhclEntry'] = $this->floorm->VhclEntrySt($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}
	
	//Sample Test Approval
	public function SampleTestApp(){
		$this->load->view('admin/FloorViewSTApp');
	}
	
	public function VhclSTApp(){
		
		$data = array();
		$data['VhclEntry'] = $this->floorm->VhclEntryStApp($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}
	
	//Unloading
	public function Unloading(){
		$this->load->view('admin/FloorViewUnloading');
	}
	
	public function VhclUnloading(){
		
		$data = array();
		$data['VhclEntry'] = $this->floorm->VhclEntryUnload($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}
	
	//Gate Exit
	public function GateExit(){
		$this->load->view('admin/FloorViewGateExit');
	}
	
	public function VhclGateExit(){
		
		$data = array();
		$data['VhclEntry'] = $this->floorm->VhclEntryGateExit($data);
		$data['message'] = 'Data Inserted Successfully';
		$this->load->view('admin/QueryPage', $data);
	}
	
	//Completed Gate Exit
	public function CompletedGateExit(){
		$this->load->view('admin/FloorViewCompletedGE');
	}
	
	//Reports
	public function floorRpt(){ 
		$this->load->view('admin/FloorViewRpt'); 
	}
	
}
