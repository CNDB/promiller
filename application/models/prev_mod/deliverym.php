<?php
class deliverym extends CI_Model  
{  
	function __construct()  
	{   
		parent::__construct();  
	}
	
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	{
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		return $query; 
	}
	
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
		where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");  
		
		return $query;  
	}
	
	public function insert_po_sub_lvl2($data)
	{
		$po_num  = $this->input->post("po_num");
		$po_s_no  = $this->input->post("po_s_no");
		$docket_detail   = $this->input->post("docket_detail");
		$transporter   = $this->input->post("transporter");
		$delivery_date   = $this->input->post("delivery_date");
		$deli_apprve   = $this->input->post("deli_apprve");
		$deli_apprve_by    = $_SESSION['username'];
		$deli_apprve_date  =  date('Y-m-d H:i:s');
		$status = 'Submit Delivery';
		
		//Transanction Start
		$this->db->trans_start();
		
		$this->db->query("update TIPLDB..insert_po set docket_detail = '$docket_detail', transporter = '$transporter', delivery_date = '$delivery_date',     				        deli_apprve = '$deli_apprve', deli_apprve_by = '$deli_apprve_by', deli_apprve_date = '$deli_apprve_date', status = '$status' 
		where po_num = '$po_num'");
		
		$this->db->query("update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'");
		
		$this->db->query("insert into TIPLDB..po_delivery_details_history (po_num, docket_details, transporter, delivery_date, deli_approve, deli_approve_by, 			        deli_approve_date) values ('$po_num','$docket_detail','$transporter','$delivery_date','$deli_apprve','$deli_apprve_by','$deli_apprve_date')");
		
		$this->db->trans_complete();
		//Transanction Complete
	}
}  
?>