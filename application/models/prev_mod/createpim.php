<?php
class createpim extends CI_Model  
{  
	function __construct()  
	{    
		parent::__construct();  
	}  
 
	public function select_prqit_prno()  
	{  
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus ='FR'");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	{
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		return $query; 
	}

	public function view_poqit_pono($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
								   where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");
		  
		return $query;  
	}
	
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	//New Code
	
	public function po_details($po_num){
		  
		$query = $this->db->query("select *,datediff(DAY, a.po_create_date, getdate()) as diff from tipldb..po_master_table a, TIPLDB..insert_po b, scmdb..po_pomas_pur_order_hdr c where a.po_num = '$po_num' and a.po_num = b.po_num and a.po_num = c.pomas_pono
and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num')");  
		
		return $query;  
		
	}
	
	public function po_item_details($po_num){
		  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, 
		scmdb..po_pomas_pur_order_hdr b where a.po_num = '$po_num' and a.po_num = b.pomas_pono 
		and b.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num')");  
		
		return $query;  
		
	}
	
	//New Code
		   
	public function insert_po_sub($data, $pi_attached)
	{
		$username = $_SESSION['username'];
		$po_num  = $this->input->post("po_num");
		$pi_rmks  = $this->input->post('pi_rmks');
		$pi_amount  = $this->input->post('pi_amount');
		$pi_create_inst = $this->input->post('pi_create_inst');
		$pi_created_date = date('Y-m-d H:i:s');
		$pi_createdby    = $_SESSION['username'];
		$pi_recieved_date = $this->input->post('pi_recieved_date');
		
		if( $pi_amount >= 50000){
			$status = "PI Send For Approval";
		} else {
			$status = "Send PI Payment To Supplier ";	
		}
		
		// Trasanction Start
		
		$this->db->trans_start();
		
		/*$this->db->query("update TIPLDB..insert_po set pi_rmks = '$pi_rmks', pi_amount = '$pi_amount', attach_pi = '$pi_attached', 
		pi_create_inst = '$pi_create_inst', pi_createdby = '$pi_createdby', status = '$status', pi_created_date = '$pi_created_date', 
		pi_recieved_date = '$pi_recieved_date' where po_num = '$po_num' ");
		
		$this->db->query("update tipldb..po_master_table set status = '$status' where po_num = '$po_num'");*/
		
		$this->db->trans_complete();
		
		//Transanction Complete
	
	}
		/********* Level 2 **********/	 
	 
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
									where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");  
		
		return $query;  
	}
	
	public function insert_po_sub_lvl2($data)
	{
		$po_num  = $this->input->post("po_num");
		$po_s_no  = $this->input->post("po_s_no");
		$pi_create_inst_lvl2   = $this->input->post("pi_create_inst_lvl2");
		$pi_createdby_lvl2    = $_SESSION['username'];
		$pi_created_date_lvl2  =  date('Y-m-d H:i:s');
		
		if($pi_create_inst_lvl2 = 'PI Approve'){
			$status = "Send PI Payment To Supplier";
		} else {
			$status = "PI Send For Approval";	
		}
		
		// Trasanction Start
		
		$this->db->trans_start();
		
		/*$this->db->query("update TIPLDB..insert_po set pi_create_inst_lvl2 = '$pi_create_inst_lvl2', pi_createdby_lvl2 = '$pi_createdby_lvl2', 			          status='$status', pi_created_date_lvl2 = '$pi_created_date_lvl2' where po_num = '$po_num' and po_s_no = '$po_s_no' ");
		
		$this->db->query("update tipldb..po_master_table set status = '$status' where po_num = '$po_num'");*/
		
		$this->db->trans_complete();
		
		//Transanction Complete
	}	 
}  
?>