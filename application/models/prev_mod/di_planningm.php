<?php
class di_planningm extends CI_Model{
	  
	function __construct(){   
		parent::__construct();  
	}  
	
	public function max_amend_no($po_num){
		 
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		 return $query; 
	 }
	 
	 public function view_po_details($po_num, $amend_no){  
	 
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$po_num' and a.po_num = b.pomas_pono and a.po_num = c.po_num and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num') order by a.po_line_no");  

         return $query;  
     }
	 
	 public function insert_di_plan($data){  
	 
	 	$po_num = $this->input->post('po_num');
		
		$remarks = $this->input->post('remarks');
		
		$remarks = str_replace("'","",$remarks);
		
		$status = "Dispatch Instruction Given by Planning";
		
		$created_by = $_SESSION['username'];
		
		$created_date = date("Y-m-d H:i:s");
		
		//Transaction Start
		$this->db->trans_start();
	 
		$sql = "update tipldb..po_master_table set di_plan_rmks = '$remarks', 
		di_plan_by = '$created_by', di_plan_date = '$created_date', status = '$status', di_given = 'Yes' where po_num = '$po_num'";  
		
		$query = $this->db->query($sql);  
		
		//$this->db->query("exec tipldb..po_l3_approval");
		
		$this->db->trans_complete();
		//Transanction Complete
	}
	
	public function mail_table($po_num){
		   
         $query = $this->db->query("select * from tipldb..po_master_table a, tipldb..insert_po b where b.po_num = '$po_num' and a.po_num = b.po_num");  

         return $query;  
     }
	 
	   
}  
?>