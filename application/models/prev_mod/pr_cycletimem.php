<?php
class pr_cycletimem extends CI_Model{
  
	function __construct(){   
		parent::__construct();  
	} 
	
	//MRP PROCEDURE 
	public function po_procedure(){  
		 $query = $this->db->query("exec TIPLDB..po_l3_approval");  
		 return $query;  
	}
	
	public function supp_procedure(){
		$query = $this->db->query("EXEC TIPLDB..supp_analysis_report");
		return $query;
	}
	
	public function all_supplier_data(){
		$query = $this->db->query("select * from tipldb..supp_analysis_report_main where category is not null order by sno");
		return $query;
	}
	
	public function all_supplier_data_ajax($category){
		if($category == 'All'){
			$query = $this->db->query("select * from tipldb..supp_analysis_report_main 
			where category is not null order by sno");
		} else {
			$query = $this->db->query("select * from tipldb..supp_analysis_report_main 
			where category is not null and category like'%$category%' order by sno");
		}
		
		return $query;
	}
	
	public function category(){
		$query = $this->db->query("select distinct live_category from tipldb..erp_live_category where live_category != 'service'");
		return $query;
	}
	
	public function ndva_proc(){
		$query = $this->db->query("exec TIPLDB..pr_need_date_vs_actual_proc");  
		return $query;	
	}
}  
?>