<?php
class po_master_updatem extends CI_Model{
	  
      function __construct(){
		     
         parent::__construct();
		   
      }  
	   
      public function po_app_mstr_view(){
		    
         $query = $this->db->query("select * from tipldb..po_approval_master_bkup order by po_type");  

         return $query;  
      }
	 
	  
	   
	  public function update_po_app_mstr($data){
		  
		$last_modified_by = $_SESSION['username'];
		$last_modified_date = date('Y-m-d H:i:s');
		$lvl1_app_val = $this->input->post('lvl1_app_val');
		$lvl2_app_val = $this->input->post('lvl2_app_val');
		$category = $this->input->post('category');
		$po_type = $this->input->post('po_type');
		$array_count = count($category);
		
		//Transanction Start
		$this->db->trans_start();
		
			for($i=0;$i<$array_count;$i++){
		
				$sql1 = "update tipldb..po_approval_master set lvl1_app_val = '".$lvl1_app_val[$i]."', lvl2_app_val = '".$lvl2_app_val[$i]."',
				last_modified_by = '".$last_modified_by."', last_modified_date = '".$last_modified_date."'
				where category = '".$category[$i]."' and po_type = '".$po_type[$i]."'";
				
				$this->db->query($sql1);
				
				$sql2 = "update tipldb..po_approval_master_bkup set lvl1_app_val = '".$lvl1_app_val[$i]."', lvl2_app_val = '".$lvl2_app_val[$i]."',
				last_modified_by = '".$last_modified_by."', last_modified_date = '".$last_modified_date."'
				where category = '".$category[$i]."' and po_type = '".$po_type[$i]."'";
				
				$this->db->query($sql2);
			
			}
		
		$this->db->trans_complete();
		//Transanction Complete
		
	  } 
}  
?>