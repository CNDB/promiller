<?php
class view_pom extends CI_Model{
	  
      function __construct()  
      {   
         parent::__construct();  
      }
	  
	  
	 public function max_amend_no($po_num)
	 {
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		 return $query; 
	 }
	 
	 public function view_poqit_pono($po_num, $amend_no)  
     {
		 $query1 = $this->db->query("select * from scmdb..po_poprq_poprcovg_detail where poprq_pono = '".$po_num."' ");
		 
		 if( $query1->num_rows() > 0) {
		   
         $query = $this->db->query("select *, datediff(DAY, a.pomas_podate, getdate()) as diff, datediff(DAY, i.preqm_prdate, getdate()) as diff1 
from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, scmdb..po_poprq_poprcovg_detail d, 		        		      
scmdb..supp_addr_address e, scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h, 
scmdb..prq_preqm_pur_reqst_hdr i, scmdb..supp_bu_suplmain j where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
and a.pomas_pono = d.poprq_pono and a.pomas_pono ='".$po_num."' and b.poitm_polineno = d.poprq_polineno and c.supp_spmn_supcode = e.supp_addr_supcode 	       
and b.poitm_itemcode = f.ml_itemcode and f.ml_itemcode = g.lov_itemcode and a.pomas_pono = h.paytm_pono 
and a.pomas_poamendmentno = '".$amend_no."' and a.pomas_poamendmentno = b.poitm_poamendmentno and a.pomas_poamendmentno = d.poprq_poamendmentno and 		         
a.pomas_poamendmentno = h.paytm_poamendmentno and d.poprq_prno = i.preqm_prno and e.supp_addr_supcode = j.supp_bu_supcode 
and e.supp_addr_addid = j.supp_bu_deforderto");
		 
		 } else {
			 
		 $query = $this->db->query("select *, datediff(DAY, a.pomas_podate, getdate()) as diff 
from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, 		        					            			        
scmdb..supp_addr_address e, scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h, scmdb..supp_bu_suplmain i
where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
and a.pomas_pono ='".$po_num."' and c.supp_spmn_supcode = e.supp_addr_supcode and b.poitm_itemcode = f.ml_itemcode and f.ml_itemcode = g.lov_itemcode         
and a.pomas_pono = h.paytm_pono and a.pomas_poamendmentno = '".$amend_no."' and a.pomas_poamendmentno = b.poitm_poamendmentno 
and a.pomas_poamendmentno = h.paytm_poamendmentno and e.supp_addr_supcode = i.supp_bu_supcode and e.supp_addr_addid = i.supp_bu_deforderto"); 
		 
		 }
		 							 
         return $query;  
     }
	 
	 public function act_timing($po_num)  
     {  
		 $query = $this->db->query("select pomas_pono, pomas_createdby, pomas_createddate, pomas_lastmodifiedby, pomas_lastmodifieddate
from SCMDB..po_pomas_pur_order_hdr where pomas_pono = '".$po_num."'");  

         return $query;
     }
}  
?>