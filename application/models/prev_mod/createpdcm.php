<?php
class createpdcm extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function select_prqit_prno()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus ='FR'");  

         return $query;  
     }
	 
	 public function max_amend_no($selectpr)
	 {
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		 return $query; 
	 }
	 
	 public function view_poqit_pono($selectpr, $amend_no)  
      {  
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
		 where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'"); 
		  
         return $query;  
      }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	   
	   public function insert_po_sub($data, $NewFileName2)
	  {
		$username = $_SESSION['username'];
		$po_num = $this->input->post("po_num");
		$pdc_check_no = $this->input->post("pdc_check_no");
		$pdc_check_date = $this->input->post("pdc_check_date");
		$pdc_bank_name = $this->input->post("pdc_bank_name");
		$pdc_check_amount = $this->input->post("pdc_check_amount");
		$pdc_check_amount = $this->input->post("pdc_check_amount");
		$pdc_create_inst = $this->input->post("pdc_create_inst");
		$pdc_create_by = $_SESSION['username'];
		$pdc_create_date = date('Y-m-d H:i:s');
		$status = 'PDC Payment Done To Supplier';
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql1 = "update tipldb..insert_po set pdc_check_no = '$pdc_check_no', pdc_check_date = '$pdc_check_date', pdc_bank_name = '$pdc_bank_name', 
		pdc_check_amount = '$pdc_check_amount', uploaded_check_img = '$NewFileName2', pdc_create_inst = '$pdc_create_inst', 
		pdc_created_by = '$username', pdc_create_date = '$pdc_create_date', status = '$status' where po_num = '$po_num' ";
		
		$sql2 = "update tipldb..po_master_table set status = '$status' where po_num = '$po_num'";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		//Transanction Complete
		
	  }
	 
   }  
?>