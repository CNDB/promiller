<?php
class flowm extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }
	  
	 
	 
	 //ERP Fresh Purchase Orders
	 public function select_prqit_pono(){
		$query = $this->db->query("select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono not in(select po_num from tipldb..insert_po)
		
		UNION
		
		select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono in(select po_num from tipldb..insert_po where status = 'Draft') 
		ORDER BY  a.pomas_pono ASC");  
           
         return $query;  
     }
	 
	 public function select_prqit_pono_fpo(){
		   
         $query = $this->db->query("select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('FPO') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono not in(select po_num from tipldb..insert_po)
		UNION
		select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('FPO') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono in(select po_num from tipldb..insert_po where status = 'Draft') ORDER BY  a.pomas_pono ASC");  
           
         return $query;  
     }
	 
	 public function select_prqit_pono_ipo(){
		   
        $query = $this->db->query("select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('IPO') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono not in(select po_num from tipldb..insert_po)
		UNION
		select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('IPO') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono in(select po_num from tipldb..insert_po where status = 'Draft') ORDER BY  a.pomas_pono ASC" );  
           
         return $query;  
     }
	 
	 public function select_prqit_pono_lpo(){ 
	  
        $query = $this->db->query("select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('LPO') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono not in(select po_num from tipldb..insert_po)
		UNION
		select a.pomas_pono, a.pomas_pobasicvalue, a.pomas_tcdtotalrate, a.pomas_tcal_total_amount, a.pomas_createdby, 
		datediff(DAY,a.pomas_podate,getdate()) as diff, a.pomas_suppliercode, b.supp_spmn_supname
		from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('LPO') and a.pomas_podocstatus in ('FR')
		and a.pomas_suppliercode = b.supp_spmn_supcode
		and a.pomas_pono in(select po_num from tipldb..insert_po where status = 'Draft') ORDER BY  a.pomas_pono ASC" );  
           
         return $query;  
     }
	 
	 //Live Purchase Orders Pending For Level 1 Authorization
	 public function select_prqit_prno_lvl1()  
      {  
		 $user_name = $_SESSION['username'];

		 $query = $this->db->query("select distinct po_num, po_supp_name, level1_mail_to, datediff(DAY,po_create_date,getdate()) as diff 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and b.pomas_podocstatus  in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV')
and status in('PO Send For Level 1 Authorization') ORDER BY  po_num ASC");
 
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl1_fpo()  
      {  
		 $user_name = $_SESSION['username'];
		 
		 $query = $this->db->query("select distinct po_num, po_supp_name, level1_mail_to, datediff(DAY,po_create_date,getdate()) as diff 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and b.pomas_podocstatus  in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('FPO')
and status in('PO Send For Level 1 Authorization') ORDER BY  po_num ASC");
 
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl1_ipo()  
      {  
		 $user_name = $_SESSION['username'];
		 
		 $query = $this->db->query("select distinct po_num, po_supp_name, level1_mail_to, datediff(DAY,po_create_date,getdate()) as diff 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and b.pomas_podocstatus  in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('IPO')
and status in('PO Send For Level 1 Authorization') ORDER BY  po_num ASC");
 
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl1_lpo()  
      {  
		 $user_name = $_SESSION['username'];
		 
		 $query = $this->db->query("select distinct po_num, po_supp_name, level1_mail_to, datediff(DAY,po_create_date,getdate()) as diff 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and b.pomas_podocstatus  in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('LPO')
and status in('PO Send For Level 1 Authorization') ORDER BY  po_num ASC");
 
         return $query;  
     }
	 
	 //Disapproved Purchase Orders At Level 1
	 public function disapproved_po_lvl1()  
      {  
		 $query = $this->db->query("select po_num, po_supp_name, po_total_value, datediff(DAY,b.po_create_date,getdate()) as diff, b.created_by from SCMDB..po_pomas_pur_order_hdr a, TIPLDB..po_master_table b where a.pomas_pono = b.po_num 
and b.status = 'PO Disapproved At Level 1' and a.pomas_podocstatus in('FR','AM') order by b.po_num ASC");
		   
         return $query;  
     }
	 
	 //Live Purchase Orders Pending For Level 2 Authorization
	  public function select_prqit_prno_lvl2()  
      {
		$query = $this->db->query("select distinct po_num, po_supp_name, level2_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') and status in('PO Send For Level 2 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl2_fpo()  
      {  
		 $query = $this->db->query("select distinct po_num, po_supp_name, level2_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('FPO') and status in('PO Send For Level 2 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl2_ipo()  
      {  
		 $query = $this->db->query("select distinct po_num, po_supp_name, level2_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('IPO') and status in('PO Send For Level 2 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl2_lpo()  
      {  
		 $query = $this->db->query("select distinct po_num, po_supp_name, level2_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('LPO') and status in('PO Send For Level 2 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 
	 //Disapproved Purchase Orders At Level 2
	 public function disapproved_po_lvl2()  
      {  
		 $query = $this->db->query("select po_num, po_supp_name, po_total_value, datediff(DAY,b.po_create_date,getdate()) as diff, b.created_by from SCMDB..po_pomas_pur_order_hdr a, TIPLDB..po_master_table b where a.pomas_pono = b.po_num 
and b.status = 'PO Disapproved At Level 2' and pomas_podocstatus in('FR','AM') order by b.po_num ASC");  

         return $query;  
     }
	 
	 
	 //Live Purchase Orders Pending For Level 3 Authorization
	  public function select_prqit_prno_lvl3()  
      {
		$query = $this->db->query("select distinct po_num, po_supp_name, level3_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') and status in('PO Send For Level 3 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl3_fpo()  
      {  
		 $query = $this->db->query("select distinct po_num, po_supp_name, level3_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('FPO') and status in('PO Send For Level 3 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl3_ipo()  
      {  
		 $query = $this->db->query("select distinct po_num, po_supp_name, level3_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('IPO') and status in('PO Send For Level 3 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 public function select_prqit_prno_lvl3_lpo()  
      {  
		 $query = $this->db->query("select distinct po_num, po_supp_name, level3_mail_to, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and b.pomas_podocstatus  in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('LPO') and status in('PO Send For Level 3 Authorization') ORDER BY  po_num ASC"); 
		  
         return $query;  
     }
	 
	 
	 //Disapproved Purchase Orders At Level 3
	 public function disapproved_po_lvl3()  
      {  
		 $query = $this->db->query("select po_num, po_supp_name, po_total_value, datediff(DAY,b.po_create_date,getdate()) as diff, b.created_by from SCMDB..po_pomas_pur_order_hdr a, TIPLDB..po_master_table b where a.pomas_pono = b.po_num 
and b.status = 'PO Disapproved At Level 3' and pomas_podocstatus in('FR','AM') order by b.po_num ASC");  

         return $query;  
     }
	 
	 
	 //Authorized Purchase Orders Pending For Send Supplier For Purchase
	  public function not_auth_po()  
	  {  
	     $query = $this->db->query("select po_num, po_supp_name, created_by, datediff(DAY,po_create_date,getdate()) as diff 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and a.status = 'Authorized PO Send to Supplier' and b.pomas_podocstatus  in('OP') 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr 
where pomas_pono = a.po_num)
and a.po_num not in('IPO-00460-17','IPO-00462-17','IPO-00463-17','IPO-00464-17')
ORDER BY  a.po_num ASC");  

         return $query;  
	  }
	  
	  //ERP Amended PO's
	  public function amend_po()  
	  { 
		$query = $this->db->query("select pomas_pono, pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount, pomas_createdby, 
		datediff(DAY,pomas_podate,getdate()) as diff, po_supp_name from scmdb..po_pomas_pur_order_hdr a, tipldb..po_master_table b where 
		SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV') and a.pomas_podocstatus in ('AM') and a.pomas_pono = b.po_num
		and a.pomas_poamendmentno > (select MAX(po_amend_no) from TIPLDB..po_master_table where po_num = a.pomas_pono)
		UNION
		select pomas_pono, pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount, pomas_createdby, 
		datediff(DAY,pomas_podate,getdate()) as diff, po_supp_name from scmdb..po_pomas_pur_order_hdr a, tipldb..po_master_table b 
		where SUBSTRING(a.pomas_pono, 1, 3) in ('IPO', 'LPO','FPO','CGP','SRV')
		and a.pomas_podocstatus in ('AM') 
		and a.pomas_pono = b.po_num 
		and b.status in('Draft')
		ORDER BY  a.pomas_pono ASC"); 

         return $query;  
	  }
	 
	  //Authorized Purchase Orders Pending For Send Supplier For Purchase
	  public function send_supplier()  
	  {  
		$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,pomas_createdby 
from TIPLDB..po_master_table a, scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono 
and a.status = 'Authorized PO Send to Supplier' and b.pomas_podocstatus in('FR','AM') 
and SUBSTRING(a.po_num, 1, 3) in('IPO', 'LPO','FPO','CGP','SRV') 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr 
where pomas_pono = a.po_num) ORDER BY  a.po_num ASC"); 
		  
         return $query;  
	  }
	  
	  public function send_supplier_fpo(){
		    
		 $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,pomas_createdby
		 from TIPLDB..po_master_table a, 
		 scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and a.status = 'Authorized PO Send to Supplier' 
		 and b.pomas_podocstatus in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('FPO')
		 and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		 ORDER BY  a.po_num ASC"); 
		  
         return $query;  
	  }
	  
	  public function send_supplier_ipo(){
		    
		 $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,pomas_createdby
		 from TIPLDB..po_master_table a, 
		 scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and a.status = 'Authorized PO Send to Supplier' 
		 and b.pomas_podocstatus in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('IPO')
		 and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		 ORDER BY  a.po_num ASC"); 
		  
         return $query;  
	  }
	  
	  public function send_supplier_lpo(){
		    
		 $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,pomas_createdby 
		 from TIPLDB..po_master_table a, 
		 scmdb..po_pomas_pur_order_hdr b where a.po_num = b.pomas_pono and a.status = 'Authorized PO Send to Supplier' 
		 and b.pomas_podocstatus in('FR','AM') and SUBSTRING(a.po_num, 1, 3) in('LPO')
		 and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		 ORDER BY  a.po_num ASC"); 
		  
         return $query;  
	  }
	  
	  //Authorized Purchase Orders Pending For Manufacturing Clearance From Planning
	  public function mc_planning(){  
	  
		$query = $this->db->query("select *, datediff(DAY,po_create_date,getdate()) as diff from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b
		where a.status in('PO Send To Issuance Of Manufacturing Clearanace By Planning') 
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		order by po_num asc"); 
		  
         return $query;  
	  }
	  
	  //Authorized Purchase Orders Pending For Manufacturing Clearanace From Purchase
	  public function manufact_clearance(){
		    
		$query = $this->db->query("select *, datediff(DAY,po_create_date,getdate()) as diff from tipldb..po_master_table a, 
		scmdb..po_pomas_pur_order_hdr b
		where a.status = 'Manufacturing Clearnace Given By Planning'
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)  
		order by po_num asc");  
									       
		 return $query;  
	  }
	  
	  //Purchase Order Pending For Acknowledgement From Supplier
	  public function ack_supp(){  
	 
		$query = $this->db->query("select *, datediff(DAY,po_create_date,getdate()) as diff from tipldb..po_master_table a, 
		scmdb..po_pomas_pur_order_hdr b
		where a.status in('Manufacturing Clearance Given By Purchase', 'Pending For Acknowledgement From Supplier') 
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		order by po_num asc");
		  	    
		return $query;  
	  }
	  
	  //Live Purchase Orders Pending For Test Certificate Uploadation
	  public function test_cert(){
		    
		$query = $this->db->query("select *, datediff(DAY,po_create_date,getdate()) as diff from tipldb..po_master_table a, 
		scmdb..po_pomas_pur_order_hdr b 
		where a.status in('Acknowledgement Updated')
		and a.po_num = b.pomas_pono and b.pomas_podocstatus = 'OP'
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)
		and a.po_num in(select distinct c.po_num from tipldb..insert_po c, tipldb..pr_submit_table d where c.po_ipr_no = d.pr_num and d.test_cert = 'Yes')");    
		      
		 return $query;  
	  }
	  
	  //Live Purchase Orders Pending For Upload Performa Invoice 
	 public function advance()  
     {  
         $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table
where payterm = 'AD+PI' ORDER BY  po_num ASC");  
           
         return $query;  
     }
	  
	 //Live Purchase Orders Pending For Upload Performa Invoice 
	 public function per_inv()  
     {  
         $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table
		 where status in('Acknowledgement Updated', 'Test Certificate Uploaded') and payterm = 'PI' ORDER BY  po_num ASC"); 
		 
		 /*$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table
		 where payterm = 'PI' ORDER BY  po_num ASC"); */
           
         return $query;  
     }
	 
	 //Live Purchase Orders Pending For PI Authorization If PI Amount Is Greater Than 50000
	 public function per_inv_lvl2()  
     {  
         $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table 
		 where status = 'PI Send For Approval' and payterm = 'PI' ORDER BY  po_num ASC");
		 
		 /*$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table 
		 where payterm = 'PI' ORDER BY  po_num ASC");*/  
           
         return $query;  
     }
	 
	 //Purchase Orders PI Payment To Supplier 
	 public function pi_payment_to_supplier()  
     {  
         $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table
		 where status = 'Send PI Payment To Supplier' and payterm like'%PI%' ORDER BY  po_num ASC"); 
		 
		 /*$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table 
		 where payterm = 'PI' ORDER BY  po_num ASC");  */
           
         return $query;  
     }
	 
	 // Purchase Order Pending For PDC Payment
	 public function post_dated_check()  
     {  
         $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,created_by from TIPLDB..po_master_table 
		 where status = 'Acknowledgement Updated' and payterm like'%PDC%' ORDER BY  po_num ASC"); 
		 
		 /*$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table 
		 where payterm like'%PDC%' ORDER BY  po_num ASC");*/  
           
         return $query;  
     }
	 
	 //Purchase Order Pending For Freight Payment
	 public function freight()  
     {  
         $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,created_by from TIPLDB..po_master_table 
		 where status in('Acknowledgement Updated', 'PI Send For Approval', 'Send PI Payment To Supplier', 'PDC Payment Done To Supplier') 
		 and freight in('EX-WORKS', 'EXW', 'EX WORKS') ORDER BY  po_num ASC");  
           
         return $query;  
     }
	  
	  //Purchase Order Pending For Dispatch Instruction
	  public function dispatch()  
	  {  
		$query = $this->db->query("select *, datediff(DAY,po_create_date,getdate()) as diff,created_by from tipldb..po_master_table a,
		scmdb..po_pomas_pur_order_hdr b 
		where status in('PO Awaited For Road Permit') 
		and a.road_permit_req = 'Yes'
		and a.po_num = b.pomas_pono
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)");
		  
		 return $query;  
	  }
	  
	  
	  //Purchase Order Pending For Delivery Details From Supplier
	  public function delivery() 
	  {  
		 $query = $this->db->query("select *, datediff(DAY,po_create_date,getdate()) as diff,created_by from tipldb..po_master_table a, 
		scmdb..po_pomas_pur_order_hdr b  
		where a.status in('Dispatch Instruction Updated')
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)"); 
		  
		 return $query;  
	  }
	  
	  //Purchase Order Pending For Gate Entry
	  public function grcpt() 
	  {  
		 $query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,created_by 
		from tipldb..po_master_table a, scmdb..po_pomas_pur_order_hdr b 
		where a.status = 'Submit Delivery'
		and a.po_num = b.pomas_pono 
		and b.pomas_podocstatus in('OP')
		and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.po_num)"); 
		  
		 return $query;  
	  }
	  
	  //Purchase Order Pending For Gate Entry
	  public function hsn_check() 
	  {  
		 /*$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff from TIPLDB..po_master_table
		 where status = 'Submit Delivery' ORDER BY  po_num ASC"); */
		 
			/*$query = $this->db->query("select po_num, po_supp_name, datediff(DAY,po_create_date,getdate()) as diff,created_by 
			from TIPLDB..po_master_table where po_num in(select distinct po_num from tipldb..gateentry_item_rec_table 
														 where status = 'Pending For HSN Check By Purchaser') 
			and po_num in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
			ORDER BY  po_num ASC");*/ 
			
			$query = $this->db->query("select a.po_num, a.po_supp_name, datediff(DAY,a.po_create_date,getdate()) as diff, a.created_by, b.entry_no from
			TIPLDB..po_master_table a, TIPLDB..gateentry_item_rec_master b
			where a.po_num = b.po_num
			and b.status in('Pending For HSN Check By Purchaser')
			and a.po_num in(select distinct pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus not in('CL','SC'))
			ORDER BY  a.po_num ASC"); 
			
			return $query;  
	  }
	 
   }  
?>