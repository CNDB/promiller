<?php
class Erpnotauthpom extends CI_Model  
{  
	function __construct()  
	{   
		parent::__construct(); 
	}
	  			
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");
		
		return $query;  
	}
		 
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");	
		
		return $query;    
	}
		 
	public function po_view_lvl2($selectpr)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and a.po_num = c.po_num and b.pomas_poamendmentno = '$amend_no'");
								
		return $query;  
	}
			  
	public function insert_po_sub_lvl2($data)
	{
		$po_num             = $this->input->post("po_num");
		$po_s_no            = $this->input->post("po_s_no");
		$supp_for_pur       = $this->input->post("supp_for_pur");
		$supp_for_pur_by    = $_SESSION['username'];
		$supp_for_pur_date  =  date('Y-m-d H:i:s');
		$status             = "PO Send To Issuance Of Manufacturing Clearanace by planning";
		
		// Trasanction Start
		
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..insert_po set supp_for_pur = '$supp_for_pur', supp_for_pur_by = '$supp_for_pur_by', 
		supp_for_pur_date = '$supp_for_pur_date', status = '$status' where po_num = '$po_num'";
		
		$sql2 = "update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		
		//Transanction Complete
	}
			 
}  
?>