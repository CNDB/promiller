<?php
class Rightm extends CI_Model{
	  
      function __construct()  
      {   
         parent::__construct();  
      }
	  
	  public function emp_list()  
      {  
         $query = $this->db->query("select * from TIPLDB..login where emp_active = 'Yes' order by name asc");  

         return $query;  
      }
	  
	  public function right_submit($data)  
      {    
		 $emp_id           = $this->input->post("emp_id");
		 $emp_name         = $this->input->post("emp_name");
		 $emp_email        = $this->input->post("emp_email");
		 $create_pr        = $this->input->post("create_pr");
		 $attach_drawing   = $this->input->post("attach_drawing");
		 $lvl1_approval    = $this->input->post("lvl1_approval");
		 $lvl1_disapprove  = $this->input->post("lvl1_disapprove");
		 $lvl2_approval    = $this->input->post("lvl2_approval");
		 $lvl2_disapprove  = $this->input->post("lvl2_disapprove");
		 $erp_auth_pending = $this->input->post("erp_auth_pending");
		 $pr_po_nt_created = $this->input->post("pr_po_nt_created");
		 
		 $fresh_po         = $this->input->post("fresh_po");
		 $auth_po_l1       = $this->input->post("auth_po_l1");
		 $dis_po_l1        = $this->input->post("dis_po_l1");
		 $auth_po_l2       = $this->input->post("auth_po_l2");
		 $dis_po_l2        = $this->input->post("dis_po_l2");
		 $erp_auth_pend_po = $this->input->post("erp_auth_pend_po");
		 $erp_amend_po     = $this->input->post("erp_amend_po");
		 $supp_for_purchase= $this->input->post("supp_for_purchase");
		 $mc_planning      = $this->input->post("mc_planning");
		 $mc_purchase      = $this->input->post("mc_purchase");
		 $acknowledgement  = $this->input->post("acknowledgement");
		 
		 $tc_upload        = $this->input->post("tc_upload");
		 $advance          = $this->input->post("advance");
		 $pi_upload        = $this->input->post("pi_upload");
		 $pi_approval      = $this->input->post("pi_approval");
		 $pi_payment       = $this->input->post("pi_payment");
		 $pdc_creation     = $this->input->post("pdc_creation");
		 $freight          = $this->input->post("freight");
		 $road_permit      = $this->input->post("road_permit");
		 $delivery_detail  = $this->input->post("delivery_detail");
		 $gate_entry       = $this->input->post("gate_entry");
		 
		 if($create_pr == ''){
			 $create_pr = 'No';
		 }
		 
		 if($attach_drawing == ''){
			 $attach_drawing = 'No';
		 }
		 
		 if($lvl1_approval == ''){
			 $lvl1_approval = 'No';
		 }
		 
		 if($lvl1_disapprove == ''){
			 $lvl1_disapprove = 'No';
		 }
		 
		 if($lvl2_approval == ''){
			 $lvl2_approval = 'No';
		 }
		 
		 if($lvl2_disapprove == ''){
			 $lvl2_disapprove = 'No';
		 }
		 
		 if($erp_auth_pending == ''){
			 $erp_auth_pending = 'No';
		 }
		 
		 if($pr_po_nt_created == ''){
			 $pr_po_nt_created = 'No';
		 }
		 
		 if($fresh_po == ''){
			 $fresh_po = 'No';
		 }
		 
		 if($auth_po_l1 == ''){
			 $auth_po_l1 = 'No';
		 }
		 
		 if($dis_po_l1 == ''){
			 $dis_po_l1 = 'No';
		 }
		 
		 if($auth_po_l2 == ''){
			 $auth_po_l2 = 'No';
		 }
		 
		 if($dis_po_l2 == ''){
			 $dis_po_l2 = 'No';
		 }
		 
		 if($erp_auth_pend_po == ''){
			 $erp_auth_pend_po = 'No';
		 }
		 
		 if($erp_amend_po == ''){
			 $erp_amend_po = 'No';
		 }
		 
		 if($supp_for_purchase == ''){
			 $supp_for_purchase = 'No';
		 }
		 
		 if($mc_planning == ''){
			 $mc_planning = 'No';
		 }
		 
		 if($mc_purchase == ''){
			 $mc_purchase = 'No';
		 }
		 
		 if($acknowledgement == ''){
			 $acknowledgement = 'No';
		 }
		 
		 if($tc_upload == ''){
			 $tc_upload = 'No';
		 }
		 
		 if($advance == ''){
			 $advance = 'No';
		 }
		 
		 if($pi_upload == ''){
			 $pi_upload = 'No';
		 }
		 
		  if($pi_approval == ''){
			 $pi_approval = 'No';
		 }
		 
		 if($pi_payment == ''){
			 $pi_payment = 'No';
		 }
		 
		 if($pdc_creation == ''){
			 $pdc_creation = 'No';
		 }
		 
		 if($freight == ''){
			 $freight = 'No';
		 }
		 
		  if($road_permit == ''){
			 $road_permit = 'No';
		 }
		 
		 if($delivery_detail == ''){
			 $delivery_detail = 'No';
		 }
		 
		 if($gate_entry == ''){
			 $gate_entry = 'No';
		 }
		 
		 $sql1 = "select  count(*) as abc from TIPLDB..purchase_right_master where emp_code = '$emp_id' and emp_name = '$emp_name'";
		 
		 $query = $this->db->query($sql1)->row();
		 
		 $count = $query->abc;
		 
		if($count > 0){
		
		$sql = "update TIPLDB..purchase_right_master set fresh_pr_creation = '$create_pr', attach_drawing = '$attach_drawing', pr_lvl1_authorize = '$lvl1_approval', disapprove_pr_lvl1 = '$lvl1_disapprove', pr_lvl2_authorize = '$lvl2_approval', disapprove_pr_lvl2 = '$lvl2_disapprove', erp_authorization_pending = '$erp_auth_pending', pr_po_nt_created = '$pr_po_nt_created', fresh_po = '$fresh_po', auth_po_l1 = '$auth_po_l1', dis_po_l1 = '$dis_po_l1', auth_po_l2 = '$auth_po_l2', dis_po_l2 = '$dis_po_l2', erp_auth_pend_po = '$erp_auth_pend_po', erp_amend_po = '$erp_amend_po', supp_for_purchase = '$supp_for_purchase', mc_planning = '$mc_planning', mc_purchase = '$mc_purchase', acknowledgement = '$acknowledgement', tc_upload = '$tc_upload', advance = '$advance', pi_upload = '$pi_upload', pi_approval = '$pi_approval', pi_payment = '$pi_payment', pdc_creation = '$pdc_creation', freight = '$freight', road_permit = '$road_permit', delivery_detail = '$delivery_detail', gate_entry = '$gate_entry'  
where emp_code = '$emp_id' and emp_name = '$emp_name'";
		
		echo $sql;
		
		$this->db->query($sql);
		
		} else {

		$sql = "insert into TIPLDB..purchase_right_master(emp_code, emp_name, emp_email, fresh_pr_creation, attach_drawing, pr_lvl1_authorize, disapprove_pr_lvl1, pr_lvl2_authorize, disapprove_pr_lvl2, erp_authorization_pending, pr_po_nt_created, fresh_po, auth_po_l1, dis_po_l1, auth_po_l2, dis_po_l2, erp_auth_pend_po, erp_amend_po, supp_for_purchase, mc_planning, mc_purchase, acknowledgement, tc_upload, advance, pi_upload, pi_approval, pi_payment, pdc_creation, freight, road_permit, delivery_detail, gate_entry) 
VALUES ('$emp_id','$emp_name','$emp_email','$create_pr','$attach_drawing','$lvl1_approval','$lvl1_disapprove','$lvl2_approval','$lvl2_disapprove','$erp_auth_pending'
,'$pr_po_nt_created','$fresh_po','$auth_po_l1','$dis_po_l1','$auth_po_l2','$dis_po_l2','$erp_auth_pend_po','$erp_amend_po','$supp_for_purchase','$mc_planning','$mc_purchase','$acknowledgement','$tc_upload','$advance','$pi_upload','$pi_approval','$pi_payment','$pdc_creation','$freight','$road_permit'
,'$delivery_detail','$gate_entry')";

		echo $sql;

		$this->db->query($sql);
			
		}
      }	 
}  
?>