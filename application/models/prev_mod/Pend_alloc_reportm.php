<?php
class Pend_alloc_reportm extends CI_Model{
  
	function __construct()  
	{   
	parent::__construct();  
	} 
	
   //pending allocation report
   public function pend_alloc(){ 
    
		 $query = $this->db->query("select pr_num,item_code,itm_desc,pr_date,required_qty,category,workorder_qty as pend_alloc,reorder_level,usage,remarks,atac_no,sono,pr_status
from tipldb..pr_submit_table 
where (convert(decimal(16,2),workorder_qty) = 0 or workorder_qty is null)
and (convert(decimal(16,2),reorder_level) = 0 or reorder_level is null)
and pr_num not in(select po_ipr_no from tipldb..insert_po where po_ipr_no = pr_num)
and pr_num in (select preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'AU')
and category not in('CAPITAL GOODS')
UNION
select pr_num,item_code,itm_desc,pr_date,required_qty,category,workorder_qty as pend_alloc,reorder_level,usage,remarks,atac_no,sono,pr_status
from tipldb..pr_submit_table 
where (convert(decimal(16,2),workorder_qty) = 0 or workorder_qty is null)
and (convert(decimal(16,2),reorder_level) = 0 or reorder_level is null)
and pr_num in(select distinct po_ipr_no from tipldb..insert_po where po_ipr_no = pr_num)
and pr_num not in(select distinct a.po_ipr_no from tipldb..insert_po a, scmdb..gr_hdr_grmain b 
where a.po_num = b.gr_hdr_orderno and a.po_ipr_no = pr_num)
and pr_num in(select preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'AU')
and category not in('CAPITAL GOODS')
order by pr_date desc");  
	
		 return $query;
		   
	}
}  
?>