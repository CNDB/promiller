<?php
class new_pdfm extends CI_Model  
{  
	function __construct()  
	{  
		// Call the Model constructor  
		parent::__construct();  
	}  
	//we will use the select function 
	
	public function view_po_pdf($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr a, scmdb..supp_spmn_suplmain b, scmdb..supp_addr_address c, scmdb..po_paytm_doclevel_detail d where 
a.pomas_suppliercode = b.supp_spmn_supcode and b.supp_spmn_supcode = c.supp_addr_supcode and a.pomas_pono = d.paytm_pono 
and a.pomas_pono = '$po_num_view' and a.pomas_poamendmentno = d.paytm_poamendmentno 
and a.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num_view')");
		
		return $query;  
	} 
	
	public function view_po_pdf_payterm($po_num_view)  
	{  
		$query = $this->db->query("select TOP 1 * from tipldb..insert_po where po_num = '$po_num_view'");  
		
		return $query;  
	}
	
	public function view_po_pdf_item($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b
where a.pomas_pono = b.poitm_pono  
and a.pomas_pono  = '$po_num_view' 
and a.pomas_poamendmentno = b.poitm_poamendmentno
and a.pomas_pono = b.poitm_pono
and a.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono)");  
		
		return $query;  
	}
	
	//CGST TAX
	public function view_po_pdf_cgst($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.podoc_pono='$po_num_view' 
and a.podoc_tcdcode in('CGST0%','CGST1.5%','CGST2.5%','CGST2.5%*','CGST5%','CGST6%','CGST9%','CGST9%*','CGST12%','CGST14%','CGST14%*','CGST18%','CGST28%')
and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	//SGST TAX
	public function view_po_pdf_sgst($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.podoc_pono='$po_num_view' 
and a.podoc_tcdcode in('SGST0%','SGST1.5%','SGST2.5%','SGST2.5%*','SGST5%','SGST6%','SGST9%','SGST9%*','SGST12%','SGST14%','SGST14%*','SGST18%','SGST28%')
and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	//IGST TAX
	public function view_po_pdf_igst($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.podoc_pono='$po_num_view' 
and a.podoc_tcdcode in('IGST0%','IGST3%','IGST3%*','IGST5%','IGST12%','IGST18%','IGST28%','IGST28%*')
and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	
	public function view_po_pdf_cst($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.PODOC_PONO='$po_num_view' 
and a.podoc_tcdcode = 'PURCST2' and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	public function freight_charges($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.PODOC_PONO='$po_num_view' 
and a.podoc_tcdcode = 'FRT' and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	public function handling_charges($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.PODOC_PONO='$po_num_view' 
and a.podoc_tcdcode = 'HANDL CH' and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	public function packing_charges($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..po_potcd_doclevel_detail a, scmdb..tcd_tcdvariantdetail_vw b where a.PODOC_PONO='$po_num_view' 
and a.podoc_tcdcode = 'PKGCGVAR' and a.podoc_tcdversion = b.tcdversionno and a.podoc_tcdvariant = b.tcdvariant 
and a.podoc_poamendmentno = (select max (podoc_poamendmentno) from scmdb..po_potcd_doclevel_detail where podoc_pono = '$po_num_view')");  
		
		return $query;  
	}
	
	public function view_po_pdf_excise($po_num_view)  	
	{  
		$query = $this->db->query("select * from scmdb..tcal_tax_hdr where tran_no='$po_num_view' and tax_type = 'EXCISE'");  
		
		return $query;  
	}
	
	public function view_po_pdf_ser($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..tcal_tax_hdr where tran_no='$po_num_view' and tax_type = 'SER'");  
		
		return $query;  
	}
	
	public function view_po_pdf_vat($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..tcal_tax_hdr where tran_no='$po_num_view' and tax_type = 'VAT'");  
		
		return $query;  
	} 
	
	public function insert_po_pdf_name($po_num_view, $pdf_name)
	{
		$query1 = $this->db->query("update tipldb..insert_po set po_pdf_name = '$pdf_name' where po_num = '$po_num_view'");
		
		$query2 = $this->db->query("update tipldb..po_master_table set po_pdf_name = '$pdf_name' where po_num = '$po_num_view'");  
		
		return $query1;
		
		return $query2;
	}
	
	public function doc_lvl_notes($po_num_view)  
	{  
		$query = $this->db->query("select * from scmdb..Not_Notes_hdr where tran_no = '$po_num_view' and amendment_no = (select max(amendment_no) from scmdb..Not_Notes_hdr where tran_no = '$po_num_view')");  
		
		return $query;  
	}
	
	//TCD Document Level Charges
	public function doc_lvl_charges($po_num_view){
		
		$query = $this->db->query("select * from scmdb..po_doctcd_vw a, scmdb..tcd_tcdvariantdetail_vw b 
where a.docno = '$po_num_view' 
and a.amendno = (select max(amendno) from scmdb..po_doctcd_vw where docno = '$po_num_view')
and a.tcdcode = b.tcdcode and a.tcdvariant = b.tcdvariant and a.tcdversion =  b.tcdversionno and a.tcdcode != 'PDISCOUNTS'");

		return $query;
	}
	
	//TCD Discount Document Level
	public function doc_lvl_discount($po_num_view){
		
		$query = $this->db->query("select * from scmdb..po_doctcd_vw a, scmdb..tcd_tcdvariantdetail_vw b 
where a.docno = '$po_num_view' 
and a.amendno = (select max(amendno) from scmdb..po_doctcd_vw where docno = '$po_num_view')
and a.tcdcode = b.tcdcode and a.tcdvariant = b.tcdvariant and a.tcdversion =  b.tcdversionno and a.tcdcode = 'PDISCOUNTS'");
		
		return $query;	
	}
	
}  
?>