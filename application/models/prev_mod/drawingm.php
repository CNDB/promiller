<?php
class drawingm extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function select_prqit_prno()  
      {  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail where SUBSTRING(prqit_prno, 1, 3) in ('FPR','IPR','LPR') 
		 and prqit_prlinestatus ='FR'");  

         return $query;  
     }
	 
	 public function view_prqit_prno($selectpr)  
      {  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, 
		 scmdb..supp_spmn_suplmain c, scmdb..itm_iou_itemvarhdr d
		 where a.prqit_prno = b.preqm_prno  and a.prqit_prno = '$selectpr' and  a.prqit_supplier_code = c.supp_spmn_supcode
		 and a.prqit_itemcode = d.iou_itemcode");
		   
         return $query;  
      }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	  public function pendal_info($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	  public function pendal_info_monthwise($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }    
	  
	  public function pendal_info_last5_trans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function pendal_info_reorder_lvl_qty($itemcode)
	  {
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function lstyrcumnrec1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function lastfivetrans1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function itemdescmaster($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	 
	 /******* Pendal Card Models *********/
	 
	  public function pendal_master($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_whstkblnc($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_pndposopr($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_allocat($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  
	   public function pendal_master_allocat_tot($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_wipdet($itemcode)
	  {
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_lastfivetrans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_lstyrcumnrec($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_supplier($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_mnthwsecurryrcons($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_bomdetail($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	 /******** Months Of Inventory if item is in reorder level *********/
	   
	 public function monthofinvtry($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
	   }
	   
	  public function orderinadvnce($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
	   }
	  
	  public function wrkordr_ser($itemcode)
	  {
	  	$query = $this->db->query("select * from scmdb..pmd_twos_wops_postn where wos_item_no = '$itemcode' and wos_ord_stat_code in('G','E','F','S')");
		
		 return $query;
	  }
	  
	  public function wrkordr_ser_res($workorder)
	  {
	  	$query = $this->db->query("select * from tipldb..pendalcard_rkg a, scmdb..pmd_twos_wops_postn b, scmdb..pmd_twoh_wo_headr c, 
        scmdb..cust_lo_info d,  tipldb..master_atac e where a.flag='ItemMaster' and a.itemcode = b.wos_item_no and b.wos_wo_no = c.woh_wo_no 
        and c.woh_cust_name = d.clo_cust_code and c.woh_so_no = e.so_no and  b.wos_wo_no = '$workorder'");
		
		 return $query;
	  }
	   
	  public function insert_pr_sub($data, $uploaded_drawing)
	  {
		$username = $_SESSION['username'];
		$pr_num = $this->input->post("pr_num");
		$username = $_SESSION['username'];
		$pr_status = "Pending For PR Authorize";
		
		//Transanction Start
		$this->db->trans_start();
		
		echo "update TIPLDB..pr_submit_table set attach_drawing = '$uploaded_drawing', pr_status = '$pr_status', created_by = '$username' 
		where pr_num = '$pr_num'";
		
		$this->db->query("update TIPLDB..pr_submit_table set attach_drawing = '$uploaded_drawing', pr_status = '$pr_status', created_by = '$username' 
		where pr_num = '$pr_num'");	
		 
		$this->db->trans_complete();
		//Transanction Complete
	  }
	 
   }  
?>