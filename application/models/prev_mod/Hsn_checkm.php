<?php
class Hsn_checkm extends CI_Model{
	  
	function __construct(){   
		parent::__construct();  
	}
	
	//Max Amend Number PO
	public function max_amend_no($po_num){
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		return $query; 
	}
	
	//PO Details
	public function po_view($po_num, $amend_no){ 
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, 
		scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$po_num' 
		and a.po_num = b.pomas_pono and a.po_num = c.po_num 
		and b.pomas_poamendmentno = ( 
		select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num' 
		) 
		order by a.po_line_no");  
		
		return $query;  
	}
	
	//Update Data
	public function grcpt_det_update($data){ 
		$entry_no = $this->input->post("entry_no");
		$po_num = $this->input->post("po_num");
		$remarks = $this->input->post("remarks");
		$proceed_to_grcpt = $this->input->post("proceed_to_grcpt");
		$created_by = $_SESSION['username'];
		$create_date = date('Y-m-d H:i:s');
		
		//Received Quanity Calculation
		$po_ipr_no = $this->input->post("po_ipr_no");
		$array_count = count($po_ipr_no);
		$curr_rec_qty = $this->input->post("curr_rec_qty");
		$prev_rec_qty = $this->input->post("prev_rec_qty");
		$excess_indent_rmks = $this->input->post("excess_indent_rmks");
		
		$sql_user = "select * from tipldb..login where email like'%$created_by%' and emp_active = 'Yes'"; 
		$qry_user = $this->db->query($sql_user)->row();
		
		$user_id = $qry_user->id;
		
		$sql_proc = "exec tipldb..po_l3_approval";
		$qry_proc  = $this->db->query($sql_proc);
		
		$this->db->close();
		$this->db->initialize();
		
		if($proceed_to_grcpt == 'Proceed To GRCPT'){
			
			$sql_mc = "select count(*) as count_mc from tipldb..po_master_table where mc_req = 'Yes' 
			and (mc_given = 'No' or mc_given is NULL) and po_num = '$po_num'
			and po_category not in('TOOLS','PRODUCTION CONSUMABLES','NON PRODUCTION CONSUMABLES','IT','SENSORS')";
			
			$sql_di = "select count(*) as count_di from tipldb..po_master_table where di_req = 'Yes' 
			and (di_given = 'No' or di_given is NULL) and po_num = '$po_num'
			and po_category not in('TOOLS','PRODUCTION CONSUMABLES','NON PRODUCTION CONSUMABLES','IT','SENSORS')";
			
			//Previous Without MC & DI approval
			$sql_prev_app = "select count(*) as count_prev from tipldb..po_master_table where po_num = '$po_num' and wo_mcdi_app = 'Yes'";
			
			$qry_mc = $this->db->query($sql_mc)->row();
			$qry_di = $this->db->query($sql_di)->row();
			$qry_prev_app = $this->db->query($sql_prev_app)->row();
			
			$count_mc = $qry_mc->count_mc;
			$count_di = $qry_di->count_di;
			$count_prev = $qry_di->count_prev;
			
			//MC Condition Check
			if($count_mc > 0 && $count_prev > 0){	
				$status = 'Pending For MC & DI Approval From RA';	
			} else if($count_di > 0 && $count_prev > 0){	
				$status = 'Pending For MC & DI Approval From RA';	
			} else {	
				$status = 'Pending For GRCPT';
			}
			
		} else {
			$status = 'Received';
		}
		
		//Transanction Start
		$this->db->trans_start();
		
		if($proceed_to_grcpt == 'Proceed To GRCPT'){
			//Proceed To GRCPT
			$sql1="update tipldb..po_master_table set status = '$status' where po_num = '$po_num'";
			
			$this->db->query($sql1);
			
			$sql2="update tipldb..gateentry_item_rec_table set status = '$status', remarks_by_purchaser = '$remarks', 
			purchaser_fwd_by = '$created_by', purchaser_fwd_by_id = '$user_id', purchaser_fwd_date = '$create_date'
			where po_num = '$po_num' and entry_no = '$entry_no'";
			
			$this->db->query($sql2);
			
			//Counting Number Of Rows
			$sql3="select count(*) as cnt1 from tipldb..gateentry_item_rec_table 
			where status in('Draft','Pending For HSN Check By Purchaser') 
			and entry_no = '$entry_no'";
			
			$query3 = $this->db->query($sql3)->row();
			
			$count = $query3->cnt1;
			//Counting Number Of Rows
			
			if($count <= 0){
				$sql4="update tipldb..gateentry set status = '$status' where entry_no = '$entry_no'";
				$this->db->query($sql4);
			}
			
			$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
			values ('$entry_no','Purchase','Proceed To GRCPT','$remarks','$user_id','$created_by','$po_num')";
			
			$this->db->query($sql5);
			
			for($i=0;$i<$array_count;$i++){
				$sql6="update tipldb..insert_po set curr_rec_qty = '".$curr_rec_qty[$i]."', prev_rec_qty = '".$prev_rec_qty[$i]."', 
				excess_indent_rmks = '".$excess_indent_rmks[$i]."' where po_ipr_no = '".$po_ipr_no[$i]."'";
				
				$this->db->query($sql6);
			}
		
		} else {
			//Revert To Store
			$sql1="update tipldb..po_master_table set status = '$status' where po_num = '$po_num'";
			$this->db->query($sql1);
			
			$sql2="update tipldb..gateentry_item_rec_table set status = 'Draft', remarks_by_purchaser = '$remarks'
			where entry_no = '$entry_no'";
			$this->db->query($sql2);
			
			
			$sql4="update tipldb..gateentry set status = '$status' where entry_no = '$entry_no'";
			$this->db->query($sql4);
			
			$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
			values ('$entry_no','Purchase','Revert To Store','$remarks','$user_id','$created_by','$po_num')";
			
			$this->db->query($sql5);
		}
		
		$this->db->trans_complete();
		//Transanction Complete
	
	}
	
	public function gate_entry_det($entry_no, $po_num){ 
		$query = $this->db->query("select * from tipldb..gateentry_item_rec_master where entry_no = '$entry_no' and po_num = '$po_num'");
		return $query;  
	}
	
	//Attachment Modal
	public function insert_attachment($po_num,$entry_no,$docmt_type,$po_ge_attachment){
		$date_time = date('Y-m-d H:i:s');
		$username  = $_SESSION['username'];
		
		$sql ="insert into tipldb..ge_po_attachment (po_num, entry_no, attachment, date_time, attached_by, doc_type) 
		values('".$po_num."','".$entry_no."','".$po_ge_attachment."','".$date_time."','".$username."','".$docmt_type."')";
		
		$query = $this->db->query($sql);
	}
	
	//Manufacturing Condition Check
	public function mc_cond($po_num){
		$query = $this->db->query("select count(*) as count_mc from tipldb..po_master_table where mc_req = 'Yes' 
		and (mc_given = 'No' or mc_given is NULL) and po_num = '$po_num'"); 
		
		return $query;
	}
	
	//Dispatch Instruction Check
	public function di_cond($po_num){
		$query = $this->db->query("select count(*) as count_di from tipldb..po_master_table where di_req = 'Yes' 
		and (di_given = 'No' or di_given is NULL) and po_num = '$po_num'");
		
		return $query;
	}
	
	//Previous MCDI Approval Check
	public function prev_app_cond($po_num){
		$query = $this->db->query("select count(*) as count_prev from tipldb..po_master_table where po_num = '$po_num' and wo_mcdi_app = 'Yes'");
		
		return $query;
	}
	
	public function fetch_lvl3_app_req(){ 
         $query = $this->db->query("exec tipldb..po_l3_approval");  
         return $query;  
     }
	  
}  
?>