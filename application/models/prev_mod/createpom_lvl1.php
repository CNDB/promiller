<?php
class createpom_lvl1 extends CI_Model  
{  
	function __construct()  
	{   
		parent::__construct();  
	}  
	   
	public function select_prqit_prno()  
	{  
		$query = $this->db->query("select * from scmdb..prq_prqit_item_detail where SUBSTRING(prqit_prno, 1, 3) in ('IPR','LPR','FPR') 
		and prqit_prlinestatus ='FR'");  
		
		return $query;  
	}
	
	public function view_prqit_prno($selectpr)  
	{  
		$query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, 
		scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = '$selectpr' 
		and a.prqit_itemcode = d.iou_itemcode");   
		return $query;  
	}
	
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function pendal_info($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		
		return $query;
	} 
	
	public function pendal_info_monthwise($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		
		return $query;
	}    
	
	public function pendal_info_last5_trans($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		
		return $query;
	} 
	
	public function pendal_info_reorder_lvl_qty($itemcode)
	{
		$query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		
		return $query;
	} 
	
	public function lstyrcumnrec1($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		
		return $query;
	}
	
	public function lastfivetrans1($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		
		return $query;
	}
	
	public function itemdescmaster($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		
		return $query;
	}
	 
	/******* Pendal Card Models *******/
	 
	public function pendal_master($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		
		return $query;
	}

	public function pendal_master_whstkblnc($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		
		return $query;
	}
	
	public function pendal_master_pndposopr($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		
		return $query;
	}
	
	public function pendal_master_allocat($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		
		return $query;
	}
	
	
	public function pendal_master_allocat_tot($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		
		return $query;
	}
	
	public function pendal_master_wipdet($itemcode)
	{
		$query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		
		return $query;
	
	}
	
	public function pendal_master_lastfivetrans($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		
		return $query;
	
	}
	
	public function pendal_master_lstyrcumnrec($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		
		return $query;
	
	}
	
	public function pendal_master_supplier($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		
		return $query;
	
	}
	
	public function pendal_master_mnthwsecurryrcons($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		
		return $query;
	
	}
	
	public function pendal_master_mnthwsecurryrrcpt($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		
		return $query;
	
	}
	
	public function pendal_master_bomdetail($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		
		return $query;
	
	}
	  
	/******** Months Of Inventory if item is in reorder level *********/
	  
	public function monthofinvtry($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		
		return $query;
	}
		   
	public function orderinadvnce($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		
		return $query;
	}
		  
	public function wrkordr_ser($itemcode)
	{
		$query = $this->db->query("select * from scmdb..pmd_twos_wops_postn a, scmdb..pmd_twoh_wo_headr b where a.wos_item_no = '$itemcode' 
		and a.wos_wo_no = b.woh_wo_no and a.wos_ord_stat_code in('G','E','F','S')");
		
		return $query;
	}
	  
	public function wrkordr_ser_res($workorder)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg a, scmdb..pmd_twos_wops_postn b, scmdb..pmd_twoh_wo_headr c, 
		scmdb..cust_lo_info d,  tipldb..master_atac e where a.flag='ItemMaster' and a.itemcode = b.wos_item_no and b.wos_wo_no = c.woh_wo_no 
		and c.woh_cust_name = d.clo_cust_code and c.woh_so_no = e.so_no and  c.woh_so_no = '$workorder'");
		
		return $query;
	}
	   
	public function insert_pr_sub($data, $cost_sheet_name)
	{
		$username = $_SESSION['username'];
		$pr_num = $this->input->post("pr_num");
		$pr_date = $this->input->post("pr_date");
		$item_code = $this->input->post("item_code");
		$required_qty = $this->input->post("required_qty");
		$authorise_qty = $this->input->post("authorise_qty");
		$trans_uom = $this->input->post("trans_uom");
		$unit_cost = $this->input->post("unit_cost");
		$need_date = $this->input->post("need_date");
		$supplier_code = $this->input->post("supplier_code");
		$supplier_name = $this->input->post("supplier_name");
		$supplier_class = $this->input->post("supplier_class");
		$warehse_code = $this->input->post("warehse_code");
		$total_val = $this->input->post("total_val");
		$itm_desc = $this->input->post("itm_desc");
		$work_odr_no = $this->input->post("work_odr_no");
		$sono = $this->input->post("sono");
		$customer_code = $this->input->post("customer_code");
		$customer_name = $this->input->post("customer_name");
		$drawing_no = $this->input->post("drawing_no");
		$pm_group    =$this->input->post("pm_group");
		$attach_drawing = $this->input->post("attach_drawing");
		$remarks = $this->input->post("remarks");
		$test_cert = $this->input->post("test_cert");
		$username = $_SESSION['username'];
		$usage = $this->input->post("usage");
		$project_target_date = $this->input->post("project_target_date");
		$costing = $this->input->post("costing");
		$category = $this->input->post("category");
		$project_name = $this->input->post("pro_ordr_name");
		
		if ($drawing_no == '') {
		$pr_status = "Pending For PR Authorize";
		} else {
		$pr_status = "Pending to upload drawing";
		}
	
		 $sql = "insert into tipldb..pr_submit_table (pr_num, pr_date, item_code, required_qty, authorise_qty, trans_uom, unit_cost, need_date, 
				 warehse_code, total_val, itm_desc, work_odr_no, sono, customer_code, customer_name, drawing_no, attach_drawing, remarks, 
				 test_cert, created_by, pr_status, pm_group, usage, project_target_date, costing, attch_cost_sheet, category, project_name) 
				 VALUES ('".$pr_num."','".$pr_date."','".$item_code."','".$required_qty."','".$authorise_qty."','".$trans_uom."','".$unit_cost."',
				 '".$need_date."','".$warehse_code."','".$total_val."','".$itm_desc."','".$work_odr_no."','".$sono."','".$customer_code."',
				 '".$customer_name."','".$drawing_no."','".$attach_drawing."','".$remarks."','".$test_cert."','".$username."','".$pr_status."',
				 '".$pm_group."','".$usage."','".$project_target_date."','".$costing."','".$cost_sheet_name."','".$category."','".$project_name."')";
	
		$this->db->query($sql);
	}
	  
	public function cost_sheet()
	{
		$query = $this->db->query("select * from TIPLDB..pr_submit_table where sono != '' and attch_cost_sheet != ''");
		
		return $query;
	}
	
	//Action Timing Report Queries
	  public function prerplive($selectpr)
	  {
		$query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr a, tipldb..pr_submit_table b where a.preqm_prno = b.pr_num 
and a.preqm_prno = '$selectpr'");
		
		return $query;  
	  }
	  // Action Timing Report Queries	 
}  
?>