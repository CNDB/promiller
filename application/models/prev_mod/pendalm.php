<?php
class Pendalm extends CI_Model{
	  
	function __construct(){
		   
		parent::__construct();  
	}
	
	public function user_det($username){
		
		$email = $username."@tipl.com";
		
		$query = $this->db->query("select * from tipldb..login where email = '$email'");
		
		return $query;    
	}
	
	public function procesdure_run($itemcode){
		
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function reorder_proc($itemcode, $id){
		
		$query = $this->db->query("exec tipldb..reorder_report_proc '$itemcode','$id'");
		
		return $query;    
	}
	
	public function pendal_master($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_whstkblnc($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_pndposopr($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_pndposopr_new($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemPendingTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_allocat($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_allocat_tot($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_wipdet($itemcode){
		
		$query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_lastfivetrans($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_lstyrcumnrec($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_supplier($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_mnthwsecurryrcons($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_mnthwsecurryrrcpt($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_bomdetail($itemcode){
		
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pics_entry($itemcode){
		
		$query = $this->db->query("select * from TIPLDB..pics_entry where item_code = '$itemcode' order by sno asc");
		
		return $query; 
	}
	
	public function cut_peice_det($itemcode){
		
		$query = $this->db->query("select *,convert(date,create_dt) as createdt, convert(date,modified_dt) as modified
        from TIPLDB..stock_cut_piece where item_code = '$itemcode' order by ID asc");
		
		return $query; 
	}
	
	public function item_creation_hist($itemcode){
		
		$query = $this->db->query("select * from tipldb..itemCreation_conversion where item_code='".$itemcode."' order by id");
		
		return $query; 
	}
	
	public function item_drawing($itemcode){
		
		$query = $this->db->query("SELECT * FROM TIPLDB..item_internalDrawing WHERE item_code='".$itemcode."'");
		
		return $query; 
	}
	
	public function item_drawing_det($itemcode){
		
		$query = $this->db->query("select * from TIPLDB..item_internalDrawing where item_code='".$itemcode."' and status='active'");
		
		return $query; 
	}
	
	public function cons_quantity($itemcode, $id){
		
		$query = $this->db->query("SELECT * FROM TIPLDB..itemLastYear_consumption_cns WHERE item_code = '$itemcode' and user_id = '$id'");
		
		return $query; 
	}
	
	public function lead_time_qry($itemcode, $id){
		
		$query = $this->db->query("SELECT * FROM TIPLDB..last_five_transactions_pr_po_grv_cns WHERE user_id='$id' AND item_code = '$itemcode'");
		
		return $query; 
	}
	
}  
?>