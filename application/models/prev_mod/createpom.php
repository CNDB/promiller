<?php
class createpom extends CI_Model{
	  
      function __construct(){
		     
         parent::__construct();
		   
      }  
	   
      public function select_prqit_prno(){
		    
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail where SUBSTRING(prqit_prno, 1, 3) in ('IPR','LPR','FPR') 
		 and prqit_prlinestatus ='FR'");  

         return $query;  
      }
	 
	  public function view_prqit_prno($selectpr){  
	  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = '$selectpr' 
		 and a.prqit_itemcode = d.iou_itemcode");
		    
         return $query;  
      }
	  
	  public function procesdure_run($itemcode){
		  
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	  public function procesdure_run_supplier($user_id, $selectpr, $itemcode){
		  
	 	 $query = $this->db->query("exec tipldb..supplier_details  '1' , '$itemcode'  , '$selectpr'");
		 	
		 return $query;    
	  }
	  
	  public function pendal_info($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		 
		 return $query; 
	  }
	  
	  public function pendal_info_monthwise($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }    
	  
	  public function pendal_info_last5_trans($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  } 
	  
	  public function pendal_info_reorder_lvl_qty($itemcode){
		  
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		 	
		 return $query; 
	  } 
	  
	  public function lstyrcumnrec1($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");	
		 
		 return $query;	 
	  }
	  
	  public function lastfivetrans1($itemcode){  
	  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function itemdescmaster($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");	
		 
		 return $query; 
	  }
	 
	  /******* Pendal Card Models *******/
	 
	  public function pendal_master($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_whstkblnc($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_pndposopr($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_pndposopr_new($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemPendingTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_allocat($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_allocat_tot($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_wipdet($itemcode){
		  
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_lastfivetrans($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_lstyrcumnrec($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_supplier($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_mnthwsecurryrcons($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");	
		 
		 return $query; 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_bomdetail($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");	
		 
		 return $query; 
	  }
	  
	  /******** Months Of Inventory if item is in reorder level *********/
	  public function monthofinvtry($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");	 
		 	
		 return $query;
	  }
	   
	  public function orderinadvnce($itemcode){
		  
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
	   }
	  
	  public function wrkordr_ser($itemcode){
		  
	  	$query = $this->db->query("select * from scmdb..pmd_twos_wops_postn a, scmdb..pmd_twoh_wo_headr b where a.wos_item_no = '$itemcode' 
		and a.wos_wo_no = b.woh_wo_no and a.wos_ord_stat_code in('G','E','F','S') and b.woh_so_no in (select so_no from tipldb..master_atac)");
		
		return $query;
	  }
	  
	  public function wrkordr_ser_res($workorder){
		  
	  	$query = $this->db->query("select * from tipldb..pendalcard_rkg a, scmdb..pmd_twos_wops_postn b, scmdb..pmd_twoh_wo_headr c, 
		scmdb..cust_lo_info d,  tipldb..master_atac e, tipldb..main_master_atac f where a.flag='ItemMaster' and a.itemcode = b.wos_item_no 
		and b.wos_wo_no = c.woh_wo_no and c.woh_cust_name = d.clo_cust_code and c.woh_so_no = e.so_no and e.atac_no = f.atac_no 
		and c.woh_so_no = '$workorder'");
		
		return $query;
	  }
	  
	  public function wrkordr_ser_res_new($atac_no){
		  								
	  	 $query = $this->db->query("select * from TIPLDB..master_atac a, TIPLDB..main_master_atac b where a.atac_no = b.atac_no and a.atac_no = '$atac_no'");
		
		 return $query;
	  }
	  
	  //Creator Email Address Starts
	  public function creator_email($created_by){
		  								
	  	 $query = $this->db->query("select * from TIPLDB..employee where emp_email like '%$created_by%'");
		 
		 return $query;
	  }
	  
	  //Creator Email Address Ends
	  public function insert_pr_sub($data, $cost_sheet_name, $NewFileName6, $level1_approval_mailto, $level2_approval_mailto, $level1_approval_req, $level2_approval_req, $error){
	
		$username = $_SESSION['username'];
		$attch_cost_sheet = strtolower($_FILES["cost_sheet"]["name"]);
		$pr_num = $this->input->post("pr_num");
		$pr_date = $this->input->post("pr_date");
		$item_code = $this->input->post("item_code");
		$required_qty = $this->input->post("required_qty");
		$trans_uom = $this->input->post("trans_uom");
		$unit_cost = $this->input->post("unit_cost");
		$need_date = $this->input->post("need_date");
		$supplier_code = $this->input->post("supplier_code");
		$supplier_name = $this->input->post("supplier_name");
		$supplier_class = $this->input->post("supplier_class");
		$warehse_code = $this->input->post("warehse_code");
		$total_val = $this->input->post("total_val");
		$itm_desc1 = $this->input->post("itm_desc");
		$itm_desc = str_replace("'","",$itm_desc1);
		$sono = $this->input->post("sono");
		$customer_code = $this->input->post("customer_code");
		$customer_name = $this->input->post("customer_name");
		$drawing_no = $this->input->post("drawing_no");
		$pm_group    =$this->input->post("pm_group");
		$attach_drawing = $this->input->post("attach_drawing");
		$remarks1 = $this->input->post("remarks");
		$remarks = str_replace("'","",$remarks1);
		$test_cert = $this->input->post("test_cert");
		$username = $_SESSION['username'];
		$usage = $this->input->post("usage");
		$project_target_date = $this->input->post("project_target_date");
		$costing = $this->input->post("costing");
		$costing = number_format($costing,2,".","");
		$category = $this->input->post("category");
		$pr_first_three = substr($pr_num,0,3);
		
		if($costing == '' || $costing == NULL){
			$costing = 0;
		}
		
		//CAPITAL GOODS CONDITION
		if($pr_first_three == 'CGP'){
			$category = "CAPITAL GOODS";
		}
		
		$project_name = $this->input->post("pro_ordr_name");
		$pm_group = $this->input->post("pm_group");
		$atac_no = $this->input->post("atac_no");
		$atac_ld_date = $this->input->post("atac_ld_date");
		$atac_need_date = $this->input->post("atac_need_date");
		$atac_payment_terms = $this->input->post("atac_payment_terms");
		$customer_payment_terms = $this->input->post("customer_payment_terms");
		$cost_available = $this->input->post("cost_available");
		$cost_calculation_remarks1 = $this->input->post("cost_calculation_remarks");
		$cost_calculation_remarks = str_replace("'","",$cost_calculation_remarks1);
		$cost_not_available_remarks1 = $this->input->post("cost_not_available_remarks");
		$cost_not_available_remarks = str_replace("'","",$cost_not_available_remarks1);
		$manufact_clearance = $this->input->post("manufact_clrnce");
		$dispatch_inst = $this->input->post("dispatch_inst");
		$pr_supp_remarks1 = $this->input->post("pr_supp_remarks");
		$pr_supp_remarks = str_replace("'","",$pr_supp_remarks1);
		$color = $this->input->post("color");
		$workorder = $this->input->post('workorder');
		$level = "LEVEL0";
		$datetime = date('Y-m-d H:i:s');
		$instruction = NULL;
		$currency = $this->input->post("currency");
		$why_spcl_rmks1 = $this->input->post('why_spcl_rmks');
		$why_spcl_rmks = str_replace("'","",$why_spcl_rmks1);
		$complaint_no = $this->input->post('complain_no');
		$complaint_no = str_replace("'","",$complaint_no);
		$workorder_qty = $this->input->post('wo_qty');
		//TC Remarks
		$tc_rmks = $this->input->post('tc_rmks');
		$tc_rmks = str_replace("'","",$tc_rmks);
		
		//Reason For Excess Indent
		$excess_indt_rmks = $this->input->post('excess_indt_rmks');
		$excess_indt_rmks = str_replace("'","",$excess_indt_rmks);
		
		//Freight Amount & Duty Columns
		$duty_amt = $this->input->post('duty_amt');
		$freight_amt = $this->input->post('freight_amt');
		
		//Tranport Mode & Item Lead Time
		$trans_mode = $this->input->post('trans_mode');
		$item_lead_time = $this->input->post('item_lead_time');
		
		if($trans_mode == '' || $trans_mode == NULL){
			$trans_mode = 'Road';
		}
		
		if($item_lead_time == '' || $item_lead_time == NULL){
			$item_lead_time = 0;
		}
		
		if($workorder_qty == '' || $workorder_qty == NULL){
			$workorder_qty = 0;
		}
		
		$reorder_level = $this->input->post('reorder_level');
		
		if($reorder_level == '' || $reorder_level == NULL){
			$reorder_level = 0;
		}
		
		$reorder_qty = $this->input->post('reorder_qty');
		
		if($reorder_qty == '' || $reorder_qty == NULL){
			$reorder_qty = 0;
		}
		
		$accepted_stock = $this->input->post('accepted_stock');
		
		if($accepted_stock == '' || $accepted_stock == NULL){
			$accepted_stock = 0;
		}
		
		$po_pr_quantity = $this->input->post('po_pr_quantity');
		
		if($po_pr_quantity == '' || $po_pr_quantity == NULL){
			$po_pr_quantity = 0;
		}
		
		$grcpt_recipt_qty = $this->input->post('grcpt_recipt_qty');
		
		if($grcpt_recipt_qty == '' || $grcpt_recipt_qty == NULL){
			$grcpt_recipt_qty = 0;
		}
		
		$grcpt_accepted_qty = $this->input->post('grcpt_accepted_qty');
		
		if($grcpt_accepted_qty == '' || $grcpt_accepted_qty == NULL){
			$grcpt_accepted_qty = 0;
		}
		
		/*if ($drawing_no == '') {
			$pr_status = "Pending For PR Authorize";
		} else {
			$pr_status = "Pending to upload drawing";
		}*/
		
		if($category == ''){
		  
			$sql_project_category = "select distinct product_type from tipldb..ind_wise_pm_so where business_vertical = '".$pm_group."' 
			and prod_visible = 'Yes'";
			
			$qry_project_category = $this->db->query($sql_project_category);
			
			foreach($qry_project_category->result() as $row){	
				$cat_live = $row->product_type;	
			}
			
			if($cat_live == 'Process_Instrumentation'){
				$category = "INSTRUMENTATION";
			} else if($cat_live == 'Security'){
				$category = "SECURITY";
			} else if($cat_live == 'Temperature_Sensors'){
				$category = "SENSORS";
			} else if($cat_live == 'Avazonic'){
				$category = "AVAZONIC";
			} else if($cat_live == 'Service'){
				$category = "SERVICE";
			}  
		   
		}
		
		//Status  
		if($level1_approval_req == 'Yes'){
			$pr_status = "Pending For PR Authorize";
		} else {
			$pr_status = "Pending For PR Authorize In ERP";
		}
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql_new = "select count(*) as abc from tipldb..pr_submit_table where pr_num = '$pr_num'";
		
		$query_new = $this->db->query($sql_new)->row();
		
		$count = $query_new->abc;
		
		if ($count > 0) {
		
			//deleting the previous records	
			$sql_dis_query1 = "delete from tipldb..pr_submit_table where pr_num = '$pr_num'";
			
			$this->db->query($sql_dis_query1);
		
		}

	    $sql = "insert into tipldb..pr_submit_table(pr_num, pr_date, item_code, required_qty, trans_uom, need_date, warehse_code,
		itm_desc, work_odr_no, sono, customer_code, customer_name, drawing_no, attach_drawing, remarks, test_cert, created_by, pr_status, pm_group, 
		usage, project_target_date, costing, attch_cost_sheet, category, project_name, atac_no, atac_ld_date, atac_need_date, 
		atac_payment_terms,  customer_payment_terms, cost_available, cost_calculation_remarks, cost_not_available_remarks, manufact_clearance, dispatch_inst,
		pr_supp_remarks, level1_approval_mailto, color, level2_approval_mailto, level1_approval_req, level2_approval_req, workorder_qty, error, reorder_level,
		reorder_qty, accepted_stk, pending_popr_qty, grcpt_accepted_qty, grcpt_receipt_qty, currency, other_attachment, why_spcl_rmks, tc_rmks, 
		excess_indt_rmks,freight_amt,duty_amt,trans_mode,item_lead_time,complaint_no) 
		VALUES ('".$pr_num."','".$pr_date."','".$item_code."','".$required_qty."','".$trans_uom."','".$need_date."','".$warehse_code."',
		'".$itm_desc."','".$workorder."','".$sono."','".$customer_code."','".$customer_name."','".$drawing_no."','".$attach_drawing."','".$remarks."',
		'".$test_cert."','".$username."','".$pr_status."','".$pm_group."','".$usage."','".$project_target_date."','".$costing."',
		'".$cost_sheet_name."','".$category."','".$project_name."','".$atac_no."','".$atac_ld_date."','".$atac_need_date."','".$atac_payment_terms."',
		'".$customer_payment_terms."','".$cost_available."','".$cost_calculation_remarks."','".$cost_not_available_remarks."','".$manufact_clearance."'
		,'".$dispatch_inst."','".$pr_supp_remarks."','".$level1_approval_mailto."','".$color."','".$level2_approval_mailto."','".$level1_approval_req."'
		,'".$level2_approval_req."','".$workorder_qty."','".$error."','".$reorder_level."','".$reorder_qty."','".$accepted_stock."','".$po_pr_quantity."'
		,'".$grcpt_accepted_qty."','".$grcpt_recipt_qty."','".$currency."','".$NewFileName6."','".$why_spcl_rmks."','".$tc_rmks."','".$excess_indt_rmks."',
		'".$freight_amt."','".$duty_amt."','".$trans_mode."','".$item_lead_time."','".$complaint_no."')";
		
		$this->db->query($sql);
		
		$sql2 = "insert into tipldb..pr_submit_table_comment(pr_num, instruction, level, comment_by, datetime, comment, enabled) 
		VALUES ('".$pr_num."','".$instruction."','".$level."','".$username."','".$datetime."','".$remarks."','YES')";
		
		$this->db->query($sql2);
		
		$sql4 = "update tipldb..pr_submit_table set costing_new = '".$costing."' where pr_num = '$pr_num'";
		
		$this->db->query($sql4);
		
		$sql5 = "EXEC tipldb..project_process_proc";
		$this->db->query($sql5);
		$this->db->close();
		$this->db->initialize();
		
		$this->db->trans_complete();
		//Transanction Complete
	  }
	  
	  // Purchase Request Authorizing Step Planning
	  public function view_prqit_prno_auth($selectpr){ 
	   
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..pr_submit_table c, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = c.pr_num  and a.prqit_prno = '$selectpr' 
		 and a.prqit_itemcode = d.iou_itemcode"); 
		   
         return $query;  
      }
	  
	  public function insert_pr_sub_auth($data){
		  
		$username = $_SESSION['username'];
		$pr_num = $this->input->post("pr_num");
		$pr_date = $this->input->post("pr_date");
		$pr_approval_remarks1 = $this->input->post("remarks_auth");
		$pr_approval_remarks = str_replace("'","",$pr_approval_remarks1);
		$pr_approval_inst = $this->input->post("APPROVE");
		$pr_approval_by = $_SESSION['username'];
		$pr_approval_date = date('Y-m-d H:i:s');
		
		$level2_approval_req = $this->input->post("level2_approval_req");
		
		if ($pr_approval_inst == 'APPROVE' && $level2_approval_req == 'Yes'){
			
			$pr_status = "PR Approved At Planning Level1";
			
		} else if($pr_approval_inst == 'APPROVE' && ($level2_approval_req == 'No' || $level2_approval_req == '')) {
			
			$pr_status = "Pending For PR Authorize In ERP";
			
		} else {
			
			$pr_status = "PR Disapproved At Planning Level1";
			
		}
		
		$level = "LEVEL 1";
		$datetime = date('Y-m-d H:i:s');
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql = "update tipldb..pr_submit_table set pr_approval_by = '".$pr_approval_by."', pr_approval_inst = '".$pr_approval_inst."', pr_approval_date = '".$pr_approval_date."', pr_approval_remarks = '".$pr_approval_remarks."', pr_status = '".$pr_status."' where pr_num = '".$pr_num."' ";
		
		$this->db->query($sql);
		
		$sql2 = "insert into tipldb..pr_submit_table_comment(pr_num, instruction, level, comment_by, datetime, comment, enabled) 
		VALUES ('".$pr_num."','".$pr_approval_inst."','".$level."','".$pr_approval_by."','".$datetime."','".$pr_approval_remarks."','YES')";
		
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		//Transanction Complete
		
	  }
	  
	  // Purchase Request Authorizing Step Planning end
	  
	  // Purchase Request Authorizing Step Planning lvl2
	  public function view_prqit_prno_auth_lvl2($selectpr){ 
	   
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..pr_submit_table c, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = c.pr_num  and a.prqit_prno = '$selectpr' 
		 and a.prqit_itemcode = d.iou_itemcode");
		    
         return $query;  
      }
	  
	  public function insert_pr_sub_auth_lvl2($data){
		$username = $_SESSION['username'];
		
		$pr_num = $this->input->post("pr_num");
		$pr_date = $this->input->post("pr_date");
		
		$pr_approval_remarks_lvl2 = $this->input->post("remarks_auth");
		$pr_approval_remarks = str_replace("'","",$pr_approval_remarks_lvl2);
		
		$pr_approval_inst_lvl2 = $this->input->post("APPROVE");
		$pr_approval_by_lvl2 = $_SESSION['username'];
		$pr_approval_date_lvl2 = date('Y-m-d H:i:s');
		
		if ($pr_approval_inst_lvl2 == 'APPROVE'){
			$pr_status = "Pending For PR Authorize In ERP";
		} else {
			$pr_status = "PR Disapproved At Planning Level2";
		}
		
		$level = "LEVEL 2";
		$datetime = date('Y-m-d H:i:s');
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql = "update tipldb..pr_submit_table set pr_approval_by_lvl2 = '$pr_approval_by_lvl2', pr_approval_inst_lvl2 = '$pr_approval_inst_lvl2', 
		pr_approval_date_lvl2 = '$pr_approval_date_lvl2', pr_approval_remarks_lvl2 = '$pr_approval_remarks', pr_status = '$pr_status' 
		where pr_num = '$pr_num' ";
		
		$this->db->query($sql);
		
		$sql2 = "insert into tipldb..pr_submit_table_comment(pr_num, instruction, level, comment_by, datetime, comment, enabled) 
		VALUES ('".$pr_num."','".$pr_approval_inst_lvl2."','".$level."','".$pr_approval_by_lvl2."','".$datetime."','".$pr_approval_remarks."','YES')";
		
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		//Transanction Complete
		
	  }
	  
	  // Purchase Request Authorizing Step Planning lvl2 end
	  
	  // Purchase Request Authorizing Step Purchase Start
	  public function view_prqit_prno_auth_pur($selectpr){  
	  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..pr_submit_table c, 
scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = c.pr_num  and a.prqit_prno = '$selectpr' 
and a.prqit_itemcode = d.iou_itemcode"); 
  
         return $query;  
      }
	  
	  public function insert_pr_sub_auth_pur($data){
		  
		$username = $_SESSION['username'];
		$pr_num = $this->input->post("pr_num");
		$pr_date = $this->input->post("pr_date");
		$purchase_rmks1 = $this->input->post("remarks_auth");
		$purchase_rmks = str_replace("'","",$purchase_rmks1);
		$purchase_auth_inst = $this->input->post("APPROVE");
		$purchase_auth_by = $_SESSION['username'];
		$purchase_auth_date = date('Y-m-d H:i:s');
		$level = "LEVEL 3";
		$datetime = date('Y-m-d H:i:s');
		
		if ($pr_approval_inst == 'APPROVE'){
			$pr_status = "PR Approved At Purchase Level";
		} else {
			$pr_status = "PR Disapproved At Purchase Level";
		}
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql = "update tipldb..pr_submit_table set purchase_auth_by = '$purchase_auth_by', purchase_auth_inst = '$purchase_auth_inst', purchase_auth_date = '$purchase_auth_date', purchase_rmks = '$purchase_rmks', pr_status = '$pr_status' where pr_num = '$pr_num'";
		
		$this->db->query($sql);
		
		$sql2 = "insert into tipldb..pr_submit_table_comment(pr_num, instruction, level, comment_by, datetime, comment, enabled) 
		VALUES ('".$pr_num."','".$purchase_auth_inst."','".$level."','".$purchase_auth_by."','".$datetime."','".$purchase_rmks."','YES')";
		
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		//Transanction Complete
		
	  }
	  
	  //Purchase Request Authorizing Step Purchase End
	  
	  
	  //Action Timing Report Queries
	  public function prerp($selectpr){
		  
		$query = $this->db->query("select preqm_prno, preqm_createdby, preqm_createddate, preqm_lastmodifiedby, preqm_lastmodifieddate from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$selectpr'");
		
		return $query;  
	  }
	  
	  public function prerplive($selectpr){
		  
		$query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr a, tipldb..pr_submit_table b where a.preqm_prno = b.pr_num 
and a.preqm_prno = '$selectpr'");
		
		return $query;  
	  }
	  // Action Timing Report Queries
	  
	  //Chat History
	  
	  public function chat_history_table($selectpr){
		  
		$query = $this->db->query("select * from tipldb..pr_submit_table_comment where pr_num = '$selectpr' and enabled = 'YES' order by s_no ASC");
		
		return $query;  
	  }
	  
	  //Chat History
	  
	  //Approval Conditions
	  public function master_data($usage_type,$atac_selection,$wo,$category){
		  
		$query = $this->db->query("select * from tipldb..pr_approval_master 
		where usage_type = '$usage_type' and atac = '$atac_selection' and workorder = '$wo' and category = '$category'");
		
		return $query;  
	  }
	  
		//Pendal Card New Queries
		
		public function user_det($username){
		
			$email = $username."@tipl.com";
			
			$query = $this->db->query("select * from tipldb..login where email = '$email'");
			
			return $query;    
		}
		
		public function reorder_proc($itemcode, $id){
			
			$query = $this->db->query("exec tipldb..reorder_report_proc '$itemcode','$id'");
			
			return $query;    
		}
		
		public function pics_entry($itemcode){
		
			$query = $this->db->query("select * from TIPLDB..pics_entry where item_code = '$itemcode' order by sno asc");
			
			return $query; 
		}
		
		public function cut_peice_det($itemcode){
		
			$query = $this->db->query("select *,convert(date,create_dt) as createdt, convert(date,modified_dt) as modified
			from TIPLDB..stock_cut_piece where item_code = '$itemcode' order by ID asc");
			
			return $query; 
		}
		
		public function item_creation_hist($itemcode){
		
			$query = $this->db->query("select * from tipldb..itemCreation_conversion where item_code='".$itemcode."' order by id");
			
			return $query; 
		}
		
		public function item_drawing($itemcode){
		
			$query = $this->db->query("SELECT * FROM TIPLDB..item_internalDrawing WHERE item_code='".$itemcode."'");
			
			return $query; 
		}
		
		public function item_drawing_det($itemcode){
		
			$query = $this->db->query("select * from TIPLDB..item_internalDrawing where item_code='".$itemcode."' and status='active'");
			
			return $query; 
		}
		
		public function cons_quantity($itemcode, $id){
		
			$query = $this->db->query("SELECT * FROM TIPLDB..itemLastYear_consumption_cns WHERE item_code = '$itemcode' and user_id = '$id'");
			
			return $query; 
		}
		
		public function lead_time_qry($itemcode, $id){
		
			$query = $this->db->query("SELECT * FROM TIPLDB..last_five_transactions_pr_po_grv_cns WHERE user_id='$id' AND item_code = '$itemcode'");
			
			return $query; 
		}	
		
		public function project_category($pm_group){
		
			$query = $this->db->query("select distinct product_type from tipldb..ind_wise_pm_so where business_vertical = '".$pm_group."' 
			and prod_visible = 'Yes'");
			
			return $query; 
		} 
		
		public function erp_stat($pr_num){
		
			$query = $this->db->query("select distinct preqm_status from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'");
			
			return $query; 
		} 
}  
?>