<?php
class createfreightm extends CI_Model  
{  
	function __construct()  
	{  
		parent::__construct();  
	}
	
	public function max_amend_no($selectpr)
	{
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		
		return $query; 
	}
	
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	/********** Level 2 ***********/	 
	
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
		where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");  
	
		return $query;  
	}
	
	public function insert_po_sub_lvl2($data, $uploaded_check_img)
	{
		$po_num  = $this->input->post("po_num");
		
		//new elements
		
		$freight_payment_mode           = $this->input->post("freight_payment_mode");
		$freight_supp_acc_name          = $this->input->post("supp_acc_name");
		$freight_supp_acc_no            = $this->input->post("supp_acc_no");
		$freight_supp_bank_name         = $this->input->post("supp_bank_name");
		$freight_supp_ifsc_code         = $this->input->post("supp_ifsc_code");
		$freight_supp_transfered_amount = $this->input->post("supp_transfered_amount");
		$freight_transfer_type          = $this->input->post("transfer_type");
		$freight_date_of_transfer       = $this->input->post("date_of_transfer");
		$freight_transfer_from_bank     = $this->input->post("transfer_from_bank");
		$freight_transfer_from_acc      = $this->input->post("transfer_from_acc");
		$freight_cheque_no              = $this->input->post("freight_cheque_no");
		$freight_cheque_date            = $this->input->post("freight_cheque_date");
		$freight_bank_name              = $this->input->post("freight_bank_name");
		$freight_cheque_amt             = $this->input->post("freight_cheque_amt");
		
		//new elements
		
		$freight_rmks_lvl2   = $this->input->post("freight_rmks_lvl2");
		$freight_create_inst_lvl2   = $this->input->post("freight_create_inst_lvl2");
		$freight_createdby_lvl2    = $_SESSION['username'];
		$freight_created_date_lvl2  =  date('Y-m-d H:i:s');
		$status = "Freight Payment Done";
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..insert_po set freight_payment_mode = '$freight_payment_mode', freight_supp_acc_name = '$freight_supp_acc_name',  
		freight_supp_acc_no = '$freight_supp_acc_no', freight_supp_bank_name = '$freight_supp_bank_name', freight_supp_ifsc_code = '$freight_supp_ifsc_code', 
		freight_supp_transfered_amount = '$freight_supp_transfered_amount', freight_transfer_type = '$freight_transfer_type', 
		freight_date_of_transfer = '$freight_date_of_transfer', freight_transfer_from_bank = '$freight_transfer_from_bank', 
		freight_transfer_from_acc = '$freight_transfer_from_acc', freight_cheque_no = '$freight_cheque_no', 
		freight_cheque_date = '$freight_cheque_date', freight_bank_name = '$freight_bank_name', 
		freight_cheque_amt = '$freight_cheque_amt', freight_uploaded_cheque_img = '$freight_uploaded_cheque_img', 
		freight_rmks_lvl2 = '$freight_rmks_lvl2', freight_create_inst_lvl2 = '$freight_create_inst_lvl2', 
		freight_createdby_lvl2 = '$freight_createdby_lvl2', freight_created_date_lvl2 = '$freight_created_date_lvl2' , 
		status ='$status' where po_num = '$po_num'";
			
		$sql2 = "update tipldb..po_master_table set status = '$status' where po_num = '$po_num'";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		//Transanction Complete
	}	 
}  
?>