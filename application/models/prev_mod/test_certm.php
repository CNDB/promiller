<?php
class test_certm extends CI_Model  
{  
	function __construct()  
	{   
		parent::__construct();  
	}
			
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	{
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		 
		 return $query; 
	}

	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}

	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
		where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");  
		
		return $query;  
	}

	public function insert_po_sub_lvl2($data, $testcert)
	{
		$po_num  = $this->input->post("po_num");
		$testcert_by    = $_SESSION['username'];
		$testcert_date  =  date('Y-m-d H:i:s');
		$status = "Test Certificate Uploaded";
		
		// Trasanction Start
		
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..insert_po set uploaded_testcert = '$testcert', testcert_by = '$testcert_by', 
		testcert_date = '$testcert_date', status = '$status' where po_num = '$po_num'";
		
		$sql2 = "update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'";
		
		$this->db->query($sql1);
		
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		
		//Transanction Complete
	}
			 
}  
?>