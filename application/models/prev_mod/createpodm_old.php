<?php
class createpodm extends CI_Model  
   {  
      function __construct()  
      {   
         parent::__construct();  
      }  
	   
      public function select_prqit_prno()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where SUBSTRING(pomas_pono, 1, 3) in ('IPO', 'LPO','FPO') 
and pomas_podocstatus ='FR'");  

         return $query;  
      }
	 
	 public function max_amend_no($selectpr){
		 
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		 return $query; 
	 }
	 
	 public function view_poqit_pono($selectpr, $amend_no)  
     {
		 $query1 = $this->db->query("select * from scmdb..po_poprq_poprcovg_detail where poprq_pono = '$selectpr' ");
		 
		 if( $query1->num_rows() > 0) {
		   
         $query = $this->db->query("select *, datediff(DAY, a.pomas_podate, getdate()) as diff, datediff(DAY, i.preqm_prdate, getdate()) as diff1 
from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, scmdb..po_poprq_poprcovg_detail d, 		        		      
scmdb..supp_addr_address e, scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h, 
scmdb..prq_preqm_pur_reqst_hdr i, scmdb..supp_bu_suplmain j where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
and a.pomas_pono = d.poprq_pono and a.pomas_pono ='$selectpr' and b.poitm_polineno = d.poprq_polineno and c.supp_spmn_supcode = e.supp_addr_supcode 	        
and b.poitm_itemcode = f.ml_itemcode and f.ml_itemcode = g.lov_itemcode and a.pomas_pono = h.paytm_pono 
and a.pomas_poamendmentno = '$amend_no' and a.pomas_poamendmentno = b.poitm_poamendmentno and a.pomas_poamendmentno = d.poprq_poamendmentno and 		         
a.pomas_poamendmentno = h.paytm_poamendmentno and d.poprq_prno = i.preqm_prno and e.supp_addr_supcode = j.supp_bu_supcode 
and e.supp_addr_addid = j.supp_bu_deforderto");
		 
		 } else {
			 
		 $query = $this->db->query("select *, datediff(DAY, a.pomas_podate, getdate()) as diff 
from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, 		        					            			        
scmdb..supp_addr_address e, scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h, scmdb..supp_bu_suplmain i
where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
and a.pomas_pono ='$selectpr' and c.supp_spmn_supcode = e.supp_addr_supcode and b.poitm_itemcode = f.ml_itemcode and f.ml_itemcode = g.lov_itemcode         
and a.pomas_pono = h.paytm_pono and a.pomas_poamendmentno = '$amend_no' and a.pomas_poamendmentno = b.poitm_poamendmentno 
and a.pomas_poamendmentno = h.paytm_poamendmentno and e.supp_addr_supcode = i.supp_bu_supcode and e.supp_addr_addid = i.supp_bu_deforderto"); 
		 
		 }
		 							 
         return $query;  
     }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;
		     
	  }
	  
	  public function insert_attachment($po_num,$po_quote_frmsupp_newname) {
		 	  
			  $date_time = date('Y-m-d H:i:s');
		  
			  $sql ="insert into tipldb..po_supplier_quotes (po_num, attached_supp_quotes, date_time) 
			  values('".$po_num."','".$po_quote_frmsupp_newname."','".$date_time."')";
			  
			  $query = $this->db->query($sql);
	  }
	  //New Approval Conditions
	  
	  public function insert_po_sub($data, $lvl1_approval_by, $lvl2_approval_by, $lvl1_email, $lvl2_email, $lvl1_app_req, $lvl2_app_req){
		     
	 	$username            = $_SESSION['username'];
		$po_num              = $this->input->post("po_num");
		$po_line_no          = $this->input->post("po_line_no");
		$po_ipr_no           = $this->input->post("po_ipr_no");
		$array_count		 = count($po_ipr_no);
		$po_date             = $this->input->post("po_date");
		$po_item_code        = $this->input->post("po_item_code");
		$po_qty              = $this->input->post("po_qty");
		$po_uom              = $this->input->post("po_uom");
		$po_cost_pr_unt      = $this->input->post("po_cost_pr_unt");
		$po_need_date        = $this->input->post("po_need_date");
		$po_wh_code          = $this->input->post("po_wh_code");
		$po_basic_val        = $this->input->post("po_basic_val");
		$po_tot_val          = $this->input->post("po_tot_val");
		$po_supp_code        = $this->input->post("po_supp_code");
		$po_supp_name        = $this->input->post("po_supp_name");
		$po_supp_add         = $this->input->post("po_supp_add");
		$po_supp_email       = $this->input->post("po_supp_email");
		$po_drwg_refno       = $this->input->post("po_drwg_refno");
		$item_desc           = $this->input->post("item_desc");
		$payterm             = $this->input->post("payterm");
		$payterm_desc        = $this->input->post("payterm_desc");
		$freight             = $this->input->post("freight");
		$po_rmks1            = $this->input->post("po_rmks");
		$po_rmks             = str_replace("'","",$po_rmks1);
		$po_deli_type        = $this->input->post("po_deli_type");
		$po_lead_time        = $this->input->post("po_lead_time");
		$po_manfact_clernce  = $this->input->post("manufact_clrnce");
		$po_dispatch_inst    = $this->input->post("dispatch_inst");
		$po_approval_lvl0    = $this->input->post("po_approval_lvl0");
		$po_approvedby_lvl0  = $_SESSION['username'];
		$currency            = $this->input->post("currency");
		$erp_part_code       = $this->input->post("carrier_name");
		$carrier_name        = $this->input->post("carrier_name");
		$lastyr_cons         = $this->input->post("lastyr_cons");
		$current_stk         = $this->input->post("current_stk");
		$reservation_qty     = $this->input->post("reservation_qty");
		$for_stk_quantity    = $this->input->post("for_stk_quantity");
		$calculation         = $this->input->post("calculation");
		$last_price          = $this->input->post("last_price");
		$current_price       = $this->input->post("current_price");
		$pnf                 = $this->input->post("pnf");
		$po_total_value      = $this->input->post("po_total_value");
		$item_remarks        = $this->input->post("item_remarks");
		$po_amend_no         = $this->input->post("po_amend_no");
		$po_type             = $this->input->post("po_type");
		$supp_phone          = $this->input->post("supp_phone");
		$contact_person      = $this->input->post("contact_person");
		$pay_mode            = $this->input->post("pay_mode");
		$trans_mode          = $this->input->post("trans_mode");
		$partial_ship        = $this->input->post("partial_ship");
		$category            = $this->input->post("category");
		$pm_group            = $this->input->post("pm_group");
		$current_yr_con      = $this->input->post("current_yr_con");
		$color               = $this->input->post("color");
		//new added parameters
		$current_yr_con      = $this->input->post('current_yr_con');
		$reorder_level       = $this->input->post('iou_reorderlevel');
		$reorder_qty         = $this->input->post('iou_reorderqty');
		$last_supplier       = $this->input->post('SuppName');
		$last_supplier_lead_time = $this->input->post('SuppLeadTime');
		$last_supplier_rate  = $this->input->post('SuppRate');
		$type_of_pr          = $this->input->post('ipr_type');
		$project_name        = $this->input->post('project_name');
		$atac_no             = $this->input->post('atac_no');
		$costing             = $this->input->post('costing');
		$costing_currency    = $this->input->post('costing_currency');
		$po_from_screen      = $this->input->post('po_from_screen');
		//$category1           = $this->input->post("category1");

		//Service PO Feilds Starts
		$spo_type            = $this->input->post('spo_type');
		$spo_usage_type      = $this->input->post('spo_usage_type');
		$spo_atac_no         = $this->input->post('spo_atac_no');
		$spo_atac_ld_date    = $this->input->post('spo_atac_ld_date');
		$spo_atac_need_date  = $this->input->post('spo_atac_need_date');
		$spo_atac_payment_terms = $this->input->post('spo_atac_payment_terms');
		$spo_atac_customer_payment_terms = $this->input->post('spo_atac_customer_payment_terms');
		$spo_project_name    = $this->input->post('spo_project_name');
		$spo_customer_name   = $this->input->post('spo_customer_name');
		$spo_pm_group        = $this->input->post('spo_pm_group');
		$spo_so_no           = $this->input->post('spo_so_no');
		$spo_why_spcl_rmks   = $this->input->post('spo_special_remarks');
		$attach_cost_sheet   = $this->input->post('attach_cost_sheet');
		
		//service payment milestone feilds
		
		$spo_description = $this->input->post('description');
		$spo_array_count = count($spo_description);
		$spo_target_date = $this->input->post('date');
		$percentage_po_value = $this->input->post('perct_po_value');
		$amount = $this->input->post('amount');
		$spo_payment_term = $this->input->post('spo_payterm');
		$spo_payment_method = $this->input->post('paymode');
		$pdc_days = $this->input->post('pdc_days');
		
		//service payment milestone feilds
		
		//Service PO Feilds Ends
		 
		$fpo_po = substr($po_num,0,3);
		//Service PO Category Starts
		
		if($fpo_po == 'SRV'){
			$category1 = $this->input->post("spo_category");
		} else {
			$category1 = $this->input->post("category1"); 
		}
		 
		//Service PO Category Ends
		
		if($lvl1_app_req == 'Yes'){
			$status              = "PO Send For Level 1 Authorization";
	    } else{
			$status              = "Authorized PO Send to Supplier";
		}
		
		$username            = $_SESSION['username'];
		$create_date         = date('Y-m-d H:i:s');
		$po_approveddate_lvl0  = date('Y-m-d H:i:s');
		$total_item_value    = $this->input->post("item_value");
		$freight_place       = $this->input->post("freight_place");
		$insurance_liablity  = $this->input->post("insurance_liablity");
		$freight_type        = $this->input->post("freight_type");
		$approx_freight      = $this->input->post("approx_freight");
		$spcl_inst_supp1     = $this->input->post("spcl_inst_supp");
		$spcl_inst_supp      = str_replace("'","",$spcl_inst_supp1);
		$ld_applicable       = $this->input->post("ld_applicable");
		$road_permit_req     = $this->input->post("road_permit_req");
		$supp_state          = $this->input->post("supp_state");
		$level               = "LEVEL 0";
		$why_spcl_rmks       = $this->input->post('why_spcl_rmks');
		
		//Checking Previous Entry In PO master Table and insert Po table
		$sql = "select count(*) as abc from tipldb..po_master_table where po_num = '$po_num'";
		
		$query = $this->db->query($sql)->row();
		
		$count = $query->abc;
		
		if ($count > 0) {
			
		//Transaction Start
		$this->db->trans_start();
		
		if($po_from_screen == 'fresh' || $po_from_screen == 'amend_po'){
		//Inserting Records in backup table
		$sql_backup_query1 = "insert into TIPLDB..insert_po_amend_history select * from TIPLDB..insert_po where po_num = '$po_num'";
		$sql_backup_query2 = "insert into TIPLDB..po_master_table_amend_history select * from TIPLDB..po_master_table where po_num = '$po_num'";
		
		$this->db->query($sql_backup_query1);
		$this->db->query($sql_backup_query2);
		//Inserting Records in backup table
		}
		
		//deleting the previous records	
		$sql_dis_query1 = "delete from tipldb..insert_po where po_num = '$po_num'";
		$sql_dis_query2 = "delete from tipldb..po_master_table where po_num = '$po_num'";
		
		$this->db->query($sql_dis_query1);
		$this->db->query($sql_dis_query2);
		//insert new record in PO master table and insert PO table
		
		for($i=0;$i<$array_count;$i++){
		
		 $sql1 = "insert into TIPLDB..insert_po (po_num, po_line_no, po_ipr_no, po_date, po_item_code, po_qty, po_uom, po_cost_pr_unt, po_need_date, 
 po_wh_code , po_basic_val , po_tot_val , po_supp_code , po_supp_name , po_supp_add , po_supp_email , po_drwg_refno , po_itm_desc, payterm, 
 freight, po_rmks, po_deli_type , po_lead_time , po_manfact_clernce , po_dispatch_inst , po_approval_lvl0 , po_approvedby_lvl0, status, 
 currency, carrier_name, lastyr_cons, current_stk, reservation_qty, for_stk_quantity, last_price, current_price, po_total_value, 
 item_remarks, po_amend_no, po_type, supp_phone, contact_person, pay_mode, trans_mode, partial_ship, total_item_value, 
 freight_place, insurance_liablity, freight_type, po_spcl_inst_frm_supp, approx_freight, ld_applicable, payterm_desc, supp_state, road_permit_req,
 ipr_pm_group, po_approveddate_lvl0, current_yr_con, color, ipr_category, reorder_level, reorder_qty, last_supplier, last_supplier_lead_time, last_supplier_rate, type_of_pr, project_name, atac_no, costing, costing_currency, why_spcl_rmks, attach_cost_sheet) 
 VALUES ('".$po_num."','".$po_line_no[$i]."','".$po_ipr_no[$i]."','".$po_date."','".$po_item_code[$i]."','".$po_qty."','".$po_uom[$i]."',
'".$po_cost_pr_unt."','".$po_need_date."','".$po_wh_code."','".$po_basic_val."','".$po_tot_val."','".$po_supp_code."','".$po_supp_name."',
'".$po_supp_add."','".$po_supp_email."','".$po_drwg_refno."','".$item_desc[$i]."','".$payterm."','".$freight."','".$po_rmks."',
'".$po_deli_type."','".$po_lead_time."','".$po_manfact_clernce[$i]."','".$po_dispatch_inst[$i]."','".$po_approval_lvl0."','".$po_approvedby_lvl0."',
'".$status."','".$currency."', '".$carrier_name."', '".$lastyr_cons[$i]."','".$current_stk[$i]."', '".$reservation_qty[$i]."', 
'".$for_stk_quantity[$i]."','".$last_price[$i]."', '".$current_price[$i]."','".$po_total_value."', '".$item_remarks[$i]."', '".$po_amend_no."', 
'".$po_type."', '".$supp_phone."', '".$contact_person."', '".$pay_mode."','".$trans_mode."', '".$partial_ship."', '".$total_item_value[$i]."', 
'".$freight_place."', '".$insurance_liablity."', '".$freight_type."', '".$spcl_inst_supp."', '".$approx_freight."', '".$ld_applicable."', 
'".$payterm_desc."','".$supp_state."','".$road_permit_req."','".$pm_group[$i]."','".$po_approveddate_lvl0."','".$current_yr_con[$i]."',
'".$color[$i]."','".$category[$i]."','".$reorder_level[$i]."','".$reorder_qty[$i]."','".$last_supplier[$i]."','".$last_supplier_lead_time[$i]."','".$last_supplier_rate[$i]."','".$type_of_pr[$i]."','".$project_name[$i]."','".$atac_no[$i]."','".$costing[$i]."','".$costing_currency[$i]."'
,'".$why_spcl_rmks[$i]."','".$attach_cost_sheet[$i]."')";
		
		$query1 = $this->db->query($sql1);
		}
		
		$sql2 = "insert into tipldb..po_master_table (po_num, uom, supplier_code, po_create_date, po_target_date, created_by, create_date, 
po_approval_level0, status, po_supp_name, po_total_value, supp_state, road_permit_req, currency, payterm, freight, level1_mail_to, level2_mail_to, level1_approval_by, level2_approval_by, po_amend_no, po_from_screen, po_category, freight_type, po_deli_type, spo_type, spo_usage_type, spo_atac_no, spo_atac_ld_date, spo_atac_need_date, spo_atac_payment_terms, spo_atac_customer_payment_terms, spo_project_name, spo_customer_name, spo_pm_group, spo_so_no, spo_why_spcl_rmks, lvl1_app_req, lvl2_app_req)
values('".$po_num."','".$po_uom."','".$po_supp_code."','".$po_date."','".$po_date."','".$po_approvedby_lvl0."','".$create_date."'
,'".$po_approval_lvl0."','".$status."','".$po_supp_name."','".$po_total_value."','".$supp_state."','".$road_permit_req."','".$currency."'
,'".$payterm."','".$freight."','".$lvl1_approval_by."','".$lvl2_approval_by."','".$lvl1_approval_by."','".$lvl2_approval_by."','".$po_amend_no."',
'".$po_from_screen."','".$category1."','".$freight_type."','".$po_deli_type."','".$spo_type."','".$spo_usage_type."','".$spo_atac_no."','".$spo_atac_ld_date."',
'".$spo_atac_need_date."','".$spo_atac_payment_terms."','".$spo_atac_customer_payment_terms."','".$spo_project_name."','".$spo_customer_name."',
'".$spo_pm_group."','".$spo_so_no."','".$spo_why_spcl_rmks."','".$lvl1_app_req."','".$lvl2_app_req."')";
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment) 
values ('".$po_num."','".$po_approval_lvl0."','".$level."','".$username."','".$create_date."','".$po_rmks."')";
		
		//SPO Live Details Starts

		for($j=0; $j<$spo_array_count; $j++){

$sql4 ="insert into tipldb..spo_payment_milestones(po_num, description, target_date, percentage_po_value, amount, payterm, payment_method, pdc_days, date_time)
values('".$po_num."','".$spo_description[$j]."','".$spo_target_date[$j]."','".$percentage_po_value[$j]."','".$amount[$j]."','".$spo_payment_term[$j]."','".$spo_payment_method[$j]."','".$pdc_days[$j]."','".$create_date."')";

echo $sql4;

		$query4 = $this->db->query($sql4);

		}
		
		//SPO Live Details Ends
		
		$query2 = $this->db->query($sql2);
		
		$query3 = $this->db->query($sql3);
		
		$this->db->trans_complete();
		//Transanction Complete
		
		} else {
			
		//Transaction Start
		$this->db->trans_start();
		
		//insert new record in PO master table and insert PO table	
		for($i=0;$i<$array_count;$i++){
		
		 $sql1 = "insert into TIPLDB..insert_po (po_num, po_line_no, po_ipr_no, po_date, po_item_code, po_qty, po_uom, po_cost_pr_unt, po_need_date, 
po_wh_code , po_basic_val , po_tot_val , po_supp_code , po_supp_name , po_supp_add , po_supp_email , po_drwg_refno , po_itm_desc, payterm, 
freight, po_rmks, po_deli_type , po_lead_time , po_manfact_clernce , po_dispatch_inst , po_approval_lvl0 , po_approvedby_lvl0, status, 
currency, carrier_name, lastyr_cons, current_stk, reservation_qty, for_stk_quantity, last_price, current_price, po_total_value, 
item_remarks, po_amend_no, po_type, supp_phone, contact_person, pay_mode, trans_mode, partial_ship, total_item_value, 
freight_place, insurance_liablity, freight_type, po_spcl_inst_frm_supp, approx_freight, ld_applicable, payterm_desc, supp_state, 
road_permit_req, pm_group, po_approveddate_lvl0, current_yr_con, color, ipr_category, reorder_level, reorder_qty, last_supplier, last_supplier_lead_time, last_supplier_rate, type_of_pr, project_name, atac_no, costing, costing_currency, why_spcl_rmks, attach_cost_sheet)  
VALUES ('".$po_num."','".$po_line_no[$i]."','".$po_ipr_no[$i]."','".$po_date."','".$po_item_code[$i]."','".$po_qty."','".$po_uom[$i]."',
'".$po_cost_pr_unt."','".$po_need_date."','".$po_wh_code."','".$po_basic_val."','".$po_tot_val."','".$po_supp_code."','".$po_supp_name."',
'".$po_supp_add."','".$po_supp_email."','".$po_drwg_refno."','".$item_desc[$i]."','".$payterm."','".$freight."','".$po_rmks."',
'".$po_deli_type."','".$po_lead_time."','".$po_manfact_clernce[$i]."','".$po_dispatch_inst[$i]."','".$po_approval_lvl0."','".$po_approvedby_lvl0."',
'".$status."','".$currency."', '".$carrier_name."', '".$lastyr_cons[$i]."','".$current_stk[$i]."', '".$reservation_qty[$i]."', 
'".$for_stk_quantity[$i]."','".$last_price[$i]."', '".$current_price[$i]."','".$po_total_value."', '".$item_remarks[$i]."', '".$po_amend_no."', 
'".$po_type."', '".$supp_phone."', '".$contact_person."', '".$pay_mode."','".$trans_mode."', '".$partial_ship."', '".$total_item_value[$i]."', 
'".$freight_place."', '".$insurance_liablity."', '".$freight_type."', '".$spcl_inst_supp."', '".$approx_freight."', '".$ld_applicable."', 
'".$payterm_desc."','".$supp_state."','".$road_permit_req."','".$pm_group[$i]."','".$po_approveddate_lvl0."','".$current_yr_con[$i]."','".$color[$i]."'
,'".$category[$i]."','".$reorder_level[$i]."','".$reorder_qty[$i]."','".$last_supplier[$i]."','".$last_supplier_lead_time[$i]."','".$last_supplier_rate[$i]."','".$type_of_pr[$i]."','".$project_name[$i]."','".$atac_no[$i]."','".$costing[$i]."','".$costing_currency[$i]."','".$why_spcl_rmks[$i]."','".$attach_cost_sheet[$i]."')";
		
		$query1 = $this->db->query($sql1);
		}
		
		$sql2 = "insert into tipldb..po_master_table (po_num, uom, supplier_code, po_create_date, po_target_date, created_by, create_date, 
po_approval_level0, status, po_supp_name, po_total_value, supp_state, road_permit_req, currency, payterm, freight, level1_mail_to, level2_mail_to, level1_approval_by, level2_approval_by, po_amend_no, po_from_screen, po_category, freight_type, po_deli_type, spo_type, spo_usage_type, spo_atac_no, spo_atac_ld_date, spo_atac_need_date, spo_atac_payment_terms, spo_atac_customer_payment_terms, spo_project_name, spo_customer_name, spo_pm_group, spo_so_no, spo_why_spcl_rmks, lvl1_app_req, lvl2_app_req)
values('".$po_num."','".$po_uom."','".$po_supp_code."','".$po_date."','".$po_date."','".$po_approvedby_lvl0."','".$create_date."'
,'".$po_approval_lvl0."','".$status."','".$po_supp_name."','".$po_total_value."','".$supp_state."','".$road_permit_req."','".$currency."'
,'".$payterm."','".$freight."','".$lvl1_approval_by."','".$lvl2_approval_by."','".$lvl1_approval_by."','".$lvl2_approval_by."','".$po_amend_no."',
'".$po_from_screen."','".$category1."','".$freight_type."','".$po_deli_type."','".$spo_type."','".$spo_usage_type."','".$spo_atac_no."','".$spo_atac_ld_date."',
'".$spo_atac_need_date."','".$spo_atac_payment_terms."','".$spo_atac_customer_payment_terms."','".$spo_project_name."','".$spo_customer_name."',
'".$spo_pm_group."','".$spo_so_no."','".$spo_why_spcl_rmks."','".$lvl1_app_req."','".$lvl2_app_req."')";
		
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment) 
values ('".$po_num."','".$po_approval_lvl0."','".$level."','".$username."','".$create_date."','".$po_rmks."')";
		
		$query2 = $this->db->query($sql2);
		
		$query3 = $this->db->query($sql3);
		
		//SPO Live Details Starts

		for($j=0; $j<$spo_array_count; $j++){

$sql4 ="insert into tipldb..spo_payment_milestones(po_num, description, target_date, percentage_po_value, amount, payterm, payment_method, pdc_days, date_time)
values('".$po_num."','".$spo_description[$j]."','".$spo_target_date[$j]."','".$percentage_po_value[$j]."','".$amount[$j]."','".$spo_payment_term[$j]."','".$spo_payment_method[$j]."','".$pdc_days[$j]."','".$create_date."')";

echo $sql4;

		$query4 = $this->db->query($sql4);

		}
		
		//SPO Live Details Ends
		
		$this->db->trans_complete();
		//Transanction Complete	
		}
	  }
	  
	  //New Approval Conditions
	  
	  
	  //action timing query level 0
	  
	  public function act_timing($selectpr)  
      {  
		 $query = $this->db->query("select pomas_pono, pomas_createdby, pomas_createddate, pomas_lastmodifiedby, pomas_lastmodifieddate
from SCMDB..po_pomas_pur_order_hdr where pomas_pono = '$selectpr'");  

         return $query;
     }
	 
	 //action timing query level 0
		
		
		/********** Level 1 **********/	 
	 
	 public function select_prqit_prno_lvl1()  
      {  
         $query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  

         return $query;  
     }
	 
	 public function po_view_lvl1($selectpr, $amend_no)  
      {  
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c 
where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and a.po_num = c.po_num 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr')");  

         return $query;  
     }
	  
	  public function insert_po_sub_lvl1($data)
	  {
		$po_num                = $this->input->post("po_num");
		$po_approval_lvl1      = $this->input->post("po_approval_lvl1");
		$po_app_disapp_rmks_lvl11   = $this->input->post("app_disapp_comnts_lvl1");
		$po_app_disapp_rmks_lvl1    = str_replace("'","",$po_app_disapp_rmks_lvl11);
		$po_approvedby_lvl1    = $_SESSION['username'];
		$po_approveddate_lvl1  =  date('Y-m-d H:i:s');
		$level 				 = "LEVEL 1";
		
		//new code added on 06-04-2018
		$lvl1_app_req  = $this->input->post("lvl1_app_req"); 
		$lvl2_app_req  = $this->input->post("lvl2_app_req");
		//new code added on 06-04-2018
		
		//die;
		
		if($po_approval_lvl1 == 'Approve' && $lvl2_app_req == 'Yes'){
			$status = "PO Send For Level 2 Authorization";
		}else if($po_approval_lvl1 == 'Approve' && $lvl2_app_req == 'No'){
			$status = "Authorized PO Send To Supplier";
		} else {
			$status = "PO Disapproved At Level 1";	
		}
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..insert_po set po_approval_lvl1 = '".$po_approval_lvl1."', po_approvedby_lvl1 = '".$po_approvedby_lvl1."', 
status='".$status."',  po_approveddate_lvl1 = '".$po_approveddate_lvl1."' where po_num = '".$po_num."'";
		
		$sql2 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment) 
VALUES ('".$po_num."','".$po_approval_lvl1."','".$level."','".$po_approvedby_lvl1."','".$po_approveddate_lvl1."','".$po_app_disapp_rmks_lvl1."')";
		
		$sql3 = "update TIPLDB..po_master_table set po_approval_lvl1 = '".$po_approval_lvl1."', status='".$status."' where po_num = '".$po_num."' ";
		
		$query1  = $this->db->query($sql1);
		$query2  = $this->db->query($sql2);
		$query3  = $this->db->query($sql3);
				
		$this->db->trans_complete();
		//Transanction Complete
	  }
	  
	  public function act_timing_lvl1($selectpr)  
      {  
         $query = $this->db->query("select pomas_pono, pomas_createdby, pomas_createddate, pomas_lastmodifiedby, pomas_lastmodifieddate
from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr'");  

         return $query;  
     }
	 
	 //action timing query level 0
	  
	 /********** Level 2 **********/	 
	 
	 public function select_prqit_prno_lvl2()  
      {  
         $query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve'");  

         return $query;  
     }
	 
	 public function po_view_lvl2($selectpr, $amend_no)  
      {  
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c 
where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and a.po_num = c.po_num 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr')");  

         return $query;  
     }
	 
	 public function mail_table($po_num)  
      {  
         $query = $this->db->query("select * from tipldb..po_master_table a, tipldb..insert_po b where b.po_num = '$po_num' and a.po_num = b.po_num");  

         return $query;  
     }
	 
	 //Mail to abhinav sir of amend po comparision
	 
	 public function amend_comparision($po_num)  
      {  
         $query = $this->db->query("select count(*) as count from TIPLDB..po_master_table a, TIPLDB..po_master_table_amend_history b 
		 where a.po_num = '$po_num' and a.po_num = b.po_num and a.po_amend_no > b.po_amend_no 
		 and a.po_num in(select po_num from TIPLDB..po_master_table_amend_history)");  

         return $query;  
     }
	 
	 public function mail_table_history($po_num)  
      {  
         $query = $this->db->query("select * from tipldb..po_master_table_amend_history a, tipldb..insert_po_amend_history b 
where a.po_num = '$po_num' and a.po_num = b.po_num and a.po_amend_no = b.po_amend_no
and a.po_amend_no = (select max(po_amend_no) from TIPLDB..po_master_table_amend_history where po_num = '$po_num')");  

         return $query;  
     }
	 
	 //Mail to abhinav sir of amend po comparision ends
	 
	 public function supplier_quotes($selectpr)  
      {   
		 $query = $this->db->query("select * from TIPLDB..po_supplier_quotes where po_num = '$selectpr'");  

         return $query;  
     }
	 
	 public function project($selectpr)  
      {  
         $query = $this->db->query("select * from tipldb..insert_po a, tipldb..pr_submit_table b where a.po_ipr_no = b.pr_num and po_num = '$selectpr'");  

         return $query;  
     }
	  
	  public function insert_po_sub_lvl2($data)
	  {
		$po_num                 = $this->input->post("po_num");
		$po_s_no                = $this->input->post("po_s_no");
		$po_approval_lvl2       = $this->input->post("po_approval_lvl2");
		$po_approvedby_lvl2     = $_SESSION['username'];
		$po_approvaldate_lvl2   =  date('Y-m-d H:i:s');
		$status                 = "Authorized PO Send to Supplier";
		$app_disapp_comnts_lvl21 = $this->input->post("app_disapp_comnts_lvl2");
		$app_disapp_comnts_lvl2 = str_replace("'","",$app_disapp_comnts_lvl21);
		$level 				  = "LEVEL 2";
		if($po_approval_lvl2 == 'Approve'){
			$status                = "Authorized PO Send to Supplier";
		} else {
			$status                = "PO Disapproved At Level 2";	
		}
		
		//Transanction Start
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..insert_po set po_approval_lvl2 = '$po_approval_lvl2', po_approvalby_lvl2 = '$po_approvedby_lvl2', status='$status',        
		po_approvaldate_lvl2 = '$po_approvaldate_lvl2', app_disapp_comnts_lvl2 = '$app_disapp_comnts_lvl2' where po_num = '$po_num' ";
		
		$sql2 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment) 
VALUES ('".$po_num."','".$po_approval_lvl2."','".$level."','".$po_approvedby_lvl2."','".$po_approvaldate_lvl2."','".$app_disapp_comnts_lvl2."')";
		
		$sql3 = "update TIPLDB..po_master_table set po_approval_lvl2 = '$po_approval_lvl2', status='$status' where po_num = '$po_num' ";
		
		$query1  = $this->db->query($sql1);
		$query2  = $this->db->query($sql2);
		$query3  = $this->db->query($sql3);
		
		$this->db->trans_complete();
		//Transanction Complete
		
	  }
	  
	  //action timing query level 0
	  
	  public function act_timing_lvl2($selectpr)  
      {  
         $query = $this->db->query("select pomas_pono, pomas_createdby, pomas_createddate, pomas_lastmodifiedby, pomas_lastmodifieddate
from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr'");  

         return $query;  
     }
	 
	 public function master_data($category, $po_type)  
      {  
		 $query = $this->db->query("select * from TIPLDB..po_approval_master_bkup where category = '$category' and po_type = '$po_type'");  

         return $query;  
     }
	 
	 public function creator_email($created_by)
	 {								
	  	 $query = $this->db->query("select * from TIPLDB..employee where emp_email like '%$created_by%' and emp_active = 'yes'");
		
		 return $query;
	 }
	 
   }  
?>