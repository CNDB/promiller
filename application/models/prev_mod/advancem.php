<?php
class Advancem extends CI_Model  
{  
	function __construct(){  
	  
		parent::__construct(); 
		 
	} 
	
	public function po_details($po_num){
		  
		$query = $this->db->query("select *,datediff(DAY, a.po_create_date, getdate()) as diff from tipldb..po_master_table a, TIPLDB..insert_po b, scmdb..po_pomas_pur_order_hdr c where a.po_num = '$po_num' and a.po_num = b.po_num and a.po_num = c.pomas_pono
and c.pomas_poamendmentno = (select max(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num')");  
		
		return $query;  
		
	}
	
	public function insert_advance(){
		  
		$po_num = $this->input->post("po_num");
		$advance_amount = $this->input->post("advance_amount");
		$advance_entered_by = $_SESSION['username'];
		$advance_entry_date = date('Y-m-d H:i:s');
		$payment_method = $this->input->post("payment_method");
		$trans_id = $this->input->post("trans_id");
		$trans_date = $this->input->post("trans_date");
		$cheque_no = $this->input->post("cheque_no");
		$cheque_date = $this->input->post("cheque_date");
		$voucher_no = $this->input->post("voucher_no");
		$voucher_date = $this->input->post("voucher_date");
		
		
		$status = "Advance Entered Pending For PI";
		
		//Trasanction Start
		
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..po_master_table set advance_amount = '$advance_amount', advance_entered_by = '$advance_entered_by', advance_entry_date = '$advance_entry_date', payment_method = '$payment_method', trans_id = '$trans_id', trans_date = '$trans_date', cheque_no = '$cheque_no', cheque_date = '$cheque_date', voucher_no = '$voucher_no', voucher_date = '$voucher_date', status = '$status' where po_num = '$po_num'";
		
		$sql2 = "update TIPLDB..insert_po set advance_amount = '$advance_amount', advance_entered_by = '$advance_entered_by', advance_entry_date = '$advance_entry_date', payment_method = '$payment_method', trans_id = '$trans_id', trans_date = '$trans_date', cheque_no = '$cheque_no', cheque_date = '$cheque_date', voucher_no = '$voucher_no', voucher_date = '$voucher_date', status = '$status' where po_num = '$po_num'";
		
		echo $sql1;
		
		echo $sql2;
		
		die;
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		
		//Transanction Complete 
		
	}
}  
?>
