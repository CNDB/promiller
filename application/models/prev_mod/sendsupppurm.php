<?php
class sendsupppurm extends CI_Model  
{  
	function __construct(){   
		parent::__construct(); 
	}
	  			
	public function select_prqit_prno_lvl2(){  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");
		
		return $query;  
	}
		 
	public function po_view_lvl2($selectpr){  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and a.po_num = c.po_num and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr')");
								
		return $query;  
	}
			  
	public function insert_po_sub_lvl2($data){
		$po_num             = $this->input->post("po_num");
		$po_s_no            = $this->input->post("po_s_no");
		$supp_for_pur       = $this->input->post("supp_for_pur");
		$supp_for_pur_by    = $_SESSION['username'];
		$supp_for_pur_date  =  date('Y-m-d H:i:s');
		//$manufact_clearnace = $this->input->post("manufact_clrnce");
		
		$this->db->query("exec tipldb..po_l3_approval");
		
		//MC Required Get
		$sql_mc_req = "select mc_req from tipldb..po_master_table where po_num = '$po_num'";
		$qry_mc_req = $this->db->query($sql_mc_req)->row();
		$manufact_clearnace = $qry_mc_req->mc_req;
		
		//Amendment PO Cycle Byepass -- 21-06-2019
		//Amend No
		$sql_amend_chk = "select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num'";
		$query_amend_chk = $this->db->query($sql_amend_chk)->row();
		$amend_no = $query_amend_chk->amend_no;
		
		//MC Count
		$sql_mc_chk = "select count(*) as count1 from tipldb..po_master_table where mc_req = 'Yes' and mc_given = 'Yes' and po_num = '$po_num'";
		$query_mc_chk = $this->db->query($sql_mc_chk)->row();
		$count1 = $query_mc_chk->count1;
		
		//GE Count
		$sql_ge_chk = "select count(*) as count2 from tipldb..gateentry_item_rec_master 
		where po_num = '$po_num' and status in('Pending For HSN Check By Purchaser','Pending For GRCPT')";
		
		$query_ge_chk = $this->db->query($sql_ge_chk)->row();
		$count2 = $query_ge_chk->count2;
		
		if($amend_no > 0 && $count1 > 0 && $count2 = 0){
			$status = "Pending For Acknowledgement From Supplier";
		} else if($amend_no > 0 && $count1 > 0 && $count2 > 0){
			$status = "Pending For Acknowledgement From Supplier";
		} else if ($manufact_clearnace == 'Yes'){
			$status = "PO Send To Issuance Of Manufacturing Clearanace by planning";
		} else {
			$status = "Pending For Acknowledgement From Supplier";
		}
		//Amendment PO Cycle Byepass -- 21-06-2019
		
		//$status = "Pending For Lead Time Update";
		
		// Trasanction Start
		$this->db->trans_start();
		
		$sql1 = "update TIPLDB..insert_po set supp_for_pur = '$supp_for_pur', supp_for_pur_by = '$supp_for_pur_by', supp_for_pur_date = '$supp_for_pur_date', status = '$status' where po_num = '$po_num'";
		
		$sql2 = "update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'";
		
		$this->db->query($sql1);
		$this->db->query($sql2);
		
		$this->db->trans_complete();
		//Transanction Complete
	}
	
	//Purchasor Details
	public function pur_details($username_table){
		$query = $this->db->query("select * from TIPLDB..employee where emp_active = 'yes' and emp_email like '%$username_table%'");
								
		return $query;
	}
	
	//Mail PO Attachments
	public function insert_attachment($po_num,$NewFileName2){
			  $date_time = date('Y-m-d H:i:s');
			  $username  = $_SESSION['username'];
		  
			  $sql ="insert into tipldb..po_supp_att (po_num, attached_supp_quotes, date_time, attached_by) 
			  values('".$po_num."','".$NewFileName2."','".$date_time."','".$username."')";
			  $query = $this->db->query($sql);
	  }
	  
	  
	 public function get_attachment($po_num){
		$query = $this->db->query("select * from TIPLDB..po_supp_att where po_num = '$po_num' 
		and date_time = (select max(date_time) from tipldb..po_supp_att where po_num = '$po_num')");
								
		return $query;
	 } 		 
}  
?>