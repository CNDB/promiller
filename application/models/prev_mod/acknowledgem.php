<?php
class acknowledgem extends CI_Model{
	  
	function __construct(){   
		parent::__construct();  
	}

	public function select_prqit_prno_lvl2(){  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr){
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		
		return $query; 
	}
	
	public function procesdure_run($itemcode){
		$query = $this->db->query("exec tipldb..pendalcard_pr '$itemcode'");
		
		return $query;    
	}
	
	public function po_view_lvl2($selectpr, $amend_no){  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c 
where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and a.po_num = c.po_num 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr')");  
		
		return $query;  
	}
	
	public function insert_po_sub($data, $uploaded_ack_newname){
		$po_num  = $this->input->post("po_num");
		$po_s_no  = $this->input->post("po_s_no");
		$ack_supp   = $this->input->post("ack_supp");
		$ack_supp_by    = $_SESSION['username'];
		$ack_supp_date  =  date('Y-m-d H:i:s');
		//$dispatch_inst = $this->input->post("dispatch_inst");
		$ack_rmks = $this->input->post("ack_rmks");
		$tc_req = $this->input->post("tc_req");
		$road_permit_req = $this->input->post("road_permit_req");
		
		$this->db->query("exec tipldb..po_l3_approval");
		
		//MC Required Get
		$sql_di_req = "select di_req from tipldb..po_master_table where po_num = '$po_num'";
		$qry_di_req = $this->db->query($sql_di_req)->row();
		$dispatch_inst = $qry_di_req->di_req;
		
		//Amendment PO Cycle Byepass -- 21-06-2019
		//Amend No.
		$sql_amend_chk = "select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num'";
		
		$query_amend_chk = $this->db->query($sql_amend_chk)->row();
		$amend_no = $query_amend_chk->amend_no;
		
		//MC Count
		$sql_mc_chk = "select count(*) as count1 from tipldb..po_master_table where di_req = 'Yes' and di_given = 'Yes' and po_num = '$po_num'";
		$query_mc_chk = $this->db->query($sql_mc_chk)->row();
		$count1 = $query_mc_chk->count1;
		
		//GE Count
		$sql_ge_chk = "select count(*) as count2 from tipldb..gateentry_item_rec_master 
		where po_num = '$po_num' and status in('Pending For HSN Check By Purchaser','Pending For GRCPT')";
		
		$query_ge_chk = $this->db->query($sql_ge_chk)->row();
		$count2 = $query_ge_chk->count2;
		
		if($amend_no > 0 && $count1 > 0 && $count2 = 0){
			if(in_array("yes", $tc_req)) {	
				$status = "Acknowledgement Updated";	
			} else if(in_array("Yes", $road_permit_req)) {	
				$status = "PO Awaited For Road Permit";	
			} else {	
				$status = "Dispatch Instruction Updated";
			}
		} else if($amend_no > 0 && $count1 > 0 && $count2 > 0){
			if(in_array("yes", $tc_req)) {	
				$status = "Acknowledgement Updated";	
			} else if(in_array("Yes", $road_permit_req)) {	
				$status = "PO Awaited For Road Permit";	
			} else {	
				$status = "Dispatch Instruction Updated";
			}
		} else {
			if($dispatch_inst == 'Yes'){
				$status = "PO Send To Issuance Of DI From Planning";	
			} else if(in_array("yes", $tc_req)) {	
				$status = "Acknowledgement Updated";	
			} else if(in_array("Yes", $road_permit_req)) {	
				$status = "PO Awaited For Road Permit";	
			} else {	
				$status = "Dispatch Instruction Updated";
			}
		}
		//Amendment PO Cycle Byepass -- 21-06-2019
		
		/*if(in_array("yes", $dispatch_inst)){
			$status = "PO Send To Issuance Of DI From Planning";	
		} else if(in_array("yes", $tc_req)) {	
			$status = "Acknowledgement Updated";	
		} else if(in_array("Yes", $road_permit_req)) {	
			$status = "PO Awaited For Road Permit";	
		} else {	
			$status = "Dispatch Instruction Updated";
		}*/
		//Code Commented On 21-06-2019 By CNS
		
		//Trasanction Start
		$this->db->trans_start();
		
		$this->db->query("update TIPLDB..insert_po set ack_supp = '$ack_supp', ack_supp_by = '$ack_supp_by', ack_supp_date = '$ack_supp_date', 
		uploaded_ack = '$uploaded_ack_newname', status = '$status' where po_num = '$po_num'");
		
		$this->db->query("update TIPLDB..po_master_table set status = '$status', ack_rmks = '$ack_rmks' where po_num = '$po_num'");
		
		$this->db->trans_complete();
		//Transanction Complete
	}
			 
}  
?>