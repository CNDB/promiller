<?php
class Ppcm extends CI_Model{
	  
      function __construct(){   
         parent::__construct();  
      } 
	  
	  //Last Report Run Date
	  public function last_run_date(){ 
		 $query = $this->db->query("select top 1 convert(date,run_date) as run_date from tipldb..ppc_report_details");   
         return $query;  
	  }
	  
	  //PPC Procedure
	  public function ppc_proc(){
		 $query = $this->db->query("exec tipldb..ppc_report_proc"); 
         return $query;  
	  }
	  
	  //PPC Procedure Test
	  public function ppc_proc_test(){ 
		 $query = $this->db->query("exec tipldb..ppc_report_proc_test");  
         return $query;  
	  }
	   
	  //Inserting Records in ppc_entry_table 
	  public function insert_ppc_entry($data){
		  $pr_num = $this->input->post('pr_num');
		  $item_code = $this->input->post('item_code');
		  $item_desc = $this->input->post('item_desc');
		  $category = $this->input->post('category');
		  $pr_created_by = $this->input->post('pr_created_by');
		  $pr_create_date = $this->input->post('pr_create_date');
		  $need_date = $this->input->post('need_date');
		  $pend_alloc_date = $this->input->post('pend_alloc_date');
		  $remarks = $this->input->post('remarks');
		  $remarks = str_replace("'","",$remarks);
		  $created_by = $_SESSION['username'];
		  $created_date = date('Y-m-d H:i:s');
		  $ques_ans=$this->input->post('ques_ans');
		  $submit_value = $this->input->post('submit');
		  
		  //Transanction Start
		  $this->db->trans_start();
		  
		  if($submit_value == "Ask Question or Answer Question"){
			  $sql_ques_ans = "insert into  tipldb..ppc_entry_table_quesans
			  (pr_num,item_code,item_desc,category,pr_created_by,pr_created_date,
			  pr_need_date,remarks,created_by,created_date)
			  values 
			  ('$pr_num','$item_code','$item_desc','$category','$pr_created_by','$pr_create_date',
			  '$need_date','$ques_ans','$created_by','$created_date')";

		      $qry_ques_ans = $this->db->query($sql_ques_ans);
		  } else {
			  //Checking Previous Entry
			  $sql_check = "select count(*) as abc from tipldb..ppc_entry_table where pr_num = '$pr_num'";
			  
			  $qry_check = $this->db->query($sql_check)->row();
			  
			  $count = $qry_check->abc;
			  
			  if($count > 0){
				  $sql_history = "insert into tipldb..ppc_entry_table_history 
				  (tran_id ,pr_num,item_code,item_desc,category,pr_created_by,pr_created_date,pr_need_date,
				  pend_alloc_date,remarks,created_by,created_date)
				  select * from tipldb..ppc_entry_table where pr_num = '$pr_num'";
	
				  $qry_history = $this->db->query($sql_history);
				  
				  $sql_delete_record = "delete from tipldb..ppc_entry_table where pr_num = '$pr_num'";
				  
				  $qry_delete_record = $this->db->query($sql_delete_record);
			  }
			  
			  //Checking Previous Entry Ends
			  
			  $sql1 = "insert into tipldb..ppc_entry_table
			  (pr_num,item_code,item_desc,category,pr_created_by,pr_created_date,pr_need_date,
			  pend_alloc_date,remarks,created_by,created_date) 
			  values 
			  ('$pr_num','$item_code','$item_desc','$category','$pr_created_by','$pr_create_date','$need_date'
			  ,'$pend_alloc_date','$remarks','$created_by','$created_date')";
			  
			  $qry1 = $this->db->query($sql1);
			  
			  $sql2 = "update tipldb..ppc_report_details set last_fm_date = '".$pend_alloc_date."', last_remarks = '".$remarks."' 
			  where pr_num = '".$pr_num."'";
			  
			  $qry2 = $this->db->query($sql2);
			  
		  }
		  
		  $this->db->trans_complete();
		  //Transanction Complete
	  }
	  //PR DETAILS
	  
	  public function view_prqit_prno_auth($selectpr){   
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..pr_submit_table c, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = c.pr_num  and a.prqit_prno = '$selectpr' 
		 and a.prqit_itemcode = d.iou_itemcode");   
		 
         return $query;  
      }
	  
	  public function procesdure_run($itemcode){
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");	
		 return $query;    
	  }
	  
	  public function procesdure_run_supplier($user_id, $selectpr, $itemcode){ 
	 	 $query = $this->db->query("exec TIPLDB..supplier_details  '1' , '$itemcode'  , '$selectpr'");	
		 return $query;    
	  }
	  
	  public function chat_history_table($selectpr){ 
		$query = $this->db->query("select * from tipldb..pr_submit_table_comment where pr_num = '$selectpr' and enabled = 'YES' order by s_no ASC");
		return $query;  
	  }
	  
	  public function pendal_info($itemcode){ 
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");	
		 return $query; 
	  }
	  
	  public function pendal_info_monthwise($itemcode){ 
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");	
		 return $query; 
	  } 
	  
	  public function pendal_info_last5_trans($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function pendal_info_reorder_lvl_qty($itemcode){
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");	
		 return $query; 
	  } 
	  
	  public function monthofinvtry($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");	
		 return $query;
	  }
	  
	  public function orderinadvnce($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");	
		 return $query;
	   }
	   
	  public function wrkordr_ser($itemcode){ 
	  	$query = $this->db->query("select * from scmdb..pmd_twos_wops_postn a, scmdb..pmd_twoh_wo_headr b where a.wos_item_no = '$itemcode' 
		and a.wos_wo_no = b.woh_wo_no and a.wos_ord_stat_code in('G','E','F','S') and b.woh_so_no in (select so_no from tipldb..master_atac)");
		return $query;
	  }
	  
	  public function lstyrcumnrec1($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function lastfivetrans1($itemcode){ 
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function itemdescmaster($itemcode){ 
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function pendal_master_mnthwsecurryrcons($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function pendal_master_supplier($itemcode){
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function pendal_master_bomdetail($itemcode){ 
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");	
		 return $query; 
	  }
	  
	  public function prerplive($selectpr){  
		$query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr a, tipldb..pr_submit_table b where a.preqm_prno = b.pr_num 
and a.preqm_prno = '$selectpr'");
		return $query;  
	  }
	  
	  //PR DETAILS ENDS
	  
	  //PO DETAILS STARTS
	  
	  public function max_amend_no($po_num){
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		 return $query; 
	  }
	  
	  public function po_view_lvl1($po_num, $amend_no){   
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c 
where a.po_num = '$po_num' and a.po_num = b.pomas_pono and a.po_num = c.po_num 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num')");  
         return $query;  
      }
	  
	  //PO DETAILS ENDS
	  //INSERTING PPC PO WISE RECORDS
	  public function insert_ppc_po_entry($data){
		  $select_pr = $this->input->post('select_pr');
		  $pend_alloc_date = $this->input->post('pend_alloc_date');
		  $remarks = $this->input->post('remarks');
		  $remarks = str_replace("'","",$remarks);
		  $created_by = $_SESSION['username'];
		  $created_date = date('Y-m-d H:i:s');
		  
		  //Transanction Start
		  $this->db->trans_start();
		  
		  $pr_count = count($select_pr);
		  
		  for($i=0;$i<$pr_count;$i++){
		  
		  //Checking Previous Entry
		  $sql_check = "select count(*) as abc from tipldb..ppc_entry_table where pr_num = '$select_pr[$i]'";
		  
		  $qry_check = $this->db->query($sql_check)->row();
		  
		  $count = $qry_check->abc;
		  
		  if($count > 0){
			  $sql_history = "insert into tipldb..ppc_entry_table_history 
			  (tran_id ,pr_num,item_code,item_desc,category,pr_created_by,pr_created_date,pr_need_date,pend_alloc_date,remarks,created_by,created_date)
			  select * from tipldb..ppc_entry_table where pr_num = '$select_pr[$i]'";

			  $qry_history = $this->db->query($sql_history);
			  
			  $sql_delete_record = "delete from tipldb..ppc_entry_table where pr_num = '$select_pr[$i]'";
			  
			  $qry_delete_record = $this->db->query($sql_delete_record);
		  }
		  //Checking Previous Entry Ends
		  $sql_pr_det = "select item_code,itm_desc,category,created_by,create_date,need_date from tipldb..pr_submit_table where pr_num = '$select_pr[$i]'";
		  
		  $qry_pr_det = $this->db->query($sql_pr_det)->row();
		  
		  $item_code      = $qry_pr_det->item_code;
		  $item_desc      = $qry_pr_det->itm_desc;
		  $category       = $qry_pr_det->category;
		  $pr_created_by  = $qry_pr_det->created_by;
		  $pr_create_date = $qry_pr_det->create_date;
		  $need_date      = $qry_pr_det->need_date;
		  
		  $sql1 = "insert into tipldb..ppc_entry_table
		  (pr_num,item_code,item_desc,category,pr_created_by,pr_created_date,pr_need_date,pend_alloc_date,remarks,created_by,created_date) 
		  values 
		  ('$select_pr[$i]','$item_code','$item_desc','$category','$pr_created_by','$pr_create_date','$need_date'
		  ,'$pend_alloc_date','$remarks','$created_by','$created_date')";
		  
		  $qry1 = $this->db->query($sql1);
		  
		  $sql2 = "update tipldb..ppc_report_details set last_fm_date = '".$pend_alloc_date."', last_remarks = '".$remarks."' 
		  where pr_num = '".$select_pr[$i]."'";
		  
		  $qry2 = $this->db->query($sql2);
		  
		  }
		  
		  $this->db->trans_complete();
		  //Transanction Complete
	  }
	  
	  public function pr_details($pr_num){
		  $query = $this->db->query("select * from tipldb..pr_submit_table where pr_num = '$pr_num'");
		  return $query;   
	  }
	  
	  public function fetch_lvl3_app_req(){ 
         $query = $this->db->query("exec tipldb..po_l3_approval");  
         return $query;  
     }
}  
?>