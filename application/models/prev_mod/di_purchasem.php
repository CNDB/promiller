<?php
class di_purchasem extends CI_Model{
	  
	function __construct(){   
		parent::__construct();  
	}  
	
	public function max_amend_no($po_num){
		 
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		 return $query; 
	 }
	 
	 public function view_po_details($po_num, $amend_no){  
	 
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$po_num' and a.po_num = b.pomas_pono and a.po_num = c.po_num and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num') order by a.po_line_no");  

         return $query;  
     }
	 
	 public function insert_di_pur($data){  
	 
	 	$po_num = $this->input->post('po_num');
		
		$remarks = $this->input->post('remarks');
		
		$remarks = str_replace("'","",$remarks);
		
		$created_by = $_SESSION['username'];
		
		$created_date = date("Y-m-d H:i:s");
		
		$sql_rp = "select road_permit_req from tipldb..po_master_table where po_num = '".$po_num."'";
		$qry_rp = $this->db->query($sql_rp);
		
		foreach($qry_rp->result() as $row){
			$rp_req = $row->road_permit_req;
		}
		
		if($rp_req == 'Yes'){
			$status = "PO Awaited For Road Permit";
		} else {
			$status = "Dispatch Instruction Updated";
		}
		
		
		//Transaction Start
		$this->db->trans_start();
	 
		$sql = "update tipldb..po_master_table set di_pur_rmks = '$remarks', 
		di_pur_by = '$created_by', di_pur_date = '$created_date', status = '$status', di_given = 'Yes' where po_num = '$po_num'";  
		
		$query = $this->db->query($sql);  
		
		$this->db->trans_complete();
		//Transanction Complete
	}
	
	//Purchasor Details
	public function pur_details($username_table)
	{
		$query = $this->db->query("select * from TIPLDB..employee where emp_active = 'yes' and emp_email like '%$username_table%'");
								
		return $query;
	}
	   
}  
?>