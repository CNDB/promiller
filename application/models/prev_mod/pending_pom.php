<?php
class Pending_pom extends CI_Model{  
      function __construct(){   
         parent::__construct();  
      }
	  
	  /******** PENDING PURCHASE ORDERS **************/ 
	  public function capital_goods_draft()  
      {

		$query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%'"); 

         return $query;  
      }
	  
	  public function sensors_draft()  
      {  

 		$query = $this->db->query("SELECT COUNT(*) as count FROM (
							select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
							and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
							where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono)
							union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
							and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
							where poprq_prno = pr_num and category = 'SENSORS')) x "); 

         return $query;  
      }
	  
	  public function security_draft()  
      { 

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SECURITY')) x "); 

         return $query;  
      }
	  
	  public function instrumentation_draft()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'INSTRUMENTATION')) x "); 

         return $query;  
      }
	  
	  public function tools_draft()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'TOOLS')) x ");  

         return $query;  
      }
	  
	  public function consumable_draft()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')) x ");   

         return $query;  
      }
	  
	   public function service_draft()  
      {

		$query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'"); 

         return $query;  
      }
	  
	  
      public function without_category_draft()  
      {  

		$query = $this->db->query("select count(*) as count 
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'DF' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' 
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
													)
							and pomas_pono NOT LIKE 'CGPO%' ");  

         return $query;  
      }
	  
	   public function packing_draft()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PACKING')) x ");   

         return $query;  
      }
	  
	  public function avazonic_draft()  
      {  
         

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'AVAZONIC')) x ");   

         return $query;  
      }
	  
	  public function nonproduction_draft()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')) x ");   

         return $query;  
      }
	  
	  public function it_draft()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'IT')) x ");   

         return $query;  
      }
	  
	  public function all_draft_po()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..PO_MASTER_TABLE)
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po)");  

         return $query;  
      }
	  
	   
	  
	  //Fresh Purchase Orders
	  
	  public function capital_goods_fresh()  
      {  
		$query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%'");   

         return $query;  
      }
	  
	  public function sensors_fresh()  
      {  
         

		// query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SENSORS')) x "); 

  

         return $query;  
      }
	  
	  public function security_fresh()  
      { 

		// query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SECURITY')
									) x ");  

 

         return $query;  
      }
	  
	  public function instrumentation_fresh()  
      {  

		// query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'INSTRUMENTATION')
									) x ");      

         return $query;  
      }
	  
	   public function tools_fresh()  
      {  

 		//query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'TOOLS')
									) x ");   

         return $query;  
      }
	  
	   public function consumable_fresh()  
      { 

		// query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')
									) x ");     

         return $query;  
      }
	  
	   public function service_fresh()  
      {

		$query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'"); 

         return $query;  
      }
	  
	  
	  public function without_category_fresh()  
      { 

 		$query = $this->db->query("select count(*) as count 
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'FR' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' 
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
													)
							and pomas_pono NOT LIKE 'CGPO%' ");



         return $query;  
      }
	  
	  public function packing_fresh()  
      {  
		// query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PACKING')
									) x ");     

         return $query;  
      }
	  
	   public function avazonic_fresh()  
      {

		$query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'AVAZONIC')
									) x "); 

         return $query;  
      }
	  
	  
	  public function nonproduction_fresh()  
      { 
 		$query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')
									) x  ");



         return $query;  
      }
	  
	  public function it_fresh()  
      { 
 		$query = $this->db->query("SELECT COUNT(*) as count
									FROM
									(
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'IT')
									) x  ");



         return $query;  
      }
	  
	  
	  public function all_fresh_po()
	  {
		  $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po)");
		  
		  return $query;
	  }
	  
	  
	 
	 //Open Purchase Orders
	 
	  public function capital_goods_open()  
      {
		 $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%'");   

         return $query;  
      }
	  
	  public function sensors_open()  
      {   

		// query modified for showing no category cases..
	
		 $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SENSORS')) x ");
									
         return $query;  
      }
	  
	  public function security_open()  
      {  
		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SECURITY')) x ");

         return $query;  
      }
	  
	  public function instrumentation_open()  
      {   

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'INSTRUMENTATION')) x ");

         return $query;  
      }
	  
	   public function tools_open()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'TOOLS')) x "); 

         return $query;  
      }
	  
	  public function consumable_open()  
      {    

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')) x "); 

         return $query;  
      }
	  
	  public function service_open()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function without_category_open()  
      { 

 		$query = $this->db->query("select COUNT(*) as count 
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'OP' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' 
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
													)
							and pomas_pono NOT LIKE 'CGPO%'");  



         return $query;  
      }
	  
	  public function packing_open()  
      {    

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PACKING')) x "); 

         return $query;  
      }
	  
	  public function avazonic_open()  
      {  
         $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'AVAZONIC')) x ");  

         return $query;  
      }
	  
	  public function nonproduction_open()  
      {   

 		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')) x ");  



         return $query;  
      }
	  
	  public function it_open()  
      {    

 		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'IT')) x ");  



         return $query;  
      }
	  
	  public function all_open_po()
	  {
		  $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po)");
		  
		  return $query;
	  }  
	  
	  
	 //Under Amendment
	 
	 public function capital_goods_amendement()  
      {  
		$query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%'");    

         return $query;  
      }
	  
	  public function sensors_amendement()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SENSORS')) x "); 

         return $query;  
      }
	  
	  public function security_amendement()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SECURITY')) x ");   

         return $query;  
      }
	  
	  public function instrumentation_amendement()  
      { 

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'INSTRUMENTATION')) x "); 

         return $query;  
      }
	  
	  public function tools_amendement()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'TOOLS')) x ");  

         return $query;  
      }
	  
	  public function consumable_amendement()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')) x ");

         return $query;  
      }
	  
	  public function service_amendement()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function without_category_amendement()  
      {   

				 $query = $this->db->query("select COUNT(*) as count 
				from scmdb..po_pomas_pur_order_hdr a
				where pomas_podocstatus = 'AM' 
				and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
				and pomas_pono NOT LIKE 'SRVPO%' 
				and a.pomas_pono not in (select poprq_pono 
										from scmdb..po_poprq_poprcovg_detail d 
										where d.poprq_pono = a.pomas_pono 
										and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
										)
				and pomas_pono NOT LIKE 'CGPO%'");  

         return $query;  
      }
	  
	  public function packing_amendement()  
      {    

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PACKING')) x ");

         return $query;  
      }
	  
	  public function avazonic_amendement()  
      {  
         $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'AVAZONIC')) x ");  

         return $query;  
      }
	  
	  public function nonproduction_amendement()  
      {  

				 $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')) x ");  

         return $query;  
      }
	  
	  public function it_amendement()  
      {   

				 $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'IT')) x ");  

         return $query;  
      }
	  
	  public function all_uamend_po()
	  {
		  $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po)");
		  
		  return $query;
	  }
	  
	  
	   //Total Counting of PO
	   
	   
	  public function all_capitalgoods_po()  
      {  
           $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM') 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%'"); 

         return $query;  
      }
	  
	  
	  public function all_sensors_po()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SENSORS')) x ");  

         return $query;  
      }
	  
	  
	  public function all_security_po()  
      {  
		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'SECURITY')) x ");   

         return $query;  
      }
	  
	  public function all_instrumentation_po()  
      {  

		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'INSTRUMENTATION')) x "); 

         return $query;  
      }
	  
	  public function all_tools_po()  
      {  
		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'TOOLS')) x "); 

         return $query;  
      }
	  
	  public function all_consumable_po()  
      {  
		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')) x ");  

         return $query;  
      }
	  
	     public function all_service_po()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM') 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'");    

         return $query;  
      }
	  
	  public function all_nocat_po()  
      {  
			$query = $this->db->query("select COUNT(*) as count 
						from scmdb..po_pomas_pur_order_hdr a
						where pomas_podocstatus in('DF','FR','OP','AM') 
						and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
						and pomas_pono NOT LIKE 'SRVPO%' and pomas_pono NOT LIKE 'CGPO%'
						and a.pomas_pono not in (select poprq_pono 
												from scmdb..po_poprq_poprcovg_detail d 
												where d.poprq_pono = a.pomas_pono 
												and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  public function all_packing_po()  
      {  
		$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'PACKING')) x ");  

         return $query;  
      }
	  
	     public function all_avazonic_po()  
      {  
         $query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'AVAZONIC')) x");    

         return $query;  
      }
	  
	  public function all_nonproduction_po()  
      {  
        /* $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM') 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%'");*/  


			$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')) x");  

         return $query;  
      }
	  
	  public function all_it_po()  
      {  
        /* $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM') 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%'");*/  


			$query = $this->db->query("SELECT COUNT(*) as count FROM (
									select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
									where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono)
									union select pomas_pono from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
									and pomas_pono In (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
									where poprq_prno = pr_num and category = 'IT')) x");  

         return $query;  
      }
	  
	  public function total_po()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','DF','OP','AM')
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po)");  

         return $query;  
      }
	  
	   
	  /******** PENDING PURCHASE ORDERS VIEW **************/
	  //Draft PO View
	  
	  public function capital_goods_draft_new()  
      {

	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%'"); 

         return $query;  
      }
	  
	  public function sensors_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono)");*/  
									
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SENSORS') order by pomas_pono");   

         return $query;  
      }
	  
	  public function security_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono)");*/  

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SECURITY') order by pomas_pono");   

         return $query;  
      }
	  
	  public function instrumentation_draft_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono)"); */ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'INSTRUMENTATION') order by pomas_pono"); 

         return $query;  
      }
	  
	   public function tools_draft_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'TOOLS') order by pomas_pono"); 

         return $query;  
      }
	  
	  public function consumable_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)"); */ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES') order by pomas_pono"); 

         return $query;  
      }
	  
	  public function service_draft_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'"); 

         return $query;  
      }
	  
	  
      public function without_category_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%'"); */ 


$query = $this->db->query("select * 
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'DF' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' and pomas_pono NOT LIKE 'CGPO%'
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))"); 

         return $query;  
      }
	  
	  public function packing_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)"); */ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PACKING') order by pomas_pono"); 

         return $query;  
      }
	  
	  public function avazonic_draft_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'AVAZONIC') order by pomas_pono"); 

         return $query;  
      }
	  
	  
      public function nonproduction_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%'"); */ 


$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES') order by pomas_pono"); 

         return $query;  
      }
	  
	  public function it_draft_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%'"); */ 


$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'IT') order by pomas_pono"); 

         return $query;  
      }
	  
	  public function all_draft_po_new()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'DF'
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..PO_MASTER_TABLE)
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po)");  

         return $query;  
      }
	  
	  
	  
	  
	  //Fresh PO View
	  
	   public function capital_goods_fresh_new()
	  {
$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%' order by pomas_pono"); 

         return $query; 
	  }
	  
	  public function sensors_fresh_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) order by pomas_pono");*/ 

	// query modified for showing no category cases..
	
		 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SENSORS')
order by pomas_pono");   

         return $query;  
      }
	  
	  public function security_fresh_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) order by pomas_pono"); */ 


	// query modified for showing no category cases..
	
		 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SECURITY')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function instrumentation_fresh_new()  
      {  
  /*       $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) order by pomas_pono"); */

// query modified for showing no category cases..
	
		 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'INSTRUMENTATION')
order by pomas_pono");  

         return $query;  
      }
	  
	   public function tools_fresh_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)");  */


	// query modified for showing no category cases..
	
		 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'TOOLS')
order by pomas_pono");  
         return $query;  
      }
	  
	  public function consumable_fresh_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)"); */ 

// query modified for showing no category cases..
	
		 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function service_fresh_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'"); 

         	return $query;  
      }	  
	  
	  
	  public function without_category_fresh_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono");*/


 $query = $this->db->query("select *
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'FR' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' and pomas_pono NOT LIKE 'CGPO%'
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table)) order by pomas_pono");  

         return $query;  
      }
	  
	  public function packing_fresh_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)"); */ 

// query modified for showing no category cases..
	
		 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PACKING')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function avazonic_fresh_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'AVAZONIC')
order by pomas_pono"); 

         	return $query;  
      }	  
	  
	  
	  public function nonproduction_fresh_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono");*/


 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function it_fresh_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono");*/


 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono) 

union 
select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'IT')
order by pomas_pono");  

         return $query;  
      }
	  
	  
	  public function all_fresh_po_new()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'FR'
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po) order by pomas_pono");  

         return $query;  
      }
	  
	  
	  
	 //Open Purchase Orders
	 
	  public function capital_goods_open_new()  
      {  
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%' order by pomas_pono");   

         return $query;  
      }
	  
	  public function sensors_open_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) order by pomas_pono");*/

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SENSORS')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function security_open_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) order by pomas_pono"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SECURITY')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function instrumentation_open_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'INSTRUMENTATION')
order by pomas_pono"); 

         return $query;  
      }
	  
	  
	   public function tools_open_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono)"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'TOOLS')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function consumable_open_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)");*/  

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function service_open_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%' order by pomas_pono"); 

         return $query;  
      }
	  
	  public function without_category_open_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono"); */ 

 $query = $this->db->query("select *
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'OP' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' and pomas_pono NOT LIKE 'CGPO%'
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table)) order by pomas_pono");

         return $query;  
      }
	  
	  public function packing_open_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono)");*/  

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono ) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PACKING')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function avazonic_open_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'AVAZONIC')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function nonproduction_open_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono"); */ 

 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')
order by pomas_pono");

         return $query;  
      }
	  
	  public function it_open_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono"); */ 

 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'IT')
order by pomas_pono");

         return $query;  
      }
	  
	   public function all_open_po_new()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'OP' 
		 AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po) order by pomas_pono");  

         return $query;  
      }
	  
	  //Under Amendment View
	 
	 public function capital_goods_amendement_new()  
      {  
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
		and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%' order by pomas_pono"); 

         return $query;  
      }
	  
	  public function sensors_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SENSORS')
order by pomas_pono");  

         return $query;  
      }
	  
	  public function security_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SECURITY')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function instrumentation_amendement_new()  
      {  
       /*  $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) order by pomas_pono");*/

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'INSTRUMENTATION')
order by pomas_pono");  

         return $query;  
      }
	  
	   public function tools_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) order by pomas_pono"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'TOOLS')
order by pomas_pono"); 

         return $query;  
      }
	  
	   public function consumable_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function service_amendement_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'"); 

         return $query;  
      }
	  
	  public function without_category_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono");*/ 

 $query = $this->db->query("select *
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus = 'AM' 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' and pomas_pono NOT LIKE 'CGPO%'
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table)) order by pomas_pono"); 

         return $query;  
      }
	  
	  public function packing_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PACKING')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function avazonic_amendement_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'AVAZONIC')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function nonproduction_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono");*/ 

 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function it_amendement_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono");*/ 

 $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM'
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'IT')
order by pomas_pono"); 

         return $query;  
      }
	  
	   public function all_uamend_po_new()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus = 'AM' 
		 AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po) order by pomas_pono");  

         return $query;  
      }
	  
	  
	  
	  //Total POS  
	  public function all_capitalgoods_po_new()  
      {   

		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM')
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'CGPO%' order by pomas_pono");

         return $query;  
      }
	  
	  public function all_sensors_po_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) order by pomas_pono"); */ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SENSORS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SENSORS')
order by pomas_pono"); 

         return $query;  
      } 
	  
	  public function all_security_po_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) order by pomas_pono");*/

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'SECURITY' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'SECURITY')
order by pomas_pono");   

         return $query;  
      }  
	 
	  
	  public function all_instrumentation_po_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) order by pomas_pono"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'INSTRUMENTATION' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'INSTRUMENTATION')
order by pomas_pono"); 

         return $query;  
      }
	  
	    public function all_tools_po_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) order by pomas_pono");*/ 


$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'TOOLS' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'TOOLS')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function all_consumable_po_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PRODUCTION CONSUMABLES')
order by pomas_pono"); 

         return $query;  
      }
	  
	  
	  public function all_service_po_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM')
			and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono LIKE 'SRVPO%'");

         return $query;  
      }	  
	  
	  public function all_nocat_po_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM')
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono"); */

$query = $this->db->query("select *
							from scmdb..po_pomas_pur_order_hdr a
							where pomas_podocstatus in('DF','FR','OP','AM') 
							and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) 
							and pomas_pono NOT LIKE 'SRVPO%' and pomas_pono NOT LIKE 'CGPO%'
							and a.pomas_pono not in (select poprq_pono 
													from scmdb..po_poprq_poprcovg_detail d 
													where d.poprq_pono = a.pomas_pono 
													and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table)) order by pomas_pono"); 

         return $query;  
      }  
	  
	  public function all_packing_po_new()  
      {  
        /* $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF') 
and pomas_pono = (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) order by pomas_pono");*/ 

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'PACKING' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'PACKING')
order by pomas_pono"); 

         return $query;  
      }
	  
	  
	   public function all_avazonic_po_new()  
      {
	  		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'AVAZONIC' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'AVAZONIC')
order by pomas_pono");

         return $query;  
      }	  
	  
	  public function all_nonproduction_po_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM')
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'NON PRODUCTION CONSUMABLES' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'NON PRODUCTION CONSUMABLES')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function all_it_po_new()  
      {  
         /*$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('DF','FR','OP','AM')
and pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = pomas_pono) and pomas_pono NOT LIKE 'SRVPO%' 
and pomas_pono NOT LIKE 'CGPO%' order by pomas_pono"); */

$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct po_num from TIPLDB..insert_po a, tipldb..pr_submit_table b 
where a.po_ipr_no = b.pr_num and b.category = 'IT' and a.po_num = pomas_pono) 

union 

select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','OP','AM','DF')
and pomas_pono in (select distinct poprq_pono from SCMDB..po_poprq_poprcovg_detail a,TIPLDB..pr_submit_table b 
where poprq_prno = pr_num and category = 'IT')
order by pomas_pono"); 

         return $query;  
      }
	  
	  public function total_po_new()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus in('FR','DF','OP','AM')
AND POMAS_PONO NOT IN(SELECT PO_NUM FROM TIPLDB..no_category_po) order by pomas_pono");  

         return $query;  
      } 
	  
	  
	  /******** PENDING PURCHASE REQUEST QUERIES **************/
	  
	  //ERP Fresh Live Not Created PR
	  
	  public function capitalgoods_erp_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno not in(select pr_num from tipldb..pr_submit_table)");  

         return $query;  
      }
	  
	  public function total_erp_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 3) not in('RPR') 
and preqm_prno not in(select pr_num from tipldb..pr_submit_table)");  

         return $query;  
      }
	  
	  // ERP Created and Live Created PR
	  
	  public function capitalgoods_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table)");  

         return $query;  
      }
	  
	  public function sensors_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS')");  

         return $query;  
      }
	  
	  public function security_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY')");  

         return $query;  
      }
	  
	  public function instrumentation_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION')");  

         return $query;  
      }
	  
	  public function tools_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS')");  

         return $query;  
      }
	  
	  public function prodcon_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES')");  

         return $query;  
      }
	  
	  public function packing_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING')");  

         return $query;  
      }
	  
	  public function avazonic_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC')");  

         return $query;  
      }
	  
	  public function nonprodcon_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES')");  

         return $query;  
      }
	  
	  public function it_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT')");  

         return $query;  
      }
	  
	  public function others_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS')");  

         return $query;  
      }
	  
	  public function total_erplive_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table)");  

         return $query;  
      }
	  
	  // level 1 pending pr
	  
	  public function capitalgoods_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function sensors_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function security_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function instrumentation_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function tools_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function prodcon_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function packing_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function avazonic_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function it_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function others_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function total_lvl1_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  // LEVEL1 DISAPPROVED PR
	  
	  public function capitalgoods_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Disapproved At Planning Level1' 
and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function sensors_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function security_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function instrumentation_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function tools_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function prodcon_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function packing_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function avazonic_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function it_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function others_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function total_lvl1_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  // level 2 pending pr
	  
	  public function capitalgoods_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Approved At Planning Level1' 
and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function sensors_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function security_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function instrumentation_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function tools_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function prodcon_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function packing_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function avazonic_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function it_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function others_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function total_lvl2_pending()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  // LEVEL2 DISAPPROVED PR
	  
	  public function capitalgoods_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Disapproved At Planning Level2' 
and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function sensors_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function security_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function instrumentation_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function tools_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function prodcon_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function packing_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function avazonic_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function it_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function others_lvl2_disapprove()  


      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' 
and pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  public function total_lvl2_disapprove()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Disapproved At Planning Level2' and level2_approval_req = 'Yes')");  

         return $query;  
      }
	  
	  //Live Authorized ERP Not Authorized PR
	  
	  public function capitalgoods_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function sensors_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function security_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function instrumentation_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function tools_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function prodcon_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function packing_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function avazonic_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function nonprodcon_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function it_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function others_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' and pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  public function total_erpnotauth_fresh()  
      {  
         $query = $this->db->query("select count(*) as count from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'Pending For PR Authorize In ERP')");  

         return $query;  
      }
	  
	  //ERP authorized PR PO not created
	  
	  //pr po not created table creation query
	  
	  public function select_prqit_prno2()  
      {  
         $query = $this->db->query("select DATEDIFF(dd,preqm_authdate,GETDATE()) as pr_age,CONVERT(date,prqit_needdate)as pr_needDate,
					CONVERT(date,preqm_createddate) as pr_createdDate,loi_itemdesc,a.*,b.* 
					from SCMDB..prq_preqm_pur_reqst_hdr a, SCMDB..prq_prqit_item_detail b , SCMDB..itm_loi_itemhdr c
					where a.preqm_status='AU'
					and a.preqm_prno = b.prqit_prno
					and b.prqit_itemcode = c.loi_itemcode
					and b.prqit_balqty > 0
					and a.preqm_prno not in('IPR-0169-17')
					and a.preqm_prno in (select pr_num from tipldb..pr_submit_table where pr_status not in('PR Disapproved At Purchase Level'))
					order by a.preqm_prno");
					  
         return $query;  
      }
	  
	  public function capitalgoods_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where preqm_prno like '%CGP%'");  

         return $query;  
      }
	  
	  public function sensors_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'SENSORS'");  

         return $query;  
      }
	  
	  public function security_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'SECURITY'");  

         return $query;  
      }
	  
	  public function instrumentation_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'INSTRUMENTATION'");  

         return $query;  
      }
	  
	  public function tools_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'TOOLS'");  

         return $query;  
      }
	  
	  public function prodcon_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'PRODUCTION CONSUMABLES'");  

         return $query;  
      }
	  
	  public function packing_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'PACKING'");  

         return $query;  
      }
	  
	  public function avazonic_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'AVAZONIC'");  

         return $query;  
      }
	  
	  public function nonprodcon_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'NON PRODUCTION CONSUMABLE'");  

         return $query;  
      }
	  
	  public function it_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'IT'");  

         return $query;  
      }
	  
	  public function others_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report where category = 'OTHERS'");  

         return $query;  
      }
	  
	  public function total_prponotcreated_auth()  
      {  
         $query = $this->db->query("select count(*) as count from tipldb..pr_po_report");  

         return $query;  
      }
	  
	  //Total PR
	  
	  public function all_capitalgoods_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR') and preqm_prno not in(select pr_num from tipldb..pr_submit_table)
union
select preqm_prno from tipldb..pr_po_report where preqm_prno like 'CGPR%'
)x");  

         return $query;  
      }
	  
	  public function all_sensors_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'SENSORS'
union
select preqm_prno from tipldb..pr_po_report where category = 'SENSORS'
)x");  

         return $query;  
      }
	  
	  public function all_security_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'SECURITY'
union
select preqm_prno from tipldb..pr_po_report where category = 'SECURITY'
)x");  

         return $query;  
      }
	  
	  public function all_instrumentation_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'INSTRUMENTATION'
union
select preqm_prno from tipldb..pr_po_report where category = 'INSTRUMENTATION'
)x");  

         return $query;  
      }
	  
	  public function all_tools_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'TOOLS'
union
select preqm_prno from tipldb..pr_po_report where category = 'TOOLS'
)x");  

         return $query;  
      }
	  
	  public function all_prodcon_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'PRODUCTION CONSUMABLES'
union
select preqm_prno from tipldb..pr_po_report where category = 'PRODUCTION CONSUMABLES'
)x");  

         return $query;  
      }
	  
	  public function all_packing_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'PACKING'
union
select preqm_prno from tipldb..pr_po_report where category = 'PACKING'
)x");  

         return $query;  
      }
	  
	  public function all_avazonic_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'AVAZONIC'
union
select preqm_prno from tipldb..pr_po_report where category = 'AVAZONIC'
)x");  

         return $query;  
      }
	  
	  public function all_nonprodcon_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'NON PRODUCTION CONSUMABLES'
union
select preqm_prno from tipldb..pr_po_report where category = 'NON PRODUCTION CONSUMABLES'
)x");  

         return $query;  
      }
	  
	  public function all_it_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'IT'
union
select preqm_prno from tipldb..pr_po_report where category = 'IT'
)x");  

         return $query;  
      }
	  
	  public function all_others_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'OTHERS'
union
select preqm_prno from tipldb..pr_po_report where category = 'OTHERS'
)x");  

         return $query;  
      }
	  
	  public function all_total_pr()  
      {  
         $query = $this->db->query("select COUNT(*) as count from (
select preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' and SUBSTRING(preqm_prno, 1,3) != 'RPR'
and preqm_prno not in(select pr_num from TIPLDB..pr_submit_table)
union
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR'
union
select preqm_prno from TIPLDB..pr_po_report
)x");  

         return $query;  
      }
	  
	  //Purchase Request Queries
	  
	  /******** PENDING PURCHASE REQUEST QUERIES VIEW **************/
	  
	  	//FRESH PR
	  
	  public function capitalgoods_erp_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno not in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_erp_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 3) not in('RPR') 
and preqm_prno not in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  //LEVEL 1 PENDING PR
	  
	  public function capitalgoods_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  public function sensors_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function security_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function instrumentation_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function tools_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function prodcon_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function packing_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function avazonic_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function it_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function others_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' and 
pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_lvl1_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'Pending For PR Authorize' and level1_approval_req = 'Yes') 
order by preqm_prno");  

         return $query;  
      }
	  
	  //LEVEL 1 DISAPPROVED PR
	  
	  public function capitalgoods_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  public function sensors_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function security_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function instrumentation_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function tools_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function prodcon_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function packing_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function avazonic_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function it_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function others_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' 
and pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_lvl1_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Disapproved At Planning Level1' and level1_approval_req = 'Yes') 
order by preqm_prno");  

         return $query;  
      }
	  
	  //LEVEL 2 PENDING
	  
	  	  public function capitalgoods_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  public function sensors_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function security_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function instrumentation_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function tools_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function prodcon_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function packing_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function avazonic_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function it_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function others_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' 
and pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_lvl2_pending_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Approved At Planning Level1' and level2_approval_req = 'Yes')
order by preqm_prno");  

         return $query;  
      }
	  
	  //LEVEL2 DISAPPROVED
	  
	  public function capitalgoods_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  public function sensors_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function security_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function instrumentation_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function tools_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function prodcon_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function packing_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function avazonic_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function nonprodcon_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function it_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function others_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' 
and pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes') order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_lvl2_disapprove_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'PR Disapproved At Planning Level2' and level1_approval_req = 'Yes')
order by preqm_prno");  

         return $query;  
      }
	  
	  //PR PENDING FOR ERP AUTHORIZATION
	  
	  public function capitalgoods_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR')
and preqm_prno in(select pr_num from tipldb..pr_submit_table) order by preqm_prno");  

         return $query;  
      }
	  
	  public function sensors_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SENSORS' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function security_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'SECURITY' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function instrumentation_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'INSTRUMENTATION' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function tools_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'TOOLS' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function prodcon_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PRODUCTION CONSUMABLES' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function packing_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'PACKING' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function avazonic_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'AVAZONIC' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function nonprodcon_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'NON PRODUCTION CONSUMABLES' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function it_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'IT' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function others_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where category = 'OTHERS' and pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_erpnotauth_fresh_new()  
      {  
         $query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR'
and preqm_prno in(select pr_num from tipldb..pr_submit_table where pr_status = 'Pending For PR Authorize In ERP')
order by preqm_prno");  

         return $query;  
      }
	  
	  //ERP AUTH PR PO NOT CREATED
	  
	  public function capitalgoods_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where preqm_prno like '%CGP%' order by preqm_prno");  

         return $query;  
      }
	  
	  public function sensors_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'SENSORS' order by preqm_prno");  

         return $query;  
      }
	  
	  public function security_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'SECURITY' order by preqm_prno");  

         return $query;  
      }
	  
	  public function instrumentation_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'INSTRUMENTATION' order by preqm_prno");  

         return $query;  
      }
	  
	  public function tools_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'TOOLS' order by preqm_prno");  

         return $query;  
      }
	  
	  public function prodcon_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'PRODUCTION CONSUMABLES' order by preqm_prno");  

         return $query;  
      }
	  
	  public function packing_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'PACKING' order by preqm_prno");  

         return $query;  
      }
	  
	  public function avazonic_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'AVAZONIC' order by preqm_prno");  

         return $query;  
      }
	  
	  public function nonprodcon_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'NON PRODUCTION CONSUMABLES' order by preqm_prno");  

         return $query;  
      }
	  
	  public function it_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'IT' order by preqm_prno");  

         return $query;  
      }
	  
	  public function others_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report where category = 'OTHERS' order by preqm_prno");  

         return $query;  
      }
	  
	  public function total_prponotcreated_auth_new()  
      {  
         $query = $this->db->query("select * from tipldb..pr_po_report order by preqm_prno");  

         return $query;  
      }
	  
	  //TOTAL PR
	  
	  public function all_capitalgoods_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' 
and SUBSTRING(preqm_prno, 1, 4) in('CGPR') and preqm_prno not in(select pr_num from tipldb..pr_submit_table)
union
select preqm_prno from tipldb..pr_po_report where preqm_prno like 'CGPR%' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_sensors_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'SENSORS'
union
select preqm_prno from tipldb..pr_po_report where category = 'SENSORS' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_security_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'SECURITY'
union
select preqm_prno from tipldb..pr_po_report where category = 'SECURITY' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_instrumentation_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'INSTRUMENTATION'
union
select preqm_prno from tipldb..pr_po_report where category = 'INSTRUMENTATION' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_tools_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'TOOLS'
union
select preqm_prno from tipldb..pr_po_report where category = 'TOOLS' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_prodcon_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'PRODUCTION CONSUMABLES'
union
select preqm_prno from tipldb..pr_po_report where category = 'PRODUCTION CONSUMABLES' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_packing_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'PACKING'
union
select preqm_prno from tipldb..pr_po_report where category = 'PACKING' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_avazonic_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'AVAZONIC'
union
select preqm_prno from tipldb..pr_po_report where category = 'AVAZONIC' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_nonprodcon_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'NON PRODUCTION CONSUMABLES'
union
select preqm_prno from tipldb..pr_po_report where category = 'NON PRODUCTION CONSUMABLES' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_it_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'IT'
union
select preqm_prno from tipldb..pr_po_report where category = 'IT' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_others_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR' and category = 'OTHERS'
union
select preqm_prno from tipldb..pr_po_report where category = 'OTHERS' order by preqm_prno");  

         return $query;  
      }
	  
	  public function all_total_pr_new()  
      {  
         $query = $this->db->query("select preqm_prno from scmdb..prq_preqm_pur_reqst_hdr where preqm_status = 'FR' and SUBSTRING(preqm_prno, 1,3) != 'RPR'
and preqm_prno not in(select pr_num from TIPLDB..pr_submit_table)
union
select pr_num from TIPLDB..pr_submit_table a, scmdb..prq_preqm_pur_reqst_hdr b where 
a.pr_num = b.preqm_prno and b.preqm_status = 'FR'
union
select preqm_prno from TIPLDB..pr_po_report order by preqm_prno");  

         return $query;  
      }
	  
	  
	  /******** PENDING PURCHASE ORDER NOT LIVE QUERIES **************/
	  
	  //PO not live Draft
	  
	  public function capitalgoods_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function sensors_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function packing_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function it_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'IT'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_draft()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //PO NOT LIVE FRESH
	  
	  public function capitalgoods_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function sensors_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function packing_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function it_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'IT'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_fresh()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //PO NOT LIVE OPEN
	  
	  public function capitalgoods_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function sensors_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function packing_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function it_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'IT'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_open()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //PO NOT LIVE AMENDED
	  
	  public function capitalgoods_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function sensors_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function packing_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function it_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'IT'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_amend()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //ALL NOT LIVE PO
	  
	  public function all_capitalgoods_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_sensors_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_security_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_instrument_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_tools_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF')  
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_prodcon_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_service_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%' 
and a.pomas_pono LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_packing_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF')  
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_avazonic_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF')  
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_nonprodcon_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_it_notlive()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'IT'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_total_po()  
      {  
         $query = $this->db->query("select count(*) AS count
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  /******** PENDING NOT LIVE PURCHASE ORDER QUERIES VIEW**************/
	  
	  //PO not live Draft
	  
	  public function capitalgoods_notlive_draft_new()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'CGPO%'");  

         return $query;  
      }
	  
	  public function sensors_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function packing_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_draft_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'DF' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //PO NOT LIVE FRESH
	  
	  public function capitalgoods_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'CGPO%'");  

         return $query;  
      }
	  
	  public function sensors_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function packing_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_fresh_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'FR' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //PO NOT LIVE OPEN
	  
	  public function capitalgoods_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'CGPO%'");  

         return $query;  
      }
	  
	  public function sensors_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function packing_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_open_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'OP' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //PO NOT LIVE AMENDED
	  
	  public function capitalgoods_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'CGPO%'");  

         return $query;  
      }
	  
	  public function sensors_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function security_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function instrumentation_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function tools_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function prodcon_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function service_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function packing_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function avazonic_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function nonprodcon_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_notlive_amend_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus = 'AM' 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //ALL NOT LIVE PO
	  
	  public function all_capitalgoods_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'CGPO%'");  

         return $query;  
      }
	  
	  public function all_sensors_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SENSORS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_security_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'SECURITY'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_instrument_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'INSTRUMENTATION'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_tools_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF')  
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'TOOLS'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_prodcon_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_service_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono LIKE 'SRVPO%'");  

         return $query;  
      }
	  
	  public function all_packing_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF')  
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'PACKING'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_avazonic_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF')  
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'AVAZONIC'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_nonprodcon_notlive_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, TIPLDB..no_category_po b
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono) 
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono = b.po_num
and b.category = 'NON PRODUCTION CONSUMABLES'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table) 
)");  

         return $query;  
      }
	  
	  public function all_total_po_new()  
      {  
         $query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a
where a.pomas_podocstatus in('FR','OP','AM','DF') 
and a.pomas_pono not in(select po_num from TIPLDB..po_master_table where po_num = a.pomas_pono)
and a.pomas_pono NOT LIKE 'SRVPO%' 
and a.pomas_pono NOT LIKE 'CGPO%'
and a.pomas_pono not in (select poprq_pono 
from scmdb..po_poprq_poprcovg_detail d 
where d.poprq_pono = a.pomas_pono 
and d.poprq_prno in (SELECT pr_num from TIPLDB..pr_submit_table))");  

         return $query;  
      }
	  
	  //Not in live purchase Orders Queries View
	  
   } 
   
    
?>
