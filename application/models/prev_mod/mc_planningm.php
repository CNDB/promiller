<?php
class mc_planningm extends CI_Model{  
	function __construct()  
	{    
		parent::__construct();  
	}
	  
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	 {
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		 
		 return $query; 
	 }
				 
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
			
		return $query;    
	}
				 
	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c 
where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and a.po_num = c.po_num 
and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr')");  
		
		return $query;  
	}
			  
	public function insert_po_sub_lvl2($data)
	{
		$po_num           = $this->input->post("po_num");
		$po_s_no          = $this->input->post("po_s_no");
		$mc_planning_rmks = $this->input->post("mc_planning_rmks");
		$mc_planning_rmks = str_replace("'","",$mc_planning_rmks);
		$mc_planning_inst = $this->input->post("mc_planning_inst");
		$mc_planning_by   = $_SESSION['username'];
		$mc_planning_date = date('Y-m-d H:i:s');
		$status           = "Manufacturing Clearnace Given By Planning";
		
		//Transanction Start
		$this->db->trans_start();
		
		$this->db->query("update TIPLDB..insert_po set mc_planning_rmks = '$mc_planning_rmks', mc_planning_inst = '$mc_planning_inst', 
		mc_planning_by = '$mc_planning_by', mc_planning_date = '$mc_planning_date', status = '$status' where po_num = '$po_num'");
				
		$this->db->query("update TIPLDB..po_master_table set status = '$status', mc_plan_rmks = '$mc_planning_rmks', 
		mc_plan_by = '$mc_planning_by', mc_plan_date = '$mc_planning_date', mc_given='Yes' where po_num = '$po_num'");
		
		//$this->db->query("exec tipldb..po_l3_approval");
		
		$this->db->trans_complete();
		//Transanction Complete
	}
			 
}  
?>