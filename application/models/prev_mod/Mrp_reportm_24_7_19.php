<?php
class Mrp_reportm extends CI_Model{
	  
  function __construct(){   
	 parent::__construct();  
  }  
  
  //MRP PROCEDURE 
  public function mrp_procedure(){  
	 $query = $this->db->query("exec scmdb..tipl_ReordercontrolNewtest_live_cns");  

	 return $query;  
  }
  
  //MRP RECORDS ENTRY
  public function mrp_records_entry($create_date){  
	 $this->db->trans_start();	
	 
	 $query1 = $this->db->query("delete from tipldb..mrp_report_main"); 
	 
	 $query2 = $this->db->query("insert into tipldb..mrp_report_main (item_code, item_desc, create_date)
	 select ItemCode, Descriptions, '$create_date' from scmdb..tipl_ReordercontrolNewtest_live_tbl_cns"); 
	 
	 $query3 = $this->db->query("insert into tipldb..mrp_report_history(item_code, item_desc, create_date)
	 select item_code, item_desc, '$create_date' from tipldb..mrp_report_main 
	 where item_code not in (select item_code from tipldb..mrp_report_history where last_visible_date is null)"); 
	 
	 $query4 = $this->db->query("update tipldb..mrp_report_history set last_visible_date = '$create_date' where item_code not 
	 in(select item_code from tipldb..mrp_report_main) and last_visible_date is null");  

	 $this->db->trans_complete();  
  }
 
 //MRP ITEM ENTRY 
  public function mrp_item_entry(){  
	 $item_code = $this->input->post("item_code");
	 $item_desc = $this->input->post("item_desc");
	 $category = $this->input->post("live_category");
	 $stock_uom = $this->input->post("StockUom");
	 $purchase_uom = $this->input->post("PurchaseUom");
	 $mrp_required_qty = $this->input->post("mrp_total_required");
	 $pr_type = $this->input->post("pr_type");
	 $pr_req_qty = $this->input->post("pr_req_qty");
	 $remarks1 = $this->input->post("remarks");
	 $remarks = str_replace("'","",$remarks1);
	 $requested_by = $this->input->post("user_name");
	 $requested_date = date('Y-m-d H:i:s');
	 $status = "Pending For PR Creation In ERP";
  
	 $this->db->trans_start();	
	 
	 $query1 = $this->db->query("insert into TIPLDB..mrp_item_request_entry_table 
(item_code, item_desc, category, stock_uom, purchase_uom, mrp_required_qty, pr_type, pr_req_qty, remarks, requested_by, requested_date,status) 
VALUES ('$item_code','$item_desc','$category','$stock_uom','$purchase_uom','$mrp_required_qty','$pr_type','$pr_req_qty','$remarks',
'$requested_by','$requested_date','$status')"); 
	 
	 $this->db->trans_complete();  
  }
  
  //MRP SPECIAL ITEM ENTRY 
  public function mrp_spcl_item_entry(){  
	$item_code = $this->input->post("item_code");
	$item_desc = $this->input->post("item_desc");
	$live_category = $this->input->post("live_category");
	$StockUom = $this->input->post("StockUom");
	$PurchaseUom = $this->input->post("PurchaseUom");
	$mrp_total_required = NULL;
	$pr_type = $this->input->post("spcl_item_usage_type");
	$pr_req_qty = $this->input->post("item_req_qty");
	$remarks1 = $this->input->post("remarks");
	$remarks = str_replace("'","",$remarks1);
	$user_name = $this->input->post("user_name");
	$requested_date = date('Y-m-d H:i:s');
	$status = "Pending For PR Creation In ERP";
	$atac_no = $this->input->post("atac_no");
	$atac_ld_date = $this->input->post("atac_ld_date");
	$atac_need_date = $this->input->post("atac_need_date");
	$payment_term = $this->input->post("payment_term");
	$customer_payment_terms = $this->input->post("customer_payment_terms");
	$project_name = $this->input->post("project_name");
	$customer_name = $this->input->post("customer_name");
	$pm_group = $this->input->post("pm_group");
		
	//TRANSANCTION START
	$this->db->trans_start();
		
	$sql = $this->db->query("insert into TIPLDB..mrp_item_request_entry_table 
(item_code, item_desc, category, stock_uom, purchase_uom, mrp_required_qty, pr_type, pr_req_qty, remarks, requested_by, requested_date,status,atac_no,atac_ld_date,atac_need_date,payment_term,customer_payment_terms,project_name,customer_name,pm_group) 
VALUES ('$item_code','$item_desc','$live_category','$StockUom','$PurchaseUom','$mrp_total_required','$pr_type','$pr_req_qty','$remarks',
'$user_name','$requested_date','$status','$atac_no','$atac_ld_date','$atac_need_date','$payment_term','$customer_payment_terms','$project_name',
'$customer_name','$pm_group')");
	
	$this->db->trans_complete();
	//TRANSACTION ENDS 
	 
  }
  
  //Pendal Card Details Starts
  
  public function procesdure_run($itemcode)
	{
		//echo $itemcode;
		
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function pendal_master($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_whstkblnc($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_pndposopr($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_pndposopr_new($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemPendingTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_allocat($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_allocat_tot($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_wipdet($itemcode)
	{
		$query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_lastfivetrans($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_lstyrcumnrec($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_supplier($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_mnthwsecurryrcons($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_mnthwsecurryrrcpt($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		
		return $query; 
	}
	
	public function pendal_master_bomdetail($itemcode)
	{
		$query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		
		return $query; 
	}
  
    //Pendal Card Details Ends
  
    //Entry Details Report
  
  	public function mrp_item_report(){
		
		$query = $this->db->query("select *,datediff(DAY, requested_date, getdate()) as diff from TIPLDB..mrp_item_request_entry_table where status = 'Pending For PR Creation In ERP' order by sno");
		
		return $query;
	}
	
	//DELETE ITEM VIEW
	public function delete_item_view($sno){
		
		$query = $this->db->query("select *,datediff(DAY, requested_date, getdate()) as diff from TIPLDB..mrp_item_request_entry_table where sno='$sno'");
		
		return $query;
	}
	
	public function delete_item_entry($sno){
		//TRANSANCTION START
		$this->db->trans_start();
		
		$query = $this->db->query("insert into TIPLDB..mrp_item_request_entry_table_history select * from TIPLDB..mrp_item_request_entry_table 
		where sno='$sno'");
		
		$query = $this->db->query("DELETE FROM TIPLDB..mrp_item_request_entry_table where sno='$sno'");
		
		$this->db->trans_complete();
		//TRANSACTION ENDS
	}
	
}
?>