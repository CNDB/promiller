<?php
class dispatchm extends CI_Model  
{  
	function __construct()  
	{    
		parent::__construct();  
	}

	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	{
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		
		return $query; 
	}

	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
		where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");  
		
		return $query;  
	}
	
	public function insert_po_sub($data, $NewFileName1)
	{
		$po_num  = $this->input->post("po_num");
		$po_s_no  = $this->input->post("po_s_no");
		$dispatch_submit   = $this->input->post("dispatch_submit");
		$dispatch_submit_by    = $_SESSION['username'];
		$dispatch_submit_date  =  date('Y-m-d H:i:s');
		$status = "Dispatch Instruction Updated";
		
		//Transanction Start
		$this->db->trans_start();
		
		$this->db->query("update TIPLDB..insert_po set dispatch_submit = '$dispatch_submit', dispatch_submit_by = '$dispatch_submit_by', 
		dispatch_submit_date = '$dispatch_submit_date', att_road_premit = '$NewFileName1', status = '$status' where po_num = '$po_num'");
		
		$this->db->query("update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'");
		
		$this->db->query("insert into tipldb..po_roadpermit_history (po_num, att_road_premit, dispatch_submit, dispatch_submit_by, dispatch_submit_date) 
		values ('$po_num','$NewFileName1','$dispatch_submit','$dispatch_submit_by','$dispatch_submit_date')");
		
		$this->db->trans_complete();
		//Transanction Complete
	}
}  
?>