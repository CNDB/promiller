<?php
class view_prm extends CI_Model{
	  
      function __construct()  
      {   
         parent::__construct();  
      }
	  
	  
	  public function view_pr_details($pr_num)  
      {  
		 $query = $this->db->query("select count(*) as count1 from scmdb..prq_preqm_pur_reqst_hdr where preqm_prno = '$pr_num'");  

         return $query;  
      }	
	  
	  public function view_prqit_prno($pr_num)  
      {  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = '".$pr_num."' 
		 and a.prqit_itemcode = d.iou_itemcode");
		    
         return $query;  
      }
	  
	  public function view_pr_live($pr_num)  
      {  
         $query = $this->db->query("select * from tipldb..pr_submit_table where pr_num = '".$pr_num."'");
		    
         return $query;  
      }
	  
	  public function procesdure_run($itemcode)
	  { 
		 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	  public function procesdure_run_supplier($user_id, $pr_num, $itemcode)
	  {
	 	 $query = $this->db->query("exec TIPLDB..supplier_details  '1' , '$itemcode'  , '$pr_num'");
		 	
		 return $query;    
	  }
	  
	  //Chat History
	  
	  public function chat_history_table($pr_num)
	  {
		$query = $this->db->query("select * from tipldb..pr_submit_table_comment where pr_num = '$pr_num' and enabled = 'YES' order by s_no ASC");
		
		return $query;  
	  }
	  
	  //Chat History
	  
	  //Action Timing Report Queries
	  
	  public function prerplive($pr_num)
	  {
		$query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr a, tipldb..pr_submit_table b where a.preqm_prno = b.pr_num 
and a.preqm_prno = '$pr_num'");
		
		return $query;  
	  }
	  // Action Timing Report Queries
	  
	  public function pendal_info($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_info_monthwise($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }    
	  
	  public function pendal_info_last5_trans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	  public function pendal_info_reorder_lvl_qty($itemcode)
	  {
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	  public function lstyrcumnrec1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function lastfivetrans1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function itemdescmaster($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query; 
	  } 
	  
	  public function pendal_master_mnthwsecurryrcons($itemcode)
	  {
		 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_bomdetail($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
	  
	  public function pendal_master_supplier($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		 	
		 return $query; 
	  }
}  
?>