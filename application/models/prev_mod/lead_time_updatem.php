<?php
class lead_time_updatem extends CI_Model{
	  
	function __construct(){   
		parent::__construct();  
	}  
	
	public function max_amend_no($po_num){
		 
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		 return $query; 
	 }
	 
	 public function view_po_details($po_num, $amend_no){  
	 
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$po_num' and a.po_num = b.pomas_pono and a.po_num = c.po_num and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num') order by a.po_line_no");  

         return $query;  
     }
	 
	 public function lead_time_update($data){  
	 
	 	$po_num = $this->input->post('po_num');
		
		$supp_lead_time = $this->input->post('supp_lead_time');
		
		$remarks = $this->input->post('remarks');
		
		$manufact_clearnace = $this->input->post("manufact_clrnce");
		
		if (in_array("yes", $manufact_clearnace))
		{
			$status = "PO Send To Issuance Of Manufacturing Clearanace by planning";
			
		} else {
			
			$status = "Pending For Acknowledgement From Supplier";
		}
		
		$created_by = $_SESSION['username'];
		
		$created_date = date("Y-m-d H:i:s");
		
		//Transaction Start
		$this->db->trans_start();
	 
		$sql = "update tipldb..po_master_table set leadtime_supp = '$supp_lead_time', leadtime_supp_rmks = '$remarks', 
		leadtime_updated_by = '$created_by', leadtime_update_date = '$created_date', status = '$status' where po_num = '$po_num'";  
		
		//echo $sql; die;
		
		$query = $this->db->query($sql);  
		
		$this->db->trans_complete();
		//Transanction Complete
	}
	
	public function mail_table($po_num){
		   
         $query = $this->db->query("select * from tipldb..po_master_table a, tipldb..insert_po b where b.po_num = '$po_num' and a.po_num = b.po_num");  

         return $query;  
     }
	 
	   
}  
?>