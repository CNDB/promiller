<?php
class Po_fy_reportm extends CI_Model{
	  
      function __construct()  
      {   
         parent::__construct();  
      }  
	   
      public function item_details_2016_17()  
      {  
		$query = $this->db->query("select pomas_pono, supp_spmn_supname, poitm_itemcode, ml_itemvardesc, class_desc, poitm_order_quantity, 
poitm_po_cost, pomas_pobasicvalue, pomas_tcdtotalrate, pomas_tcal_total_amount
from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, 
scmdb..itm_ml_multilanguage d, scmdb..itm_ibu_itemvarhdr e, scmdb..cls_item_category_vw f
where a.pomas_pono = b.poitm_pono 
and a.pomas_suppliercode = supp_spmn_supcode 
and b.poitm_itemcode = d.ml_itemcode
and b.poitm_itemcode = e.ibu_itemcode
and e.ibu_category = f.class_code
and pomas_podocstatus not in('DE') 
and pomas_poauthdate between '2016-04-01' and '2017-03-30'
and pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono) 
and a.pomas_poamendmentno = b.poitm_poamendmentno 
order by a.pomas_pono asc");  

         return $query;  
      }
	  
	  public function item_details_2015_16()  
      { 
		$query = $this->db->query("select *
from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, 
scmdb..itm_ml_multilanguage d, scmdb..itm_ibu_itemvarhdr e, scmdb..cls_item_category_vw f
where a.pomas_pono = b.poitm_pono 
and a.pomas_suppliercode = supp_spmn_supcode 
and b.poitm_itemcode = d.ml_itemcode
and b.poitm_itemcode = e.ibu_itemcode
and e.ibu_category = f.class_code
and pomas_podocstatus not in('DE') 
and pomas_poauthdate between '2015-04-01' and '2016-03-30'
and pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = a.pomas_pono) 
and a.pomas_poamendmentno = b.poitm_poamendmentno 
order by a.pomas_pono asc");  

         return $query;  
      }
	  
	  public function pr_po_report()  
      {  
         $query = $this->db->query("select DATEDIFF(dd,preqm_authdate,GETDATE()) as pr_age,CONVERT(date,prqit_needdate)as pr_needDate,
					CONVERT(date,preqm_createddate) as pr_createdDate,loi_itemdesc,a.*,b.* 
					from SCMDB..prq_preqm_pur_reqst_hdr a, SCMDB..prq_prqit_item_detail b , SCMDB..itm_loi_itemhdr c
					where a.preqm_status='AU'
					and a.preqm_prno = b.prqit_prno
					and b.prqit_itemcode = c.loi_itemcode
					and b.prqit_balqty > 0
					and a.preqm_prno not in('IPR-0169-17')
					and a.preqm_prno in (select pr_num from tipldb..pr_submit_table where pr_status not in('PR Disapproved At Purchase Level'))
					order by a.preqm_prno");
					
         return $query;  
      }
}
?>