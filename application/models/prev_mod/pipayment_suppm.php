<?php
class Pipayment_suppm extends CI_Model  
{  
	function __construct()  
	{    
		parent::__construct();  
	}   
	  
	public function select_prqit_prno()  
	{  
		$query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus ='FR'");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	{
		$query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		return $query; 
	}
	
	public function view_poqit_pono($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
									where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");
		  
		return $query;  
	}
	
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function view_payment_details($po_supp_code)  
	{
		//echo "select * from tipldb..insert_po where po_num = '$po_supp_code'";  
		/*$query = $this->db->query("select * from scmdb..sr_receipt_mst a, TIPLDB..po_master_table b where a.supplier_code = b.supplier_code 
and a.receipt_date <= b.po_create_date and a.supplier_code = '$po_supp_code'");*/

		$query = $this->db->query("select * from tipldb..insert_po where po_num = '$po_supp_code'");
		  
		return $query;  
	}
	
	public function insert_po_sub($data, $NewFileName2)
	{
		$username                 = $_SESSION['username'];
		$po_num                   = $this->input->post("po_num");
		//new elements
		$pi_payment_mode          = $this->input->post("pi_payment_mode");
		$supp_acc_name            = $this->input->post("supp_acc_name");
		$supp_acc_no              = $this->input->post("supp_acc_no");
		$supp_bank_name           = $this->input->post("supp_bank_name");
		$supp_ifsc_code           = $this->input->post("supp_ifsc_code");
		$supp_transfered_amount   = $this->input->post("supp_transfered_amount");
		$transfer_type            = $this->input->post("transfer_type");
		$date_of_transfer         = $this->input->post("date_of_transfer");
		$transfer_from_bank       = $this->input->post("transfer_from_bank");
		$transfer_from_acc        = $this->input->post("transfer_from_acc");
		$pi_cheque_no             = $this->input->post("pi_cheque_no");
		$pi_cheque_date           = $this->input->post("pi_cheque_date");
		$pi_bank_name             = $this->input->post("pi_bank_name");
		$pi_cheque_amt            = $this->input->post("pi_cheque_amt");
		$pi_uploaded_cheque_img   = $this->input->post("pi_uploaded_cheque_img");
		//new elements
		$pi_payment_supp          = $this->input->post('pi_payment_supp');
		$pi_payment_supp_by       = $_SESSION['username'];
		$pi_payment_supp_date     = date('Y-m-d H:i:s');		
		$status                   = 'PI Payment to Supplier Done';
		
		//Transanction Start
		$this->db->trans_start();
		
		$this->db->query("update TIPLDB..insert_po set pi_payment_mode = '$pi_payment_mode', supp_acc_name = '$supp_acc_name', supp_acc_no = '$supp_acc_no', 
		supp_bank_name = '$supp_bank_name', supp_ifsc_code = '$supp_ifsc_code', supp_transfered_amount = '$supp_transfered_amount', 
		transfer_type = '$transfer_type', date_of_transfer = '$date_of_transfer', transfer_from_bank = '$transfer_from_bank', 
		transfer_from_acc = '$transfer_from_acc', pi_cheque_no = '$pi_cheque_no', pi_cheque_date = '$pi_cheque_date', pi_bank_name = '$pi_bank_name',  
		pi_cheque_amt = '$pi_cheque_amt', pi_uploaded_cheque_img = '$NewFileName2', pi_payment_supp = '$pi_payment_supp', 
		pi_payment_supp_by = '$pi_payment_supp_by', pi_payment_supp_date = '$pi_payment_supp_date', status = '$status' where po_num = '$po_num'");
		
		$this->db->query("update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'");
		
		$this->db->trans_complete();
		//Transanction Complete
	}	 
}  
?>