<?php
class mcdi_approvalm extends CI_Model{
	  
	function __construct(){   
		parent::__construct();  
	}  
	
	public function max_amend_no($po_num){
		 
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$po_num'");
		 return $query; 
	 }
	 
	 public function view_po_details($po_num, $amend_no){  
	 
         $query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b, tipldb..po_master_table c where a.po_num = '$po_num' and a.po_num = b.pomas_pono and a.po_num = c.po_num and b.pomas_poamendmentno = (select MAX(pomas_poamendmentno) from scmdb..po_pomas_pur_order_hdr where pomas_pono = '$po_num') order by a.po_line_no");  

         return $query;  
     }
	 
	 //MC & DI Reporting Authority Query
	 public function mcdi_ra_submit($data){  
	 
		$po_num = $this->input->post('po_num');
		$po_date = $this->input->post('po_date');
		$approval = $this->input->post('approval');
		$ra_rmks = $this->input->post('ra_rmks');
		$entry_no = $this->input->post('entry_no');
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d H:i:s");
		$level = "LEVEL RA";
		
		/*if($approval == 'APPROVE'){
			$status = "Pending For Approval From Project Incharge";
		} else if($approval == 'DISAPPROVE'){
			$status = "PO Disapproved At Reporting Authority Level";
		}*/
		
		if($approval == 'APPROVE'){
			$status = "Pending For Approval From Cash Flow Incharge LVL2";
		} else if($approval == 'DISAPPROVE'){
			$status = "PO Disapproved At Reporting Authority Level";
		}
		
		//Transaction Start
		$this->db->trans_start();
		
		$sql1 = "update tipldb..po_master_table set mcdi_ra_rmks = '$ra_rmks', 
		mcdi_ra_app_inst = '$approval', mcdi_ra_app_by = '$created_by', mcdi_ra_app_date = '$created_date', status = '$status' 
		where po_num = '$po_num'";  
		
		$query1 = $this->db->query($sql1);
		
		$sql2 = "update tipldb..gateentry_item_rec_table set status = '$status' where po_num = '$po_num' and entry_no = '$entry_no'";  
		
		$query2 = $this->db->query($sql2);
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment)
		values ('$po_num', '$approval', '$level', '$created_by', '$created_date', '$ra_rmks')";  
		
		$query3 = $this->db->query($sql3);
		
		$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
		values ('$entry_no','Purchase','Approved/Disapproved From RA','$remarks','$user_id','$created_by','$po_num')";
			
		$this->db->query($sql5);
		
		$this->db->trans_complete();
		//Transanction Complete
		  
     }
	 
	 //MC & DI Project Incharge Entry
	 public function mcdi_pi_submit($data){  
	 
		$po_num = $this->input->post('po_num');
		$po_date = $this->input->post('po_date');
		$approval = $this->input->post('approval');
		$pi_rmks = $this->input->post('pi_rmks');
		$entry_no = $this->input->post('entry_no');
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d H:i:s");
		$level = "LEVEL PI";
		
		if($approval == 'APPROVE'){
			$status = "Pending For Approval From Cash Flow Incharge LVL2";
		} else if($approval == 'DISAPPROVE'){
			$status = "PO Disapproved At Project Incharge Level";
		}
		
		//Transaction Start
		$this->db->trans_start();
		
		$sql1 = "update tipldb..po_master_table set mcdi_pi_rmks = '$pi_rmks', 
		mcdi_pi_app_inst = '$approval', mcdi_pi_app_by = '$created_by', mcdi_pi_app_date = '$created_date', status = '$status' 
		where po_num = '$po_num'";  
		
		$query1 = $this->db->query($sql1);
		
		$sql2 = "update tipldb..gateentry_item_rec_table set status = '$status' where po_num = '$po_num' and entry_no = '$entry_no'";  
		
		$query2 = $this->db->query($sql2);
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment)
		values ('$po_num', '$approval', '$level', '$created_by', '$created_date', '$pi_rmks')";  
		
		$query3 = $this->db->query($sql3);
		
		$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
		values ('$entry_no','Purchase','Approved/Disapproved From PI','$remarks','$user_id','$created_by','$po_num')";
			
		$this->db->query($sql5);
		
		$this->db->trans_complete();
		//Transanction Complete
		  
     }
	 
	 //MC & DI Cash Flow Incharge LVL1 Entry
	 public function mcdi_cf1_submit($data){  
	 
		$po_num = $this->input->post('po_num');
		$po_date = $this->input->post('po_date');
		$approval = $this->input->post('approval');
		$cf1_rmks = $this->input->post('cf1_rmks');
		$entry_no = $this->input->post('entry_no');
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d H:i:s");
		$level = "LEVEL CF1";
		
		if($approval == 'APPROVE'){
			$status = "Pending For Approval From Cash Flow Incharge LVL2";
		} else if($approval == 'DISAPPROVE'){
			$status = "PO Disapproved At Cash Flow Incharge LVL1";
		}
		
		//Transaction Start
		$this->db->trans_start();
		
		$sql1 = "update tipldb..po_master_table set mcdi_cf1_rmks = '$cf1_rmks', 
		mcdi_cf1_app_inst = '$approval', mcdi_cf1_app_by = '$created_by', mcdi_cf1_app_date = '$created_date', status = '$status' 
		where po_num = '$po_num'";  
		
		$query1 = $this->db->query($sql1);
		
		$sql2 = "update tipldb..gateentry_item_rec_table set status = '$status' where po_num = '$po_num' and entry_no = '$entry_no'";  
		
		$query2 = $this->db->query($sql2);
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment)
		values ('$po_num', '$approval', '$level', '$created_by', '$created_date', '$cf1_rmks')";  
		
		$query3 = $this->db->query($sql3);
		
		$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
		values ('$entry_no','Purchase','Approved/Disapproved From CF1','$remarks','$user_id','$created_by','$po_num')";
			
		$this->db->query($sql5);
		
		$this->db->trans_complete();
		//Transanction Complete
		  
     }
	 
	 //MC & DI Cash Flow Incharge LVL2 Entry
	 public function mcdi_cf2_submit($data){  
	 
		$po_num = $this->input->post('po_num');
		$po_date = $this->input->post('po_date');
		$approval = $this->input->post('approval');
		$cf2_rmks = $this->input->post('cf2_rmks');
		$entry_no = $this->input->post('entry_no');
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d H:i:s");
		$level = "LEVEL CF2";
		
		if($approval == 'APPROVE'){
			$status = "Pending For GRCPT";
		} else if($approval == 'DISAPPROVE'){
			$status = "PO Disapproved At Cash Flow Incharge LVL2";
		}
		
		//Transaction Start
		$this->db->trans_start();
		
		$sql1 = "update tipldb..po_master_table set mcdi_cf2_rmks = '$cf2_rmks', 
		mcdi_cf2_app_inst = '$approval', mcdi_cf2_app_by = '$created_by', mcdi_cf2_app_date = '$created_date', status = '$status' 
		where po_num = '$po_num'";  
		
		$query1 = $this->db->query($sql1);
		
		$sql2 = "update tipldb..gateentry_item_rec_table set status = '$status' where po_num = '$po_num' and entry_no = '$entry_no'";  
		
		$query2 = $this->db->query($sql2);
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment)
		values ('$po_num', '$approval', '$level', '$created_by', '$created_date', '$cf2_rmks')";  
		
		$query3 = $this->db->query($sql3);
		
		$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
		values ('$entry_no','Purchase','Approved/Disapproved From CF2','$remarks','$user_id','$created_by','$po_num')";
			
		$this->db->query($sql5);
		
		$this->db->trans_complete();
		//Transanction Complete
		  
     }	   
	 
	 //MC & DI Cash Flow Incharge LVL2 Entry
	 public function mcdi_disapproved_submit($data){  
	 
		$po_num = $this->input->post('po_num');
		$po_date = $this->input->post('po_date');
		$approval = $this->input->post('approval');
		$dis_rmks = $this->input->post('dis_rmks');
		$entry_no = $this->input->post('entry_no');
		$created_by = $_SESSION['username'];
		$created_date = date("Y-m-d H:i:s");
		$level = "LEVEL 0";
		
		$status = "Pending For MC & DI Approval From RA";
		
		//Transaction Start
		$this->db->trans_start();
		
		$sql1 = "update tipldb..po_master_table set mcdi_dis_rmks = '$dis_rmks', 
		mcdi_dis_app_inst = '$approval', mcdi_dis_app_by = '$created_by', mcdi_dis_app_date = '$created_date', status = '$status' 
		where po_num = '$po_num'";  
		
		$query1 = $this->db->query($sql1);
		
		$sql2 = "update tipldb..gateentry_item_rec_table set status = '$status' where po_num = '$po_num' and entry_no = '$entry_no'";  
		
		$query2 = $this->db->query($sql2);
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment)
		values ('$po_num', '$approval', '$level', '$created_by', '$created_date', '$dis_rmks')";  
		
		$query3 = $this->db->query($sql3);
		
		$sql5="insert into tipldb..gate_entry_comment (entry_no,level,status,instruction,comment_id,comment_name,po_num)
		values ('$entry_no','Purchase','Send For Approval From RA','$remarks','$user_id','$created_by','$po_num')";
			
		$this->db->query($sql5);
		
		$this->db->trans_complete();
		//Transanction Complete
		  
     }
}  
?>