<?php
class disapproved_pom extends CI_Model  
   {  
      function __construct()  
      {  
         parent::__construct();  
      }  
      
	  public function select_prqit_prno()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus ='FR'");  

         return $query;  
      }
	  
	  public function cal_amend_no($selectpr)  
      {  
         $query = $this->db->query("SELECT MAX(pomas_poamendmentno) as amend_no FROM scmdb..po_pomas_pur_order_hdr where pomas_pono = '$selectpr';");  

         return $query;  
      }
	 
	  public function view_poqit_pono($selectpr, $amend_no)  
      {  
         $query1 = $this->db->query("select * from scmdb..po_poprq_poprcovg_detail where poprq_pono = '$selectpr' ");
		 
		 if( $query1->num_rows() > 0) {
		   
         $query = $this->db->query("select *, datediff(DAY, a.pomas_podate, getdate()) as diff, datediff(DAY, i.preqm_prdate, getdate()) as diff1 
		 from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, scmdb..po_poprq_poprcovg_detail d, 		        					         scmdb..supp_addr_address e, scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h, 
		 scmdb..prq_preqm_pur_reqst_hdr i, scmdb..supp_bu_suplmain j where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
		 and a.pomas_pono = d.poprq_pono and a.pomas_pono ='$selectpr' and b.poitm_polineno = d.poprq_polineno and c.supp_spmn_supcode = e.supp_addr_supcode 				         and b.poitm_itemcode = f.ml_itemcode and f.ml_itemcode = g.lov_itemcode and a.pomas_pono = h.paytm_pono 
		 and a.pomas_poamendmentno = '$amend_no' and a.pomas_poamendmentno = b.poitm_poamendmentno and a.pomas_poamendmentno = d.poprq_poamendmentno and 		         a.pomas_poamendmentno = h.paytm_poamendmentno and d.poprq_prno = i.preqm_prno and e.supp_addr_supcode = j.supp_bu_supcode 
		 and e.supp_addr_addid = j.supp_bu_deforderto");
		 
		 } else {
			 
		 $query = $this->db->query("select *, datediff(DAY, a.pomas_podate, getdate()) as diff 
		 from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, scmdb..supp_spmn_suplmain c, 		        					            					         scmdb..supp_addr_address e, scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h, scmdb..supp_bu_suplmain i
		 where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
		 and a.pomas_pono ='$selectpr' and c.supp_spmn_supcode = e.supp_addr_supcode and b.poitm_itemcode = f.ml_itemcode and f.ml_itemcode = g.lov_itemcode         and a.pomas_pono = h.paytm_pono 
		 and a.pomas_poamendmentno = '$amend_no' and a.pomas_poamendmentno = b.poitm_poamendmentno and a.pomas_poamendmentno = h.paytm_poamendmentno
		 and e.supp_addr_supcode = i.supp_bu_supcode and e.supp_addr_addid = i.supp_bu_deforderto"); 
		 
		 }
		 							 
         return $query;    
      }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	  public function insert_attachment($po_num,$po_quote_frmsupp_newname) 
	  {  
		  $sql ="insert into tipldb..po_supplier_quotes (po_num, attached_supp_quotes) values('".$po_num."','".$po_quote_frmsupp_newname."')";
		  
		  //$query = $this->db->query($sql);  
	  }
	   
	  public function insert_po_sub($data)
	  {  
	 	$username             = $_SESSION['username'];
		$po_num               = $this->input->post("po_num");
		$po_line_no           = $this->input->post("po_line_no");
		$po_ipr_no            = $this->input->post("po_ipr_no");
		$array_count		  = count($po_ipr_no);
		$po_date              = $this->input->post("po_date");
		$po_item_code         = $this->input->post("po_item_code");
		$po_qty               = $this->input->post("po_qty");
		$po_uom               = $this->input->post("po_uom");
		$po_cost_pr_unt       = $this->input->post("po_cost_pr_unt");
		$po_need_date         = $this->input->post("po_need_date");
		$po_wh_code           = $this->input->post("po_wh_code");
		$po_basic_val         = $this->input->post("po_basic_val");
		$po_tot_val           = $this->input->post("po_tot_val");
		$po_supp_code         = $this->input->post("po_supp_code");
		$po_supp_name         = $this->input->post("po_supp_name");
		$po_supp_add          = $this->input->post("po_supp_add");
		$po_supp_email        = $this->input->post("po_supp_email");
		$po_drwg_refno        = $this->input->post("po_drwg_refno");
		$item_desc            = $this->input->post("item_desc");
		$payterm              = $this->input->post("payterm");
		$payterm_desc         = $this->input->post("payterm_desc");
		$freight              = $this->input->post("freight");
		$po_rmks              = $this->input->post("po_rmks");
		$po_deli_type         = $this->input->post("po_deli_type");
		$po_lead_time         = $this->input->post("po_lead_time");
		$po_manfact_clernce   = $this->input->post("po_manfact_clernce");
		$po_dispatch_inst     = $this->input->post("po_dispatch_inst");
		$po_approval_lvl0     = $this->input->post("po_approval_lvl0");
		$po_approvedby_lvl0   = $_SESSION['username'];
		$currency             = $this->input->post("currency");
		$erp_part_code        = $this->input->post("carrier_name");
		$carrier_name         = $this->input->post("carrier_name");
		$lastyr_cons          = $this->input->post("lastyr_cons");
		$current_stk          = $this->input->post("current_stk");
		$reservation_qty      = $this->input->post("reservation_qty");
		$for_stk_quantity     = $this->input->post("for_stk_quantity");
		$calculation          = $this->input->post("calculation");
		$last_price           = $this->input->post("last_price");
		$current_price        = $this->input->post("current_price");
		$pnf                  = $this->input->post("pnf");
		$po_total_value       = $this->input->post("po_total_value");
		$item_remarks         = $this->input->post("item_remarks");
		$po_amend_no          = $this->input->post("po_amend_no");
		$po_type              = $this->input->post("po_type");
		$supp_phone           = $this->input->post("supp_phone");
		$contact_person       = $this->input->post("contact_person");
		$pay_mode             = $this->input->post("pay_mode");
		$trans_mode           = $this->input->post("trans_mode");
		$partial_ship         = $this->input->post("partial_ship");
		$status               = "PO Send For Authorization";
		$username             = $_SESSION['username'];
		$create_date          = date('Y-m-d H:i:s');
		$total_item_value     = $this->input->post("item_value");
		$freight_place        = $this->input->post("freight_place");
		$insurance_liablity   = $this->input->post("insurance_liablity");
		$freight_type         = $this->input->post("freight_type");
		$approx_freight       = $this->input->post("approx_freight");
		$spcl_inst_supp       = $this->input->post("spcl_inst_supp");
		$ld_applicable        = $this->input->post("ld_applicable");
		$po_approval_lvl2     = NULL;	
		$po_approvalby_lvl2   = NULL;
		$po_approvaldate_lvl2 = NULL; 
		$remarks_po_lvl2      = NULL;
		$level                = "LEVEL 0";
		
		for($i=0;$i<$array_count;$i++){
		
		$sql1 = "update tipldb..insert_po set po_line_no = '$po_line_no[$i]', po_ipr_no  = '$po_ipr_no[$i]', po_date  = '$po_date', po_item_code ='$po_item_code[$i]', po_qty = '$po_qty', po_uom = '$po_uom', po_cost_pr_unt = '$po_cost_pr_unt', po_need_date = '$po_need_date', po_wh_code = '$po_wh_code', po_basic_val = '$po_basic_val', po_tot_val = '$po_tot_val', po_supp_code = '$po_supp_code', po_supp_name = '$po_supp_name', po_supp_add = '$po_supp_add', po_supp_email = '$po_supp_email', po_drwg_refno = '$po_drwg_refno', po_itm_desc = '$item_desc[$i]', payterm = '$payterm', freight = '$freight', po_rmks = '$po_rmks', po_deli_type = '$po_deli_type', po_lead_time = '$po_lead_time', po_manfact_clernce = '$po_manfact_clernce', po_dispatch_inst = '$po_dispatch_inst',po_approval_lvl0 = '$po_approval_lvl0', po_approvedby_lvl0 = '$po_approvedby_lvl0', status = '$status', currency = '$currency', carrier_name = '$carrier_name', lastyr_cons = '$lastyr_cons[$i]', current_stk = '$current_stk[$i]', reservation_qty = '$reservation_qty[$i]', for_stk_quantity = '$for_stk_quantity[$i]', last_price = '$last_price[$i]', current_price = '$current_price[$i]', po_total_value = '$po_total_value', item_remarks = '$item_remarks[$i]', po_amend_no = '$po_amend_no', po_type = '$po_type', supp_phone = '$supp_phone', contact_person = '$contact_person', pay_mode = '$pay_mode', trans_mode = '$trans_mode', partial_ship = '$partial_ship', total_item_value = '$total_item_value[$i]', freight_place = '$freight_place', insurance_liablity = '$insurance_liablity', freight_type = '$freight_type', po_spcl_inst_frm_supp = '$spcl_inst_supp', approx_freight = '$approx_freight', ld_applicable = '$ld_applicable', payterm_desc = '$payterm_desc', po_approval_lvl2 = '$po_approval_lvl2', po_approvalby_lvl2 = '$po_approvalby_lvl2',	po_approvaldate_lvl2 = '$po_approvaldate_lvl2', remarks_po_lvl2 = '$remarks_po_lvl2' where po_num = '$po_num'";
		
		$query1 = $this->db->query($sql1);
		
		}
		
		$sql2 = "update tipldb..po_master_table set uom = '$po_uom', supplier_code = '$po_supp_code', po_create_date = '$po_date', po_target_date = '$po_date', created_by = '$po_approvedby_lvl0', create_date = '$create_date', 
		po_approval_level0 = '$po_approval_lvl0', status = '$status', po_supp_name = '$po_supp_name', po_total_value = '$po_total_value', po_approval_lvl2 = '$po_approval_lvl2' where po_num = '$po_num'";
		
		$sql3 = "insert into tipldb..insert_po_comment (po_num, instruction, level, comment_by, datentime, comment) 
		values ('".$po_num."','".$po_approval_lvl0."','".$level."','".$username."','".$create_date."','".$po_rmks."')";
		
		$query2 = $this->db->query($sql2);
		
		$query3 = $this->db->query($sql3);
	  }
	  
	  public function chat_history($selectpr)
	  {
	  	$query = $this->db->query("select * from tipldb..insert_po_comment where po_num = '$selectpr' ");
	  }
 
   }  
?>