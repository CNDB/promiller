<?php
class createscrm extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function select_scrit_scrno()  
      {   
		$query = $this->db->query("select * FROM scmdb..scr_scrqm_screquest_hdr where SUBSTRING(scrqm_scrno, 1, 3) in ('SCR') and scrqm_status = 'FR'");  

         return $query;  
     }
	 
	 public function view_prqit_prno($selectpr)  
      {  
         $query = $this->db->query("select * from scmdb..scr_scrit_item_detail a, scmdb..scr_scrqm_screquest_hdr  b, scmdb..supp_spmn_suplmain c, scmdb..itm_iou_itemvarhdr d
where a.scrit_scrno = b.scrqm_scrno  and a.scrit_scrno = '$selectpr' and  a.scrit_supplier_code = c.supp_spmn_supcode and a.scrit_itemcode = d.iou_itemcode");  
         return $query;  
      }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	  public function pendal_info($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	  public function pendal_info_monthwise($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }    
	  
	  public function pendal_info_last5_trans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function pendal_info_reorder_lvl_qty($itemcode)
	  {
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function lstyrcumnrec1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function lastfivetrans1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function itemdescmaster($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	 
	 /******* Pendal Card Models *******/
	 
	  public function pendal_master($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_whstkblnc($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_pndposopr($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_allocat($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  
	   public function pendal_master_allocat_tot($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_wipdet($itemcode)
	  {
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_lastfivetrans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_lstyrcumnrec($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_supplier($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_mnthwsecurryrcons($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_bomdetail($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   /******** Months Of Inventory if item is in reorder level *******/
	 public function monthofinvtry($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
	   }
	   
	  public function orderinadvnce($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
	   }
	  
	  public function wrkordr_ser($itemcode)
	  {
	  	$query = $this->db->query("select * from scmdb..pmd_twos_wops_postn where wos_item_no = '$itemcode' and wos_ord_stat_code in('G','E','F','S')");
		
		 return $query;
	  }
	  
	  public function wrkordr_ser_res($workorder)
	  {
	  	$query = $this->db->query("select * from tipldb..pendalcard_rkg a, scmdb..pmd_twos_wops_postn b, scmdb..pmd_twoh_wo_headr c, scmdb..cust_lo_info d 
where a.flag='ItemMaster' and a.itemcode = b.wos_item_no and b.wos_wo_no = c.woh_wo_no and c.woh_cust_name = d.clo_cust_code and b.wos_wo_no = '$workorder'");
		
		 return $query;
	  }
	   
	   public function insert_scr_sub($data)
	  {
	  
		$username = $_SESSION['username'];
		
		$attch_cost_sheet = strtolower($_FILES["cost_sheet"]["name"]);
		$attach_drawing = strtolower($_FILES["attach_drawing"]["name"]);

		$scr_no = $this->input->post("scr_no");
		$scr_date = $this->input->post("scr_date"); 
		$item_code = $this->input->post("item_code");
		$item_desc = $this->input->post("item_desc");
		$bom_reqd = $this->input->post("bom_reqd");
		$bom_ref = $this->input->post("bom_ref");
		$required_qty = $this->input->post("required_qty");
		$authorise_qty = $this->input->post("authorise_qty");
		$trans_uom = $this->input->post("trans_uom");
		$unit_cost = $this->input->post("unit_cost");
		$need_date = $this->input->post("need_date");
		$supplier_code = $this->input->post("supplier_code");
		$supplier_name = $this->input->post("supplier_name");
		$supplier_class = $this->input->post("supplier_class");
		$warehse_code = $this->input->post("warehse_code");
		$ref_doc_type = $this->input->post("ref_doc_type");
		$total_val = $this->input->post("total_val");
		$itm_desc = $this->input->post("itm_desc");
		$work_odr_no = $this->input->post("work_odr_no");
		$sono = $this->input->post("sono");
		$customer_code = $this->input->post("customer_code");
		$customer_name = $this->input->post("customer_name");
		$drawing_no = $this->input->post("drawing_no");
		//$attach_cost_sheet = $this->input->post("attach_cost_sheet");
		//$attach_drawing = $this->input->post("attach_drawing");
		$remarks = $this->input->post("remarks");
		

		 $sql = "insert into tipldb..scr_submit_table ( scr_no, scr_date, item_code, item_desc, bom_reqd, bom_ref, required_qty, authorise_qty, trans_uom, unit_cost, need_date, supplier_code ,supplier_name, supplier_class, warehse_code, ref_doc_type, total_val, itm_desc, work_odr_no, sono, customer_code, customer_name, drawing_no, attach_cost_sheet, attach_drawing, remarks, created_by ) 
		VALUES ('".$scr_no."','".$scr_date."','".$item_code."','".$item_desc."','".$bom_reqd."','".$bom_ref."','".$required_qty."','".$authorise_qty."','".$trans_uom."','".$unit_cost."','".$need_date."','".$supplier_code."','".$supplier_name."','".$supplier_class."','".$warehse_code."','".$ref_doc_type."','".$total_val."','".$itm_desc."','".$work_odr_no."','".$sono."','".$customer_code."','".$customer_name."','".$drawing_no."','".$attch_cost_sheet."','".$attach_drawing."','".$remarks."','".$username."')";
		
		$this->db->query($sql);
		
		
		
	  }
	 
   }  
?>