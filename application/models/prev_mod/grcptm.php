<?php
class grcptm extends CI_Model  
{  
	function __construct()  
	{  
		parent::__construct();  
	}  
	
	public function select_prqit_prno_lvl2()  
	{  
		$query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' and status = 'Submit Delivery' ");  
		
		return $query;  
	}
	
	public function max_amend_no($selectpr)
	 {
		 $query = $this->db->query("select max(pomas_poamendmentno) as amend_no from scmdb..po_pomas_pur_order_hdr where pomas_pono ='$selectpr'");
		 return $query; 
	 }
	
	public function procesdure_run($itemcode)
	{
		$query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		
		return $query;    
	}
	
	public function po_view_lvl2($selectpr, $amend_no)  
	{  
		$query = $this->db->query("select *, datediff(DAY, a.po_date,getdate()) as diff from tipldb..insert_po a, scmdb..po_pomas_pur_order_hdr b
		where a.po_num = '$selectpr' and a.po_num = b.pomas_pono and b.pomas_poamendmentno = '$amend_no'");  
		
		return $query;  
	}
	
	public function lastfivetrans1_lvl2($itemcode)
	{
		$query = $this->db->query("select * from TIPLDB..pendalcard_rkg a, scmdb..po_paytm_doclevel_detail b where a.flag = 'ItemLastFiveTrans' and 
		a.LastPOGRTran = b.paytm_pono and ItemCode='$itemcode'");
		
		return $query;
	
	}
	
	public function insert_po_sub_lvl2($data)
	{
		$po_num  = $this->input->post("po_num");
		$po_s_no  = $this->input->post("po_s_no");
		$po_qty   = $this->input->post("po_qty");
		$grcpt_no   = $this->input->post("grcpt_no");
		$grcpt_rec_qty = $this->input->post("grcpt_rec_qty");
		$grcpt_rec_date = $this->input->post("grcpt_rec_date");
		$grcpt_apprve   = $this->input->post("grcpt_apprve");
		$grcpt_apprve_by  = $_SESSION['username'];
		$grcpt_apprve_date  =  date('Y-m-d H:i:s');
		
		if($grcpt_rec_qty < $po_qty && $grcpt_apprve == 'Submit GRCPT'){
			
			$status = 'Partial Order Delivery At GRCPT';
			
		} elseif($grcpt_rec_qty < $po_qty && $grcpt_apprve == 'Short Close PO'){
			
			$status = 'PO Short Close At GRCPT';
			
		} else {
			
			$status = 'Full Order Delivery At GRCPT';
			
		}
		
		/*echo "update TIPLDB..insert_po set grcpt_no = '$grcpt_no', grcpt_rec_qty = '$grcpt_rec_qty', grcpt_rec_date = '$grcpt_rec_date', 			           			        grcpt_apprve = '$grcpt_apprve', grcpt_apprve_by = '$grcpt_apprve_by', grcpt_apprve_date = '$grcpt_apprve_date', status = '$status'  where 	          	        po_num = '$po_num'";
		
		echo "<br>";
		
		echo "update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'";
		
		echo "<br>";
		
		echo "insert into TIPLDB..po_grcpt_odr_history 
		(grcpt_no, po_num, po_order_qty, grcpt_recived_qty, grcpt_recived_date, grcpt_created_by, grcpt_created_date, grcpt_status) 
		values 
		('$grcpt_no','$po_num','$po_qty','$grcpt_rec_qty','$grcpt_rec_date','$grcpt_apprve_by','$grcpt_apprve_date','$status')";*/
		
		
		$this->db->trans_start();
		
		$this->db->query("update TIPLDB..insert_po set grcpt_no = '$grcpt_no', grcpt_rec_qty = '$grcpt_rec_qty', grcpt_rec_date = '$grcpt_rec_date', 			        grcpt_apprve = '$grcpt_apprve', grcpt_apprve_by = '$grcpt_apprve_by', grcpt_apprve_date = '$grcpt_apprve_date', status = '$status'  where 	          	        po_num = '$po_num'");
		
		$this->db->query("update TIPLDB..po_master_table set status = '$status' where po_num = '$po_num'");
		
		$this->db->query("insert into TIPLDB..po_grcpt_odr_history 
		(grcpt_no, po_num, po_order_qty, grcpt_recived_qty, grcpt_recived_date, grcpt_created_by, grcpt_created_date, grcpt_status) 
		values ('$grcpt_no','$po_num','$po_qty','$grcpt_rec_qty','$grcpt_rec_date','$grcpt_apprve_by','$grcpt_apprve_date','$status')");
		
		$this->db->trans_complete();
	}
}  
?>