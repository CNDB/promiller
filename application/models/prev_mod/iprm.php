<?php
class iprm extends CI_Model  
   {  
      function __construct()  
      {  
         // Call the Model constructor  
         parent::__construct();  
      }  
      //we will use the select function  
      public function select_prqit_prno()  
      {  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail where SUBSTRING(prqit_prno, 1, 3) in ('FPR','IPR','LPR') 
		 and prqit_prlinestatus ='FR'");  

         return $query;  
     }
	 
	 public function view_ipr($ipr_no)  
      {  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..prq_preqm_pur_reqst_hdr b, tipldb..pr_submit_table c, 
		 scmdb..itm_iou_itemvarhdr d where a.prqit_prno = b.preqm_prno  and a.prqit_prno = c.pr_num  and a.prqit_prno = '$ipr_no' 
		 and a.prqit_itemcode = d.iou_itemcode"); 
		  
         return $query;  
      }
	  
	  public function view_suppcode($ipr_no)  
      {  
         $query = $this->db->query("select * from scmdb..prq_prqit_item_detail a, scmdb..supp_spmn_suplmain c
		 where a.prqit_supplier_code = c.supp_spmn_supcode and a.prqit_prno = '$ipr_no'");  
		 
         return $query;  
      }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	  public function pendal_info($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	  public function pendal_info_monthwise($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }    
	  
	  public function pendal_info_last5_trans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function pendal_info_reorder_lvl_qty($itemcode)
	  {
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function lstyrcumnrec1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function lastfivetrans1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function itemdescmaster($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  /******* Pendal Card Models *******/
	 
	  public function pendal_master($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_whstkblnc($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_pndposopr($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_allocat($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  
	   public function pendal_master_allocat_tot($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_wipdet($itemcode)
	  {
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_lastfivetrans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_lstyrcumnrec($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_supplier($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_mnthwsecurryrcons($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_bomdetail($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	 /******** Months Of Inventory if item is in reorder level *********/
	 
	 public function monthofinvtry($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
	   }
	   
	  public function orderinadvnce($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
	   }
	   
	   //Action Timing Report Queries
	  
	  public function prerplive($ipr_no)
	  {
		$query = $this->db->query("select * from scmdb..prq_preqm_pur_reqst_hdr a, tipldb..pr_submit_table b where a.preqm_prno = b.pr_num 
and a.preqm_prno = '$ipr_no'");
		
		return $query;  
	  }
	  // Action Timing Report Queries
	  
	  //Chat History
	  
	  public function chat_history_table($ipr_no)
	  {
		$query = $this->db->query("select * from tipldb..pr_submit_table_comment where pr_num = '$ipr_no' and enabled = 'YES' order by s_no ASC");
		
		return $query;  
	  }
	  
	  //Chat History
	 
   }  
?>
