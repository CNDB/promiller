<?php
class scr_pom extends CI_Model  
   {  
      function __construct()  
      {    
         parent::__construct();  
      }  
	   
      public function select_prqit_prno()  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr where pomas_podocstatus ='FR'");  

         return $query;  
     }
	 
	 public function view_poqit_pono($selectpr)  
      {  
         $query = $this->db->query("select * from scmdb..po_pomas_pur_order_hdr a, scmdb..po_poitm_item_detail b, 
			scmdb..supp_spmn_suplmain c, scmdb..po_poprq_poprcovg_detail d, scmdb..supp_addr_address e, 
			scmdb..itm_ml_multilanguage f , scmdb..itm_lov_varianthdr g, scmdb..po_paytm_doclevel_detail h
			where a.pomas_pono = b.poitm_pono and a.pomas_suppliercode = c.supp_spmn_supcode 
			and a.pomas_pono = d.poprq_pono and a.pomas_pono ='$selectpr' and b.poitm_polineno = d.poprq_polineno 
			and c.supp_spmn_supcode = e.supp_addr_supcode and b.poitm_itemcode = f.ml_itemcode
			and f.ml_itemcode = g.lov_itemcode and a.pomas_pono = h.paytm_pono");  
         return $query;  
      }
	  
	  public function procesdure_run($itemcode)
	  {
	 	 $query = $this->db->query("exec tipldb..pendalcard '$itemcode'");
		 	
		 return $query;    
	  }
	  
	 public function pendal_info($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where itemCode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	  public function pendal_info_monthwise($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }    
	  
	  public function pendal_info_last5_trans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function pendal_info_reorder_lvl_qty($itemcode)
	  {
	 	 $query = $this->db->query("select * from scmdb..itm_iou_itemvarhdr where iou_itemcode = '$itemcode'");
		 	
		 return $query;
		 
	  } 
	  
	   public function lstyrcumnrec1($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function lastfivetrans1($itemcode)
	  {
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg a, scmdb..po_paytm_doclevel_detail b where a.flag = 'ItemLastFiveTrans' and 
		 a.LastPOGRTran = b.paytm_pono and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  
	  public function itemdescmaster($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	 
	 /********* Pendal Card Models *******/
	 
	  public function pendal_master($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_whstkblnc($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_pndposopr($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='PUR_PO' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_allocat($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  
	   public function pendal_master_allocat_tot($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemAllocTransTOTAL' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_wipdet($itemcode)
	  {
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg where Flag = 'ItemWIPTrans' and ItemCode = '$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_lastfivetrans($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemLastFiveTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_lstyrcumnrec($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='LastYrConsRecptDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_supplier($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='SuppDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   public function pendal_master_mnthwsecurryrcons($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearIssTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_mnthwsecurryrrcpt($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemYearRecTrans' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function pendal_master_bomdetail($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='BomDetail' and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	   /***** Months Of Inventory if item is in reorder level ******/
	 public function monthofinvtry($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where Flag='ItemWarehouseStkBalance' and ItemCode='$itemcode'");
		 	
		 return $query;
	   }
	   
	  public function orderinadvnce($itemcode)
	  {
	 	 $query = $this->db->query("select * from tipldb..pendalcard_rkg where flag='ItemMaster' and itemcode='$itemcode'");
		 	
		 return $query;
	   }
	  
	  public function wrkordr_ser($itemcode)
	  {
	  	$query = $this->db->query("select * from scmdb..pmd_twos_wops_postn where wos_item_no = '$itemcode' and wos_ord_stat_code in('G','E','F','S')");
		
		 return $query;
	  }
	  
	  public function wrkordr_ser_res($workorder)
	  {
	  	$query = $this->db->query("select * from tipldb..pendalcard_rkg a, scmdb..pmd_twos_wops_postn b, scmdb..pmd_twoh_wo_headr c, scmdb..cust_lo_info d 
where a.flag='ItemMaster' and a.itemcode = b.wos_item_no and b.wos_wo_no = c.woh_wo_no and c.woh_cust_name = d.clo_cust_code and b.wos_wo_no = '$workorder'");
		
		 return $query;
	  }
	   
	   public function insert_po_sub($data, $po_quote_frmsupp_newname)
	  {
	 	$username            = $_SESSION['username'];
		$po_num              = $this->input->post("po_num");
		$po_line_no          = $this->input->post("po_line_no");
		$po_ipr_no           = $this->input->post("po_ipr_no");
		$po_date             = $this->input->post("po_date");
		$po_item_code        = $this->input->post("po_item_code");
		$po_qty              = $this->input->post("po_qty");
		$po_uom              = $this->input->post("po_uom");
		$po_cost_pr_unt      = $this->input->post("po_cost_pr_unt");
		$po_need_date        = $this->input->post("po_need_date");
		$po_wh_code          = $this->input->post("po_wh_code");
		$po_basic_val        = $this->input->post("po_basic_val");
		$po_tot_val          = $this->input->post("po_tot_val");
		$po_supp_code        = $this->input->post("po_supp_code");
		$po_supp_name        = $this->input->post("po_supp_name");
		$po_supp_add         = $this->input->post("po_supp_add");
		$po_supp_email       = $this->input->post("po_supp_email");
		$po_drwg_refno       = $this->input->post("po_drwg_refno");
		$item_desc           = $this->input->post("item_desc");
		$payterm             = $this->input->post("payterm");
		$freight             = $this->input->post("freight");
		$po_rmks             = $this->input->post("po_rmks");
		$po_deli_type        = $this->input->post("po_deli_type");
		$po_lead_time        = $this->input->post("po_lead_time");
		$po_manfact_clernce  = $this->input->post("po_manfact_clernce");
		$po_dispatch_inst    = $this->input->post("po_dispatch_inst");
		$po_approval_lvl1    = $this->input->post("po_approval_lvl1");
		$po_approvedby_lvl1  = $_SESSION['username'];
		$currency            = $this->input->post("currency");
		$erp_part_code       = $this->input->post("carrier_name");
		$carrier_name        = $this->input->post("carrier_name");
		$lastyr_cons         = $this->input->post("lastyr_cons");
		$current_stk         = $this->input->post("current_stk");
		$reservation_qty     = $this->input->post("reservation_qty");
		$for_stk_quantity    = $this->input->post("for_stk_quantity");
		$against_cons        = $this->input->post("against_cons");
		$calculation         = $this->input->post("calculation");
		$last_price          = $this->input->post("last_price");
		$current_price       = $this->input->post("current_price");
		$pnf                 = $this->input->post("pnf");
		$po_total_value      = $this->input->post("po_total_value");
		$item_remarks        = $this->input->post("item_remarks");
		//$project_order     = $this->input->post("project_order");
		//$pro_ordr_name     = $this->input->post("pro_ordr_name");
		$status              = "PO Send For Authorization";
		$username            = $_SESSION['username'];
		

		 $sql = "insert into tipldb..insert_po (po_num , po_line_no , po_ipr_no , po_date , po_item_code , po_qty , po_uom , po_cost_pr_unt , po_need_date , po_wh_code , po_basic_val , po_tot_val , po_supp_code , po_supp_name , po_supp_add , po_supp_email , po_drwg_refno , po_itm_desc, payterm, freight,  po_quote_frmsupp , po_rmks , po_deli_type , po_lead_time , po_manfact_clernce , po_dispatch_inst , po_approval_lvl1 , po_approvedby_lvl1, status, currency, erp_part_code, carrier_name, lastyr_cons, current_stk, reservation_qty, for_stk_quantity, against_cons, calculation, last_price, current_price, pnf, po_total_value, item_remarks)
		  
		VALUES ('".$po_num."','".$po_line_no."','".$po_ipr_no."','".$po_date."','".$po_item_code."','".$po_qty."','".$po_uom."','".$po_cost_pr_unt."','".$po_need_date."','".$po_wh_code."','".$po_basic_val."','".$po_tot_val."','".$po_supp_code."','".$po_supp_name."','".$po_supp_add."','".$po_supp_email."','".$po_drwg_refno."','".$item_desc."','".$payterm."','".$freight."','".$po_quote_frmsupp_newname."','".$po_rmks."','".$po_deli_type."','".$po_lead_time."','".$po_manfact_clernce."','".$po_dispatch_inst."','".$po_approval_lvl1."','".$po_approvedby_lvl1."','".$status."','".$currency."','".$erp_part_code."', '".$carrier_name."', '".$lastyr_cons."', '".$current_stk."', '".$reservation_qty."', '".$for_stk_quantity."', '".$against_cons."', '".$calculation."', '".$last_price."', '".$current_price."', '".$pnf."', '".$po_total_value."', '".$item_remarks."')";
		
		$this->db->query($sql);
		
		
		
	  }
		/********** Level 2 **********/	 
	 
	 public function select_prqit_prno_lvl2()  
      {  
         $query = $this->db->query("select * from tipldb..insert_po where po_approval_lvl1 = 'Approve' ");  

         return $query;  
     }
	 
	 public function po_view_lvl2($selectpr)  
      {  
         $query = $this->db->query("select * from tipldb..insert_po where po_num = '$selectpr'");  

         return $query;  
     }
	 
	 public function lastfivetrans1_lvl2($itemcode)
	  {
	 	 $query = $this->db->query("select * from TIPLDB..pendalcard_rkg a, scmdb..po_paytm_doclevel_detail b where a.flag = 'ItemLastFiveTrans' and 
		 a.LastPOGRTran = b.paytm_pono and ItemCode='$itemcode'");
		 	
		 return $query;
		 
	  }
	  
	  public function insert_po_sub_lvl2($data)

	  {
		$po_num                = $this->input->post("po_num");
		$po_s_no               = $this->input->post("po_s_no");
		$po_approval_lvl2      = $this->input->post("po_approval_lvl2");
		$po_approvedby_lvl2    = $_SESSION['username'];
		$po_approvaldate_lvl2  =  date('Y-m-d H:i:s');
		$status                = "Authorized PO Send to Supplier";
		
		$sql = "update TIPLDB..insert_po set po_approval_lvl2 = '$po_approval_lvl2', po_approvalby_lvl2 = '$po_approvedby_lvl2', status='$status', po_approvaldate_lvl2 = '$po_approvaldate_lvl2' where po_num = '$po_num' and po_s_no = '$po_s_no' ";
		
		$this->db->query($sql);
		
		
		
	  }
	 
   }  
?>