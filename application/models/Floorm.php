<?php
class floorm extends CI_Model{  
      function __construct(){   
         parent::__construct();  
      }
	  
	  //Vehicle Entry
	  public function VhclEntry($data){    
	 	$username = $_SESSION['username'];
		$vehicle_arr_tm  = $this->input->post("vehicle_arr_tm");
		$vhcl_type = $this->input->post("vhcl_type");
		$vhcl_reg_no = $this->input->post("vhcl_reg_no");
		$driver_nm = $this->input->post("driver_nm");
		$driver_mob_no = $this->input->post("driver_mob_no");
		$vehicle_arr_plc = $this->input->post("vehicle_arr_plc");
		$doc_by_driver = $this->input->post("doc_by_driver");
		$vehicle_dep_tm = $this->input->post("vehicle_dep_tm");
		$status = "Pending For Weighting Entry";
		
		$sql = "insert into vehicle_entry(vehicle_arr_tm, vhcl_type, vhcl_reg_no, driver_nm, driver_mob_no, doc_by_driver, vehicle_arr_plc, status, created_by) 
		values 
		('$vehicle_arr_tm', '$vhcl_type', '$vhcl_reg_no', '$driver_nm', '$driver_mob_no', '$doc_by_driver', '$vehicle_arr_plc', '$status', '$username')";
		
		$this->db->query($sql);
		
		//action timing
		$sql_at = "insert into vehicle_entry_history(vhcl_reg_no,stage,created_by) values('$vhcl_reg_no','$status','$username')";
		$this->db->query($sql_at);
	  }
	  
	  //Vehicle Weighing
	  public function VhclEntryWeighing($data){    
	 	$username = $_SESSION['username'];
		$vhcl_reg_no = $this->input->post("vhcl_reg_no");		
		$vehicle_wht_scale = $this->input->post("vehicle_wht_scale");
		$vehicle_wht_bill = $this->input->post("vehicle_wht_bill");
		$status = "Pending For Sample Test";
		
		$sql = "update vehicle_entry set vehicle_wht_scale = '$vehicle_wht_scale', vehicle_wht_bill = '$vehicle_wht_bill', status = '$status', created_by = '$username' where vhcl_reg_no = '$vhcl_reg_no'";
		
		$this->db->query($sql);
		
		$sql_at = "insert into vehicle_entry_history(vhcl_reg_no,stage,created_by) values('$vhcl_reg_no','$status','$username')";		
		$this->db->query($sql_at);
	  }
	  
	  //Vehicle Sample Test
	  public function VhclEntrySt($data){    
	 	$username = $_SESSION['username'];
		$vhcl_reg_no = $this->input->post("vhcl_reg_no");
		$status = "Pending For Sample Test Approval";
		$moist_perc = $this->input->post("moist_perc");
		$hdhlw_perc = $this->input->post("hdhlw_perc");
		$murgi_perc = $this->input->post("murgi_perc");
		$stone_perc = $this->input->post("stone_perc");
		$fg_perc = $this->input->post("fg_perc");
		$dantal_perc = $this->input->post("dantal_perc");
		$chilka_perc = $this->input->post("chilka_perc");
		$stat_wheat = $this->input->post("stat_wheat");
		
		
		$sql = "update vehicle_entry set status = '$status', created_by = '$username', moist_perc = '$moist_perc', hdhlw_perc = '$hdhlw_perc', murgi_perc = '$murgi_perc', stone_perc = '$stone_perc', fg_perc = '$fg_perc', dantal_perc = '$dantal_perc', chilka_perc = '$chilka_perc', stat_wheat = '$stat_wheat' where vhcl_reg_no = '$vhcl_reg_no'";
		
		$this->db->query($sql);
		
		//action timing
		$sql_at = "insert into vehicle_entry_history(vhcl_reg_no,stage,created_by) values('$vhcl_reg_no','$status','$username')";		
		$this->db->query($sql_at);
	  }
	  
	  //Vehicle Sample Test Approval
	  public function VhclEntryStApp($data){    
	 	$username = $_SESSION['username'];
		//$status = "Pending For Unloading";
		$vhcl_reg_no = $this->input->post("vhcl_reg_no");
		$submit_btn_app = $this->input->post("submit_btn_app");
		
		if($submit_btn_app == "Approve"){
			$status = "Pending For Unloading";
		} else {
			$status = "Pending For Gate Exit";
		}
		
		$sql = "update vehicle_entry set status = '$status' where vhcl_reg_no = '$vhcl_reg_no'";
		
		$this->db->query($sql);
		
		//action timing
		$sql_at = "insert into vehicle_entry_history(vhcl_reg_no,stage,created_by) values('$vhcl_reg_no','$status','$username')";		
		$this->db->query($sql_at);
	  }
	  
	  //Vehicle Entry Unload
	  public function VhclEntryUnload($data){    
	 	$username = $_SESSION['username'];
		$vhcl_reg_no = $this->input->post("vhcl_reg_no");
		$status = "Pending For Gate Exit";
		$bag_type = $this->input->post("bag_type");
		$no_bags = $this->input->post("no_bags");
		
		$sql = "update vehicle_entry set bag_type = '$bag_type', no_bags = '$no_bags', status = '$status', created_by = '$username' where vhcl_reg_no = '$vhcl_reg_no'";
		
		$this->db->query($sql);
		
		//action timing
		$sql_at = "insert into vehicle_entry_history(vhcl_reg_no,stage,created_by) values('$vhcl_reg_no','$status','$username')";		
		$this->db->query($sql_at);
	  }
	  
	  //Vehicle Gate Exit
	  public function VhclEntryGateExit($data){    
	 	$username = $_SESSION['username'];
		$vhcl_reg_no = $this->input->post("vhcl_reg_no");
		$vehicle_dep_tm = $this->input->post("vehicle_dep_tm");
		$status = "Vehicle Entry Cycle Complete";
		
		$sql = "update vehicle_entry set vehicle_dep_tm = '$vehicle_dep_tm', status = '$status', created_by = '$username' where vhcl_reg_no = '$vhcl_reg_no'";
		
		$this->db->query($sql);
		
		//action timing
		$sql_at = "insert into vehicle_entry_history(vhcl_reg_no,stage,created_by) values('$vhcl_reg_no','$status','$username')";		
		$this->db->query($sql_at);
	  }
	 
   }  
?>