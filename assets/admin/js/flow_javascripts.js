<!-- FRESH PURCHASE REQUEST -->

function show_content_fresh_pr(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}

<!-- AUTHORIZE PURCHASE REQUEST PLANNING -->

function show_content_auth_pr_plan(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}

<!-- DISAPPROVED PURCHASE REQUEST PLANNING -->

function show_content_dis_pr_plan(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}

<!-- ERP NOT AUTHORIZED PURCHASE REQUESTS -->
function show_content_erp_not_auth_pr(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}

<!-- ERP AUTHORIZED PURCHASE REQUESTS WHOSE PO NOT CREATED -->
function show_content_pr_po_report(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}

<!-- FRESH PURCHASE REQUEST -->

function show_content_dis_pr_pur(a){
	if(a == 'ALL'){
		
		$('#all_pr').show();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'FPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').show();
		$('#all_ipr').hide();
		$('#all_lpr').hide();
		
	} else if (a == 'IPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').show();
		$('#all_lpr').hide();
		
	} else if (a == 'LPR') {
		
		$('#all_pr').hide();
		$('#all_fpr').hide();
		$('#all_ipr').hide();
		$('#all_lpr').show();
		
	}
}

<!-- ERP Fresh Purchase Orders Pending For Live PO Creation -->
function show_content_fresh_po(a){
	if(a == 'ALL'){
		
		$('#all_fresh_po').show();
		$('#all_fresh_fpo').hide();
		$('#all_fresh_ipo').hide();
		$('#all_fresh_lpo').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_fresh_po').hide();
		$('#all_fresh_fpo').show();
		$('#all_fresh_ipo').hide();
		$('#all_fresh_lpo').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_fresh_po').hide();
		$('#all_fresh_fpo').hide();
		$('#all_fresh_ipo').show();
		$('#all_fresh_lpo').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_fresh_po').hide();
		$('#all_fresh_fpo').hide();
		$('#all_fresh_ipo').hide();
		$('#all_fresh_lpo').show();
		
	}
}

<!-- Live PO Pending For Authorization LVL1 -->
function show_content_auth_po_lvl1(a){
	if(a == 'ALL'){
		
		$('#all_fresh_po_lvl1').show();
		$('#all_fresh_fpo_lvl1').hide();
		$('#all_fresh_ipo_lvl1').hide();
		$('#all_fresh_lpo_lvl1').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_fresh_po_lvl1').hide();
		$('#all_fresh_fpo_lvl1').show();
		$('#all_fresh_ipo_lvl1').hide();
		$('#all_fresh_lpo_lvl1').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_fresh_po_lvl1').hide();
		$('#all_fresh_fpo_lvl1').hide();
		$('#all_fresh_ipo_lvl1').show();
		$('#all_fresh_lpo_lvl1').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_fresh_po_lvl1').hide();
		$('#all_fresh_fpo_lvl1').hide();
		$('#all_fresh_ipo_lvl1').hide();
		$('#all_fresh_lpo_lvl1').show();
	}
}

<!-- Live PO Creation Pending For Authorization LVL2 -->
function show_content_auth_po_lvl2(a){
	if(a == 'ALL'){
		
		$('#all_fresh_po_lvl2').show();
		$('#all_fresh_fpo_lvl2').hide();
		$('#all_fresh_ipo_lvl2').hide();
		$('#all_fresh_lpo_lvl2').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_fresh_po_lvl2').hide();
		$('#all_fresh_fpo_lvl2').show();
		$('#all_fresh_ipo_lvl2').hide();
		$('#all_fresh_lpo_lvl2').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_fresh_po_lvl2').hide();
		$('#all_fresh_fpo_lvl2').hide();
		$('#all_fresh_ipo_lvl2').show();
		$('#all_fresh_lpo_lvl2').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_fresh_po_lvl2').hide();
		$('#all_fresh_fpo_lvl2').hide();
		$('#all_fresh_ipo_lvl2').hide();
		$('#all_fresh_lpo_lvl2').show();
		
	}
}

<!-- Live Authorized Purchase Order Pending For Send Supplier For Purchase -->
function show_content_supp_for_pur(a){
	if(a == 'ALL'){
		
		$('#all_po_supp_for_pur').show();
		$('#all_po_supp_for_pur_fpo').hide();
		$('#all_po_supp_for_pur_ipo').hide();
		$('#all_po_supp_for_pur_lpo').hide();
		
	} else if (a == 'FPO') {
		
		$('#all_po_supp_for_pur').hide();
		$('#all_po_supp_for_pur_fpo').show();
		$('#all_po_supp_for_pur_ipo').hide();
		$('#all_po_supp_for_pur_lpo').hide();
		
	} else if (a == 'IPO') {
		
		$('#all_po_supp_for_pur').hide();
		$('#all_po_supp_for_pur_fpo').hide();
		$('#all_po_supp_for_pur_ipo').show();
		$('#all_po_supp_for_pur_lpo').hide();
		
	} else if (a == 'LPO') {
		
		$('#all_po_supp_for_pur').hide();
		$('#all_po_supp_for_pur_fpo').hide();
		$('#all_po_supp_for_pur_ipo').hide();
		$('#all_po_supp_for_pur_lpo').show();
		
	}
}